﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using Dapper;
using Microsoft.Data.SqlClient;
using Serilog;

namespace MyDrilling.Migrator
{
    public static class NewDbCreator
    {
        public static void CreateIfNotExists(string newServerConnectionString)
        {
            string dbId;
            using (var conn = new SqlConnection(newServerConnectionString))
            {
                dbId = conn.QuerySingle<string>($"select DB_ID('{ConnectionStringExt.NewMyDrillingDbName}')");
            }

            if (!string.IsNullOrEmpty(dbId))
            {
                Log.Logger.Information("new MyDrilling database exists, creation step is skipped");
                return;
            }

            Log.Logger.Information("new MyDrilling database not found, will create it");

            using (var conn = new SqlConnection(newServerConnectionString))
            {
                conn.Execute($"create database {ConnectionStringExt.NewMyDrillingDbName}");
            }

            var sqlFiles = GetSqlFiles();
            foreach (var sqlFile in sqlFiles)
            {
                using (var conn = new SqlConnection(newServerConnectionString.GetNewDbConnectionString()))
                {
                    foreach (var statement in File.ReadAllText(sqlFile).Split(new[] { "GO" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        conn.Execute(statement);
                    }
                }
            }

            Log.Logger.Information("new MyDrilling database created!!!");
        }

        private static string[] GetSqlFiles()
        {
            var migrationsFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
                .Replace(@"tools\MyDrilling.Migrator\bin\Debug\netcoreapp3.1", @"src\MyDrilling.Infrastructure\Migrations");

            return Directory.GetFiles(migrationsFolder, "*.sql").OrderBy(x => x).ToArray();
        }
    }
}
