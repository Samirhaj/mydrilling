﻿id| name| what's in value| in profile| records in db | used in code
--- | --- | ---
2| http://mydrilling.com/ws/2012/03/process/bulletin-confirmer-implemented | | | no
3| http://mydrilling.com/ws/2012/03/process/bulletin-confirmer-receiver | | | no
4| http://mydrilling.com/ws/2012/03/process/bulletin-viewer | | yes | yes
5| http://mydrilling.com/ws/2012/03/process/documentation-viewer | rig| yes | yes
6| http://mydrilling.com/ws/2012/03/process/enquiry-handling-approver | rig | yes | yes
8| http://mydrilling.com/ws/2012/03/process/order-handling-viewer | | yes | yes
9| http://mydrilling.com/ws/2012/03/process/sparepart-viewer | | yes | yes
10| http://mydrilling.com/ws/2012/03/process/realtime-viewer | rig | no | yes
12| http://mydrilling.com/ws/2012/03/userprofile/lastaccepttermsandconditionstimestamp | datetime | no | yes
13| http://mydrilling.com/ws/2012/03/process/mydrilling-viewer | rig | yes | yes
14| http://mydrilling.com/ws/2012/03/process/enquiry-handling-initiator | rig | yes | yes
15| http://mydrilling.com/ws/2012/03/process/admin-useradmin | customer string | no | yes
17| http://mydrilling.com/ws/2012/03/userprofile/myDrilling-user | customer string a| no | yes
18| http://mydrilling.com/ws/2012/03/process/enquiry-handling-collaborator | | yes | yes
19| http://mydrilling.com/ws/2012/03/process/monitoring-viewer | rig | no | yes
20| http://mydrilling.com/ws/2012/03/userprofile/department | some number | no | yes
21| http://mydrilling.com/ws/2012/03/process/admin-sysadmin | All or INTERNAL | no | yes
22| http://mydrilling.com/ws/2012/03/process/kpitrend-viewer | All | no | yes
23| http://mydrilling.com/ws/2012/03/process/profile-subscription-configuration | email | no | yes
24| http://mydrilling.com/ws/2012/03/process/enquiry-subscription-configuration | email | no | yes
25| http://mydrilling.com/ws/2012/03/process/customer-admin | root customer | no | yes
26| http://www.akersolutions.com/ws/2011/04/identity/claims/customer-access | root customer | no | yes
27| http://mydrilling.com/ws/2012/03/process/enquiry-handling-preparation-viewer | rig | yes | yes
28| http://mydrilling.com/ws/2012/03/process/enquiry-handling-approved-viewer | rig | yes | yes
29| http://mydrilling.com/ws/2012/03/process/enquiry-handling-provider | | yes | yes
30| http://mydrilling.com/ws/2012/03/process/enquiry-handling-customer-assigner | | yes | yes
31| http://mydrilling.com/ws/2012/03/process/enquiry-handling-provider-assigner | | yes | yes
32| http://mydrilling.com/ws/2012/03/process/performancemonitoring-viewer | All | no | yes
33| http://mydrilling.com/ws/2012/03/process/performancemonitoring-editor | | no | no
34| http://mydrilling.com/ws/2012/03/process/performancemonitoring-admin | | no | no
35| http://mydrilling.com/ws/2012/03/process/cbm-viewer | rig | yes | yes
36| http://mydrilling.com/ws/2012/03/process/cbm-editor | | no | no
37| http://mydrilling.com/ws/2012/03/process/cbm-admin | | no | no
38| http://mydrilling.com/ws/2012/03/process/upgrade-matrix-viewer | | no | no
39| http://mydrilling.com/ws/2012/03/process/upgrade-matrix-editor | | no | no
40| http://mydrilling.com/ws/2012/03/process/upgrade-matrix-admin | | no | no
45| http://mydrilling.com/ws/2012/03/process/enquiry-handling-notifier | rig | yes | yes
46| http://mydrilling.com/ws/2012/03/process/drillcon-viewer | | yes | yes
47| http://mydrilling.com/ws/2012/03/process/smartmodule-viewer | | yes | yes