﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

namespace MyDrilling.Migrator
{
    public static class CommonStringExt
    {
        private static readonly Regex ManyWhiteSpacesRegex = new Regex("[ ]{2,}");

        public static string GetEmailPattern(string firstNameInput, string lastNameInput)
        {
            var firstName = string.IsNullOrEmpty(firstNameInput)
                ? null
                : ManyWhiteSpacesRegex.Replace(firstNameInput.Trim(), " ")
                    .Replace(" ", ".").ToLowerInvariant();
            var lastName = string.IsNullOrEmpty(lastNameInput)
                ? null
                : ManyWhiteSpacesRegex.Replace(lastNameInput.Trim(), " ")
                    .Replace(" ", ".").ToLowerInvariant();
            var pattern = $"{firstName}.{lastName}@";

            return pattern;
        }

        /// <summary>
        /// Invariant string comparer to ignore case and diacritics
        /// </summary>
        public static readonly StringComparer MdStringComparer = StringComparer.Create(CultureInfo.InvariantCulture, CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreCase);

        public static bool MdStartsWith(this string str, string value)
        {
            if (str.Length >= value.Length)
                return MdStringComparer.Compare(str.Substring(0, value.Length), value) == 0;
            else
                return false;
        }

        public static string Normalize(this string email)
        {
            return email.Trim().ToLowerInvariant();
        }
    }
}
