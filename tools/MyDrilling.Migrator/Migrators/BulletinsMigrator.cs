﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Dapper;
using EFCore.BulkExtensions;
using Microsoft.Data.SqlClient;
using MyDrilling.Core;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Bulletin;
using Serilog;

namespace MyDrilling.Migrator.Migrators
{
    public static class BulletinsMigrator
    {
        private static string _newServer;
        private static string _oldServer;

        public static void Do(string oldServer, string newServer)
        {
            _oldServer = oldServer;
            _newServer = newServer;

            Log.Logger.Information("Importing Bulletins...");

            using (var newDb = _newServer.GetNewMyDrillingDbContext())
            {
                if (newDb.Bulletin_BulletinTypes.Any() || newDb.Bulletin_ZaBulletins.Any() || newDb.Bulletin_ZbBulletins.Any())
                {
                    Log.Logger.Information("Bulletins have been already imported.");
                    return;
                }
            }
            MigrateBulletinTypes();
            MigrateZaBulletins();

            Log.Logger.Information("Imported bulletins");
        }

        private static void MigrateBulletinTypes()
        {
            var source = _oldServer.GetDwhConnectionString();
            var exportBulletinTypeQuery = "SELECT Bulletin_Type, Bulletin_Consolidated_Name FROM DIM_Bulletin_Type";
            List<DimBulletinType> dimBulletinTypes;
            using (var sourceConnection = new SqlConnection(source))
            {
                dimBulletinTypes = sourceConnection.Query<DimBulletinType>(exportBulletinTypeQuery).ToList();
            }

            if (!dimBulletinTypes.Any())
            {
                Log.Error("No Bulletin Types found.");
            }

            var newBullitens = dimBulletinTypes.Select(bt => new BulletinType
            {
                Name = bt.Bulletin_Type,
                ConsolidatedName = bt.Bulletin_Consolidated_Name
            });

            using (var newDb = _newServer.GetNewMyDrillingDbContext())
            {
                newDb.Bulletin_BulletinTypes.AddRange(newBullitens);
                newDb.SaveChanges();
            }
        }

        private static void MigrateZaBulletins()
        {
            var dwhSource = _oldServer.GetDwhConnectionString();
            var exportZaQuery = "SELECT * FROM Dim_Bulletin_v2";
            List<DimZaBulletin> dimZaBulletins;
            using (var sourceConnection = new SqlConnection(dwhSource))
            {
                dimZaBulletins = sourceConnection.Query<DimZaBulletin>(exportZaQuery).ToList();
            }

            if (!dimZaBulletins.Any())
            {
                Log.Warning("No ZA Bulletins found");
                return;
            }

            var bulletinTypes = ExtractBulletinTypes();
            var docIds = ExtractDocumentIds();

            var newBulletins = new List<ZaBulletin>();
            foreach (var dimZaBulletinGroup in dimZaBulletins.GroupBy(x => x.REFERENCE_ID))
            {
                var dimZaBulletin = dimZaBulletinGroup.First();
                var newBulletin = new ZaBulletin
                {
                    ReferenceId = dimZaBulletin.REFERENCE_ID,
                    Title = dimZaBulletin.Bulletin_Title.Trim(),
                    FileType = dimZaBulletin.Bulletin_File_Type,
                    Description = dimZaBulletin.Description.Trim(),
                    IssueDate = dimZaBulletin.Bulletin_Issued_Date,
                    Created = dimZaBulletin.INSERT_DATE
                };

                if (bulletinTypes.TryGetValue(dimZaBulletin.Bulletin_Type, out var typeId))
                {
                    newBulletin.TypeId = typeId;
                }
                else
                {
                    Log.Logger.Warning($"Bulletin type {dimZaBulletin.Bulletin_Type} not found.");
                }

                if (docIds.TryGetValue(dimZaBulletin.Bulletin_Document_ID, out var docId))
                {
                    newBulletin.DocumentReferenceId = docId;
                }
                else
                {
                    Log.Logger.Warning($"Bulletin Document Id for {dimZaBulletin.Bulletin_Document_ID} not found.");
                }

                newBulletins.Add(newBulletin);
            }

            List<ZaBulletin> zaBulletins;
            using (var newDb = _newServer.GetNewMyDrillingDbContext())
            {
                newDb.Bulletin_ZaBulletins.AddRange(newBulletins);
                newDb.SaveChanges();
                zaBulletins = newDb.Bulletin_ZaBulletins.ToList();
            }

            var newZbsMap = ExtractZbBulletins();
            var numberToSave = newZbsMap.Values.Sum(v => v.Count);
            var zbsToInsert = new List<ZbBulletin>(numberToSave);
            foreach (var zaBulletin in zaBulletins)
            {
                if (newZbsMap.TryGetValue(zaBulletin.ReferenceId, out var zbs))
                {
                    foreach (var zbBulletin in zbs)
                    {
                        zbBulletin.ZaBulletinId = zaBulletin.Id;
                    }

                    zbsToInsert.AddRange(zbs);
                }
            }

            if (!zbsToInsert.Any())
            {
                Log.Warning("No ZB Bulletins found");
                return;
            }

            using (var newDb = _newServer.GetNewMyDrillingDbContext())
            {
                newDb.BulkInsert(zbsToInsert);
            }
        }
        
        private static Dictionary<string, short> ExtractBulletinTypes()
        {
            using (var newDb = _newServer.GetNewMyDrillingDbContext())
            {
                return newDb.Bulletin_BulletinTypes.ToDictionary(t => t.Name, t => t.Id);
            }
        }

        private static Dictionary<string, string> ExtractDocumentIds()
        {
            var stg1Source = _oldServer.GetStage1ConnectionString();
            var docIdQuery = "SELECT gi.GlobalId , ri.ReferenceId " +
                             "FROM SIB_GlobalIdentifiers gi JOIN SIB_RelatedIdentifiers ri on gi.GsiId = ri.GsiId " +
                             "WHERE GlobalId LIKE '%id/11/globalidentifier%'"; //extract only bulletin related Docs

            using (var sourceConnection = new SqlConnection(stg1Source))
            {
                return sourceConnection.Query<DocumentMap>(docIdQuery).ToDictionary(dm => dm.GlobalId, dm => dm.ReferenceId);
            }
        }

        private static Dictionary<int, long> GetRigIdMap()
        {
            var source = _oldServer.GetDwhConnectionString();
            var rigQuery = "SELECT PK_Dim_Rig , REFERENCE_ID FROM Dim_Rig";

            List<DimRig> dimRigs;
            using (var sourceConnection = new SqlConnection(source))
            {
                dimRigs = sourceConnection.Query<DimRig>(rigQuery).ToList();
            }

            Dictionary<long, long> rigs;
            using (var newDb = _newServer.GetNewMyDrillingDbContext())
            {
                rigs = newDb.Rigs.ToDictionary(r => r.ReferenceId, b => b.Id);
            }

            var result = new Dictionary<int, long>();
            foreach (var dimRig in dimRigs)
            {
                if (rigs.TryGetValue(dimRig.REFERENCE_ID.ToLongReferenceId(), out var rigId))
                {
                    result.Add(dimRig.PK_Dim_Rig, rigId);
                }
                else
                {
                    Log.Warning($"No rig found for {dimRig.REFERENCE_ID} in the new DB.");
                }
            }

            return result;
        }

        private static Dictionary<int, long> GetCustomerIdMap()
        {
            var source = _oldServer.GetDwhConnectionString();
            var rigQuery = "SELECT PK_Dim_Customer, REFERENCE_ID FROM Dim_Customer WHERE REFERENCE_ID IS NOT NULL";

            List<DimCustomer> dimCustomers;
            using (var sourceConnection = new SqlConnection(source))
            {
                dimCustomers = sourceConnection.Query<DimCustomer>(rigQuery).ToList();
            }

            Dictionary<string, long> customers;
            using (var newDb = _newServer.GetNewMyDrillingDbContext())
            {
                customers = newDb.Customers.Where(x => !string.IsNullOrEmpty(x.ReferenceId)).ToDictionary(r => r.ReferenceId, b => b.Id);
            }

            var result = new Dictionary<int, long>();
            foreach (var dimCustomer in dimCustomers)
            {
                if (customers.TryGetValue(dimCustomer.REFERENCE_ID, out var customerId))
                {
                    result.Add(dimCustomer.PK_Dim_Customer, customerId);
                }
            }

            return result;
        }
        
        private static Dictionary<int, long> GetEquipmentMap()
        {
            var source = _oldServer.GetDwhConnectionString();
            var rigQuery = "SELECT PK_Dim_Equipment, REFERENCE_ID FROM Dim_Equipment";

            List<DimEquipment> dimEquipment;
            using (var sourceConnection = new SqlConnection(source))
            {
                dimEquipment = sourceConnection.Query<DimEquipment>(rigQuery).ToList();
            }

            Dictionary<long, long> equipment;
            using (var newDb = _newServer.GetNewMyDrillingDbContext())
            {
                equipment = newDb.Equipments.ToDictionary(r => r.ReferenceId, b => b.Id);
            }

            var result = new Dictionary<int, long>();
            foreach (var dimEq in dimEquipment)
            {
                if (equipment.TryGetValue(dimEq.REFERENCE_ID.ToLongReferenceId(), out var eqId))
                {
                    result.Add(dimEq.PK_Dim_Equipment, eqId);
                }
            }

            return result;
        }

        private static Dictionary<string, List<ZbBulletin>> ExtractZbBulletins()
        {
            var source = _oldServer.GetDwhConnectionString();
            //use left join for Process_State to migrate bulletins with no matching state in DIM_Process_State table
            //there several bulletins with the same REFERENCE_ID in Fact_Bulletin_Confirmed_Received_v2 that shouldn't be migrated
            var receivedZbQuery =
                "SELECT r.*, b.REFERENCE_ID AS 'PARENT_REFERENCE_ID',  1 /*Bulletin_Confirmed_Received*/ AS 'Bulletin_Group', " +
                "p.Process_State_Description AS 'Process_State' FROM Fact_Bulletin_Confirmed_Received_v2 r " +
                "RIGHT JOIN Dim_Bulletin_v2 b ON r.PARENT_GLOBAL_ID = b.GLOBAL_ID LEFT JOIN DIM_Process_State p on r.STATE = p.Process_State_Id " +
                "WHERE r.LAST_VERSION = 1 AND r.REFERENCE_ID <> '123456789'";

            var implZbQuery =
                "SELECT i.*, b.REFERENCE_ID AS 'PARENT_REFERENCE_ID', 2 /*Bulletin_Confirmed_Implemented*/ AS 'Bulletin_Group', " +
                "p.Process_State_Description AS 'Process_State' FROM Fact_Bulletin_Confirmed_Implemented_v2 i " +
                "RIGHT JOIN Dim_Bulletin_v2 b ON i.PARENT_GLOBAL_ID = b.GLOBAL_ID LEFT JOIN DIM_Process_State p on i.STATE = p.Process_State_Id " +
                "WHERE i.LAST_VERSION = 1 AND i.REFERENCE_ID <> '1234567890'";

            List<DimZbBulletin> dimZbBulletins;
            using (var sourceConnection = new SqlConnection(source))
            {
                var receivedZbBulletins = sourceConnection.Query<DimZbBulletin>(receivedZbQuery).ToList();
                var implZbBulletins = sourceConnection.Query<DimZbBulletin>(implZbQuery).ToList();
                dimZbBulletins = receivedZbBulletins.Concat(implZbBulletins).ToList();
            }

            if (!dimZbBulletins.Any())
            {
                Log.Warning("No ZB Bulletins found");
            }

            dimZbBulletins = dimZbBulletins.GroupBy(x => x.REFERENCE_ID).Select(x => x.First()).ToList();

            var rigIdMap = GetRigIdMap();
            var customers = GetCustomerIdMap();
            var equipment = GetEquipmentMap();

            Dictionary<string, long> users;
            using (var newDb = _newServer.GetNewMyDrillingDbContext())
            {
                users = newDb.Users.ToDictionary(u => u.Upn, u => u.Id);
            }

            var result = new Dictionary<string, List<ZbBulletin>>();
            foreach (var dimZb in dimZbBulletins)
            {
                var newZb = new ZbBulletin
                {
                    ReferenceId = dimZb.REFERENCE_ID,
                    ConfirmationTimestamp = dimZb.Bulletin_Confirmation_Timestamp,
                    Created = dimZb.INSERT_DATE,
                    State = dimZb.Process_State.ParseState(),
                    Status = dimZb.Status.ParseStatus(),
                    Group = (BulletinGroup) dimZb.Bulletin_Group
                };

                if (!string.IsNullOrWhiteSpace(dimZb.Bulletin_Confirmation_By)
                    && users.TryGetValue(dimZb.Bulletin_Confirmation_By.ToLower().Trim(), out var userId))
                {
                    newZb.ConfirmedByUserId = userId;
                }

                if (rigIdMap.TryGetValue(dimZb.Dim_Rig_FK, out var rigId))
                {
                    newZb.RigId = rigId;
                }

                if (customers.TryGetValue(dimZb.Dim_Customer_FK, out var customerId))
                {
                    newZb.CustomerId = customerId;
                }

                if (equipment.TryGetValue(dimZb.Dim_Equipment_FK, out var eqId))
                {
                    newZb.EquipmentId = eqId;
                }

                if (!result.ContainsKey(dimZb.PARENT_REFERENCE_ID))
                {
                    result[dimZb.PARENT_REFERENCE_ID] = new List<ZbBulletin> {newZb};
                }
                else
                {
                    result[dimZb.PARENT_REFERENCE_ID].Add(newZb);
                }
            }

            return result;
        }


        private static BulletinState ParseState(this string state)
        {
            if (string.IsNullOrWhiteSpace(state))
            {
                return BulletinState.None;
            }
            return state.Equals("Active") ? BulletinState.Active : BulletinState.Confirmed;
        }

        private static BulletinStatus ParseStatus(this string status)
        {
            if (string.IsNullOrEmpty(status))
            {
                return BulletinStatus.None;
            }

            if (status.Contains("bulletin/implemented/confirmation/status/implemented"))
            {
                return BulletinStatus.Implemented;
            }

            return status.Contains("bulletin/implemented/confirmation/status/na") ? BulletinStatus.Na : BulletinStatus.None;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class DimBulletinType
        {
            public string Bulletin_Type { get; set; }
            public string Bulletin_Consolidated_Name { get; set; }
        }

        public class Result
        {
            public Result(bool isSuccess)
            {
                IsSuccess = isSuccess;
            }

            public Result(bool isSuccess, string message) : this(isSuccess)
            {
                Message = message;
            }

            public bool IsSuccess { get; }
            public string Message { get; }
        }

        /// <summary>
        ///represent entries in Dim_Bulletin_v2 table 
        /// </summary>
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class DimZaBulletin
        {
            public string REFERENCE_ID { get; set; }
            public string Bulletin_Title { get; set; }
            public string Bulletin_Document_ID { get; set; }
            public string Bulletin_Type { get; set; }
            public DateTime Bulletin_Issued_Date { get; set; }
            public string Bulletin_File_Type { get; set; }
            public string Description { get; set; }
            public DateTime INSERT_DATE { get; set; }
            public string INSERT_BY { get; set; }
        }

        /// <summary>
        ///represents entries in Fact_Bulletin_Confirmed_Implemented_v2 and Fact_Bulletin_Confirmed_Received_v2 tables 
        /// </summary>
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class DimZbBulletin
        {
            public string REFERENCE_ID { get; set; }
            public string Bulletin_Confirmation_By { get; set; }
            public DateTime? Bulletin_Confirmation_Timestamp { get; set; }
            public int Dim_Rig_FK { get; set; }
            public int Dim_Customer_FK { get; set; }
            public string PARENT_REFERENCE_ID { get; set; }
            public int Dim_Equipment_FK { get; set; }
            public string Process_State { get; set; }
            public string Status { get; set; }
            public int Bulletin_Group { get; set; }
            public DateTime INSERT_DATE { get; set; }
            public string INSERT_BY { get; set; }
        }

        public class DocumentMap
        {
            public string GlobalId { get; set; }
            public string ReferenceId { get; set; }
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class DimRig
        {
            public int PK_Dim_Rig { get; set; }
            public string REFERENCE_ID { get; set; }
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class DimCustomer
        {
            public int PK_Dim_Customer { get; set; }
            public string REFERENCE_ID { get; set; }
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class DimEquipment
        {
            public int PK_Dim_Equipment { get; set; }
            public string REFERENCE_ID { get; set; }
        }
    }
}
