﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using Azure.Storage.Blobs;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Enquiry;
using Serilog;

namespace MyDrilling.Migrator.Migrators
{
    public static class EnquiryMigrator
    {
        private static readonly HashSet<string> InitialCommentConversationTypes = new HashSet<string>
        {
            "http://www.mydrilling.com/repository/enquiry/conversation/type/id/2",
            "http://www.mydrilling.com/repository/businessobject/id/3/type/id/2"
        };

        public static void Do(string oldServerConnectionString, string newServerConnectionString, BlobContainerClient blobContainerClient)
        {
            Log.Logger.Information("Importing enquiries...");

            blobContainerClient.CreateIfNotExists();

            Dictionary<string, FACT_Enquiry[]> enquiriesGrouped;
            Dictionary<string, FACT_Enquiry_Conversation[]> comments;
            Dictionary<string, FACT_Enquiry_Document[]> files;
            Dictionary<int, string> dimCustomers;
            Dictionary<int, string> dimEquipments;
            Dictionary<int, string> dimRigs;

            using (var conn = new SqlConnection(oldServerConnectionString.GetDwhConnectionString()))
            {
                enquiriesGrouped = conn.Query<FACT_Enquiry>("select * from FACT_Enquiry").GroupBy(x => x.GLOBAL_ID).ToDictionary(x => x.Key, x => x.OrderBy(y => y.LAST_VERSION).ThenBy(y => y.CreatedDate).ToArray());
                comments = conn.Query<FACT_Enquiry_Conversation>("select * from FACT_Enquiry_Conversation where LAST_VERSION = 1 and REFERENCE_ID is not null").GroupBy(x => x.PARENT_GLOBAL_ID).ToDictionary(x => x.Key, x => x.OrderBy(y => y.CreatedDate).ToArray());
                files = conn.Query<FACT_Enquiry_Document>("select * from FACT_Enquiry_Document where LAST_VERSION = 1 and REFERENCE_ID is not null").GroupBy(x => x.PARENT_GLOBAL_ID).ToDictionary(x => x.Key, x => x.ToArray());

                dimCustomers = conn.Query<BulletinsMigrator.DimCustomer>("select PK_Dim_Customer, REFERENCE_ID from dim_customer where Reference_Id is not null")
                    .ToDictionary(x => x.PK_Dim_Customer, x => x.REFERENCE_ID);
                dimEquipments = conn.Query<BulletinsMigrator.DimEquipment>("select PK_Dim_Equipment, REFERENCE_ID from dim_equipment").ToDictionary(x => x.PK_Dim_Equipment, x => x.REFERENCE_ID);
                dimRigs = conn.Query<BulletinsMigrator.DimRig>("select PK_Dim_Rig , REFERENCE_ID from dim_rig").ToDictionary(x => x.PK_Dim_Rig, x => x.REFERENCE_ID);
            }

            Dictionary<string, string[]> usersAdded;
            Dictionary<string, string[]> usersRead;
            Dictionary<string, CommentDocumentLink> links;

            using (var conn = new SqlConnection(oldServerConnectionString.GetPortalConnectionString()))
            {
                usersAdded = conn.Query<UserAddedToEnquiries>("select * from UserAddedToEnquiries").GroupBy(x => x.EnquiryId)
                    .ToDictionary(x => x.Key, x => x.Select(y => y.UserUPN).Distinct().ToArray(), CommonStringExt.MdStringComparer);
                usersRead = conn.Query<UserReadedEnquiries>("select * from UserReadedEnquiries").GroupBy(x => x.EnquiryId)
                    .ToDictionary(x => x.Key, x => x.Select(y => y.UserUPN).Distinct().ToArray(), CommonStringExt.MdStringComparer);
                links = conn.Query<CommentDocumentLink>("select * from CommentDocumentLink").GroupBy(x => x.DocumentGlobalId).ToDictionary(x => x.Key, x => x.First());
            }

            var noReferenceIdEnquiries = new HashSet<int>();
            var noRigEnquiries = new HashSet<int>();
            var noMydCustomerEnquiries = new HashSet<int>();
            var unknownTypeEnquiries = new HashSet<int>();
            var noStateEnquiries = new HashSet<int>();
            var notUpns = new HashSet<string>();
            var addedUsers = 0;

            using (var newDb = newServerConnectionString.GetNewMyDrillingDbContext())
            {
                var customers = newDb.Customers.Where(x => !string.IsNullOrEmpty(x.ReferenceId)).Include(x => x.RootCustomer).ToDictionary(x => x.ReferenceId, x => x);
                var mhwirthCustomer = newDb.RootCustomers.First(x => x.IsInternal);
                var rigs = newDb.Rigs.Include(x => x.Owner).ToDictionary(x => x.ReferenceId, x => x);
                var equipments = newDb.Equipments.ToDictionary(x => x.ReferenceId, x => x);
                var users = newDb.Users.ToDictionary(x => x.Upn, x => x, CommonStringExt.MdStringComparer);

                bool TryGetUser(string upn, out User user)
                {
                    user = null;
                    if (string.IsNullOrEmpty(upn))
                    {
                        return false;
                    }

                    if (!User.UpnValidationPattern.IsMatch(upn))
                    {
                        notUpns.Add(upn);
                        return false;
                    }

                    upn = upn.Trim().ToLower();
                    if (!users.TryGetValue(upn, out user))
                    {
                        addedUsers++;
                        user = new User(upn);
                        users.Add(upn, user);
                        newDb.Users.Add(user);
                    }

                    return true;
                }

                void SetUser(Enquiry enquiry, string upn, Action<Enquiry, User> setter)
                {
                    if (!TryGetUser(upn, out var user))
                    {
                        return;
                    }

                    setter.Invoke(enquiry, user);
                }

                //enquiries
                var referenceIds = new HashSet<string>();
                var enquiryMap = new Dictionary<string, Enquiry>();
                foreach (var enq in enquiriesGrouped)
                {
                    var enquiry = enq.Value.Last();

                    if (string.IsNullOrEmpty(enquiry.Request_Title))
                    {
                        continue;
                    }

                    if (!enquiry.LAST_VERSION ||
                        string.IsNullOrEmpty(enquiry.REFERENCE_ID))
                    {
                        noReferenceIdEnquiries.Add(enquiry.PK_Fact_Enquiry);
                        continue;
                    }

                    if (referenceIds.Contains(enquiry.REFERENCE_ID))
                    {
                        continue;
                    }

                    if (enquiry.Dim_Enquiry_Type_FK < 1 || enquiry.Dim_Enquiry_Type_FK > 17)
                    {
                        unknownTypeEnquiries.Add(enquiry.PK_Fact_Enquiry);
                        continue;
                    }

                    if (string.IsNullOrEmpty(enquiry.STATE))
                    {
                        noStateEnquiries.Add(enquiry.PK_Fact_Enquiry);
                        continue;
                    }

                    if (enquiry.REFERENCE_ID != "Pending")
                    {
                        referenceIds.Add(enquiry.REFERENCE_ID);
                    }

                    if (!enquiry.Dim_Rig_FK.HasValue
                        || !dimRigs.TryGetValue(enquiry.Dim_Rig_FK.Value, out var rigRefId)
                        || !rigs.TryGetValue(rigRefId.ToLongReferenceId(), out var rig))
                    {
                        noRigEnquiries.Add(enquiry.PK_Fact_Enquiry);
                        continue;
                    }

                    if (string.IsNullOrEmpty(enquiry.Dim_MYD_Customer_Customer)
                        || !customers.TryGetValue(enquiry.Dim_MYD_Customer_Customer, out var customer))
                    {
                        noMydCustomerEnquiries.Add(enquiry.PK_Fact_Enquiry);
                        continue;
                    }

                    //enquiries
                    var type = (TypeInfo.Type) enquiry.Dim_Enquiry_Type_FK;
                    var newEnquiry = new Enquiry();
                    newEnquiry.SetVersion(1);
                    newEnquiry.SetRig(rig);
                    newEnquiry.SetRootCustomer(customer?.RootCustomer);
                    newEnquiry.SetCustomer(customer);
                    newEnquiry.SetSupplier(mhwirthCustomer);
                    newEnquiry.SetType(type);
                    newEnquiry.SetReferenceIdObsolete(enquiry.REFERENCE_ID == "Pending" ? null : enquiry.REFERENCE_ID);
                    newEnquiry.OldGlobalId = enquiry.GLOBAL_ID;
                    newEnquiry.SetDate(enquiry.ApprovedDate.HasValue ? enquiry.ApprovedDate.Value : enquiry.CreatedDate);

                    newEnquiry.SetTitle(enquiry.Request_Title);
                    newEnquiry.SetDescription(enquiry.Request_Description);
                    newEnquiry.SetCreated(enq.Value.First().CreatedDate);
                    newEnquiry.SetModified(enq.Value.Last().INSERT_DATE);
                    newEnquiry.SetApproved(enquiry.ApprovedDate);
                    newEnquiry.SetSubmitted(enquiry.ApprovedDate, enquiry.CreatedDate);

                    newEnquiry.ChangeState((StateInfo.State) int.Parse(Path.GetFileName(enquiry.STATE)));

                    if (enquiry.Dim_Equipment_FK.HasValue
                        && dimEquipments.TryGetValue(enquiry.Dim_Equipment_FK.Value, out var eqRefId)
                        && equipments.TryGetValue(eqRefId.ToLongReferenceId(), out var eq))
                    {
                        newEnquiry.SetEquipment(eq);
                    }

                    if (enquiry.Dim_Customer_FK.HasValue
                        && dimCustomers.TryGetValue(enquiry.Dim_Customer_FK.Value, out var customerRefId)
                        && customers.TryGetValue(customerRefId, out var rigOwner))
                    {
                        newEnquiry.SetRigOwner(rigOwner);
                    }

                    SetUser(newEnquiry, enquiry.CreatedByUserId, (e, u) => e.SetCreatedBy(u));
                    SetUser(newEnquiry, enquiry.CreatedByUserId, (e, u) => e.SetModifiedBy(u));
                    SetUser(newEnquiry, enquiry.ApprovedByUserId, (e, u) => e.SetApprovedBy(u));
                    SetUser(newEnquiry, enquiry.AssignerUserId, (e, u) => e.SetAssignedBy(u));
                    SetUser(newEnquiry, enquiry.AssignedUserId, (e, u) => e.SetAssignedTo(u));
                    SetUser(newEnquiry, enquiry.RejectedByUserId, (e, u) => e.SetRejectedBy(u));
                    
                    //status logs
                    var currentState = "";
                    foreach (var enqValue in enq.Value)
                    {
                        if (currentState == enqValue.STATE)
                        {
                            continue;
                        }

                        if (string.IsNullOrEmpty(enqValue.STATE))
                        {
                            continue;
                        }

                        currentState = enqValue.STATE;
                        var state = new StateLog
                        {
                            Enquiry = newEnquiry,
                            State = (StateInfo.State) int.Parse(Path.GetFileName(currentState)),
                            Changed = enqValue.CreatedDate
                        };

                        if (TryGetUser(enqValue.CreatedByUserId, out var user))
                        {
                            state.ChangedBy = user;
                        }

                        newDb.Enquiry_StateLogs.Add(state);
                    }

                    newDb.Enquiry_Enquiries.Add(newEnquiry);
                    enquiryMap[enquiry.GLOBAL_ID] = newEnquiry;
                }

                newDb.SaveChanges();

                var commentReferenceIds = new HashSet<string>();
                var fileReferenceIds = new HashSet<string>();
                foreach (var enquiry in enquiryMap)
                {
                    //comments staff
                    var commentsMap = new Dictionary<string, Comment>();
                    if (comments.TryGetValue(enquiry.Key, out var enqComments))
                    {
                        var commentsCount = 0;
                        foreach (var enqComment in enqComments)
                        {
                            if (commentsMap.ContainsKey(enqComment.GLOBAL_ID))
                            {
                                continue;
                            }

                            if (enqComment.REFERENCE_ID != "Pending" && commentReferenceIds.Contains(enqComment.REFERENCE_ID))
                            {
                                continue;
                            }
                            commentReferenceIds.Add(enqComment.REFERENCE_ID);

                            var newComment = new Comment
                            {
                                ReferenceId = enqComment.REFERENCE_ID == "Pending" ? null : enqComment.REFERENCE_ID,
                                Title = enqComment.Title,
                                Text = enqComment.ConversationText,
                                Enquiry = enquiry.Value,
                                Created = enqComment.CreatedDate,
                                Submitted = enqComment.CreatedDate,
                                IsInitial = InitialCommentConversationTypes.Contains(enqComment.ConversationType),
                                CreatedBy = InitialCommentConversationTypes.Contains(enqComment.ConversationType) && enquiry.Value.CreatedBy != null
                                    ? enquiry.Value.CreatedBy
                                    : !string.IsNullOrEmpty(enqComment.INSERT_BY) && TryGetUser(enqComment.INSERT_BY, out var commentCreator)
                                        ? commentCreator
                                        : null
                            };

                            if (!string.IsNullOrEmpty(enqComment.CreatedByDescription)
                                && TryGetUser(enqComment.CreatedByDescription, out var user))
                            {
                                newComment.CreatedBy = user;
                            }

                            commentsCount++;
                            newDb.Enquiry_Comments.Add(newComment);
                            commentsMap.Add(enqComment.GLOBAL_ID, newComment);
                        }

                        var realComments = commentsMap.Values.Where(x => !x.IsInitial).ToArray();
                        if (realComments.Length > 0)
                        {
                            enquiry.Value.SetLastCommented(realComments.Max(x => x.Created));
                        }
                    }

                    //involved users
                    if (usersAdded.TryGetValue(enquiry.Key, out var added))
                    {
                        foreach (var addedItem in added)
                        {
                            if (!TryGetUser(addedItem, out var user))
                            {
                                continue;
                            }

                            newDb.Enquiry_AddedUsers.Add(new AddedUser
                            {
                                Enquiry = enquiry.Value,
                                User = user,
                                Created = DateTime.UnixEpoch
                            });
                        }
                    }

                    //added users
                    if (usersRead.TryGetValue(enquiry.Key, out var read))
                    {
                        foreach (var readItem in read)
                        {
                            if (!TryGetUser(readItem, out var user))
                            {
                                continue;
                            }

                            newDb.Enquiry_ReadReceipts.Add(new ReadReceipt
                            {
                                Enquiry = enquiry.Value,
                                User = user,
                                Created = DateTime.UnixEpoch
                            });
                        }
                    }

                    //attachments
                    if (files.TryGetValue(enquiry.Key, out var enqFiles))
                    {
                        foreach (var enqFile in enqFiles)
                        {
                            if (enqFile.REFERENCE_ID != "Pending" &&  fileReferenceIds.Contains(enqFile.REFERENCE_ID))
                            {
                                continue;
                            }

                            var path = $"{Path.GetFileName(enqFile.GLOBAL_ID)}\\{enqFile.Filename}";

                            if (enqFile.REFERENCE_ID == "Pending")
                            {
                                var blobClient = blobContainerClient.GetBlobClient(path);
                                if (!blobClient.Exists())
                                {
                                    byte[] binary;
                                    using (var conn = new SqlConnection(oldServerConnectionString.GetStage1ConnectionString()))
                                    {
                                        var base64 = conn.QueryFirstOrDefault<string>($"select Binary_Data from SIB_Attachment_Cache where global_id='{enqFile.GLOBAL_ID}'");
                                        if (string.IsNullOrEmpty(base64))
                                        {
                                            continue;
                                        }

                                        binary = Convert.FromBase64String(base64);
                                    }

                                    using (var stream = new MemoryStream(binary))
                                    {
                                        blobClient.Upload(stream);
                                    }
                                }
                            }

                            var file = new Attachment
                            {
                                ReferenceId = enqFile.REFERENCE_ID == "Pending" ? null : enqFile.REFERENCE_ID,
                                FileName = enqFile.Filename,
                                FileExtension = enqFile.Extension?.Trim().Trim(new[] {'.'}).Trim().ToLower(),
                                Enquiry = enquiry.Value,
                                Created = enqFile.INSERT_DATE,
                                BlobPath = path,
                                CreatedBy = !string.IsNullOrEmpty(enqFile.INSERT_BY) && TryGetUser(enqFile.INSERT_BY, out var fileCreator)
                                    ? fileCreator
                                    : null
                            };

                            newDb.Enquiry_Attachments.Add(file);

                            if (links.TryGetValue(enqFile.GLOBAL_ID, out var comment) && commentsMap.TryGetValue(comment.CommentGlobalId, out var newComment))
                            {
                                file.Comment = newComment;
                                file.CreatedBy = newComment.CreatedBy;
                                continue;
                            }

                            fileReferenceIds.Add(enqFile.REFERENCE_ID);
                        }
                    }
                }

                newDb.SaveChanges();
            }

            Log.Logger.Warning(nameof(noReferenceIdEnquiries));
            Log.Logger.Warning(string.Join(',', noReferenceIdEnquiries));
            Log.Logger.Warning(nameof(noRigEnquiries));
            Log.Logger.Warning(string.Join(',', noRigEnquiries));
            Log.Logger.Warning(nameof(noMydCustomerEnquiries));
            Log.Logger.Warning(string.Join(',', noMydCustomerEnquiries));
            Log.Logger.Warning(nameof(unknownTypeEnquiries));
            Log.Logger.Warning(string.Join(',', unknownTypeEnquiries));
            Log.Logger.Warning(nameof(noStateEnquiries));
            Log.Logger.Warning(string.Join(',', noStateEnquiries));
            Log.Logger.Warning(nameof(notUpns));
            Log.Logger.Warning(string.Join(',', notUpns.Select(x => $"'{x}'")));
            Log.Logger.Information($"{nameof(addedUsers)}={addedUsers}");

            Log.Logger.Information("Imported enquiries");
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class FACT_Enquiry
        {
            public int PK_Fact_Enquiry;
            public int? Dim_Rig_FK;
            public int? Dim_Equipment_FK;
            public int Dim_Enquiry_Type_FK;
            public int? Dim_Customer_FK;
            public string Dim_MYD_Customer_Customer;
            public string Dim_MDR_Equipment_FK;
            public string GLOBAL_ID;
            public string REFERENCE_ID;
            public string Request_Title;
            public string Request_Description;
            public string ResponsibleUserName;
            public string CreatedByUserId;
            public DateTime CreatedDate;
            public string RequestState;
            public string RequestStateDate;
            public string AssignedToConsolidated;
            public string ApprovedByUserId;
            public DateTime? ApprovedDate;
            public string AssignedUserId;
            public string AssignerUserId;
            public string RejectedByUserId;
            public string Domain;
            public DateTime? AssignDate;
            public DateTime? ClosedDate;
            public string Priority;
            public string ResponsibleDepartmentId;
            public DateTime LastUpdatedInternallyDateTime;
            public string STATE;
            public string SOURCE_ID;
            public DateTime INSERT_DATE;
            public string INSERT_BY;
            public bool LAST_VERSION;
            public string CONTEXT;
            public string QUALITY;
            public string VERSION;

            public string RigRef;
            public string EquipmentRef;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class DIM_Enquiry_Type
        {
            public int PK_Enquiry_Type;
            public string SIB_Id;
            public int Enquiry_Type_Id;
            public string Enquiry_Type_Name;
            public int Enquiry_Sub_Type_Id;
            public string Enquiry_Sub_Type_Name;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class FACT_Enquiry_Conversation
        {
            public int PK_FACT_Enquiry_Conversation;
            public int Dim_Rig_FK;
            public int Dim_Customer_FK;
            public int Dim_Enquiry_Type_FK;
            public string GLOBAL_ID;
            public string PARENT_GLOBAL_ID;
            public string REFERENCE_ID;
            public string Title;
            public string ConversationText;
            public string ConversationType;
            public DateTime CreatedDate;
            public string CreatedByDescription;
            public int ConversationOrigin;
            public string Enquiry_STATE;
            public string STATE;
            public string SOURCE_ID;
            public DateTime INSERT_DATE;
            public string INSERT_BY;
            public bool LAST_VERSION;
            public string CONTEXT;
            public string QUALITY;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class FACT_Enquiry_Document
        {
            public int PK_FACT_Enquiry_Document;
            public int Dim_Rig_FK;
            public int Dim_Customer_FK;
            public int Dim_Enquiry_Type_FK;
            public string GLOBAL_ID;
            public string PARENT_GLOBAL_ID;
            public string REFERENCE_ID;
            public string Filename;
            public string Title;
            public string Extension;
            public string MimeType;
            public int FileSize;
            public string DocumentName;
            public string DocumentDescription;
            public DateTime DocumentCreatedTime;
            public string DocumentCreatedBy;
            public string Enquiry_STATE;
            public string STATE;
            public string SOURCE_ID;
            public string ORIGINATING_SOURCE_ID;
            public DateTime INSERT_DATE;
            public string INSERT_BY;
            public bool LAST_VERSION;
            public string CONTEXT;
            public string QUALITY;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class UserAddedToEnquiries
        {
            public int Id;
            public string EnquiryId;
            public string UserUPN;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class UserReadedEnquiries
        {
            public int Id;
            public string EnquiryId;
            public string UserUPN;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class CommentDocumentLink
        {
            public string CommentGlobalId;
            public string DocumentGlobalId;
            public long DocumentCommentLinkRowId;
        }
    }
}
