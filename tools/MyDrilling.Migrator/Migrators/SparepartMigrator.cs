﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Dapper;
using EFCore.BulkExtensions;
using Microsoft.Data.SqlClient;
using MyDrilling.Core.Entities.Spareparts;
using Serilog;

namespace MyDrilling.Migrator.Migrators
{
    public static class SparepartMigrator
    {
        public static void Do(string oldServerConnectionString, string newServerConnectionString, bool insertAll)
        {
            Log.Logger.Information("Importing spareparts...");

       
            FactSparepart[] factSpareparts;
            string[] sparepartsLookup;

            using (var conn = new SqlConnection(oldServerConnectionString.GetDwhConnectionString()))
            {

                factSpareparts = conn.Query<FactSparepart>("select * from Fact_GlobalSparePartList").ToArray();
                sparepartsLookup = conn.Query<string>(@"SELECT distinct [GLOBAL_ID]
                              FROM [dbo].[GlobalSparePartLookup]").ToArray();
            }

            factSpareparts = factSpareparts.GroupBy(x => x.REFERENCE_ID).Select(x => x.First()).ToArray();

            using (var newDb = newServerConnectionString.GetNewMyDrillingDbContext())
            {
                var sparepartsWhitelist = new List<Whitelist>();
                var existingSparepartsWhitelisted = newDb.Sparepart_WhiteList.Select(x => x.GlobalId).ToArray();
                foreach (var sp in sparepartsLookup)
                {
                    if (!existingSparepartsWhitelisted.Contains(sp))
                        sparepartsWhitelist.Add(new Whitelist { GlobalId = sp });
                }

                newDb.BulkInsert(sparepartsWhitelist);

                if (insertAll)
                {
                    var spareparts = new List<Sparepart>();
                    foreach (var oldPart in factSpareparts)
                    {
                        var newPart = new Sparepart
                        {
                            ReferenceId = oldPart.REFERENCE_ID,
                            Description = oldPart.Description,
                            MaterialNo = oldPart.Material_no,
                            Year = short.TryParse(oldPart.Year, out var year) ? year : (short)0
                        };

                        spareparts.Add(newPart);
                    }
                    newDb.BulkInsert(spareparts);
                }
                newDb.SaveChanges();
            }

            Log.Logger.Information("Imported spareparts");
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class FactSparepart
        {
            public string REFERENCE_ID;
            public string Material_no;
            public string Description;
            public string Year;
            public int Reference;
        }
    }
}