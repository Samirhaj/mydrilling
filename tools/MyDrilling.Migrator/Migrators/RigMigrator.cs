﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Dapper;
using Microsoft.Data.SqlClient;
using MyDrilling.Core;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Rig;
using Serilog;

namespace MyDrilling.Migrator.Migrators
{
    public static class RigMigrator
    {
        public static void Do(string oldServerConnectionString, string newServerConnectionString, bool insert)
        {
            Log.Logger.Information("Importing rigs...");

            DimRig[] dimRigs;
            CustomerMigrator.DimMydCustomer[] dimMydCustomers;

            using (var conn = new SqlConnection(oldServerConnectionString.GetDwhConnectionString()))
            {
                dimRigs = conn.Query<DimRig>("select * from dim_rig").ToArray();
                dimMydCustomers = conn.Query<CustomerMigrator.DimMydCustomer>("select * from dim_myd_customer where last_version = 1").ToArray();
            }

            FacilityCustomerAccess[] customerAccesses;
            Dictionary<string, CustomerResponsible[]> responsibleUsers;

            using (var conn = new SqlConnection(oldServerConnectionString.GetPortalConnectionString()))
            {
                customerAccesses = conn.Query<FacilityCustomerAccess>("select * from FacilityCustomerAccess").ToArray();
                responsibleUsers = conn.Query<CustomerResponsible>("select cr.*, u.UPN, u.Email, u.FirstName, u.LastName from CustomerResponsible cr join myDrillingUser u on cr.UserId = u.myDrillingUserId")
                    .GroupBy(x => x.RigId).ToDictionary(x => x.Key, x => x.OrderBy(y => y.CustomerTCEId).ToArray());
            }

            using (var newDb = newServerConnectionString.GetNewMyDrillingDbContext())
            {
                var users = newDb.Users.ToArray().ToDictionary(x => x.Upn, x => x);
                var customers = newDb.Customers.Where(x => !string.IsNullOrEmpty(x.ReferenceId)).ToDictionary(x => x.ReferenceId, x => x.Id);

                Dictionary<long, Rig> existingRigs = null;
                if (!insert)
                {
                    existingRigs = newDb.Rigs.ToDictionary(x => x.ReferenceId, x => x);
                }

                //rigs
                foreach (var dimRig in dimRigs)
                {
                    var newRig = insert
                        ? new Rig
                            {
                                ReferenceId = dimRig.REFERENCE_ID.ToLongReferenceId(),
                                Name = dimRig.Rig_Name.ToCamelCase(true),
                                Sector = dimRig.Rig_Sector,
                                Region = dimRig.Rig_Region,
                                TypeCode = dimRig.Rig_Type_Code,
                                Created = dimRig.insert_date,
                                Modified = dimRig.update_date ?? dimRig.insert_date
                            }
                        : existingRigs.TryGetValue(dimRig.GLOBAL_ID.ToLongReferenceId(), out _)
                            ? existingRigs[dimRig.GLOBAL_ID.ToLongReferenceId()]
                            : null;

                    if (newRig == null)
                    {
                        Log.Logger.Error($"rig {dimRig.GLOBAL_ID} not found");
                        continue;
                    }

                    newRig.Coordinates = new Coordinates
                    {
                        Longitude = dimRig.Rig_Longitude,
                        Latitude = dimRig.Rig_Latitude
                    };

                    if (responsibleUsers.TryGetValue(dimRig.REFERENCE_ID, out var responsibleForRig))
                    {
                        var responsible = responsibleForRig.Last();
                        if (!string.IsNullOrEmpty(responsible.Upn) && users.TryGetValue(responsible.Upn.ToLower().Trim(), out var user))
                        {
                            newRig.ResponsibleUser = user;
                        }
                        else
                        {
                            var upn = users.Keys.FirstOrDefault(x => x.MdStartsWith(responsible.GetEmailPattern()));
                            if(!string.IsNullOrEmpty(upn))
                            {
                                newRig.ResponsibleUser = users[upn];
                            }
                            else
                            {
                                Log.Logger.Warning($"can't find new user for old user {responsible.UserId}'");
                            }
                        }
                    }

                    //Rig_Customer_Sold_To_Party
                    if (insert && !string.IsNullOrEmpty(dimRig.Rig_Customer_Sold_To_Party))
                    {
                        if (!customers.TryGetValue(dimRig.Rig_Customer_Sold_To_Party, out var newCustomerId))
                        {
                            Log.Logger.Warning($"no customer with ReferenceId {dimRig.Rig_Customer_Sold_To_Party} for rig {dimRig.PK_Dim_Rig}");
                        }
                        else
                        {
                            newRig.OwnerId = newCustomerId;
                        }
                    }

                    //FacilityCustomerAccess
                    var accesses = customerAccesses.Where(x => x.FacilityGlobalId == dimRig.REFERENCE_ID).ToArray();
                    if (accesses.Length > 0)
                    {
                        foreach (var access in accesses)
                        {
                            var mydCustomer = dimMydCustomers.SingleOrDefault(x => x.GLOBAL_ID == access.CustomerGlobalId);
                            if (mydCustomer == null)
                            {
                                Log.Logger.Warning($"no mydcustomer with ReferenceId {access.CustomerGlobalId} in FacilityCustomerAccess {access.Id}");
                                continue;
                            }

                            var newCustomer = newDb.RootCustomers.Single(x => x.Name == mydCustomer.MYD_Customer_Name);

                            if (access.RoleId == 1)
                            {
                                newRig.RootOperator = newCustomer;
                            }
                            else
                            {
                                newRig.RootOwner = newCustomer;
                            }
                        }
                    }

                    if (insert)
                    {
                        newDb.Rigs.Add(newRig);
                    }
                }

                newDb.SaveChanges();
            }

            Log.Logger.Information("Imported rigs");
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class DimRig
        {
            public int PK_Dim_Rig;
            public string GLOBAL_ID;
            public string PARENT_GLOBAL_ID;
            public string REFERENCE_ID;
            public string Rig_Id;
            public string Rig_Name;
            public string Rig_Owner;
            public string Rig_Type_Code;
            public string Rig_Type;
            public string Rig_Client;
            public string Rig_Sector;
            public string Rig_Region;
            public double? Rig_Latitude;
            public double? Rig_Longitude;
            public string Rig_Customer_Sold_To_Party;
            public string SOURCE_ID;
            public DateTime? valid_from_date;
            public DateTime? valid_to_date;
            public DateTime insert_date;
            public DateTime? update_date;
        }

        public class FacilityCustomerAccess
        {
            public long Id;
            public string FacilityGlobalId;
            public string CustomerGlobalId;
            public int RoleId;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class CustomerResponsible
        {
            public long CustomerTCEId;
            public string RigId;
            public string Upn;
            public string Email;
            public long UserId;
            public string FirstName;
            public string LastName;
            public string GetEmailPattern() => CommonStringExt.GetEmailPattern(FirstName, LastName);
        }
    }
}
