﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MyDrilling.Infrastructure.Data;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using MyDrilling.Core.Entities.UpgradeMatrix;
using Serilog;
using SpreadSheetRow = DocumentFormat.OpenXml.Spreadsheet.Row;
using SpreadSheetCell = DocumentFormat.OpenXml.Spreadsheet.Cell;
using Azure.Storage.Blobs;
using EFCore.BulkExtensions;
using MyDrilling.Core;
using MyDrilling.Infrastructure.Storage;

namespace MyDrilling.Migrator.Migrators
{
    public static class UpgradeMatrixMigrator
    {
        private static readonly Regex RegexPatternForCustomerId = new Regex(@"\d{10}");
        private static BlobContainerClient _containerClient;
        public static void Do(string upgradeMatrixPath, string newServerConnectionString, string blobConnectionString)
        {
            using (var newDb = newServerConnectionString.GetNewMyDrillingDbContext())
            {
                if (newDb.UpgradeMatrix_CustomerSettings.Any() || newDb.UpgradeMatrix_RigSettings.Any())
                {
                    Log.Logger.Information("UpgradeMatrix has been already imported.");
                    return;
                }
            }

            BlobServiceClient blobServiceClient = new BlobServiceClient(blobConnectionString);
            // Create the container and return a container client object
            BlobContainerClient containerClient = blobServiceClient.GetBlobContainerClient(StorageConfig.BlobCcnContainerName);
            _containerClient = containerClient;
           
            Log.Logger.Information("Importing Software matrix data from excel...");
            try
            {
                if (string.IsNullOrWhiteSpace(upgradeMatrixPath))
                {
                    Log.Logger.Error("Configuration does not contain file path.");
                    return;
                }
                if (!Directory.Exists(upgradeMatrixPath))
                {
                    Log.Logger.Error("There are no defined file path");
                    return;
                }
                var customerFiles = GetExistingSotwareMatrixFileNames(upgradeMatrixPath);
                if (customerFiles.Count == 0)
                {
                    Log.Logger.Error("There are no excel files to import from");
                    return;
                }
                var customerMatrixDetails = new List<CustomerMatrixInfo>();
                //get customers
                foreach (var customerFile in customerFiles)
                {
                    //get software matrix details by customer
                    try
                    {
                        var customerId = GetCustomerIdListFromFileName(customerFile).FirstOrDefault();
                        if (customerId == null)
                        {
                            //report error - missing customer
                            Log.Logger.Error($"Missing Customer in file name - {customerFile}");
                            continue;
                        }
                        var customerMatrixInfo = new CustomerMatrixInfo { CustomerReferenceId = customerId};
                        var excelData = new ExcelWorksheet(3, upgradeMatrixPath + "\\" + customerFile);
                       
                        customerMatrixInfo.CcnContent = new CcnParser(excelData).GetContent(); 
                        customerMatrixInfo.QuotationsContent = new QuotationsParser(excelData).GetContent(); 
                        customerMatrixDetails.Add(customerMatrixInfo);
                    }
                    catch (Exception ex)
                    {
                        Log.Logger.Error(ex, $"Customer file '{customerFile}' failed on importing.");
                    }
                }
                Log.Logger.Information("Inserting data to db...");

                using (var newDb = newServerConnectionString.GetNewMyDrillingDbContext())
                {
                    var customerSettings = new List<CustomerSettings>();
                    var rigSettings = new List<RigSettings>();
                    var equipmentCategories = new List<EquipmentCategory>();
                    foreach (var customerMatrixDetail in customerMatrixDetails)
                    {
                        var customerDb = newDb.Customers.FirstOrDefault(c => c.ReferenceId == customerMatrixDetail.CustomerReferenceId);
                        if (customerDb?.RootCustomerId == null)
                        {
                            Log.Logger.Warning($"Customer {customerMatrixDetail.CustomerReferenceId} doesn't exist in database.");
                            continue;
                        }
                        //save customer settings
                        var customerSettingsDb = customerSettings.FirstOrDefault(cs => cs.RootCustomerId == customerDb.Id);
                        if (customerSettingsDb == null && customerDb.RootCustomerId.HasValue)
                        {
                            customerSettings.Add(new CustomerSettings
                            {
                                RootCustomerId = (long) customerDb.RootCustomerId,
                                Key = "Status",
                                Value = UpgradeMatrixCustomerStatus.Active.ToString()
                            });
                        }
                        //collect all rigs and equipment
                        if (customerMatrixDetail.CcnContent != null)
                        {
                            SaveRigSettings(customerMatrixDetail.CcnContent.Rigs, rigSettings, newDb);
                            SaveEquipmentCategory(customerMatrixDetail.CcnContent.Rows.Select(r => r.Equipment).ToArray(), equipmentCategories);
                        }

                        if (customerMatrixDetail.QuotationsContent != null)
                        {
                            SaveRigSettings(customerMatrixDetail.QuotationsContent.Rigs, rigSettings, newDb);
                            SaveEquipmentCategory(customerMatrixDetail.QuotationsContent.Rows.Select(r => r.Equipment).ToArray(), equipmentCategories);
                        }
                    }
                    newDb.BulkInsert(customerSettings);
                    newDb.BulkInsert(rigSettings);
                    newDb.BulkInsert(equipmentCategories);

                    var newRigSettings = newDb.UpgradeMatrix_RigSettings;
                    var newEquipmentCategories = newDb.UpgradeMatrix_EquipmentCategories;

                    foreach (var customerMatrixDetail in customerMatrixDetails)
                    {
                        var customerDb = newDb.Customers.FirstOrDefault(c => c.ReferenceId == customerMatrixDetail.CustomerReferenceId);
                        if (customerDb?.RootCustomerId == null)
                        {
                           continue;
                        }

                        if (customerMatrixDetail.CcnContent != null)
                        {
                            var ccnContent = customerMatrixDetail.CcnContent;
                            var ccnItems = new List<CcnItem>();
                            foreach (var ccnRow in ccnContent.Rows)
                            {
                                if (string.IsNullOrEmpty(ccnRow.Equipment)) continue;
                                var equipmentCategory = newEquipmentCategories.FirstOrDefault(e => e.ShortName == ccnRow.Equipment);
                                if(equipmentCategory == null) continue;
                                
                                //insert ccnItem
                                var ccnRowDb = new CcnItem
                                {
                                    RootCustomerId = (long)customerDb.RootCustomerId,
                                    EquipmentCategoryId = equipmentCategory.Id,
                                    Comments = ccnRow.Comments,
                                    Description = ccnRow.Description,
                                    RigType = GetRigTypeForString(ccnRow.RigType),
                                    PackageUpgrade = GetPackageUpgradeForString(ccnRow.PackageUpgrade)
                                };
                                ccnItems.Add(ccnRowDb);
                            }
                            newDb.BulkInsert(ccnItems);
                            var ccnStates = new List<CcnState>();
                            foreach (var ccnRow in ccnContent.Rows)
                            {
                                var ccnRowRigType = GetRigTypeForString(ccnRow.RigType);
                                if (ccnRow.RigStates.Length == 0 || string.IsNullOrEmpty(ccnRow.Equipment)) continue;
                                var equipmentCategory = newEquipmentCategories.FirstOrDefault(e => e.ShortName == ccnRow.Equipment);
                                if (equipmentCategory == null) continue;
                                var ccnRowDb = newDb.UpgradeMatrix_CcnItems.FirstOrDefault(ci =>
                                    ci.RootCustomerId == customerDb.RootCustomerId &&
                                    ci.EquipmentCategoryId == equipmentCategory.Id &&
                                    ci.Comments == ccnRow.Comments &&
                                    ci.Description == ccnRow.Description &&
                                    ci.RigType == ccnRowRigType &&
                                    ci.PackageUpgrade == GetPackageUpgradeForString(ccnRow.PackageUpgrade)
                                );
                                if (ccnRowDb == null) continue;
                                //insert item states
                                foreach (var rigState in ccnRow.RigStates)
                                {
                                    var rig = newRigSettings.FirstOrDefault(r => r.ShortName == rigState.Rig);
                                    if (rig == null || (ccnRowRigType != UpgradeMatrixRigType.Undefined && rig.RigType != ccnRowRigType)) continue;
                                    var rigStateDb = ccnStates.FirstOrDefault(rs => rs.CcnItemId == ccnRowDb.Id && rs.RigSettingsId == rig.Id);
                                    if (rigStateDb != null) continue;
                                    var ccnRigState = Enum.TryParse(((int)rigState.ImplementedState).ToString(), out UpgradeMatrixItemStatus myState)
                                        ? myState
                                        : UpgradeMatrixItemStatus.Undefined;
                                    if (ccnRow.Status == "Implemented" && ccnRigState == UpgradeMatrixItemStatus.Undefined)
                                        ccnRigState = UpgradeMatrixItemStatus.Na;
                                    var newRigState = new CcnState
                                    {
                                        CcnItemId = ccnRowDb.Id,
                                        RigSettingsId = rig.Id,
                                        Status = ccnRigState,
                                        CcnName = !string.IsNullOrEmpty(rigState.CcnDocumentName) ? rigState.CcnDocumentName : null
                                    };

                                    //upload CCN document, if any
                                    if (!string.IsNullOrEmpty(rigState.CcnDocument) && !string.IsNullOrEmpty(rigState.CcnDocumentName))
                                    {
                                        var docBlobName = $"{rig.Rig.ReferenceId}\\{rigState.CcnDocumentName}.pdf";
                                        Task<bool> uploadTask = UploadFileToBlobContainerAsync(docBlobName, rigState.CcnDocument);
                                        uploadTask.Wait();
                                        if (uploadTask.Result)
                                        {
                                            newRigState.DocumentPath = docBlobName.Replace("\\", "/");
                                        }
                                    }
                                    ccnStates.Add(newRigState);
                                }
                            }
                            newDb.BulkInsert(ccnStates);
                        }

                        if (customerMatrixDetail.QuotationsContent == null) continue;
                        var quotationContent = customerMatrixDetail.QuotationsContent;
                        var quotationItems = new List<QuotationItem>();

                        foreach (var quotationRow in quotationContent.Rows)
                        {
                            if (string.IsNullOrEmpty(quotationRow.Equipment)) continue;

                            var equipmentCategory = newEquipmentCategories.FirstOrDefault(e => e.ShortName == quotationRow.Equipment);
                            if (equipmentCategory == null) continue;

                            //insert quotationItem
                            var quotationRowDb = new QuotationItem
                            {
                                RootCustomerId = (long)customerDb.RootCustomerId,
                                EquipmentCategoryId = equipmentCategory.Id,
                                Description = quotationRow.Description,
                                Benefits = quotationRow.Benefits,
                                Heading = quotationRow.Heading
                            };
                            quotationItems.Add(quotationRowDb);
                        }
                        newDb.BulkInsert(quotationItems);
                        var quotationStates = new List<QuotationState>();
                        foreach (var quotationRow in quotationContent.Rows)
                        {
                            if (quotationRow.RigStates.Length == 0 || string.IsNullOrEmpty(quotationRow.Equipment)) continue;

                            var equipmentCategory = newEquipmentCategories.FirstOrDefault(e => e.ShortName == quotationRow.Equipment);
                            if (equipmentCategory == null) continue;
                            var quotationRowDb = newDb.UpgradeMatrix_QuotationItems.FirstOrDefault(qi =>
                                qi.RootCustomerId == customerDb.RootCustomerId &&
                                qi.EquipmentCategoryId == equipmentCategory.Id &&
                                qi.Description == quotationRow.Description &&
                                qi.Benefits == quotationRow.Benefits &&
                                qi.Heading == quotationRow.Heading
                            );
                            if (quotationRowDb == null) continue;
                            //insert item states
                            foreach (var rigState in quotationRow.RigStates)
                            {
                                var rig = newRigSettings.FirstOrDefault(r => r.ShortName == rigState.Rig);
                                if (rig == null) continue;
                                var rigStateDb = quotationStates.FirstOrDefault(rs => rs.QuotationItemId == quotationRowDb.Id && rs.RigSettingsId == rig.Id);
                                if (rigStateDb != null) continue;
                                var quotationRigState = Enum.TryParse(((int)rigState.ImplementedState).ToString(), out UpgradeMatrixItemStatus myState)
                                    ? myState
                                    : UpgradeMatrixItemStatus.Undefined;
                                var newRigState = new QuotationState
                                {
                                    QuotationItemId = quotationRowDb.Id,
                                    RigSettingsId = rig.Id,
                                    Status = quotationRigState
                                };
                                quotationStates.Add(newRigState);
                            }
                        }
                        newDb.BulkInsert(quotationStates);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, $"Software matrix failed on importing.");
            }
            Log.Logger.Information("Imported Software matrix...");
        }

        public static async Task<bool> UploadFileToBlobContainerAsync(string fileName, string filePath)
        {
            if (string.IsNullOrEmpty(filePath)) return false;

            // Get a reference to a blob
            var blobClient = _containerClient.GetBlobClient(fileName);
            if (blobClient.Exists())
            {
                return true;
            }
            await using FileStream uploadFileStream = File.OpenRead(filePath);
            var response = await blobClient.UploadAsync(uploadFileStream);
            return response.GetRawResponse().Status == (int) HttpStatusCode.Created;
        }

        private static void SaveRigSettings(UpgradeMatrixRig[] rigs, List<RigSettings> rigSettings, MyDrillingDb newDb)
        {
            foreach (var ccnRig in rigs)
            {
                var rigDb = newDb.Rigs.FirstOrDefault(r => r.ReferenceId == ccnRig.ReferenceId.ToLongReferenceId());
                if (rigDb == null)
                {
                    Log.Logger.Warning($"Rig {ccnRig.ReferenceId} doesn't exist in database.");
                }
                else
                {
                    var rigSettingsDb = rigSettings.FirstOrDefault(cs => cs.RigId == rigDb.Id);
                    if (rigSettingsDb == null)
                    {
                        rigSettings.Add(new RigSettings
                        {
                            RigId = rigDb.Id,
                            ShortName = ccnRig.ShortName,
                            RigType = GetRigTypeForString(ccnRig.RigType)
                        });
                    }
                }
            }
        }

        private static void SaveEquipmentCategory(string[] equipmentList, List<EquipmentCategory> equipmentCategories)
        {
            foreach (var equipment in equipmentList)
            {
                if (string.IsNullOrEmpty(equipment)) continue;
                var equipmentCategory = equipmentCategories.FirstOrDefault(ec => ec.ShortName == equipment);
                if (equipmentCategory == null)
                {
                    equipmentCategory = new EquipmentCategory { ShortName = equipment };
                    equipmentCategories.Add(equipmentCategory);
                }
            }
        }

        #region GetCustomers
        private static List<string> GetCustomerIdListFromFileName(string fileName)
        {
            var matchCollection = RegexPatternForCustomerId.Matches(fileName);
            return (from object m in matchCollection select m.ToString()).ToList();
        }
        
        private static List<string> GetExistingSotwareMatrixFileNames(string filePath, Regex regexFilenamePattern)
        {
            var fileNames = Directory.GetFiles(filePath, "*.xlsx").Select(fileName => new FileInfo(fileName).Name).ToList();
            fileNames.Sort((s1, s2) => string.Compare(s1, s2, StringComparison.InvariantCultureIgnoreCase));
            const string tempExcelFilePrefix = @"~$";
            var swMatrixFileNames = fileNames.Where(fname => !fname.Contains(tempExcelFilePrefix) && regexFilenamePattern.IsMatch(fname)).ToList();
            return swMatrixFileNames;
        }

        private static List<string> GetExistingSotwareMatrixFileNames(string filePath)
        {
            return GetExistingSotwareMatrixFileNames(filePath, RegexPatternForCustomerId);
        }
        #endregion

        private static UpgradeMatrixRigType GetRigTypeForString(string rigType)
        {
            if (string.IsNullOrEmpty(rigType)) return UpgradeMatrixRigType.Undefined;
            try
            {
                return EnumExtensions.GetValueFromDescription<UpgradeMatrixRigType>(rigType);
            }
            catch (Exception)
            {
                return UpgradeMatrixRigType.Undefined;
            }
        }

        private static UpgradeMatrixPackageUpgrade GetPackageUpgradeForString(string packageUpgrade)
        {
            if (string.IsNullOrEmpty(packageUpgrade)) return UpgradeMatrixPackageUpgrade.Undefined;
            try
            {
                return EnumExtensions.GetValueFromDescription<UpgradeMatrixPackageUpgrade>(packageUpgrade);
            }
            catch (Exception)
            {
                return UpgradeMatrixPackageUpgrade.Undefined;
            }
        }
    }

    public class CustomerMatrixInfo
    {
        public string CustomerReferenceId { get; set; }
        public CcnContent CcnContent { get; set; }
        public QuotationsContent QuotationsContent { get; set; }
    }

    public class CcnContent
    {
        public UpgradeMatrixRig[] Rigs { get; set; }
        public Row[] Rows { get; set; }
        public class Row
        {
            public string Equipment { get; set; }
            public string Description { get; set; }
            public string Comments { get; set; }
            public RigState[] RigStates { get; set; }
            public string PackageUpgrade { get; set; }
            public string RigType { get; set; }
            public string Status { get; set; }

        }
    }

    public class QuotationsContent
    {
        public UpgradeMatrixRig[] Rigs { get; set; }
        public Row[] Rows { get; set; }
        public class Row
        {
            public string Equipment { get; set; }
            public string Heading { get; set; }
            public string Description { get; set; }
            public string Quotation { get; set; }
            public string Benefits { get; set; }
            public RigState[] RigStates { get; set; }
        }
    }

    public class RigState
    {
        public RigState()
        {
            ImplementedState = SoftwareMatrixRigImplementedState.Undefined;
        }

        public SoftwareMatrixRigImplementedState ImplementedState { get; set; }
        public string Rig { get; set; }
        public string CcnDocumentName { get; set; }
        public string CcnDocument { get; set; }
    }

    public class UpgradeMatrixRig
    {
        public string ReferenceId { get; set; }
        public string ShortName { get; set; }
        public string RigType { get; set; }
    }

    public enum SoftwareMatrixRigImplementedState
    {
        [StringValue("Undefined")]
        Undefined = 1,
        [StringValue("NA")]
        Na,
        [StringValue("Not Ready|Ready")]
        NotReady,
        [StringValue("Ready For Installation|Approved")]
        ReadyForInstallation,
        [StringValue("Completed")]
        Completed
    }

    public class StringValueAttribute : System.Attribute
    {
        public StringValueAttribute(string value)
        {
            Value = value;
        }
        public string Value { get; }
    }

    public class EnumHelper
    {
        public static string GetStringValue<T>(T value)
        {
            var type = value.GetType();

            var fi = type.GetField(value.ToString());
            if (!(fi.GetCustomAttributes(typeof(StringValueAttribute), false) is StringValueAttribute[] attrs))
                return value.ToString();

            var attr = attrs.FirstOrDefault();
            return attr == null ? value.ToString() : attr.Value;
        }

    }
    public interface ICell
    {
        string Value { get; }
        string Column { get; }
        int Row { get; }
        string Comment { get; }
    }
    public interface IRow
    {
        IEnumerable<ICell> Cells { get; }
    }
    public interface IDataTable : IDisposable
    {
        string FilePath { get; }
        IEnumerable<IRow> GetRows(int sheetIndex);
        string GetCellMetadata(ICell cell, int sheetIndex = 0);
    }
    public class ExcelWorksheet : IDataTable
    {
        private class Cell : ICell
        {
            private const string CellRefColumnGroupName = "column";
            private const string CellRefRowGroupName = "row";
            private static readonly Regex CellParserRegex = new Regex(@"(?<column>[^\d]+)(?<row>[\d]+)");

            internal Cell(SpreadSheetCell cell, SharedStringTablePart sharedStringTable, Dictionary<string, Comment> comments)
            {
                UnderlyingCell = cell;
                Value = GetCellValue(cell, sharedStringTable);
                var cellRef = UnderlyingCell.CellReference.Value;
                Column = GetTextByGroup(CellParserRegex, CellRefColumnGroupName, cellRef);

                int row;
                if (!int.TryParse(GetTextByGroup(CellParserRegex, CellRefRowGroupName, cellRef), out row))
                {
                    throw new InvalidDataException(string.Format("Invalid cell reference in Excel document {0}.", cellRef));
                }
                Row = row;

                Comment comment;
                if (comments.TryGetValue(cellRef, out comment))
                {
                    Comment = comment.InnerText;
                }
            }

            private string GetTextByGroup(Regex regex, string groupName, string format, params object[] args)
            {
                var matchCollection = regex.Matches(string.Format(format, args));
                return matchCollection.Count > 0 && matchCollection[0].Groups[groupName].Success
                    ? matchCollection[0].Groups[groupName].Value
                    : string.Empty;
            }

            public string Value { get; private set; }
            public string Column { get; private set; }
            public string Comment { get; private set; }
            // ReSharper disable once MemberHidesStaticFromOuterClass
            public int Row { get; private set; }

            public SpreadSheetCell UnderlyingCell { get; private set; }

            private static string GetCellValue(SpreadSheetCell cell, SharedStringTablePart sharedStringTable)
            {
                if (cell.CellValue == null)
                    return null;

                if (IsSharedStringValue(cell))
                {
                    var cellValue = sharedStringTable.SharedStringTable.ElementAt(Int32.Parse(cell.CellValue.Text));
                    return cellValue.InnerText;
                }

                return cell.CellValue.Text;
            }

            private static bool IsSharedStringValue(SpreadSheetCell cell)
            {
                return cell.DataType != null && cell.DataType == CellValues.SharedString;
            }
        }

        private class Row : IRow
        {
            private readonly SpreadSheetRow row;
            private readonly SharedStringTablePart sharedStringTable;
            private readonly Dictionary<string, Comment> cellComments;

            internal Row(SpreadSheetRow row, SharedStringTablePart sharedStringTable, Dictionary<string, Comment> cellComments)
            {
                this.row = row;
                this.sharedStringTable = sharedStringTable;
                this.cellComments = cellComments;
            }

            public IEnumerable<ICell> Cells
            {
                get
                {
                    var descendants = row.Descendants<SpreadSheetCell>();
                    return descendants.Select(c => new Cell(c, sharedStringTable, cellComments));
                }
            }
        }

        private readonly Stream stream;
        private readonly SpreadsheetDocument doc;
        private readonly List<SheetData> sheetData;
        private readonly List<WorksheetPart> sheetParts;
        private readonly SharedStringTablePart sharedStringTable;
        private readonly Dictionary<string, Comment> comments;

        public ExcelWorksheet(int maxNumberOfSheets, string fileName)
        {
            FilePath = fileName;
            stream = File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            doc = SpreadsheetDocument.Open(stream, false);
            sharedStringTable = doc.WorkbookPart.GetPartsOfType<SharedStringTablePart>().First();
            var sheets = doc.WorkbookPart.Workbook.Descendants<Sheet>().ToList();
            if (sheets.Count == 0)
            {
                return;
            }

            sheetData = new List<SheetData>();
            sheetParts = new List<WorksheetPart>();
            comments = new Dictionary<string, Comment>();
            for (int i = 0; i < Math.Min(maxNumberOfSheets, sheets.Count); i++)
            {
                var sheet = sheets[i];
                var sheetPart = (WorksheetPart)(doc.WorkbookPart.GetPartById(sheet.Id));
                sheetParts.Add(sheetPart);
                sheetData.Add(sheetPart.Worksheet.Elements<SheetData>().First());
                BuildCommentsDictionary(sheetPart, i);
            }
        }

        public string FilePath { get; private set; }

        public string GetCellMetadata(ICell cell, int sheetIndex = 0)
        {
            Comment comment;
            return comments.TryGetValue(GetCommentKey(sheetIndex, ((Cell)cell).UnderlyingCell.CellReference), out comment)
                ? comment.InnerText
                : null;
        }

        public IEnumerable<IRow> GetRows(int sheetIndex)
        {
            if (sheetIndex >= sheetData.Count)
            {
                return Enumerable.Empty<IRow>();
            }

            var commentPart = sheetParts[sheetIndex].GetPartsOfType<WorksheetCommentsPart>().FirstOrDefault();
            var commentsDictionary = new Dictionary<string, Comment>();
            if (commentPart != null && commentPart.Comments != null)
            {
                commentsDictionary = commentPart.Comments.CommentList.ToDictionary(k => ((Comment)k).Reference.Value, v => (Comment)v);
            }

            return sheetData[sheetIndex].Elements<SpreadSheetRow>()
                .Select(r => new Row(r, sharedStringTable, commentsDictionary));
        }

        private void BuildCommentsDictionary(WorksheetPart sheetPart, int sheetIndex)
        {
            var commentPart = sheetPart.GetPartsOfType<WorksheetCommentsPart>().FirstOrDefault();
            if (commentPart == null || commentPart.Comments == null)
            {
                return;
            }

            foreach (var comment in commentPart.Comments.CommentList.ToDictionary(k => ((Comment)k).Reference.Value, v => (Comment)v))
            {
                string commentKey = GetCommentKey(sheetIndex, comment.Key);
                if (!comments.ContainsKey(commentKey))
                    comments.Add(commentKey, comment.Value);
            }
        }

        private static string GetCommentKey(int sheetIndex, string cellReference)
        {
            return sheetIndex + "_" + cellReference;
        }

        public void Dispose()
        {
            try
            {
                if (doc != null)
                {
                    doc.Dispose();
                }
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
            catch
            {
                // do not throw exceptions from Dispose
            }
        }
    }
    public interface IContentParser<TContent, TRow>
    {
        TContent GetContent();
        IEnumerable<TRow> GetRows();
        UpgradeMatrixRig[] GetRigs();
    }
    public class RigInfo
    {
        public RigInfo(string sapId, int columnIndex, string rigType)
        {
            SapId = sapId;
            ColumnIndex = columnIndex;
            RigType = rigType;
        }

        public string SapId { get; private set; }
        public int ColumnIndex { get; private set; }
        public string RigType { get; private set; }
    }

    internal class ColumnDetails
    {
        public ColumnDetails(string columnName, bool required)
        {
            ColumnName = columnName;
            Required = required;
        }

        public string ColumnName { get; private set; }
        public bool Required { get; private set; }
    }
    public class CcnParser : IContentParser<CcnContent, CcnContent.Row>
    {
        private const int SheetIndex = 0;
        private const int HeaderRowIndex = 3;
        private const int FirstDataRowIndex = 4;
        public const string RigIdRegexGroupName = "rigId";

        public static readonly Regex RegexForSapRigId = new Regex(@"([^\d]|^)(?<rigId>\d{8})([^\d]|$)");
        private static readonly Regex RegexForCcn = new Regex(@"^CCN[\s]*[\d]+$", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        private readonly bool invalid;
        private readonly IDataTable document;
        private readonly Dictionary<string, int> indexes;
        private readonly Dictionary<string, RigInfo> rigInfo;
        private readonly FileInfo[] ccnFileInfoList;

        private static readonly FileInfo[] Empty = new FileInfo[0];
        private const string CcnFolderName = "CCN";
        private const string CcnFilePattern = "*.pdf";

        private static class Columns
        {
            public static readonly ColumnDetails Sfi = new ColumnDetails("SFI", false);
            public static readonly ColumnDetails Equipment = new ColumnDetails("EQUIPMENT", true);
            public static readonly ColumnDetails EquipmentId = new ColumnDetails("EQUIPMENT ID", false);
            public static readonly ColumnDetails Description = new ColumnDetails("DESCRIPTION", true);
            public static readonly ColumnDetails Comments = new ColumnDetails("COMMENTS", true);
            public static readonly ColumnDetails Quotation = new ColumnDetails("QUOTATION", true);
            public static readonly ColumnDetails PackageUpgrade = new ColumnDetails("PACKAGE UPGRADE", true);
            public static readonly ColumnDetails Status = new ColumnDetails("STATUS", false);
            public static readonly ColumnDetails RigStart = new ColumnDetails("RIG_START", true);
            public static readonly ColumnDetails RigEnd = new ColumnDetails("RIG_END", true);
            public static readonly ColumnDetails RigType = new ColumnDetails("RIG TYPE", false);

            public static readonly ColumnDetails[] List = { Sfi, Equipment, EquipmentId, Description, Comments, Quotation, PackageUpgrade, Status, RigStart, RigEnd, RigType };
        }

        public CcnParser(IDataTable document)
        {
            try
            {
                this.document = document;

                var header = Utils.GetRow(document, SheetIndex, HeaderRowIndex);
                indexes = header.GetColumnsIndexes(Columns.List);
                rigInfo = header.GetRigInfo(indexes[Columns.RigStart.ColumnName] + 1, indexes[Columns.RigEnd.ColumnName] - 1);
                //todo need to inject it for testing
                ccnFileInfoList = GetCcnFiles(Path.GetDirectoryName(document.FilePath));
            }
            catch (InvalidDataException)
            {
                invalid = true;
            }
        }

        public static FileInfo[] GetCcnFiles(string directory)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(directory))
                {
                    return Empty;
                }

                var di = new DirectoryInfo(directory);
                if (!di.Exists)
                {
                    return Empty;
                }

                var ccnFiles = new List<FileInfo>();
                var ccnDirectory = new DirectoryInfo(Path.Combine(di.FullName, CcnFolderName));
                PopulateFileList(ccnDirectory, ccnFiles);
                return ccnFiles.ToArray();
            }
            catch (Exception)
            {
                return Empty; //in case there are file/folder access issues
            }
        }

        private static void PopulateFileList(DirectoryInfo ccnDirectory, List<FileInfo> ccnFileInfoList)
        {
            if (!ccnDirectory.Exists)
            {
                return;
            }

            ccnFileInfoList.AddRange(ccnDirectory.GetFiles(CcnFilePattern));
            foreach (var subFolder in ccnDirectory.GetDirectories())
            {
                PopulateFileList(subFolder, ccnFileInfoList);
            }
        }

        public CcnContent GetContent()
        {
            if (invalid)
            {
                return null;
            }

            return new CcnContent
            {
                Rigs = GetRigs(),
                Rows = GetRows().ToArray()
            };
        }

        public UpgradeMatrixRig[] GetRigs()
        {
            if (invalid)
            {
                return null;
            }

            return rigInfo.Select(r => new UpgradeMatrixRig { ShortName = r.Key, ReferenceId = r.Value.SapId.ConvertSapIdToMyDrillingId(), RigType = r.Value.RigType }).ToArray();
        }

        public IEnumerable<CcnContent.Row> GetRows()
        {
            if (invalid)
            {
                yield break;
            }

            var enumerable = document.GetRows(SheetIndex).ToList();
            if (enumerable.Count == 0)
            {
                yield break;
            }

            var lastDataRowIndex = enumerable.CalculateLastDataRowIndex(FirstDataRowIndex);
            if (lastDataRowIndex < FirstDataRowIndex)
            {
                throw new InvalidOperationException("Software matrix table contains no data");
            }

            foreach (var row in enumerable.Skip(FirstDataRowIndex).Take(lastDataRowIndex - FirstDataRowIndex + 1))
            {
                var cells = row.Cells.ToArray();

                var ccnRow = new CcnContent.Row
                {
                    Equipment = cells[indexes[Columns.Equipment.ColumnName]].Value,
                    Description = cells[indexes[Columns.Description.ColumnName]].Value,
                    Comments = cells[indexes[Columns.Comments.ColumnName]].Value,
                    RigStates = rigInfo.Select(r => GetRigStateByCell(cells, r.Key, r.Value)).ToArray(),
                    PackageUpgrade = cells[indexes[Columns.PackageUpgrade.ColumnName]].Value,
                    RigType = indexes[Columns.RigType.ColumnName] > -1 ? cells[indexes[Columns.RigType.ColumnName]].Value : string.Empty,
                    Status = cells[indexes[Columns.Status.ColumnName]].Value
                };
                yield return ccnRow;
            }
        }

        private RigState GetRigStateByCell(IList<ICell> cells, string rigKey, RigInfo rig)
        {
            string rigStateValue = null;
            string ccnValue = null;
            var cellValue = cells[rig.ColumnIndex].Value;
            if (!string.IsNullOrWhiteSpace(cellValue))
            {
                cellValue = cellValue.Trim();
                if (!cellValue.Contains("/"))
                {
                    rigStateValue = cellValue;
                }
                else
                {
                    var cellValueParts = cellValue.Split('/');
                    if (cellValueParts.Length > 0)
                    {
                        ccnValue = cellValueParts[0].Trim();
                        if (!RegexForCcn.IsMatch(ccnValue))
                        {
                            ccnValue = string.Empty;
                        }
                    }

                    if (cellValueParts.Length > 1)
                    {
                        rigStateValue = cellValueParts[1].Trim();
                    }
                }
            }

            var isCcnValid = CcnDocumentValid(ccnValue, rigKey);
            var ccn = isCcnValid ? ccnValue : "";
            var ccnDocumentFullName = isCcnValid ? GetDocumentFullName(rigKey, ccn) : null;
            var rigState = new RigState
            {
                Rig = rigKey,
                ImplementedState = rigStateValue.GetSoftwareMatrixRigImplementedStateByStringValue(),
                CcnDocumentName = ccn,
                CcnDocument = ccnDocumentFullName
            };
            return rigState;
        }

        private bool CcnDocumentValid(string ccnValue, string rigSapId)
        {
            if (string.IsNullOrWhiteSpace(ccnValue) 
                || !RegexForCcn.IsMatch(ccnValue)
                || Array.Exists(ccnFileInfoList, ccnFileInfo => CcnFileMatches(rigSapId, ccnValue, ccnFileInfo)))
            {
                return false;
            }
            return true;
        }

        private static bool CcnFileMatches(string rigSapId, string ccnValue, FileInfo ccnFileInfo)
        {
            var sapIdInDirectory = RegexHelper.GetTextByGroup(RegexForSapRigId, RigIdRegexGroupName, ccnFileInfo.Directory != null ? ccnFileInfo.Directory.Name : string.Empty);
            return string.Compare(ccnValue, Path.GetFileNameWithoutExtension(ccnFileInfo.Name), StringComparison.InvariantCultureIgnoreCase) == 0
                    && sapIdInDirectory == rigSapId;
        }

        public string GetDocumentFullName(string rigKey, string ccn)
        {
            if (!rigInfo.ContainsKey(rigKey))
            {
                return null;
            }
            var fileInfo = ccnFileInfoList.FirstOrDefault(ccnFileInfo => CcnFileMatches(rigInfo[rigKey].SapId, ccn, ccnFileInfo));
            if (fileInfo == null)
            {
                return null;
            }
            return fileInfo.FullName;
        }
    }

    public class QuotationsParser : IContentParser<QuotationsContent, QuotationsContent.Row>
    {
        private const int SheetIndex = 1;
        private const int HeaderRowIndex = 3;
        private const int FirstDataRowIndex = 4;

        private readonly bool invalid;
        private readonly IDataTable document;
        private readonly Dictionary<string, int> indexes;
        private readonly Dictionary<string, RigInfo> rigInfo;

        private static class Columns
        {
            public static readonly ColumnDetails Equipment = new ColumnDetails("EQUIPMENT", true);
            public static readonly ColumnDetails EquipmentId = new ColumnDetails("EQUIPMENT ID", false);
            public static readonly ColumnDetails Description = new ColumnDetails("DESCRIPTION", true);
            public static readonly ColumnDetails Quotation = new ColumnDetails("QUOTATION", true);
            public static readonly ColumnDetails Status = new ColumnDetails("STATUS", false);
            public static readonly ColumnDetails RigStart = new ColumnDetails("RIG_START", true);
            public static readonly ColumnDetails RigEnd = new ColumnDetails("RIG_END", true);
            public static readonly ColumnDetails Heading = new ColumnDetails("HEADING", true);
            public static readonly ColumnDetails Benefits = new ColumnDetails("ROI/BENEFITS", true);
            public static readonly ColumnDetails TechnicalService = new ColumnDetails("TECHNICAL SERVICE BULLETIN", false);

            public static readonly ColumnDetails[] List = { Equipment, EquipmentId, Description, Quotation, Status, RigStart, RigEnd, Heading, Benefits, TechnicalService };
        }

        public QuotationsParser(IDataTable document)
        {
            try
            {
                this.document = document;

                var header = Utils.GetRow(document, SheetIndex, HeaderRowIndex);
                indexes = header.GetColumnsIndexes(Columns.List);
                rigInfo = header.GetRigInfo(indexes[Columns.RigStart.ColumnName] + 1, indexes[Columns.RigEnd.ColumnName] - 1);
            }
            catch (InvalidDataException)
            {
                invalid = true;
            }
        }

        public QuotationsContent GetContent()
        {
            if (invalid)
            {
                return null;
            }

            return new QuotationsContent
            {
                Rigs = GetRigs(),
                Rows = GetRows().ToArray()
            };
        }

        public UpgradeMatrixRig[] GetRigs()
        {
            if (invalid)
            {
                return null;
            }

            return rigInfo.Select(r => new UpgradeMatrixRig { ShortName = r.Key, ReferenceId = r.Value.SapId.ConvertSapIdToMyDrillingId(), RigType = r.Value.RigType }).ToArray();
        }

        public IEnumerable<QuotationsContent.Row> GetRows()
        {
            if (invalid)
            {
                yield break;
            }

            var enumerable = document.GetRows(SheetIndex).ToList();
            if (enumerable.Count == 0)
            {
                yield break;
            }

            var lastDataRowIndex = enumerable.CalculateLastDataRowIndex(FirstDataRowIndex);
            if (lastDataRowIndex < FirstDataRowIndex)
            {
                yield break;
            }

            foreach (var row in enumerable.Skip(FirstDataRowIndex).Take(lastDataRowIndex - FirstDataRowIndex + 1))
            {
                var cells = row.Cells.ToArray();

                var quotationRow = new QuotationsContent.Row
                {
                    Equipment = cells[indexes[Columns.Equipment.ColumnName]].Value,
                    Description = cells[indexes[Columns.Description.ColumnName]].Value,
                    Quotation = cells[indexes[Columns.Quotation.ColumnName]].Value,
                    RigStates = rigInfo.Select(r => new RigState
                    {
                        Rig = r.Key,
                        ImplementedState = cells[r.Value.ColumnIndex].Value.GetSoftwareMatrixRigImplementedStateByStringValue()
                    }).ToArray(),
                    Heading = cells[indexes[Columns.Heading.ColumnName]].Value,
                    Benefits = cells[indexes[Columns.Benefits.ColumnName]].Value
                };
                yield return quotationRow;
            }
        }
    }

    internal static class CellArrayExt
    {
        public static Dictionary<string, int> GetColumnsIndexes(this ICell[] header, ColumnDetails[] columnDetails)
        {
            if (header == null)
            {
                throw new InvalidDataException("Data table is empty");
            }

            var result = new Dictionary<string, int>();
            var columns = header.Select(t => !string.IsNullOrEmpty(t.Value) ? t.Value.ToUpper() : string.Empty).ToList();
            foreach (var cd in columnDetails)
            {
                var index = columns.IndexOf(cd.ColumnName);
                if (cd.Required && index < 0)
                {
                    throw new InvalidDataException(string.Format("Data table doesn't contain column with name '{0}'", cd.ColumnName));
                }

                result[cd.ColumnName] = index;
            }

            return result;
        }

        private static readonly Regex RegexForSapRigId = new Regex(@"([^\d]|^)(?<rigId>\d{8})([^\d]|$)");
        private const string RigIdRegexGroupName = "rigId";
        public static Dictionary<string, RigInfo> GetRigInfo(this ICell[] header, int from, int to)
        {
            var result = new Dictionary<string, RigInfo>();
            if (header == null)
            {
                return result;
            }

            for (var cellIdx = from; cellIdx <= to; cellIdx++)
            {
                var cell = header[cellIdx];
                var cellComment = string.IsNullOrWhiteSpace(cell.Comment) ? string.Empty : cell.Comment.Trim();
                var cellCommentParts = !string.IsNullOrWhiteSpace(cellComment) ? cellComment.Split(';') : null;
                if (cellCommentParts != null && cellCommentParts.Length > 0)
                {
                    var sapRigId = RegexHelper.GetTextByGroup(RegexForSapRigId, RigIdRegexGroupName, cellCommentParts[0]);
                    var rigType = cellCommentParts.Length > 1 ? cellCommentParts[1] : "";
                    if (!string.IsNullOrWhiteSpace(sapRigId) && !result.ContainsKey(cell.Value))
                    {
                        result.Add(cell.Value, new RigInfo(sapRigId, cellIdx, rigType));
                    }
                }
            }

            return result;
        }
    }

    internal static class StringExt
    {
        private const int MyDrillingMaxIdLength = 18;
        public static string ConvertSapIdToMyDrillingId(this string sapId)
        {
            if (!string.IsNullOrWhiteSpace(sapId) && sapId.Length < MyDrillingMaxIdLength)
            {
                var correctedSapId = sapId.PadLeft(MyDrillingMaxIdLength, '0');
                return correctedSapId;
            }

            return sapId;
        }

        public static SoftwareMatrixRigImplementedState GetSoftwareMatrixRigImplementedStateByStringValue(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return SoftwareMatrixRigImplementedState.Undefined;
            }

            value = value.Trim().TrimStart('/').Trim();

            var enumValues = Enum.GetValues(typeof(SoftwareMatrixRigImplementedState));
            foreach (SoftwareMatrixRigImplementedState enumValue in enumValues)
            {
                var enumStringValue = EnumHelper.GetStringValue(enumValue);
                if (enumStringValue.Contains("|"))
                {
                    var enumAllowedValues = enumStringValue.Split('|');
                    if (enumAllowedValues.Any(av => string.Compare(value, av, StringComparison.InvariantCultureIgnoreCase) == 0))
                    {
                        return enumValue;
                    }
                }
                else if (string.Compare(value, EnumHelper.GetStringValue(enumValue), StringComparison.InvariantCultureIgnoreCase) == 0)
                {
                    return enumValue;
                }
            }
            return SoftwareMatrixRigImplementedState.Undefined;
        }
    }

    internal static class RowArrayExt
    {
        public static int CalculateLastDataRowIndex(this IEnumerable<IRow> rows, int start, string column = "A")
        {
            var lastDataRowIndex = start - 1;
            foreach (var row in rows.Skip(start))
            {
                var cell = row.Cells.FirstOrDefault(c => c.Column == column);
                var keyColumnValue = cell != null ? cell.Value : null;
                if (string.IsNullOrWhiteSpace(keyColumnValue))
                {
                    return lastDataRowIndex;
                }

                lastDataRowIndex++;
            }

            return lastDataRowIndex;
        }
    }
    
    public static class RegexHelper
    {
        public static string GetTextByGroup(Regex regex, string groupName, string format, params object[] args)
        {
            var matchCollection = regex.Matches(string.Format(format, args));
            return matchCollection.Count > 0 && matchCollection[0].Groups[groupName].Success
                ? matchCollection[0].Groups[groupName].Value
                : string.Empty;
        }
    }

    internal static class Utils
    {
        public static ICell[] GetRow(IDataTable document, int sheet, int row)
        {
            var rows = document.GetRows(sheet);
            var enumerable = rows as IRow[] ?? rows.ToArray();
            var header = rows != null && enumerable.Any() ? enumerable.ElementAt(row).Cells.ToArray() : null;
            return header;
        }
    }
}
