﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Linq;
using Azure.Storage.Blobs;
using Dapper;
using DocumentFormat.OpenXml.Bibliography;
using Microsoft.Data.SqlClient;
using MyDrilling.Core.Entities;
using MyDrilling.Infrastructure.Storage;
using Serilog;

namespace MyDrilling.Migrator.Migrators
{
    public static class UserMigrator
    {
        private static myDrillingUser[] _users;
        private static Dictionary<long, UserSetting[]> _userSettings;
        private static Dictionary<string, UserImage> _userImages;
        private static ClaimsMigrator.User[] _usersWithClaims;
        private static Dictionary<string, TermsAndConditions> _termsAndConditions;

        public static void Do(string oldServerConnectionString, string newServerConnectionString, string storageConnectionString)
        {
            Log.Logger.Information("Importing users...");

            LoadIfNeeded(oldServerConnectionString);

            var usersWithoutUpn = _users.Where(x => string.IsNullOrEmpty(x.UPN) && !string.IsNullOrEmpty(x.DisplayName))
                .GroupBy(x => $"{x.FirstName?.ToLowerInvariant().Trim()}-{x.LastName?.ToLowerInvariant().Trim()}".ToLowerInvariant()).ToDictionary(x => x.Key, x => x.OrderBy(y => y.myDrillingUserId).ToArray());
            var usersWithUpn = _users.Where(x => !string.IsNullOrEmpty(x.UPN))
                .GroupBy(x => x.UPN.Trim().ToLowerInvariant()).ToDictionary(x => x.Key, x => x.OrderBy(y => y.myDrillingUserId).ToArray());

            var hs = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);

            var blobServiceClient = new BlobServiceClient(storageConnectionString);
            var containerClient = blobServiceClient.GetBlobContainerClient(StorageConfig.BlobUserImagesContainerName);

            var badUpns = new HashSet<string>();

            using (var newDb = newServerConnectionString.GetNewMyDrillingDbContext())
            {
                foreach (var usersWithUpnGrouped in usersWithUpn)
                {
                    var userWithUpn = usersWithUpnGrouped.Value.Last();

                    if (hs.Contains(usersWithUpnGrouped.Key))
                    {
                        continue;
                    }

                    if (!User.UpnValidationPattern.IsMatch(usersWithUpnGrouped.Key))
                    {
                        badUpns.Add(usersWithUpnGrouped.Key);
                        continue;
                    }

                    hs.Add(usersWithUpnGrouped.Key);

                    var newUserWithUpn = new User(usersWithUpnGrouped.Key)
                    { 
                        AcceptedTermsAndConditions = _termsAndConditions.TryGetValue(usersWithUpnGrouped.Key, out var read) ? read.GetDateTime() : default(DateTime?)
                    };
                    Map(userWithUpn, usersWithUpnGrouped.Value.Select(x => x.myDrillingUserId).ToArray(), newUserWithUpn);

                    newDb.Users.Add(newUserWithUpn);

                    if (_userImages.TryGetValue(usersWithUpnGrouped.Key, out var image))
                    {
                        var imageName = $"{CommonStringExt.Normalize(usersWithUpnGrouped.Key)}.{image.Extension.ToLowerInvariant().Trim().Trim(new[] { '.' })}";
                        var blobClient = containerClient.GetBlobClient(imageName);
                        if (!blobClient.Exists())
                        {
                            blobClient.Upload(new MemoryStream(image.Data));
                        }

                        newUserWithUpn.Image = imageName;
                    }
                }

                newDb.SaveChanges();

                foreach (var userWithoutUpnGrouped in usersWithoutUpn)
                {
                    var userWithoutUpn = userWithoutUpnGrouped.Value.Last();
                    
                    var usersInClaims = _usersWithClaims
                        .Where(x => x.UPN.MdStartsWith(userWithoutUpn.GetEmailPattern())).ToArray();

                    if (usersInClaims.Length == 0)
                    {
                        continue;
                    }

                    var upn = usersInClaims.First().UPN.Trim();

                    if (hs.Contains(upn))
                    {
                        continue;
                    }
                    hs.Add(upn);
                    userWithoutUpn.UPN = upn; //this upn will be used in SetCustomerIdForUsers method

                    var newUserWithoutUpn = new User(upn)
                    {
                        AcceptedTermsAndConditions = _termsAndConditions.TryGetValue(upn, out var read) ? read.GetDateTime() : default(DateTime?)
                    };
                    Map(userWithoutUpn, userWithoutUpnGrouped.Value.Select(x => x.myDrillingUserId).ToArray(), newUserWithoutUpn);

                    newDb.Users.Add(newUserWithoutUpn);

                    if (_userImages.TryGetValue(upn, out var image))
                    {
                        var imageName = $"{CommonStringExt.Normalize(upn)}.{image.Extension.ToLowerInvariant().Trim().Trim(new[] { '.' })}";
                        var blobClient = containerClient.GetBlobClient(imageName);
                        if (!blobClient.Exists())
                        {
                            blobClient.Upload(new MemoryStream(image.Data));
                        }

                        newUserWithoutUpn.Image = imageName;
                    }
                }

                newDb.SaveChanges();
            }

            Log.Logger.Information(nameof(badUpns));
            Log.Logger.Information(string.Join(',', badUpns.Select(x => $"'{x}'")));

            Log.Logger.Information("Imported users");
        }

        public static void SetCustomerIdForUsers(string oldServerConnectionString, string newServerConnectionString)
        {
            Log.Logger.Information("Assigning users to customers...");

            LoadIfNeeded(oldServerConnectionString);

            Dictionary<string, string> userCustomers;

            using (var conn = new SqlConnection(oldServerConnectionString.GetClaimsStoreConnectionString()))
            {
                userCustomers = conn.Query<UserCustomer>("select u.UPN, ua.Id, ua.Value as Customer from Users u join UserAttributes ua on u.Id = ua.User_ID where Attribute_ID = 17").ToArray()
                    .GroupBy(x => x.Upn.ToLower().Trim()).ToDictionary(x => x.Key, x => x.OrderBy(y => y.Id).Select(y => y.Customer).Last());
            }

            var users = _users.Where(x => !string.IsNullOrEmpty(x.UPN)).GroupBy(x => x.UPN.ToLower().Trim()).ToDictionary(x => x.Key, x => x.OrderBy(y => y.myDrillingUserId).Last().CustomerId);

            using (var newDb = newServerConnectionString.GetNewMyDrillingDbContext())
            {
                var rootCustomers = newDb.RootCustomers.ToDictionary(x => x.Name, x => x);

                foreach (var newUser in newDb.Users.ToArray())
                {
                    if (users.TryGetValue(newUser.Upn, out var customerId) 
                        && CustomerMappings.TryFindMydCustomerName(customerId, out var mydCustomerName)
                        && rootCustomers.TryGetValue(mydCustomerName, out var rootCustomer))
                    {
                        newUser.RootCustomer = rootCustomer;
                    }
                    
                    if (userCustomers.TryGetValue(newUser.Upn, out var customerFromClaims)
                        && CustomerMappings.TryFindMydCustomerName(customerFromClaims, out mydCustomerName)
                        && rootCustomers.TryGetValue(mydCustomerName, out rootCustomer))
                    {
                        newUser.RootCustomer = rootCustomer;
                    }
                }

                newDb.SaveChanges();
            }

            Log.Logger.Information("Assigned users to customers");
        }

        private static void LoadIfNeeded(string oldServerConnectionString)
        {
            if (_users != null)
            {
                return;
            }

            using (var conn = new SqlConnection(oldServerConnectionString.GetPortalConnectionString()))
            {
                _users = conn.Query<myDrillingUser>("select * from myDrillingUser where CustomerId <> 'DISABLED' order by myDrillingUserId").ToArray();
                _userSettings = conn.Query<UserSetting>("select * from UserSetting").GroupBy(x => x.UserId).ToDictionary(x => x.Key, x => x.ToArray());
                _userImages = conn.Query<UserImage>("select ui.*, d.* from UserImage ui join Document d on ui.DocumentId = d.DocumentId where Thumbnail = 1")
                    .ToDictionary(x => x.UserUPN.ToLower().Trim(), x => x, CommonStringExt.MdStringComparer);
            }

            using (var conn = new SqlConnection(oldServerConnectionString.GetClaimsStoreConnectionString()))
            {
                _usersWithClaims = conn.Query<ClaimsMigrator.User>("select * from Users").ToArray();

                //loading http://mydrilling.com/ws/2012/03/userprofile/lastaccepttermsandconditionstimestamp
                _termsAndConditions = conn.Query<TermsAndConditions>("select u.UPN, MAX(ua.Value) as Value from UserAttributes ua join Users u on u.ID = ua.User_ID where Attribute_ID = 12 group by u.UPN  ")
                    .ToDictionary(x => x.Upn.ToLower().Trim(), x => x, CommonStringExt.MdStringComparer);
            }
        }

        private static void Map(myDrillingUser fromUser, long[] mdUserIds, User toUser)
        {
            toUser.Email = fromUser.Email?.Trim().ToLower();
            toUser.IsActive = fromUser.Status != "Disable" && !string.IsNullOrEmpty(fromUser.UPN);
            toUser.IsInternal = fromUser.InternalUser ?? false;
            toUser.FirstName = fromUser.FirstName;
            toUser.LastName = fromUser.LastName;
            toUser.DisplayName = fromUser.DisplayName;
            toUser.Position = fromUser.Title;
            toUser.PhoneNumber = fromUser.PhoneNumber;
            toUser.MobileNumber = fromUser.MobileNumber;
            toUser.FaxNumber = fromUser.FaxNumber;
            toUser.Country = fromUser.Country;
            toUser.CountryCode = fromUser.CountryCode;
            toUser.LastLogIn = fromUser.LastLogIn;
            toUser.Created = fromUser.LastModified ?? DateTime.UnixEpoch;
            toUser.Modified = fromUser.LastModified ?? DateTime.UnixEpoch;
            if (!string.IsNullOrEmpty(fromUser.UserID) && int.TryParse(fromUser.UserID, out _))
            {
                toUser.ReferenceId = fromUser.UserID;
            }

            var tz = GetUserSetting(Core.Entities.UserSetting.Keys.SelectedTimezone, mdUserIds);
            if (!string.IsNullOrEmpty(tz))
            {
                toUser.AddSetting(Core.Entities.UserSetting.Keys.SelectedTimezone, tz);
            }
            var addToEnquiries = GetUserSetting(Core.Entities.UserSetting.Keys.DoNotAllowOthersToAddMeToEnquiries, mdUserIds);
            if (!string.IsNullOrEmpty(addToEnquiries))
            {
                toUser.AddSetting(Core.Entities.UserSetting.Keys.DoNotAllowOthersToAddMeToEnquiries, addToEnquiries);
            }
            var showUnreadEnquiries = GetUserSetting(Core.Entities.UserSetting.Keys.ShowUnreadEnquiries, mdUserIds);
            if (!string.IsNullOrEmpty(showUnreadEnquiries))
            {
                toUser.AddSetting(Core.Entities.UserSetting.Keys.ShowUnreadEnquiries, showUnreadEnquiries);
            }
        }

        private static string GetUserSetting(string setting, long[] userIds)
        {
            var userSettings = _userSettings.Where(x => userIds.Contains(x.Key)).SelectMany(x => x.Value).Where(x => x.Key == setting).ToArray();
            return userSettings.Length == 0 ? null : userSettings.Last().Value;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class myDrillingUser
        {
            public string UniqueID;
            public string UserID;
            public string EmployeeID;
            public string Status;
            public string DisplayName;
            public string FirstName;
            public string LastName;
            public string CustomerName;
            public string CompanyID;
            public string Department;
            public string Title;
            public string Position;
            public string PhoneNumber;
            public string MobileNumber;
            public string FaxNumber;
            public string Email;
            public string Country;
            public string CountryCode;
            public string CustomerId;
            public DateTime? LastModified;
            public bool? InternalUser;
            public long myDrillingUserId;
            public DateTime? LastLogIn;
            public string LastImport;
            public string UPN;
            public string DepartmentID;
            public string Subdomain;

            public string GetEmailPattern() => CommonStringExt.GetEmailPattern(FirstName, LastName);
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class UserImage
        {
            public int Id;
            public string UserUPN;
            public bool Thumbnail;
            public int Height;
            public int Width;

            //from Document table
            public string DocumentTitle;
            public string ContentType;
            public string Extension;
            public byte[] Data;
        }

        public class UserSetting
        {
            public long UserId;
            public string Key;
            public string Value;
        }

        public class UserCustomer
        {
            public int Id { get; set; }
            public string Upn;
            public string Customer;
        }

        public class TermsAndConditions
        {
            public string Upn;
            public string Value;

            public DateTime GetDateTime() => DateTime.TryParse(Value, out var parsed1)
                ? parsed1
                : DateTime.TryParseExact(Value, @"dd/MM/yyyy hh:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out var parsed2)
                    ? parsed2
                    : DateTime.UnixEpoch;
        }
    }
}
