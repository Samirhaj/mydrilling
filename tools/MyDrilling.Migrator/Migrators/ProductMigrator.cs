﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core.Entities;
using MyDrilling.Infrastructure.Data;
using Serilog;

namespace MyDrilling.Migrator.Migrators
{
    public static class ProductMigrator
    {
        public static void Do(string oldServerConnectionString, string newServerConnectionString)
        {
            Log.Logger.Information("Importing products...");

            DimProduct[] dimProducts;

            using (var conn = new SqlConnection(oldServerConnectionString.GetDwhConnectionString()))
            {
                dimProducts = conn.Query<DimProduct>("select * from dim_product where product_id is not null").ToArray();
            }

            using (var newDb = newServerConnectionString.GetNewMyDrillingDbContext())
            {
                foreach (var dp in dimProducts)
                {
                    //newDb.Products.Add(new Product
                    //{
                    //    Number = dp.Product_Id,
                    //    Name = dp.Product_Name,
                    //    Group = dp.Product_Group,
                    //    Created = dp.Insert_Date,
                    //    Modified = dp.Update_Date ?? dp.Insert_Date
                    //});
                }

                newDb.SaveChanges();
            }

            Log.Logger.Information("Imported products");
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class DimProduct
        {
            public int PK_Dim_Product;
            public string Product_Id;
            public string Product_Name;
            public string Product_Description;
            public string Product_Image;
            public string Product_Group;
            public string Source;
            public DateTime? Valid_From_Date;
            public DateTime? Valid_To_Date;
            public DateTime Insert_Date;
            public DateTime? Update_Date;
        }
    }
}
