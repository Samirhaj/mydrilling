﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using Dapper;
using Microsoft.Data.SqlClient;
using MyDrilling.Core;
using MyDrilling.Core.Entities.Permission;
using Serilog;

namespace MyDrilling.Migrator.Migrators
{
    public static class ClaimsMigrator
    {
        public static void Do(string oldServerConnectionString, string newServerConnectionString)
        {
            Log.Logger.Information("Importing claims...");

            AccessProfileRight[] profileRights;
            AccessProfile[] profiles;
            ClaimAttribute[] attributes;
            UserAccessProfileFacility[] profileAttributes;
            UserAttribute[] userAttributes;
            Dictionary<int, string> users;
            StringComparer.Create(CultureInfo.CurrentCulture, CompareOptions.IgnoreNonSpace);

            using (var conn = new SqlConnection(oldServerConnectionString.GetClaimsStoreConnectionString()))
            {
                profileRights = conn.Query<AccessProfileRight>("select * from AccessProfileRights").ToArray();
                profiles = conn.Query<AccessProfile>("select * from AccessProfiles").ToArray();
                attributes = conn.Query<ClaimAttribute>("select * from ClaimAttributes").ToArray();
                profileAttributes = conn.Query<UserAccessProfileFacility>("select * from UserAccessProfileFacilities").ToArray();
                userAttributes = conn.Query<UserAttribute>("select * from UserAttributes").ToArray();
                users = conn.Query<User>("select * from Users").ToDictionary(x => x.ID, x => x.UPN.ToLower().Trim());
            }

            var missedCustomers = new HashSet<string>();
            var missedUsers = new HashSet<int>();
            var missedRigs = new HashSet<string>();

            using (var newDb = newServerConnectionString.GetNewMyDrillingDbContext())
            {
                var rootCustomers = newDb.RootCustomers.ToDictionary(x => x.Name, x => x);
                var newUsers = newDb.Users.ToDictionary(x => x.Upn, x => x, CommonStringExt.MdStringComparer);
                var newRigs = newDb.Rigs.ToDictionary(x => x.ReferenceId, x => x);

                //roles
                var rolesMapping = new Dictionary<int, RoleInfo>();
                foreach (var role in RoleInfo.Roles)
                {
                    rolesMapping.Add(attributes.First(x => System.IO.Path.GetFileName(x.AttributeName) == role.Key).ID, role);
                }
                newDb.SaveChanges();

                //profiles
                var profilesMapping = new Dictionary<int, Profile>();
                foreach (var profile in profiles)
                {
                    if (!CustomerMappings.TryFindMydCustomerName(profile.Company, out var mydCustomerName)
                        || !rootCustomers.TryGetValue(mydCustomerName, out var rootCustomer))
                    {
                        missedCustomers.Add(profile.Company);
                        continue;
                    }

                    //var newProfile = new Profile(profile.Name, rootCustomer);
                    var newProfile = new Profile(profile.Name);

                    newDb.Permission_Profiles.Add(newProfile);
                    profilesMapping.Add(profile.ID, newProfile);
                }
                newDb.SaveChanges();
                
                //profile roles
                foreach (var profileRight in profileRights)
                {
                    if (!rolesMapping.TryGetValue(profileRight.RoleAttribute_ID, out var role))
                    {
                        continue;
                    }

                    if (!profilesMapping.TryGetValue(profileRight.AccessProfile_ID, out var profile))
                    {
                        continue;
                    }

                    newDb.Permission_ProfileRoles.Add(new ProfileRole(profile, role.Value));
                }
                newDb.SaveChanges();

                //user claims
                foreach (var userAttribute in userAttributes)
                {
                    if(!rolesMapping.TryGetValue(userAttribute.Attribute_ID, out var role))
                    {
                        continue;
                    }

                    if (!users.TryGetValue(userAttribute.User_ID, out var upn)
                        || !newUsers.TryGetValue(upn, out var user))
                    {
                        missedUsers.Add(userAttribute.User_ID);
                        continue;
                    }

                    switch (role.RoleType)
                    {
                        case RoleInfo.Type.General:
                            newDb.Permission_UserRoles.Add(UserRole.AssignGeneralRole(user, role));
                            break;
                        case RoleInfo.Type.Rig:
                            if (newRigs.TryGetValue(userAttribute.Value.ToLongReferenceId(), out var rig))
                            {
                                newDb.Permission_UserRoles.Add(UserRole.AssignRigRole(user, role, rig));
                            }
                            else
                            {
                                missedRigs.Add(userAttribute.Value);
                            }
                            break;
                        case RoleInfo.Type.RootCustomer:
                            if (CustomerMappings.TryFindMydCustomerName(userAttribute.Value, out var mydCustomerName) 
                                && rootCustomers.TryGetValue(mydCustomerName, out var customer))
                            {
                                newDb.Permission_UserRoles.Add(UserRole.AssignRootCustomerRole(user, role, customer));
                            }
                            else
                            {
                                missedCustomers.Add(userAttribute.Value);
                            }
                            break;
                    }
                }
                newDb.SaveChanges();

                //profile to rigs and users
                foreach (var profileAttribute in profileAttributes)
                {
                    if (!profilesMapping.TryGetValue(profileAttribute.AccessProfile_ID, out var profile))
                    {
                        continue;
                    }

                    if (!users.TryGetValue(profileAttribute.User_ID, out var upn)
                        || !newUsers.TryGetValue(upn, out var user))
                    {
                        missedUsers.Add(profileAttribute.User_ID);
                        continue;
                    }

                    newDb.Permission_UserProfiles.Add(new UserProfile(profile, user, newRigs[profileAttribute.Facility.ToLongReferenceId()]));
                }
                newDb.SaveChanges();
            }

            Log.Logger.Warning("missed users:");
            Log.Logger.Warning(string.Join(',', missedUsers)); 
            
            Log.Logger.Warning("missed customers:");
            Log.Logger.Warning(string.Join(',', missedCustomers.Select(x => $"'{x}'")));

            Log.Logger.Warning("missed rigs:");
            Log.Logger.Warning(string.Join(',', missedRigs.Select(x => $"'{x}'")));

            Log.Logger.Information("Imported claims");
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class AccessProfileRight
        {
            public int ID;
            public int AccessProfile_ID;
            public int RoleAttribute_ID;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class AccessProfile
        {
            public int ID;
            public string Name;
            public string Company;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class ClaimAttribute
        {
            public int ID;
            public string AttributeName;
            public int AttributeType_ID;
            public int OrderIndex;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class UserAccessProfileFacility
        {
            public int ID;
            public string Facility;
            public int User_ID;
            public int AccessProfile_ID;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class UserAttribute
        {
            public int ID;
            public string Value;
            public int Attribute_ID;
            public int User_ID;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class User
        {
            public int ID;
            public string UPN;
        }
    }
}
