﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Dapper;
using Microsoft.Data.SqlClient;
using MyDrilling.Core;
using MyDrilling.Core.Entities;
using Serilog;

namespace MyDrilling.Migrator.Migrators
{
    public static class CustomerMigrator
    {
        public static void Do(string oldServerConnectionString, string newServerConnectionString, bool insert)
        {
            Log.Logger.Information("Importing customers...");

            DimCustomer[] dimCustomers;
            DimMydCustomer[] dimMydCustomers;
            Dictionary<string, DimMydCustomerBridge[]> dimMydCustomerBridge;

            using (var conn = new SqlConnection(oldServerConnectionString.GetDwhConnectionString()))
            {
                dimCustomers = conn.Query<DimCustomer>("select * from dim_customer where Reference_Id is not null").ToArray();
                dimMydCustomers = conn.Query<DimMydCustomer>("select * from dim_myd_customer where last_version = 1").ToArray();
                dimMydCustomerBridge = conn.Query<DimMydCustomerBridge>("select * from DIM_Myd_Customer_Bridge where last_version = 1")
                    .GroupBy(x => x.PARENT_GLOBAL_ID).ToDictionary(x => x.Key, x => x.ToArray());
            }

            var savedDimCustomers = new HashSet<string>();

            using (var newDb = newServerConnectionString.GetNewMyDrillingDbContext())
            {
                var rootCustomerMappings = new Dictionary<string, RootCustomer>();

                //save root customers
                foreach (var dimMydCustomer in dimMydCustomers)
                {
                    var customer = new RootCustomer(dimMydCustomer.MYD_Customer_Name)
                    {
                        Created = dimMydCustomer.INSERT_DATE,
                        //CreatedById = , todo set user here when user migration is implemented
                        Modified = dimMydCustomer.INSERT_DATE,
                        IsInternal = false,
                        //ModifiedBy = todo set user here when user migration is implemented

                    };
                    newDb.RootCustomers.Add(customer);

                    rootCustomerMappings[dimMydCustomer.GLOBAL_ID] = customer;
                }
                newDb.SaveChanges();

                Dictionary<string, Customer> existingCustomers = null;
                if (!insert)
                {
                    existingCustomers = newDb.Customers.Where(x => !string.IsNullOrEmpty(x.ReferenceId)).ToDictionary(x => x.ReferenceId, x => x);
                }

                //save sap customers with root customers
                foreach (var dimMydCustomer in dimMydCustomers)
                {
                    var rootCustomer = rootCustomerMappings[dimMydCustomer.GLOBAL_ID];

                    if (!dimMydCustomerBridge.TryGetValue(dimMydCustomer.GLOBAL_ID, out var bridges))
                    {
                        continue;
                    }

                    foreach (var bridge in bridges)
                    {
                        if (savedDimCustomers.Contains(bridge.GLOBAL_ID))
                        {
                            Log.Logger.Warning($"dim_customer {bridge.GLOBAL_ID} is already added");
                            continue;
                        }

                        var sapCustomer = dimCustomers.SingleOrDefault(x => x.GLOBAL_ID == bridge.GLOBAL_ID);
                        if (sapCustomer == null)
                        {
                            Log.Logger.Warning($"dim_customer {bridge.GLOBAL_ID} not found for bridge {bridge.PK}, dim_myd_customer {dimMydCustomer.GLOBAL_ID}");
                            continue;
                        }

                        var newSapCustomer = insert
                            ? new Customer
                                {
                                    ReferenceId = sapCustomer.REFERENCE_ID,
                                    Name = sapCustomer.Customer_Name,
                                    Address = new Address
                                    {
                                        PostalCode = sapCustomer.Customer_Postal_Code,
                                        City = sapCustomer.Customer_City
                                    },
                                    Created = sapCustomer.insert_date,
                                    Modified = sapCustomer.update_date ?? sapCustomer.insert_date
                                }
                            : existingCustomers.TryGetValue(sapCustomer.REFERENCE_ID, out _)
                                ? existingCustomers[sapCustomer.REFERENCE_ID]
                                : null;

                        if (newSapCustomer == null)
                        {
                            Log.Logger.Error($"customer {sapCustomer.REFERENCE_ID} not found");
                            continue;
                        }

                        savedDimCustomers.Add(bridge.GLOBAL_ID);

                        if (bridge.IS_DEFAULT_GLOBAL_ID)
                        {
                            rootCustomer.SetDefaultCustomer(newSapCustomer);
                            continue;
                        }

                        rootCustomer.AddCustomer(newSapCustomer);
                    }
                }
                newDb.SaveChanges();

                if (insert)
                {
                    //save sapcustomers without mydcustomers
                    foreach (var dimCustomer in dimCustomers)
                    {
                        if (savedDimCustomers.Contains(dimCustomer.GLOBAL_ID))
                        {
                            continue;
                        }

                        var newSapCustomer = new Customer
                        {
                            ReferenceId = dimCustomer.REFERENCE_ID,
                            Name = dimCustomer.Customer_Name,
                            Address = new Address
                            {
                                PostalCode = dimCustomer.Customer_Postal_Code,
                                City = dimCustomer.Customer_City
                            },
                            Created = dimCustomer.insert_date,
                            Modified = dimCustomer.update_date ?? dimCustomer.insert_date
                        };
                        newDb.Customers.Add(newSapCustomer);

                        savedDimCustomers.Add(dimCustomer.REFERENCE_ID);
                    }

                    newDb.SaveChanges();
                }

                //add special MHWirth root customer - it will be used instead of INTERNAL constant

                newDb.RootCustomers.Add(new RootCustomer(Constants.MhWirth)
                {
                    IsInternal = true,
                });
                newDb.SaveChanges();
            }

            Log.Logger.Information("Imported customers");
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class DimCustomer
        {
            public int PK_Dim_Customer;
            public string GLOBAL_ID;
            public string PARENT_GLOBAL_ID;
            public string REFERENCE_ID;
            public string Customer_Id;
            public string Customer_Name;
            public string Customer_Client;
            public string Customer_Sold_To_Party;
            public string Customer_City;
            public string Customer_Postal_Code;
            public string Source;
            public string SOURCE_ID;
            public DateTime? valid_from_date;
            public DateTime? valid_to_date;
            public DateTime insert_date;
            public DateTime? update_date;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class DimMydCustomer
        {
            public int PK_Dim_MYD_Customer;
            public string GLOBAL_ID;
            public string MYD_Customer_Name;
            public string STATE;
            public string CONTEXT;
            public string QUALITY;
            public DateTime INSERT_DATE;
            public string INSERT_BY;
            public bool LAST_VERSION;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class DimMydCustomerBridge
        {
            public int PK;
            public string PARENT_GLOBAL_ID;
            public string GLOBAL_ID;
            public bool IS_DEFAULT_GLOBAL_ID;
            public DateTime INSERT_DATE;
            public string STATE;
            public string CONTEXT;
            public string INSERT_BY;
            public bool LAST_VERSION;
        }
    }
}
