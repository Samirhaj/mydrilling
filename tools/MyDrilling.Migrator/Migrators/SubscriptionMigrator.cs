﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Dapper;
using Microsoft.Data.SqlClient;
using MyDrilling.Core;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Core.Entities.Subscription;
using MyDrilling.Core.Entities.UpgradeMatrix;
using Serilog;
using MessageTemplate = MyDrilling.Core.Entities.Enquiry.MessageTemplate;
using UserSetting = MyDrilling.Core.Entities.Subscription.UserSetting;

namespace MyDrilling.Migrator.Migrators
{
    public static class SubscriptionMigrator
    {
        private static readonly Dictionary<string, EnquiryEventType> EnquiryEventMappings = new Dictionary<string, EnquiryEventType>
        {
            {"http://www.mydrilling.com/repository/process/id/13.1/subscription/id/3/event/id/1", EnquiryEventType.EnquiryDraftDeleted},
            {"http://www.mydrilling.com/repository/process/id/13.1/subscription/id/1/event/id/1", EnquiryEventType.EnquiryAssigned},
            {"http://www.mydrilling.com/repository/process/id/13.1/subscription/id/1/event/id/2", EnquiryEventType.EnquiryDraftAssigned},
            {"http://www.mydrilling.com/repository/process/id/13.2/subscription/id/2/event/id/1", EnquiryEventType.EnquiryRigMovedCreated},
            {"http://www.mydrilling.com/repository/process/id/13.2/subscription/id/1/event/id/1", EnquiryEventType.EnquiryRigDowntimeCreated},
            {"http://www.mydrilling.com/repository/process/id/13.1/subscription/id/4/event/id/1", EnquiryEventType.EnquiryNewCommentAdded},
            {"http://www.mydrilling.com/repository/process/id/13.1/subscription/id/2/event/id/1", EnquiryEventType.EnquiryAssignedToRig},
            {"http://www.mydrilling.com/repository/process/id/13.1/subscription/id/2/event/id/2", EnquiryEventType.EnquiryAssignedToProvider},
            {"http://www.mydrilling.com/repository/process/id/13.1/subscription/id/2/event/id/3", EnquiryEventType.EnquiryDraftAssignedToRig}
        };

        private static readonly Dictionary<string, UpgradeMatrixEventType> UpgradeMatrixEventMappings = new Dictionary<string, UpgradeMatrixEventType>
        {
            {"http://www.mydrilling.com/repository/process/id/13.1/subscription/id/5/event/id/1", UpgradeMatrixEventType.NewItemAvailable}
        };

        private static readonly Dictionary<string, SubscriptionType> SubscriptionSettingMappings = new Dictionary<string, SubscriptionType>
        {
            {"http://www.mydrilling.com/repository/process/id/13.1/subscription/id/5", SubscriptionType.NewUpgradeMatrixItemAvailable },
            {"http://www.mydrilling.com/repository/process/id/13.1/subscription/id/3", SubscriptionType.DraftEnquiryCreatedByMeDeleted },
            {"http://www.mydrilling.com/repository/process/id/13.1/subscription/id/1", SubscriptionType.EnquiryAssignedToMe },
            {"http://www.mydrilling.com/repository/process/id/13.2/subscription/id/2", SubscriptionType.EnquiryRigMovedCreated },
            {"http://www.mydrilling.com/repository/process/id/13.2/subscription/id/1", SubscriptionType.EnquiryRigOnDowntimeCreated },
            {"http://www.mydrilling.com/repository/process/id/13.1/subscription/id/4", SubscriptionType.EnquiryCommentAdded },
            {"http://www.mydrilling.com/repository/process/id/30.1/subscription/id/1", SubscriptionType.EnquiryIAmInvolvedInCommentAdded },
            {"http://www.mydrilling.com/repository/process/id/30.1/subscription/id/4", SubscriptionType.EnquiryRigOnDowntimeCommentAdded },
            {"http://www.mydrilling.com/repository/process/id/13.1/subscription/id/2", SubscriptionType.EnquiryAssignedToMyOrganization }
        };

        private const string RigFilter = "http://www.mydrilling.com/repository/process/id/30.1/subscription/id/2";
        private const string EnquiryTypeFilter = "http://www.mydrilling.com/repository/process/id/30.1/subscription/id/3";
        private const string EnabledState = "http://www.mydrilling.com/repository/process/id/13.1/state/id/1";
        private const string DisabledState = "http://www.mydrilling.com/repository/process/id/13.1/state/id/2";

        public static void Do(string oldServerConnectionString, string newServerConnectionString)
        {
            Log.Logger.Information("Importing subscription settings...");

            SIB_SubscriptionMessageTemplate[] templates;
            Dictionary<string, SIB_SubscriptionStaticSubscriber[]> staticSubscribers;
            Dictionary<string, Fact_Subscription_Setting_V1_0[]> settings;
            Dictionary<string, Fact_Subscription_Setting_Option[]> options;

            using (var conn = new SqlConnection(oldServerConnectionString.GetDwhConnectionString()))
            {
                settings = conn.Query<Fact_Subscription_Setting_V1_0>("select * from Fact_Subscription_Setting_V1_0")
                    .GroupBy(x => x.UPN, CommonStringExt.MdStringComparer).ToDictionary(x => x.Key, x => x.ToArray());
                options = conn.Query<Fact_Subscription_Setting_Option>("select * from Fact_Subscription_Setting_Option")
                    .GroupBy(x => x.UPN, CommonStringExt.MdStringComparer).ToDictionary(x => x.Key, x => x.ToArray());
            }

            using (var conn = new SqlConnection(oldServerConnectionString.GetStage1ConnectionString()))
            {
                templates = conn.Query<SIB_SubscriptionMessageTemplate>("select * from SIB_SubscriptionMessageTemplate").ToArray();
                staticSubscribers = conn.Query<SIB_SubscriptionStaticSubscriber>("select * from SIB_SubscriptionStaticSubscriber")
                    .GroupBy(x => x.Email).ToDictionary(x => x.Key, x => x.ToArray());
            }

            using (var newDb = newServerConnectionString.GetNewMyDrillingDbContext())
            {
                var newRigMappings = newDb.Rigs.Select(x => new {x.ReferenceId, x.Id})
                    .ToDictionary(x => x.ReferenceId, x => x.Id);
                var newUserMappings = newDb.Users.Select(x => new { x.Upn, x.Id })
                    .ToDictionary(x => x.Upn, x => x.Id, CommonStringExt.MdStringComparer);

                //templates
                foreach (var template in templates)
                {
                    if (EnquiryEventMappings.TryGetValue(template.EventType, out var enquiryEventType))
                    {
                        newDb.Enquiry_MessageTemplates.Add(new MessageTemplate
                        {
                            EnquiryEventType = enquiryEventType,
                            Subject = template.TitleTemplate,
                            Body = template.BodyTemplate
                        });
                    }

                    if (UpgradeMatrixEventMappings.TryGetValue(template.EventType, out var umEventType))
                    {
                        newDb.UpgradeMatrix_MessageTemplates.Add(new Core.Entities.UpgradeMatrix.MessageTemplate
                        {
                            UpgradeMatrixEventType = umEventType,
                            Subject = template.TitleTemplate,
                            Body = template.BodyTemplate
                        });
                    }
                }

                //static subscribers
                var staticSubscriberMapping = new Dictionary<string, StaticSubscriber>();
                foreach (var ss in staticSubscribers)
                {
                    var newSs = new StaticSubscriber
                    {
                        Email = ss.Value.First().Email.Trim().ToLower(),
                        FirstName = ss.Value.First().FirstName,
                        LastName = ss.Value.First().LastName
                    };
                    newDb.Subscription_StaticSubscribers.Add(newSs);
                    staticSubscriberMapping[newSs.Email] = newSs;

                    foreach (var subscriptionType in ss.Value.Select(x => x.SubscriptionType).Distinct())
                    {
                        newDb.Subscription_StaticSubscriberSettings.Add(new StaticSubscriberSetting
                        {
                            StaticSubscriber = newSs,
                            SubscriptionType = SubscriptionSettingMappings[subscriptionType],
                            Created = DateTime.UnixEpoch
                        });
                    }
                }

                //settings
                foreach (var setting in settings)
                {
                    if (newUserMappings.TryGetValue(setting.Key, out var userId))
                    {
                        foreach (var val in setting.Value.GroupBy(x => x.Subscription_Type).Select(x => x.Last()))
                        {
                            if (val.STATE == DisabledState)
                            {
                                continue;
                            }

                            newDb.Subscription_UserSettings.Add(new UserSetting
                            {
                                UserId = userId,
                                SubscriptionType = SubscriptionSettingMappings[val.Subscription_Type],
                                Created = val.INSERT_DATE
                            });
                        }
                    }
                }

                foreach (var option in options)
                {
                    //user options
                    if (newUserMappings.TryGetValue(option.Key, out var userId))
                    {
                        foreach (var userOpt in option.Value.GroupBy(x => x.Subscription_Option_Type).Select(x => x.Last()))
                        {
                            if (userOpt.VALUE == DisabledState)
                            {
                                continue;
                            }

                            //rig
                            if (userOpt.Subscription_Option_Type == RigFilter)
                            {
                                newDb.Subscription_RigFilters.Add(new RigFilter
                                {
                                    UserId = userId,
                                    RigId = newRigMappings[userOpt.VALUE.ToLongReferenceId()],
                                    Created = userOpt.INSERT_DATE
                                });
                                continue;
                            }

                            //enquiry type
                            if (userOpt.Subscription_Option_Type == EnquiryTypeFilter)
                            {
                                newDb.Subscription_EnquiryTypeFilters.Add(new EnquiryTypeFilter
                                {
                                    UserId = userId,
                                    Type = (TypeInfo.Type)int.Parse(Path.GetFileName(userOpt.VALUE)),
                                    Created = userOpt.INSERT_DATE
                                });
                                continue;
                            }

                            //others
                            newDb.Subscription_UserSettings.Add(new UserSetting
                            {
                                UserId = userId,
                                SubscriptionType = SubscriptionSettingMappings[userOpt.Subscription_Option_Type],
                                Created = userOpt.INSERT_DATE
                            });
                        }
                    }

                    //static subscribers options
                    if (staticSubscriberMapping.TryGetValue(option.Key.Trim().ToLower(), out var newSs))
                    {
                        foreach (var userOpt in option.Value.GroupBy(x => x.Subscription_Option_Type).Select(x => x.Last()))
                        {
                            if (userOpt.VALUE == DisabledState)
                            {
                                continue;
                            }

                            //rig
                            if (userOpt.Subscription_Option_Type == RigFilter)
                            {
                                newDb.Subscription_StaticSubscriberRigFilters.Add(new StaticSubscriberRigFilter
                                {
                                    StaticSubscriber = newSs,
                                    RigId = newRigMappings[userOpt.VALUE.ToLongReferenceId()],
                                    Created = userOpt.INSERT_DATE
                                });
                                continue;
                            }

                            //enquiry type
                            if (userOpt.Subscription_Option_Type == EnquiryTypeFilter)
                            {
                                newDb.Subscription_StaticSubscriberEnquiryTypeFilters.Add(new StaticSubscriberEnquiryTypeFilter
                                {
                                    StaticSubscriber = newSs,
                                    Type = (TypeInfo.Type)int.Parse(Path.GetFileName(userOpt.VALUE)),
                                    Created = userOpt.INSERT_DATE
                                });
                                continue;
                            }

                            //others
                            newDb.Subscription_StaticSubscriberSettings.Add(new StaticSubscriberSetting
                            {
                                StaticSubscriber = newSs,
                                SubscriptionType = SubscriptionSettingMappings[userOpt.Subscription_Option_Type],
                                Created = userOpt.INSERT_DATE
                            });
                        }
                    }
                }

                newDb.SaveChanges();
            }

            Log.Logger.Information("Imported subscription settings");
        }

        // ReSharper disable InconsistentNaming

        public class SIB_SubscriptionMessageTemplate
        {
            public string ID;
            public string EventType;
            public string ChannelType;
            public string TitleTemplate;
            public string BodyTemplate;
        }

        public class SIB_SubscriptionStaticSubscriber
        {
            public int PK_SIB_SubscriptionStaticSubscriber;
            public string Email;
            public string FirstName;
            public string LastName;
            public string SubscriptionType;
        }

        public class Fact_Subscription_Setting_V1_0
        {
            public int PK_Subscription;
            public string GLOBAL_ID;
            public string REFERENCE_ID;
            public string UPN;
            public string Subscription_Type;
            public string SendWhen;
            public DateTime INSERT_DATE;
            public string INSERT_BY;
            public DateTime? LAST_UPDATED;
            public string SOURCE_ID;
            public string ORIGINSOURCE_ID;
            public string STATE;
            public string CONTEXT;
            public string QUALITY;
        }

        public class Fact_Subscription_Setting_Option
        {
            public int PK_Subscription_Option;
            public string UPN;
            public string Subscription_Option_Type;
            public DateTime INSERT_DATE;
            public string INSERT_BY;
            public DateTime? LAST_UPDATED;
            public string VALUE;
        }

        // ReSharper restore InconsistentNaming
    }
}
