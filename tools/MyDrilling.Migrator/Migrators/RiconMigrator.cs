﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Dapper;
using Microsoft.Data.SqlClient;
using MyDrilling.Core;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Ricon;
using MyDrilling.Core.Entities.Rig;
using Serilog;

namespace MyDrilling.Migrator.Migrators
{
    public static class RiconMigrator
    {
        public static void Do(string oldServerConnectionString, string newServerConnectionString, bool insertAll)
        {
            Log.Logger.Information("Importing Ricon data...");

            RigEquipmentParentAllowed[] allowedParents;
            EquipmentFatigue[] equipmentFatigues;
            EquipmentDocument[] equipmentDocuments;

            using (var conn = new SqlConnection(oldServerConnectionString.GetDwhConnectionString()))
            {
                allowedParents = conn.Query<RigEquipmentParentAllowed>("select * from Rig_Equipment_Parent_Allowed").ToArray();
                equipmentFatigues = conn.Query<EquipmentFatigue>(@"select DISTINCT FACILITY_REFERENCE_ID, EQUIPMENT_REFERENCE_ID, RISER_COC, 
                                                                    RISER_VI_NEXT_INSPECTION_DATE, RISER_TH_NEXT_INSPECTION_DATE, 
                                                                    RISER_THICKNESS_STATUS, FATIGUE, TAG_NUMBER
                                                                    from FACT_Equipment_Fatigue where Last_Version=1").ToArray();
                equipmentDocuments = conn.Query<EquipmentDocument>("select * from FACT_Equipment_Document where Last_Version=1").ToArray();
                
            }

            using (var newDb = newServerConnectionString.GetNewMyDrillingDbContext())
            {
                foreach (var parent in allowedParents)
                {
                    Rig rig = newDb.Rigs.FirstOrDefault(r => r.ReferenceId == parent.Rig_Global_Id.ToLongReferenceId());
                    if (rig == null)
                    {
                        Log.Logger.Warning($"Rig '{parent.Rig_Global_Id}' does not exist.");
                        continue;
                    }

                    if (!newDb.Ricon_RootEquipments.Any(r => r.RigId == rig.Id && r.ReferenceId == parent.Equipment_Global_Id.ToLongReferenceId()))
                    {
                        newDb.Ricon_RootEquipments.Add(new RootEquipment
                        {
                            RigId = rig.Id,
                            ReferenceId = long.Parse(parent.Equipment_Global_Id)
                        });
                    }
                }
                newDb.SaveChanges();
                if (insertAll)
                {
                    foreach (var eqFatigue in equipmentFatigues)
                    {

                        var rootEquipment = newDb.Ricon_RootEquipments.FirstOrDefault(r => r.Rig.ReferenceId == eqFatigue.FACILITY_REFERENCE_ID.ToLongReferenceId());
                        if (rootEquipment == null || !long.TryParse(eqFatigue.EQUIPMENT_REFERENCE_ID, out long eqReference) || eqReference == 0)
                        {
                            Log.Logger.Warning($"Equipment '{eqFatigue.EQUIPMENT_REFERENCE_ID}' wasn't imported.");
                            continue;
                        }
                        if (!newDb.Ricon_Joints.Any(e => e.RootEquipmentId == rootEquipment.Id && e.ReferenceId == eqReference))
                        {
                            newDb.Ricon_Joints.Add(new Joint
                            {
                                RootEquipmentId = rootEquipment.Id,
                                ReferenceId = eqReference,
                                TagNumber = eqFatigue.TAG_NUMBER,
                                Fatigue = eqFatigue.FATIGUE,
                                Coc = eqFatigue.RISER_COC,
                                Status = eqFatigue.RISER_THICKNESS_STATUS,
                                LastVisualInspection = eqFatigue.RISER_VI_NEXT_INSPECTION_DATE,
                                LastWallThicknessInspection = eqFatigue.RISER_TH_NEXT_INSPECTION_DATE
                            });
                        }
                        else
                        {
                            Log.Logger.Information($"Equipment '{eqFatigue.EQUIPMENT_REFERENCE_ID}' already exists.");

                        }
                    }
                    newDb.SaveChanges();
                    foreach (var eqDocument in equipmentDocuments)
                    {
                        if (!long.TryParse(eqDocument.EQUNR, out long eqReference)) continue;
                        var rootEquipment = newDb.Ricon_RootEquipments.FirstOrDefault(r => r.ReferenceId == eqReference);
                        var newDocument = new Document
                        {
                            ReferenceId = eqDocument.DOKNR.ToLongReferenceId(),
                            Version = eqDocument.DOKVR,
                            Url = $"SAP|{eqDocument.DOKAR}|{eqDocument.DOKNR.ToLongReferenceId()}|{eqDocument.DOKTL}|{eqDocument.DOKVR}",
                            Description = eqDocument.DESCRIPTION,
                            PublishedDate = eqDocument.INSERT_DATE.HasValue ? eqDocument.INSERT_DATE.Value : DateTime.UtcNow
                        };
                        if (rootEquipment != null)
                        {
                            //document related to root equipment, global for rig
                            newDocument.RootEquipmentId = rootEquipment.Id;
                            newDb.Ricon_Documents.Add(newDocument);
                        }
                        else
                        {
                            var equipment = newDb.Ricon_Joints.FirstOrDefault(r => r.ReferenceId == eqReference);
                            if (equipment != null)
                            {
                                //document related to risers
                                newDocument.JointId = equipment.Id;
                                newDocument.JointReferenceId = eqReference;
                                newDb.Ricon_Documents.Add(newDocument);
                            }
                            else
                            {
                                Log.Logger.Warning($"Document '{eqDocument.DOKNR}' wasn't imported. Equipment '{eqDocument.EQUNR}' does not exist in RiconRootEquipment either RiconEquipment.");
                            }
                        }
                    }
                    newDb.SaveChanges();

                }

            }
            Log.Logger.Information("Imported Ricon data");
        }
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class RigEquipmentParentAllowed
    {
        public string Rig_Global_Id;
        public string Equipment_Global_Id;
        public int Documents_Count;
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class EquipmentFatigue
    {
        public string FACILITY_REFERENCE_ID;
        public string EQUIPMENT_REFERENCE_ID; 
        public string RISER_COC;
        public DateTime? RISER_VI_NEXT_INSPECTION_DATE;
        public DateTime? RISER_TH_NEXT_INSPECTION_DATE;
        public string RISER_THICKNESS_STATUS;
        public string FATIGUE;
        public string TAG_NUMBER;
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class EquipmentDocument
    {
        public string GLOBAL_ID;
        public string EQUNR;
        public string DESCRIPTION;
        public string DOKNR;
        public string DOKAR;
        public string DOKVR;
        public string DOKTL;
        public DateTime? INSERT_DATE;
    }
}
