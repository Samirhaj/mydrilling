﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Dapper;
using EFCore.BulkExtensions;
using Microsoft.Data.SqlClient;
using MyDrilling.Core;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Equipment;
using Serilog;

namespace MyDrilling.Migrator.Migrators
{
    public static class EquipmentMigrator
    {
        public static void Do(string oldServerConnectionString, string newServerConnectionString, bool insert)
        {
            Log.Logger.Information("Importing equipments...");

            DimEquipment[] dimEquipments;
            Dictionary<int, FactFunctionalLocation> funcLocations;
            Dictionary<int, FactFunctionalLocation> funcLocationsHistory;
            Dictionary<int, string> oldRigs;
            Dictionary<int, string> oldCustomers;
            Dictionary<string, ProductMigrator.DimProduct> dimProducts;

            using (var conn = new SqlConnection(oldServerConnectionString.GetDwhConnectionString()))
            {
                dimEquipments = conn.Query<DimEquipment>("select * from dim_equipment").ToArray();
                funcLocations = conn.Query<FactFunctionalLocation>("select * from Fact_FunctionalLocation").GroupBy(x => x.Dim_Equipment_FK)
                    .ToDictionary(x => x.Key, x => x.First());
                funcLocationsHistory = conn.Query<FactFunctionalLocation>(@"
;WITH cte AS
(
   SELECT *,
         ROW_NUMBER() OVER (PARTITION BY Dim_Equipment_FK ORDER BY PK_Fact_FunctionalLocation DESC) AS rn
   FROM Fact_FunctionalLocation_History
)
SELECT *
FROM cte
WHERE rn = 1").ToDictionary(x => x.Dim_Equipment_FK, x => x);
                oldRigs = conn.Query<DimRig>("select PK_Dim_Rig, GLOBAL_ID from dim_rig").ToDictionary(x => x.PK_Dim_Rig, x => x.GLOBAL_ID);
                oldCustomers = conn.Query<DimCustomer>("select PK_Dim_Customer, GLOBAL_ID from dim_customer").ToDictionary(x => x.PK_Dim_Customer, x => x.GLOBAL_ID);
                dimProducts = conn.Query<ProductMigrator.DimProduct>("select * from dim_product where product_id is not null").GroupBy(x => x.Product_Id)
                    .ToDictionary(x => x.Key, x => x.First());
            }

            //remove duplications
            dimEquipments = dimEquipments.GroupBy(x => x.REFERENCE_ID).Select(x => x.First()).ToArray();

            using (var newDb = newServerConnectionString.GetNewMyDrillingDbContext())
            {
                var newRigs = newDb.Rigs.ToDictionary(x => x.ReferenceId, x => x);
                var newCustomers = newDb.Customers.Where(x => !string.IsNullOrEmpty(x.ReferenceId)).ToDictionary(x => x.ReferenceId, x => x);
                Dictionary<string, Equipment> existingEquipments = null;
                if (!insert)
                {
                    existingEquipments = newDb.Equipments.Where(x => x.ProductLongReferenceId == "FOR_MIGRATOR").ToDictionary(x => x.ReferenceId.ToStringReferenceId(18), x => x);
                }

                var newEquipments = new List<Equipment>();
                foreach (var dimEquipment in dimEquipments)
                {
                    var equipment = insert
                        ? new Equipment
                            {
                                ReferenceId = dimEquipment.REFERENCE_ID.ToLongReferenceId(),
                                Name = dimEquipment.Equipment_Name,
                                SerialNumber = dimEquipment.Equipment_SerialNo,
                                TagNumber = dimEquipment.Equipment_TagNo,
                                Material = dimEquipment.Equipment_Material,
                                Created = dimEquipment.insert_date,
                                Modified = dimEquipment.update_date ?? dimEquipment.insert_date,
                                ProductCode = dimEquipment.Equipment_Norsok_Short,
                                ParentReferenceId = dimEquipment.Parent.ToNullableLongReferenceId(),
                                IsDeleted = true
                            }
                        : existingEquipments.TryGetValue(dimEquipment.REFERENCE_ID, out _)
                            ? existingEquipments[dimEquipment.REFERENCE_ID]
                            : null;

                    if (insert)
                    {
                        newEquipments.Add(equipment);

                        //set actual rigs from func locations
                        if (funcLocations.TryGetValue(dimEquipment.PK_Dim_Equipment, out var fl))
                        {
                            equipment.IsDeleted = false;
                            if (fl.Dim_Rig_FK.HasValue)
                            {
                                equipment.RigId = newRigs[oldRigs[fl.Dim_Rig_FK.Value].ToLongReferenceId()].Id;
                                equipment.RigOwnerId =
                                    newRigs[oldRigs[fl.Dim_Rig_FK.Value].ToLongReferenceId()].OwnerId;
                            }

                            continue;
                        }

                        //set outdated rigs from func locations history
                        if (funcLocationsHistory.TryGetValue(dimEquipment.PK_Dim_Equipment, out var flh))
                        {
                            if (flh.Dim_Rig_FK.HasValue)
                            {
                                equipment.RigId = newRigs[oldRigs[flh.Dim_Rig_FK.Value].ToLongReferenceId()].Id;
                                equipment.RigOwnerId = newCustomers[oldCustomers[flh.Dim_Customer_FK]].Id;
                            }
                        }

                        continue;
                    }

                    if (equipment != null)
                    {
                        if (!string.IsNullOrEmpty(dimEquipment.Equipment_Norsok_Short) && dimProducts.TryGetValue(dimEquipment.Equipment_Norsok_Short, out var product))
                        {
                            equipment.ProductName = product.Product_Name?.ToUpper();
                            equipment.ProductGroup = product.Product_Group?.ToUpper();
                        }

                        equipment.ProductCode = null;

                        equipment.ProductLongReferenceId = dimEquipment.Equipment_Norsok_Long;
                        equipment.ProductShortReferenceId = string.IsNullOrEmpty(dimEquipment.Equipment_Norsok_Short) 
                            ? dimEquipment.Equipment_Norsok_Long?.Substring(0, 4)
                            : dimEquipment.Equipment_Norsok_Short;

                        if (funcLocationsHistory.TryGetValue(dimEquipment.PK_Dim_Equipment, out var flh))
                        {
                            if (flh.Dim_Rig_FK.HasValue)
                            {
                                equipment.RigReferenceId = newRigs[oldRigs[flh.Dim_Rig_FK.Value].ToLongReferenceId()].ReferenceId;
                                equipment.RigOwnerReferenceId = newCustomers[oldCustomers[flh.Dim_Customer_FK]].ReferenceId;
                            }
                        }
                    }
                }

                if (newEquipments.Count > 0)
                {
                    newDb.BulkInsert(newEquipments);
                }

                newDb.SaveChanges();
            }

            Log.Logger.Information("Imported equipments");
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class DimEquipment
        {
            public int PK_Dim_Equipment;
            public string GLOBAL_ID;
            public string PARENT_GLOBAL_ID;
            public string REFERENCE_ID;
            public string Equipment_Id;
            public string Equipment_Name;
            public string Equipment_Material;
            public string Equipment_SerialNo;
            public string Equipment_TagNo;
            public string Equipment_Norsok_Long;
            public string Equipment_Norsok_Short;
            public string Equipment_Type;
            public string Parent;
            public int Equipment_Level;
            public int Equipment_Visible;
            public string SOURCE_ID;
            public DateTime? valid_from_date;
            public DateTime? valid_to_date;
            public DateTime insert_date;
            public DateTime? update_date;
            public string Equipment_Path;
            public string Rig_Global_ID;
        }

        public class FactFunctionalLocation
        {
            public int Dim_Customer_FK;
            public int? Dim_Rig_FK;
            public int Dim_Product_FK;
            public int Dim_Equipment_FK;
        }

        public class DimRig
        {
            public int PK_Dim_Rig;
            public string GLOBAL_ID;
        }


        public class DimCustomer
        {
            public int PK_Dim_Customer;
            public string GLOBAL_ID;
        }
    }
}
