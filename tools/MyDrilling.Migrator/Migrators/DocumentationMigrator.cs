﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using DocumentFormat.OpenXml.Office2016.Drawing.ChartDrawing;
using EFCore.BulkExtensions;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Documentation;
using Serilog;

namespace MyDrilling.Migrator.Migrators
{
    public static class DocumentationMigrator
    {
        private static Hashtable _newRigs;
        private static Hashtable _productNames;

        public static void Do(string oldServerConnectionString, string newServerConnectionString, bool insertAll)
        {
            Log.Logger.Information("Importing Documentation data...");

            _newRigs = new Hashtable();
            _productNames = new Hashtable();

            using (var newDb = newServerConnectionString.GetNewMyDrillingDbContext())
            {
                foreach (var rig in newDb.Rigs)
                {
                    _newRigs.Add(rig.ReferenceId, rig.Id);
                }
            }

            using (var conn = new SqlConnection(oldServerConnectionString.GetDwhConnectionString()))
            {
                var dwhProductNames = conn.Query<ProductName>(@"SELECT distinct
                      [Product_Id] as 'Product_Code'
                      ,[Product_Name] as 'Product_Name'     
                  FROM [dbo].[Dim_Product]
                  where Product_Id is not null
                  order by Product_Id").ToArray();
                foreach (var product in dwhProductNames)
                {
                    _productNames.Add(product.Product_Code, product.Product_Name);
                }
            }

            UpdateRigsWithDocumentationFlag(oldServerConnectionString, newServerConnectionString);
            if (insertAll)
            {
                ImportTechnicalReports(oldServerConnectionString, newServerConnectionString);
                ImportUserManuals(oldServerConnectionString, newServerConnectionString);
            }
            Log.Logger.Information("Imported Documentation data");
        }

        private static void UpdateRigsWithDocumentationFlag(string oldServerConnectionString, string newServerConnectionString)
        {
            Log.Logger.Information("Import Documentation flag updating...");
            string[] rigsToImportDocumentation;

            using (var stg1Connection = new SqlConnection(oldServerConnectionString.GetStage1ConnectionString()))
            {
                rigsToImportDocumentation = stg1Connection.Query<string>(@"SELECT [Rig_GLOBAL_ID]                                                
                                                FROM [DWH_Rig_LookUp_Table]
                                                where Flag_Import_Documentation = 'YES'").ToArray();
            }

            using (var newDb = newServerConnectionString.GetNewMyDrillingDbContext())
            {
                foreach (var rigRef in rigsToImportDocumentation)
                {
                    var newRig = newDb.Rigs.FirstOrDefault(x => x.ReferenceId == rigRef.ToLongReferenceId());
                    if (newRig != null)
                    {
                        newRig.ImportDocumentation = true;
                    }
                    else
                    {
                        Log.Logger.Information($"Rig '{rigRef}' is not present in new database.");
                    }
                }
                newDb.SaveChanges();
            }
            Log.Logger.Information("Import Documentation flag updated.");
        }

        private static void ImportTechnicalReports(string oldServerConnectionString, string newServerConnectionString)
        {
            Log.Logger.Information("Importing Technical reports data...");

            TechnicalReportDoc[] technicalReports;
            using (var conn = new SqlConnection(oldServerConnectionString.GetDwhConnectionString()))
            {
                technicalReports = conn.Query<TechnicalReportDoc>(@"SELECT distinct 
                    case
                    when substring(Document_File_Extension, 1, 1) LIKE '%[0-9]%' then n.[REFERENCE_ID] + '.pdf'
                    else n.[REFERENCE_ID]
                    end as 'Doc_REFERENCE_ID'
                    , case
                    when substring(Document_File_Extension, 1, 1) LIKE '%[0-9]%' then n.[REFERENCE_ID]
                    else n.[Document_Id]
                    end as 'Document_Id'
                    , n.Global_ID
                    , r.Reference_ID as 'Rig_reference'
                    , p.Product_Id                                    
                    , [Document_Description]
                    , [Document_Date]
                    , [Document_Created_By]
                    , case
                    when substring(Document_File_Extension, 1, 1) LIKE '%[0-9]%' then n.[Document_Url] + '.pdf'
                    else n.[Document_Url]
                    end as 'Document_Url'
                    , [Document_File_Extension]
                    , n.[insert_date]
                    , n.[update_date]
                    FROM (SELECT ROW_NUMBER()
                    OVER(PARTITION BY (case
                    when substring(Document_File_Extension, 1, 1) LIKE '%[0-9]%' then [REFERENCE_ID]
                    else [Document_Id]
                    end)
                    ORDER BY Reference_Id DESC) AS StRank, *
                    FROM [dbo].[Dim_Document] 
                    where Document_Area = 'technical report'
                    and PK_Dim_Document in (SELECT distinct [Dim_Document_FK]    
                        FROM [dbo].[Fact_Documentation]
                    )) n 
                    left join dbo.fact_Documentation fd on fd.Dim_Document_FK = n.PK_Dim_Document
                    left join dbo.Dim_Rig r on r.PK_Dim_Rig = fd.Dim_Rig_FK
                    left join dbo.Dim_Product p on p.PK_Dim_Product = fd.Dim_Product_FK
                    where StRank = 1                   
                    order by [Document_Date]").ToArray();
            }

            using (var newDb = newServerConnectionString.GetNewMyDrillingDbContext())
            {
                var newTechnicalReports = new List<TechnicalReport>();
                foreach (var techReport in technicalReports)
                {
                    var rigReferenceId = techReport.Rig_reference.ToLongReferenceId();
                    var rigId = _newRigs.Contains(rigReferenceId) ? (long)_newRigs[rigReferenceId] : 0;
                    if (rigId == 0)
                    {
                        Log.Logger.Warning($"Rig '{rigReferenceId}' does not exist.");
                        continue;
                    }

                    string productName = string.Empty;
                    var productCode = techReport.Product_Id;
                    if (!string.IsNullOrEmpty(productCode))
                    {
                        productName = _productNames.Contains(productCode) ? (string)_productNames[productCode] : "";
                    }

                    if (int.TryParse(techReport.Document_File_Extension.Substring(0, 1), out int extNumber) && extNumber > 0)
                    {
                        Log.Logger.Warning($"Document '{techReport.Doc_REFERENCE_ID}' does not have correct file extension.");
                    }

                    //check if document has been already added
                    if (!newTechnicalReports.Any(tr =>
                        tr.ReferenceId == techReport.Document_Id
                        && tr.Name == techReport.Document_Description.Trim()
                        && tr.FileName == techReport.Doc_REFERENCE_ID
                        && tr.RigReferenceId == rigReferenceId
                        && (string.IsNullOrEmpty(productCode) || tr.ProductCode == techReport.Product_Id)
                        && tr.DocumentDate == techReport.Document_Date
                        && tr.Url == techReport.Document_Url
                    ))
                    {
                        var newTechReport = new TechnicalReport()
                        {
                            ReferenceId = techReport.Document_Id,
                            Name = techReport.Document_Description.Trim(),
                            FileName = techReport.Doc_REFERENCE_ID,
                            RigId = rigId,
                            RigReferenceId = rigReferenceId,
                            ProductCode = productCode,
                            ProductName = productName,
                            Url = techReport.Document_Url,
                            Author = techReport.Document_Created_By,
                            DocumentDate = techReport.Document_Date,
                            Created = techReport.insert_date,
                            Modified = techReport.update_date
                        };
                        newTechnicalReports.Add(newTechReport);
                    }
                    else
                    {
                        Log.Logger.Warning($"Document '{techReport.Doc_REFERENCE_ID}' already exists.");
                    }
                }
                newDb.BulkInsert(newTechnicalReports);
            }
            Log.Logger.Information("Imported Technical reports...");
        }
        private static void ImportUserManuals(string oldServerConnectionString, string newServerConnectionString)
        {
            Log.Logger.Information("Importing User manuals...");

            UserManualDocument[] userManuals;

            using (var conn = new SqlConnection(oldServerConnectionString.GetStage2ConnectionString()))
            {
                userManuals = conn.Query<UserManualDocument>(@"SELECT distinct 
                                                    [rig_no]     
                                                    ,[doc_drawing]
	                                                ,[master_doc_drawing]
                                                    ,[doc_description]
                                                    ,SUBSTRING(product_code, 1, 4) as 'product_code'	 
                                                    ,[revision_date]     
                                                    ,[url]
                                                    ,[discipline_code]
                                                    ,[discipline_description]                                             
                                                    ,[modified_date]
                                                    FROM (SELECT ROW_NUMBER()       
	                                                OVER(PARTITION BY doc_drawing, master_doc_drawing
                                                    ORDER BY modified_date DESC) AS StRank, *
                                                    FROM [dbo].[Stg2_UserManuals]
                                                    ) n  		
                                                    where 1=1
	                                                and StRank = 1
                                                    and url not like 'http://apps.enet/%'
	                                                order by [doc_drawing], [master_doc_drawing]").ToArray();


            }

            using (var newDb = newServerConnectionString.GetNewMyDrillingDbContext())
            {
                var newUserManuals = new List<UserManual>();
                var newUserManualsRef = new HashSet<string>();
                foreach (var userMan in userManuals)
                {
                    try
                    {
                        if (!newUserManualsRef.Contains(GetUserManualReference(userMan)))
                        {
                            if (!UpdateUserManual(newUserManuals, newUserManualsRef, userMan))
                            {
                                Log.Warning($"Failed on adding document {userMan.doc_drawing}.");
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Log.Error(e, $"Failed on adding document {userMan.doc_drawing} with its related document {userMan.master_doc_drawing}.");
                    }
                }

                if (newUserManuals.Count > 0)
                {
                    newDb.BulkInsert(newUserManuals);
                }
            }

            Log.Logger.Information("Imported User manuals...");
        }

        private static bool UpdateUserManual(List<UserManual> newUserManuals, HashSet<string> newUserManualsRef, UserManualDocument userMan)
        {
            var rigReferenceId = userMan.rig_no.ToLongReferenceId();

            long rigId = _newRigs.Contains(rigReferenceId) ? (long)_newRigs[rigReferenceId] : 0;
            if (rigId == 0)
            {
                Log.Logger.Warning($"Rig '{rigReferenceId}' used in document '{userMan.doc_drawing}' does not exist.");
                return false;
            }

            string productName = string.Empty;
            var productCode = userMan.product_code;
            if (!string.IsNullOrEmpty(productCode))
            {
                productName = _productNames.Contains(productCode) ? (string)_productNames[productCode] : "";
            }

            var userManRef = GetUserManualReference(userMan);
            if (!newUserManualsRef.Contains(userManRef))
            {
                var userManDb = new UserManual
                {
                    ReferenceId = userMan.doc_drawing,
                    Name = userMan.doc_description.Trim(),
                    Discipline = userMan.discipline_description,
                    DisciplineCode = userMan.discipline_code,
                    RigId = rigId,
                    RigReferenceId = rigReferenceId,
                    ProductCode = productCode,
                    ProductName = productName,
                    RelatedReferenceId = !string.IsNullOrEmpty(userMan.master_doc_drawing) ? userMan.master_doc_drawing : null,
                    Url = userMan.url,
                    //FileName = userMan.Reference_Id + (string.IsNullOrEmpty(userMan.Document_File_Extension)
                    //               ? ""
                    //               : "." + userMan.Document_File_Extension.ToLower()),
                    DocumentDate = userMan.revision_date,
                    IsMaster = string.IsNullOrEmpty(userMan.master_doc_drawing),
                    Created = userMan.revision_date,
                    Modified = userMan.modified_date
                };
                newUserManuals.Add(userManDb);
                newUserManualsRef.Add(userManRef);
            }

            return true;
        }
        private static string GetUserManualReference(UserManualDocument userMan)
        {
            return userMan.doc_drawing + '-' + userMan.master_doc_drawing;
        }
    }

    public class TechnicalReportDoc
    {
        public string Doc_REFERENCE_ID { get; set; }
        public string Document_Id { get; set; }
        public string Rig_reference { get; set; }
        public string Product_Id { get; set; }
        public string Document_Description { get; set; }
        public DateTime Document_Date { get; set; }
        public string Document_Created_By { get; set; }
        public string Document_Url { get; set; }
        public string Document_File_Extension { get; set; }
        public DateTime insert_date { get; set; }
        public DateTime update_date { get; set; }
    }

    public class UserManualDocument
    {
        public string doc_drawing { get; set; }
        public string doc_description { get; set; }
        public string rig_no { get; set; }
        public string master_doc_drawing { get; set; }
        public string product_code { get; set; }
        public string discipline_description { get; set; }
        public string discipline_code { get; set; }
        public DateTime revision_date { get; set; }
        public string url { get; set; }
        public DateTime modified_date { get; set; }
    }

    public class ProductName
    {
        public string Product_Code { get; set; }
        public string Product_Name { get; set; }
    }
}
