﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Dapper;
using Microsoft.Data.SqlClient;
using MyDrilling.Core;
using MyDrilling.Core.Entities;
using Serilog;

namespace MyDrilling.Migrator.Migrators
{
    public static class FunctionalLocationLogMigrator
    {
        public static void Do(string oldServerConnectionString, string newServerConnectionString)
        {
            Log.Logger.Information("Importing functional locations history...");

            Dictionary<int, FactFunctionalLocationHistory[]> factFuncLocationHistories;
            Dictionary<int, string> dimCustomers;
            Dictionary<int, string> dimRigs;
            Dictionary<int, string> dimProducts;
            Dictionary<int, string> dimEquipments;

            using (var conn = new SqlConnection(oldServerConnectionString.GetDwhConnectionString()))
            {
                factFuncLocationHistories = conn.Query<FactFunctionalLocationHistory>(@"
select Dim_Customer_FK, Dim_Rig_FK, p.Product_Id, Dim_Equipment_FK, EFFECTIVE_DATE, flh.INSERT_DATE
from Fact_FunctionalLocation_History flh
join dim_product p on flh.Dim_Product_Fk = p.PK_dim_product")
                    .GroupBy(x => x.Dim_Equipment_FK).ToDictionary(x => x.Key, x => x.OrderBy(y => y.EFFECTIVE_DATE).ToArray());
                dimCustomers = conn.Query<CustomerMigrator.DimCustomer>("select PK_Dim_Customer, REFERENCE_ID from dim_customer where Reference_Id is not null")
                    .ToDictionary(x => x.PK_Dim_Customer, x => x.REFERENCE_ID);
                dimRigs = conn.Query<RigMigrator.DimRig>("select PK_Dim_Rig, REFERENCE_ID from dim_rig")
                    .ToDictionary(x => x.PK_Dim_Rig, x => x.REFERENCE_ID);
                dimProducts = conn.Query<ProductMigrator.DimProduct>("select PK_Dim_Product, Product_Id from dim_product where product_id is not null")
                    .ToDictionary(x => x.PK_Dim_Product, x => x.Product_Id);
                dimEquipments = conn.Query<EquipmentMigrator.DimEquipment>("select PK_Dim_Equipment, REFERENCE_ID from dim_equipment")
                    .ToDictionary(x => x.PK_Dim_Equipment, x => x.REFERENCE_ID);
            }

            var handledEquipments = new HashSet<long>();
            using (var newDb = newServerConnectionString.GetNewMyDrillingDbContext())
            {
                var newEquipments = newDb.Equipments.ToDictionary(x => x.ReferenceId, x => x);
                var newCustomers = newDb.Customers.Where(x => !string.IsNullOrEmpty(x.ReferenceId)).ToDictionary(x => x.ReferenceId, x => x);
                var newRigs = newDb.Rigs.ToDictionary(x => x.ReferenceId, x => x);

                foreach (var flh in factFuncLocationHistories)
                {
                    if (!dimEquipments.TryGetValue(flh.Key, out var eqRefId)
                        || !newEquipments.TryGetValue(eqRefId.ToLongReferenceId(), out var eq))
                    {
                        Log.Logger.Warning($"Dim_Equipment_FK={flh.Key} not found");
                        continue;
                    }

                    if (handledEquipments.Contains(eq.ReferenceId))
                    {
                        continue;
                    }

                    handledEquipments.Add(eq.ReferenceId);

                    FactFunctionalLocationHistory current = null;
                    foreach (var flhItem in flh.Value)
                    {
                        if (current == null || !current.Equals(flhItem))
                        {
                            current = flhItem;
                            newDb.FunctionalLocationLogs.Add(new FunctionalLocationLog
                            {
                                Equipment = eq,
                                Customer = dimCustomers.TryGetValue(current.Dim_Customer_FK, out var custRefId) && newCustomers.TryGetValue(custRefId, out var cust) ? cust : default,
                                Rig = dimRigs.TryGetValue(current.Dim_Rig_FK, out var rigRefId) && newRigs.TryGetValue(rigRefId.ToLongReferenceId(), out var rig) ? rig : default,
                                ProductCode = current.Product_Id,
                                EffectiveDate = current.EFFECTIVE_DATE,
                                Created = current.INSERT_DATE
                            });
                        }
                    }
                }
                
                newDb.SaveChanges();

                //todo check whether customer in fc is the same as in rig
                //todo rig customer isn't updated from SAP, check it also
            }

            Log.Logger.Information("Imported functional locations history");
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class FactFunctionalLocation
        {
            public int PK_Fact_FunctionalLocation;
            public DateTime? Dim_Time_FK;
            public int Dim_Customer_FK;
            public int Dim_Rig_FK;
            public int Dim_Product_FK;
            public int Dim_Equipment_FK;
            public string Source;
            public DateTime INSERT_DATE;
            public DateTime? UPDATE_DATE;
            public string INSERT_BY;
            public string Reference;
            public string Dim_MDR_Equipment_FK;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class FactFunctionalLocationHistory
        {
            public int PK_Fact_FunctionalLocation;
            public DateTime? Dim_Time_FK;
            public int Dim_Customer_FK;
            public int Dim_Rig_FK;
            public int Dim_Product_FK;
            public int Dim_Equipment_FK;
            public string Source;
            public DateTime INSERT_DATE;
            public DateTime? UPDATE_DATE;
            public string INSERT_BY;
            public string Reference;
            public string Dim_MDR_Equipment_FK;
            public DateTime EFFECTIVE_DATE;
            public string Product_Id;

            public bool Equals(FactFunctionalLocationHistory another)
            {
                return Dim_Customer_FK == another.Dim_Customer_FK
                       && Dim_Rig_FK == another.Dim_Rig_FK
                       && Product_Id == another.Product_Id
                       && Dim_Equipment_FK == another.Dim_Equipment_FK;
            }
        }
    }
}
