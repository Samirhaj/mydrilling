﻿using System;
using System.Diagnostics;
using Serilog;

namespace MyDrilling.Migrator
{
    public static class Meter
    {
        public static void Do(this Action action)
        {
            var sw = Stopwatch.StartNew();
            action.Invoke();
            sw.Stop();
            Log.Logger.Information($"spent {sw.Elapsed.ToString()}");
        }
    }
}
