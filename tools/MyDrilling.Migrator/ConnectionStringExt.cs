﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Infrastructure.Data;
using Serilog.Extensions.Logging;

namespace MyDrilling.Migrator
{
    public static class ConnectionStringExt
    {
        public const string NewMyDrillingDbName = "MyDrilling";
        //public const string NewMyDrillingDbName = "MyDrilling_PRD";
        //public const string NewMyDrillingDbName = "MyDrilling_QA";

        public static string GetDwhConnectionString(this string oldServerConnectionString) =>
            $"{oldServerConnectionString};Database=MD_DWH_MyDrilling";
            //$"{oldServerConnectionString};Database=MD_QA_DWH_MyDrilling";

        public static string GetStage1ConnectionString(this string oldServerConnectionString) =>
            $"{oldServerConnectionString};Database=MD_STG1_MyDrilling";
            //$"{oldServerConnectionString};Database=MD_QA_STG1_MyDrilling";

        public static string GetStage2ConnectionString(this string oldServerConnectionString) =>
            $"{oldServerConnectionString};Database=MD_STG2_MyDrilling";
            //$"{oldServerConnectionString};Database=MD_QA_STG2_MyDrilling";

        public static string GetPortalConnectionString(this string oldServerConnectionString) =>
            $"{oldServerConnectionString};Database=MD_Portal_MyDrilling";
            //$"{oldServerConnectionString};Database=MD_QA_Portal_MyDrilling";

        public static string GetClaimsStoreConnectionString(this string oldServerConnectionString) =>
            $"{oldServerConnectionString};Database=myDrilling-UserAccessAttributeStore";
            //$"{oldServerConnectionString};Database=MD_QA_UserAttributeStore";

        public static string GetNewDbConnectionString(this string newServerConnectionString) =>
            $"{newServerConnectionString};Database={NewMyDrillingDbName}";

        public static MyDrillingDb GetNewMyDrillingDbContext(this string newServerConnectionString, bool logSql = false)
        {
            var op = new DbContextOptionsBuilder<MyDrillingDb>();
            op.UseSqlServer(newServerConnectionString.GetNewDbConnectionString());
            if (logSql)
            {
                var lf = new LoggerFactory(new[] {new SerilogLoggerProvider()});
                op.UseLoggerFactory(lf);
                op.EnableSensitiveDataLogging();
            }

            return new MyDrillingDb(op.Options);
        }
    }
}
