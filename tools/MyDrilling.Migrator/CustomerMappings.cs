﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using Dapper;
using Microsoft.Data.SqlClient;
using MyDrilling.Migrator.Migrators;

namespace MyDrilling.Migrator
{
    public static class CustomerMappings
    {
        /// <summary>
        /// Mappings between customers from [myDrilling-UserAccessAttributeStore].[dbo].[UserAttributes], [MD_Portal_MyDrilling].[dbo].[myDrillingUser] tables
        /// to [MD_DWH_MyDrilling].[dbo].[Dim_MYD_Customer] table
        /// All mapping found manually, see CustomerMappings.md for details
        /// </summary>
        private static readonly Dictionary<string, string> Mapping = new Dictionary<string, string>();
        private static Dictionary<string, string> _dimMydCustomers;
        private static Dictionary<string, string> _dimMydCustomerBridge;

        //skip header
        private const int StartsFromLine = 2;

        public static void Init(string oldServer)
        {
            using (var conn = new SqlConnection(oldServer.GetDwhConnectionString()))
            {
                _dimMydCustomers = conn.Query<CustomerMigrator.DimMydCustomer>("select distinct GLOBAL_ID, MYD_Customer_Name from dim_myd_customer where last_version = 1")
                    .ToDictionary(x => x.GLOBAL_ID, x => x.MYD_Customer_Name);
            }

            using (var conn = new SqlConnection(oldServer.GetDwhConnectionString()))
            {
                _dimMydCustomerBridge = conn.Query<CustomerMigrator.DimMydCustomerBridge>("select distinct PARENT_GLOBAL_ID, GLOBAL_ID from Dim_Myd_Customer_bridge where last_version = 1")
                    .ToDictionary(x => x.GLOBAL_ID, x => x.PARENT_GLOBAL_ID);
            }

            var file = oldServer.Contains("DEV06", StringComparison.InvariantCultureIgnoreCase)
                ? "CustomerMappings-QA.md"
                : "CustomerMappings-PRD.md";

            var mappingFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), file);
            var lines = File.ReadAllLines(mappingFile);
            for (int i = StartsFromLine; i < lines.Length; i++)
            {
                var mapping = lines[i].Split('|');
                var crapCustomerId = mapping[0].Trim();
                var dimMydGlobalId = mapping[1].Trim();

                Mapping[crapCustomerId.ToLower()] = dimMydGlobalId;
            }
        }

        public static bool TryFindMydCustomerName(string crapCustomerId, out string dimMydCustomerName)
        {
            if (string.IsNullOrEmpty(crapCustomerId))
            {
                dimMydCustomerName = string.Empty;
                return false;
            }

            crapCustomerId = crapCustomerId.Trim().ToLower();
            if (crapCustomerId.ToUpper() == Constants.Internal)
            {
                dimMydCustomerName = Constants.MhWirth;
                return true;
            }

            if (_dimMydCustomerBridge.TryGetValue(crapCustomerId, out var dimMydCustomerGlobalId)
                && _dimMydCustomers.TryGetValue(dimMydCustomerGlobalId, out dimMydCustomerName))
            {
                return true;
            }

            if (_dimMydCustomers.TryGetValue(crapCustomerId, out dimMydCustomerName))
            {
                return true;
            }

            if (Mapping.TryGetValue(crapCustomerId, out dimMydCustomerGlobalId)
                && _dimMydCustomers.TryGetValue(dimMydCustomerGlobalId, out dimMydCustomerName))
            {
                return true;
            }

            return false;
        }
    }
}
