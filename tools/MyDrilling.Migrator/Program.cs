﻿using System;
using System.IO;
using Azure.Storage.Blobs;
using Microsoft.Extensions.Configuration;
using MyDrilling.Infrastructure.Storage;
using MyDrilling.Migrator.Migrators;
using Serilog;
using Serilog.Events;

namespace MyDrilling.Migrator
{
    class Program
    {
        static void Main()
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft.EntityFrameworkCore", LogEventLevel.Information)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .WriteTo.File((long)DateTime.UtcNow.Subtract(DateTime.UnixEpoch).TotalSeconds + ".log")
                .CreateLogger();

            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: false)
                .Build();

            var oldServer = configuration.GetConnectionString("OldMyDrillingDbServer");
            var newServer = configuration.GetConnectionString("NewMyDrillingDbServer");
            var upgradeMatrixPath = configuration.GetConnectionString("UpgradeMatrixPath");
            var blobConnectionString = configuration.GetConnectionString("BlobConnectionString");

            CustomerMappings.Init(oldServer);

            NewDbCreator.CreateIfNotExists(newServer);
            var blobServiceClient = new BlobServiceClient(blobConnectionString);

            bool insertRecords = true;

            //this one should be executed before consistency checks when insertRecords = false
            //Meter.Do(() => FunctionalLocationLogMigrator.Do(oldServer, newServer));

            Meter.Do(() => UserMigrator.Do(oldServer, newServer, blobConnectionString));
            Meter.Do(() => CustomerMigrator.Do(oldServer, newServer, insertRecords));
            Meter.Do(() => UserMigrator.SetCustomerIdForUsers(oldServer, newServer));
            Meter.Do(() => RigMigrator.Do(oldServer, newServer, insertRecords));
            Meter.Do(() => EquipmentMigrator.Do(oldServer, newServer, insertRecords));
            Meter.Do(() => BulletinsMigrator.Do(oldServer, newServer));
            Meter.Do(() => ClaimsMigrator.Do(oldServer, newServer));
            Meter.Do(() => EnquiryMigrator.Do(oldServer, newServer, blobServiceClient.GetBlobContainerClient(StorageConfig.EnquiriesContainerName)));
            Meter.Do(() => UpgradeMatrixMigrator.Do(upgradeMatrixPath, newServer, blobConnectionString));
            Meter.Do(() => RiconMigrator.Do(oldServer, newServer, insertRecords));
            //Meter.Do(() => FunctionalLocationLogMigrator.Do(oldServer, newServer));
            Meter.Do(() => SubscriptionMigrator.Do(oldServer, newServer));
            Meter.Do(() => DocumentationMigrator.Do(oldServer, newServer, insertRecords));
            Meter.Do(() => SparepartMigrator.Do(oldServer, newServer, insertRecords));
        }
    }
}
