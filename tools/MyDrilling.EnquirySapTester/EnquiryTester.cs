using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MyDrilling.EnquirySapTester.Sap;
using MyDrilling.EnquirySapTester.Sap.Extensions;
using Newtonsoft.Json;

namespace MyDrilling.EnquirySapTester
{
    public class EnquiryTester
    {
        private const string TestFileName = "testfile.png";
        private readonly ILogger<EnquiryTester> _logger;
        private readonly ISoapSapClientsFactory _soapSap;

        public EnquiryTester(ILogger<EnquiryTester> logger,
            ISoapSapClientsFactory soapSap)
        {
            _logger = logger;
            _soapSap = soapSap;
        }

        public async Task ExecuteAsync()
        {
            _logger.LogInformation("1. adding enquiry");
            var linuxTs = (long)DateTime.UtcNow.Subtract(DateTime.UnixEpoch).TotalSeconds;
            var enquiryReferenceId = await _soapSap.CreateEnquiry($"{linuxTs}", 8, "0000106672", "000000000010236392", "000000000010624344");
            _logger.LogInformation("enquiry created: {referenceId}", enquiryReferenceId);

            _logger.LogInformation("2. adding short comment");
            var shortCommentReferenceId = await _soapSap.AddCommentToEnquiry(enquiryReferenceId, "hello from netcore3.1", "mtk");
            _logger.LogInformation("short comment added: {referenceId}", shortCommentReferenceId);

            _logger.LogInformation("3. adding long comment");
            var longCommentReferenceId = await _soapSap.AddCommentToEnquiry(enquiryReferenceId, "You can spell check very long text areas without compromising any performance hits. Regardless of the size of the text, UltimateSpell only sends small portions of the text to the server as needed, while the user spell checks through the text.", "mtk");
            _logger.LogInformation("long comment added: {referenceId}", longCommentReferenceId);

            _logger.LogInformation("4. adding file");
            var pathToFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), TestFileName);
            var fileReferenceId = await _soapSap.AddFileToEnquiry(enquiryReferenceId, TestFileName, File.ReadAllBytes(pathToFile));
            _logger.LogInformation("file added: {referenceId}", fileReferenceId);

            _logger.LogInformation("5. reading and downloading file");
            var fileResult = await _soapSap.GetFile(fileReferenceId);
            var fileName = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), Guid.NewGuid() + fileResult.Name);
            File.WriteAllBytes(fileName, fileResult.Content);
            _logger.LogInformation("file downloaded: {fileName}", fileName);

            _logger.LogInformation("6. reading enquiry");
            var enquiry = await _soapSap.ReadEnquiry(enquiryReferenceId);
            _logger.LogInformation(JsonConvert.SerializeObject(enquiry, Formatting.Indented));

            _logger.LogInformation("7. reading notification");
            var notification = await _soapSap.ReadNotification(enquiryReferenceId);
            _logger.LogInformation(JsonConvert.SerializeObject(notification, Formatting.Indented));
        }
    }
}
