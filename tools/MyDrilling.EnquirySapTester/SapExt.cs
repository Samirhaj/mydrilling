﻿using System.Collections.Generic;
using System.Linq;

namespace MyDrilling.EnquirySapTester
{
    public static class SapExt
    {
        private const int SapCommentMaxLength = 69;

        public static string ToSapReferenceId(this string referenceId) => referenceId.ToSapId(12);

        public static string RemoveLeadingZeros(this string referenceId) => 
            string.IsNullOrEmpty(referenceId) ? referenceId : referenceId.TrimStart('0');

        public static string ToSapCommentNumber(this int number) => number.ToString().ToSapId(4);

        public static IEnumerable<string> SplitCommentForSap(this string comment)
        {
            if (string.IsNullOrEmpty(comment))
            {
                return Enumerable.Empty<string>();
            }

            comment = comment.Trim();
            var remainingComment = comment;
            var comments = new List<string>();

            while (remainingComment.Length > 0)
            {
                if (remainingComment.Length < SapCommentMaxLength)
                {
                    comments.Add(remainingComment);
                    break;
                }

                var commentPart = remainingComment.Substring(0, SapCommentMaxLength);
                //never split after a space, always before due to SAPs internal handling
                commentPart = commentPart.TrimEnd(' ');
                comments.Add(commentPart);

                remainingComment = remainingComment.Substring(SapCommentMaxLength);
                //move removed spaces to start of remaining description
                remainingComment = new string(' ', SapCommentMaxLength - commentPart.Length) + remainingComment;
            }

            return comments;
        }

        private static string ToSapId(this string referenceId, int length)
        {
            return string.IsNullOrEmpty(referenceId) || referenceId.Length == length
                ? referenceId
                : referenceId.PadLeft(length, '0');
        }
    }
}
