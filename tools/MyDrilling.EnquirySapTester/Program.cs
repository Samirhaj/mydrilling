using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MyDrilling.EnquirySapTester.Sap;
using Serilog;
using Serilog.Events;

namespace MyDrilling.EnquirySapTester
{
    public class Program
    {
        public static async Task Main()
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Information)
                .MinimumLevel.Override("Microsoft.AspNetCore", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .WriteTo.File(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), (long)DateTime.UtcNow.Subtract(DateTime.UnixEpoch).TotalSeconds + ".log"))
                .CreateLogger();

            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            var sp = new ServiceCollection()
                .AddSingleton<ISoapSapClientsFactory, SoapSapClientsFactory>()
                .AddLogging(logging => logging.ClearProviders().AddSerilog())
                .BuildServiceProvider();

            await ActivatorUtilities.CreateInstance<EnquiryTester>(sp).ExecuteAsync();
        }
    }
}
