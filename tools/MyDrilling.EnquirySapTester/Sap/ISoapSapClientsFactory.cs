﻿using SapService1;
using SapService2;
using SapService3;
using SapService4;
using SapService5;
using SapService6;
using SapService7;
using SapService8;
using SapService9;

namespace MyDrilling.EnquirySapTester.Sap
{
    public interface ISoapSapClientsFactory
    {
        AsyncDisposeWrapper<ZZMYDRILLING_CREATE_NOTIF_V02> GetCreateEnquiryClient();
        AsyncDisposeWrapper<ZZBAPI_ALM_NOTIF_DATA_ADD_1> GetAddCommentToEnquiryClient();
        AsyncDisposeWrapper<DocumentERPCreateRequestConfirmation_In_V1> GetAddFileToEnquiryClient();
        AsyncDisposeWrapper<ServiceRequestERPByIDQueryResponse_In_V1> GetReadEnquiryClient();
        AsyncDisposeWrapper<ZZBAPI_ALM_NOTIF_GET_DETAIL> GetReadNotificationClient();
        AsyncDisposeWrapper<ServiceRequestERPAttachmentFolderByIDQueryResponse_In> GetReadDocumentsForEnquiryClient();
        AsyncDisposeWrapper<ZZ_MH_ADD_DIR_TO_FOLDER_V1> GetAddFileToFolderClient();
        AsyncDisposeWrapper<DocumentERPByIDQueryResponse_In_V1> GetReadDocumentDetailsClient();
        AsyncDisposeWrapper<DocumentERPFileVariantByIDQueryResponse_In> GetReadFileClient();
    }
}
