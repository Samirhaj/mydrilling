﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.Threading.Tasks;
using SapService1;
using SapService2;
using SapService3;
using SapService4;
using SapService5;
using SapService6;
using SapService7;
using SapService8;
using SapService9;

namespace MyDrilling.EnquirySapTester.Sap
{
    public sealed class SoapSapClientsFactory : ISoapSapClientsFactory
    {
        public AsyncDisposeWrapper<ZZMYDRILLING_CREATE_NOTIF_V02> GetCreateEnquiryClient()
        {
            var url = SapServicesOptions.Urls.ZZMYDRILLING_CREATE_NOTIF_V02;
            var client = new ZZMYDRILLING_CREATE_NOTIF_V02Client(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<ZZMYDRILLING_CREATE_NOTIF_V02>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<ZZBAPI_ALM_NOTIF_DATA_ADD_1> GetAddCommentToEnquiryClient()
        {
            var url = SapServicesOptions.Urls.ZZBAPI_ALM_NOTIF_DATA_ADD_1;
            var client = new ZZBAPI_ALM_NOTIF_DATA_ADD_1Client(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<ZZBAPI_ALM_NOTIF_DATA_ADD_1>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<DocumentERPCreateRequestConfirmation_In_V1> GetAddFileToEnquiryClient()
        {
            var url = SapServicesOptions.Urls.DocumentERPCreateRequestConfirmation_In_V1;
            var client = new DocumentERPCreateRequestConfirmation_In_V1Client(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<DocumentERPCreateRequestConfirmation_In_V1>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<ServiceRequestERPByIDQueryResponse_In_V1> GetReadEnquiryClient()
        {
            var url = SapServicesOptions.Urls.ServiceRequestERPByIDQueryResponse_In_V1;
            var client = new ServiceRequestERPByIDQueryResponse_In_V1Client(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<ServiceRequestERPByIDQueryResponse_In_V1>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<ZZBAPI_ALM_NOTIF_GET_DETAIL> GetReadNotificationClient()
        {
            var url = SapServicesOptions.Urls.ZZBAPI_ALM_NOTIF_GET_DETAIL;
            var client = new ZZBAPI_ALM_NOTIF_GET_DETAILClient(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<ZZBAPI_ALM_NOTIF_GET_DETAIL>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<ServiceRequestERPAttachmentFolderByIDQueryResponse_In> GetReadDocumentsForEnquiryClient()
        {
            var url = SapServicesOptions.Urls.ServiceRequestERPAttachmentFolderByIDQueryResponse_In;
            var client = new ServiceRequestERPAttachmentFolderByIDQueryResponse_InClient(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<ServiceRequestERPAttachmentFolderByIDQueryResponse_In>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<ZZ_MH_ADD_DIR_TO_FOLDER_V1> GetAddFileToFolderClient()
        {
            var url = SapServicesOptions.Urls.ZZ_MH_ADD_DIR_TO_FOLDER_V1;
            var client = new ZZ_MH_ADD_DIR_TO_FOLDER_V1Client(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<ZZ_MH_ADD_DIR_TO_FOLDER_V1>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<DocumentERPByIDQueryResponse_In_V1> GetReadDocumentDetailsClient()
        {
            var url = SapServicesOptions.Urls.DocumentERPByIDQueryResponse_In_V1;
            var client = new DocumentERPByIDQueryResponse_In_V1Client(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<DocumentERPByIDQueryResponse_In_V1>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<DocumentERPFileVariantByIDQueryResponse_In> GetReadFileClient()
        {
            var url = SapServicesOptions.Urls.DocumentERPFileVariantByIDQueryResponse_In;
            var client = new DocumentERPFileVariantByIDQueryResponse_InClient(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<DocumentERPFileVariantByIDQueryResponse_In>(client, CloseCommunicationObjectAsync);
        }

        private static async Task CloseCommunicationObjectAsync<T>(T client)
        {
            if (!(client is ICommunicationObject comObj))
            {
                return;
            }

            try
            {
                // Only want to call Close if it is in the Opened state
                if (comObj.State == CommunicationState.Opened)
                {
                    //copied from generated proxy class, CloseAsync() method
                    await Task.Factory.FromAsync(comObj.BeginClose(null, null), comObj.EndClose);
                }
                // Anything not closed by this point should be aborted
                if (comObj.State != CommunicationState.Closed)
                {
                    comObj.Abort();
                }
            }
            catch (TimeoutException)
            {
                comObj.Abort();
            }
            catch (CommunicationException)
            {
                comObj.Abort();
            }
            catch (Exception)
            {
                comObj.Abort();
                throw;
            }
        }

        private void ConfigureAuthentication<TClient>(ClientBase<TClient> client)
            where TClient: class
        {
            client.ClientCredentials.UserName.UserName = SapServicesOptions.Username;
            client.ClientCredentials.UserName.Password = SapServicesOptions.Password;

            //now SAP web services use self-signed TLS certificate
            //so we skip certificate validation
            if (SapServicesOptions.SkipHttpsCertificateValidation)
            {
                client.ClientCredentials.ServiceCertificate.SslCertificateAuthentication =
                    new X509ServiceCertificateAuthentication
                    {
                        CertificateValidationMode = X509CertificateValidationMode.None,
                        RevocationMode = X509RevocationMode.NoCheck
                    };
            }
        }

        private Binding CreateBinding(string endpointUrl)
        {
            return endpointUrl.StartsWith("https", StringComparison.InvariantCultureIgnoreCase)
                ? CreateHttpsBindingForEndpoint()
                : CreateHttpBindingForEndpoint();
        }

        private Binding CreateHttpsBindingForEndpoint()
        {
            var binding = new BasicHttpsBinding();
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            binding.Security.Mode = BasicHttpsSecurityMode.Transport;

            binding.MaxBufferSize = int.MaxValue;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
            binding.AllowCookies = true;

            binding.ReceiveTimeout = SapServicesOptions.Timeout;
            binding.SendTimeout = SapServicesOptions.Timeout;
            binding.OpenTimeout = SapServicesOptions.Timeout;
            binding.CloseTimeout = SapServicesOptions.Timeout;

            return binding;
        }

        private Binding CreateHttpBindingForEndpoint()
        {
            var binding = new BasicHttpBinding();
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;

            binding.MaxBufferSize = int.MaxValue;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
            binding.AllowCookies = true;

            binding.ReceiveTimeout = SapServicesOptions.Timeout;
            binding.SendTimeout = SapServicesOptions.Timeout;
            binding.OpenTimeout = SapServicesOptions.Timeout;
            binding.CloseTimeout = SapServicesOptions.Timeout;

            return binding;
        }

        private static EndpointAddress CreateEndpointAddress(string endpointUrl)
        {
            return new EndpointAddress(endpointUrl);
        }
    }
}
