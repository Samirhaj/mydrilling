﻿using System.Threading.Tasks;
using SapService1;

namespace MyDrilling.EnquirySapTester.Sap.Extensions
{
    public static class CreateEnquiryExt
    {
        public static async Task<string> CreateEnquiry(this ISoapSapClientsFactory soapSap,
            string title, //max length 40
            int type,
            string customerSapId,
            string rigSapId,
            string equipmentSapId)
        {
            var coding = Mappings.EnquiryTypesToSapCodings[type];
            await using var createEnquiryClient = soapSap.GetCreateEnquiryClient();
            var res = await createEnquiryClient.Target.ZZMYDRILLING_CREATE_NOTIF_V02Async(new ZZMYDRILLING_CREATE_NOTIF_V02Request(
                new ZZMYDRILLING_CREATE_NOTIF_V021
                {
                    CODE_GROUP = coding.CodingGroup,
                    CODING = coding.Coding,
                    EQUIPMENT = string.IsNullOrEmpty(equipmentSapId) ? rigSapId : equipmentSapId,
                    NOTIF_TYPE = "S0",
                    PRIORITY = type == 3 ? "1" : "3",
                    SHORT_TEXT = title,
                    SOLD_TO = customerSapId,
                    RETURN = new[]
                    {
                        new BAPIRET2
                        {
                            MESSAGE = "0 ",
                            MESSAGE_V1 = "0 ",
                            MESSAGE_V2 = "0 ",
                            MESSAGE_V3 = "0 ",
                            MESSAGE_V4 = "0 "
                        }
                    }
                }));

            return res.ZZMYDRILLING_CREATE_NOTIF_V02Response.NOTIFHEADER_EXPORT.NOTIF_NO.RemoveLeadingZeros();
        }
    }
}
