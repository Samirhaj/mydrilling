﻿using System.Linq;
using System.Threading.Tasks;
using SapService2;

namespace MyDrilling.EnquirySapTester.Sap.Extensions
{
    public static class AddCommentToEnquiryExt
    {
        private const int FirstCommentNumber = 1;

        public static async Task<string> AddCommentToEnquiry(this ISoapSapClientsFactory soapSap,
            string enquiryReferenceId,
            string commentText,
            string createdBy)
        {
            var commentNumber = await soapSap.GetNextCommentNumber(enquiryReferenceId);
            await using var createEnquiryClient = soapSap.GetAddCommentToEnquiryClient();
            await createEnquiryClient.Target.ZzbapiAlmNotifDataAddAsync(new ZzbapiAlmNotifDataAddRequest(new ZzbapiAlmNotifDataAdd
            {
                Number = enquiryReferenceId.ToSapReferenceId(),
                Notfulltxt = commentText.SplitCommentForSap().Select(x => new Bapi2080Notfulltxti
                {
                    Objtype = "QMMA",
                    Objkey = commentNumber,
                    FormatCol = "*",
                    TextLine = x
                }).ToArray(),
                Notifactv = new[]
                {
                    new Bapi2080Notactvi
                    {
                        ActSortNo = commentNumber,
                        Acttext = $"Comment added by {createdBy}",
                        ActCodegrp = "MYDR",
                        ActCode = "DESC"
                    }
                },
                Return = new[]
                {
                    new Bapiret2{Message = "X"}
                }
            }));

            return $"{enquiryReferenceId.RemoveLeadingZeros()}|{commentNumber}";
        }

        private static async Task<string> GetNextCommentNumber(this ISoapSapClientsFactory soapSap, string enquiryReferenceId)
        {
            var enquiry = await soapSap.ReadEnquiry(enquiryReferenceId);
            if (enquiry.ServiceRequestERPByIDResponse_sync_V1.ServiceRequest.Activity == null
                || enquiry.ServiceRequestERPByIDResponse_sync_V1.ServiceRequest.Activity.Length == 0)
            {
                return FirstCommentNumber.ToSapCommentNumber();
            }

            var numbers = enquiry.ServiceRequestERPByIDResponse_sync_V1.ServiceRequest.Activity.Select(x => x.ID)
                .Where(x => x != null)
                .Select(x => int.TryParse(x.TrimStart('0'), out var number) ? number : -1)
                .Where(x => x > 0)
                .ToArray();

            return (numbers.Length == 0 ? FirstCommentNumber : numbers.Max() + 1).ToSapCommentNumber();
        }
    }
}
