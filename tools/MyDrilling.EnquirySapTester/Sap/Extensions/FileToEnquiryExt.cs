﻿using System;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SapService3;
using SapService6;
using SapService7;
using SapService8;
using SapService9;

namespace MyDrilling.EnquirySapTester.Sap.Extensions
{
    public static class FileToEnquiryExt
    {
        private const string SapAdministrationFolderName = "Administration";
        private const string SapDocumentTypeCode = "ADM";
        private const string FileReferenceSeparator = "|";

        public static async Task<string> AddFileToEnquiry(this ISoapSapClientsFactory soapSap,
            string enquiryReferenceId,
            string fileName,
            byte[] fileContent)
        {
            //get all files and folders for enquiry
            await using var readDocumentsClient = soapSap.GetReadDocumentsForEnquiryClient();
            var documents = await readDocumentsClient.Target.ServiceRequestERPAttachmentFolderByIDQueryResponse_InAsync(new ServiceRequestERPAttachmentFolderByIDQueryResponse_InRequest(new ServiceRequestERPAttachmentFolderByIDQueryMessage_sync
            {
                ServiceRequestAttachmentFolderSelectionByID = new ServiceRequestERPAttachmentFolderByIDQueryMessage_syncServiceRequestAttachmentFolderSelectionByID { ID = new ServiceRequestID { Value = enquiryReferenceId } }
            }));
            Serilog.Log.Logger.Information(JsonConvert.SerializeObject(documents, Formatting.Indented));

            //find Administration
            DocumentERPByIDQueryResponse_In_V1Response administrationDocumentDetails = null;
            await using var readDocumentDetailsClient = soapSap.GetReadDocumentDetailsClient();
            foreach (var document in documents.ServiceRequestERPAttachmentFolderByIDResponse_sync.ServiceRequest.AttachmentFolder)
            {
                var documentDetails = await readDocumentDetailsClient.Target.DocumentERPByIDQueryResponse_In_V1Async(new DocumentERPByIDQueryResponse_In_V1Request(new DocumentByIDQueryMessage_sync
                {
                    DocumentSelectionByID = new DocumentByIDQueryMessage_syncDocumentSelectionByID
                    {
                        DocumentName = document.Name,
                        DocumentTypeCode = new SapService8.DocumentTypeCode { Value = document.TypeCode.Value },
                        AlternativeDocumentID = document.AlternativeDocumentID,
                        DocumentVersionID = document.VersionID
                    }
                }));

                if (documentDetails.DocumentERPByIDResponse_sync_V1.Document.Description.Value == SapAdministrationFolderName)
                {
                    administrationDocumentDetails = documentDetails;
                    break;
                }
            }

            if (administrationDocumentDetails == null)
            {
                throw new DirectoryNotFoundException($"{SapAdministrationFolderName} folder not found for enquiry {enquiryReferenceId}.");
            }
            Serilog.Log.Logger.Information(JsonConvert.SerializeObject(administrationDocumentDetails, Formatting.Indented));

            //add file
            await using var createEnquiryClient = soapSap.GetAddFileToEnquiryClient();
            var attachedFile = await createEnquiryClient.Target.DocumentERPCreateRequestConfirmation_In_V1Async(new DocumentERPCreateRequestConfirmation_In_V1Request(new DocumentERPCreateRequestMessage_sync_V1
            {
                Document = new DocumentERPCreateRequestMessage_sync_V1Document
                {
                    TypeCode = new SapService3.DocumentTypeCode { Value = SapDocumentTypeCode },
                    CategoryCode = "2",
                    Description = new SapService3.SHORT_Description { Value = fileName },
                    ComputerAidedDesignDrawing = false,
                    FileVariant = new[]
                    {
                        new DocERPBulkCrteReq_s_V1FileVar
                        {
                            LargeFileHandlingIndicator = false,
                            DocumentFormatCode = new SapService3.DocumentFormatCode{Value = Path.GetExtension(fileName).TrimStart('.').ToUpper()},
                            DocumentPathName = fileName,
                            Description = new SapService3.SHORT_Description{Value = fileName},
                            DocumentRepositoryCode = new SapService3.DocumentRepositoryCode{Value = SapServicesOptions.DocumentRepositoryCode},
                            BinaryObject = new SapService3.BinaryObject
                            {
                                mimeCode = "Request",
                                fileName = fileName,
                                Value = fileContent
                            }
                        }
                    },
                    PropertyValuation = new[]
                    {
                        new DocumentERPCreateRequestMessage_sync_V1DocumentPropertyValuation
                        {
                            CollectionTypeCode = "017",
                            CollectionID = "MH_CORRESPONDENCE",
                            Valuation = new []
                            {
                                new SapService3.PropertyValuation
                                {
                                    PropertyReference = new SapService3.PropertyReference{ID = new SapService3.PropertyID{Value = "DOC_CLASSIFICATION"}},
                                    ValueGroup = new []
                                    {
                                        new SapService3.PropertyValuationValueGroup
                                        {
                                            OrdinalNumberValue = 1,
                                            PropertyValue = new SapService3.PropertyValue{NameSpecification = new []
                                            {
                                                new SapService3.PropertyValueNameSpecification
                                                {
                                                    Name = new SapService3.Name{Value = "Public"}
                                                }
                                            }}
                                        }
                                    }
                                }
                            }
                        }
                    },
                    ObjectReference = new[]
                    {
                        new DocERPBulkCrteReq_s_V1ObjRef
                        {
                            ReferenceObjectID = new SapService3.ObjectID{Value = enquiryReferenceId.ToSapReferenceId()},
                            ReferenceObjectTypeCode = new SapService3.DocumentObjectReferenceTypeCode{Value = "SMQMEL"}
                        }
                    },
                    Status = new DocumentERPCreateRequestMessage_sync_V1DocumentStatus
                    {
                        Code = new SapService3.DocumentStatusCode { Value = "96" }
                    }
                }
            }));

            //move to this folder
            var folder = administrationDocumentDetails.DocumentERPByIDResponse_sync_V1.Document;
            var file = attachedFile.DocumentERPCreateConfirmation_sync_V1.Document;
            await using var addFileToFolderClient = soapSap.GetAddFileToFolderClient();
            await addFileToFolderClient.Target.ZZ_MH_ADD_DIR_TO_FOLDER_V1Async(new ZZ_MH_ADD_DIR_TO_FOLDER_V1Request(new ZZ_MH_ADD_DIR_TO_FOLDER_V11
            {
                IMP_DIR_DOCUMENTNUMBER = file.Name,
                IMP_DIR_DOCUMENTPART = file.AlternativeDocumentID,
                IMP_DIR_DOCUMENTTYPE = file.TypeCode.Value,
                IMP_DIR_DOCUMENTVERSION = file.VersionID,
                IMP_FOLDER_DOCUMENTNUMBER = folder.Name,
                IMP_FOLDER_DOCUMENTPART = folder.AlternativeDocumentID,
                IMP_FOLDER_DOCUMENTTYPE = folder.TypeCode.Value,
                IMP_FOLDER_DOCUMENTVERSION = folder.VersionID
            }));

            return $"{file.Name}{FileReferenceSeparator}{file.TypeCode.Value}{FileReferenceSeparator}{file.AlternativeDocumentID}{FileReferenceSeparator}{file.VersionID}";
        }

        public static async Task<(string Name, byte[] Content)> GetFile(this ISoapSapClientsFactory soapSap, string fileReferenceId)
        {
            var parts = fileReferenceId.Split(FileReferenceSeparator);
            if (parts.Length != 4)
            {
                return (string.Empty, Array.Empty<byte>());
            }

            await using var client = soapSap.GetReadFileClient();
            var res = await client.Target.DocumentERPFileVariantByIDQueryResponse_InAsync(new DocumentERPFileVariantByIDQueryResponse_InRequest(new DocumentERPFileVariantByIDQueryMessage_sync
            {
                DocumentFileVariantSelectionByID = new DocumentERPFileVariantByIDQueryMessage_syncDocumentFileVariantSelectionByID
                {
                    DocumentName = parts[0],
                    DocumentTypeCode = new SapService9.DocumentTypeCode { Value = parts[1] },
                    AlternativeDocumentID = parts[2],
                    DocumentVersionID = parts[3],
                    LargeFileHandlingIndicator = false
                }
            }));

            var file = res.DocumentERPFileVariantByIDResponse_sync.Document.FileVariant[0];
            return (file.Description.Value, file.BinaryObject.Value);
        }
    }
}
