﻿using System.Threading.Tasks;
using SapService4;
using SapService5;

namespace MyDrilling.EnquirySapTester.Sap.Extensions
{
    public static class ReadEnquiryExt
    {
        public static async Task<ServiceRequestERPByIDQueryResponse_In_V1Response> ReadEnquiry(this ISoapSapClientsFactory soapSap, string referenceId)
        {
            await using var client = soapSap.GetReadEnquiryClient();
            return await client.Target.ServiceRequestERPByIDQueryResponse_In_V1Async(new ServiceRequestERPByIDQueryResponse_In_V1Request(new SrvcReqERPByIDQryMsg_s_V1
            {
                ServiceRequestSelectionByID = new SrvcReqERPByIDQry_s_V1SrvcReq { ServiceRequestID = referenceId.ToSapReferenceId() }
            }));
        }

        public static async Task<AlmNotifGetDetailResponse1> ReadNotification(this ISoapSapClientsFactory soapSap, string referenceId)
        {
            await using var client = soapSap.GetReadNotificationClient();
            return await client.Target.AlmNotifGetDetailAsync(new AlmNotifGetDetailRequest(new AlmNotifGetDetail
            {
                Notifpartnr = new[] { new Bapi2080Notpartnre { PartnRole = "ZN" } },
                Number = referenceId.ToSapReferenceId()
            }));
        }
    }
}
