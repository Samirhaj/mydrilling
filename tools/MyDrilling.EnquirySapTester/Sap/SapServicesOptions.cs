﻿using System;

namespace MyDrilling.EnquirySapTester.Sap
{
    public static class SapServicesOptions
    {
        public static string Username { get; } = "MYDRILLING";
        public static string Password { get; } = "aBxY54zz";
        public static TimeSpan Timeout { get; } = TimeSpan.FromSeconds(30);
        public static bool SkipHttpsCertificateValidation { get; } = true;

        //public static string DocumentRepositoryCode = "Z_EAP_CAG"; //eap
        public static string DocumentRepositoryCode = "Z_EAQ_ALL"; //eaq
        //public static string DocumentRepositoryCode = "Z_EA1_ALL"; //ea1

        public static class Urls
        {
            //public const string Host = "https://sapeap.capdir.net:44318"; //eap
            public const string Host = "https://sapeaq.capdir.net:44307"; //eaq
            //public const string Host = "http://sapea1.capdir.net:8034"; //ea1

            // ReSharper disable InconsistentNaming

            public static string ZZMYDRILLING_CREATE_NOTIF_V02 { get; } = $"{Host}/sap/bc/srt/pm/sap/zzmydrilling_create_notif_v02/101/local/zakerinternal/1/binding_t_http_a_http_zzmydrilling_create_notif_v02_zakerinternal_l";
            public static string ZZBAPI_ALM_NOTIF_DATA_ADD_1 { get; } = $"{Host}/sap/bc/srt/pm/sap/zzbapi_alm_notif_data_add_1/101/local/zakerinternal/1/binding_t_http_a_http_zzbapi_alm_notif_data_add_1_zakerinternal_l";
            public static string DocumentERPCreateRequestConfirmation_In_V1 { get; } = $"{Host}/sap/bc/srt/pm/sap/ecc_documentcrtrc1/101/local/zakerinternal/1/binding_t_http_a_http_ecc_documentcrtrc1_zakerinternal_l";
            public static string ServiceRequestERPByIDQueryResponse_In_V1 { get; } = $"{Host}/sap/bc/srt/pm/sap/ecc_srvcreqidqr1/101/local/zakerinternal/1/binding_t_http_a_http_ecc_srvcreqidqr1_zakerinternal_l";
            public static string ZZBAPI_ALM_NOTIF_GET_DETAIL { get; } = $"{Host}/sap/bc/srt/pm/sap/zzbapi_alm_notif_get_detail/101/local/zakerinternal/1/binding_t_http_a_http_zzbapi_alm_notif_get_detail_zakerinternal_l";
            public static string ServiceRequestERPAttachmentFolderByIDQueryResponse_In { get; } = $"{Host}/sap/bc/srt/pm/sap/ecc_srvcreqattchfldridqr/101/local/zakerinternal/1/binding_t_http_a_http_ecc_srvcreqattchfldridqr_zakerinternal_l";
            public static string ZZ_MH_ADD_DIR_TO_FOLDER_V1 { get; } = $"{Host}/sap/bc/srt/pm/sap/zz_mh_add_dir_to_folder_v1/101/local/zakerinternal/1/binding_t_http_a_http_zz_mh_add_dir_to_folder_v1_zakerinternal_l";
            public static string DocumentERPByIDQueryResponse_In_V1 { get; } = $"{Host}/sap/bc/srt/pm/sap/ecc_documentidqr1/101/local/zakerinternal/1/binding_t_http_a_http_ecc_documentidqr1_zakerinternal_l";
            public static string DocumentERPFileVariantByIDQueryResponse_In { get; } = $"{Host}/sap/bc/srt/pm/sap/ecc_documentfvidqr/101/local/zakerinternal/1/binding_t_http_a_http_ecc_documentfvidqr_zakerinternal_l";

            // ReSharper restore InconsistentNaming
        }
    }
}
