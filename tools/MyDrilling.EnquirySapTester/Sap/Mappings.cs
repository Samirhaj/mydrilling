﻿using System.Collections.Generic;
using System.Linq;

namespace MyDrilling.EnquirySapTester.Sap
{
    public static class Mappings
    {
        //from stg1.SIB_DataMappings table
        private static readonly (int EnquiryTypeId, string CodingGroup, string Coding)[] EnquiryTypesMappings = 
        {
            (1, "SGEN", "SGSU"),
            (2, "SGEN", "SGRO"),
            (3, "HSE", "HROD"),
            (4, "OVER", "OVER"),
            (5, "TRAIN", "TRN"),
            (6, "REMO", "REMO"),
            (7, "BULL", "BULL"),
            (8, "SGEN", "SGEN"),
            (9, "SPGEN", "SPBA"),
            (10, "SPGEN", "SPPA"),
            (11, "SPGEN", "SPOS"),
            (12, "SPGEN", "SPGE"),
            (13, "RMOV", "RMOV"),
            (14, "EMOV", "EMOV"),
            (15, "INTER", "INT"),
            (16, "FEEDB", "IMPR")
        };

        public static readonly IReadOnlyDictionary<int, (int EnquiryTypeId, string CodingGroup, string Coding)> EnquiryTypesToSapCodings =
            EnquiryTypesMappings.ToDictionary(x => x.EnquiryTypeId, x => x);

        public static readonly IReadOnlyDictionary<(string CodingGroup, string Codingint), (int EnquiryTypeId, string CodingGroup, string Coding)> SapCodingsToEnquiryTypes =
            EnquiryTypesMappings.ToDictionary(x => (x.CodingGroup, x.Coding), x => x);
    }
}
