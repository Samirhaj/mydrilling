﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Dapper;
using Microsoft.Data.SqlClient;

namespace MissedRecordsCopier
{
    public static class EquipmentHandler
    {
        public static void Do()
        {
            HashSet<string> presentedEquipments;
            using (var conn = new SqlConnection(Program.NewCs))
            {
                presentedEquipments = conn.Query<long>("select referenceid from equipments").Select(x => x.ToString("D18")).ToArray().ToHashSet();
            }

            DimEquipment[] dimEquipments;
            using (var conn = new SqlConnection(Program.OldCs))
            {
                dimEquipments = conn.Query<DimEquipment>(@"select * from dim_equipment").ToArray();
            }

            dimEquipments = dimEquipments.GroupBy(x => x.GLOBAL_ID).Select(x => x.OrderBy(y => y.insert_date).Last())
                .ToArray();

            DateTime insertDateCharact;
            DateTime insertDateEquipment;
            using (var conn = new SqlConnection(Program.RawData))
            {
                insertDateCharact = conn.QuerySingle<DateTime>("select max(INSERT_DATE) from ZCS_EXTRACT_CHARACT");
                insertDateEquipment = conn.QuerySingle<DateTime>("select max(INSERT_DATE) from ZCS_EXTRACT_EQUIPMENT");
            }

            var charactForMissedEquipment = new List<ZcsCharact>();
            var missedEquipments = new List<ZcsEquipment>();
            foreach (var eq in dimEquipments.Where(x => !presentedEquipments.Contains(x.GLOBAL_ID)))
            {
                missedEquipments.Add(new ZcsEquipment
                {
                    EQUNR = eq.GLOBAL_ID,
                    EQKTU = eq.Equipment_Name.ToUpper(),
                    MATNR = eq.Equipment_Material,
                    SERNR = eq.Equipment_SerialNo,
                    HEQUI = eq.Parent,
                    TIDNR = eq.Equipment_TagNo,
                    INSERT_DATE = insertDateEquipment
                });

                charactForMissedEquipment.Add(new ZcsCharact
                {
                    EQUNR = eq.GLOBAL_ID,
                    ATNAM = "MH_NORSOK",
                    ATWRT = "FOR_MIGRATOR",
                    INSERT_DATE = insertDateCharact
                });
            }

            Console.WriteLine($"missed equipments {missedEquipments.Count}: {string.Join(",", missedEquipments.Select(x => x.EQUNR))}");
            Console.WriteLine($"charact for missed equipments {charactForMissedEquipment.Count}");

            using (var conn = new SqlConnection(Program.RawData))
            {
                conn.Execute(@"
INSERT INTO ZCS_EXTRACT_EQUIPMENT 
(EQUNR, EQKTU, MATNR, SERNR, HEQUI, TIDNR, INSERT_DATE) 
VALUES(@EQUNR, @EQKTU, @MATNR, @SERNR, @HEQUI, @TIDNR, @INSERT_DATE)", missedEquipments);

                conn.Execute(@"
INSERT INTO ZCS_EXTRACT_CHARACT 
(EQUNR, ATNAM, ATWRT, INSERT_DATE) 
VALUES(@EQUNR, @ATNAM, @ATWRT, @INSERT_DATE)", charactForMissedEquipment);
            }
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class DimEquipment
        {
            public int PK_Dim_Equipment;
            public string GLOBAL_ID;
            public string PARENT_GLOBAL_ID;
            public string REFERENCE_ID;
            public string Equipment_Id;
            public string Equipment_Name;
            public string Equipment_Material;
            public string Equipment_SerialNo;
            public string Equipment_TagNo;
            public string Equipment_Norsok_Long;
            public string Equipment_Norsok_Short;
            public string Equipment_Type;
            public string Parent;
            public int Equipment_Level;
            public int Equipment_Visible;
            public string SOURCE_ID;
            public DateTime? valid_from_date;
            public DateTime? valid_to_date;
            public DateTime insert_date;
            public DateTime? update_date;
            public string Equipment_Path;
            public string Rig_Global_ID;
        }

        public class ZcsEquipment
        {
            public string EQUNR { get; set; }
            public string EQKTU { get; set; }
            public string SERNR { get; set; }
            public string TIDNR { get; set; }
            public string MATNR { get; set; }
            public string HEQUI { get; set; }
            public string RIG_EQUNR { get; set; }
            public DateTime INSERT_DATE { get; set; }
        }

        public class ZcsCharact
        {
            public string EQUNR { get; set; }
            public string ATNAM { get; set; }
            public string ATWRT { get; set; }
            public string ATWTB { get; set; }
            public DateTime INSERT_DATE { get; set; }
        }
    }
}
