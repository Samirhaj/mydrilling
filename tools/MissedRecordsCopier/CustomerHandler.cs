﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Dapper;
using Microsoft.Data.SqlClient;

namespace MissedRecordsCopier
{
    public static class CustomerHandler
    {
        public static void Do()
        {
            HashSet<string> presentedCustomers;
            using (var conn = new SqlConnection(Program.NewCs))
            {
                presentedCustomers = conn.Query<string>("select referenceid from customers").ToArray().ToHashSet();
            }

            DimCustomer[] dimCustomers;
            using (var conn = new SqlConnection(Program.OldCs))
            {
                dimCustomers = conn.Query<DimCustomer>("select * from dim_customer").ToArray();
            }

            dimCustomers = dimCustomers.GroupBy(x => x.GLOBAL_ID).Select(x => x.OrderBy(y => y.insert_date).Last())
                .ToArray();

            DateTime insertDate;
            using (var conn = new SqlConnection(Program.RawData))
            {
                insertDate = conn.QuerySingle<DateTime>("select max(INS_DATE_TIME) from SFI07_Customer");
            }

            var missedCustomers = dimCustomers.Where(x => !presentedCustomers.Contains(x.GLOBAL_ID))
                .Select(x => new ZcsCustomer
                {
                    CUSTOMER_ID = x.GLOBAL_ID,
                    CUSTOMER_NAME = x.Customer_Name,
                    POSTAL_CODE = x.Customer_Postal_Code,
                    CITY = x.Customer_City,
                    INS_DATE_TIME = insertDate
                }).ToArray();

            Console.WriteLine($"missed customers {missedCustomers.Length}: {string.Join(",", missedCustomers.Select(x => x.CUSTOMER_ID))}");

            using (var conn = new SqlConnection(Program.RawData))
            {
                conn.Execute(@"
INSERT INTO SFI07_Customer 
(CUSTOMER_ID, CUSTOMER_NAME, POSTAL_CODE, CITY, INS_DATE_TIME) 
VALUES(@CUSTOMER_ID, @CUSTOMER_NAME, @POSTAL_CODE, @CITY, @INS_DATE_TIME)", missedCustomers);
            }
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class DimCustomer
        {
            public int PK_Dim_Customer;
            public string GLOBAL_ID;
            public string PARENT_GLOBAL_ID;
            public string REFERENCE_ID;
            public string Customer_Id;
            public string Customer_Name;
            public string Customer_Client;
            public string Customer_Sold_To_Party;
            public string Customer_City;
            public string Customer_Postal_Code;
            public string Source;
            public string SOURCE_ID;
            public DateTime? valid_from_date;
            public DateTime? valid_to_date;
            public DateTime insert_date;
            public DateTime? update_date;
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class ZcsCustomer
        {
            public string CUSTOMER_ID { get; set;}
            public string CUSTOMER_NAME { get; set;}
            public string POSTAL_CODE { get; set;}
            public string CITY { get; set;}
            public DateTime INS_DATE_TIME { get; set; }
        }
    }
}
