﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using Dapper;
using Microsoft.Data.SqlClient;

namespace MissedRecordsCopier
{
    public static class RigHandler
    {
        public static void Do()
        {
            HashSet<string> presentedRigs;
            using (var conn = new SqlConnection(Program.NewCs))
            {
                presentedRigs = conn.Query<long>("select referenceid from rigs").Select(x => x.ToString("D18")).ToArray().ToHashSet();
            }

            DimRig[] dimRigs;
            using (var conn = new SqlConnection(Program.OldCs))
            {
                dimRigs = conn.Query<DimRig>("select * from dim_rig").ToArray();
            }

            dimRigs = dimRigs.GroupBy(x => x.GLOBAL_ID).Select(x => x.OrderBy(y => y.insert_date).Last())
                .ToArray();

            DateTime insertDate;
            using (var conn = new SqlConnection(Program.RawData))
            {
                insertDate = conn.QuerySingle<DateTime>("select max(INSERT_DATE) from ZCS_EXTRACT_RIG");
            }

            var missedRigs = dimRigs.Where(x => !presentedRigs.Contains(x.GLOBAL_ID))
                .Select(x => new ZcsRig
                {
                    RIG = x.GLOBAL_ID.Trim('0'),
                    RIG_EQUNR = x.GLOBAL_ID,
                    EQTYP = "R",
                    RIG_EQKTU = x.Rig_Name.ToUpper(),
                    EQART = x.Rig_Type_Code,
                    KUNNR = x.Rig_Customer_Sold_To_Party,
                    TPLNR = $"a/a/{x.Rig_Region}",
                    INSERT_DATE = insertDate
                }).ToArray();

            Console.WriteLine($"missed rigs {missedRigs.Length}: {string.Join(",", missedRigs.Select(x => x.RIG_EQUNR))}");

            using (var conn = new SqlConnection(Program.RawData))
            {
                conn.Execute(@"
INSERT INTO ZCS_EXTRACT_RIG 
(RIG, RIG_EQUNR, EQTYP, RIG_EQKTU, EQART, KUNNR, TPLNR, INSERT_DATE) 
VALUES(@RIG, @RIG_EQUNR, @EQTYP, @RIG_EQKTU, @EQART, @KUNNR, @TPLNR, @INSERT_DATE)", missedRigs);
            }
        }

        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public class DimRig
        {
            public int PK_Dim_Rig;
            public string GLOBAL_ID;
            public string PARENT_GLOBAL_ID;
            public string REFERENCE_ID;
            public string Rig_Id;
            public string Rig_Name;
            public string Rig_Owner;
            public string Rig_Type_Code;
            public string Rig_Type;
            public string Rig_Client;
            public string Rig_Sector;
            public string Rig_Region;
            public double? Rig_Latitude;
            public double? Rig_Longitude;
            public string Rig_Customer_Sold_To_Party;
            public string SOURCE_ID;
            public DateTime? valid_from_date;
            public DateTime? valid_to_date;
            public DateTime insert_date;
            public DateTime? update_date;
        }

        public class ZcsRig
        {
            public string RIG { get; set; }
            public string RIG_EQUNR { get; set; }
            public string EQTYP { get; set; }
            public string RIG_EQKTU { get; set; }
            public string TPLNR { get; set; }
            public string EQART { get; set; }
            public string KUNNR { get; set; }
            public DateTime INSERT_DATE { get; set; }
        }
    }
}
