﻿using System;

namespace MissedRecordsCopier
{
    class Program
    {
        //qa
        public const string OldCs = @"Data Source=GDC-VTST-F10.testa.aktest.com\DEV06;Initial Catalog=MD_QA_DWH_MyDrilling;Integrated Security=true";
        public const string NewCs = @"Server=tcp:qa-mydrilling.database.windows.net,1433;Initial Catalog=MyDrilling;Persist Security Info=False;User ID=sqladmin;Password=duke667#GGD6rret!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
        public const string RawData = @"Server=tcp:qa-mydrilling.database.windows.net,1433;Initial Catalog=RawData-QA;Persist Security Info=False;User ID=sqladmin;Password=duke667#GGD6rret!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        //prd
        //public const string OldCs = @"Data Source=GDC-VSQL-G09.no.enterdir.com\MYD01;Initial Catalog=MD_DWH_MyDrilling;Integrated Security=true";
        //public const string NewCs = @"Server=tcp:prod-mydrilling.database.windows.net,1433;Initial Catalog=MyDrilling;Persist Security Info=False;User ID=sqladmin;Password=arret943&FFK1ffaz!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
        //public const string RawData = @"Server=tcp:prod-mydrilling.database.windows.net,1433;Initial Catalog=RawData-Prod;Persist Security Info=False;User ID=sqladmin;Password=arret943&FFK1ffaz!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        static void Main(string[] args)
        {
            CustomerHandler.Do();
            Console.WriteLine();
            Console.WriteLine();
            RigHandler.Do();
            Console.WriteLine();
            Console.WriteLine();
            EquipmentHandler.Do();
        }
    }
}
