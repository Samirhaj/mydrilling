﻿using System;

namespace MyDrilling.Core.Exceptions
{
    public class BusinessException : Exception
    {
        public string Details { get; }

        public BusinessException(string message) : base(message)
        { }

        public BusinessException(string message, string details) : base(message)
        {
            Details = details;
        }
    }
}
