﻿using System;
using System.Collections.Generic;
using MyDrilling.Core.Entities.Permission;

namespace MyDrilling.Core
{
    public class CombinedUserRoles
    {
        private readonly IReadOnlyDictionary<RoleInfo.Role, long[]> _roles;

        public static readonly CombinedUserRoles Empty = new CombinedUserRoles(new Dictionary<RoleInfo.Role, long[]>());

        public CombinedUserRoles(IReadOnlyDictionary<RoleInfo.Role, long[]> roles)
        {
            _roles = roles;
        }

        public long[] GetViewAccessToRigs() => GetIdentifiers(RoleInfo.Role.MyDrillingViewer);

        public long[] GetEnquiryHandlingInitiatorRigs() => GetIdentifiers(RoleInfo.Role.EnquiryHandlingInitiator);

        public long[] GetEnquiryHandlingApproverRigs() => GetIdentifiers(RoleInfo.Role.EnquiryHandlingApprover);

        public long[] GetEnquiryHandlingNotifierRigs() => GetIdentifiers(RoleInfo.Role.EnquiryHandlingNotifier);

        public long[] GetEnquiryHandlingCustomerAssignerRigs() => GetIdentifiers(RoleInfo.Role.EnquiryHandlingCustomerAssigner);

        public long[] GetEnquiryHandlingProviderAssignerRigs() => GetIdentifiers(RoleInfo.Role.EnquiryHandlingProviderAssigner);

        public long[] GetEnquiryHandlingCollaboratorRigs() => GetIdentifiers(RoleInfo.Role.EnquiryHandlingCollaborator);

        public long[] GetAccessToRootCustomers() => GetIdentifiers(RoleInfo.Role.CustomerAccess);

        public long[] GetEnquiryHandlingPreparationViewerRigs() => GetIdentifiers(RoleInfo.Role.EnquiryHandlingPreparationViewer);

        public long[] GetEnquiryHandlingProviderRigs() => GetIdentifiers(RoleInfo.Role.EnquiryHandlingProvider);

        public bool HasEnquiryHandlingPreparationViewerRigs() => IsInRole(RoleInfo.Role.EnquiryHandlingPreparationViewer);

        public long[] GetEnquiryHandlingApprovedViewerRigs() => GetIdentifiers(RoleInfo.Role.EnquiryHandlingApprovedViewer);

        public bool HasEnquiryHandlingApprovedViewerRigs() => IsInRole(RoleInfo.Role.EnquiryHandlingApprovedViewer);

        public bool IsSystemAdmin() => IsInRole(RoleInfo.Role.AdminSysadmin);
        public bool IsUserAdmin() => IsInRole(RoleInfo.Role.AdminUserAdmin);
        public bool IsCustomerAdmin() => IsInRole(RoleInfo.Role.CustomerAdmin);
        public long[] GetCustomerAdminAccess() => GetIdentifiers(RoleInfo.Role.CustomerAdmin);
        public long[] GetUserAdminAccessToCustomers() => GetIdentifiers(RoleInfo.Role.AdminUserAdmin);

        public bool HasUpgradeMatrixAccess() => IsInRole(RoleInfo.Role.UpgradeMatrixViewer) || IsInRole(RoleInfo.Role.UpgradeMatrixEditor) || IsInRole(RoleInfo.Role.UpgradeMatrixAdmin);

        public bool IsUpgradeMatrixAdmin() => IsInRole(RoleInfo.Role.UpgradeMatrixAdmin);

        public long[] GetUpgradeMatrixViewAccessToRigs() => GetIdentifiers(RoleInfo.Role.UpgradeMatrixViewer);

        public long[] GetUpgradeMatrixEditorAccessToRigs() => GetIdentifiers(RoleInfo.Role.UpgradeMatrixEditor);

        public long[] GetRiconViewerAccessToRigs() => GetIdentifiers(RoleInfo.Role.CbmViewer);

        public long[] GetDocumentationViewerAccessToRigs() => GetIdentifiers(RoleInfo.Role.DocumentationViewer);

        public long[] GetBulletinViewerAccessToRigs() => GetIdentifiers(RoleInfo.Role.BulletinViewer);

        public bool HasBulletinViewerAccessToRigs() => IsInRole(RoleInfo.Role.BulletinViewer);
        
        public bool HasSparepartAccess() => IsInRole(RoleInfo.Role.SparePartViewer);
        
        public bool HasDrillPerformAccess() => IsInRole(RoleInfo.Role.SmartModuleViewer);
        
        public bool HasDrillConfigAccess() => IsInRole(RoleInfo.Role.DrillConViewer);

        public bool CanDoAdministration() => IsInRole(RoleInfo.Role.AdminSysadmin) || IsInRole(RoleInfo.Role.AdminUserAdmin) || IsInRole(RoleInfo.Role.CustomerAdmin);
        
        private bool IsInRole(RoleInfo.Role role) => _roles.TryGetValue(role, out _);

        private long[] GetIdentifiers(RoleInfo.Role role) => _roles.TryGetValue(role, out var ids) ? ids : Array.Empty<long>();
    }
}
