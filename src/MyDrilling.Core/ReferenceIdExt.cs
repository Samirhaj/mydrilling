﻿namespace MyDrilling.Core
{
    public static class ReferenceIdExt
    {
        public static bool IsPending(this string referenceId) => string.IsNullOrEmpty(referenceId);

        public static string FixPending(this string referenceId) => referenceId.IsPending() ? "Pending" : referenceId;

        public static long ToLongReferenceId(this string referenceId)
        {
            var parsed = referenceId.ToNullableLongReferenceId();
            return parsed ?? default;
        }

        public static long? ToNullableLongReferenceId(this string referenceId)
        {
            if (string.IsNullOrEmpty(referenceId))
            {
                return null;
            }

            referenceId = referenceId.Trim().TrimStart('0');
            return long.TryParse(referenceId, out long parsed) ? parsed : (long?)null;
        }

        public static string ToStringReferenceId(this long referenceId, int length) => length == 0 
            ? referenceId.ToString() 
            : referenceId.ToString($"D{length}");

        public static string ToStringReferenceId(this long? referenceId, int length)
        {
            return referenceId.HasValue ? referenceId.Value.ToStringReferenceId(length) : string.Empty;
        }
    }
}
