﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using MyDrilling.Core.Exceptions;

namespace MyDrilling.Core.Entities
{
    /// <summary>
    /// Created in MyDrilling
    /// Contains a set <see cref="Customers"/> of SAP customers
    /// In SAP we have different customer records for branches of the same company like:
    /// - customers "Maersk Drilling UK Limited", "Maersk Drilling Norge AS", "Maersk Drilling UK Limited" exist in SAP
    /// - in MyDrilling we have one "Maersk Drilling" customer which unites its branches created in SAP
    /// </summary>
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local")]
    public class RootCustomer
    {
        public const int MaxNameLength = 255;

        // ReSharper disable once UnusedMember.Local
        // for ef
        private RootCustomer() { }

        public RootCustomer(string name)
        {
            if (string.IsNullOrWhiteSpace(name)) throw new BusinessException($"{nameof(Name)} is empty");
            if (name.Length > MaxNameLength) throw new BusinessException($"{nameof(Name)} is too long: max length is {MaxNameLength}");

            Name = name;
            Created = DateTime.UnixEpoch;
            Modified = DateTime.UnixEpoch;
        }

        public long Id { get; set; }
        public string Name { get; set; }
        public string Subdomain { get; set; } //todo set from users table
        /// <summary>
        /// It's true only for MhWirth customer
        /// </summary>
        public bool IsInternal { get; set; }

        private readonly List<Customer> _customers = new List<Customer>();
        public IEnumerable<Customer> Customers => _customers.AsReadOnly();

        public long? DefaultCustomerId { get; private set; }
        public Customer DefaultCustomer { get; private set; }

        public long? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public DateTime Created { get; set; }

        public long? ModifiedById { get; set; }
        public User ModifiedBy { get; set; }
        public DateTime Modified { get; set; }

        public bool IsDeleted { get; set; }
        public bool IsHidden { get; set; }

        public void AddCustomer(Customer customer)
        {
            customer.RootCustomer = this;
            _customers.Add(customer);
        }

        public void SetDefaultCustomer(Customer customer)
        {
            AddCustomer(customer);
            DefaultCustomer = customer;
        }
    }
}
