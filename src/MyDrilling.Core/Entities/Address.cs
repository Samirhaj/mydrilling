﻿namespace MyDrilling.Core.Entities
{
    public class Address
    {
        public string City { get; set; }
        public string PostalCode { get; set; }
    }
}
