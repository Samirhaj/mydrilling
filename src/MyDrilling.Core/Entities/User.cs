﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;
using MyDrilling.Core.Entities.Permission;
using MyDrilling.Core.Entities.Subscription;
using MyDrilling.Core.Exceptions;

namespace MyDrilling.Core.Entities
{
    /// <summary>
    /// Created in MyDrilling
    /// </summary>
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local")]
    public class User
    {
        public const int MaxUpnLength = 255;
        public static readonly Regex UpnValidationPattern = new Regex(@"^\S+@\S+$", RegexOptions.Compiled);

        public static readonly User Empty = new User("unknown@unknown.com")
        {
            FirstName = "UNKNOWN",
            LastName = "UNKNOWN"
        };

        // ReSharper disable once UnusedMember.Local
        // for ef
        private User() { }

        public User(string upn)
        {
            if (string.IsNullOrWhiteSpace(upn)) throw new BusinessException($"{nameof(Upn)} is empty");
            if (upn.Length > MaxUpnLength) throw new BusinessException($"{nameof(Upn)} is too long: max length is {MaxUpnLength}");
            if (!UpnValidationPattern.IsMatch(upn)) throw new BusinessException($"Invalid {nameof(Upn)}");

            Upn = upn.Trim().ToLower();
            Email = Upn;
            Created = DateTime.UnixEpoch;
            Modified = DateTime.UnixEpoch;
        }

        public long Id { get; private set; }
        public string Upn { get; private set; }
        public string ObjectId { get; set; }
        public string TenantId { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public bool IsInternal { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public string Position { get; set; }
        public string PhoneNumber { get; set; }
        public string MobileNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string Image { get; set; }
        public string ReferenceId { get; set; }
        public DateTime? LastLogIn { get; set; }
        public DateTime? AcceptedTermsAndConditions { get; set; }

        public long? RootCustomerId { get; set; }
        public RootCustomer RootCustomer { get; set; }

        public long? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public DateTime Created { get; set; }

        public long? ModifiedById { get; set; }
        public User ModifiedBy { get; set; }
        public DateTime Modified { get; set; }

        private readonly List<UserProfile> _profiles = new List<UserProfile>();
        public IEnumerable<UserProfile> Profiles => _profiles.AsReadOnly();

        private readonly List<UserRole> _roles = new List<UserRole>();
        public IEnumerable<UserRole> Roles => _roles.AsReadOnly();

        private readonly List<Subscription.UserSetting> _subscriptionSettings = new List<Subscription.UserSetting>();
        public IEnumerable<Subscription.UserSetting> SubscriptionSettings => _subscriptionSettings.AsReadOnly();

        private readonly List<RigFilter> _subscriptionRigFilters = new List<RigFilter>();
        public IEnumerable<RigFilter> SubscriptionRigFilters => _subscriptionRigFilters.AsReadOnly();

        private readonly List<EnquiryTypeFilter> _subscriptionEnquiryTypeFilters = new List<EnquiryTypeFilter>();
        public IEnumerable<EnquiryTypeFilter> SubscriptionEnquiryTypeFilters => _subscriptionEnquiryTypeFilters.AsReadOnly();

        private readonly List<UserSetting> _settings = new List<UserSetting>();
        public IEnumerable<UserSetting> Settings => _settings.AsReadOnly();

        public void AddSetting(string key, string value)
        {
            if(string.IsNullOrEmpty(value)) throw new BusinessException($"Empty {nameof(key)}");
            if (string.IsNullOrEmpty(value)) throw new BusinessException($"Empty {nameof(value)}");

            _settings.Add(new UserSetting
            {
                UserId = Id,
                User = this,
                Key = key,
                Value = value
            });
        }

        public string GetDisplayName() => $"{FirstName} {LastName}";

        public TimeZoneInfo GetTimeZoneInfo()
        {
            var value = Settings.SingleOrDefault(x => x.Key == UserSetting.Keys.SelectedTimezone)?.Value;
            return value == null
                ? TimeZoneInfo.Utc
                : TimeZoneInfo.FindSystemTimeZoneById(value);
        }

        public bool GetShowUnreadEnquiries()
        {
            var value = Settings.SingleOrDefault(x => x.Key == UserSetting.Keys.ShowUnreadEnquiries)?.Value;
            return bool.TryParse(value, out var parsed) && parsed;
        }

        public bool GetDoNotAllowOthersToAddMeToEnquiries()
        {
            var value = Settings.SingleOrDefault(x => x.Key == UserSetting.Keys.DoNotAllowOthersToAddMeToEnquiries)?.Value;
            return bool.TryParse(value, out var parsed) && parsed;
        }
    }
}
