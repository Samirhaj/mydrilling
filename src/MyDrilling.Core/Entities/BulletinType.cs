﻿namespace MyDrilling.Core.Entities
{
    public class BulletinType
    {
        public short Id { get; set; }
        public string Name { get; set; }
        public string ConsolidatedName { get; set; }
    }
}