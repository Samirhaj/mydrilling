﻿using System;

namespace MyDrilling.Core.Entities.Bulletin
{
    public class ZbBulletin
    {
        public long Id { get; set; }
        public string ReferenceId { get; set; }
        public long ZaBulletinId { get; set; }
        public ZaBulletin ZaBulletin { get; set; }
        public long? CustomerId { get; set; }
        public Customer Customer { get; set; }
        public long? RigId { get; set; }
        public Rig.Rig Rig { get; set; }
        public long? EquipmentId { get; set; }
        public Equipment.Equipment Equipment { get; set; }
        public long? ConfirmedByUserId { get; set; }
        public User ConfirmedByUser { get; set; }
        public DateTime? ConfirmationTimestamp { get; set; }
        public BulletinGroup Group { get; set; }
        public BulletinStatus Status { get; set; }
        public BulletinState State { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
    }

    public enum BulletinStatus
    {
        None = 0,
        Na = 1,
        Implemented
    }

    public enum BulletinState
    {
        None = 0,
        Active = 1,
        Confirmed,
        ConfirmedReceived
    }

    public enum BulletinGroup
    {
        None = 0,
        Received = 1,
        Implemented
    }
}