﻿using System.Linq;

namespace MyDrilling.Core.Entities.Bulletin
{
    public static class BulletinPermissionExt
    {
        public static bool CanAccessBulletins(this CombinedUserRoles roles, long rigId = 0)
        {
            var bulletinAccessToRigs = roles.GetBulletinViewerAccessToRigs();
            return bulletinAccessToRigs.Any(d => rigId == 0 || d == rigId);
        }
        public static bool CanView(this ZbBulletin bulletin, CombinedUserRoles roles)
        {
            var rigs = roles.GetBulletinViewerAccessToRigs();

            return  bulletin.RigId.HasValue && rigs.Contains(bulletin.RigId.Value);
        }

        public static IQueryable<ZbBulletin> ApplyPermissions(this IQueryable<ZbBulletin> query, CombinedUserRoles roles)
        {
            var rigs = roles.GetBulletinViewerAccessToRigs();

            return query.Where(b => b.RigId.HasValue && rigs.Contains(b.RigId.Value));
        }
    }
}