﻿using System;

namespace MyDrilling.Core.Entities.Bulletin
{
    public class ZaBulletin
    {
        public long Id { get; set; }
        public string ReferenceId { get; set; }
        public string Title { get; set; }
        /// <summary>
        /// SAP document Identifier
        /// </summary>
        public string DocumentReferenceId { get; set; }
        public short TypeId { get; set; }
        public BulletinType Type { get; set; }
        public DateTime IssueDate { get; set; }
        public string FileType { get; set; }
        public string Description { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
    }
}