﻿namespace MyDrilling.Core.Entities.Bulletin
{
    public class BulletinTypes
    {
        public const string HseAlert = "HSE Alert";
        public const string HseBulletin = "HSE Bulletin";
        public const string HseAlertGeneral = "HSE Alert General";
        public const string HseBulletinGeneral = "HSE Bulletin General";
        public const string ProductBulletinGeneral = "Product Bulletin General";
        public const string ProductBulletin = "Product Bulletin";
    }
}