﻿using System;

namespace MyDrilling.Core.Entities
{
    /// <summary>
    /// Received from SAP, filled by integration
    /// </summary>
    public class Customer
    {
        public long Id { get; set; }

        /// <summary>
        /// SAP identifier
        /// </summary>
        public string ReferenceId { get; set; }

        public string Name { get; set; }
        public Address Address { get; set; }

        public long? RootCustomerId { get; set; }
        public RootCustomer RootCustomer { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
