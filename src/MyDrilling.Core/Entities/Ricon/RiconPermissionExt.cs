﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyDrilling.Core.Entities.Ricon
{
    public static class RiconPermissionExt
    {
        public static bool CanAccessRicon(this CombinedUserRoles roles, long rigId = 0)
        {
            var riconAccessToRigs = roles.GetRiconViewerAccessToRigs();
            return riconAccessToRigs.Any(d => rigId == 0 || d == rigId);
        }
        public static IQueryable<Joint> ApplyPermissions(this IQueryable<Joint> query, CombinedUserRoles roles)
        {
            var riconAccessToRigs = roles.GetRiconViewerAccessToRigs();
            return query.Where(j => riconAccessToRigs.Contains(j.RootEquipment.RigId));
        }
        public static IQueryable<Document> ApplyPermissions(this IQueryable<Document> query, CombinedUserRoles roles)
        {
            var riconAccessToRigs = roles.GetRiconViewerAccessToRigs();
            var riconDocumentAccessToRigs = roles.GetDocumentationViewerAccessToRigs();
            return query.Where(d => riconAccessToRigs.Contains( d.JointId.HasValue 
                ? d.Joint.RootEquipment.RigId 
                : d.RootEquipment.RigId) 
                && riconDocumentAccessToRigs.Contains(d.JointId.HasValue
                    ? d.Joint.RootEquipment.RigId
                    : d.RootEquipment.RigId));
        }
    }
}
