﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyDrilling.Core.Entities.Ricon
{
    public class RootEquipment
    {
        public long Id { get; set; }
        public long RigId { get; set; }
        public Rig.Rig Rig { get; set; }
        public long ReferenceId { get; set; }
    }
}
