﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyDrilling.Core.Entities.Ricon
{
    public class Document
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public long? RootEquipmentId { get; set; }
        public RootEquipment RootEquipment { get; set; }
        public long? JointReferenceId { get; set; }
        public long? JointId { get; set; }
        public Joint Joint { get; set; }
        public long ReferenceId { get; set; }
        public string Version { get; set; }
        public string Url { get; set; }
        public DateTime PublishedDate { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime LastSeen { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
    }
}
