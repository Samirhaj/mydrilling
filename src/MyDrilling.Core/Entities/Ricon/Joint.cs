﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MyDrilling.Core.Entities.Ricon
{
    public class Joint
    {
        public long Id { get; set; }
        public long RootEquipmentId { get; set; }
        public RootEquipment RootEquipment { get; set; }
        public long ReferenceId { get; set; }
        public string TagNumber { get; set; }
        public string Fatigue { get; set; }
        public string Coc { get; set; }
        public string Status { get; set; }
        public DateTime? LastVisualInspection { get; set; }
        public DateTime? LastWallThicknessInspection { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime LastSeen { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
    }
}
