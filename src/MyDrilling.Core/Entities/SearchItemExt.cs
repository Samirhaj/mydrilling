﻿using System;
using System.Collections.Generic;
using System.Text;
using MyDrilling.Core.Entities.Bulletin;
using MyDrilling.Core.Entities.Documentation;


namespace MyDrilling.Core.Entities
{
    public static class SearchItemExt
    {
        public static readonly string IdSeparator = "_";
        public static SearchItem ToSearchItem(this Enquiry.Enquiry enquiry)
        {
            return new SearchItem
            {
                Id = nameof(Enquiry) + IdSeparator + enquiry.Id,
                EntityId = enquiry.Id,
                EntityType = nameof(Enquiry),
                RigId = enquiry.RigId.ToString(),
                Name = enquiry.Title,
                Description = enquiry.Description,
                ReferenceId = enquiry.ReferenceId
            };
        }

        public static SearchItem ToSearchItem(this TechnicalReport techReport)
        {
            return new SearchItem
            {
                Id = nameof(TechnicalReport) + IdSeparator + techReport.Id,
                EntityId = techReport.Id,
                EntityType = nameof(TechnicalReport),
                RigId = techReport.RigId.HasValue ? techReport.RigId.Value.ToString() : "0",
                Name = techReport.Name,
                Description = techReport.DocumentReferenceId.ToString(),
                ReferenceId = techReport.ReferenceId
            };
        }
        public static SearchItem ToSearchItem(this UserManual userManual)
        {
            return new SearchItem
            {
                Id = nameof(UserManual) + IdSeparator + userManual.Id,
                EntityId = userManual.Id,
                EntityType = nameof(UserManual),
                RigId = userManual.RigId.HasValue ? userManual.RigId.Value.ToString() : "0",
                Name = userManual.Name,
                ReferenceId = userManual.ReferenceId
            };
        }
    }
}
