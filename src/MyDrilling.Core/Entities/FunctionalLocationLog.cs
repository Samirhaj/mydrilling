﻿using System;

namespace MyDrilling.Core.Entities
{
    /// <summary>
    /// Received from SAP, filled by integration
    /// </summary>
    public class FunctionalLocationLog
    {
        public static readonly FunctionalLocationLog Invalid = new FunctionalLocationLog
        {
            Id = -1,
            EquipmentId = -1,
            CustomerId = -1,
            RigId = -1,
            ProductCode = string.Empty
        };

        public long Id { get; set; }

        public long EquipmentId { get; set; }
        public Equipment.Equipment Equipment { get; set; }

        public long CustomerId { get; set; }
        public Customer Customer { get; set; }

        public long RigId { get; set; }
        public Rig.Rig Rig { get; set; }

        public string ProductCode { get; set; }

        public DateTime EffectiveDate { get; set; }
        public DateTime Created { get; set; }
    }
}
