﻿namespace MyDrilling.Core.Entities.AsyncOperation
{
    public enum AsyncOperationStatus
    {
        None = 0,
        Requested = 1,
        Succeeded = 2,
        Failed = 3
    }
}