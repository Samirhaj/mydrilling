﻿using System;

namespace MyDrilling.Core.Entities.AsyncOperation
{
    public class AsyncOperation
    {
        //for ef
        private AsyncOperation() { }

        public AsyncOperation(User initiator)
        {
            var now = DateTime.UtcNow;
            Status = AsyncOperationStatus.Requested;
            CreatedAt = now;
            UpdatedAt = now;
            UserId = initiator.Id;
        }

        public long Id { get; private set; }
        public long UserId { get; private set; }
        public User User { get; private set; }
        public string Response { get; private set; }
        public DateTime CreatedAt { get; private set; }
        public DateTime UpdatedAt { get; private set; }
        public AsyncOperationStatus Status { get; private set; }

        public void Fail(string response)
        {
            Status = AsyncOperationStatus.Failed;
            Response = response;
            UpdatedAt = DateTime.UtcNow;
        }

        public void Succeed(string response)
        {
            Status = AsyncOperationStatus.Succeeded;
            Response = response;
            UpdatedAt = DateTime.UtcNow;
        }
    }
}