﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace MyDrilling.Core.Entities
{
    public class SearchItem
    {
        [JsonPropertyName("Id")]
        public string Id { get; set; }

        [JsonPropertyName("EntityId")]
        public long EntityId { get; set; }

        [JsonPropertyName("EntityType")]
        public string EntityType { get; set; }

        [JsonPropertyName("RigId")]
        public string RigId { get; set; }

        [JsonPropertyName("Name")]
        public string Name { get; set; }

        [JsonPropertyName("Description")]
        public string Description { get; set; }

        [JsonPropertyName("ReferenceId")]
        public string ReferenceId { get; set; }
    }
}
