﻿namespace MyDrilling.Core.Entities
{
    public class UserSetting
    {
        public long Id { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }

        public long UserId { get; set; }
        public User User { get; set; }

        public static class Keys
        {
            public const string SelectedTimezone = "SelectedTimezone";
            public const string DoNotAllowOthersToAddMeToEnquiries = "DontAllowOthersToAddMeToEnquiries";
            public const string ShowUnreadEnquiries = "ShowUnreadEnquiries";
        }
    }
}
