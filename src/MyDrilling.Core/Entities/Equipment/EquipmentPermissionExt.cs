﻿using System.Linq;

namespace MyDrilling.Core.Entities.Equipment
{
    public static class EquipmentPermissionExt
    {
        public static IQueryable<Equipment> ApplyPermission(this IQueryable<Equipment> query, CombinedUserRoles roles)
        {
            var accessToRigs = roles.GetViewAccessToRigs();
            return query.Where(x => accessToRigs.Contains(x.RigId.Value));
        }
    }
}
