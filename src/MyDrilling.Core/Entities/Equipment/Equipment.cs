﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MyDrilling.Core.Entities.Equipment
{
    /// <summary>
    /// Received from SAP, filled by integration
    /// </summary>
    public class Equipment
    {
        public const int SapReferenceLength = 18;

        public long Id { get; set; }

        /// <summary>
        /// SAP identifier
        /// </summary>
        public long ReferenceId { get; set; }

        public string Name { get; set; }
        public string SerialNumber { get; set; }
        public string TagNumber { get; set; }
        public string Material { get; set; }

        /// <summary>
        /// Received from SAP
        /// </summary>
        public long? ParentReferenceId { get; set; }
        /// <summary>
        /// Set by consistency checker
        /// </summary>
        public long? ParentId { get; set; }
        public Equipment Parent { get; set; }

        private readonly HashSet<Equipment> _children = new HashSet<Equipment>();
        public IEnumerable<Equipment> Children => _children.ToList().AsReadOnly();

        /// <summary>
        /// Calculated by Sesam
        /// </summary>
        public bool IsDeleted { get; set; }
        /// <summary>
        /// Received from SAP
        /// </summary>
        public string ProductLongReferenceId { get; set; }
        /// <summary>
        /// Received from SAP
        /// </summary>
        public string ProductShortReferenceId { get; set; }
        /// <summary>
        /// Received from SAP
        /// </summary>
        public string ProductName { get; set; }
        /// <summary>
        /// Received from SAP
        /// </summary>
        public string ProductGroup { get; set; }
        /// <summary>
        /// Set by consistency checker
        /// </summary>
        public string ProductCode { get; set; }

        /// <summary>
        /// Received from SAP
        /// </summary>
        public long? RigReferenceId { get; set; }
        /// <summary>
        /// Set by consistency checker
        /// </summary>
        public long? RigId { get; set; }
        public Rig.Rig Rig { get; set; }

        /// <summary>
        /// Received from SAP
        /// </summary>
        public string RigOwnerReferenceId { get; set; }
        /// <summary>
        /// Set by consistency checker
        /// </summary>
        public long? RigOwnerId { get; set; }
        public Customer RigOwner { get; set; }

        public DateTime LastSeen { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }

        public void AddChild(Equipment child)
        {
            child.Parent = this;
            _children.Add(child);
        }

        public bool IsLocated() => RigOwnerId != null && !string.IsNullOrEmpty(ProductCode) && RigId != null;

        public FunctionalLocationLog CreateFunctionalLocation()
        {
            if (!IsLocated())
            {
                return FunctionalLocationLog.Invalid;
            }

            return new FunctionalLocationLog
            {
                EquipmentId = Id,
                CustomerId = RigOwnerId.Value,
                RigId = RigId.Value,
                ProductCode = ProductCode,
                EffectiveDate = LastSeen,
                Created = DateTime.UtcNow
            };
        }

        public bool IsTheSame(FunctionalLocationLog lastLocation)
        {
            return lastLocation != null
                   && lastLocation.CustomerId == RigOwnerId
                   && lastLocation.ProductCode == ProductCode
                   && lastLocation.RigId == RigId;
        }

        public bool IsOutdated(FunctionalLocationLog lastLocation)
        {
            return lastLocation != null
                   && LastSeen < lastLocation.EffectiveDate;
        }

        public string NameFormatted => Name.ToCamelCase();
        public string ProductNameFormatted => ProductName.ToCamelCase();
    }
}
