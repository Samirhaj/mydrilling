﻿using System.Collections.Generic;
using System.Linq;

namespace MyDrilling.Core.Entities
{
    public class RigTypeInfo
    {
        public RigTypeInfo(string code, string name)
        {
            Code = code;
            Name = name;
        }

        public string Code { get; }
        public string Name { get; }

        public static RigTypeInfo[] Types = 
        {
            new RigTypeInfo("DS", "Drill Ships"),
            new RigTypeInfo("FI", "Fixed Installation"),
            new RigTypeInfo("IB", "Inland Barge"),
            new RigTypeInfo("INV", "Intervention Tooling"),
            new RigTypeInfo("JU", "Jack-Up"),
            new RigTypeInfo("LR", "Land Rig"),
            new RigTypeInfo("MIS", "MISC Tooling"),
            new RigTypeInfo("NA", "Not Applicable"),
            new RigTypeInfo("PM", "Plant Maintenance"),
            new RigTypeInfo("PP", "Production Platform"),
            new RigTypeInfo("PS", "Production Ship"),
            new RigTypeInfo("SCS", "Subsea Control Syste"),
            new RigTypeInfo("SS", "Semisubmersible"),
            new RigTypeInfo("TB", "Tender Barge"),
            new RigTypeInfo("TIN", "Tie In Tooling"),
            new RigTypeInfo("TLP", "Tension Leg Platform"),
            new RigTypeInfo("TRN", "Transportation Equip"),
            new RigTypeInfo("TS", "Tender Support"),
            new RigTypeInfo("WEL", "Wellhead Tooling"),
            new RigTypeInfo("WO", "Workover rig"),
            new RigTypeInfo("WOS", "WOCS"),
            new RigTypeInfo("WRL", "Wireline Tooling"),
            new RigTypeInfo("XME", "Christmas Equipment"),
            new RigTypeInfo("XMT", "XT Tooling"),
            new RigTypeInfo("XX", "Inactive"),
            new RigTypeInfo("FPSO", "Floating production, storage and offloading"),
            new RigTypeInfo("MR", "")
        };

        public static readonly IReadOnlyDictionary<string, RigTypeInfo> ByCode = Types.ToDictionary(x => x.Code, x => x);
    }
}
