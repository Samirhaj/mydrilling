﻿using System;

namespace MyDrilling.Core.Entities
{
    public class SapConnectorStatus
    {
        public long Id { get; set; }
        public DateTime Timestamp { get; set; }
        public bool SapState { get; set; }
        public bool QueuesState { get; set; }
        public bool BlobsState { get; set; }

        public bool IsDbOk(TimeSpan threshold) => DateTime.UtcNow.Subtract(Timestamp) < threshold;
        public bool IsSapOk(TimeSpan threshold) => SapState && DateTime.UtcNow.Subtract(Timestamp) < threshold;
        public bool IsQueuesOk(TimeSpan threshold) => QueuesState && DateTime.UtcNow.Subtract(Timestamp) < threshold;
        public bool IsBlobsOk(TimeSpan threshold) => BlobsState && DateTime.UtcNow.Subtract(Timestamp) < threshold;
    }
}
