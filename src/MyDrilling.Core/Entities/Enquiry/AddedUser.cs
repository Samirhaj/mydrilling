﻿using System;

namespace MyDrilling.Core.Entities.Enquiry
{
    public class AddedUser
    {
        public long Id { get; set; }

        public long EnquiryId { get; set; }
        public Enquiry Enquiry { get; set; }

        public long UserId { get; set; }
        public User User { get; set; }

        public long? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public DateTime Created { get; set; }
    }
}
