﻿using System;

namespace MyDrilling.Core.Entities.Enquiry
{
    public partial class Enquiry
    {
        [Obsolete("It is used only for migration only")]
        public Enquiry() { }

        [Obsolete("It is used only for migration only")]
        public void SetLastCommented(DateTime dt)
        {
            LastCommented = dt;
            if (dt > LastActivity)
            {
                LastActivity = dt;
            }
        }

        [Obsolete("It is used only for migration only")]
        public void SetCreated(DateTime created)
        {
            Created = created;
            Modified = created;
            LastActivity = created;
        }

        [Obsolete("It is used only for migration only")]
        public void ChangeState(StateInfo.State state)
        {
            State = state;

            switch (state)
            {
                case StateInfo.State.Draft:
                case StateInfo.State.Rejected:
                case StateInfo.State.ReadyForApproval:
                    AssignedToCustomer = Supplier;
                    LogicalState = LogicalStateInfo.LogicalState.Draft;
                    break;
                case StateInfo.State.AssignedProvider:
                    AssignedToCustomer = Supplier;
                    LogicalState = LogicalStateInfo.LogicalState.Open;
                    break;
                case StateInfo.State.AssignedCustomer:
                    AssignedToCustomer = RootCustomer;
                    LogicalState = LogicalStateInfo.LogicalState.Open;
                    break;
                case StateInfo.State.SuggestedClosed:
                    AssignedToCustomer = Supplier;
                    LogicalState = LogicalStateInfo.LogicalState.Open;
                    break;
                case StateInfo.State.Deleted:
                    AssignedToCustomer = Supplier;
                    LogicalState = LogicalStateInfo.LogicalState.Deleted;
                    break;
                case StateInfo.State.Closed:
                    AssignedToCustomer = Supplier;
                    LogicalState = LogicalStateInfo.LogicalState.Closed;
                    break;
            }
        }

        [Obsolete("It is used only for migration only")]
        public void AddComment(Comment comment)
        {
            comment.Enquiry = this;
            comment.IsInitial = false;
            LastCommented = comment.Created;
            _comments.Add(comment);
        }

        [Obsolete("It is used only for migration only")] public void SetTitle(string title) => Title = title;
        [Obsolete("It is used only for migration only")] public void SetDescription(string description) => Description = description;
        [Obsolete("It is used only for migration only")] public void SetCreatedBy(User user) => CreatedBy = user;
        [Obsolete("It is used only for migration only")] public void SetModifiedBy(User user) => ModifiedBy = user;
        [Obsolete("It is used only for migration only")] public void SetApprovedBy(User user) => ApprovedBy = user;
        [Obsolete("It is used only for migration only")] public void SetAssignedBy(User user) => AssignedBy = user;
        [Obsolete("It is used only for migration only")] public void SetAssignedTo(User user) => AssignedTo = user;
        [Obsolete("It is used only for migration only")] public void SetRejectedBy(User user) => RejectedBy = user;
        [Obsolete("It is used only for migration only")] public void SetModified(DateTime modified) => Modified = modified;
        [Obsolete("It is used only for migration only")] public void SetApproved(DateTime? approved) => Approved = approved;
        [Obsolete("It is used only for migration only")] public void SetRig(Rig.Rig rig) => Rig = rig;
        [Obsolete("It is used only for migration only")] public void SetRootCustomer(RootCustomer rootCustomer) => RootCustomer = rootCustomer;
        [Obsolete("It is used only for migration only")] public void SetCustomer(Customer customer) => Customer = customer;
        [Obsolete("It is used only for migration only")] public void SetRigOwner(Customer rigOwner) => RigOwner = rigOwner;
        [Obsolete("It is used only for migration only")] public void SetSupplier(RootCustomer supplier) => Supplier = supplier;
        [Obsolete("It is used only for migration only")] public void SetEquipment(Equipment.Equipment equipment) => Equipment = equipment;
        [Obsolete("It is used only for migration only")] public void SetType(TypeInfo.Type type) => Type = type;
        [Obsolete("It is used only for migration only")] public void SetReferenceIdObsolete(string referenceId) => ReferenceId = referenceId;
        [Obsolete("It is used only for migration only")] public void SetSubmitted(DateTime? approvedDt, DateTime created) => Submitted = approvedDt ?? created;
        [Obsolete("It is used only for migration only")] public void SetVersion(long version) => Version = version;
        [Obsolete("It is used only for migration only")] public void SetDate(DateTime dt) => Date = dt;
    }
}
