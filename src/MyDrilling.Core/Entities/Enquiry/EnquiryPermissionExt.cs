﻿using System.Linq;

namespace MyDrilling.Core.Entities.Enquiry
{
    public static class EnquiryPermissionExt
    {
        public static bool CanCreateDraft(this CombinedUserRoles roles, long rootCustomerId = 0, long rigId = 0)
        {
            var availableRootCustomers = roles.GetAccessToRootCustomers();
            var initiatorRigs = roles.GetEnquiryHandlingInitiatorRigs();

            return (rootCustomerId == 0 && availableRootCustomers.Any()
                    ||
                    rootCustomerId > 0 && availableRootCustomers.Contains(rootCustomerId))
                   &&
                   (rigId == 0 && initiatorRigs.Any()
                    ||
                    rigId > 0 && initiatorRigs.Contains(rigId));
        }

        public static bool CanCreateOpen(this CombinedUserRoles roles, long rootCustomerId = 0, long rigId = 0)
        {
            var availableRootCustomers = roles.GetAccessToRootCustomers();
            var approverRigs = roles.GetEnquiryHandlingApproverRigs();

            return (rootCustomerId == 0 && availableRootCustomers.Any()
                    ||
                    rootCustomerId > 0 && availableRootCustomers.Contains(rootCustomerId))
                   &&
                   (rigId == 0 && approverRigs.Any()
                    ||
                    rigId > 0 && approverRigs.Contains(rigId));
        }

        public static bool CanCreateDraftOnly(this CombinedUserRoles roles, long rootCustomerId = 0, long rigId = 0) =>
            roles.CanCreateDraft(rootCustomerId, rigId) && !roles.CanCreateOpen(rootCustomerId, rigId);

        public static bool CanCreate(this CombinedUserRoles roles, long rootCustomerId = 0, long rigId = 0) =>
            roles.CanCreateDraft(rootCustomerId, rigId) || roles.CanCreateOpen(rootCustomerId, rigId);

        public static bool CanEdit(this Enquiry enquiry, CombinedUserRoles roles)
        {
            return (enquiry.State == StateInfo.State.Draft
                    && roles.GetEnquiryHandlingInitiatorRigs().Contains(enquiry.RigId)
                    && roles.GetEnquiryHandlingPreparationViewerRigs().Contains(enquiry.RigId)
                    && enquiry.RootCustomerId.HasValue && roles.GetAccessToRootCustomers().Contains(enquiry.RootCustomerId.Value))
                   ||
                   (enquiry.State == StateInfo.State.ReadyForApproval
                    && roles.GetEnquiryHandlingPreparationViewerRigs().Contains(enquiry.RigId)
                    && roles.GetEnquiryHandlingApproverRigs().Contains(enquiry.RigId)
                    && enquiry.RootCustomerId.HasValue && roles.GetAccessToRootCustomers().Contains(enquiry.RootCustomerId.Value));
        }

        public static bool CanApprove(this Enquiry enquiry, CombinedUserRoles roles)
        {
            return enquiry.State == StateInfo.State.ReadyForApproval
                   && roles.GetEnquiryHandlingPreparationViewerRigs().Contains(enquiry.RigId)
                   && roles.GetEnquiryHandlingApproverRigs().Contains(enquiry.RigId)
                   && enquiry.RootCustomerId.HasValue && roles.GetAccessToRootCustomers().Contains(enquiry.RootCustomerId.Value);
        }

        public static bool CanSendForApproval(this Enquiry enquiry, CombinedUserRoles roles)
        {
            return enquiry.State == StateInfo.State.Draft
                   && roles.GetEnquiryHandlingInitiatorRigs().Contains(enquiry.RigId)
                   && roles.GetEnquiryHandlingPreparationViewerRigs().Contains(enquiry.RigId)
                   && enquiry.RootCustomerId.HasValue && roles.GetAccessToRootCustomers().Contains(enquiry.RootCustomerId.Value);
        }

        public static bool CanReject(this Enquiry enquiry, CombinedUserRoles roles)
        {
            return enquiry.State == StateInfo.State.ReadyForApproval
                   && roles.GetEnquiryHandlingPreparationViewerRigs().Contains(enquiry.RigId)
                   && roles.GetEnquiryHandlingApproverRigs().Contains(enquiry.RigId)
                   && enquiry.RootCustomerId.HasValue && roles.GetAccessToRootCustomers().Contains(enquiry.RootCustomerId.Value);
        }

        public static bool CanDelete(this Enquiry enquiry, CombinedUserRoles roles)
        {
            return (enquiry.State == StateInfo.State.Draft
                    && roles.GetEnquiryHandlingInitiatorRigs().Contains(enquiry.RigId)
                    && roles.GetEnquiryHandlingPreparationViewerRigs().Contains(enquiry.RigId)
                    && enquiry.RootCustomerId.HasValue && roles.GetAccessToRootCustomers().Contains(enquiry.RootCustomerId.Value))
                    ||
                    (enquiry.State == StateInfo.State.ReadyForApproval
                     && roles.GetEnquiryHandlingPreparationViewerRigs().Contains(enquiry.RigId)
                     && roles.GetEnquiryHandlingApproverRigs().Contains(enquiry.RigId)
                     && enquiry.RootCustomerId.HasValue && roles.GetAccessToRootCustomers().Contains(enquiry.RootCustomerId.Value))
                    ||
                    (enquiry.State == StateInfo.State.Rejected
                     && roles.GetEnquiryHandlingInitiatorRigs().Contains(enquiry.RigId)
                     && roles.GetEnquiryHandlingPreparationViewerRigs().Contains(enquiry.RigId)
                     && enquiry.RootCustomerId.HasValue && roles.GetAccessToRootCustomers().Contains(enquiry.RootCustomerId.Value));
        }

        public static bool CanAssignToCustomer(this Enquiry enquiry, CombinedUserRoles roles)
        {
            return (enquiry.State == StateInfo.State.AssignedProvider || enquiry.State == StateInfo.State.SuggestedClosed)
                   && enquiry.RootCustomerId.HasValue && roles.GetAccessToRootCustomers().Contains(enquiry.RootCustomerId.Value)
                   && roles.GetEnquiryHandlingApprovedViewerRigs().Contains(enquiry.RigId)
                   && roles.GetEnquiryHandlingProviderRigs().Contains(enquiry.RigId);
        }

        public static bool CanAssignToProvider(this Enquiry enquiry, CombinedUserRoles roles)
        {
            return enquiry.State == StateInfo.State.AssignedCustomer
                   && enquiry.RootCustomerId.HasValue && roles.GetAccessToRootCustomers().Contains(enquiry.RootCustomerId.Value)
                   && roles.GetEnquiryHandlingApprovedViewerRigs().Contains(enquiry.RigId)
                   && roles.GetEnquiryHandlingApproverRigs().Contains(enquiry.RigId);
        }

        public static bool CanAssignWithinCustomer(this Enquiry enquiry, CombinedUserRoles roles)
        {
             return enquiry.State == StateInfo.State.AssignedCustomer
                    && roles.GetEnquiryHandlingCustomerAssignerRigs().Contains(enquiry.RigId)
                    && roles.GetEnquiryHandlingApprovedViewerRigs().Contains(enquiry.RigId)
                    && enquiry.RootCustomerId.HasValue && roles.GetAccessToRootCustomers().Contains(enquiry.RootCustomerId.Value);
        }

        public static bool CanAssignWithinProvider(this Enquiry enquiry, CombinedUserRoles roles)
        {
            return enquiry.State == StateInfo.State.AssignedProvider
                   && roles.GetEnquiryHandlingProviderAssignerRigs().Contains(enquiry.RigId)
                   && roles.GetEnquiryHandlingApprovedViewerRigs().Contains(enquiry.RigId)
                   && enquiry.RootCustomerId.HasValue && roles.GetAccessToRootCustomers().Contains(enquiry.RootCustomerId.Value);
        }

        public static bool CanSuggestToClose(this Enquiry enquiry, CombinedUserRoles roles)
        {
            return enquiry.State == StateInfo.State.AssignedCustomer
                   && roles.GetEnquiryHandlingApprovedViewerRigs().Contains(enquiry.RigId)
                   && roles.GetEnquiryHandlingApproverRigs().Contains(enquiry.RigId)
                   && enquiry.RootCustomerId.HasValue && roles.GetAccessToRootCustomers().Contains(enquiry.RootCustomerId.Value);
        }

        public static bool CanAddComment(this Enquiry enquiry, CombinedUserRoles roles)
        {
            return (enquiry.State == StateInfo.State.Draft
                    && roles.GetEnquiryHandlingInitiatorRigs().Contains(enquiry.RigId)
                    && roles.GetEnquiryHandlingPreparationViewerRigs().Contains(enquiry.RigId)
                    && enquiry.RootCustomerId.HasValue && roles.GetAccessToRootCustomers().Contains(enquiry.RootCustomerId.Value)) 
                    || 
                    (enquiry.State == StateInfo.State.ReadyForApproval
                     && roles.GetEnquiryHandlingPreparationViewerRigs().Contains(enquiry.RigId)
                     && roles.GetEnquiryHandlingApproverRigs().Contains(enquiry.RigId)
                     && enquiry.RootCustomerId.HasValue && roles.GetAccessToRootCustomers().Contains(enquiry.RootCustomerId.Value)) 
                    || 
                    ((enquiry.State == StateInfo.State.AssignedProvider || enquiry.State == StateInfo.State.AssignedCustomer || enquiry.State == StateInfo.State.SuggestedClosed)
                     && enquiry.RootCustomerId.HasValue && roles.GetAccessToRootCustomers().Contains(enquiry.RootCustomerId.Value)
                     && roles.GetEnquiryHandlingApprovedViewerRigs().Contains(enquiry.RigId)
                     && roles.GetEnquiryHandlingCollaboratorRigs().Contains(enquiry.RigId));
        }

        public static bool CanAddAttachment(this Enquiry enquiry, CombinedUserRoles roles) => enquiry.CanAddComment(roles);

        public static bool CanDeleteComment(this Enquiry enquiry, CombinedUserRoles roles) => enquiry.CanEdit(roles);

        public static bool CanDeleteAttachment(this Enquiry enquiry, CombinedUserRoles roles) => enquiry.CanEdit(roles);

        public static bool CanReopen(this Enquiry enquiry, CombinedUserRoles roles)
        {
            return enquiry.State == StateInfo.State.Rejected
                   && roles.GetEnquiryHandlingInitiatorRigs().Contains(enquiry.RigId)
                   && roles.GetEnquiryHandlingPreparationViewerRigs().Contains(enquiry.RigId)
                   && enquiry.RootCustomerId.HasValue && roles.GetAccessToRootCustomers().Contains(enquiry.RootCustomerId.Value);
        }

        public static bool CanAddPersonnel(this Enquiry enquiry, CombinedUserRoles roles) => roles.GetEnquiryHandlingNotifierRigs().Contains(enquiry.RigId);

        public static bool CanDeletePersonnel(this Enquiry enquiry, CombinedUserRoles roles) => roles.GetEnquiryHandlingApproverRigs().Contains(enquiry.RigId);

        public static bool CanSetAssignee(this Enquiry enquiry, CombinedUserRoles roles) => enquiry.CanAssignWithinCustomer(roles) || enquiry.CanAssignWithinProvider(roles);

        public static bool CanRemoveAssignee(this Enquiry enquiry, CombinedUserRoles roles) => enquiry.AssignedToId > 0 && enquiry.CanSetAssignee(roles);

        public static bool CanRead(this Enquiry enquiry, CombinedUserRoles roles)
        {
            var accessToCustomers = roles.GetAccessToRootCustomers();
            var enquiryHandlingApprovedViewerRigs = roles.GetEnquiryHandlingApprovedViewerRigs();
            var enquiryHandlingPreparationViewerRigs = roles.GetEnquiryHandlingPreparationViewerRigs();

            return enquiry.RootCustomerId.HasValue && accessToCustomers.Contains(enquiry.RootCustomerId.Value)
                &&
                (
                  enquiryHandlingPreparationViewerRigs.Contains(enquiry.RigId) && StateInfo.NotPublishedStates.Contains(enquiry.State)
                  ||
                  enquiryHandlingApprovedViewerRigs.Contains(enquiry.RigId) && StateInfo.PublishedStates.Contains(enquiry.State)
                );
        }

        public static IQueryable<Enquiry> ApplyPermissions(this IQueryable<Enquiry> query, CombinedUserRoles roles)
        {
            var accessToCustomers = roles.GetAccessToRootCustomers();
            var enquiryHandlingApprovedViewerRigs = roles.GetEnquiryHandlingApprovedViewerRigs();
            var enquiryHandlingPreparationViewerRigs = roles.GetEnquiryHandlingPreparationViewerRigs();

            return query.Where(e => e.RootCustomerId.HasValue && accessToCustomers.Contains(e.RootCustomerId.Value)
                &&
                (
                    enquiryHandlingPreparationViewerRigs.Contains(e.RigId) && StateInfo.NotPublishedStates.Contains(e.State)
                    ||
                    enquiryHandlingApprovedViewerRigs.Contains(e.RigId) && StateInfo.PublishedStates.Contains(e.State)
                ));
        }
    }
}
