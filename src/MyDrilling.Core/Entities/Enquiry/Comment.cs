﻿using System;
using System.Collections.Generic;

namespace MyDrilling.Core.Entities.Enquiry
{
    public class Comment
    {
        //for ef and migrator
        public Comment() { }

        public Comment(string text, User creator)
        {
            var now = DateTime.UtcNow;
            Text = text;
            Title = $"Comment added by {creator.GetDisplayName()}";
            Created = now;
            Submitted = now;
            CreatedBy = creator;
            IsInitial = false;
        }

        public long Id { get; set; }
        public string ReferenceId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        /// <summary>
        /// If IsInitial=true the comment should be hidden from UI because SAP creates 1st comment for each enquiry automatically with the same content as in enquiry
        /// </summary>
        public bool IsInitial { get; set; }

        private readonly List<Attachment> _attachments = new List<Attachment>();
        public IEnumerable<Attachment> Attachments => _attachments.AsReadOnly();

        public void AddAttachment(Attachment file)
        {
            file.Comment = this;
            file.Enquiry = Enquiry;
            _attachments.Add(file);
        }

        public long? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public DateTime Submitted { get; set; }

        public long EnquiryId { get; set; }
        public Enquiry Enquiry { get; set; }
    }
}
