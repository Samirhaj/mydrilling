﻿using System;

namespace MyDrilling.Core.Entities.Enquiry
{
    public class ReadReceipt
    {
        public long Id { get; set; }
        public DateTime Created { get; set; }

        public long EnquiryId { get; set; }
        public Enquiry Enquiry { get; set; }

        public long UserId { get; set; }
        public User User { get; set; }
    }
}
