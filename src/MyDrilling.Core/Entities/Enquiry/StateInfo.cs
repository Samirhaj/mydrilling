﻿using System.Collections.Generic;
using System.Linq;

namespace MyDrilling.Core.Entities.Enquiry
{
    public class StateInfo
    {
        public StateInfo(State value, string title)
        {
            Value = value;
            Title = title;
        }

        public State Value { get; }
        public string Title { get; }

        public enum State
        {
            /// <summary>
            /// Enquiry exists only in MyDrilling database, <see cref="Enquiry.ReferenceId"/> is null, enquiry hasn't been sent to SAP yet
            /// </summary>
            Draft = 1,
            AssignedProvider = 2,
            AssignedCustomer = 3,
            Rejected = 4,
            SuggestedClosed = 5,
            Closed = 6,
            Deleted = 7,
            ReadyForApproval = 8
        }

        public static StateInfo[] States = 
        {
            new StateInfo(State.Draft, "Draft"),
            new StateInfo(State.AssignedProvider, "Assigned to MHWirth"),
            new StateInfo(State.AssignedCustomer, "Assigned to customer"),
            new StateInfo(State.Rejected, "Rejected"),
            new StateInfo(State.SuggestedClosed, "Suggested closed"),
            new StateInfo(State.Closed, "Closed"),
            new StateInfo(State.Deleted, "Deleted"),
            new StateInfo(State.ReadyForApproval, "Ready for approval")
        };

        public static IReadOnlyDictionary<State, StateInfo> ByState = States.ToDictionary(x => x.Value, x => x);

        public static StateInfo[] Published =
        {
            ByState[State.AssignedProvider],
            ByState[State.AssignedCustomer],
            ByState[State.SuggestedClosed],
            ByState[State.Closed]
        };

        public static StateInfo[] NotPublished =
        {
            ByState[State.Draft],
            ByState[State.Rejected],
            ByState[State.Deleted],
            ByState[State.ReadyForApproval]
        };

        public static readonly State[] NotPublishedStates = NotPublished.Select(x => x.Value).ToArray();

        public static readonly State[] PublishedStates = Published.Select(x => x.Value).ToArray();
    }
}
