﻿using System.Collections.Generic;
using System.Linq;

namespace MyDrilling.Core.Entities.Enquiry
{
    public class TypeInfo
    {
        public TypeInfo(Type type, string title, string category, bool canHaveEquipment)
        {
            Id = (int)type;
            Value = type;
            Title = title;
            Category = category;
            CanHaveEquipment = canHaveEquipment;
        }

        public int Id { get; }
        public string Title { get;}
        public string Category { get; }
        public Type Value { get; }
        public bool CanHaveEquipment { get; }
        
        public enum Type
        {
            // ReSharper disable InconsistentNaming

            Support_Service = 1,
            Support_RigOptimization = 2,
            Support_RigOnDowntime = 3,
            Support_Overhaul = 4,
            Support_Training = 5,
            Support_RemoteDiagnostics = 6,
            Support_BulletinSupport = 7,
            Support_General = 8,
            Sparepart_IdentifyPartNumber = 9,
            Sparepart_PriceAndAvailability = 10,
            Sparepart_Order = 11,
            Sparepart_General = 12,
            Other_RigMove = 13,
            Other_EquipmentMove = 14,
            Internal_Internal = 15,
            Support_ProductImprovement = 16

            // ReSharper restore InconsistentNaming
        }

        //internal type shouldn't be here
        public static readonly TypeInfo[] Types =
        {
            new TypeInfo(Type.Support_Service, "Service", "Support", true),
            new TypeInfo(Type.Support_RigOptimization, "Rig optimization", "Support", true),
            new TypeInfo(Type.Support_RigOnDowntime, "Rig on downtime", "Support", true),
            new TypeInfo(Type.Support_Overhaul, "Overhaul", "Support", true),
            new TypeInfo(Type.Support_Training, "Training", "Support", true),
            new TypeInfo(Type.Support_RemoteDiagnostics, "Remote diagnostics", "Support", true),
            new TypeInfo(Type.Support_BulletinSupport, "Bulletin support", "Support", true),
            new TypeInfo(Type.Support_General, "General support", "Support", true),
            new TypeInfo(Type.Sparepart_IdentifyPartNumber, "Identify part-number", "Sparepart", true),
            new TypeInfo(Type.Sparepart_PriceAndAvailability, "Price and availability", "Sparepart", true),
            new TypeInfo(Type.Sparepart_Order, "Order", "Sparepart", true),
            new TypeInfo(Type.Sparepart_General, "General sparepart", "Sparepart", true),
            new TypeInfo(Type.Other_RigMove, "Rig move", "Rig move", false),
            new TypeInfo(Type.Other_EquipmentMove, "Equipment move", "Report new equipment location", true),
            new TypeInfo(Type.Support_ProductImprovement, "Product improvement", "Support", true)
        };

        public static readonly IReadOnlyDictionary<string, TypeInfo[]> ByCategory =
            Types.GroupBy(x => x.Category).ToDictionary(x => x.Key, x => x.ToArray());

        public static readonly IReadOnlyDictionary<int, TypeInfo> ById =
            Types.ToDictionary(x => (int)x.Value, x => x);
    }
}
