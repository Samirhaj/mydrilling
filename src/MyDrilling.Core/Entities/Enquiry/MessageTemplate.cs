﻿namespace MyDrilling.Core.Entities.Enquiry
{
    public class MessageTemplate
    {
        public long Id { get; set; }
        public EnquiryEventType EnquiryEventType { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
