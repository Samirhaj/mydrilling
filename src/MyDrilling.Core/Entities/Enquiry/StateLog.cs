﻿using System;

namespace MyDrilling.Core.Entities.Enquiry
{
    public class StateLog
    {
        public long Id { get; set; }

        public long EnquiryId { get; set; }
        public Enquiry Enquiry { get; set; }

        public StateInfo.State State { get; set; }

        public long? ChangedById { get; set; }
        public User ChangedBy { get; set; }
        public DateTime Changed { get; set; }
    }
}
