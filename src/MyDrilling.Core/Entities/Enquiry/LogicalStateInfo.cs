﻿using System.Collections.Generic;
using System.Linq;

namespace MyDrilling.Core.Entities.Enquiry
{
    public class LogicalStateInfo
    {
        public LogicalStateInfo(LogicalState value, string title)
        {
            Value = value;
            Title = title;
        }

        public LogicalState Value { get; }
        public string Title { get; }

        public enum LogicalState
        {
            Draft = 1,
            Open = 2,
            Deleted = 3,
            Closed = 4
        }

        public static LogicalStateInfo[] LogicalStates =
        {
            new LogicalStateInfo(LogicalState.Draft, "Draft"),
            new LogicalStateInfo(LogicalState.Open, "Open"),
            new LogicalStateInfo(LogicalState.Deleted, "Deleted"),
            new LogicalStateInfo(LogicalState.Closed, "Closed")
        };

        public static IReadOnlyDictionary<LogicalState, LogicalStateInfo> ByLogicalState = LogicalStates.ToDictionary(x => x.Value, x => x);
    }
}
