﻿namespace MyDrilling.Core.Entities.Enquiry
{
    public enum EnquiryEventType
    {
        EnquiryDraftDeleted = 1,
        EnquiryAssigned = 2,
        EnquiryDraftAssigned = 3,
        EnquiryRigMovedCreated = 4,
        EnquiryRigDowntimeCreated = 5,
        EnquiryNewCommentAdded = 6,
        EnquiryAssignedToRig = 7,
        EnquiryDraftAssignedToRig = 8,
        EnquiryAssignedToProvider = 9
    }
}
