﻿using System;

namespace MyDrilling.Core.Entities.Enquiry
{
    public class Attachment
    {
        public long Id { get; set; }
        public string ReferenceId { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public string BlobPath { get; set; }

        public long EnquiryId { get; set; }
        public Enquiry Enquiry { get; set; }

        public long? CommentId { get; set; }
        public Comment Comment { get; set; }

        public DateTime Created { get; set; }
        public long? CreatedById { get; set; }
        public User CreatedBy { get; set; }
    }
}
