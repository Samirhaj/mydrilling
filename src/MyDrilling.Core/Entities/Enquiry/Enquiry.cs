﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using MyDrilling.Core.Exceptions;

namespace MyDrilling.Core.Entities.Enquiry
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local")]
    public partial class Enquiry
    {
        public const int TitleMaxLength = 41;
        public const int DescriptionMaxLength = 8000;

        public Enquiry(string title, string description, TypeInfo.Type type, Rig.Rig rig,
            RootCustomer rootCustomer, Customer customer, Customer rigOwner, RootCustomer supplier,
            Equipment.Equipment equipment, User author)
        {
            SetTitle(title, author);
            SetDescription(description, author);
            SetType(type, author);
            SetRig(rig, author);
            SetRootCustomer(rootCustomer, author);
            SetCustomer(customer, author);
            SetRigOwner(rigOwner, author);
            SetSupplier(supplier, author);
            SetEquipment(equipment, author);
            
            var now = DateTime.UtcNow;
            CreatedById = author.Id;
            CreatedBy = author;
            Created = now;
            Submitted = now;
            LastActivity = now;
            Date = now;
        }

        //should be set by ef only
        // ReSharper disable once UnusedAutoPropertyAccessor.Local
        public long Id { get; private set; }

        public string Title { get; private set; }
        public void SetTitle(string title, User initiator)
        {
            if (string.IsNullOrWhiteSpace(title)) throw new BusinessException($"{nameof(Title)} is empty");
            if (title.Length > TitleMaxLength) throw new BusinessException($"{nameof(Title)} is too long: max length is {TitleMaxLength}");

            Title = title;
            UpdateModified(initiator);
        }
        
        public string Description { get; private set; }
        public void SetDescription(string description, User initiator)
        {
            if (description?.Length > DescriptionMaxLength) throw new BusinessException($"{nameof(Description)} is too long: max length is {DescriptionMaxLength}");

            Description = description;
            UpdateModified(initiator);
        }

        public TypeInfo.Type Type { get; private set; }
        public void SetType(TypeInfo.Type type, User initiator)
        {
            Type = type;
            UpdateModified(initiator);
        }

        public StateInfo.State State { get; private set; }
        public bool IsDeleted { get; private set; }
        public string ReferenceId { get; private set; }
        public DateTime? LastCommented { get; private set; }
        public DateTime LastActivity { get; private set; }
        public LogicalStateInfo.LogicalState LogicalState { get; private set; }
        /// <summary>
        /// It's for support urls generated for old myDrilling
        /// </summary>
        public string OldGlobalId { get; set; }

        public long Version { get; private set; }

        public DateTime Date { get; private set; }
        public DateTime Submitted { get; private set; }
        public void SetSubmitted(DateTime submitted, User initiator)
        {
            Submitted = submitted;
            Date = submitted;
            UpdateModified(initiator);
        }

        public long RigId { get; private set; }
        public Rig.Rig Rig { get; private set; }
        public void SetRig(Rig.Rig rig, User initiator)
        {
            Rig = rig ?? throw new BusinessException($"{nameof(Rig)} is null");
            RigId = rig.Id;
            UpdateModified(initiator);
        }

        public long? RigOwnerId { get; private set; }
        public Customer RigOwner { get; private set; }
        public void SetRigOwner(Customer rigOwner, User initiator)
        {
            RigOwner = rigOwner;
            RigOwnerId = rigOwner?.Id;
            UpdateModified(initiator);
        }

        public long CustomerId { get; private set; }
        public Customer Customer { get; private set; }
        public void SetCustomer(Customer customer, User initiator)
        {
            Customer = customer ?? throw new BusinessException($"{nameof(Customer)} is null");
            CustomerId = customer.Id;
            UpdateModified(initiator);
        }

        public long? RootCustomerId { get; private set; }
        public RootCustomer RootCustomer { get; private set; }
        public void SetRootCustomer(RootCustomer rootCustomer, User initiator)
        {
            RootCustomer = rootCustomer;
            RootCustomerId = rootCustomer?.Id;
            UpdateModified(initiator);
        }

        public long? EquipmentId { get; private set; }
        public Equipment.Equipment Equipment { get; private set; }
        public void SetEquipment(Equipment.Equipment equipment, User initiator)
        {
            Equipment = equipment;
            EquipmentId = equipment?.Id;
            UpdateModified(initiator);
        }

        public long? SupplierId { get; private set; }
        public RootCustomer Supplier { get; private set; }
        public void SetSupplier(RootCustomer supplier, User initiator)
        {
            Supplier = supplier;
            SupplierId = supplier?.Id;
            UpdateModified(initiator);
        }

        public long? AssignedToCustomerId { get; private set; }
        public RootCustomer AssignedToCustomer { get; private set; }

        public long? CreatedById { get; private set; }
        public User CreatedBy { get; private set; }
        public DateTime Created { get; private set; }

        public long? ModifiedById { get; private set; }
        public User ModifiedBy { get; private set; }
        public DateTime Modified { get; private set; }

        public long? ApprovedById { get; private set; }
        public User ApprovedBy { get; private set; }
        public DateTime? Approved { get; private set; }

        public long? AssignedById { get; private set; }
        public User AssignedBy { get; private set; }
        public long? AssignedToId { get; private set; }
        public User AssignedTo { get; private set; }
        public DateTime? Assigned { get; private set; }

        public long? RejectedById { get; private set; }
        public User RejectedBy { get; private set; }
        public DateTime? Rejected { get; private set; }

        private readonly List<AddedUser> _addedUsers = new List<AddedUser>();
        public IEnumerable<AddedUser> AddedUsers => _addedUsers.AsReadOnly();

        private readonly List<Attachment> _attachments = new List<Attachment>();
        public IEnumerable<Attachment> Attachments => _attachments.AsReadOnly();

        private readonly List<Comment> _comments = new List<Comment>();
        public IEnumerable<Comment> Comments => _comments.AsReadOnly();

        private readonly List<StateLog> _stateLogs = new List<StateLog>();
        public IEnumerable<StateLog> StateLogs => _stateLogs.AsReadOnly();

        public void RemoveAssignee(User initiator)
        {
            AssignedTo = null;
            AssignedToId = null;
            UpdateModified(initiator);
        }

        public void SetReferenceId(string referenceId)
        {
            if (string.IsNullOrEmpty(referenceId)) throw new BusinessException($"Can't set empty {nameof(ReferenceId)}'");

            ReferenceId = referenceId;
        }
        
        public void DeleteSoftly(User initiator)
        {
            IsDeleted = true;
            UpdateModified(initiator);
        }

        public Attachment AddAttachment(string blobPath, User initiator, Comment comment = null)
        {
            var attachment = new Attachment
            {
                FileName = Path.GetFileName(blobPath),
                FileExtension = Path.GetExtension(blobPath).Trim('.').ToLowerInvariant(),
                BlobPath = blobPath,
                Enquiry = this,
                Comment = comment,
                CreatedBy = initiator,
                Created = DateTime.UtcNow
            };
            _attachments.Add(attachment);
            return attachment;
        }

        public void AddPersonnel(User personnel, User initiator)
        {
            _addedUsers.Add(new AddedUser
            {
                Enquiry = this,
                User = personnel,
                Created = DateTime.UtcNow,
                CreatedBy = initiator
            });
        }

        public Comment AddInitialComment()
        {
            var comment = new Comment(Description, CreatedBy)
            {
                Enquiry = this,
                IsInitial = true
            };
            _comments.Add(comment);
            return comment;
        }

        public Comment AddComment(string text, User initiator, DateTime? submitted = null)
        {
            var comment = new Comment(text, initiator)
            {
                Enquiry = this, 
                IsInitial = false
            };

            if (submitted.HasValue)
            {
                comment.Submitted = submitted.Value;
            }

            _comments.Add(comment);
            LastCommented = comment.Created;
            LastActivity = comment.Created;
            return comment;
        }

        public void DeleteComment(Comment comment)
        {
            if (comment.EnquiryId != Id)
            {
                return;
            }

            foreach (var file in _attachments)
            {
                if (file.CommentId == comment.Id)
                {
                    file.CommentId = default;
                }
            }

            _comments.Remove(comment);

            LastCommented = _comments.Any() ? _comments.Max(x => x.Created) : default(DateTime?);
        }

        public void AsDraft(User initiator)
        {
            _stateLogs.Add(new StateLog
            {
                Enquiry = this,
                State = StateInfo.State.Draft,
                Changed = DateTime.UtcNow,
                ChangedBy = initiator
            });
            UpdateModified(initiator);

            AssignedToCustomerId = SupplierId;
            AssignedToCustomer = Supplier;
            State = StateInfo.State.Draft;
            LogicalState = LogicalStateInfo.LogicalState.Draft;
        }

        /// <summary>
        /// Reject draft enquiry, i.e. author should fix description or so
        /// </summary>
        public void Reject(User initiator)
        {
            _stateLogs.Add(new StateLog
            {
                Enquiry = this,
                State = StateInfo.State.Rejected,
                Changed = DateTime.UtcNow,
                ChangedBy = initiator
            });
            UpdateModified(initiator);

            State = StateInfo.State.Rejected;
            AssignedToCustomerId = SupplierId;
            AssignedToCustomer = Supplier;
            LogicalState = LogicalStateInfo.LogicalState.Draft;

            RejectedById = initiator.Id;
            RejectedBy = initiator;
            Rejected = DateTime.UtcNow;
        }

        /// <summary>
        /// Make draft enquiry available for approve
        /// </summary>
        public void SendForApproval(User initiator)
        {
            _stateLogs.Add(new StateLog
            {
                Enquiry = this,
                State = StateInfo.State.ReadyForApproval,
                Changed = DateTime.UtcNow,
                ChangedBy = initiator
            });
            UpdateModified(initiator);

            State = StateInfo.State.ReadyForApproval;
            AssignedToCustomerId = SupplierId;
            AssignedToCustomer = Supplier;
            LogicalState = LogicalStateInfo.LogicalState.Draft;
        }

        /// <summary>
        /// Set assignee
        /// </summary>
        public void SetAssignee(User initiator, User assignee)
        {
            AssignedTo = assignee;
            AssignedToId = assignee.Id;
            UpdateModified(initiator);
        }

        public void AssignToCustomer(User initiator)
        {
            _stateLogs.Add(new StateLog
            {
                Enquiry = this,
                State = StateInfo.State.AssignedCustomer,
                Changed = DateTime.UtcNow,
                ChangedBy = initiator
            });
            UpdateModified(initiator);

            State = StateInfo.State.AssignedCustomer;
            AssignedToCustomerId = RootCustomerId;
            AssignedToCustomer = RootCustomer;
            AssignedTo = null;
            AssignedToId = default;
            LogicalState = LogicalStateInfo.LogicalState.Open;
        }

        public void AssignToProvider(User initiator)
        {
            _stateLogs.Add(new StateLog
            {
                Enquiry = this,
                State = StateInfo.State.AssignedProvider,
                Changed = DateTime.UtcNow,
                ChangedBy = initiator
            });
            UpdateModified(initiator);

            State = StateInfo.State.AssignedProvider;
            AssignedToCustomerId = SupplierId;
            AssignedToCustomer = Supplier;
            AssignedTo = null;
            AssignedToId = default;
            LogicalState = LogicalStateInfo.LogicalState.Open;
        }

        /// <summary>
        /// Reopen rejected draft enquiry
        /// </summary>
        public void Reopen(User initiator)
        {
            _stateLogs.Add(new StateLog
            {
                Enquiry = this,
                State = StateInfo.State.Draft,
                Changed = DateTime.UtcNow,
                ChangedBy = initiator
            });
            UpdateModified(initiator);

            State = StateInfo.State.Draft;
            AssignedToCustomerId = SupplierId;
            AssignedToCustomer = Supplier;
            LogicalState = LogicalStateInfo.LogicalState.Draft;

            ApprovedById = initiator.Id;
            ApprovedBy = initiator;
            Approved = DateTime.UtcNow;
        }

        /// <summary>
        /// Approve draft enquiry, i.e. send to SAP
        /// </summary>
        public void Approve(User initiator, DateTime? deleteDateTime = null)
        {
            AssignToProvider(initiator);

            var now = DateTime.UtcNow;
            ApprovedById = initiator.Id;
            ApprovedBy = initiator;
            Approved = deleteDateTime ?? now;
            Date = deleteDateTime ?? now;
        }

        /// <summary>
        /// This comment 'Suggested closed' is just propagated into SAP as a signal that enquiry is ready to close
        /// </summary>
        public Comment SuggestToClose(User initiator)
        {
            var now = DateTime.UtcNow;
            _stateLogs.Add(new StateLog
            {
                Enquiry = this,
                State = StateInfo.State.SuggestedClosed,
                Changed = now,
                ChangedBy = initiator
            });
            UpdateModified(initiator);

            var nowFormatted = now.ToString("d MMM yyyy") + " " + now.ToString("H:mm") + " UTC";
            var comment = AddComment($"Suggested closed {nowFormatted}", initiator);

            State = StateInfo.State.SuggestedClosed;
            AssignedToCustomerId = SupplierId;
            AssignedToCustomer = Supplier;
            AssignedTo = null;
            AssignedToId = default;
            LogicalState = LogicalStateInfo.LogicalState.Open;

            return comment;
        }

        /// <summary>
        /// This method is supposed to be used for draft enquiries only, i.e. for enquiries which haven't been sent into SAP
        /// </summary>
        public void Delete(User initiator, DateTime? deleteDateTime = null)
        {
            _stateLogs.Add(new StateLog
            {
                Enquiry = this,
                State = StateInfo.State.Deleted,
                Changed = deleteDateTime ?? DateTime.UtcNow,
                ChangedBy = initiator
            });
            UpdateModified(initiator);

            State = StateInfo.State.Deleted;
            AssignedToCustomerId = SupplierId;
            AssignedToCustomer = Supplier;
            LogicalState = LogicalStateInfo.LogicalState.Deleted;
        }

        /// <summary>
        /// Enquiries can be closed only from SAP. MyDrilling is notified by SAP about closing
        /// </summary>
        public void Close(User initiator, DateTime? deleteDateTime = null)
        {
            _stateLogs.Add(new StateLog
            {
                Enquiry = this,
                State = StateInfo.State.Closed,
                Changed = deleteDateTime ?? DateTime.UtcNow,
                ChangedBy = initiator
            });
            UpdateModified(initiator);

            State = StateInfo.State.Closed;
            AssignedToCustomerId = SupplierId;
            AssignedToCustomer = Supplier;
            LogicalState = LogicalStateInfo.LogicalState.Closed;
        }

        public void AssignToInternalUser(User initiator, User assignee)
        {
            AssignedToId = assignee.Id;
            AssignedTo = assignee;
            AssignedById = initiator.Id;
            AssignedBy = initiator;
            Assigned = DateTime.UtcNow;
            UpdateModified(initiator);
        }

        public void UpdateModified(User initiator)
        {
            if (Id == default && Version > 0)
            {
                return;
            }

            ModifiedById = initiator.Id;
            ModifiedBy = initiator;
            Modified = DateTime.UtcNow;
            Version++;
        }

        public HashSet<long> GetInvolvedUserIds()
        {
            var involvedUserIds = new List<long>();
            if (CreatedById.HasValue)
            {
                involvedUserIds.Add(CreatedById.Value);
            }
            if (ApprovedById.HasValue)
            {
                involvedUserIds.Add(ApprovedById.Value);
            }
            if (AssignedToId.HasValue)
            {
                involvedUserIds.Add(AssignedToId.Value);
            }
            if (AssignedById.HasValue)
            {
                involvedUserIds.Add(AssignedById.Value);
            }
            if (RejectedById.HasValue)
            {
                involvedUserIds.Add(RejectedById.Value);
            }

            involvedUserIds.AddRange(Comments.Where(x => x.CreatedById.HasValue).Select(x => x.CreatedById.Value));
            involvedUserIds.AddRange(Attachments.Where(x => x.CreatedById.HasValue).Select(x => x.CreatedById.Value));
            involvedUserIds.AddRange(StateLogs.Where(x => x.ChangedById.HasValue).Select(x => x.ChangedById.Value));

            return involvedUserIds.Where(x => x > 0).ToHashSet();
        }
    }
}
