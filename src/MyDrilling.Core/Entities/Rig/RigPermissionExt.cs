﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyDrilling.Core.Entities.Rig
{
    public static class RigPermissionExt
    {
        public static IQueryable<Rig> ApplyPermission(this IQueryable<Rig> query, CombinedUserRoles roles)
        {
            var accessToRigs = roles.GetViewAccessToRigs();
            return query.Where(x => accessToRigs.Contains(x.Id));
        }
    }
}
