﻿using System;

namespace MyDrilling.Core.Entities.Rig
{
    /// <summary>
    /// Received from SAP, filled by integration
    /// </summary>
    public class Rig
    {
        public const int SapReferenceLength = 18;

        public long Id { get; set; }

        /// <summary>
        /// SAP identifier
        /// </summary>
        public long ReferenceId { get; set; }
        
        public string Name { get; set; }
        public string Sector { get; set; }
        public string Region { get; set; }
        public Coordinates Coordinates { get; set; }
        public string TypeCode { get; set; }

        /// <summary>
        /// Received from SAP
        /// </summary>
        public string OwnerReferenceId { get; set; }
        /// <summary>
        /// Set by consistency checker
        /// </summary>
        public long? OwnerId { get; set; }
        /// <summary>
        /// We receive this owner from SAP, it's a SAP customer
        /// Most likely <see cref="Owner"/> has <see cref="RootOwner"/> configured as its <see cref="Customer.RootCustomer"/>
        /// </summary>
        public Customer Owner { get; set; }

        public long? RootOwnerId { get; set; }
        /// <summary>
        /// We set this owner in MyDrilling, it's a customer created in MyDrilling
        /// </summary>
        public RootCustomer RootOwner { get; set; }

        public long? RootOperatorId { get; set; }
        /// <summary>
        /// We set operator in MyDrilling, it's a customer created in MyDrilling
        /// </summary>
        public RootCustomer RootOperator { get; set; }

        public bool IsDeleted { get; set; }
        public bool IsHidden { get; set; }
        public DateTime LastSeen { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }

        public long? ResponsibleUserId { get; set; }
        public User ResponsibleUser { get; set; }
        
        public bool ImportDocumentation { get; set; }
        //todo need timestamp and user to define who set rootowner and/or rootoperator
        public string NameFormatted => Name.ToCamelCase(true);
    }
}
