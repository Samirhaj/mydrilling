﻿using System.Linq;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Core.Entities.Permission;
using MyDrilling.Core.Entities.Subscription;
using MyDrilling.Core.Exceptions;

namespace MyDrilling.Core.Entities
{
    public static class UserQueriesExt
    {
        public static IQueryable<User> FindByUpn(this IQueryable<User> query, string upn)
        {
            if (string.IsNullOrWhiteSpace(upn))
            {
                throw new BusinessException("upn is empty");
            }

            return query.Where(x => x.Upn == upn);
        }

        public static IQueryable<User> FindByObjectId(this IQueryable<User> query, string objectId)
        {
            if (string.IsNullOrWhiteSpace(objectId))
            {
                throw new BusinessException("objectId is empty");
            }

            return query.Where(x => x.ObjectId == objectId);
        }

        public static IQueryable<User> WithRigRole(this IQueryable<User> query, RoleInfo.Role role, long rigId)
        {
            if (RoleInfo.ByRole[role].RoleType != RoleInfo.Type.Rig)
            {
                throw new BusinessException($"{role} role must have {RoleInfo.Type.Rig} type");
            }

            if (rigId <= 0)
            {
                throw new BusinessException($"{nameof(rigId)} isn't set'");
            }

            return query.Where(x => 
                x.Profiles.Any(up => up.RigId == rigId && up.Profile.ProfileRoles.Any(pr => pr.Role == role))
                || 
                x.Roles.Any(ur => ur.Role == role && ur.RigId == rigId));
        }

        public static IQueryable<User> WithCustomerRole(this IQueryable<User> query, RoleInfo.Role role, long? rootCustomerId)
        {
            if (RoleInfo.ByRole[role].RoleType != RoleInfo.Type.RootCustomer)
            {
                throw new BusinessException($"{role} role must have {RoleInfo.Type.RootCustomer} type");
            }

            if (rootCustomerId <= 0)
            {
                throw new BusinessException($"{nameof(rootCustomerId)} isn't set'");
            }

            return query.Where(x => x.Roles.Any(ur => ur.Role == role && ur.RootCustomerId == rootCustomerId));
        }

        public static IQueryable<User> WithRole(this IQueryable<User> query, RoleInfo.Role role)
        {
            return query.Where(x => x.Profiles.Any(up => up.Profile.ProfileRoles.Any(pr => pr.Role == role)) || x.Roles.Any(ur => ur.Role == role));
        }

        public static IQueryable<User> WithSubscription(this IQueryable<User> query, SubscriptionType subscriptionType)
        {
            return query.Where(u => u.SubscriptionSettings.Any(ss => ss.SubscriptionType == subscriptionType));
        }

        public static IQueryable<User> WithoutSubscription(this IQueryable<User> query, SubscriptionType subscriptionType)
        {
            return query.Where(u => u.SubscriptionSettings.All(ss => ss.SubscriptionType != subscriptionType));
        }

        public static IQueryable<User> WithRigSubscription(this IQueryable<User> query, long rigId)
        {
            return query.Where(u => !u.SubscriptionRigFilters.Any() || u.SubscriptionRigFilters.Any(rf => rf.RigId == rigId));
        }

        public static IQueryable<User> WithEnquiryTypeSubscription(this IQueryable<User> query, TypeInfo.Type enquiryType)
        {
            return query.Where(u => !u.SubscriptionEnquiryTypeFilters.Any() || u.SubscriptionEnquiryTypeFilters.Any(rf => rf.Type == enquiryType));
        }
    }
}
