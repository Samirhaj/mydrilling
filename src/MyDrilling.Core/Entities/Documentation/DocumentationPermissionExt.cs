﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyDrilling.Core.Entities.Documentation
{
    public static class DocumentationPermissionExt
    {
        public static bool CanAccessDocumentation(this CombinedUserRoles roles, long rigId = 0)
        {
            var documentationViewerAccessToRigs = roles.GetDocumentationViewerAccessToRigs();
            return documentationViewerAccessToRigs.Any(d => rigId == 0 || d == rigId);
        }

        public static IQueryable<UserManual> ApplyPermissions(this IQueryable<UserManual> query, CombinedUserRoles roles)
        {
            var documentationViewerAccessToRigs = roles.GetDocumentationViewerAccessToRigs();
            return query.Where(u => documentationViewerAccessToRigs.Contains(u.RigId ?? default));
        }

        public static IQueryable<TechnicalReport> ApplyPermissions(this IQueryable<TechnicalReport> query, CombinedUserRoles roles)
        {
            var documentationViewerAccessToRigs = roles.GetDocumentationViewerAccessToRigs();
            return query.Where(u => documentationViewerAccessToRigs.Contains(u.RigId ?? default));
        }
    }
}
