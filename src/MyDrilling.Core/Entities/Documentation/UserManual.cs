﻿using System;

namespace MyDrilling.Core.Entities.Documentation
{
    public class UserManual
    {
        public long Id { get; set; }
        public string ReferenceId { get; set; }
        public string Name { get; set; }
        public long RigReferenceId { get; set; }
        public long? RigId { get; set; }
        public Rig.Rig Rig { get; set; }
        public string RelatedReferenceId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string ProductNameFormatted => ProductName.ToCamelCase();
        public string Discipline { get; set; }
        public string DisciplineCode { get; set; }
        public string FileName { get; set; }
        public DateTime DocumentDate { get; set; }
        public string Url { get; set; }
        public bool IsMaster { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
    }
}