﻿using System;

namespace MyDrilling.Core.Entities
{
    /// <summary>
    /// Received from SAP, filled by integration
    /// </summary>
    public class Product
    {
        public long Id { get; set; }

        public string Number { get; set; }
        public string Name { get; set; }
        public string Group { get; set; }

        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
    }
}
