﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace MyDrilling.Core.Entities.UpgradeMatrix
{
    public class CcnItem
    {
        public long Id { get; set; }
        public long RootCustomerId { get; set; }
        public RootCustomer RootCustomer { get; set; }
        public long EquipmentCategoryId { get; set; }
        public EquipmentCategory EquipmentCategory { get; set; }
        public string Description { get; set; }
        public string Comments { get; set; }
        public UpgradeMatrixPackageUpgrade PackageUpgrade { get; set; }
        public UpgradeMatrixRigType RigType { get; set; }
        public long? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public long? ModifiedById { get; set; }
        public User ModifiedBy { get; set; }
        public DateTime Modified { get; set; }
    }

    public enum UpgradeMatrixPackageUpgrade
    {
        [Description("")]
        Undefined = 1,
        [Description("Performance Upgrade")]
        PerformanceUpgrade,
        Individual,
        [Description("DW")]
        Dw,
        [Description("TM upgrade")]
        TmUpgrade,
        [Description("Mud Pumps")]
        MudPumps,
        Bulletin
    }
}