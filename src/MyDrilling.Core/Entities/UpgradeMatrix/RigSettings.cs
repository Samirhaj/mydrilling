﻿using System;
using System.ComponentModel;

namespace MyDrilling.Core.Entities.UpgradeMatrix
{
    public class RigSettings
    {
        public long Id { get; set; }
        public long RigId { get; set; }
        public Rig.Rig Rig { get; set; }
        public string ShortName { get; set; }
        public UpgradeMatrixRigType RigType { get; set; }
        public long? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public long? ModifiedById { get; set; }
        public User ModifiedBy { get; set; }
        public DateTime Modified { get; set; }
    }

    public enum UpgradeMatrixRigType
    {
        [Description("")]
        Undefined = 1,
        [Description("Cat-D")]
        CatD,
        [Description("Ram Rig")]
        RamRig,
        [Description("Drill Ships")]
        DrillShips,
        [Description("EE-Class")]
        EeClass,
        [Description("Jack-up")]
        JackUp,
        Fixed
    }
}
