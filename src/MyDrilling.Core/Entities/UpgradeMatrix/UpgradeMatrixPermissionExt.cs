﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using System.Linq;
using System.Text;

namespace MyDrilling.Core.Entities.UpgradeMatrix
{
    public static class UpgradeMatrixPermissionExt
    {
       
        public static bool CanApproveCcnState(this CcnState ccnState, CombinedUserRoles roles)
        {
            if(roles.IsUpgradeMatrixAdmin()) return true;
            var editorAccessToRigs = roles.GetUpgradeMatrixEditorAccessToRigs();
            return ccnState != null && editorAccessToRigs.Contains(ccnState.RigSettings.RigId);
        }

        public static IQueryable<RootCustomer> ApplyPermissionsForRootCustomer(this IQueryable<RootCustomer> query, CombinedUserRoles roles)
        {
            if (roles.IsUpgradeMatrixAdmin()) return query;
            var accessToCustomers = roles.GetAccessToRootCustomers();
            return query.Where(c => accessToCustomers.Contains(c.Id));
        }

        public static IQueryable<Rig.Rig> ApplyPermissionsForRig(this IQueryable<Rig.Rig> query, CombinedUserRoles roles)
        {
            if (roles.IsUpgradeMatrixAdmin()) return query;
            var viewerAccessToRigs = roles.GetUpgradeMatrixViewAccessToRigs();
            var editorAccessToRigs = roles.GetUpgradeMatrixEditorAccessToRigs();
            return query.Where(r => viewerAccessToRigs.Contains(r.Id) || editorAccessToRigs.Contains(r.Id));
        }

        public static IQueryable<CustomerSettings> ApplyPermissionsForCustomerSettings(this IQueryable<CustomerSettings> query, CombinedUserRoles roles)
        {
            if (roles.IsUpgradeMatrixAdmin()) return query;
            var accessToCustomers = roles.GetAccessToRootCustomers();
            return query.Where(cs => accessToCustomers.Contains(cs.RootCustomerId));
        }

        public static IQueryable<RigSettings> ApplyPermissionsForRigSettings(this IQueryable<RigSettings> query, CombinedUserRoles roles)
        {
            if (roles.IsUpgradeMatrixAdmin()) return query;
            var viewerAccessToRigs = roles.GetUpgradeMatrixViewAccessToRigs();
            var editorAccessToRigs = roles.GetUpgradeMatrixEditorAccessToRigs();
            return query.Where(rs => viewerAccessToRigs.Contains(rs.RigId) || editorAccessToRigs.Contains(rs.RigId));
        }

        public static IQueryable<CcnState> ApplyPermissionsForCcnState(this IQueryable<CcnState> query, CombinedUserRoles roles)
        {
            if (roles.IsUpgradeMatrixAdmin()) return query;
            var viewerAccessToRigs = roles.GetUpgradeMatrixViewAccessToRigs();
            var editorAccessToRigs = roles.GetUpgradeMatrixEditorAccessToRigs();
            return query.Where(cs => cs.RigSettings != null && (viewerAccessToRigs.Contains(cs.RigSettings.RigId) || editorAccessToRigs.Contains(cs.RigSettings.RigId)));
        }

        public static IQueryable<QuotationState> ApplyPermissionsForQuotationState(this IQueryable<QuotationState> query, CombinedUserRoles roles)
        {
            if (roles.IsUpgradeMatrixAdmin()) return query;
            var viewerAccessToRigs = roles.GetUpgradeMatrixViewAccessToRigs();
            var editorAccessToRigs = roles.GetUpgradeMatrixEditorAccessToRigs();
            return query.Where(qs => qs.RigSettings != null && (viewerAccessToRigs.Contains(qs.RigSettings.RigId) || editorAccessToRigs.Contains(qs.RigSettings.RigId)));
        }



    }
}
