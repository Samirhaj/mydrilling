﻿using System;

namespace MyDrilling.Core.Entities.UpgradeMatrix
{
    public class CcnState
    {
        public long Id { get; set; }
        public long CcnItemId { get; set; }
        public CcnItem CcnItem { get; set; }
        public long RigSettingsId { get; set; }
        public RigSettings RigSettings { get; set; }
        public UpgradeMatrixItemStatus Status { get; set; }
        public string CcnName { get; set; }
        public string DocumentPath { get; set; }
        public long? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public long? ModifiedById { get; set; }
        public User ModifiedBy { get; set; }
        public DateTime Modified { get; set; }
        public long? ApprovedById { get; set; }
        public User ApprovedBy { get; set; }
        public DateTime? Approved { get; set; }
    }

    public enum UpgradeMatrixItemStatus
    {
        Undefined = 1, 
        Na,
        Ready,
        Approved,
        Completed
    }
}
