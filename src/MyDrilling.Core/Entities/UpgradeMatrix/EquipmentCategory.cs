﻿using System;

namespace MyDrilling.Core.Entities.UpgradeMatrix
{
    public class EquipmentCategory
    {
        public long Id { get; set; }
        public string ShortName { get; set; }
        public long? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public long? ModifiedById { get; set; }
        public User ModifiedBy { get; set; }
        public DateTime Modified { get; set; }
    }
}
