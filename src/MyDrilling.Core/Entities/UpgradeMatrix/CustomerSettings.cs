﻿using System;

namespace MyDrilling.Core.Entities.UpgradeMatrix
{
    public class CustomerSettings
    {
        public long Id { get; set; }
        public long RootCustomerId { get; set; }
        public RootCustomer RootCustomer { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
        public long? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public long? ModifiedById { get; set; }
        public User ModifiedBy { get; set; }
        public DateTime Modified { get; set; }
    }

    public enum UpgradeMatrixCustomerStatus
    {
        Archived = 0,
        Active
    }
}
