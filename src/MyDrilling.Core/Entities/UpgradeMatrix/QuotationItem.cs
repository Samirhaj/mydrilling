﻿using System;

namespace MyDrilling.Core.Entities.UpgradeMatrix
{
    public class QuotationItem
    {
        public long Id { get; set; }
        public long RootCustomerId { get; set; }
        public RootCustomer RootCustomer { get; set; }
        public long EquipmentCategoryId { get; set; }
        public EquipmentCategory EquipmentCategory { get; set; }
        public string Description { get; set; }
        public string Heading { get; set; }
        public string Benefits { get; set; }
        public string QuotationReference { get; set; }
        public long? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public long? ModifiedById { get; set; }
        public User ModifiedBy { get; set; }
        public DateTime Modified { get; set; }
    }
}
