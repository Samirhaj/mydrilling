﻿namespace MyDrilling.Core.Entities.UpgradeMatrix
{
    public class MessageTemplate
    {
        public long Id { get; set; }
        public UpgradeMatrixEventType UpgradeMatrixEventType { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
