﻿using System;

namespace MyDrilling.Core.Entities.UpgradeMatrix
{
    public class QuotationState
    {
        public long Id { get; set; }
        public long QuotationItemId { get; set; }
        public QuotationItem QuotationItem { get; set; }
        public long RigSettingsId { get; set; }
        public RigSettings RigSettings { get; set; }
        public UpgradeMatrixItemStatus Status { get; set; }
        public long? CreatedById { get; set; }
        public User CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public long? ModifiedById { get; set; }
        public User ModifiedBy { get; set; }
        public DateTime Modified { get; set; }
    }
}
