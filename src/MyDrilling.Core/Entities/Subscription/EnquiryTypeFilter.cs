﻿using System;

namespace MyDrilling.Core.Entities.Subscription
{
    public class RigFilter
    {
        public long Id { get; set; }

        public long UserId { get; set; }
        public User User { get; set; }

        public long RigId { get; set; }
        public Rig.Rig Rig { get; set; }

        public DateTime Created { get; set; }
    }
}
