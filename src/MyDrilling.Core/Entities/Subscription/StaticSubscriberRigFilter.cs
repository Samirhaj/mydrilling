﻿using System;

namespace MyDrilling.Core.Entities.Subscription
{
    public class StaticSubscriberRigFilter
    {
        public long Id { get; set; }

        public long StaticSubscriberId { get; set; }
        public StaticSubscriber StaticSubscriber { get; set; }

        public long RigId { get; set; }
        public Rig.Rig Rig { get; set; }

        public DateTime Created { get; set; }
    }
}
