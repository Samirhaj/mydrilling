﻿using System;
using MyDrilling.Core.Entities.Enquiry;

namespace MyDrilling.Core.Entities.Subscription
{
    public class StaticSubscriberEnquiryTypeFilter
    {
        public long Id { get; set; }

        public long StaticSubscriberId { get; set; }
        public StaticSubscriber StaticSubscriber { get; set; }

        public TypeInfo.Type Type { get; set; }

        public DateTime Created { get; set; }
    }
}
