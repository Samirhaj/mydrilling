﻿using System;
using MyDrilling.Core.Entities.Enquiry;

namespace MyDrilling.Core.Entities.Subscription
{
    public class EnquiryTypeFilter
    {
        public long Id { get; set; }

        public long UserId { get; set; }
        public User User { get; set; }

        public TypeInfo.Type Type { get; set; }

        public DateTime Created { get; set; }
    }
}
