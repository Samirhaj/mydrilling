﻿using System.Collections.Generic;

namespace MyDrilling.Core.Entities.Subscription
{
    public class StaticSubscriber
    {
        public long Id { get; set; }
        public string Email{ get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        private readonly List<StaticSubscriberSetting> _subscriptionSettings = new List<StaticSubscriberSetting>();
        public IEnumerable<StaticSubscriberSetting> SubscriptionSettings => _subscriptionSettings.AsReadOnly();

        private readonly List<StaticSubscriberRigFilter> _subscriptionRigFilters = new List<StaticSubscriberRigFilter>();
        public IEnumerable<StaticSubscriberRigFilter> SubscriptionRigFilters => _subscriptionRigFilters.AsReadOnly();

        private readonly List<StaticSubscriberEnquiryTypeFilter> _subscriptionEnquiryTypeFilters = new List<StaticSubscriberEnquiryTypeFilter>();
        public IEnumerable<StaticSubscriberEnquiryTypeFilter> SubscriptionEnquiryTypeFilters => _subscriptionEnquiryTypeFilters.AsReadOnly();
    }
}
