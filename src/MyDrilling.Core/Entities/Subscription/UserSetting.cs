﻿using System;

namespace MyDrilling.Core.Entities.Subscription
{
    public class UserSetting
    {
        public long Id { get; set; }

        public long UserId { get; set; }
        public User User { get; set; }

        public SubscriptionType SubscriptionType { get; set; }

        public DateTime Created { get; set; }
    }
}
