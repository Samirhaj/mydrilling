﻿using System;

namespace MyDrilling.Core.Entities.Subscription
{
    public class StaticSubscriberSetting
    {
        public long Id { get; set; }

        public long StaticSubscriberId { get; set; }
        public StaticSubscriber StaticSubscriber { get; set; }

        public SubscriptionType SubscriptionType { get; set; }

        public DateTime Created { get; set; }
    }
}
