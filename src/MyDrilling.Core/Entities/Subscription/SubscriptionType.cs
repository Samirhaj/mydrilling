﻿namespace MyDrilling.Core.Entities.Subscription
{
    public enum SubscriptionType
    {
        NewUpgradeMatrixItemAvailable = 1,
        DraftEnquiryCreatedByMeDeleted = 2,
        EnquiryAssignedToMe = 3,
        EnquiryRigMovedCreated = 4,
        EnquiryRigOnDowntimeCreated = 5,
        EnquiryCommentAdded = 6,
        EnquiryIAmInvolvedInCommentAdded = 7,
        EnquiryRigOnDowntimeCommentAdded = 8,
        EnquiryAssignedToMyOrganization = 9
    }
}
