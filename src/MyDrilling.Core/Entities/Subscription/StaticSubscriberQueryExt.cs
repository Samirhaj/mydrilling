﻿using System.Linq;
using MyDrilling.Core.Entities.Enquiry;

namespace MyDrilling.Core.Entities.Subscription
{
    public static class StaticSubscriberQueryExt
    {
        public static IQueryable<StaticSubscriber> WithEnquiryTypeSubscription(this IQueryable<StaticSubscriber> query, TypeInfo.Type enquiryType)
        {
            return query.Where(u => !u.SubscriptionEnquiryTypeFilters.Any() || u.SubscriptionEnquiryTypeFilters.Any(rf => rf.Type == enquiryType));
        }

        public static IQueryable<StaticSubscriber> WithSubscription(this IQueryable<StaticSubscriber> query, SubscriptionType subscriptionType)
        {
            return query.Where(u => u.SubscriptionSettings.Any(ss => ss.SubscriptionType == subscriptionType));
        }
    }
}
