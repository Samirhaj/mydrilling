﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace MyDrilling.Core.Entities.Permission
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local")]
    public class UserProfile
    {
        // ReSharper disable once UnusedMember.Local
        // for ef
        private UserProfile() { }

        public UserProfile(Profile profile, User user, Rig.Rig rig)
        {
            Profile = profile ?? throw new NullReferenceException(nameof(profile));
            User = user ?? throw new NullReferenceException(nameof(user));
            Rig = rig ?? throw new NullReferenceException(nameof(rig));
        }

        public long Id { get; private set; }

        public long ProfileId { get; private set; }
        public Profile Profile { get; private set; }

        public long UserId { get; private set; }
        public User User { get; private set; }

        public long RigId { get; private set; }
        public Rig.Rig Rig { get; private set; }
    }
}
