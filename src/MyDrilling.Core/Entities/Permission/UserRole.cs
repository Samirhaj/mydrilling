﻿using System;
using System.Diagnostics.CodeAnalysis;
using MyDrilling.Core.Exceptions;

namespace MyDrilling.Core.Entities.Permission
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local")]
    public class UserRole
    {
        // ReSharper disable once UnusedMember.Local
        // for ef
        private UserRole() { }

        public static UserRole AssignGeneralRole(User user, RoleInfo roleInfo)
        {
            _ = roleInfo ?? throw new NullReferenceException(nameof(roleInfo));
            if (roleInfo.RoleType != RoleInfo.Type.General) throw new BusinessException($"{nameof(AssignGeneralRole)} can be used only with {nameof(roleInfo.RoleType)}={RoleInfo.Type.General}");

            var userRole = new UserRole
            {
                User = user ?? throw new NullReferenceException(nameof(user)),
                Role = roleInfo.Value
            };
            return userRole;
        }

        public static UserRole AssignRigRole(User user, RoleInfo roleInfo, Rig.Rig rig)
        {
            _ = roleInfo ?? throw new NullReferenceException(nameof(roleInfo));
            if (roleInfo.RoleType != RoleInfo.Type.Rig) throw new BusinessException($"{nameof(AssignRigRole)} can be used only with {nameof(roleInfo.RoleType)}={RoleInfo.Type.Rig}");

            var userRole = new UserRole
            {
                User = user ?? throw new NullReferenceException(nameof(user)),
                Rig = rig ?? throw new NullReferenceException(nameof(rig)),
                Role = roleInfo.Value
            };
            return userRole;
        }

        public static UserRole AssignRootCustomerRole(User user, RoleInfo roleInfo, RootCustomer rootCustomer)
        {
            _ = roleInfo ?? throw new NullReferenceException(nameof(roleInfo));
            if (roleInfo.RoleType != RoleInfo.Type.RootCustomer) throw new BusinessException($"{nameof(AssignRootCustomerRole)} can be used only with {nameof(roleInfo.RoleType)}={RoleInfo.Type.RootCustomer}");

            var userRole = new UserRole
            {
                User = user ?? throw new NullReferenceException(nameof(user)),
                RootCustomer = rootCustomer ?? throw new NullReferenceException(nameof(rootCustomer)),
                Role = roleInfo.Value
            };
            return userRole;
        }

        public long Id { get; private set; }

        public RoleInfo.Role Role { get; private set; }

        public long UserId { get; private set; }
        public User User { get; private set; }

        public long? RigId { get; private set; }
        public Rig.Rig Rig { get; private set; }

        public long? RootCustomerId { get; private set; }
        public RootCustomer RootCustomer { get; private set; }
    }
}
