﻿using System.Collections.Generic;
using System.Linq;

namespace MyDrilling.Core.Entities.Permission
{
    public class RoleInfo
    {
        public RoleInfo(Role role, string key, string name, Type roleType, string description = "", string area = "")
        {
            Id = (int) role;
            Value = role;
            Name = name;
            Key = key;
            RoleType = roleType;
            Description = description;
            Area = area;
        }

        public int Id { get; }
        public Role Value { get; }
        public string Name { get; }
        public string Key { get; }
        public string Description { get; }
        public string Area { get; }
        public Type RoleType { get; }

        public static RoleInfo[] Roles = 
        {
            new RoleInfo(Role.MyDrillingViewer, RoleKeys.MyDrillingViewer, Role.MyDrillingViewer.ToString(), Type.Rig, RoleDescriptions.MyDrillingViewer, RoleAreas.MyDrillingViewer),
            new RoleInfo(Role.EnquiryHandlingInitiator, RoleKeys.EnquiryHandlingInitiator, Role.EnquiryHandlingInitiator.ToString(), Type.Rig, RoleDescriptions.EnquiryHandlingInitiator, RoleAreas.EnquiryHandlingInitiator),
            new RoleInfo(Role.EnquiryHandlingApprover, RoleKeys.EnquiryHandlingApprover, Role.EnquiryHandlingApprover.ToString(), Type.Rig, RoleDescriptions.EnquiryHandlingApprover, RoleAreas.EnquiryHandlingApprover),
            new RoleInfo(Role.CustomerAccess, RoleKeys.CustomerAccess, Role.CustomerAccess.ToString(), Type.RootCustomer),
            new RoleInfo(Role.EnquiryHandlingApprovedViewer, RoleKeys.EnquiryHandlingApprovedViewer, Role.EnquiryHandlingApprovedViewer.ToString(), Type.Rig, RoleDescriptions.EnquiryHandlingApprovedViewer, RoleAreas.EnquiryHandlingApprovedViewer),
            new RoleInfo(Role.EnquiryHandlingPreparationViewer, RoleKeys.EnquiryHandlingPreparationViewer, Role.EnquiryHandlingPreparationViewer.ToString(), Type.Rig, RoleDescriptions.EnquiryHandlingPreparationViewer, RoleAreas.EnquiryHandlingPreparationViewer),
            new RoleInfo(Role.EnquiryHandlingProvider, RoleKeys.EnquiryHandlingProvider, Role.EnquiryHandlingProvider.ToString(), Type.Rig, RoleDescriptions.EnquiryHandlingProvider, RoleAreas.EnquiryHandlingProvider),
            new RoleInfo(Role.EnquiryHandlingCustomerAssigner, RoleKeys.EnquiryHandlingCustomerAssigner, Role.EnquiryHandlingCustomerAssigner.ToString(), Type.Rig, RoleDescriptions.EnquiryHandlingCustomerAssigner, RoleAreas.EnquiryHandlingCustomerAssigner),
            new RoleInfo(Role.EnquiryHandlingProviderAssigner, RoleKeys.EnquiryHandlingProviderAssigner, Role.EnquiryHandlingProviderAssigner.ToString(), Type.Rig, RoleDescriptions.EnquiryHandlingProviderAssigner, RoleAreas.EnquiryHandlingProviderAssigner),
            new RoleInfo(Role.EnquiryHandlingCollaborator, RoleKeys.EnquiryHandlingCollaborator, Role.EnquiryHandlingCollaborator.ToString(), Type.Rig, RoleDescriptions.EnquiryHandlingCollaborator, RoleAreas.EnquiryHandlingCollaborator),
            new RoleInfo(Role.AdminSysadmin, RoleKeys.AdminSysadmin, Role.AdminSysadmin.ToString(), Type.General),
            new RoleInfo(Role.CbmViewer, RoleKeys.CbmViewer, Role.CbmViewer.ToString(), Type.Rig, RoleDescriptions.CbmViewer, RoleAreas.CbmViewer),
            new RoleInfo(Role.EnquiryHandlingNotifier, RoleKeys.EnquiryHandlingNotifier, Role.EnquiryHandlingNotifier.ToString(), Type.Rig, RoleDescriptions.EnquiryHandlingNotifier, RoleAreas.EnquiryHandlingNotifier),
            new RoleInfo(Role.DocumentationViewer, RoleKeys.DocumentationViewer, Role.DocumentationViewer.ToString(), Type.Rig, RoleDescriptions.DocumentationViewer, RoleAreas.DocumentationViewer),
            new RoleInfo(Role.UpgradeMatrixAdmin, RoleKeys.UpgradeMatrixAdmin, Role.UpgradeMatrixAdmin.ToString(), Type.General, RoleDescriptions.UpgradeMatrixAdmin, RoleAreas.UpgradeMatrixAdmin),
            new RoleInfo(Role.UpgradeMatrixEditor, RoleKeys.UpgradeMatrixEditor, Role.UpgradeMatrixEditor.ToString(), Type.Rig, RoleDescriptions.UpgradeMatrixEditor, RoleAreas.UpgradeMatrixEditor),
            new RoleInfo(Role.UpgradeMatrixViewer, RoleKeys.UpgradeMatrixViewer, Role.UpgradeMatrixViewer.ToString(), Type.Rig, RoleDescriptions.UpgradeMatrixViewer, RoleAreas.UpgradeMatrixViewer),
            new RoleInfo(Role.BulletinViewer, RoleKeys.BulletinViewer, Role.BulletinViewer.ToString(), Type.Rig, RoleDescriptions.BulletinViewer, RoleAreas.BulletinViewer),
            new RoleInfo(Role.SparePartViewer, RoleKeys.SparePartViewer, Role.SparePartViewer.ToString(), Type.General, RoleDescriptions.SparePartViewer, RoleAreas.SparePartViewer),
            new RoleInfo(Role.DrillConViewer, RoleKeys.DrillConViewer, Role.DrillConViewer.ToString(), Type.General, RoleDescriptions.DrillConViewer, RoleAreas.DrillConViewer),
            new RoleInfo(Role.SmartModuleViewer, RoleKeys.SmartModuleViewer, Role.SmartModuleViewer.ToString(), Type.General, RoleDescriptions.SmartModuleViewer, RoleAreas.SmartModuleViewer),
            new RoleInfo(Role.PerformanceMonitoringViewer, RoleKeys.PerformanceMonitoringViewer, Role.PerformanceMonitoringViewer.ToString(), Type.Rig),
            new RoleInfo(Role.AdminUserAdmin, RoleKeys.AdminUserAdmin, Role.AdminUserAdmin.ToString(), Type.RootCustomer),
            new RoleInfo(Role.RealtimeViewer, RoleKeys.RealtimeViewer, Role.RealtimeViewer.ToString(), Type.Rig),
            new RoleInfo(Role.MonitoringViewer, RoleKeys.MonitoringViewer, Role.MonitoringViewer.ToString(), Type.Rig),
            new RoleInfo(Role.KpiTrendViewer, RoleKeys.KpiTrendViewer, Role.KpiTrendViewer.ToString(), Type.Rig),
            new RoleInfo(Role.CustomerAdmin, RoleKeys.CustomerAdmin, Role.CustomerAdmin.ToString(), Type.RootCustomer)
        };

        public static readonly IReadOnlyDictionary<string, RoleInfo> ByKey = Roles.ToDictionary(x => x.Key, x => x);

        public static readonly IReadOnlyDictionary<Role, RoleInfo> ByRole = Roles.ToDictionary(x => x.Value, x => x);

        public enum Type
        {
            /// <summary>
            /// Role isn't related to particular rig or root customer
            /// </summary>
            General = 1,
            /// <summary>
            /// Role is related to particular customer
            /// </summary>
            RootCustomer = 2,
            /// <summary>
            /// Role is related to particular rig
            /// </summary>
            Rig = 3
        }

        public enum Role
        {
            //not in use in DB - 1,2,6,8,13,14,16,17,18,26,27,28,30,31
            BulletinConfirmerImplemented = 1,
            BulletinConfirmerReceiver = 2,
            BulletinViewer = 3,
            DocumentationViewer = 4,
            EnquiryHandlingApprover = 5,
            OrderHandlingViewer = 6,
            SparePartViewer = 7,
            RealtimeViewer = 8,
            MyDrillingViewer = 9,
            EnquiryHandlingInitiator = 10,
            AdminUserAdmin = 11,
            EnquiryHandlingCollaborator = 12,
            MonitoringViewer = 13,
            Department = 14,
            AdminSysadmin = 15,
            KpiTrendViewer = 16,
            ProfileSubscriptionConfiguration = 17,
            EnquirySubscriptionConfiguration = 18,
            CustomerAdmin = 19,
            CustomerAccess = 20,
            EnquiryHandlingPreparationViewer = 21,
            EnquiryHandlingApprovedViewer = 22,
            EnquiryHandlingProvider = 23,
            EnquiryHandlingCustomerAssigner = 24,
            EnquiryHandlingProviderAssigner = 25,
            PerformanceMonitoringViewer = 26,
            PerformanceMonitoringEditor = 27,
            PerformanceMonitoringAdmin = 28,
            CbmViewer = 29,
            CbmEditor = 30,
            CbmAdmin = 31,
            UpgradeMatrixViewer = 32,
            UpgradeMatrixEditor = 33,
            UpgradeMatrixAdmin = 34,
            EnquiryHandlingNotifier = 35,
            DrillConViewer = 36,
            SmartModuleViewer = 37
        }

        public static class RoleKeys
        {
            public const string BulletinConfirmerImplemented = "bulletin-confirmer-implemented";
            public const string BulletinConfirmerReceiver = "bulletin-confirmer-receiver";
            public const string BulletinViewer = "bulletin-viewer";
            public const string DocumentationViewer = "documentation-viewer";
            public const string EnquiryHandlingApprover = "enquiry-handling-approver";
            public const string OrderHandlingViewer = "order-handling-viewer";
            public const string SparePartViewer = "sparepart-viewer";
            public const string RealtimeViewer = "realtime-viewer";
            public const string MyDrillingViewer = "mydrilling-viewer";
            public const string EnquiryHandlingInitiator = "enquiry-handling-initiator";
            public const string AdminUserAdmin = "admin-useradmin";
            public const string EnquiryHandlingCollaborator = "enquiry-handling-collaborator";
            public const string MonitoringViewer = "monitoring-viewer";
            public const string Department = "department";
            public const string AdminSysadmin = "admin-sysadmin";
            public const string KpiTrendViewer = "kpitrend-viewer";
            public const string ProfileSubscriptionConfiguration = "profile-subscription-configuration";
            public const string EnquirySubscriptionConfiguration = "enquiry-subscription-configuration";
            public const string CustomerAdmin = "customer-admin";
            public const string CustomerAccess = "customer-access";
            public const string EnquiryHandlingPreparationViewer = "enquiry-handling-preparation-viewer";
            public const string EnquiryHandlingApprovedViewer = "enquiry-handling-approved-viewer";
            public const string EnquiryHandlingProvider = "enquiry-handling-provider";
            public const string EnquiryHandlingCustomerAssigner = "enquiry-handling-customer-assigner";
            public const string EnquiryHandlingProviderAssigner = "enquiry-handling-provider-assigner";
            public const string PerformanceMonitoringViewer = "performancemonitoring-viewer";
            public const string PerformanceMonitoringEditor = "performancemonitoring-editor";
            public const string PerformanceMonitoringAdmin = "performancemonitoring-admin";
            public const string CbmViewer = "cbm-viewer";
            public const string CbmEditor = "cbm-editor";
            public const string CbmAdmin = "cbm-admin";
            public const string UpgradeMatrixViewer = "upgrade-matrix-viewer";
            public const string UpgradeMatrixEditor = "upgrade-matrix-editor";
            public const string UpgradeMatrixAdmin = "upgrade-matrix-admin";
            public const string EnquiryHandlingNotifier = "enquiry-handling-notifier";
            public const string DrillConViewer = "drillcon-viewer";
            public const string SmartModuleViewer = "smartmodule-viewer";
        }

        public static class RoleDescriptions
        {
            public const string MyDrillingViewer = "Viewer - View equipment installed on rig";
            public const string BulletinViewer = "Viewer - View relevant bulletins and alerts";
            public const string DocumentationViewer = "Viewer - View relevant user manuals and technical reports";
            public const string SparePartViewer = "Viewer - Search for and view price and availability of a spare part";
            public const string EnquiryHandlingApprover = "Enquiry approver - Make a new or approve an enquiry to MHWirth and participate in collaboration";
            public const string EnquiryHandlingInitiator = "Draft initiator - Make a new draft enquiry";
            public const string EnquiryHandlingCollaborator = "Collaborator - Collaborate on enquiries";
            public const string EnquiryHandlingPreparationViewer = "Draft enquiry viewer - View details for draft enquiries";
            public const string EnquiryHandlingApprovedViewer = "Approved enquiry viewer - View details for approved enquiries";
            public const string EnquiryHandlingProvider = "MHWirth participant - Participate on approved enquiries";
            public const string EnquiryHandlingCustomerAssigner = "Customer assigner - Assign enquiries to customer users";
            public const string EnquiryHandlingProviderAssigner = "MHWirth assigner - Assign enquiries to MHWirth users";
            public const string EnquiryHandlingNotifier = "Add user - Add or remove users within own organization to enquires for \"Added Personnel\"";
            public const string CbmViewer = "Viewer - View RiCon page (please assure that Documentation viewer is also checked in order to see documents under RiCon)";
            public const string UpgradeMatrixViewer = "Viewer - View Upgrade Matrix";
            public const string UpgradeMatrixEditor = "Editor - Can Approve CCN items with Ready state";
            public const string UpgradeMatrixAdmin = "Admin - Can administrate Upgrade Matrix";
            public const string DrillConViewer = "Viewer - View DrillCon page";
            public const string SmartModuleViewer = "Viewer - View DRILLPerform™ page";
            //public const string AdminUserAdmin = "admin-useradmin";
            //public const string AdminSysadmin = "admin-sysadmin";
            //public const string CustomerAdmin = "customer-admin";
            //public const string CustomerAccess = "customer-access";


        }

        public static class RoleAreas
        {
            public const string MyDrillingViewer = "Rig Information";
            public const string BulletinViewer = "Bulletins";
            public const string DocumentationViewer = "Documentation";
            public const string SparePartViewer = "Sparepart";
            public const string EnquiryHandlingApprover = "Enquiry";
            public const string EnquiryHandlingInitiator = "Enquiry";
            public const string EnquiryHandlingCollaborator = "Enquiry";
            public const string EnquiryHandlingPreparationViewer = "Enquiry";
            public const string EnquiryHandlingApprovedViewer = "Enquiry";
            public const string EnquiryHandlingProvider = "Enquiry";
            public const string EnquiryHandlingCustomerAssigner = "Enquiry";
            public const string EnquiryHandlingProviderAssigner = "Enquiry";
            public const string EnquiryHandlingNotifier = "Enquiry";
            public const string CbmViewer = "RiCon";
            public const string UpgradeMatrixViewer = "Upgrade Matrix";
            public const string UpgradeMatrixEditor = "Upgrade Matrix";
            public const string UpgradeMatrixAdmin = "Upgrade Matrix";
            public const string DrillConViewer = "DrillCon";
            public const string SmartModuleViewer = "DRILLPerform";
            //public const string AdminUserAdmin = "admin-useradmin";
            //public const string AdminSysadmin = "admin-sysadmin";
            //public const string CustomerAdmin = "customer-admin";
            //public const string CustomerAccess = "customer-access";


        }
    }
}
