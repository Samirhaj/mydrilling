﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using MyDrilling.Core.Exceptions;

namespace MyDrilling.Core.Entities.Permission
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local")]
    public class Profile
    {
        public const int MaxNameLength = 300;

        // ReSharper disable once UnusedMember.Local
        // for ef
        private Profile() { }

        public Profile(string name)
        {
            if (string.IsNullOrWhiteSpace(name)) throw new BusinessException($"{nameof(Name)} is empty");
            if (name.Length > MaxNameLength) throw new BusinessException($"{nameof(Name)} is too long: max length is {MaxNameLength}");

            Name = name;
        }

        public long Id { get; private set; }
        public string Name { get; set; }

        private readonly List<ProfileRole> _profileRoles = new List<ProfileRole>();
        public IEnumerable<ProfileRole> ProfileRoles => _profileRoles.AsReadOnly();
    }
}
