﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace MyDrilling.Core.Entities.Permission
{
    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local")]
    public class ProfileRole
    {
        // ReSharper disable once UnusedMember.Local
        // for ef
        private ProfileRole() { }

        public ProfileRole(Profile profile, RoleInfo.Role role)
        {
            Profile = profile ?? throw new NullReferenceException(nameof(profile));
            Role = role;
        }

        public long Id { get; private set; }
        public RoleInfo.Role Role { get; private set; }

        public long ProfileId { get; private set; }
        public Profile Profile { get; private set; }
    }
}
