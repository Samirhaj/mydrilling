﻿using System.Linq;

namespace MyDrilling.Core.Entities.Spareparts
{
    public static class SparepartPermissionExt
    {
        public static bool CanView(this Sparepart sparepart, CombinedUserRoles roles)
        {
            //todo make sure we do not need any permissions
            return true;
        }

        public static IQueryable<Sparepart> ApplyPermissions(this IQueryable<Sparepart> query, CombinedUserRoles roles)
        {
            //todo make sure we do not need any permissions
            return query;
        }
    }
}