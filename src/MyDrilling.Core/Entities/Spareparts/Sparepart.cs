﻿using System;

namespace MyDrilling.Core.Entities.Spareparts
{
    public class Sparepart
    {
        public long Id { get; set; }
        public string ReferenceId { get; set; } //for example, BA0010638
        public string MaterialNo { get; set; }
        public string Description { get; set; }
        public short Year { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime LastSeen { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
    }
}