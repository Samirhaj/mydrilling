﻿using System.Collections.Generic;
using System.Linq;

namespace MyDrilling.Core
{
    public static class NameExt
    {
        //Formatting Roman numerals logic - user for Rig name (Noble Bully Ii --> Noble Bully II)
        private static Dictionary<char, int> CharValues = new Dictionary<char, int> { { 'I', 1 }, { 'V', 5 }, { 'X', 10 }, { 'L', 50 }, { 'C', 100 }, { 'D', 500 }, { 'M', 1000 } };

        private static int RomanToArabic(string roman)
        {
            if (roman.Length == 0 || int.TryParse(roman, out int intNumber)) return 0;
            roman = roman.Replace("(", "").Replace(")", "").ToUpper();

            // The number doesn't begin with (.
            // Convert the letters' values.
            int total = 0;
            int lastValue = 0;
            for (int i = roman.Length - 1; i >= 0; i--)
            {
                if (!CharValues.ContainsKey(roman[i]))
                {
                    total = 0;
                    break;
                }
                int newValue = CharValues[roman[i]];

                // See if we should add or subtract.
                if (newValue < lastValue)
                    total -= newValue;
                else
                {
                    total += newValue;
                    lastValue = newValue;
                }
            }

            // Return the result.
            return total;
        }

        public static string ToCamelCase(this string inputString, bool checkForRomanNumerals = false)
        {
            if (string.IsNullOrEmpty(inputString)) return "";
            return string.Join(" ", inputString.Split()
                .Select(i => checkForRomanNumerals && RomanToArabic(i) > 0 
                    ? i.ToUpper() 
                    : !string.IsNullOrEmpty(i) ? char.ToUpperInvariant(i[0]) + i.Substring(1).ToLowerInvariant() : ""));
        }

        public static string ToCamelCase(this string inputString) => ToCamelCase(inputString, false);
        public static string ToCamelCaseWithRomanNumerals(this string inputString) => ToCamelCase(inputString, true);
    }
}
