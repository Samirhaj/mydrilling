﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Sparepart_Spareparts]') AND [c].[name] = N'InsertDate');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Sparepart_Spareparts] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Sparepart_Spareparts] DROP COLUMN [InsertDate];

GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Sparepart_Spareparts]') AND [c].[name] = N'ReferenceId');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [Sparepart_Spareparts] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [Sparepart_Spareparts] ALTER COLUMN [ReferenceId] nvarchar(20) NULL;

GO

DECLARE @var2 sysname;
SELECT @var2 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Sparepart_Spareparts]') AND [c].[name] = N'MaterialNo');
IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [Sparepart_Spareparts] DROP CONSTRAINT [' + @var2 + '];');
ALTER TABLE [Sparepart_Spareparts] ALTER COLUMN [MaterialNo] nvarchar(20) NULL;

GO

DECLARE @var3 sysname;
SELECT @var3 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Sparepart_Spareparts]') AND [c].[name] = N'Description');
IF @var3 IS NOT NULL EXEC(N'ALTER TABLE [Sparepart_Spareparts] DROP CONSTRAINT [' + @var3 + '];');
ALTER TABLE [Sparepart_Spareparts] ALTER COLUMN [Description] nvarchar(255) NULL;

GO

ALTER TABLE [Sparepart_Spareparts] ADD [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

ALTER TABLE [Sparepart_Spareparts] ADD [LastSeen] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.0000000';

GO

ALTER TABLE [Sparepart_Spareparts] ADD [Created] datetime2 NOT NULL DEFAULT (getutcdate());

GO

ALTER TABLE [Sparepart_Spareparts] ADD [Modified] datetime2 NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200710141914_SparepartAddLastSeenCreatedModified', N'3.1.3');

GO

