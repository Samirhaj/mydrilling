﻿CREATE TABLE [SapConnectorStatuses] (
    [Id] bigint NOT NULL IDENTITY,
    [Timestamp] datetime2 NOT NULL,
    [SapState] bit NOT NULL,
    [QueuesState] bit NOT NULL,
    [BlobsState] bit NOT NULL,
    CONSTRAINT [PK_SapConnectorStatuses] PRIMARY KEY ([Id])
);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200810095059_SapConnectorStatus', N'3.1.3');

GO

