﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyDrilling.Infrastructure.Migrations
{
    public partial class SparepartAddLastSeenCreatedModified : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "InsertDate",
                table: "Sparepart_Spareparts");

            migrationBuilder.AlterColumn<string>(
                name: "ReferenceId",
                table: "Sparepart_Spareparts",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MaterialNo",
                table: "Sparepart_Spareparts",
                maxLength: 20,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Sparepart_Spareparts",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Created",
                table: "Sparepart_Spareparts",
                nullable: false,
                defaultValueSql: "getutcdate()");

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Sparepart_Spareparts",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastSeen",
                table: "Sparepart_Spareparts",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "Modified",
                table: "Sparepart_Spareparts",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Created",
                table: "Sparepart_Spareparts");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Sparepart_Spareparts");

            migrationBuilder.DropColumn(
                name: "LastSeen",
                table: "Sparepart_Spareparts");

            migrationBuilder.DropColumn(
                name: "Modified",
                table: "Sparepart_Spareparts");

            migrationBuilder.AlterColumn<string>(
                name: "ReferenceId",
                table: "Sparepart_Spareparts",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 20,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "MaterialNo",
                table: "Sparepart_Spareparts",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 20,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Sparepart_Spareparts",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "InsertDate",
                table: "Sparepart_Spareparts",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
