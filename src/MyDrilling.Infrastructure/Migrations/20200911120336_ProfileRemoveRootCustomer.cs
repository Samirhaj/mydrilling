﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyDrilling.Infrastructure.Migrations
{
    public partial class ProfileRemoveRootCustomer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Permission_Profiles_RootCustomers_RootCustomerId",
                table: "Permission_Profiles");

            migrationBuilder.DropIndex(
                name: "IX_Permission_Profiles_RootCustomerId",
                table: "Permission_Profiles");

            migrationBuilder.DropColumn(
                name: "RootCustomerId",
                table: "Permission_Profiles");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "RootCustomerId",
                table: "Permission_Profiles",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateIndex(
                name: "IX_Permission_Profiles_RootCustomerId",
                table: "Permission_Profiles",
                column: "RootCustomerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Permission_Profiles_RootCustomers_RootCustomerId",
                table: "Permission_Profiles",
                column: "RootCustomerId",
                principalTable: "RootCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
