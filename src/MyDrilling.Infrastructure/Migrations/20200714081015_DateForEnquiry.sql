﻿ALTER TABLE [Enquiry_Enquiries] ADD [Date] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.0000000';

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200714081015_DateForEnquiry', N'3.1.3');

GO

