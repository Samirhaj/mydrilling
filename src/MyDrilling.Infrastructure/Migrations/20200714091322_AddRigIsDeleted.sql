﻿ALTER TABLE [Rigs] ADD [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200714091322_AddRigIsDeleted', N'3.1.3');

GO

