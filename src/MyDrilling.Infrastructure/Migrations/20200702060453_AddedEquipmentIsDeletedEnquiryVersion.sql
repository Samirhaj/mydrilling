﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Enquiry_Enquiries]') AND [c].[name] = N'CommentsCount');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Enquiry_Enquiries] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Enquiry_Enquiries] DROP COLUMN [CommentsCount];

GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Enquiry_Enquiries]') AND [c].[name] = N'Timestamp');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [Enquiry_Enquiries] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [Enquiry_Enquiries] DROP COLUMN [Timestamp];

GO

DECLARE @var2 sysname;
SELECT @var2 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Ricon_Documents]') AND [c].[name] = N'PublishedDate');
IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [Ricon_Documents] DROP CONSTRAINT [' + @var2 + '];');
ALTER TABLE [Ricon_Documents] ALTER COLUMN [PublishedDate] datetime2 NOT NULL;
ALTER TABLE [Ricon_Documents] ADD DEFAULT (getutcdate()) FOR [PublishedDate];

GO

DECLARE @var3 sysname;
SELECT @var3 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Equipments]') AND [c].[name] = N'Modified');
IF @var3 IS NOT NULL EXEC(N'ALTER TABLE [Equipments] DROP CONSTRAINT [' + @var3 + '];');
ALTER TABLE [Equipments] ALTER COLUMN [Modified] datetime2 NULL;

GO

ALTER TABLE [Equipments] ADD [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

ALTER TABLE [Enquiry_Enquiries] ADD [Version] bigint NOT NULL DEFAULT CAST(0 AS bigint);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200702060453_AddedEquipmentIsDeletedEnquiryVersion', N'3.1.3');

GO

