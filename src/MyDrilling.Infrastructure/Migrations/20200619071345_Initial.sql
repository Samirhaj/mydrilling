﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [Bulletin_BulletinTypes] (
    [Id] smallint NOT NULL IDENTITY,
    [Name] nvarchar(50) NOT NULL,
    [ConsolidatedName] nvarchar(50) NOT NULL,
    CONSTRAINT [PK_Bulletin_BulletinTypes] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Enquiry_MessageTemplates] (
    [Id] bigint NOT NULL IDENTITY,
    [EnquiryEventType] int NOT NULL,
    [Subject] nvarchar(max) NULL,
    [Body] nvarchar(max) NULL,
    CONSTRAINT [PK_Enquiry_MessageTemplates] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Sparepart_Spareparts] (
    [Id] bigint NOT NULL IDENTITY,
    [ReferenceId] nvarchar(max) NULL,
    [MaterialNo] nvarchar(max) NULL,
    [Description] nvarchar(max) NULL,
    [Year] smallint NOT NULL,
    [InsertDate] datetime2 NOT NULL,
    CONSTRAINT [PK_Sparepart_Spareparts] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Subscription_StaticSubscribers] (
    [Id] bigint NOT NULL IDENTITY,
    [Email] nvarchar(255) NULL,
    [FirstName] nvarchar(255) NULL,
    [LastName] nvarchar(255) NULL,
    [SubscriptionType] int NOT NULL,
    CONSTRAINT [PK_Subscription_StaticSubscribers] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [UpgradeMatrix_MessageTemplates] (
    [Id] bigint NOT NULL IDENTITY,
    [UpgradeMatrixEventType] int NOT NULL,
    [Subject] nvarchar(max) NULL,
    [Body] nvarchar(max) NULL,
    CONSTRAINT [PK_UpgradeMatrix_MessageTemplates] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Bulletin_ZaBulletins] (
    [Id] bigint NOT NULL IDENTITY,
    [ReferenceId] nvarchar(50) NOT NULL,
    [Title] nvarchar(max) NULL,
    [DocumentReferenceId] nvarchar(50) NULL,
    [TypeId] smallint NOT NULL,
    [IssueDate] datetime2 NOT NULL,
    [FileType] nvarchar(10) NULL,
    [Description] nvarchar(max) NULL,
    [CreatedBy] nvarchar(50) NULL,
    [Created] datetime2 NOT NULL,
    CONSTRAINT [PK_Bulletin_ZaBulletins] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Bulletin_ZaBulletins_Bulletin_BulletinTypes_TypeId] FOREIGN KEY ([TypeId]) REFERENCES [Bulletin_BulletinTypes] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Bulletin_ZbBulletins] (
    [Id] bigint NOT NULL IDENTITY,
    [ReferenceId] nvarchar(50) NOT NULL,
    [ZaBulletinId] bigint NOT NULL,
    [CustomerId] bigint NULL,
    [RigId] bigint NULL,
    [EquipmentId] bigint NULL,
    [ConfirmedByUserId] bigint NULL,
    [ConfirmationTimestamp] datetime2 NULL,
    [Group] smallint NOT NULL,
    [Status] smallint NOT NULL,
    [State] smallint NOT NULL,
    [CreatedBy] nvarchar(50) NULL,
    [Created] datetime2 NOT NULL,
    CONSTRAINT [PK_Bulletin_ZbBulletins] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Bulletin_ZbBulletins_Bulletin_ZaBulletins_ZaBulletinId] FOREIGN KEY ([ZaBulletinId]) REFERENCES [Bulletin_ZaBulletins] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Enquiry_Enquiries] (
    [Id] bigint NOT NULL IDENTITY,
    [Title] nvarchar(42) NULL,
    [Description] nvarchar(max) NULL,
    [Type] int NOT NULL,
    [State] int NOT NULL,
    [IsDeleted] bit NOT NULL,
    [ReferenceId] nvarchar(50) NULL,
    [Submitted] datetime2 NOT NULL,
    [LastCommented] datetime2 NULL,
    [CommentsCount] int NOT NULL,
    [LogicalState] int NOT NULL,
    [OldGlobalId] nvarchar(255) NULL,
    [Timestamp] rowversion NULL,
    [RigId] bigint NOT NULL,
    [RigOwnerId] bigint NOT NULL,
    [CustomerId] bigint NOT NULL,
    [RootCustomerId] bigint NULL,
    [EquipmentId] bigint NULL,
    [SupplierId] bigint NULL,
    [AssignedToCustomerId] bigint NULL,
    [CreatedById] bigint NULL,
    [Created] datetime2 NOT NULL,
    [ModifiedById] bigint NULL,
    [Modified] datetime2 NOT NULL,
    [ApprovedById] bigint NULL,
    [Approved] datetime2 NULL,
    [AssignedById] bigint NULL,
    [AssignedToId] bigint NULL,
    [Assigned] datetime2 NULL,
    [RejectedById] bigint NULL,
    [Rejected] datetime2 NULL,
    CONSTRAINT [PK_Enquiry_Enquiries] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Equipments] (
    [Id] bigint NOT NULL IDENTITY,
    [ReferenceId] bigint NOT NULL,
    [Name] nvarchar(50) NOT NULL,
    [SerialNumber] nvarchar(50) NULL,
    [TagNumber] nvarchar(50) NULL,
    [Material] nvarchar(50) NULL,
    [ParentReferenceId] bigint NULL,
    [ParentId] bigint NULL,
    [ProductLongReferenceId] nvarchar(50) NULL,
    [ProductShortReferenceId] nvarchar(50) NULL,
    [ProductName] nvarchar(200) NULL,
    [ProductGroup] nvarchar(200) NULL,
    [ProductCode] nvarchar(5) NULL,
    [RigReferenceId] bigint NULL,
    [RigId] bigint NULL,
    [RigOwnerReferenceId] nvarchar(50) NULL,
    [RigOwnerId] bigint NULL,
    [Created] datetime2 NOT NULL DEFAULT (getutcdate()),
    [Modified] datetime2 NOT NULL,
    CONSTRAINT [PK_Equipments] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Equipments_Equipments_ParentId] FOREIGN KEY ([ParentId]) REFERENCES [Equipments] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [FunctionalLocationLogs] (
    [Id] bigint NOT NULL IDENTITY,
    [EquipmentId] bigint NOT NULL,
    [CustomerId] bigint NOT NULL,
    [RigId] bigint NOT NULL,
    [ProductCode] nvarchar(5) NULL,
    [EffectiveDateTime] datetime2 NOT NULL,
    [Created] datetime2 NOT NULL,
    CONSTRAINT [PK_FunctionalLocationLogs] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_FunctionalLocationLogs_Equipments_EquipmentId] FOREIGN KEY ([EquipmentId]) REFERENCES [Equipments] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Rigs] (
    [Id] bigint NOT NULL IDENTITY,
    [ReferenceId] bigint NOT NULL,
    [Name] nvarchar(50) NOT NULL,
    [Sector] nvarchar(50) NULL,
    [Region] nvarchar(50) NULL,
    [Coordinates_Latitude] float NULL,
    [Coordinates_Longitude] float NULL,
    [TypeCode] nvarchar(50) NULL,
    [OwnerReferenceId] nvarchar(50) NULL,
    [OwnerId] bigint NULL,
    [RootOwnerId] bigint NULL,
    [RootOperatorId] bigint NULL,
    [Created] datetime2 NOT NULL DEFAULT (getutcdate()),
    [Modified] datetime2 NOT NULL,
    [ResponsibleUserId] bigint NULL,
    CONSTRAINT [PK_Rigs] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Documentation_TechnicalReports] (
    [Id] bigint NOT NULL IDENTITY,
    [ReferenceId] nvarchar(255) NOT NULL,
    [Name] nvarchar(max) NULL,
    [RigReferenceId] bigint NOT NULL,
    [RigId] bigint NULL,
    [ProductCode] nvarchar(max) NULL,
    [ProductName] nvarchar(max) NULL,
    [FileName] nvarchar(255) NULL,
    [DocumentDate] datetime2 NOT NULL DEFAULT (getutcdate()),
    [Author] nvarchar(30) NULL,
    [Url] nvarchar(255) NULL,
    [Created] datetime2 NOT NULL DEFAULT (getutcdate()),
    [Modified] datetime2 NULL,
    CONSTRAINT [PK_Documentation_TechnicalReports] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Documentation_TechnicalReports_Rigs_RigId] FOREIGN KEY ([RigId]) REFERENCES [Rigs] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Documentation_UserManuals] (
    [Id] bigint NOT NULL IDENTITY,
    [ReferenceId] nvarchar(60) NOT NULL,
    [Name] nvarchar(255) NULL,
    [RigReferenceId] bigint NOT NULL,
    [RigId] bigint NULL,
    [RelatedReferenceId] nvarchar(60) NULL,
    [ProductCode] nvarchar(5) NULL,
    [ProductName] nvarchar(50) NULL,
    [Discipline] nvarchar(50) NULL,
    [DisciplineCode] nvarchar(5) NULL,
    [FileName] nvarchar(255) NULL,
    [DocumentDate] datetime2 NOT NULL DEFAULT (getutcdate()),
    [Url] nvarchar(255) NULL,
    [IsMaster] bit NOT NULL,
    [Created] datetime2 NOT NULL DEFAULT (getutcdate()),
    [Modified] datetime2 NULL,
    CONSTRAINT [PK_Documentation_UserManuals] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Documentation_UserManuals_Rigs_RigId] FOREIGN KEY ([RigId]) REFERENCES [Rigs] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Ricon_RootEquipments] (
    [Id] bigint NOT NULL IDENTITY,
    [RigId] bigint NOT NULL,
    [ReferenceId] bigint NOT NULL,
    CONSTRAINT [PK_Ricon_RootEquipments] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Ricon_RootEquipments_Rigs_RigId] FOREIGN KEY ([RigId]) REFERENCES [Rigs] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Ricon_Joints] (
    [Id] bigint NOT NULL IDENTITY,
    [RootEquipmentId] bigint NOT NULL,
    [ReferenceId] bigint NOT NULL,
    [TagNumber] nvarchar(30) NULL,
    [Fatigue] nvarchar(40) NULL,
    [Coc] nvarchar(max) NULL,
    [Status] nvarchar(max) NULL,
    [LastVisualInspection] datetime2 NULL,
    [LastWallThicknessInspection] datetime2 NULL,
    [Created] datetime2 NOT NULL DEFAULT (getutcdate()),
    [Modified] datetime2 NULL,
    CONSTRAINT [PK_Ricon_Joints] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Ricon_Joints_Ricon_RootEquipments_RootEquipmentId] FOREIGN KEY ([RootEquipmentId]) REFERENCES [Ricon_RootEquipments] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Ricon_Documents] (
    [Id] bigint NOT NULL IDENTITY,
    [Description] nvarchar(40) NULL,
    [RootEquipmentId] bigint NULL,
    [JointReferenceId] bigint NOT NULL,
    [JointId] bigint NULL,
    [ReferenceId] bigint NOT NULL,
    [Version] nvarchar(2) NULL DEFAULT N'00',
    [Url] nvarchar(50) NULL,
    [PublishedDate] datetime2 NOT NULL,
    [Created] datetime2 NOT NULL DEFAULT (getutcdate()),
    [Modified] datetime2 NULL,
    CONSTRAINT [PK_Ricon_Documents] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Ricon_Documents_Ricon_Joints_JointId] FOREIGN KEY ([JointId]) REFERENCES [Ricon_Joints] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Ricon_Documents_Ricon_RootEquipments_RootEquipmentId] FOREIGN KEY ([RootEquipmentId]) REFERENCES [Ricon_RootEquipments] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [RootCustomers] (
    [Id] bigint NOT NULL IDENTITY,
    [Name] nvarchar(255) NOT NULL,
    [Subdomain] nvarchar(255) NULL,
    [IsInternal] bit NOT NULL,
    [DefaultCustomerId] bigint NULL,
    [CreatedById] bigint NULL,
    [Created] datetime2 NOT NULL,
    [ModifiedById] bigint NULL,
    [Modified] datetime2 NOT NULL,
    CONSTRAINT [PK_RootCustomers] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Customers] (
    [Id] bigint NOT NULL IDENTITY,
    [ReferenceId] nvarchar(50) NULL,
    [Name] nvarchar(100) NOT NULL,
    [Address_City] nvarchar(max) NULL,
    [Address_PostalCode] nvarchar(max) NULL,
    [RootCustomerId] bigint NULL,
    [Created] datetime2 NOT NULL DEFAULT (getutcdate()),
    [Modified] datetime2 NOT NULL,
    CONSTRAINT [PK_Customers] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Customers_RootCustomers_RootCustomerId] FOREIGN KEY ([RootCustomerId]) REFERENCES [RootCustomers] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Permission_Profiles] (
    [Id] bigint NOT NULL IDENTITY,
    [Name] nvarchar(300) NOT NULL,
    [RootCustomerId] bigint NOT NULL,
    CONSTRAINT [PK_Permission_Profiles] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Permission_Profiles_RootCustomers_RootCustomerId] FOREIGN KEY ([RootCustomerId]) REFERENCES [RootCustomers] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Users] (
    [Id] bigint NOT NULL IDENTITY,
    [Upn] nvarchar(255) NOT NULL,
    [Email] nvarchar(max) NULL,
    [IsActive] bit NOT NULL,
    [IsInternal] bit NOT NULL,
    [FirstName] nvarchar(255) NULL,
    [LastName] nvarchar(255) NULL,
    [DisplayName] nvarchar(255) NULL,
    [Position] nvarchar(255) NULL,
    [PhoneNumber] nvarchar(255) NULL,
    [MobileNumber] nvarchar(255) NULL,
    [FaxNumber] nvarchar(255) NULL,
    [Country] nvarchar(255) NULL,
    [CountryCode] nvarchar(2) NULL,
    [Image] nvarchar(255) NULL,
    [LastLogIn] datetime2 NULL,
    [AcceptedTermsAndConditions] datetime2 NULL,
    [RootCustomerId] bigint NULL,
    [CreatedById] bigint NULL,
    [Created] datetime2 NOT NULL,
    [ModifiedById] bigint NULL,
    [Modified] datetime2 NOT NULL,
    CONSTRAINT [PK_Users] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Users_Users_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Users_Users_ModifiedById] FOREIGN KEY ([ModifiedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Users_RootCustomers_RootCustomerId] FOREIGN KEY ([RootCustomerId]) REFERENCES [RootCustomers] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Permission_ProfileRoles] (
    [Id] bigint NOT NULL IDENTITY,
    [Role] int NOT NULL,
    [ProfileId] bigint NOT NULL,
    CONSTRAINT [PK_Permission_ProfileRoles] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Permission_ProfileRoles_Permission_Profiles_ProfileId] FOREIGN KEY ([ProfileId]) REFERENCES [Permission_Profiles] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [AsyncOperations] (
    [Id] bigint NOT NULL IDENTITY,
    [UserId] bigint NOT NULL,
    [Response] nvarchar(500) NULL,
    [CreatedAt] datetime2 NOT NULL,
    [UpdatedAt] datetime2 NOT NULL,
    [Status] int NOT NULL,
    CONSTRAINT [PK_AsyncOperations] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_AsyncOperations_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Enquiry_AddedUsers] (
    [Id] bigint NOT NULL IDENTITY,
    [EnquiryId] bigint NOT NULL,
    [UserId] bigint NOT NULL,
    [CreatedById] bigint NULL,
    [Created] datetime2 NOT NULL,
    CONSTRAINT [PK_Enquiry_AddedUsers] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Enquiry_AddedUsers_Users_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Enquiry_AddedUsers_Enquiry_Enquiries_EnquiryId] FOREIGN KEY ([EnquiryId]) REFERENCES [Enquiry_Enquiries] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Enquiry_AddedUsers_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Enquiry_Comments] (
    [Id] bigint NOT NULL IDENTITY,
    [ReferenceId] nvarchar(50) NULL,
    [Title] nvarchar(50) NULL,
    [Text] nvarchar(max) NULL,
    [IsInitial] bit NOT NULL,
    [CreatedById] bigint NULL,
    [Created] datetime2 NOT NULL,
    [EnquiryId] bigint NOT NULL,
    CONSTRAINT [PK_Enquiry_Comments] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Enquiry_Comments_Users_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Enquiry_Comments_Enquiry_Enquiries_EnquiryId] FOREIGN KEY ([EnquiryId]) REFERENCES [Enquiry_Enquiries] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Enquiry_ReadReceipts] (
    [Id] bigint NOT NULL IDENTITY,
    [Created] datetime2 NOT NULL,
    [EnquiryId] bigint NOT NULL,
    [UserId] bigint NOT NULL,
    CONSTRAINT [PK_Enquiry_ReadReceipts] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Enquiry_ReadReceipts_Enquiry_Enquiries_EnquiryId] FOREIGN KEY ([EnquiryId]) REFERENCES [Enquiry_Enquiries] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Enquiry_ReadReceipts_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Enquiry_StateLogs] (
    [Id] bigint NOT NULL IDENTITY,
    [EnquiryId] bigint NOT NULL,
    [State] int NOT NULL,
    [ChangedById] bigint NOT NULL,
    [Changed] datetime2 NOT NULL,
    CONSTRAINT [PK_Enquiry_StateLogs] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Enquiry_StateLogs_Users_ChangedById] FOREIGN KEY ([ChangedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Enquiry_StateLogs_Enquiry_Enquiries_EnquiryId] FOREIGN KEY ([EnquiryId]) REFERENCES [Enquiry_Enquiries] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Permission_UserProfiles] (
    [Id] bigint NOT NULL IDENTITY,
    [ProfileId] bigint NOT NULL,
    [UserId] bigint NOT NULL,
    [RigId] bigint NOT NULL,
    CONSTRAINT [PK_Permission_UserProfiles] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Permission_UserProfiles_Permission_Profiles_ProfileId] FOREIGN KEY ([ProfileId]) REFERENCES [Permission_Profiles] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Permission_UserProfiles_Rigs_RigId] FOREIGN KEY ([RigId]) REFERENCES [Rigs] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Permission_UserProfiles_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Permission_UserRoles] (
    [Id] bigint NOT NULL IDENTITY,
    [Role] int NOT NULL,
    [UserId] bigint NOT NULL,
    [RigId] bigint NULL,
    [RootCustomerId] bigint NULL,
    CONSTRAINT [PK_Permission_UserRoles] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Permission_UserRoles_Rigs_RigId] FOREIGN KEY ([RigId]) REFERENCES [Rigs] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Permission_UserRoles_RootCustomers_RootCustomerId] FOREIGN KEY ([RootCustomerId]) REFERENCES [RootCustomers] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Permission_UserRoles_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Subscription_EnquiryTypeFilters] (
    [Id] bigint NOT NULL IDENTITY,
    [UserId] bigint NOT NULL,
    [Type] int NOT NULL,
    [Created] datetime2 NOT NULL,
    CONSTRAINT [PK_Subscription_EnquiryTypeFilters] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Subscription_EnquiryTypeFilters_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Subscription_RigFilters] (
    [Id] bigint NOT NULL IDENTITY,
    [UserId] bigint NOT NULL,
    [RigId] bigint NOT NULL,
    [Created] datetime2 NOT NULL,
    CONSTRAINT [PK_Subscription_RigFilters] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Subscription_RigFilters_Rigs_RigId] FOREIGN KEY ([RigId]) REFERENCES [Rigs] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Subscription_RigFilters_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Subscription_UserSettings] (
    [Id] bigint NOT NULL IDENTITY,
    [UserId] bigint NOT NULL,
    [SubscriptionType] int NOT NULL,
    [Created] datetime2 NOT NULL,
    CONSTRAINT [PK_Subscription_UserSettings] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Subscription_UserSettings_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [UpgradeMatrix_CustomerSettings] (
    [Id] bigint NOT NULL IDENTITY,
    [RootCustomerId] bigint NOT NULL,
    [Key] nvarchar(100) NOT NULL,
    [Value] nvarchar(max) NULL,
    [CreatedById] bigint NULL,
    [Created] datetime2 NOT NULL DEFAULT (getutcdate()),
    [ModifiedById] bigint NULL,
    [Modified] datetime2 NOT NULL DEFAULT (getutcdate()),
    CONSTRAINT [PK_UpgradeMatrix_CustomerSettings] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_UpgradeMatrix_CustomerSettings_Users_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UpgradeMatrix_CustomerSettings_Users_ModifiedById] FOREIGN KEY ([ModifiedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UpgradeMatrix_CustomerSettings_RootCustomers_RootCustomerId] FOREIGN KEY ([RootCustomerId]) REFERENCES [RootCustomers] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [UpgradeMatrix_EquipmentCategory] (
    [Id] bigint NOT NULL IDENTITY,
    [ShortName] nvarchar(20) NOT NULL,
    [CreatedById] bigint NULL,
    [Created] datetime2 NOT NULL DEFAULT (getutcdate()),
    [ModifiedById] bigint NULL,
    [Modified] datetime2 NOT NULL DEFAULT (getutcdate()),
    CONSTRAINT [PK_UpgradeMatrix_EquipmentCategory] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_UpgradeMatrix_EquipmentCategory_Users_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UpgradeMatrix_EquipmentCategory_Users_ModifiedById] FOREIGN KEY ([ModifiedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [UpgradeMatrix_RigSettings] (
    [Id] bigint NOT NULL IDENTITY,
    [RigId] bigint NOT NULL,
    [ShortName] nvarchar(20) NOT NULL,
    [RigType] int NOT NULL,
    [CreatedById] bigint NULL,
    [Created] datetime2 NOT NULL DEFAULT (getutcdate()),
    [ModifiedById] bigint NULL,
    [Modified] datetime2 NOT NULL DEFAULT (getutcdate()),
    CONSTRAINT [PK_UpgradeMatrix_RigSettings] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_UpgradeMatrix_RigSettings_Users_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UpgradeMatrix_RigSettings_Users_ModifiedById] FOREIGN KEY ([ModifiedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UpgradeMatrix_RigSettings_Rigs_RigId] FOREIGN KEY ([RigId]) REFERENCES [Rigs] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [UserSettings] (
    [Id] bigint NOT NULL IDENTITY,
    [Key] nvarchar(500) NOT NULL,
    [Value] nvarchar(500) NOT NULL,
    [UserId] bigint NOT NULL,
    CONSTRAINT [PK_UserSettings] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_UserSettings_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Enquiry_Attachments] (
    [Id] bigint NOT NULL IDENTITY,
    [ReferenceId] nvarchar(50) NULL,
    [FileName] nvarchar(255) NULL,
    [FileExtension] nvarchar(10) NULL,
    [BlobPath] nvarchar(500) NULL,
    [EnquiryId] bigint NOT NULL,
    [CommentId] bigint NULL,
    [Created] datetime2 NOT NULL,
    [CreatedById] bigint NULL,
    CONSTRAINT [PK_Enquiry_Attachments] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Enquiry_Attachments_Enquiry_Comments_CommentId] FOREIGN KEY ([CommentId]) REFERENCES [Enquiry_Comments] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Enquiry_Attachments_Users_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Enquiry_Attachments_Enquiry_Enquiries_EnquiryId] FOREIGN KEY ([EnquiryId]) REFERENCES [Enquiry_Enquiries] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [UpgradeMatrix_CcnItem] (
    [Id] bigint NOT NULL IDENTITY,
    [RootCustomerId] bigint NOT NULL,
    [EquipmentCategoryId] bigint NOT NULL,
    [Description] nvarchar(max) NOT NULL,
    [Comments] nvarchar(max) NULL,
    [PackageUpgrade] int NOT NULL,
    [RigType] int NOT NULL,
    [CreatedById] bigint NULL,
    [Created] datetime2 NOT NULL DEFAULT (getutcdate()),
    [ModifiedById] bigint NULL,
    [Modified] datetime2 NOT NULL DEFAULT (getutcdate()),
    CONSTRAINT [PK_UpgradeMatrix_CcnItem] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_UpgradeMatrix_CcnItem_Users_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UpgradeMatrix_CcnItem_UpgradeMatrix_EquipmentCategory_EquipmentCategoryId] FOREIGN KEY ([EquipmentCategoryId]) REFERENCES [UpgradeMatrix_EquipmentCategory] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UpgradeMatrix_CcnItem_Users_ModifiedById] FOREIGN KEY ([ModifiedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UpgradeMatrix_CcnItem_RootCustomers_RootCustomerId] FOREIGN KEY ([RootCustomerId]) REFERENCES [RootCustomers] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [UpgradeMatrix_QuotationItem] (
    [Id] bigint NOT NULL IDENTITY,
    [RootCustomerId] bigint NOT NULL,
    [EquipmentCategoryId] bigint NOT NULL,
    [Description] nvarchar(max) NOT NULL,
    [Heading] nvarchar(1000) NULL,
    [Benefits] nvarchar(1000) NULL,
    [CreatedById] bigint NULL,
    [Created] datetime2 NOT NULL DEFAULT (getutcdate()),
    [ModifiedById] bigint NULL,
    [Modified] datetime2 NOT NULL DEFAULT (getutcdate()),
    CONSTRAINT [PK_UpgradeMatrix_QuotationItem] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_UpgradeMatrix_QuotationItem_Users_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UpgradeMatrix_QuotationItem_UpgradeMatrix_EquipmentCategory_EquipmentCategoryId] FOREIGN KEY ([EquipmentCategoryId]) REFERENCES [UpgradeMatrix_EquipmentCategory] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UpgradeMatrix_QuotationItem_Users_ModifiedById] FOREIGN KEY ([ModifiedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UpgradeMatrix_QuotationItem_RootCustomers_RootCustomerId] FOREIGN KEY ([RootCustomerId]) REFERENCES [RootCustomers] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [UpgradeMatrix_CcnState] (
    [Id] bigint NOT NULL IDENTITY,
    [CcnItemId] bigint NOT NULL,
    [RigSettingsId] bigint NOT NULL,
    [Status] int NOT NULL,
    [CcnName] nvarchar(20) NULL,
    [DocumentPath] nvarchar(max) NULL,
    [CreatedById] bigint NULL,
    [Created] datetime2 NOT NULL DEFAULT (getutcdate()),
    [ModifiedById] bigint NULL,
    [Modified] datetime2 NOT NULL DEFAULT (getutcdate()),
    CONSTRAINT [PK_UpgradeMatrix_CcnState] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_UpgradeMatrix_CcnState_UpgradeMatrix_CcnItem_CcnItemId] FOREIGN KEY ([CcnItemId]) REFERENCES [UpgradeMatrix_CcnItem] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UpgradeMatrix_CcnState_Users_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UpgradeMatrix_CcnState_Users_ModifiedById] FOREIGN KEY ([ModifiedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UpgradeMatrix_CcnState_UpgradeMatrix_RigSettings_RigSettingsId] FOREIGN KEY ([RigSettingsId]) REFERENCES [UpgradeMatrix_RigSettings] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [UpgradeMatrix_QuotationState] (
    [Id] bigint NOT NULL IDENTITY,
    [QuotationItemId] bigint NOT NULL,
    [RigSettingsId] bigint NOT NULL,
    [Status] int NOT NULL,
    [CreatedById] bigint NULL,
    [Created] datetime2 NOT NULL DEFAULT (getutcdate()),
    [ModifiedById] bigint NULL,
    [Modified] datetime2 NOT NULL DEFAULT (getutcdate()),
    CONSTRAINT [PK_UpgradeMatrix_QuotationState] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_UpgradeMatrix_QuotationState_Users_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UpgradeMatrix_QuotationState_Users_ModifiedById] FOREIGN KEY ([ModifiedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UpgradeMatrix_QuotationState_UpgradeMatrix_QuotationItem_QuotationItemId] FOREIGN KEY ([QuotationItemId]) REFERENCES [UpgradeMatrix_QuotationItem] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_UpgradeMatrix_QuotationState_UpgradeMatrix_RigSettings_RigSettingsId] FOREIGN KEY ([RigSettingsId]) REFERENCES [UpgradeMatrix_RigSettings] ([Id]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_AsyncOperations_UserId] ON [AsyncOperations] ([UserId]);

GO

CREATE UNIQUE INDEX [IX_Bulletin_ZaBulletins_ReferenceId] ON [Bulletin_ZaBulletins] ([ReferenceId]);

GO

CREATE INDEX [IX_Bulletin_ZaBulletins_TypeId] ON [Bulletin_ZaBulletins] ([TypeId]);

GO

CREATE INDEX [IX_Bulletin_ZbBulletins_ConfirmedByUserId] ON [Bulletin_ZbBulletins] ([ConfirmedByUserId]);

GO

CREATE INDEX [IX_Bulletin_ZbBulletins_CustomerId] ON [Bulletin_ZbBulletins] ([CustomerId]);

GO

CREATE INDEX [IX_Bulletin_ZbBulletins_EquipmentId] ON [Bulletin_ZbBulletins] ([EquipmentId]);

GO

CREATE INDEX [IX_Bulletin_ZbBulletins_RigId] ON [Bulletin_ZbBulletins] ([RigId]);

GO

CREATE INDEX [IX_Bulletin_ZbBulletins_ZaBulletinId] ON [Bulletin_ZbBulletins] ([ZaBulletinId]);

GO

CREATE UNIQUE INDEX [IX_Bulletin_ZbBulletins_ReferenceId_Group] ON [Bulletin_ZbBulletins] ([ReferenceId], [Group]);

GO

CREATE UNIQUE INDEX [IX_Customers_ReferenceId] ON [Customers] ([ReferenceId]) WHERE [ReferenceId] IS NOT NULL;

GO

CREATE INDEX [IX_Customers_RootCustomerId] ON [Customers] ([RootCustomerId]);

GO

CREATE INDEX [IX_Documentation_TechnicalReports_DocumentDate] ON [Documentation_TechnicalReports] ([DocumentDate]);

GO

CREATE INDEX [IX_Documentation_TechnicalReports_RigId] ON [Documentation_TechnicalReports] ([RigId]);

GO

CREATE INDEX [IX_Documentation_UserManuals_DocumentDate] ON [Documentation_UserManuals] ([DocumentDate]);

GO

CREATE INDEX [IX_Documentation_UserManuals_RigId_DisciplineCode_IsMaster] ON [Documentation_UserManuals] ([RigId], [DisciplineCode], [IsMaster]) INCLUDE ([Discipline]);

GO

CREATE INDEX [IX_Enquiry_AddedUsers_CreatedById] ON [Enquiry_AddedUsers] ([CreatedById]);

GO

CREATE INDEX [IX_Enquiry_AddedUsers_UserId] ON [Enquiry_AddedUsers] ([UserId]);

GO

CREATE UNIQUE INDEX [IX_Enquiry_AddedUsers_EnquiryId_UserId] ON [Enquiry_AddedUsers] ([EnquiryId], [UserId]);

GO

CREATE INDEX [IX_Enquiry_Attachments_CommentId] ON [Enquiry_Attachments] ([CommentId]);

GO

CREATE INDEX [IX_Enquiry_Attachments_CreatedById] ON [Enquiry_Attachments] ([CreatedById]);

GO

CREATE INDEX [IX_Enquiry_Attachments_EnquiryId] ON [Enquiry_Attachments] ([EnquiryId]);

GO

CREATE UNIQUE INDEX [IX_Enquiry_Attachments_ReferenceId] ON [Enquiry_Attachments] ([ReferenceId]) WHERE [ReferenceId] IS NOT NULL;

GO

CREATE INDEX [IX_Enquiry_Comments_CreatedById] ON [Enquiry_Comments] ([CreatedById]);

GO

CREATE INDEX [IX_Enquiry_Comments_EnquiryId] ON [Enquiry_Comments] ([EnquiryId]);

GO

CREATE UNIQUE INDEX [IX_Enquiry_Comments_ReferenceId] ON [Enquiry_Comments] ([ReferenceId]) WHERE [ReferenceId] IS NOT NULL;

GO

CREATE INDEX [IX_Enquiry_Enquiries_ApprovedById] ON [Enquiry_Enquiries] ([ApprovedById]);

GO

CREATE INDEX [IX_Enquiry_Enquiries_AssignedById] ON [Enquiry_Enquiries] ([AssignedById]);

GO

CREATE INDEX [IX_Enquiry_Enquiries_AssignedToCustomerId] ON [Enquiry_Enquiries] ([AssignedToCustomerId]);

GO

CREATE INDEX [IX_Enquiry_Enquiries_AssignedToId] ON [Enquiry_Enquiries] ([AssignedToId]);

GO

CREATE INDEX [IX_Enquiry_Enquiries_CreatedById] ON [Enquiry_Enquiries] ([CreatedById]);

GO

CREATE INDEX [IX_Enquiry_Enquiries_CustomerId] ON [Enquiry_Enquiries] ([CustomerId]);

GO

CREATE INDEX [IX_Enquiry_Enquiries_EquipmentId] ON [Enquiry_Enquiries] ([EquipmentId]);

GO

CREATE INDEX [IX_Enquiry_Enquiries_ModifiedById] ON [Enquiry_Enquiries] ([ModifiedById]);

GO

CREATE INDEX [IX_Enquiry_Enquiries_OldGlobalId] ON [Enquiry_Enquiries] ([OldGlobalId]);

GO

CREATE UNIQUE INDEX [IX_Enquiry_Enquiries_ReferenceId] ON [Enquiry_Enquiries] ([ReferenceId]) WHERE [ReferenceId] IS NOT NULL;

GO

CREATE INDEX [IX_Enquiry_Enquiries_RejectedById] ON [Enquiry_Enquiries] ([RejectedById]);

GO

CREATE INDEX [IX_Enquiry_Enquiries_RigId] ON [Enquiry_Enquiries] ([RigId]);

GO

CREATE INDEX [IX_Enquiry_Enquiries_RigOwnerId] ON [Enquiry_Enquiries] ([RigOwnerId]);

GO

CREATE INDEX [IX_Enquiry_Enquiries_RootCustomerId] ON [Enquiry_Enquiries] ([RootCustomerId]);

GO

CREATE INDEX [IX_Enquiry_Enquiries_SupplierId] ON [Enquiry_Enquiries] ([SupplierId]);

GO

CREATE INDEX [IX_Enquiry_ReadReceipts_UserId] ON [Enquiry_ReadReceipts] ([UserId]);

GO

CREATE UNIQUE INDEX [IX_Enquiry_ReadReceipts_EnquiryId_UserId] ON [Enquiry_ReadReceipts] ([EnquiryId], [UserId]);

GO

CREATE INDEX [IX_Enquiry_StateLogs_ChangedById] ON [Enquiry_StateLogs] ([ChangedById]);

GO

CREATE INDEX [IX_Enquiry_StateLogs_EnquiryId] ON [Enquiry_StateLogs] ([EnquiryId]);

GO

CREATE INDEX [IX_Equipments_ParentId] ON [Equipments] ([ParentId]);

GO

CREATE UNIQUE INDEX [IX_Equipments_ReferenceId] ON [Equipments] ([ReferenceId]);

GO

CREATE INDEX [IX_Equipments_RigId] ON [Equipments] ([RigId]);

GO

CREATE INDEX [IX_Equipments_RigOwnerId] ON [Equipments] ([RigOwnerId]);

GO

CREATE INDEX [IX_FunctionalLocationLogs_CustomerId] ON [FunctionalLocationLogs] ([CustomerId]);

GO

CREATE INDEX [IX_FunctionalLocationLogs_EquipmentId] ON [FunctionalLocationLogs] ([EquipmentId]);

GO

CREATE INDEX [IX_FunctionalLocationLogs_RigId] ON [FunctionalLocationLogs] ([RigId]);

GO

CREATE UNIQUE INDEX [IX_Permission_ProfileRoles_ProfileId_Role] ON [Permission_ProfileRoles] ([ProfileId], [Role]);

GO

CREATE INDEX [IX_Permission_Profiles_RootCustomerId] ON [Permission_Profiles] ([RootCustomerId]);

GO

CREATE INDEX [IX_Permission_UserProfiles_ProfileId] ON [Permission_UserProfiles] ([ProfileId]);

GO

CREATE INDEX [IX_Permission_UserProfiles_RigId] ON [Permission_UserProfiles] ([RigId]);

GO

CREATE INDEX [IX_Permission_UserProfiles_UserId] ON [Permission_UserProfiles] ([UserId]);

GO

CREATE INDEX [IX_Permission_UserRoles_RigId] ON [Permission_UserRoles] ([RigId]);

GO

CREATE INDEX [IX_Permission_UserRoles_RootCustomerId] ON [Permission_UserRoles] ([RootCustomerId]);

GO

CREATE INDEX [IX_Permission_UserRoles_UserId] ON [Permission_UserRoles] ([UserId]);

GO

CREATE INDEX [IX_Ricon_Documents_JointId] ON [Ricon_Documents] ([JointId]);

GO

CREATE INDEX [IX_Ricon_Documents_RootEquipmentId] ON [Ricon_Documents] ([RootEquipmentId]);

GO

CREATE UNIQUE INDEX [IX_Ricon_Joints_RootEquipmentId_ReferenceId] ON [Ricon_Joints] ([RootEquipmentId], [ReferenceId]);

GO

CREATE UNIQUE INDEX [IX_Ricon_RootEquipments_RigId_ReferenceId] ON [Ricon_RootEquipments] ([RigId], [ReferenceId]);

GO

CREATE INDEX [IX_Rigs_OwnerId] ON [Rigs] ([OwnerId]);

GO

CREATE UNIQUE INDEX [IX_Rigs_ReferenceId] ON [Rigs] ([ReferenceId]);

GO

CREATE INDEX [IX_Rigs_ResponsibleUserId] ON [Rigs] ([ResponsibleUserId]);

GO

CREATE INDEX [IX_Rigs_RootOperatorId] ON [Rigs] ([RootOperatorId]);

GO

CREATE INDEX [IX_Rigs_RootOwnerId] ON [Rigs] ([RootOwnerId]);

GO

CREATE INDEX [IX_RootCustomers_CreatedById] ON [RootCustomers] ([CreatedById]);

GO

CREATE INDEX [IX_RootCustomers_DefaultCustomerId] ON [RootCustomers] ([DefaultCustomerId]);

GO

CREATE INDEX [IX_RootCustomers_ModifiedById] ON [RootCustomers] ([ModifiedById]);

GO

CREATE UNIQUE INDEX [IX_RootCustomers_Name] ON [RootCustomers] ([Name]);

GO

CREATE UNIQUE INDEX [IX_Subscription_EnquiryTypeFilters_UserId_Type] ON [Subscription_EnquiryTypeFilters] ([UserId], [Type]);

GO

CREATE INDEX [IX_Subscription_RigFilters_RigId] ON [Subscription_RigFilters] ([RigId]);

GO

CREATE UNIQUE INDEX [IX_Subscription_RigFilters_UserId_RigId] ON [Subscription_RigFilters] ([UserId], [RigId]);

GO

CREATE UNIQUE INDEX [IX_Subscription_UserSettings_UserId_SubscriptionType] ON [Subscription_UserSettings] ([UserId], [SubscriptionType]);

GO

CREATE INDEX [IX_UpgradeMatrix_CcnItem_CreatedById] ON [UpgradeMatrix_CcnItem] ([CreatedById]);

GO

CREATE INDEX [IX_UpgradeMatrix_CcnItem_EquipmentCategoryId] ON [UpgradeMatrix_CcnItem] ([EquipmentCategoryId]);

GO

CREATE INDEX [IX_UpgradeMatrix_CcnItem_ModifiedById] ON [UpgradeMatrix_CcnItem] ([ModifiedById]);

GO

CREATE INDEX [IX_UpgradeMatrix_CcnItem_RootCustomerId] ON [UpgradeMatrix_CcnItem] ([RootCustomerId]);

GO

CREATE INDEX [IX_UpgradeMatrix_CcnState_CcnItemId] ON [UpgradeMatrix_CcnState] ([CcnItemId]);

GO

CREATE INDEX [IX_UpgradeMatrix_CcnState_CreatedById] ON [UpgradeMatrix_CcnState] ([CreatedById]);

GO

CREATE INDEX [IX_UpgradeMatrix_CcnState_ModifiedById] ON [UpgradeMatrix_CcnState] ([ModifiedById]);

GO

CREATE INDEX [IX_UpgradeMatrix_CcnState_RigSettingsId] ON [UpgradeMatrix_CcnState] ([RigSettingsId]);

GO

CREATE INDEX [IX_UpgradeMatrix_CustomerSettings_CreatedById] ON [UpgradeMatrix_CustomerSettings] ([CreatedById]);

GO

CREATE INDEX [IX_UpgradeMatrix_CustomerSettings_ModifiedById] ON [UpgradeMatrix_CustomerSettings] ([ModifiedById]);

GO

CREATE UNIQUE INDEX [IX_UpgradeMatrix_CustomerSettings_RootCustomerId_Key] ON [UpgradeMatrix_CustomerSettings] ([RootCustomerId], [Key]);

GO

CREATE INDEX [IX_UpgradeMatrix_EquipmentCategory_CreatedById] ON [UpgradeMatrix_EquipmentCategory] ([CreatedById]);

GO

CREATE INDEX [IX_UpgradeMatrix_EquipmentCategory_ModifiedById] ON [UpgradeMatrix_EquipmentCategory] ([ModifiedById]);

GO

CREATE INDEX [IX_UpgradeMatrix_QuotationItem_CreatedById] ON [UpgradeMatrix_QuotationItem] ([CreatedById]);

GO

CREATE INDEX [IX_UpgradeMatrix_QuotationItem_EquipmentCategoryId] ON [UpgradeMatrix_QuotationItem] ([EquipmentCategoryId]);

GO

CREATE INDEX [IX_UpgradeMatrix_QuotationItem_ModifiedById] ON [UpgradeMatrix_QuotationItem] ([ModifiedById]);

GO

CREATE INDEX [IX_UpgradeMatrix_QuotationItem_RootCustomerId] ON [UpgradeMatrix_QuotationItem] ([RootCustomerId]);

GO

CREATE INDEX [IX_UpgradeMatrix_QuotationState_CreatedById] ON [UpgradeMatrix_QuotationState] ([CreatedById]);

GO

CREATE INDEX [IX_UpgradeMatrix_QuotationState_ModifiedById] ON [UpgradeMatrix_QuotationState] ([ModifiedById]);

GO

CREATE INDEX [IX_UpgradeMatrix_QuotationState_QuotationItemId] ON [UpgradeMatrix_QuotationState] ([QuotationItemId]);

GO

CREATE INDEX [IX_UpgradeMatrix_QuotationState_RigSettingsId] ON [UpgradeMatrix_QuotationState] ([RigSettingsId]);

GO

CREATE INDEX [IX_UpgradeMatrix_RigSettings_CreatedById] ON [UpgradeMatrix_RigSettings] ([CreatedById]);

GO

CREATE INDEX [IX_UpgradeMatrix_RigSettings_ModifiedById] ON [UpgradeMatrix_RigSettings] ([ModifiedById]);

GO

CREATE INDEX [IX_UpgradeMatrix_RigSettings_RigId] ON [UpgradeMatrix_RigSettings] ([RigId]);

GO

CREATE UNIQUE INDEX [IX_UpgradeMatrix_RigSettings_ShortName] ON [UpgradeMatrix_RigSettings] ([ShortName]);

GO

CREATE INDEX [IX_Users_CreatedById] ON [Users] ([CreatedById]);

GO

CREATE INDEX [IX_Users_ModifiedById] ON [Users] ([ModifiedById]);

GO

CREATE INDEX [IX_Users_RootCustomerId] ON [Users] ([RootCustomerId]);

GO

CREATE UNIQUE INDEX [IX_Users_Upn] ON [Users] ([Upn]);

GO

CREATE UNIQUE INDEX [IX_UserSettings_UserId_Key] ON [UserSettings] ([UserId], [Key]);

GO

ALTER TABLE [Bulletin_ZbBulletins] ADD CONSTRAINT [FK_Bulletin_ZbBulletins_Users_ConfirmedByUserId] FOREIGN KEY ([ConfirmedByUserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Bulletin_ZbBulletins] ADD CONSTRAINT [FK_Bulletin_ZbBulletins_Customers_CustomerId] FOREIGN KEY ([CustomerId]) REFERENCES [Customers] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Bulletin_ZbBulletins] ADD CONSTRAINT [FK_Bulletin_ZbBulletins_Equipments_EquipmentId] FOREIGN KEY ([EquipmentId]) REFERENCES [Equipments] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Bulletin_ZbBulletins] ADD CONSTRAINT [FK_Bulletin_ZbBulletins_Rigs_RigId] FOREIGN KEY ([RigId]) REFERENCES [Rigs] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Enquiry_Enquiries] ADD CONSTRAINT [FK_Enquiry_Enquiries_Users_ApprovedById] FOREIGN KEY ([ApprovedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Enquiry_Enquiries] ADD CONSTRAINT [FK_Enquiry_Enquiries_Users_AssignedById] FOREIGN KEY ([AssignedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Enquiry_Enquiries] ADD CONSTRAINT [FK_Enquiry_Enquiries_Users_AssignedToId] FOREIGN KEY ([AssignedToId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Enquiry_Enquiries] ADD CONSTRAINT [FK_Enquiry_Enquiries_Users_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Enquiry_Enquiries] ADD CONSTRAINT [FK_Enquiry_Enquiries_Users_ModifiedById] FOREIGN KEY ([ModifiedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Enquiry_Enquiries] ADD CONSTRAINT [FK_Enquiry_Enquiries_Users_RejectedById] FOREIGN KEY ([RejectedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Enquiry_Enquiries] ADD CONSTRAINT [FK_Enquiry_Enquiries_Customers_CustomerId] FOREIGN KEY ([CustomerId]) REFERENCES [Customers] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Enquiry_Enquiries] ADD CONSTRAINT [FK_Enquiry_Enquiries_Customers_RigOwnerId] FOREIGN KEY ([RigOwnerId]) REFERENCES [Customers] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Enquiry_Enquiries] ADD CONSTRAINT [FK_Enquiry_Enquiries_Equipments_EquipmentId] FOREIGN KEY ([EquipmentId]) REFERENCES [Equipments] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Enquiry_Enquiries] ADD CONSTRAINT [FK_Enquiry_Enquiries_Rigs_RigId] FOREIGN KEY ([RigId]) REFERENCES [Rigs] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Enquiry_Enquiries] ADD CONSTRAINT [FK_Enquiry_Enquiries_RootCustomers_AssignedToCustomerId] FOREIGN KEY ([AssignedToCustomerId]) REFERENCES [RootCustomers] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Enquiry_Enquiries] ADD CONSTRAINT [FK_Enquiry_Enquiries_RootCustomers_RootCustomerId] FOREIGN KEY ([RootCustomerId]) REFERENCES [RootCustomers] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Enquiry_Enquiries] ADD CONSTRAINT [FK_Enquiry_Enquiries_RootCustomers_SupplierId] FOREIGN KEY ([SupplierId]) REFERENCES [RootCustomers] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Equipments] ADD CONSTRAINT [FK_Equipments_Customers_RigOwnerId] FOREIGN KEY ([RigOwnerId]) REFERENCES [Customers] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Equipments] ADD CONSTRAINT [FK_Equipments_Rigs_RigId] FOREIGN KEY ([RigId]) REFERENCES [Rigs] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [FunctionalLocationLogs] ADD CONSTRAINT [FK_FunctionalLocationLogs_Customers_CustomerId] FOREIGN KEY ([CustomerId]) REFERENCES [Customers] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [FunctionalLocationLogs] ADD CONSTRAINT [FK_FunctionalLocationLogs_Rigs_RigId] FOREIGN KEY ([RigId]) REFERENCES [Rigs] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Rigs] ADD CONSTRAINT [FK_Rigs_Users_ResponsibleUserId] FOREIGN KEY ([ResponsibleUserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Rigs] ADD CONSTRAINT [FK_Rigs_Customers_OwnerId] FOREIGN KEY ([OwnerId]) REFERENCES [Customers] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Rigs] ADD CONSTRAINT [FK_Rigs_RootCustomers_RootOperatorId] FOREIGN KEY ([RootOperatorId]) REFERENCES [RootCustomers] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [Rigs] ADD CONSTRAINT [FK_Rigs_RootCustomers_RootOwnerId] FOREIGN KEY ([RootOwnerId]) REFERENCES [RootCustomers] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [RootCustomers] ADD CONSTRAINT [FK_RootCustomers_Users_CreatedById] FOREIGN KEY ([CreatedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [RootCustomers] ADD CONSTRAINT [FK_RootCustomers_Users_ModifiedById] FOREIGN KEY ([ModifiedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION;

GO

ALTER TABLE [RootCustomers] ADD CONSTRAINT [FK_RootCustomers_Customers_DefaultCustomerId] FOREIGN KEY ([DefaultCustomerId]) REFERENCES [Customers] ([Id]) ON DELETE NO ACTION;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200619071345_Initial', N'3.1.3');

GO

