﻿ALTER TABLE [Permission_Profiles] DROP CONSTRAINT [FK_Permission_Profiles_RootCustomers_RootCustomerId];

GO

DROP INDEX [IX_Permission_Profiles_RootCustomerId] ON [Permission_Profiles];

GO

DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Permission_Profiles]') AND [c].[name] = N'RootCustomerId');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Permission_Profiles] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Permission_Profiles] DROP COLUMN [RootCustomerId];

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200911120336_ProfileRemoveRootCustomer', N'3.1.3');

GO

