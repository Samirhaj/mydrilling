﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Rigs]') AND [c].[name] = N'Modified');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Rigs] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Rigs] ALTER COLUMN [Modified] datetime2 NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200707090329_RigModifiedNullable', N'3.1.3');

GO

