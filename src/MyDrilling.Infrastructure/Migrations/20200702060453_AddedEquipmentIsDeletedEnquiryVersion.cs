﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyDrilling.Infrastructure.Migrations
{
    public partial class AddedEquipmentIsDeletedEnquiryVersion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CommentsCount",
                table: "Enquiry_Enquiries");

            migrationBuilder.DropColumn(
                name: "Timestamp",
                table: "Enquiry_Enquiries");

            migrationBuilder.AlterColumn<DateTime>(
                name: "PublishedDate",
                table: "Ricon_Documents",
                nullable: false,
                defaultValueSql: "getutcdate()",
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Modified",
                table: "Equipments",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "Equipments",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<long>(
                name: "Version",
                table: "Enquiry_Enquiries",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "Equipments");

            migrationBuilder.DropColumn(
                name: "Version",
                table: "Enquiry_Enquiries");

            migrationBuilder.AlterColumn<DateTime>(
                name: "PublishedDate",
                table: "Ricon_Documents",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldDefaultValueSql: "getutcdate()");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Modified",
                table: "Equipments",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CommentsCount",
                table: "Enquiry_Enquiries",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<byte[]>(
                name: "Timestamp",
                table: "Enquiry_Enquiries",
                type: "rowversion",
                rowVersion: true,
                nullable: true);
        }
    }
}
