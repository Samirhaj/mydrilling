﻿ALTER TABLE [Users] ADD [ObjectId] nvarchar(255) NULL;

GO

ALTER TABLE [Users] ADD [TenantId] nvarchar(255) NULL;

GO

CREATE UNIQUE INDEX [IX_Users_ObjectId] ON [Users] ([ObjectId]) WHERE [ObjectId] IS NOT NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200730091612_UserObjectIdTenantId', N'3.1.3');

GO

