﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyDrilling.Infrastructure.Migrations
{
    public partial class ZaZbCreatedByCreatedModified : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Bulletin_ZbBulletins");

            migrationBuilder.DropColumn(
                name: "CreatedBy",
                table: "Bulletin_ZaBulletins");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Created",
                table: "Bulletin_ZbBulletins",
                nullable: false,
                defaultValueSql: "getutcdate()",
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<DateTime>(
                name: "Modified",
                table: "Bulletin_ZbBulletins",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Created",
                table: "Bulletin_ZaBulletins",
                nullable: false,
                defaultValueSql: "getutcdate()",
                oldClrType: typeof(DateTime),
                oldType: "datetime2");

            migrationBuilder.AddColumn<DateTime>(
                name: "Modifed",
                table: "Bulletin_ZaBulletins",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Modified",
                table: "Bulletin_ZbBulletins");

            migrationBuilder.DropColumn(
                name: "Modifed",
                table: "Bulletin_ZaBulletins");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Created",
                table: "Bulletin_ZbBulletins",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldDefaultValueSql: "getutcdate()");

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Bulletin_ZbBulletins",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Created",
                table: "Bulletin_ZaBulletins",
                type: "datetime2",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldDefaultValueSql: "getutcdate()");

            migrationBuilder.AddColumn<string>(
                name: "CreatedBy",
                table: "Bulletin_ZaBulletins",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);
        }
    }
}
