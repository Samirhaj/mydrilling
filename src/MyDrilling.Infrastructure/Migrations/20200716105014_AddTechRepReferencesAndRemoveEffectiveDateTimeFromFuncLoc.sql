﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[FunctionalLocationLogs]') AND [c].[name] = N'EffectiveDateTime');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [FunctionalLocationLogs] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [FunctionalLocationLogs] DROP COLUMN [EffectiveDateTime];

GO

ALTER TABLE [Documentation_TechnicalReports] ADD [DocumentReferenceId] bigint NOT NULL DEFAULT CAST(0 AS bigint);

GO

ALTER TABLE [Documentation_TechnicalReports] ADD [EquipmentReferenceId] bigint NOT NULL DEFAULT CAST(0 AS bigint);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200716105014_AddTechRepReferencesAndRemoveEffectiveDateTimeFromFuncLoc', N'3.1.3');

GO

