﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyDrilling.Infrastructure.Migrations
{
    public partial class AddTechRepReferencesAndRemoveEffectiveDateTimeFromFuncLoc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EffectiveDateTime",
                table: "FunctionalLocationLogs");

            migrationBuilder.AddColumn<long>(
                name: "DocumentReferenceId",
                table: "Documentation_TechnicalReports",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "EquipmentReferenceId",
                table: "Documentation_TechnicalReports",
                nullable: false,
                defaultValue: 0L);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DocumentReferenceId",
                table: "Documentation_TechnicalReports");

            migrationBuilder.DropColumn(
                name: "EquipmentReferenceId",
                table: "Documentation_TechnicalReports");

            migrationBuilder.AddColumn<DateTime>(
                name: "EffectiveDateTime",
                table: "FunctionalLocationLogs",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
