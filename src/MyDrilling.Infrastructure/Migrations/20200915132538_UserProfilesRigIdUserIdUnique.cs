﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyDrilling.Infrastructure.Migrations
{
    public partial class UserProfilesRigIdUserIdUnique : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Permission_UserProfiles_UserId",
                table: "Permission_UserProfiles");

            migrationBuilder.CreateIndex(
                name: "IX_Permission_UserProfiles_UserId_RigId",
                table: "Permission_UserProfiles",
                columns: new[] { "UserId", "RigId" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Permission_UserProfiles_UserId_RigId",
                table: "Permission_UserProfiles");

            migrationBuilder.CreateIndex(
                name: "IX_Permission_UserProfiles_UserId",
                table: "Permission_UserProfiles",
                column: "UserId");
        }
    }
}
