﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Subscription_StaticSubscribers]') AND [c].[name] = N'SubscriptionType');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Subscription_StaticSubscribers] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Subscription_StaticSubscribers] DROP COLUMN [SubscriptionType];

GO

CREATE TABLE [Subscription_StaticSubscriberEnquiryTypeFilters] (
    [Id] bigint NOT NULL IDENTITY,
    [StaticSubscriberId] bigint NOT NULL,
    [Type] int NOT NULL,
    [Created] datetime2 NOT NULL,
    CONSTRAINT [PK_Subscription_StaticSubscriberEnquiryTypeFilters] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Subscription_StaticSubscriberEnquiryTypeFilters_Subscription_StaticSubscribers_StaticSubscriberId] FOREIGN KEY ([StaticSubscriberId]) REFERENCES [Subscription_StaticSubscribers] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Subscription_StaticSubscriberRigFilters] (
    [Id] bigint NOT NULL IDENTITY,
    [StaticSubscriberId] bigint NOT NULL,
    [RigId] bigint NOT NULL,
    [Created] datetime2 NOT NULL,
    CONSTRAINT [PK_Subscription_StaticSubscriberRigFilters] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Subscription_StaticSubscriberRigFilters_Rigs_RigId] FOREIGN KEY ([RigId]) REFERENCES [Rigs] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_Subscription_StaticSubscriberRigFilters_Subscription_StaticSubscribers_StaticSubscriberId] FOREIGN KEY ([StaticSubscriberId]) REFERENCES [Subscription_StaticSubscribers] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Subscription_StaticSubscriberSettings] (
    [Id] bigint NOT NULL IDENTITY,
    [StaticSubscriberId] bigint NOT NULL,
    [SubscriptionType] int NOT NULL,
    [Created] datetime2 NOT NULL,
    CONSTRAINT [PK_Subscription_StaticSubscriberSettings] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Subscription_StaticSubscriberSettings_Subscription_StaticSubscribers_StaticSubscriberId] FOREIGN KEY ([StaticSubscriberId]) REFERENCES [Subscription_StaticSubscribers] ([Id]) ON DELETE NO ACTION
);

GO

CREATE UNIQUE INDEX [IX_Subscription_StaticSubscriberEnquiryTypeFilters_StaticSubscriberId_Type] ON [Subscription_StaticSubscriberEnquiryTypeFilters] ([StaticSubscriberId], [Type]);

GO

CREATE INDEX [IX_Subscription_StaticSubscriberRigFilters_RigId] ON [Subscription_StaticSubscriberRigFilters] ([RigId]);

GO

CREATE UNIQUE INDEX [IX_Subscription_StaticSubscriberRigFilters_StaticSubscriberId_RigId] ON [Subscription_StaticSubscriberRigFilters] ([StaticSubscriberId], [RigId]);

GO

CREATE UNIQUE INDEX [IX_Subscription_StaticSubscriberSettings_StaticSubscriberId_SubscriptionType] ON [Subscription_StaticSubscriberSettings] ([StaticSubscriberId], [SubscriptionType]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200731120558_StaticSubscriberSettings', N'3.1.3');

GO

