﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyDrilling.Infrastructure.Migrations
{
    public partial class ZaModifiedTypo : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Modifed",
                table: "Bulletin_ZaBulletins");

            migrationBuilder.AddColumn<DateTime>(
                name: "Modified",
                table: "Bulletin_ZaBulletins",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Modified",
                table: "Bulletin_ZaBulletins");

            migrationBuilder.AddColumn<DateTime>(
                name: "Modifed",
                table: "Bulletin_ZaBulletins",
                type: "datetime2",
                nullable: true);
        }
    }
}
