﻿DROP INDEX [IX_Permission_UserProfiles_UserId] ON [Permission_UserProfiles];

GO

CREATE UNIQUE INDEX [IX_Permission_UserProfiles_UserId_RigId] ON [Permission_UserProfiles] ([UserId], [RigId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200915132538_UserProfilesRigIdUserIdUnique', N'3.1.3');

GO

