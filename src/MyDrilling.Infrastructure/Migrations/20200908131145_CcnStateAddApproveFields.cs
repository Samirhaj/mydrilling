﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyDrilling.Infrastructure.Migrations
{
    public partial class CcnStateAddApproveFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Approved",
                table: "UpgradeMatrix_CcnState",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ApprovedById",
                table: "UpgradeMatrix_CcnState",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_CcnState_ApprovedById",
                table: "UpgradeMatrix_CcnState",
                column: "ApprovedById");

            migrationBuilder.AddForeignKey(
                name: "FK_UpgradeMatrix_CcnState_Users_ApprovedById",
                table: "UpgradeMatrix_CcnState",
                column: "ApprovedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UpgradeMatrix_CcnState_Users_ApprovedById",
                table: "UpgradeMatrix_CcnState");

            migrationBuilder.DropIndex(
                name: "IX_UpgradeMatrix_CcnState_ApprovedById",
                table: "UpgradeMatrix_CcnState");

            migrationBuilder.DropColumn(
                name: "Approved",
                table: "UpgradeMatrix_CcnState");

            migrationBuilder.DropColumn(
                name: "ApprovedById",
                table: "UpgradeMatrix_CcnState");
        }
    }
}
