﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Enquiry_StateLogs]') AND [c].[name] = N'ChangedById');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Enquiry_StateLogs] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Enquiry_StateLogs] ALTER COLUMN [ChangedById] bigint NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200722092324_EnquiryStateLogChangedByIdNullable', N'3.1.3');

GO

