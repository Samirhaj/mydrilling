﻿ALTER TABLE [UpgradeMatrix_QuotationItem] ADD [QuotationReference] nvarchar(max) NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200909081354_QuotationItemAddQuotationReference', N'3.1.3');

GO

