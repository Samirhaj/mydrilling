﻿ALTER TABLE [Users] ADD [ReferenceId] nvarchar(50) NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200710082443_ReferenceIdForUser', N'3.1.3');

GO

