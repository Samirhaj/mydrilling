﻿ALTER TABLE [UpgradeMatrix_CcnState] ADD [Approved] datetime2 NULL;

GO

ALTER TABLE [UpgradeMatrix_CcnState] ADD [ApprovedById] bigint NULL;

GO

CREATE INDEX [IX_UpgradeMatrix_CcnState_ApprovedById] ON [UpgradeMatrix_CcnState] ([ApprovedById]);

GO

ALTER TABLE [UpgradeMatrix_CcnState] ADD CONSTRAINT [FK_UpgradeMatrix_CcnState_Users_ApprovedById] FOREIGN KEY ([ApprovedById]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200908131145_CcnStateAddApproveFields', N'3.1.3');

GO

