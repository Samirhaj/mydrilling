﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Enquiry_Enquiries]') AND [c].[name] = N'Title');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Enquiry_Enquiries] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Enquiry_Enquiries] ALTER COLUMN [Title] nvarchar(41) NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200908131950_EnquiryTitleLengthChange', N'3.1.3');

GO

