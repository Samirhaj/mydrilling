﻿ALTER TABLE [Ricon_Joints] ADD [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

ALTER TABLE [Ricon_Documents] ADD [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200702103101_AddRiconIsDeleted', N'3.1.3');

GO

