﻿ALTER TABLE [Enquiry_Enquiries] ADD [LastActivity] datetime2 NOT NULL DEFAULT '0001-01-01T00:00:00.0000000';

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200717095032_AddEnquiryLastActivity', N'3.1.3');

GO

