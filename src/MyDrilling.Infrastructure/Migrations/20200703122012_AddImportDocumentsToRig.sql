﻿ALTER TABLE [Rigs] ADD [ImportDocumentation] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200703122012_AddImportDocumentsToRig', N'3.1.3');

GO

