﻿CREATE TABLE [Sparepart_Whitelist] (
    [Id] bigint NOT NULL IDENTITY,
    [GlobalId] nvarchar(25) NULL,
    CONSTRAINT [PK_Sparepart_Whitelist] PRIMARY KEY ([Id])
);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200703124334_AddSparepartWhitelistTable', N'3.1.3');

GO

