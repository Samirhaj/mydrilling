﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyDrilling.Infrastructure.Migrations
{
    public partial class EnquiryStateLogChangedByIdNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "ChangedById",
                table: "Enquiry_StateLogs",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "ChangedById",
                table: "Enquiry_StateLogs",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);
        }
    }
}
