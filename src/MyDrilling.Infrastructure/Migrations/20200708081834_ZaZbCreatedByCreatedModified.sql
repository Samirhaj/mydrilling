﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Bulletin_ZbBulletins]') AND [c].[name] = N'CreatedBy');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Bulletin_ZbBulletins] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Bulletin_ZbBulletins] DROP COLUMN [CreatedBy];

GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Bulletin_ZaBulletins]') AND [c].[name] = N'CreatedBy');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [Bulletin_ZaBulletins] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [Bulletin_ZaBulletins] DROP COLUMN [CreatedBy];

GO

DECLARE @var2 sysname;
SELECT @var2 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Bulletin_ZbBulletins]') AND [c].[name] = N'Created');
IF @var2 IS NOT NULL EXEC(N'ALTER TABLE [Bulletin_ZbBulletins] DROP CONSTRAINT [' + @var2 + '];');
ALTER TABLE [Bulletin_ZbBulletins] ALTER COLUMN [Created] datetime2 NOT NULL;
ALTER TABLE [Bulletin_ZbBulletins] ADD DEFAULT (getutcdate()) FOR [Created];

GO

ALTER TABLE [Bulletin_ZbBulletins] ADD [Modified] datetime2 NULL;

GO

DECLARE @var3 sysname;
SELECT @var3 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Bulletin_ZaBulletins]') AND [c].[name] = N'Created');
IF @var3 IS NOT NULL EXEC(N'ALTER TABLE [Bulletin_ZaBulletins] DROP CONSTRAINT [' + @var3 + '];');
ALTER TABLE [Bulletin_ZaBulletins] ALTER COLUMN [Created] datetime2 NOT NULL;
ALTER TABLE [Bulletin_ZaBulletins] ADD DEFAULT (getutcdate()) FOR [Created];

GO

ALTER TABLE [Bulletin_ZaBulletins] ADD [Modifed] datetime2 NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200708081834_ZaZbCreatedByCreatedModified', N'3.1.3');

GO

