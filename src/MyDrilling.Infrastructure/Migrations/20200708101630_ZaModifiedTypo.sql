﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[Bulletin_ZaBulletins]') AND [c].[name] = N'Modifed');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [Bulletin_ZaBulletins] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [Bulletin_ZaBulletins] DROP COLUMN [Modifed];

GO

ALTER TABLE [Bulletin_ZaBulletins] ADD [Modified] datetime2 NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200708101630_ZaModifiedTypo', N'3.1.3');

GO

