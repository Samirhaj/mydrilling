﻿ALTER TABLE [RootCustomers] ADD [IsDeleted] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20201019135649_RigsAddIsDeleted', N'3.1.3');

GO

