﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyDrilling.Infrastructure.Migrations
{
    public partial class StaticSubscriberSettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SubscriptionType",
                table: "Subscription_StaticSubscribers");

            migrationBuilder.CreateTable(
                name: "Subscription_StaticSubscriberEnquiryTypeFilters",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StaticSubscriberId = table.Column<long>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscription_StaticSubscriberEnquiryTypeFilters", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Subscription_StaticSubscriberEnquiryTypeFilters_Subscription_StaticSubscribers_StaticSubscriberId",
                        column: x => x.StaticSubscriberId,
                        principalTable: "Subscription_StaticSubscribers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Subscription_StaticSubscriberRigFilters",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StaticSubscriberId = table.Column<long>(nullable: false),
                    RigId = table.Column<long>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscription_StaticSubscriberRigFilters", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Subscription_StaticSubscriberRigFilters_Rigs_RigId",
                        column: x => x.RigId,
                        principalTable: "Rigs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Subscription_StaticSubscriberRigFilters_Subscription_StaticSubscribers_StaticSubscriberId",
                        column: x => x.StaticSubscriberId,
                        principalTable: "Subscription_StaticSubscribers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Subscription_StaticSubscriberSettings",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    StaticSubscriberId = table.Column<long>(nullable: false),
                    SubscriptionType = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscription_StaticSubscriberSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Subscription_StaticSubscriberSettings_Subscription_StaticSubscribers_StaticSubscriberId",
                        column: x => x.StaticSubscriberId,
                        principalTable: "Subscription_StaticSubscribers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Subscription_StaticSubscriberEnquiryTypeFilters_StaticSubscriberId_Type",
                table: "Subscription_StaticSubscriberEnquiryTypeFilters",
                columns: new[] { "StaticSubscriberId", "Type" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Subscription_StaticSubscriberRigFilters_RigId",
                table: "Subscription_StaticSubscriberRigFilters",
                column: "RigId");

            migrationBuilder.CreateIndex(
                name: "IX_Subscription_StaticSubscriberRigFilters_StaticSubscriberId_RigId",
                table: "Subscription_StaticSubscriberRigFilters",
                columns: new[] { "StaticSubscriberId", "RigId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Subscription_StaticSubscriberSettings_StaticSubscriberId_SubscriptionType",
                table: "Subscription_StaticSubscriberSettings",
                columns: new[] { "StaticSubscriberId", "SubscriptionType" },
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Subscription_StaticSubscriberEnquiryTypeFilters");

            migrationBuilder.DropTable(
                name: "Subscription_StaticSubscriberRigFilters");

            migrationBuilder.DropTable(
                name: "Subscription_StaticSubscriberSettings");

            migrationBuilder.AddColumn<int>(
                name: "SubscriptionType",
                table: "Subscription_StaticSubscribers",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
