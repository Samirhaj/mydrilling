﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyDrilling.Infrastructure.Migrations
{
    public partial class EnquiryTitleLengthChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Title",
                table: "Enquiry_Enquiries",
                maxLength: 41,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(42)",
                oldMaxLength: 42,
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Title",
                table: "Enquiry_Enquiries",
                type: "nvarchar(42)",
                maxLength: 42,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 41,
                oldNullable: true);
        }
    }
}
