﻿ALTER TABLE [RootCustomers] ADD [IsHidden] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

ALTER TABLE [Rigs] ADD [IsHidden] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20201023070104_RigsAndRootCustomersAddIsHidden', N'3.1.3');

GO

