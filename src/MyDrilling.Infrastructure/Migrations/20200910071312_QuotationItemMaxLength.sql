﻿DECLARE @var0 sysname;
SELECT @var0 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[UpgradeMatrix_QuotationItem]') AND [c].[name] = N'QuotationReference');
IF @var0 IS NOT NULL EXEC(N'ALTER TABLE [UpgradeMatrix_QuotationItem] DROP CONSTRAINT [' + @var0 + '];');
ALTER TABLE [UpgradeMatrix_QuotationItem] ALTER COLUMN [QuotationReference] nvarchar(50) NULL;

GO

DECLARE @var1 sysname;
SELECT @var1 = [d].[name]
FROM [sys].[default_constraints] [d]
INNER JOIN [sys].[columns] [c] ON [d].[parent_column_id] = [c].[column_id] AND [d].[parent_object_id] = [c].[object_id]
WHERE ([d].[parent_object_id] = OBJECT_ID(N'[UpgradeMatrix_QuotationItem]') AND [c].[name] = N'Description');
IF @var1 IS NOT NULL EXEC(N'ALTER TABLE [UpgradeMatrix_QuotationItem] DROP CONSTRAINT [' + @var1 + '];');
ALTER TABLE [UpgradeMatrix_QuotationItem] ALTER COLUMN [Description] nvarchar(2000) NOT NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200910071312_QuotationItemMaxLength', N'3.1.3');

GO

