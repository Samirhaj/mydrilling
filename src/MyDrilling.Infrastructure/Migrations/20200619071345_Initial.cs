﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MyDrilling.Infrastructure.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Bulletin_BulletinTypes",
                columns: table => new
                {
                    Id = table.Column<short>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    ConsolidatedName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bulletin_BulletinTypes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Enquiry_MessageTemplates",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EnquiryEventType = table.Column<int>(nullable: false),
                    Subject = table.Column<string>(nullable: true),
                    Body = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enquiry_MessageTemplates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Sparepart_Spareparts",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReferenceId = table.Column<string>(nullable: true),
                    MaterialNo = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Year = table.Column<short>(nullable: false),
                    InsertDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sparepart_Spareparts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Subscription_StaticSubscribers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Email = table.Column<string>(maxLength: 255, nullable: true),
                    FirstName = table.Column<string>(maxLength: 255, nullable: true),
                    LastName = table.Column<string>(maxLength: 255, nullable: true),
                    SubscriptionType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscription_StaticSubscribers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UpgradeMatrix_MessageTemplates",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UpgradeMatrixEventType = table.Column<int>(nullable: false),
                    Subject = table.Column<string>(nullable: true),
                    Body = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpgradeMatrix_MessageTemplates", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Bulletin_ZaBulletins",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReferenceId = table.Column<string>(maxLength: 50, nullable: false),
                    Title = table.Column<string>(nullable: true),
                    DocumentReferenceId = table.Column<string>(maxLength: 50, nullable: true),
                    TypeId = table.Column<short>(nullable: false),
                    IssueDate = table.Column<DateTime>(nullable: false),
                    FileType = table.Column<string>(maxLength: 10, nullable: true),
                    Description = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bulletin_ZaBulletins", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Bulletin_ZaBulletins_Bulletin_BulletinTypes_TypeId",
                        column: x => x.TypeId,
                        principalTable: "Bulletin_BulletinTypes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Bulletin_ZbBulletins",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReferenceId = table.Column<string>(maxLength: 50, nullable: false),
                    ZaBulletinId = table.Column<long>(nullable: false),
                    CustomerId = table.Column<long>(nullable: true),
                    RigId = table.Column<long>(nullable: true),
                    EquipmentId = table.Column<long>(nullable: true),
                    ConfirmedByUserId = table.Column<long>(nullable: true),
                    ConfirmationTimestamp = table.Column<DateTime>(nullable: true),
                    Group = table.Column<short>(nullable: false),
                    Status = table.Column<short>(nullable: false),
                    State = table.Column<short>(nullable: false),
                    CreatedBy = table.Column<string>(maxLength: 50, nullable: true),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bulletin_ZbBulletins", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Bulletin_ZbBulletins_Bulletin_ZaBulletins_ZaBulletinId",
                        column: x => x.ZaBulletinId,
                        principalTable: "Bulletin_ZaBulletins",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Enquiry_Enquiries",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(maxLength: 42, nullable: true),
                    Description = table.Column<string>(maxLength: 8000, nullable: true),
                    Type = table.Column<int>(nullable: false),
                    State = table.Column<int>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    ReferenceId = table.Column<string>(maxLength: 50, nullable: true),
                    Submitted = table.Column<DateTime>(nullable: false),
                    LastCommented = table.Column<DateTime>(nullable: true),
                    CommentsCount = table.Column<int>(nullable: false),
                    LogicalState = table.Column<int>(nullable: false),
                    OldGlobalId = table.Column<string>(maxLength: 255, nullable: true),
                    Timestamp = table.Column<byte[]>(rowVersion: true, nullable: true),
                    RigId = table.Column<long>(nullable: false),
                    RigOwnerId = table.Column<long>(nullable: false),
                    CustomerId = table.Column<long>(nullable: false),
                    RootCustomerId = table.Column<long>(nullable: true),
                    EquipmentId = table.Column<long>(nullable: true),
                    SupplierId = table.Column<long>(nullable: true),
                    AssignedToCustomerId = table.Column<long>(nullable: true),
                    CreatedById = table.Column<long>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    ModifiedById = table.Column<long>(nullable: true),
                    Modified = table.Column<DateTime>(nullable: false),
                    ApprovedById = table.Column<long>(nullable: true),
                    Approved = table.Column<DateTime>(nullable: true),
                    AssignedById = table.Column<long>(nullable: true),
                    AssignedToId = table.Column<long>(nullable: true),
                    Assigned = table.Column<DateTime>(nullable: true),
                    RejectedById = table.Column<long>(nullable: true),
                    Rejected = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enquiry_Enquiries", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Equipments",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReferenceId = table.Column<long>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    SerialNumber = table.Column<string>(maxLength: 50, nullable: true),
                    TagNumber = table.Column<string>(maxLength: 50, nullable: true),
                    Material = table.Column<string>(maxLength: 50, nullable: true),
                    ParentReferenceId = table.Column<long>(nullable: true),
                    ParentId = table.Column<long>(nullable: true),
                    ProductLongReferenceId = table.Column<string>(maxLength: 50, nullable: true),
                    ProductShortReferenceId = table.Column<string>(maxLength: 50, nullable: true),
                    ProductName = table.Column<string>(maxLength: 200, nullable: true),
                    ProductGroup = table.Column<string>(maxLength: 200, nullable: true),
                    ProductCode = table.Column<string>(maxLength: 5, nullable: true),
                    RigReferenceId = table.Column<long>(nullable: true),
                    RigId = table.Column<long>(nullable: true),
                    RigOwnerReferenceId = table.Column<string>(maxLength: 50, nullable: true),
                    RigOwnerId = table.Column<long>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    Modified = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Equipments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Equipments_Equipments_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Equipments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "FunctionalLocationLogs",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EquipmentId = table.Column<long>(nullable: false),
                    CustomerId = table.Column<long>(nullable: false),
                    RigId = table.Column<long>(nullable: false),
                    ProductCode = table.Column<string>(maxLength: 5, nullable: true),
                    EffectiveDateTime = table.Column<DateTime>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FunctionalLocationLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FunctionalLocationLogs_Equipments_EquipmentId",
                        column: x => x.EquipmentId,
                        principalTable: "Equipments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Rigs",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReferenceId = table.Column<long>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Sector = table.Column<string>(maxLength: 50, nullable: true),
                    Region = table.Column<string>(maxLength: 50, nullable: true),
                    Coordinates_Latitude = table.Column<double>(nullable: true),
                    Coordinates_Longitude = table.Column<double>(nullable: true),
                    TypeCode = table.Column<string>(maxLength: 50, nullable: true),
                    OwnerReferenceId = table.Column<string>(maxLength: 50, nullable: true),
                    OwnerId = table.Column<long>(nullable: true),
                    RootOwnerId = table.Column<long>(nullable: true),
                    RootOperatorId = table.Column<long>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    Modified = table.Column<DateTime>(nullable: false),
                    ResponsibleUserId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Rigs", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Documentation_TechnicalReports",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReferenceId = table.Column<string>(maxLength: 255, nullable: false),
                    Name = table.Column<string>(nullable: true),
                    RigReferenceId = table.Column<long>(nullable: false),
                    RigId = table.Column<long>(nullable: true),
                    ProductCode = table.Column<string>(nullable: true),
                    ProductName = table.Column<string>(nullable: true),
                    FileName = table.Column<string>(maxLength: 255, nullable: true),
                    DocumentDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    Author = table.Column<string>(maxLength: 30, nullable: true),
                    Url = table.Column<string>(maxLength: 255, nullable: true),
                    Created = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    Modified = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documentation_TechnicalReports", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Documentation_TechnicalReports_Rigs_RigId",
                        column: x => x.RigId,
                        principalTable: "Rigs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Documentation_UserManuals",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReferenceId = table.Column<string>(maxLength: 60, nullable: false),
                    Name = table.Column<string>(maxLength: 255, nullable: true),
                    RigReferenceId = table.Column<long>(nullable: false),
                    RigId = table.Column<long>(nullable: true),
                    RelatedReferenceId = table.Column<string>(maxLength: 60, nullable: true),
                    ProductCode = table.Column<string>(maxLength: 5, nullable: true),
                    ProductName = table.Column<string>(maxLength: 50, nullable: true),
                    Discipline = table.Column<string>(maxLength: 50, nullable: true),
                    DisciplineCode = table.Column<string>(maxLength: 5, nullable: true),
                    FileName = table.Column<string>(maxLength: 255, nullable: true),
                    DocumentDate = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    Url = table.Column<string>(maxLength: 255, nullable: true),
                    IsMaster = table.Column<bool>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    Modified = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Documentation_UserManuals", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Documentation_UserManuals_Rigs_RigId",
                        column: x => x.RigId,
                        principalTable: "Rigs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Ricon_RootEquipments",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RigId = table.Column<long>(nullable: false),
                    ReferenceId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ricon_RootEquipments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Ricon_RootEquipments_Rigs_RigId",
                        column: x => x.RigId,
                        principalTable: "Rigs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Ricon_Joints",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RootEquipmentId = table.Column<long>(nullable: false),
                    ReferenceId = table.Column<long>(nullable: false),
                    TagNumber = table.Column<string>(maxLength: 30, nullable: true),
                    Fatigue = table.Column<string>(maxLength: 40, nullable: true),
                    Coc = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true),
                    LastVisualInspection = table.Column<DateTime>(nullable: true),
                    LastWallThicknessInspection = table.Column<DateTime>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    Modified = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ricon_Joints", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Ricon_Joints_Ricon_RootEquipments_RootEquipmentId",
                        column: x => x.RootEquipmentId,
                        principalTable: "Ricon_RootEquipments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Ricon_Documents",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Description = table.Column<string>(maxLength: 40, nullable: true),
                    RootEquipmentId = table.Column<long>(nullable: true),
                    JointReferenceId = table.Column<long>(nullable: false),
                    JointId = table.Column<long>(nullable: true),
                    ReferenceId = table.Column<long>(nullable: false),
                    Version = table.Column<string>(maxLength: 2, nullable: true, defaultValue: "00"),
                    Url = table.Column<string>(maxLength: 50, nullable: true),
                    PublishedDate = table.Column<DateTime>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    Modified = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ricon_Documents", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Ricon_Documents_Ricon_Joints_JointId",
                        column: x => x.JointId,
                        principalTable: "Ricon_Joints",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ricon_Documents_Ricon_RootEquipments_RootEquipmentId",
                        column: x => x.RootEquipmentId,
                        principalTable: "Ricon_RootEquipments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RootCustomers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 255, nullable: false),
                    Subdomain = table.Column<string>(maxLength: 255, nullable: true),
                    IsInternal = table.Column<bool>(nullable: false),
                    DefaultCustomerId = table.Column<long>(nullable: true),
                    CreatedById = table.Column<long>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    ModifiedById = table.Column<long>(nullable: true),
                    Modified = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RootCustomers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReferenceId = table.Column<string>(maxLength: 50, nullable: true),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Address_City = table.Column<string>(nullable: true),
                    Address_PostalCode = table.Column<string>(nullable: true),
                    RootCustomerId = table.Column<long>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    Modified = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customers_RootCustomers_RootCustomerId",
                        column: x => x.RootCustomerId,
                        principalTable: "RootCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Permission_Profiles",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 300, nullable: false),
                    RootCustomerId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permission_Profiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Permission_Profiles_RootCustomers_RootCustomerId",
                        column: x => x.RootCustomerId,
                        principalTable: "RootCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Upn = table.Column<string>(maxLength: 255, nullable: false),
                    Email = table.Column<string>(nullable: true),
                    IsActive = table.Column<bool>(nullable: false),
                    IsInternal = table.Column<bool>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 255, nullable: true),
                    LastName = table.Column<string>(maxLength: 255, nullable: true),
                    DisplayName = table.Column<string>(maxLength: 255, nullable: true),
                    Position = table.Column<string>(maxLength: 255, nullable: true),
                    PhoneNumber = table.Column<string>(maxLength: 255, nullable: true),
                    MobileNumber = table.Column<string>(maxLength: 255, nullable: true),
                    FaxNumber = table.Column<string>(maxLength: 255, nullable: true),
                    Country = table.Column<string>(maxLength: 255, nullable: true),
                    CountryCode = table.Column<string>(maxLength: 2, nullable: true),
                    Image = table.Column<string>(maxLength: 255, nullable: true),
                    LastLogIn = table.Column<DateTime>(nullable: true),
                    AcceptedTermsAndConditions = table.Column<DateTime>(nullable: true),
                    RootCustomerId = table.Column<long>(nullable: true),
                    CreatedById = table.Column<long>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    ModifiedById = table.Column<long>(nullable: true),
                    Modified = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_Users_ModifiedById",
                        column: x => x.ModifiedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Users_RootCustomers_RootCustomerId",
                        column: x => x.RootCustomerId,
                        principalTable: "RootCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Permission_ProfileRoles",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Role = table.Column<int>(nullable: false),
                    ProfileId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permission_ProfileRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Permission_ProfileRoles_Permission_Profiles_ProfileId",
                        column: x => x.ProfileId,
                        principalTable: "Permission_Profiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AsyncOperations",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<long>(nullable: false),
                    Response = table.Column<string>(maxLength: 500, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false),
                    UpdatedAt = table.Column<DateTime>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AsyncOperations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AsyncOperations_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Enquiry_AddedUsers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EnquiryId = table.Column<long>(nullable: false),
                    UserId = table.Column<long>(nullable: false),
                    CreatedById = table.Column<long>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enquiry_AddedUsers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Enquiry_AddedUsers_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Enquiry_AddedUsers_Enquiry_Enquiries_EnquiryId",
                        column: x => x.EnquiryId,
                        principalTable: "Enquiry_Enquiries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Enquiry_AddedUsers_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Enquiry_Comments",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReferenceId = table.Column<string>(maxLength: 50, nullable: true),
                    Title = table.Column<string>(maxLength: 50, nullable: true),
                    Text = table.Column<string>(maxLength: 8000, nullable: true),
                    IsInitial = table.Column<bool>(nullable: false),
                    CreatedById = table.Column<long>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    EnquiryId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enquiry_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Enquiry_Comments_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Enquiry_Comments_Enquiry_Enquiries_EnquiryId",
                        column: x => x.EnquiryId,
                        principalTable: "Enquiry_Enquiries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Enquiry_ReadReceipts",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Created = table.Column<DateTime>(nullable: false),
                    EnquiryId = table.Column<long>(nullable: false),
                    UserId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enquiry_ReadReceipts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Enquiry_ReadReceipts_Enquiry_Enquiries_EnquiryId",
                        column: x => x.EnquiryId,
                        principalTable: "Enquiry_Enquiries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Enquiry_ReadReceipts_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Enquiry_StateLogs",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EnquiryId = table.Column<long>(nullable: false),
                    State = table.Column<int>(nullable: false),
                    ChangedById = table.Column<long>(nullable: false),
                    Changed = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enquiry_StateLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Enquiry_StateLogs_Users_ChangedById",
                        column: x => x.ChangedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Enquiry_StateLogs_Enquiry_Enquiries_EnquiryId",
                        column: x => x.EnquiryId,
                        principalTable: "Enquiry_Enquiries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Permission_UserProfiles",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProfileId = table.Column<long>(nullable: false),
                    UserId = table.Column<long>(nullable: false),
                    RigId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permission_UserProfiles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Permission_UserProfiles_Permission_Profiles_ProfileId",
                        column: x => x.ProfileId,
                        principalTable: "Permission_Profiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Permission_UserProfiles_Rigs_RigId",
                        column: x => x.RigId,
                        principalTable: "Rigs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Permission_UserProfiles_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Permission_UserRoles",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Role = table.Column<int>(nullable: false),
                    UserId = table.Column<long>(nullable: false),
                    RigId = table.Column<long>(nullable: true),
                    RootCustomerId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Permission_UserRoles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Permission_UserRoles_Rigs_RigId",
                        column: x => x.RigId,
                        principalTable: "Rigs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Permission_UserRoles_RootCustomers_RootCustomerId",
                        column: x => x.RootCustomerId,
                        principalTable: "RootCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Permission_UserRoles_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Subscription_EnquiryTypeFilters",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<long>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscription_EnquiryTypeFilters", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Subscription_EnquiryTypeFilters_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Subscription_RigFilters",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<long>(nullable: false),
                    RigId = table.Column<long>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscription_RigFilters", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Subscription_RigFilters_Rigs_RigId",
                        column: x => x.RigId,
                        principalTable: "Rigs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Subscription_RigFilters_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Subscription_UserSettings",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<long>(nullable: false),
                    SubscriptionType = table.Column<int>(nullable: false),
                    Created = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subscription_UserSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Subscription_UserSettings_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UpgradeMatrix_CustomerSettings",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RootCustomerId = table.Column<long>(nullable: false),
                    Key = table.Column<string>(maxLength: 100, nullable: false),
                    Value = table.Column<string>(nullable: true),
                    CreatedById = table.Column<long>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    ModifiedById = table.Column<long>(nullable: true),
                    Modified = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpgradeMatrix_CustomerSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_CustomerSettings_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_CustomerSettings_Users_ModifiedById",
                        column: x => x.ModifiedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_CustomerSettings_RootCustomers_RootCustomerId",
                        column: x => x.RootCustomerId,
                        principalTable: "RootCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UpgradeMatrix_EquipmentCategory",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ShortName = table.Column<string>(maxLength: 20, nullable: false),
                    CreatedById = table.Column<long>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    ModifiedById = table.Column<long>(nullable: true),
                    Modified = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpgradeMatrix_EquipmentCategory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_EquipmentCategory_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_EquipmentCategory_Users_ModifiedById",
                        column: x => x.ModifiedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UpgradeMatrix_RigSettings",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RigId = table.Column<long>(nullable: false),
                    ShortName = table.Column<string>(maxLength: 20, nullable: false),
                    RigType = table.Column<int>(nullable: false),
                    CreatedById = table.Column<long>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    ModifiedById = table.Column<long>(nullable: true),
                    Modified = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpgradeMatrix_RigSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_RigSettings_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_RigSettings_Users_ModifiedById",
                        column: x => x.ModifiedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_RigSettings_Rigs_RigId",
                        column: x => x.RigId,
                        principalTable: "Rigs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UserSettings",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Key = table.Column<string>(maxLength: 500, nullable: false),
                    Value = table.Column<string>(maxLength: 500, nullable: false),
                    UserId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSettings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserSettings_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Enquiry_Attachments",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ReferenceId = table.Column<string>(maxLength: 50, nullable: true),
                    FileName = table.Column<string>(maxLength: 255, nullable: true),
                    FileExtension = table.Column<string>(maxLength: 10, nullable: true),
                    BlobPath = table.Column<string>(maxLength: 500, nullable: true),
                    EnquiryId = table.Column<long>(nullable: false),
                    CommentId = table.Column<long>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    CreatedById = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Enquiry_Attachments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Enquiry_Attachments_Enquiry_Comments_CommentId",
                        column: x => x.CommentId,
                        principalTable: "Enquiry_Comments",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Enquiry_Attachments_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Enquiry_Attachments_Enquiry_Enquiries_EnquiryId",
                        column: x => x.EnquiryId,
                        principalTable: "Enquiry_Enquiries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UpgradeMatrix_CcnItem",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RootCustomerId = table.Column<long>(nullable: false),
                    EquipmentCategoryId = table.Column<long>(nullable: false),
                    Description = table.Column<string>(maxLength: 5000, nullable: false),
                    Comments = table.Column<string>(maxLength: 5000, nullable: true),
                    PackageUpgrade = table.Column<int>(nullable: false),
                    RigType = table.Column<int>(nullable: false),
                    CreatedById = table.Column<long>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    ModifiedById = table.Column<long>(nullable: true),
                    Modified = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpgradeMatrix_CcnItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_CcnItem_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_CcnItem_UpgradeMatrix_EquipmentCategory_EquipmentCategoryId",
                        column: x => x.EquipmentCategoryId,
                        principalTable: "UpgradeMatrix_EquipmentCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_CcnItem_Users_ModifiedById",
                        column: x => x.ModifiedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_CcnItem_RootCustomers_RootCustomerId",
                        column: x => x.RootCustomerId,
                        principalTable: "RootCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UpgradeMatrix_QuotationItem",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RootCustomerId = table.Column<long>(nullable: false),
                    EquipmentCategoryId = table.Column<long>(nullable: false),
                    Description = table.Column<string>(maxLength: 5000, nullable: false),
                    Heading = table.Column<string>(maxLength: 1000, nullable: true),
                    Benefits = table.Column<string>(maxLength: 1000, nullable: true),
                    CreatedById = table.Column<long>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    ModifiedById = table.Column<long>(nullable: true),
                    Modified = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpgradeMatrix_QuotationItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_QuotationItem_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_QuotationItem_UpgradeMatrix_EquipmentCategory_EquipmentCategoryId",
                        column: x => x.EquipmentCategoryId,
                        principalTable: "UpgradeMatrix_EquipmentCategory",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_QuotationItem_Users_ModifiedById",
                        column: x => x.ModifiedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_QuotationItem_RootCustomers_RootCustomerId",
                        column: x => x.RootCustomerId,
                        principalTable: "RootCustomers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UpgradeMatrix_CcnState",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CcnItemId = table.Column<long>(nullable: false),
                    RigSettingsId = table.Column<long>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    CcnName = table.Column<string>(maxLength: 20, nullable: true),
                    DocumentPath = table.Column<string>(nullable: true),
                    CreatedById = table.Column<long>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    ModifiedById = table.Column<long>(nullable: true),
                    Modified = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpgradeMatrix_CcnState", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_CcnState_UpgradeMatrix_CcnItem_CcnItemId",
                        column: x => x.CcnItemId,
                        principalTable: "UpgradeMatrix_CcnItem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_CcnState_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_CcnState_Users_ModifiedById",
                        column: x => x.ModifiedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_CcnState_UpgradeMatrix_RigSettings_RigSettingsId",
                        column: x => x.RigSettingsId,
                        principalTable: "UpgradeMatrix_RigSettings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "UpgradeMatrix_QuotationState",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    QuotationItemId = table.Column<long>(nullable: false),
                    RigSettingsId = table.Column<long>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    CreatedById = table.Column<long>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()"),
                    ModifiedById = table.Column<long>(nullable: true),
                    Modified = table.Column<DateTime>(nullable: false, defaultValueSql: "getutcdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UpgradeMatrix_QuotationState", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_QuotationState_Users_CreatedById",
                        column: x => x.CreatedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_QuotationState_Users_ModifiedById",
                        column: x => x.ModifiedById,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_QuotationState_UpgradeMatrix_QuotationItem_QuotationItemId",
                        column: x => x.QuotationItemId,
                        principalTable: "UpgradeMatrix_QuotationItem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UpgradeMatrix_QuotationState_UpgradeMatrix_RigSettings_RigSettingsId",
                        column: x => x.RigSettingsId,
                        principalTable: "UpgradeMatrix_RigSettings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AsyncOperations_UserId",
                table: "AsyncOperations",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Bulletin_ZaBulletins_ReferenceId",
                table: "Bulletin_ZaBulletins",
                column: "ReferenceId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Bulletin_ZaBulletins_TypeId",
                table: "Bulletin_ZaBulletins",
                column: "TypeId");

            migrationBuilder.CreateIndex(
                name: "IX_Bulletin_ZbBulletins_ConfirmedByUserId",
                table: "Bulletin_ZbBulletins",
                column: "ConfirmedByUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Bulletin_ZbBulletins_CustomerId",
                table: "Bulletin_ZbBulletins",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Bulletin_ZbBulletins_EquipmentId",
                table: "Bulletin_ZbBulletins",
                column: "EquipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Bulletin_ZbBulletins_RigId",
                table: "Bulletin_ZbBulletins",
                column: "RigId");

            migrationBuilder.CreateIndex(
                name: "IX_Bulletin_ZbBulletins_ZaBulletinId",
                table: "Bulletin_ZbBulletins",
                column: "ZaBulletinId");

            migrationBuilder.CreateIndex(
                name: "IX_Bulletin_ZbBulletins_ReferenceId_Group",
                table: "Bulletin_ZbBulletins",
                columns: new[] { "ReferenceId", "Group" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Customers_ReferenceId",
                table: "Customers",
                column: "ReferenceId",
                unique: true,
                filter: "[ReferenceId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Customers_RootCustomerId",
                table: "Customers",
                column: "RootCustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Documentation_TechnicalReports_DocumentDate",
                table: "Documentation_TechnicalReports",
                column: "DocumentDate");

            migrationBuilder.CreateIndex(
                name: "IX_Documentation_TechnicalReports_RigId",
                table: "Documentation_TechnicalReports",
                column: "RigId");

            migrationBuilder.CreateIndex(
                name: "IX_Documentation_UserManuals_DocumentDate",
                table: "Documentation_UserManuals",
                column: "DocumentDate");

            migrationBuilder.CreateIndex(
                name: "IX_Documentation_UserManuals_RigId_DisciplineCode_IsMaster",
                table: "Documentation_UserManuals",
                columns: new[] { "RigId", "DisciplineCode", "IsMaster" })
                .Annotation("SqlServer:Include", new[] { "Discipline" });

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_AddedUsers_CreatedById",
                table: "Enquiry_AddedUsers",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_AddedUsers_UserId",
                table: "Enquiry_AddedUsers",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_AddedUsers_EnquiryId_UserId",
                table: "Enquiry_AddedUsers",
                columns: new[] { "EnquiryId", "UserId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Attachments_CommentId",
                table: "Enquiry_Attachments",
                column: "CommentId");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Attachments_CreatedById",
                table: "Enquiry_Attachments",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Attachments_EnquiryId",
                table: "Enquiry_Attachments",
                column: "EnquiryId");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Attachments_ReferenceId",
                table: "Enquiry_Attachments",
                column: "ReferenceId",
                unique: true,
                filter: "[ReferenceId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Comments_CreatedById",
                table: "Enquiry_Comments",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Comments_EnquiryId",
                table: "Enquiry_Comments",
                column: "EnquiryId");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Comments_ReferenceId",
                table: "Enquiry_Comments",
                column: "ReferenceId",
                unique: true,
                filter: "[ReferenceId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Enquiries_ApprovedById",
                table: "Enquiry_Enquiries",
                column: "ApprovedById");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Enquiries_AssignedById",
                table: "Enquiry_Enquiries",
                column: "AssignedById");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Enquiries_AssignedToCustomerId",
                table: "Enquiry_Enquiries",
                column: "AssignedToCustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Enquiries_AssignedToId",
                table: "Enquiry_Enquiries",
                column: "AssignedToId");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Enquiries_CreatedById",
                table: "Enquiry_Enquiries",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Enquiries_CustomerId",
                table: "Enquiry_Enquiries",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Enquiries_EquipmentId",
                table: "Enquiry_Enquiries",
                column: "EquipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Enquiries_ModifiedById",
                table: "Enquiry_Enquiries",
                column: "ModifiedById");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Enquiries_OldGlobalId",
                table: "Enquiry_Enquiries",
                column: "OldGlobalId");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Enquiries_ReferenceId",
                table: "Enquiry_Enquiries",
                column: "ReferenceId",
                unique: true,
                filter: "[ReferenceId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Enquiries_RejectedById",
                table: "Enquiry_Enquiries",
                column: "RejectedById");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Enquiries_RigId",
                table: "Enquiry_Enquiries",
                column: "RigId");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Enquiries_RigOwnerId",
                table: "Enquiry_Enquiries",
                column: "RigOwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Enquiries_RootCustomerId",
                table: "Enquiry_Enquiries",
                column: "RootCustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_Enquiries_SupplierId",
                table: "Enquiry_Enquiries",
                column: "SupplierId");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_ReadReceipts_UserId",
                table: "Enquiry_ReadReceipts",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_ReadReceipts_EnquiryId_UserId",
                table: "Enquiry_ReadReceipts",
                columns: new[] { "EnquiryId", "UserId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_StateLogs_ChangedById",
                table: "Enquiry_StateLogs",
                column: "ChangedById");

            migrationBuilder.CreateIndex(
                name: "IX_Enquiry_StateLogs_EnquiryId",
                table: "Enquiry_StateLogs",
                column: "EnquiryId");

            migrationBuilder.CreateIndex(
                name: "IX_Equipments_ParentId",
                table: "Equipments",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Equipments_ReferenceId",
                table: "Equipments",
                column: "ReferenceId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Equipments_RigId",
                table: "Equipments",
                column: "RigId");

            migrationBuilder.CreateIndex(
                name: "IX_Equipments_RigOwnerId",
                table: "Equipments",
                column: "RigOwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_FunctionalLocationLogs_CustomerId",
                table: "FunctionalLocationLogs",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_FunctionalLocationLogs_EquipmentId",
                table: "FunctionalLocationLogs",
                column: "EquipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_FunctionalLocationLogs_RigId",
                table: "FunctionalLocationLogs",
                column: "RigId");

            migrationBuilder.CreateIndex(
                name: "IX_Permission_ProfileRoles_ProfileId_Role",
                table: "Permission_ProfileRoles",
                columns: new[] { "ProfileId", "Role" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Permission_Profiles_RootCustomerId",
                table: "Permission_Profiles",
                column: "RootCustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Permission_UserProfiles_ProfileId",
                table: "Permission_UserProfiles",
                column: "ProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_Permission_UserProfiles_RigId",
                table: "Permission_UserProfiles",
                column: "RigId");

            migrationBuilder.CreateIndex(
                name: "IX_Permission_UserProfiles_UserId",
                table: "Permission_UserProfiles",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Permission_UserRoles_RigId",
                table: "Permission_UserRoles",
                column: "RigId");

            migrationBuilder.CreateIndex(
                name: "IX_Permission_UserRoles_RootCustomerId",
                table: "Permission_UserRoles",
                column: "RootCustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Permission_UserRoles_UserId",
                table: "Permission_UserRoles",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Ricon_Documents_JointId",
                table: "Ricon_Documents",
                column: "JointId");

            migrationBuilder.CreateIndex(
                name: "IX_Ricon_Documents_RootEquipmentId",
                table: "Ricon_Documents",
                column: "RootEquipmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Ricon_Joints_RootEquipmentId_ReferenceId",
                table: "Ricon_Joints",
                columns: new[] { "RootEquipmentId", "ReferenceId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Ricon_RootEquipments_RigId_ReferenceId",
                table: "Ricon_RootEquipments",
                columns: new[] { "RigId", "ReferenceId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Rigs_OwnerId",
                table: "Rigs",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Rigs_ReferenceId",
                table: "Rigs",
                column: "ReferenceId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Rigs_ResponsibleUserId",
                table: "Rigs",
                column: "ResponsibleUserId");

            migrationBuilder.CreateIndex(
                name: "IX_Rigs_RootOperatorId",
                table: "Rigs",
                column: "RootOperatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Rigs_RootOwnerId",
                table: "Rigs",
                column: "RootOwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_RootCustomers_CreatedById",
                table: "RootCustomers",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_RootCustomers_DefaultCustomerId",
                table: "RootCustomers",
                column: "DefaultCustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_RootCustomers_ModifiedById",
                table: "RootCustomers",
                column: "ModifiedById");

            migrationBuilder.CreateIndex(
                name: "IX_RootCustomers_Name",
                table: "RootCustomers",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Subscription_EnquiryTypeFilters_UserId_Type",
                table: "Subscription_EnquiryTypeFilters",
                columns: new[] { "UserId", "Type" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Subscription_RigFilters_RigId",
                table: "Subscription_RigFilters",
                column: "RigId");

            migrationBuilder.CreateIndex(
                name: "IX_Subscription_RigFilters_UserId_RigId",
                table: "Subscription_RigFilters",
                columns: new[] { "UserId", "RigId" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Subscription_UserSettings_UserId_SubscriptionType",
                table: "Subscription_UserSettings",
                columns: new[] { "UserId", "SubscriptionType" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_CcnItem_CreatedById",
                table: "UpgradeMatrix_CcnItem",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_CcnItem_EquipmentCategoryId",
                table: "UpgradeMatrix_CcnItem",
                column: "EquipmentCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_CcnItem_ModifiedById",
                table: "UpgradeMatrix_CcnItem",
                column: "ModifiedById");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_CcnItem_RootCustomerId",
                table: "UpgradeMatrix_CcnItem",
                column: "RootCustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_CcnState_CcnItemId",
                table: "UpgradeMatrix_CcnState",
                column: "CcnItemId");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_CcnState_CreatedById",
                table: "UpgradeMatrix_CcnState",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_CcnState_ModifiedById",
                table: "UpgradeMatrix_CcnState",
                column: "ModifiedById");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_CcnState_RigSettingsId",
                table: "UpgradeMatrix_CcnState",
                column: "RigSettingsId");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_CustomerSettings_CreatedById",
                table: "UpgradeMatrix_CustomerSettings",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_CustomerSettings_ModifiedById",
                table: "UpgradeMatrix_CustomerSettings",
                column: "ModifiedById");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_CustomerSettings_RootCustomerId_Key",
                table: "UpgradeMatrix_CustomerSettings",
                columns: new[] { "RootCustomerId", "Key" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_EquipmentCategory_CreatedById",
                table: "UpgradeMatrix_EquipmentCategory",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_EquipmentCategory_ModifiedById",
                table: "UpgradeMatrix_EquipmentCategory",
                column: "ModifiedById");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_QuotationItem_CreatedById",
                table: "UpgradeMatrix_QuotationItem",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_QuotationItem_EquipmentCategoryId",
                table: "UpgradeMatrix_QuotationItem",
                column: "EquipmentCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_QuotationItem_ModifiedById",
                table: "UpgradeMatrix_QuotationItem",
                column: "ModifiedById");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_QuotationItem_RootCustomerId",
                table: "UpgradeMatrix_QuotationItem",
                column: "RootCustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_QuotationState_CreatedById",
                table: "UpgradeMatrix_QuotationState",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_QuotationState_ModifiedById",
                table: "UpgradeMatrix_QuotationState",
                column: "ModifiedById");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_QuotationState_QuotationItemId",
                table: "UpgradeMatrix_QuotationState",
                column: "QuotationItemId");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_QuotationState_RigSettingsId",
                table: "UpgradeMatrix_QuotationState",
                column: "RigSettingsId");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_RigSettings_CreatedById",
                table: "UpgradeMatrix_RigSettings",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_RigSettings_ModifiedById",
                table: "UpgradeMatrix_RigSettings",
                column: "ModifiedById");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_RigSettings_RigId",
                table: "UpgradeMatrix_RigSettings",
                column: "RigId");

            migrationBuilder.CreateIndex(
                name: "IX_UpgradeMatrix_RigSettings_ShortName",
                table: "UpgradeMatrix_RigSettings",
                column: "ShortName",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Users_CreatedById",
                table: "Users",
                column: "CreatedById");

            migrationBuilder.CreateIndex(
                name: "IX_Users_ModifiedById",
                table: "Users",
                column: "ModifiedById");

            migrationBuilder.CreateIndex(
                name: "IX_Users_RootCustomerId",
                table: "Users",
                column: "RootCustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_Upn",
                table: "Users",
                column: "Upn",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserSettings_UserId_Key",
                table: "UserSettings",
                columns: new[] { "UserId", "Key" },
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Bulletin_ZbBulletins_Users_ConfirmedByUserId",
                table: "Bulletin_ZbBulletins",
                column: "ConfirmedByUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Bulletin_ZbBulletins_Customers_CustomerId",
                table: "Bulletin_ZbBulletins",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Bulletin_ZbBulletins_Equipments_EquipmentId",
                table: "Bulletin_ZbBulletins",
                column: "EquipmentId",
                principalTable: "Equipments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Bulletin_ZbBulletins_Rigs_RigId",
                table: "Bulletin_ZbBulletins",
                column: "RigId",
                principalTable: "Rigs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Enquiries_Users_ApprovedById",
                table: "Enquiry_Enquiries",
                column: "ApprovedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Enquiries_Users_AssignedById",
                table: "Enquiry_Enquiries",
                column: "AssignedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Enquiries_Users_AssignedToId",
                table: "Enquiry_Enquiries",
                column: "AssignedToId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Enquiries_Users_CreatedById",
                table: "Enquiry_Enquiries",
                column: "CreatedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Enquiries_Users_ModifiedById",
                table: "Enquiry_Enquiries",
                column: "ModifiedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Enquiries_Users_RejectedById",
                table: "Enquiry_Enquiries",
                column: "RejectedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Enquiries_Customers_CustomerId",
                table: "Enquiry_Enquiries",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Enquiries_Customers_RigOwnerId",
                table: "Enquiry_Enquiries",
                column: "RigOwnerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Enquiries_Equipments_EquipmentId",
                table: "Enquiry_Enquiries",
                column: "EquipmentId",
                principalTable: "Equipments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Enquiries_Rigs_RigId",
                table: "Enquiry_Enquiries",
                column: "RigId",
                principalTable: "Rigs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Enquiries_RootCustomers_AssignedToCustomerId",
                table: "Enquiry_Enquiries",
                column: "AssignedToCustomerId",
                principalTable: "RootCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Enquiries_RootCustomers_RootCustomerId",
                table: "Enquiry_Enquiries",
                column: "RootCustomerId",
                principalTable: "RootCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Enquiry_Enquiries_RootCustomers_SupplierId",
                table: "Enquiry_Enquiries",
                column: "SupplierId",
                principalTable: "RootCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Equipments_Customers_RigOwnerId",
                table: "Equipments",
                column: "RigOwnerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Equipments_Rigs_RigId",
                table: "Equipments",
                column: "RigId",
                principalTable: "Rigs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FunctionalLocationLogs_Customers_CustomerId",
                table: "FunctionalLocationLogs",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_FunctionalLocationLogs_Rigs_RigId",
                table: "FunctionalLocationLogs",
                column: "RigId",
                principalTable: "Rigs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rigs_Users_ResponsibleUserId",
                table: "Rigs",
                column: "ResponsibleUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rigs_Customers_OwnerId",
                table: "Rigs",
                column: "OwnerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rigs_RootCustomers_RootOperatorId",
                table: "Rigs",
                column: "RootOperatorId",
                principalTable: "RootCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Rigs_RootCustomers_RootOwnerId",
                table: "Rigs",
                column: "RootOwnerId",
                principalTable: "RootCustomers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RootCustomers_Users_CreatedById",
                table: "RootCustomers",
                column: "CreatedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RootCustomers_Users_ModifiedById",
                table: "RootCustomers",
                column: "ModifiedById",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RootCustomers_Customers_DefaultCustomerId",
                table: "RootCustomers",
                column: "DefaultCustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RootCustomers_Users_CreatedById",
                table: "RootCustomers");

            migrationBuilder.DropForeignKey(
                name: "FK_RootCustomers_Users_ModifiedById",
                table: "RootCustomers");

            migrationBuilder.DropForeignKey(
                name: "FK_RootCustomers_Customers_DefaultCustomerId",
                table: "RootCustomers");

            migrationBuilder.DropTable(
                name: "AsyncOperations");

            migrationBuilder.DropTable(
                name: "Bulletin_ZbBulletins");

            migrationBuilder.DropTable(
                name: "Documentation_TechnicalReports");

            migrationBuilder.DropTable(
                name: "Documentation_UserManuals");

            migrationBuilder.DropTable(
                name: "Enquiry_AddedUsers");

            migrationBuilder.DropTable(
                name: "Enquiry_Attachments");

            migrationBuilder.DropTable(
                name: "Enquiry_MessageTemplates");

            migrationBuilder.DropTable(
                name: "Enquiry_ReadReceipts");

            migrationBuilder.DropTable(
                name: "Enquiry_StateLogs");

            migrationBuilder.DropTable(
                name: "FunctionalLocationLogs");

            migrationBuilder.DropTable(
                name: "Permission_ProfileRoles");

            migrationBuilder.DropTable(
                name: "Permission_UserProfiles");

            migrationBuilder.DropTable(
                name: "Permission_UserRoles");

            migrationBuilder.DropTable(
                name: "Ricon_Documents");

            migrationBuilder.DropTable(
                name: "Sparepart_Spareparts");

            migrationBuilder.DropTable(
                name: "Subscription_EnquiryTypeFilters");

            migrationBuilder.DropTable(
                name: "Subscription_RigFilters");

            migrationBuilder.DropTable(
                name: "Subscription_StaticSubscribers");

            migrationBuilder.DropTable(
                name: "Subscription_UserSettings");

            migrationBuilder.DropTable(
                name: "UpgradeMatrix_CcnState");

            migrationBuilder.DropTable(
                name: "UpgradeMatrix_CustomerSettings");

            migrationBuilder.DropTable(
                name: "UpgradeMatrix_MessageTemplates");

            migrationBuilder.DropTable(
                name: "UpgradeMatrix_QuotationState");

            migrationBuilder.DropTable(
                name: "UserSettings");

            migrationBuilder.DropTable(
                name: "Bulletin_ZaBulletins");

            migrationBuilder.DropTable(
                name: "Enquiry_Comments");

            migrationBuilder.DropTable(
                name: "Permission_Profiles");

            migrationBuilder.DropTable(
                name: "Ricon_Joints");

            migrationBuilder.DropTable(
                name: "UpgradeMatrix_CcnItem");

            migrationBuilder.DropTable(
                name: "UpgradeMatrix_QuotationItem");

            migrationBuilder.DropTable(
                name: "UpgradeMatrix_RigSettings");

            migrationBuilder.DropTable(
                name: "Bulletin_BulletinTypes");

            migrationBuilder.DropTable(
                name: "Enquiry_Enquiries");

            migrationBuilder.DropTable(
                name: "Ricon_RootEquipments");

            migrationBuilder.DropTable(
                name: "UpgradeMatrix_EquipmentCategory");

            migrationBuilder.DropTable(
                name: "Equipments");

            migrationBuilder.DropTable(
                name: "Rigs");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "RootCustomers");
        }
    }
}
