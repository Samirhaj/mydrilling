﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MyDrilling.Infrastructure.Migrations
{
    public partial class RiconDocumentJointReferenceIdNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "JointReferenceId",
                table: "Ricon_Documents",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "JointReferenceId",
                table: "Ricon_Documents",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);
        }
    }
}
