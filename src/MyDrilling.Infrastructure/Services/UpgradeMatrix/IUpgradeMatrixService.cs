﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyDrilling.Core;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Rig;
using MyDrilling.Core.Entities.UpgradeMatrix;

namespace MyDrilling.Infrastructure.Services.UpgradeMatrix
{
    public interface IUpgradeMatrixService
    {
        Task<List<RootCustomer>> GetCustomersAsync(CombinedUserRoles roles);
        Task<List<RigSettings>> GetRigSettingsForCustomerAsync(long customerId, CombinedUserRoles roles);
        Task<List<CustomerSettings>> GetCustomerSettingsWithStatusAsync(CombinedUserRoles roles);
        Task<CustomerSettings> SaveCustomerSettingsAsync(CustomerSettings entity, CombinedUserRoles roles, User user);
        Task<CustomerSettings> GetCustomerSettingsAsync(long id, CombinedUserRoles roles);
        Task<List<Rig>> GetRigsForCustomerAsync(long customerId, CombinedUserRoles roles);
        Task<RigSettings> SaveRigSettings(RigSettings entity, CombinedUserRoles roles, User user);
        Task<bool> DeleteRigSettings(long id, CombinedUserRoles roles); 
        Task<List<EquipmentCategory>> GetEquipmentCategoriesAsync(CombinedUserRoles roles);
        Task<EquipmentCategory> GetEquipmentCategoryAsync(long id, CombinedUserRoles roles);
        Task<EquipmentCategory> GetEquipmentCategoryByNameAsync(string shortName, CombinedUserRoles roles);
        Task<EquipmentCategory> SaveEquipmentCategoryAsync(EquipmentCategory entity, CombinedUserRoles roles, User user);
        Task<bool> DeleteEquipmentCategory(long id, CombinedUserRoles roles);
        Task<List<CcnItem>> GetCcnItemsAsync(long customerId, CombinedUserRoles roles);
        Task<CcnItem> SaveCcnItemAsync(CcnItem entity, CombinedUserRoles roles, User user);
        Task<bool> DeleteCcnItemAsync(long id, CombinedUserRoles roles);
        Task<CcnState> GetCcnStateAsync(long id, CombinedUserRoles roles);
        Task<CcnState> SaveCcnStateAsync(CcnState entity, CombinedUserRoles roles, User user);
        Task<CcnState> ApproveCcnStateAsync(long id, CombinedUserRoles roles, User user);
        Dictionary<long, List<CcnState>> GetCcnStates(IEnumerable<long> ccnItemIds, CombinedUserRoles roles);
        Task<List<QuotationItem>> GetQuotationItemsAsync(long customerId, CombinedUserRoles roles);
        Task<QuotationItem> SaveQuotationItemAsync(QuotationItem entity, CombinedUserRoles roles, User user);
        Task<bool> DeleteQuotationItemAsync(long id, CombinedUserRoles roles);
        Dictionary<long, List<QuotationState>> GetQuotationStates(IEnumerable<long> quotationItemIds, CombinedUserRoles roles);
        Task<QuotationState> SaveQuotationStateAsync(QuotationState entity, CombinedUserRoles roles, User user);
    }
}
