﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Azure.Storage.Sas;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Rig;
using MyDrilling.Core.Entities.UpgradeMatrix;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.Storage;
using MyDrilling.Infrastructure.Storage.Blob;

namespace MyDrilling.Infrastructure.Services.UpgradeMatrix
{
    public class UpgradeMatrixService : IUpgradeMatrixService
    {
        private readonly MyDrillingDb _db;
        private readonly IBlobSasGenerator _blobSasGenerator;
        public UpgradeMatrixService(MyDrillingDb db, 
            IBlobSasGenerator blobSasGenerator)
        {
            _db = db;
            _blobSasGenerator = blobSasGenerator;
        }

        public async Task<List<RootCustomer>> GetCustomersAsync(CombinedUserRoles roles)
        {
            if(! roles.HasUpgradeMatrixAccess()) return new List<RootCustomer>();
            return await _db.RootCustomers
                .ApplyPermissionsForRootCustomer(roles)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<CustomerSettings> GetCustomerSettingsAsync(long id, CombinedUserRoles roles)
        {
            if (!roles.HasUpgradeMatrixAccess()) return null;
            return await _db.UpgradeMatrix_CustomerSettings
                .AsNoTracking()
                .ApplyPermissionsForCustomerSettings(roles)
                .FirstOrDefaultAsync(c => c.Id == id);
        }

        public async Task<List<Rig>> GetRigsForCustomerAsync(long customerId, CombinedUserRoles roles)
        {
            if (!roles.HasUpgradeMatrixAccess()) return new List<Rig>();
            return await _db.Rigs
                .AsNoTracking()
                .ApplyPermissionsForRig(roles)
                .Include(r => r.Owner)
                .Where(r => (r.Owner != null && r.Owner.RootCustomerId == customerId) || r.RootOwnerId == customerId)
                .ToListAsync();
        }

        public async Task<List<CustomerSettings>> GetCustomerSettingsWithStatusAsync(CombinedUserRoles roles)
        {
            if (!roles.HasUpgradeMatrixAccess()) return new List<CustomerSettings>();
            return await _db.UpgradeMatrix_CustomerSettings
                .AsNoTracking()
                .ApplyPermissionsForCustomerSettings(roles)
                .Include(cs => cs.RootCustomer)
                .Where(cs => cs.Key == "Status" && cs.RootCustomerId > 0 && cs.RootCustomer != null)
                .ToListAsync();
        }

        public async Task<CustomerSettings> SaveCustomerSettingsAsync(CustomerSettings entity, CombinedUserRoles roles, User user)
        {
            if (entity == null || !roles.IsUpgradeMatrixAdmin() || user == null) return null;
            CustomerSettings dbEntity = null;
            if (entity.Id > 0)
            {
                //update
                dbEntity = await _db.UpgradeMatrix_CustomerSettings.FirstOrDefaultAsync(c => c.Id == entity.Id);
                if (dbEntity == null) return null;
                dbEntity.RootCustomerId = entity.RootCustomerId;
                dbEntity.Key = entity.Key;
                dbEntity.Value = entity.Value;
                dbEntity.Modified = DateTime.UtcNow;
                dbEntity.ModifiedById = user.Id;
            }
            else
            {
                //insert
                dbEntity = new CustomerSettings
                {
                    RootCustomerId = entity.RootCustomerId,
                    Key = entity.Key,
                    Value = entity.Value,
                    CreatedById = user.Id,
                    Created = DateTime.UtcNow
            };
                _db.UpgradeMatrix_CustomerSettings.Add(dbEntity);
            }

            await _db.SaveChangesAsync();
            return dbEntity;
        }

        public async Task<List<RigSettings>> GetRigSettingsForCustomerAsync(long customerId, CombinedUserRoles roles)
        {
            if (!roles.HasUpgradeMatrixAccess()) return new List<RigSettings>();
            return await _db.UpgradeMatrix_RigSettings
                .AsNoTracking()
                .ApplyPermissionsForRigSettings(roles)
                .Include(r => r.Rig)
                .ThenInclude(r => r.Owner)
                .Where(r => r.Rig != null && ((r.Rig.Owner != null && r.Rig.Owner.RootCustomerId == customerId) || r.Rig.RootOwnerId == customerId))
                .ToListAsync();
        }

        public async Task<RigSettings> SaveRigSettings(RigSettings entity, CombinedUserRoles roles, User user)
        {
            if (entity == null || !roles.IsUpgradeMatrixAdmin() || user == null) return null;
            RigSettings dbEntity = null;
            if (entity.Id > 0)
            {
                //update
                dbEntity = await _db.UpgradeMatrix_RigSettings.FirstOrDefaultAsync(r => r.Id == entity.Id);
                if (dbEntity == null) return null;
                dbEntity.RigId = entity.RigId;
                dbEntity.RigType = entity.RigType;
                dbEntity.ShortName = entity.ShortName;
                dbEntity.Modified = DateTime.UtcNow;
                dbEntity.ModifiedById = user.Id;
            }
            else
            {
                //insert
                dbEntity = new RigSettings
                {
                    RigId = entity.RigId,
                    RigType = entity.RigType,
                    ShortName = entity.ShortName,
                    CreatedById = user.Id,
                    Created = DateTime.UtcNow
                };
                _db.UpgradeMatrix_RigSettings.Add(dbEntity);
            }

            await _db.SaveChangesAsync();
            return dbEntity;
        }

        public async Task<bool> DeleteRigSettings(long id, CombinedUserRoles roles)
        {
            if(!roles.IsUpgradeMatrixAdmin()) return false;
            var dbEntity = await _db.UpgradeMatrix_RigSettings.FirstOrDefaultAsync(e => e.Id == id);
            if (dbEntity == null) return false;
            try
            {
                //delete CcnState
                foreach (var ccnState in _db.UpgradeMatrix_CcnStates.Where(cs => cs.RigSettingsId == id))
                {
                    _db.UpgradeMatrix_CcnStates.Remove(ccnState);
                }
                //delete QuotationState
                foreach (var quotationState in _db.UpgradeMatrix_QuotationStates.Where(cs => cs.RigSettingsId == id))
                {
                    _db.UpgradeMatrix_QuotationStates.Remove(quotationState);
                }
                //delete RigSettings
                _db.UpgradeMatrix_RigSettings.Remove(dbEntity);
                await _db.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<List<EquipmentCategory>> GetEquipmentCategoriesAsync(CombinedUserRoles roles)
        {
            if (!roles.HasUpgradeMatrixAccess()) return new List<EquipmentCategory>();
            return await _db.UpgradeMatrix_EquipmentCategories
                .AsNoTracking()
                .OrderBy(ec => ec.ShortName)
                .ToListAsync();
        }

        public async Task<EquipmentCategory> GetEquipmentCategoryAsync(long id, CombinedUserRoles roles)
        {
            if (!roles.HasUpgradeMatrixAccess()) return null;
            return await _db.UpgradeMatrix_EquipmentCategories
                .AsNoTracking()
                .FirstOrDefaultAsync(ec => ec.Id == id);
        }

        public async Task<EquipmentCategory> GetEquipmentCategoryByNameAsync(string shortName, CombinedUserRoles roles)
        {
            if (!roles.HasUpgradeMatrixAccess()) return null;
            return await _db.UpgradeMatrix_EquipmentCategories
                .AsNoTracking()
                .FirstOrDefaultAsync(ec => ec.ShortName == shortName.Trim());
        }

        public async Task<EquipmentCategory> SaveEquipmentCategoryAsync(EquipmentCategory entity, CombinedUserRoles roles, User user)
        {
            if (entity == null || !roles.IsUpgradeMatrixAdmin() || user == null) return null;
            EquipmentCategory dbEntity = null;
            if (entity.Id > 0)
            {
                //update
                dbEntity = await _db.UpgradeMatrix_EquipmentCategories.FirstOrDefaultAsync(e => e.Id == entity.Id);
                if (dbEntity == null) return null;
                dbEntity.ShortName = entity.ShortName;
                dbEntity.Modified = DateTime.UtcNow;
                dbEntity.ModifiedById = user.Id  ;
            }
            else
            {
                //insert
                dbEntity = new EquipmentCategory
                {
                    ShortName = entity.ShortName,
                    CreatedById = user.Id,
                    Created = DateTime.UtcNow
                };
                _db.UpgradeMatrix_EquipmentCategories.Add(dbEntity);
            }

            await _db.SaveChangesAsync();
            return dbEntity;
        }

        public async Task<bool> DeleteEquipmentCategory(long id, CombinedUserRoles roles)
        {
            if(!roles.IsUpgradeMatrixAdmin()) return false;
            var dbEntity = _db.UpgradeMatrix_EquipmentCategories.FirstOrDefault(e => e.Id == id);
            if (dbEntity == null) return false;
            try
            {
                //delete QuotationItem/QuotationState
                var quotationItems = await _db.UpgradeMatrix_QuotationItems.Where(qi => qi.EquipmentCategoryId == id).ToListAsync();
                foreach (var quotationItem in quotationItems)
                {
                    DeleteQuotationItemAndRelatedStates(quotationItem);
                }
                //delete CcnItem/CcnState
                var ccnItems = await _db.UpgradeMatrix_CcnItems.Where(ci => ci.EquipmentCategoryId == id).ToListAsync();
                foreach (var ccnItem in ccnItems)
                {
                    await DeleteCcnItemAndRelatedStates(ccnItem);
                }
                //delete EquipmentCategory
                _db.UpgradeMatrix_EquipmentCategories.Remove(dbEntity);
                await _db.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<List<CcnItem>> GetCcnItemsAsync(long customerId, CombinedUserRoles roles)
        {
            if (!roles.HasUpgradeMatrixAccess()) return new List<CcnItem>();
            return await _db.UpgradeMatrix_CcnItems
                .AsNoTracking()
                .Include(ci => ci.EquipmentCategory)
                .Where(i => i.RootCustomerId == customerId)
                .ToListAsync();
        }

        public async Task<CcnItem> SaveCcnItemAsync(CcnItem entity, CombinedUserRoles roles, User user)
        {
            if (entity == null || !roles.IsUpgradeMatrixAdmin() || user == null) return null;
            CcnItem dbEntity = null;
            if (entity.Id > 0)
            {
                //update
                dbEntity = await _db.UpgradeMatrix_CcnItems.FirstOrDefaultAsync(ci => ci.Id == entity.Id);
                if (dbEntity == null) return null;
                dbEntity.EquipmentCategoryId = entity.EquipmentCategoryId;
                dbEntity.Description = entity.Description;
                dbEntity.Comments = entity.Comments;
                dbEntity.PackageUpgrade = entity.PackageUpgrade;
                dbEntity.RigType = entity.RigType;
                dbEntity.Modified = DateTime.UtcNow;
                dbEntity.ModifiedById = user.Id ;
            }
            else
            {
                //insert
                dbEntity = new CcnItem
                {
                    RootCustomerId = entity.RootCustomerId,
                    EquipmentCategoryId = entity.EquipmentCategoryId,
                    Description = entity.Description,
                    Comments = entity.Comments,
                    PackageUpgrade = entity.PackageUpgrade,
                    RigType = entity.RigType,
                    CreatedById = user.Id,
                    Created = DateTime.UtcNow
                };
                _db.UpgradeMatrix_CcnItems.Add(dbEntity);
            }

            await _db.SaveChangesAsync();
            return dbEntity;
        }

        public async Task<bool> DeleteCcnItemAsync(long id, CombinedUserRoles roles)
        {
            if(!roles.IsUpgradeMatrixAdmin()) return false;
            var dbEntity = _db.UpgradeMatrix_CcnItems.FirstOrDefault(e => e.Id == id);
            if (dbEntity == null) return false;
            try
            {
                await DeleteCcnItemAndRelatedStates(dbEntity);
                await _db.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }

        private async Task DeleteCcnItemAndRelatedStates(CcnItem dbEntity)
        {

            var relatedRigStates = _db.UpgradeMatrix_CcnStates.Where(s => s.CcnItemId == dbEntity.Id);
            foreach (var relRigState in relatedRigStates)
            {
                //delete ccn document, if any
                if (!string.IsNullOrEmpty(relRigState.DocumentPath))
                {
                    var blobUri = await _blobSasGenerator.GetSasBlobUri(relRigState.DocumentPath, StorageConfig.BlobCcnContainerName,
                        BlobAccountSasPermissions.Read | BlobAccountSasPermissions.Delete);
                    BlobClient blobClient = new BlobClient(blobUri, null);
                    if (blobClient.Exists())
                    {
                        //delete it
                        await blobClient.DeleteAsync();
                    }
                }
                _db.UpgradeMatrix_CcnStates.Remove(relRigState);
            }
            _db.UpgradeMatrix_CcnItems.Remove(dbEntity);
        }

        public async Task<CcnState> GetCcnStateAsync(long id, CombinedUserRoles roles)
        {
            if (!roles.HasUpgradeMatrixAccess()) return null;
            return await _db.UpgradeMatrix_CcnStates
                .AsNoTracking()
                .Include(cs => cs.RigSettings)
                .ThenInclude(rs => rs.Rig)
                .ApplyPermissionsForCcnState(roles)
                .FirstOrDefaultAsync(cs => cs.Id == id);
        }

        public async Task<CcnState> SaveCcnStateAsync(CcnState entity, CombinedUserRoles roles, User user)
        {
            if (entity == null || !roles.IsUpgradeMatrixAdmin() || user == null) return null;
            CcnState dbEntity = null;
            if (entity.Id > 0)
            {
                //update
                dbEntity = await _db.UpgradeMatrix_CcnStates.FirstOrDefaultAsync(ci => ci.Id == entity.Id);
                if (dbEntity == null) return null;
                dbEntity.CcnItemId = entity.CcnItemId;
                dbEntity.RigSettingsId = entity.RigSettingsId;
                dbEntity.Status = entity.Status;
                dbEntity.CcnName = entity.CcnName;
                dbEntity.DocumentPath = entity.DocumentPath;
                dbEntity.Modified = DateTime.UtcNow;
                dbEntity.ModifiedById = user.Id;
            }
            else
            {
                //insert
                dbEntity = new CcnState
                {
                    CcnItemId =  entity.CcnItemId,
                    RigSettingsId = entity.RigSettingsId,
                    Status = entity.Status,
                    CcnName = entity.CcnName,
                    DocumentPath = entity.DocumentPath,
                    Created = DateTime.UtcNow,
                    CreatedById = user.Id
                };
                _db.UpgradeMatrix_CcnStates.Add(dbEntity);
            }

            await _db.SaveChangesAsync();
            return dbEntity;
        }

        public async Task<CcnState> ApproveCcnStateAsync(long id, CombinedUserRoles roles, User user)
        {
            var dbEntity = await _db.UpgradeMatrix_CcnStates
                .Include(cs => cs.RigSettings)
                .FirstOrDefaultAsync(e => e.Id == id);
            if (dbEntity == null || !dbEntity.CanApproveCcnState(roles)) return null;
            try
            {
                dbEntity.Status = UpgradeMatrixItemStatus.Approved;
                dbEntity.ApprovedById = user.Id;
                dbEntity.Approved = DateTime.UtcNow;
                await _db.SaveChangesAsync();
                return dbEntity;
            }
            catch
            {
                return null;
            }
        }

        public Dictionary<long, List<CcnState>> GetCcnStates(IEnumerable<long> ccnItemIds, CombinedUserRoles roles)
        {
            if (!roles.HasUpgradeMatrixAccess()) return new Dictionary<long, List<CcnState>>();
            return _db.UpgradeMatrix_CcnStates
                .AsNoTracking()
                .Include(cs => cs.RigSettings)
                .Include(cs => cs.CcnItem)
                .Include(cs => cs.ApprovedBy)
                .ApplyPermissionsForCcnState(roles)
                .Where(cs => ccnItemIds.Contains(cs.CcnItemId))
                .AsEnumerable()
                .GroupBy(cs => cs.CcnItemId)
                .ToDictionary(g => g.Key, g => g.ToList());
        }

        public async Task<List<QuotationItem>> GetQuotationItemsAsync(long customerId, CombinedUserRoles roles)
        {
            if (!roles.HasUpgradeMatrixAccess()) return null;
            return await _db.UpgradeMatrix_QuotationItems
                .AsNoTracking()
                .Include(ci => ci.EquipmentCategory)
                .Where(i => i.RootCustomerId == customerId)
                .ToListAsync();
        }

        public async Task<QuotationItem> SaveQuotationItemAsync(QuotationItem entity, CombinedUserRoles roles, User user)
        {
            if (entity == null || !roles.IsUpgradeMatrixAdmin()) return null;
            QuotationItem dbEntity = null;
            if (entity.Id > 0)
            {
                //update
                dbEntity = await _db.UpgradeMatrix_QuotationItems.FirstOrDefaultAsync(qi => qi.Id == entity.Id);
                if (dbEntity == null) return null;
                dbEntity.EquipmentCategoryId = entity.EquipmentCategoryId;
                dbEntity.Description = entity.Description;
                dbEntity.QuotationReference = entity.QuotationReference;
                dbEntity.Benefits = entity.Heading;
                dbEntity.Heading = entity.Heading;
                dbEntity.Modified = DateTime.UtcNow;
                dbEntity.ModifiedById = user.Id;

            }
            else
            {
                //insert
                dbEntity = new QuotationItem
                {
                    RootCustomerId = entity.RootCustomerId,
                    EquipmentCategoryId = entity.EquipmentCategoryId,
                    Description = entity.Description,
                    Benefits = entity.Benefits,
                    Heading = entity.Heading,
                    CreatedById = user.Id,
                    Created = DateTime.UtcNow
                };
                _db.UpgradeMatrix_QuotationItems.Add(dbEntity);
            }

            await _db.SaveChangesAsync();
            return dbEntity;
        }

        public async Task<bool> DeleteQuotationItemAsync(long id, CombinedUserRoles roles)
        {
            if (!roles.IsUpgradeMatrixAdmin()) return false;
            var dbEntity = await _db.UpgradeMatrix_QuotationItems.FirstOrDefaultAsync(e => e.Id == id);
            if (dbEntity == null) return false;
            try
            {
                DeleteQuotationItemAndRelatedStates(dbEntity);
                await _db.SaveChangesAsync();
                return true;
            }
            catch
            {
                return false;
            }
        }

        private void DeleteQuotationItemAndRelatedStates(QuotationItem dbEntity)
        {
            var relatedRigStates = _db.UpgradeMatrix_QuotationStates.Where(s => s.QuotationItemId == dbEntity.Id);
            foreach (var relRigState in relatedRigStates)
            {
                _db.UpgradeMatrix_QuotationStates.Remove(relRigState);
            }
            _db.UpgradeMatrix_QuotationItems.Remove(dbEntity);
        }

        public Dictionary<long, List<QuotationState>> GetQuotationStates(IEnumerable<long> quotationItemIds, CombinedUserRoles roles)
        {
            if (!roles.HasUpgradeMatrixAccess()) return null;
            return _db.UpgradeMatrix_QuotationStates
                .AsNoTracking()
                .Include(cs => cs.QuotationItem)
                .Include(cs => cs.RigSettings)
                .ApplyPermissionsForQuotationState(roles)
                .Where(cs => quotationItemIds.Contains(cs.QuotationItemId))
                .AsEnumerable()
                .GroupBy(cs => cs.QuotationItemId)
                .ToDictionary(g => g.Key, g => g.ToList());
        }

        public async Task<QuotationState> SaveQuotationStateAsync(QuotationState entity, CombinedUserRoles roles, User user)
        {
            if (entity == null || !roles.IsUpgradeMatrixAdmin()) return null;
            QuotationState dbEntity = null;
            if (entity.Id > 0)
            {
                //update
                dbEntity = await _db.UpgradeMatrix_QuotationStates.FirstOrDefaultAsync(qs => qs.Id == entity.Id);
                if (dbEntity == null) return null;
                dbEntity.QuotationItemId = entity.QuotationItemId;
                dbEntity.RigSettingsId = entity.RigSettingsId;
                dbEntity.Status = entity.Status;
                dbEntity.Modified = DateTime.UtcNow;
                dbEntity.ModifiedById = user.ModifiedById;
            }
            else
            {
                //insert
                dbEntity = new QuotationState
                {
                    QuotationItemId = entity.QuotationItemId,
                    RigSettingsId = entity.RigSettingsId,
                    Status = entity.Status,
                    Created = DateTime.UtcNow,
                    CreatedById = user.Id
                };
                _db.UpgradeMatrix_QuotationStates.Add(dbEntity);
            }

            await _db.SaveChangesAsync();
            return dbEntity;
        }
    }
}
