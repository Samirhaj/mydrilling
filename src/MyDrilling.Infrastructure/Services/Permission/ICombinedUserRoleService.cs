﻿using System.Threading.Tasks;
using MyDrilling.Core;

namespace MyDrilling.Infrastructure.Services.Permission
{
    public interface ICombinedUserRoleService
    {
        Task<CombinedUserRoles> GetRolesAsync(long userId);
    }
}
