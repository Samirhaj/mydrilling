﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core;
using MyDrilling.Core.Entities.Permission;
using MyDrilling.Core.Exceptions;
using MyDrilling.Infrastructure.Data;

namespace MyDrilling.Infrastructure.Services.Permission
{
    public class CombinedUserRoleService : ICombinedUserRoleService
    {
        private readonly MyDrillingDb _db;

        public CombinedUserRoleService(MyDrillingDb db)
        {
            _db = db;
        }

        public async Task<CombinedUserRoles> GetRolesAsync(long userId)
        {
            //roles connected directly to user
            var userRoles = await _db.Permission_UserRoles.AsNoTracking()
                .Where(ur => ur.UserId == userId)
                .Select(ur => new UserRole
                {
                    Role = ur.Role,
                    RigId = ur.RigId,
                    RootCustomerId = ur.RootCustomerId
                })
                .ToListAsync();

            //roles connected to user through profile
            var userProfileRoles = await _db.Permission_UserProfiles.AsNoTracking()
                .Where(up => up.UserId == userId)
                .SelectMany(up => up.Profile.ProfileRoles, (up, r) => new UserRole
                {
                    Role = r.Role,
                    RigId = up.RigId
                })
                .ToListAsync();

            userRoles.AddRange(userProfileRoles);

            var combinedRoles = userRoles.GroupBy(x => x.Role)
                .ToDictionary(x => x.Key,
                    x => RoleInfo.ByRole[x.Key].RoleType == RoleInfo.Type.General
                        ? Array.Empty<long>()
                        : x.Select(y => y.GetId()).Distinct().ToArray());

            return new CombinedUserRoles(combinedRoles);
        }

        private sealed class UserRole
        {
            public RoleInfo.Role Role { get; set; }
            public long? RigId { get; set; }
            public long? RootCustomerId { get; set; }
            private RoleInfo.Type Type => RoleInfo.ByRole[Role].RoleType;

            public long GetId()
            {
                return Type == RoleInfo.Type.General
                    ? default //ignore rigs and root customers if role is general
                    : Type == RoleInfo.Type.Rig
                        ? RigId ?? throw new BusinessException($"Invalid data: {nameof(RigId)} is empty for {nameof(RoleInfo.RoleType)}={RoleInfo.Type.Rig}")
                        : RootCustomerId ?? throw new BusinessException($"Invalid data: {nameof(RootCustomerId)} is empty for {nameof(RoleInfo.RoleType)}={RoleInfo.Type.RootCustomer}");
            }
        }
    }
}
