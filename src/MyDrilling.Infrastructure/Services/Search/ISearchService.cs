﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Azure.Search.Documents;
using Azure.Search.Documents.Indexes;
using Azure.Search.Documents.Models;
using MyDrilling.Core.Entities;

namespace MyDrilling.Infrastructure.Services.Search
{
    public interface ISearchService
    {
        SearchIndexClient GetSearchIndexClient();
        SearchClient GetSearchClient();
        Task CreateSearchIndex();
        Task IndexDocuments(IndexDocumentsAction<SearchItem>[] searchItems);
    }
}
