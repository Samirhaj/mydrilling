﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Azure;
using Azure.Search.Documents;
using Azure.Search.Documents.Indexes;
using Azure.Search.Documents.Indexes.Models;
using Azure.Search.Documents.Models;
using MyDrilling.Core.Entities;

namespace MyDrilling.Infrastructure.Services.Search
{
    public class SearchService : ISearchService
    {
        public readonly string IndexName = "search-index";
        public readonly string SuggesterName = "suggester-all";
        

        private readonly Uri _serviceEndpoint;
        private readonly AzureKeyCredential _credential;
        private readonly SearchIndexClient _idxClient;
        private readonly SearchClient _searchClient;
        public SearchService(string searchServiceName, string searchApiKey)
        {
            _serviceEndpoint = new Uri($"https://{searchServiceName}.search.windows.net/");
            _credential = new AzureKeyCredential(searchApiKey);
            _idxClient = new SearchIndexClient(_serviceEndpoint, _credential);
            _searchClient = new SearchClient(_serviceEndpoint, IndexName, _credential);
        }
        public SearchIndexClient GetSearchIndexClient()
        {
            return _idxClient;
        }

        public SearchClient GetSearchClient()
        {
            return _searchClient;
        }

        public async Task CreateSearchIndex()
        {
            SearchIndex index = new SearchIndex(IndexName)
            {
                Fields =
                {
                    new SimpleField("Id", SearchFieldDataType.String) {IsKey = true},
                    new SimpleField("RigId", SearchFieldDataType.String) {IsFilterable = true},
                    new SimpleField("EntityType", SearchFieldDataType.String) {IsFilterable = true},
                    new SimpleField("EntityId", SearchFieldDataType.Int64),
                    new SearchableField("ReferenceId"),
                    new SearchableField("Name"),
                    new SearchableField("Description")
                }, 
                Suggesters =
                {
                    new SearchSuggester(SuggesterName, new []{"ReferenceId", "Name", "Description"})
                }
            };
            await _idxClient.CreateIndexAsync(index);
        }

        public async Task IndexDocuments(IndexDocumentsAction<SearchItem>[] searchItems)
        {
            var i = 0;
            var maxBatchCount = 30000;
            while (i * maxBatchCount < searchItems.Length)
            {
                var searchItemsBatch = searchItems.Skip(i++ * maxBatchCount).Take(maxBatchCount).ToArray();
                IndexDocumentsBatch<SearchItem> batch = IndexDocumentsBatch.Create(searchItemsBatch);
                IndexDocumentsOptions idxOptions = new IndexDocumentsOptions { ThrowOnAnyError = true };
                await _searchClient.IndexDocumentsAsync(batch, idxOptions);
            }
        }
    }
}
