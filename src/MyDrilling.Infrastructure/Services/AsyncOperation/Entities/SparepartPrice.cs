﻿using System.Text.Json.Serialization;

namespace MyDrilling.Infrastructure.Services.AsyncOperation.Entities
{
    public class SparepartPrice
    {
        [JsonPropertyName("value")] public decimal Value { get; set; }
        [JsonPropertyName("currency")] public string Currency { get; set; }
    }
}