﻿using System.Threading.Tasks;
using MyDrilling.Core.Entities;
using MyDrilling.Infrastructure.Data;

namespace MyDrilling.Infrastructure.Services.AsyncOperation
{
    public static class AsyncOperationCreateExt
    {
        public static async Task<long> CreateNewAsyncOperation(this MyDrillingDb _db, User initiator)
        {
            var operation = new Core.Entities.AsyncOperation.AsyncOperation(initiator);

            _db.AsyncOperations.Add(operation);
            await _db.SaveChangesAsync();

            return operation.Id;
        }
    }
}