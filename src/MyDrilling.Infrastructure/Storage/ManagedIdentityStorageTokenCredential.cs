﻿using System.Threading;
using System.Threading.Tasks;
using Azure.Core;
using Microsoft.Azure.Services.AppAuthentication;

namespace MyDrilling.Infrastructure.Storage
{
    /// <summary>
    /// Inspired by https://github.com/juunas11/Joonasw.ManagedIdentityDemos/blob/master/Joonasw.ManagedIdentityDemos/Services/ManagedIdentityStorageTokenCredential.cs
    /// Use access token based authentication for azure storage
    /// </summary>
    public class ManagedIdentityStorageTokenCredential : TokenCredential
    {
        private const string Resource = "https://storage.azure.com/";

        public override async ValueTask<AccessToken> GetTokenAsync(TokenRequestContext requestContext, CancellationToken cancellationToken)
        {
            var result = await new AzureServiceTokenProvider().GetAuthenticationResultAsync(Resource, cancellationToken: cancellationToken);
            return new AccessToken(result.AccessToken, result.ExpiresOn);
        }

        public override AccessToken GetToken(TokenRequestContext requestContext, CancellationToken cancellationToken)
        {
            return GetTokenAsync(requestContext, cancellationToken).GetAwaiter().GetResult();
        }
    }
}
