﻿using System;
using System.Threading.Tasks;
using Azure.Storage.Sas;

namespace MyDrilling.Infrastructure.Storage.Blob
{
    public interface IBlobSasGenerator
    {
        Task<Uri> GetSasBlobUri(string blobName, string blobContainerName, BlobAccountSasPermissions permissions);
    }
}
