﻿using System;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Azure.Storage.Sas;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;

namespace MyDrilling.Infrastructure.Storage.Blob
{
    /// <summary>
    /// https://docs.microsoft.com/en-us/azure/storage/blobs/storage-blob-user-delegation-sas-create-dotnet
    /// Account (managed identity or AzureAD user) must have "Storage Blob Data Contributor" role in storage account to get delegation key
    /// </summary>
    public class BlobSasGenerator : IBlobSasGenerator
    {
        private const string CacheKey = "USER_DELEGATION_KEY";
        private readonly IMemoryCache _cache;
        private readonly StorageConfig _storageConfig;
        private readonly TimeSpan _keyTtl = TimeSpan.FromHours(3);
        private readonly BlobServiceClient _blobServiceClient;
        private readonly TimeSpan _keyExpirationFix = TimeSpan.FromMinutes(10);

        public BlobSasGenerator(IMemoryCache cache,
            BlobServiceClient blobServiceClient,
            IOptions<StorageConfig> storageConfig)
        {
            _cache = cache;
            _blobServiceClient = blobServiceClient;
            _storageConfig = storageConfig.Value;
        }

        public async Task<Uri> GetSasBlobUri(string blobName, string blobContainerName, BlobAccountSasPermissions permissions)
        {
            var sasBuilder = new BlobSasBuilder
            {
                BlobContainerName = blobContainerName,
                BlobName = blobName,
                Resource = "b",
                StartsOn = DateTimeOffset.UtcNow.AddMinutes(-_storageConfig.SasTimeFrameDelay),
                ExpiresOn = DateTimeOffset.UtcNow.AddMinutes(_storageConfig.SasTimeFrameDelay)
            };
            sasBuilder.SetPermissions(permissions);
            var userDelegatedKey = await GetKeyAsync();
            var sasToken = sasBuilder.ToSasQueryParameters(userDelegatedKey, _storageConfig.AccountName).ToString();

            var blobUri = new UriBuilder
            {
                Scheme = "https",
                Host = _storageConfig.BlobHost,
                Path = $"{blobContainerName}/{blobName}",
                Query = sasToken
            };
            return blobUri.Uri;
        }

        private Task<UserDelegationKey> GetKeyAsync()
        {
            return _cache.GetOrCreateAsync(CacheKey, async x =>
            {
                var now = DateTimeOffset.UtcNow.AddMinutes(-_storageConfig.SasTimeFrameDelay);
                var expiresOn = now.Add(_keyTtl);

                //we need to remove the key from the cache before its actual expiration to avoid generation of immediately expired SAS signatures
                x.AbsoluteExpiration = expiresOn.Subtract(_keyExpirationFix);

                var response = await _blobServiceClient.GetUserDelegationKeyAsync(now, expiresOn);
                return response.Value;
            });
        }
    }
}
