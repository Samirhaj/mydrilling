﻿using System;
using Azure.Storage.Blobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MyDrilling.Infrastructure.Storage.Blob
{
    public static class BlobRegistrationExt
    {
        public static void AddMyDrillingBlob(this IServiceCollection services, IConfiguration config)
        {
            var section = config.GetSection("StorageConfig");
            services.Configure<StorageConfig>(section);
            var storageConfig = new StorageConfig();
            section.Bind(storageConfig);

            if (string.IsNullOrEmpty(storageConfig.ConnectionString))
            {
                services.AddSingleton(sp => new BlobServiceClient(new Uri(storageConfig.BlobUrl), new ManagedIdentityStorageTokenCredential()));
            }
            else
            {
                services.AddSingleton(sp => new BlobServiceClient(storageConfig.ConnectionString));
            }
        }
    }
}
