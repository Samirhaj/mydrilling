﻿namespace MyDrilling.Infrastructure.Storage
{
    public static class StorageConstants
    {
        public static class Enquiry
        {
            public static string GetFinalBlobLocation(long enquiryId, string blobPath) =>
                $"enquiries\\{enquiryId}\\{blobPath}";
        }

        public static class BlobMetadataKeys
        {
            public const string EnquiryId = "enquiryId";
            public const string AuthorId = "authorId";
        }
    }
}
