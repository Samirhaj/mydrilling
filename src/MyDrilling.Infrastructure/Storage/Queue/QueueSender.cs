﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Core.Entities.UpgradeMatrix;
using MyDrilling.Infrastructure.MessageContracts;

namespace MyDrilling.Infrastructure.Storage.Queue
{
    public class QueueSender : IQueueSender
    {
        private readonly IQueueClientFactory _queueClientFactory;
        
        public QueueSender(IQueueClientFactory queueClientFactory)
        {
            _queueClientFactory = queueClientFactory;
        }

        public Task EnqueueSyncEnquiryToSapAsync(long enquiryId) =>
            _queueClientFactory.GetClient(StorageConfig.SyncEnquiryToSapQueue)
                .SendMessageAsync(ToBase64(JsonSerializer.Serialize(new SyncEnquiryToSapMessage {EnquiryId = enquiryId})));

        public Task EnqueueFileRequestAsync(long requestId, string blobRelativePath, string docName, string docType, string altDocId, string docVersion, bool isLarge) =>
            _queueClientFactory.GetClient(StorageConfig.FileRequestQueue)
                .SendMessageAsync(ToBase64(JsonSerializer.Serialize(new FileRequestMessage
            {
                RequestId = requestId, 
                BlobRelativePath = blobRelativePath, 
                DocumentName = docName, 
                DocumentType = docType, 
                AltDocumentId = altDocId, 
                DocumentVersion = docVersion, 
                IsLarge = isLarge
            })));

        public Task EnqueueEnquiryEventAsync(long enquiryId, EnquiryEventType enquiryEventType, Dictionary<string, string> details = null) =>
            _queueClientFactory.GetClient(StorageConfig.EnquiryEventQueue)
                .SendMessageAsync(ToBase64(JsonSerializer.Serialize(new EnquiryEventMessage { EnquiryId = enquiryId, EventId = (int)enquiryEventType, Details = details})));

        public Task EnqueueUpgradeMatrixEventAsync(long id, UpgradeMatrixItemType itemType) =>
            _queueClientFactory.GetClient(StorageConfig.UpgradeMatrixEventQueue)
                .SendMessageAsync(ToBase64(JsonSerializer.Serialize(new UpgradeMatrixEventMessage { Id = id, ItemType = itemType })));
        
        public Task EnqueueRequestSparepartPriceAsync(long requestId, string referenceId, string customerId) =>
            _queueClientFactory.GetClient(StorageConfig.RequestSparepartPriceQueue)
                .SendMessageAsync(ToBase64(JsonSerializer.Serialize(new RequestSparepartPriceMessage { RequestId = requestId, CustomerId = customerId, ReferenceId = referenceId})));

        private static string ToBase64(string msg) => Convert.ToBase64String(Encoding.UTF8.GetBytes(msg));
    }
}
