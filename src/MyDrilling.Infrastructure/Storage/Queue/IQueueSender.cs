﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Core.Entities.UpgradeMatrix;

namespace MyDrilling.Infrastructure.Storage.Queue
{
    public interface IQueueSender
    {
        Task EnqueueSyncEnquiryToSapAsync(long enquiryId);
        Task EnqueueFileRequestAsync(long requestId, string blobRelativePath, string docName, string docType, string altDocId, string docVersion, bool isLarge);
        Task EnqueueEnquiryEventAsync(long enquiryId, EnquiryEventType enquiryEventType, Dictionary<string, string> details = null);
        Task EnqueueUpgradeMatrixEventAsync(long id, UpgradeMatrixItemType itemType);
        Task EnqueueRequestSparepartPriceAsync(long requestId, string referenceId, string customerId);

    }
}
