﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using Azure.Storage.Queues;
using Microsoft.Extensions.Options;

namespace MyDrilling.Infrastructure.Storage.Queue
{
    public interface IQueueClientFactory
    {
        QueueClient GetClient(string queueName);
    }

    public class QueueClientFactory : IQueueClientFactory
    {
        private readonly StorageConfig _storageConfig;

        public QueueClientFactory(IOptions<StorageConfig> storageConfig)
        {
            _storageConfig = storageConfig.Value;
        }

        public QueueClient GetClient(string queueName)
        {
            var queueClient = _queues.GetOrAdd(queueName, 
                q => new Lazy<QueueClient>(() => string.IsNullOrEmpty(_storageConfig.ConnectionString)
                ? new QueueClient(new Uri($"https://{_storageConfig.AccountName}.queue.core.windows.net/{queueName}"), ManagedIdentityCredential.Value)
                : new QueueClient(_storageConfig.ConnectionString, queueName), LazyThreadSafetyMode.ExecutionAndPublication));

            return queueClient.Value;
        }

        private Lazy<ManagedIdentityStorageTokenCredential> ManagedIdentityCredential => new Lazy<ManagedIdentityStorageTokenCredential>();

        private readonly ConcurrentDictionary<string, Lazy<QueueClient>> _queues = new ConcurrentDictionary<string, Lazy<QueueClient>>();
    }
}
