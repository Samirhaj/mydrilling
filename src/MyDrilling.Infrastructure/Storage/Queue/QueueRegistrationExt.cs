﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MyDrilling.Infrastructure.Storage.Queue
{
    public static class QueueRegistrationExt
    {
        public static void AddMyDrillingQueues(this IServiceCollection services, IConfiguration config)
        {
            var section = config.GetSection("StorageConfig");
            services.Configure<StorageConfig>(section);
            services.AddSingleton<IQueueSender, QueueSender>();
            services.AddSingleton<IQueueClientFactory, QueueClientFactory>();
        }
    }
}
