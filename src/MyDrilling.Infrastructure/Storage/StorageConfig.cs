﻿namespace MyDrilling.Infrastructure.Storage
{
    public class StorageConfig
    {
        public string ConnectionString { get; set; }
        public string BlobUrl => $"https://{BlobHost}";
        public string BlobHost => $"{AccountName}.blob.core.windows.net";
        public string AccountName { get; set; }
        public int SasTimeFrameDelay { get; set; }

        public const string BlobCcnContainerName = "ccn-documents";
        public const string BlobUserImagesContainerName = "user-profile-images";
        public const string EnquiriesContainerName = "enquiries";
        public const string TemporaryDocumentsContainerName = "temp-docs";

        public const string SyncEnquiryToSapQueue = "sync-enquiry-to-sap";
        public const string FileRequestQueue = "file-request";
        public const string EnquiryEventQueue = "enquiry-event";
        public const string EmailQueue = "email";
        public const string UpgradeMatrixEventQueue = "upgrade-matrix-event";
        public const string EnquiryNotificationFromSapQueue = "enquiry-from-sap";
        public const string BulletinNotificationFromSapQueue = "bulletin-from-sap";
        public const string RequestSparepartPriceQueue = "request-sparepart-price-for-sap";

    }
}
