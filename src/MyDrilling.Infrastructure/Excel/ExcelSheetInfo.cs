﻿using System.Collections.Generic;

namespace MyDrilling.Infrastructure.Excel
{
    /// <summary>
    /// Contains information about excel document's sheet
    /// </summary>
    public class ExcelSheetInfo
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the columns.
        /// </summary>
        /// <value>
        /// The columns.
        /// </value>
        public IEnumerable<ExcelColumnInfo> Columns { get; set; }

        /// <summary>
        /// Gets or sets the rows.
        /// </summary>
        /// <value>
        /// The rows.
        /// </value>
        public IEnumerable<IEnumerable<string>> Rows { get; set; }
    }
}
