﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace MyDrilling.Infrastructure.Excel
{
    /// <summary>
    /// Base class for all excel generators
    /// </summary>
    public abstract class ExcelGeneratorBase
    {
        /// <summary>
        /// Sets the cell value.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="columnIndex">Index of the column.</param>
        /// <param name="rowIndex">Index of the row.</param>
        /// <param name="valueType">Type of the value.</param>
        /// <param name="value">The value.</param>
        /// <param name="styleIndex">Index of the style.</param>
        protected void SetCellValue(Worksheet worksheet, uint columnIndex, uint rowIndex, CellValues valueType, string value, uint? styleIndex = null)
        {
            var sheetData = worksheet.GetFirstChild<SheetData>();

            CreateColumnIfNecessary(worksheet, columnIndex);

            var row = CreateRowIfNecessary(sheetData, rowIndex);

            var cell = CreateCellIfNecessary(row, rowIndex, columnIndex);

            // Add the value
            cell.CellValue = new CellValue(value);
            cell.DataType = new EnumValue<CellValues>(valueType);
            if (styleIndex.HasValue) cell.StyleIndex = styleIndex.Value;
        }

        /// <summary>
        /// Returns the row.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="rowIndex">Index of the row.</param>
        /// <returns></returns>
        protected Row GetRow(Worksheet worksheet, uint rowIndex)
        {
            return worksheet.GetFirstChild<SheetData>().Elements<Row>().FirstOrDefault(r => r.RowIndex == rowIndex);
        }

        /// <summary>
        /// Returns the cell.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="columnName">Name of the column.</param>
        /// <param name="rowIndex">Index of the row.</param>
        /// <returns></returns>
        protected Cell GetCell(Worksheet worksheet, string columnName, uint rowIndex)
        {
            var row = GetRow(worksheet, rowIndex);

            if (row == null) return null;

            return row.Elements<Cell>().FirstOrDefault(c => String.Compare(c.CellReference.Value, columnName + rowIndex, StringComparison.OrdinalIgnoreCase) == 0);
        }

        /// <summary>
        /// Gets the cell.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="columnIndex">Index of the column.</param>
        /// <param name="rowIndex">Index of the row.</param>
        /// <returns></returns>
        protected Cell GetCell(Worksheet worksheet, uint columnIndex, uint rowIndex)
        {
            var row = GetRow(worksheet, rowIndex);

            if (row == null) return null;

            var columnName = GetColumnNameFromIndex(columnIndex);

            return row.Elements<Cell>().FirstOrDefault(c => String.Compare(c.CellReference.Value, columnName + rowIndex, StringComparison.OrdinalIgnoreCase) == 0);
        }

        /// <summary>
        /// Sets the width of the column.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="columnIndex">Index of the column.</param>
        /// <param name="width">The width.</param>
        protected void SetColumnWidth(Worksheet worksheet, uint columnIndex, double width)
        {
            var columns = worksheet.Elements<Columns>().FirstOrDefault();

            if (columns == null) return;

            var column = columns.Elements<Column>().FirstOrDefault(item => item.Min == columnIndex);

            if (column == null) return;

            column.Width = width;
            column.CustomWidth = true;
        }

        /// <summary>
        /// Creates the spreadsheet document.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <returns></returns>
        protected virtual SpreadsheetDocument CreateSpreadsheetDocument(Stream stream)
        {
            return SpreadsheetDocument.Create(stream, SpreadsheetDocumentType.Workbook, false);
        }

        /// <summary>
        /// Adds the default styles part to the workbook .
        /// </summary>
        /// <param name="workbookPart">The workbook part.</param>
        /// <returns></returns>
        protected virtual WorkbookStylesPart AddWorkbookDefaultStylesPart(WorkbookPart workbookPart)
        {
            var workbookStylesPart = workbookPart.AddNewPart<WorkbookStylesPart>();
            workbookStylesPart.Stylesheet = new Stylesheet();

            workbookStylesPart.Stylesheet.Fonts = new Fonts();
            workbookStylesPart.Stylesheet.Fonts.AppendChild(new Font());

            workbookStylesPart.Stylesheet.Fills = new Fills();
            workbookStylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = new PatternFill { PatternType = PatternValues.None } });
            workbookStylesPart.Stylesheet.Fills.AppendChild(new Fill { PatternFill = new PatternFill { PatternType = PatternValues.Gray125 } });

            workbookStylesPart.Stylesheet.Borders = new Borders();
            workbookStylesPart.Stylesheet.Borders.AppendChild(new Border());

            workbookStylesPart.Stylesheet.CellStyleFormats = new CellStyleFormats();
            workbookStylesPart.Stylesheet.CellStyleFormats.AppendChild(new CellFormat());

            // cell format list
            workbookStylesPart.Stylesheet.CellFormats = new CellFormats();
            workbookStylesPart.Stylesheet.CellFormats.AppendChild(new CellFormat());

            return workbookStylesPart;
        }

        /// <summary>
        /// Adds the worksheet part.
        /// </summary>
        /// <param name="workbookPart">The workbook part.</param>
        /// <param name="sheetName">Name of the sheet.</param>
        /// <returns></returns>
        protected virtual WorksheetPart AddWorksheetPartWithSheet(WorkbookPart workbookPart, string sheetName)
        {
            var worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
            worksheetPart.Worksheet = new Worksheet(new SheetData());
            worksheetPart.Worksheet.Save();

            var sheet = new Sheet
            {
                Id = workbookPart.GetIdOfPart(worksheetPart),
                Name = sheetName
            };

            if (workbookPart.Workbook.Sheets == null)
            {
                workbookPart.Workbook.AppendChild(new Sheets());
                sheet.SheetId = 1;
            }
            else
            {
                sheet.SheetId = Convert.ToUInt32(workbookPart.Workbook.Sheets.Count() + 1);
            }

            workbookPart.Workbook.GetFirstChild<Sheets>().AppendChild(sheet);

            return worksheetPart;
        }

        #region Private Methods
        /// <summary>
        /// Creates the row if necessary.
        /// </summary>
        /// <param name="sheetData">The sheet data.</param>
        /// <param name="rowIndex">Index of the row.</param>
        private Row CreateRowIfNecessary(SheetData sheetData, uint rowIndex)
        {
            var row = sheetData.Elements<Row>().FirstOrDefault(item => item.RowIndex == rowIndex);

            if (row == null)
            {
                Row previousRow = null;
                row = new Row { RowIndex = rowIndex };
                for (uint i = rowIndex - 1; i > 0; i--)
                {
                    previousRow = sheetData.Elements<Row>().FirstOrDefault(item => item.RowIndex == i);
                    if (previousRow != null) break;
                }
                sheetData.InsertAfter(row, previousRow);
            }

            return row;
        }

        /// <summary>
        /// Creates the cell if necessary.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="rowIndex">Index of the row.</param>
        /// <param name="columnIndex">Index of the column.</param>
        /// <returns></returns>
        private Cell CreateCellIfNecessary(Row row, uint rowIndex, uint columnIndex)
        {
            var cellAddress = GetColumnNameFromIndex(columnIndex) + rowIndex;

            var cell = row.Elements<Cell>().FirstOrDefault(item => item.CellReference.Value == cellAddress);

            if (cell == null)
            {
                Cell previousCell = null;

                for (uint i = columnIndex - 1; i > 0; i--)
                {
                    previousCell = row.Elements<Cell>().FirstOrDefault(item => item.CellReference.Value == GetColumnNameFromIndex(i) + rowIndex);
                    if (previousCell != null) break;
                }

                cell = new Cell { CellReference = cellAddress };
                row.InsertAfter(cell, previousCell);
            }

            return cell;
        }

        /// <summary>
        /// Creates the column if necessary.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="columnIndex">Index of the column.</param>
        /// <returns></returns>
        private Column CreateColumnIfNecessary(Worksheet worksheet, uint columnIndex)
        {
            var columns = worksheet.Elements<Columns>().FirstOrDefault() ?? worksheet.InsertAt(new Columns(), 0);
            var column = columns.Elements<Column>().FirstOrDefault(item => item.Min == columnIndex);

            if (column == null)
            {
                Column previousColumn = null;
                for (uint i = columnIndex - 1; i > 0; i--)
                {
                    previousColumn = columns.Elements<Column>().FirstOrDefault(item => item.Min == i);

                    if (previousColumn != null) break;
                }
                column = new Column { Min = columnIndex, Max = columnIndex };
                columns.InsertAfter(column, previousColumn);
            }

            return column;
        }

        /// <summary>
        /// Gets the index of the column name from.
        /// </summary>
        /// <param name="columnIndex">Index of the column.</param>
        /// <returns></returns>
        private string GetColumnNameFromIndex(uint columnIndex)
        {
            var columnName = string.Empty;

            while (columnIndex > 0)
            {
                uint remainder = (columnIndex - 1) % 26;
                columnName = string.Concat(Convert.ToChar(65 + remainder).ToString(CultureInfo.InvariantCulture), columnName);
                columnIndex = (columnIndex - remainder) / 26;
            }

            return columnName;
        }
        #endregion
    }
}
