﻿namespace MyDrilling.Infrastructure.Excel
{
    /// <summary>
    /// Contains information about excel document's column 
    /// </summary>
    public class ExcelColumnInfo
    {
        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public int Width { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        /// <value>
        /// The title.
        /// </value>
        public string Title { get; set; }
    }
}
