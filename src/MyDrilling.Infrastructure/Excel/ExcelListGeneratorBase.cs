﻿using System.Collections.Generic;
using System.IO;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace MyDrilling.Infrastructure.Excel
{
    /// <summary>
    /// Base class for all excel generators of documents containing the list of data
    /// </summary>
    public abstract class ExcelListGeneratorBase : ExcelGeneratorBase
    {
        /// <summary>
        /// Creates the excel document stream.
        /// </summary>
        /// <param name="columns">The columns.</param>
        /// <param name="rows">The rows.</param>
        /// <param name="sheetName">Name of the sheet.</param>
        /// <returns></returns>
        protected virtual Stream CreateExcelDocumentStream(IEnumerable<ExcelColumnInfo> columns, IEnumerable<IEnumerable<string>> rows, string sheetName)
        {
            var documentStream = new MemoryStream();

            PrepareWorkbookPartAndSpreadsheetDocument(documentStream, out var workBookPart, out var spreadsheetDocument);


            FillWorkbookPart(workBookPart, new ExcelSheetInfo
            {
                Name = sheetName,
                Columns = columns,
                Rows = rows
            });

            return FinalizeDocumentStream(documentStream, spreadsheetDocument);
        }

        /// <summary>
        /// Creates the header.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="columns">The columns.</param>
        protected virtual void AddHeader(Worksheet worksheet, IEnumerable<ExcelColumnInfo> columns)
        {
            if (columns != null)
            {
                uint columnIndex = 1;

                foreach (var column in columns)
                {
                    SetCellValue(worksheet, columnIndex, 1, CellValues.String, column.Title, 1);
                    SetColumnWidth(worksheet, columnIndex, column.Width);
                    columnIndex++;
                }
            }
        }

        /// <summary>
        /// Adds the data rows.
        /// </summary>
        /// <param name="worksheet">The worksheet.</param>
        /// <param name="rows">The rows.</param>
        protected virtual void AddDataRows(Worksheet worksheet, IEnumerable<IEnumerable<string>> rows)
        {
            if (rows != null)
            {
                uint rowIndex = 2;
                foreach (var row in rows)
                {
                    uint cellIndex = 1;
                    if (row != null)
                    {
                        foreach (var cellValue in row)
                        {
                            SetCellValue(worksheet, cellIndex, rowIndex, CellValues.String, cellValue);
                            cellIndex++;
                        }
                    }

                    rowIndex++;
                }
            }
        }

        /// <summary>
        /// Prepares the workbook part.
        /// </summary>
        /// <param name="documentStream">The document stream.</param>
        /// <param name="workbookpart">The workbookpart.</param>
        /// <param name="spreadsheetDocument">The spreadsheet document.</param>
        /// <returns></returns>
        protected void PrepareWorkbookPartAndSpreadsheetDocument(Stream documentStream, out WorkbookPart workbookpart, out SpreadsheetDocument spreadsheetDocument)
        {
            spreadsheetDocument = CreateSpreadsheetDocument(documentStream);

            //Add a Workbookpart
            workbookpart = spreadsheetDocument.AddWorkbookPart();
            workbookpart.Workbook = new Workbook();
            workbookpart.Workbook.Save();

            //Add a StylesheetPart
            var stylesheetPart = AddWorkbookDefaultStylesPart(workbookpart);
            stylesheetPart.Stylesheet.Fonts.AppendChild(new Font { Bold = new Bold() });
            stylesheetPart.Stylesheet.CellFormats.AppendChild(new CellFormat { FormatId = 0, FontId = 1, BorderId = 0, FillId = 0, ApplyFill = true }).AppendChild(new Alignment { Horizontal = HorizontalAlignmentValues.Left });
            stylesheetPart.Stylesheet.Save();
        }


        /// <summary>
        /// Fills the workbook part.
        /// </summary>
        /// <param name="workbookpart">The workbookpart.</param>
        /// <param name="sheet">The sheet.</param>
        protected void FillWorkbookPart(WorkbookPart workbookpart, ExcelSheetInfo sheet)
        {
            // Add a WorksheetPart
            var worksheetPart = AddWorksheetPartWithSheet(workbookpart, sheet.Name);
            workbookpart.Workbook.Save();

            //Add header
            AddHeader(worksheetPart.Worksheet, sheet.Columns);

            //Add data rows
            AddDataRows(worksheetPart.Worksheet, sheet.Rows);

            worksheetPart.Worksheet.Save();
        }

        /// <summary>
        /// Finalizes the document stream.
        /// </summary>
        /// <param name="documentStream">The document stream.</param>
        /// <param name="spreadsheetDocument">The spreadsheet document.</param>
        /// <returns></returns>
        protected Stream FinalizeDocumentStream(Stream documentStream, SpreadsheetDocument spreadsheetDocument)
        {
            spreadsheetDocument.Close();
            documentStream.Seek(0, SeekOrigin.Begin);

            return documentStream;
        }
    }
}
