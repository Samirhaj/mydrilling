﻿using System.Text.Json.Serialization;
using MyDrilling.Core.Entities.UpgradeMatrix;


namespace MyDrilling.Infrastructure.MessageContracts
{
    public class UpgradeMatrixEventMessage
    {
        [JsonPropertyName("itemId")] public long Id { get; set; }
        [JsonPropertyName("itemType")] public UpgradeMatrixItemType ItemType { get; set; }
    }
}