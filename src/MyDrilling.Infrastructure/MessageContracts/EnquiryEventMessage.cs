﻿using System.Collections.Generic;
using System.Text.Json.Serialization;
using MyDrilling.Core.Entities.Enquiry;

namespace MyDrilling.Infrastructure.MessageContracts
{
    public class EnquiryEventMessage
    {
        [JsonPropertyName("enquiryId")] public long EnquiryId { get; set; }
        [JsonPropertyName("eventId")] public int EventId { get; set; }
        [JsonPropertyName("details")] public Dictionary<string, string> Details { get; set; }
        [JsonIgnore] public EnquiryEventType EnquiryEventType => (EnquiryEventType) EventId;

        public static class DetailKeys
        {
            public const string CommentId = "commentId";
        }
    }
}
