﻿using System.Text.Json.Serialization;

namespace MyDrilling.Infrastructure.MessageContracts
{
    public class EmailMessage
    {
        [JsonPropertyName("to")] public string To { get; set; }
        [JsonPropertyName("subject")] public string Subject { get; set; }
        [JsonPropertyName("body")] public string Body { get; set; }
    }
}
