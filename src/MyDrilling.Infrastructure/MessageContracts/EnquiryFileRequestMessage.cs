﻿using System.Text.Json.Serialization;

namespace MyDrilling.Infrastructure.MessageContracts
{
    public class EnquiryFileRequestMessage
    {
        [JsonPropertyName("attachmentId")] public long AttachmentId { get; set; }
    }
}
