﻿using System.Text.Json.Serialization;

namespace MyDrilling.Infrastructure.MessageContracts
{
    public class NewEnquiryCommentToSapMessage
    {
        [JsonPropertyName("enquiryId")] public long EnquiryId { get; set; }
        [JsonPropertyName("enquiryCommentId")] public long EnquiryCommentId { get; set; }
    }
}
