﻿using System.Text.Json.Serialization;

namespace MyDrilling.Infrastructure.MessageContracts
{
    public class FileRequestMessage
    {
        [JsonPropertyName("requestId")] public long RequestId { get; set; }
        [JsonPropertyName("blobRelativePath")] public string BlobRelativePath { get; set; }
        [JsonPropertyName("documentName")] public string DocumentName { get; set; }
        [JsonPropertyName("documentType")] public string DocumentType { get; set; }
        [JsonPropertyName("altDocumentId")] public string AltDocumentId { get; set; }
        [JsonPropertyName("documentVersion")] public string DocumentVersion { get; set; }
        [JsonPropertyName("isLarge")] public bool IsLarge { get; set; }
    }
}
