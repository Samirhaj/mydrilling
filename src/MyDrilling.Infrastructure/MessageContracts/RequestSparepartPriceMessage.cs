﻿using System.Text.Json.Serialization;

namespace MyDrilling.Infrastructure.MessageContracts
{
    public class RequestSparepartPriceMessage
    {
        [JsonPropertyName("requestId")] public long RequestId { get; set; }
        [JsonPropertyName("referenceId")] public string ReferenceId { get; set; }
        [JsonPropertyName("customerId")] public string CustomerId { get; set; }
    }
}