﻿using System.Text.Json.Serialization;

namespace MyDrilling.Infrastructure.MessageContracts
{
    public class EnquiryNotificationFromSapMessage
    {
        [JsonPropertyName("referenceId")] public string ReferenceId { get; set; }
        [JsonPropertyName("customerId")] public string CustomerId { get; set; }
    }
}
