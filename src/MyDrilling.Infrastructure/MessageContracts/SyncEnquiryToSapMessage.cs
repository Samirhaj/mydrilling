﻿using System.Text.Json.Serialization;

namespace MyDrilling.Infrastructure.MessageContracts
{
    public class SyncEnquiryToSapMessage
    {
        [JsonPropertyName("enquiryId")] public long EnquiryId { get; set; }
    }
}
