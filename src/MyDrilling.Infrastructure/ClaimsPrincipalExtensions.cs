﻿using System.Security.Claims;

namespace MyDrilling.Infrastructure
{
    public static class ClaimsPrincipalExtensions
    {
        public static string GetEmail(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.FindFirstValue(ClaimsConstants.PreferredUserName)?.ToLower();
        }

        public static string GetObjectId(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.FindFirstValue(ClaimsConstants.ObjectId);
        }

        public static string GetTenantId(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.FindFirstValue(ClaimsConstants.TenantId);
        }
    }
}
