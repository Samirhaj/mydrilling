﻿namespace MyDrilling.Infrastructure
{
    public static class ClaimsConstants
    {
        public const string PreferredUserName = "preferred_username";
        public const string ObjectId = "http://schemas.microsoft.com/identity/claims/objectidentifier";
        public const string TenantId = "http://schemas.microsoft.com/identity/claims/tenantid";
    }
}
