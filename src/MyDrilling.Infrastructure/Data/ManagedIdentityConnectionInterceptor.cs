﻿using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace MyDrilling.Infrastructure.Data
{
    /// <summary>
    /// Inspired by https://github.com/juunas11/Joonasw.ManagedIdentityDemos/blob/master/Joonasw.ManagedIdentityDemos/Data/ManagedIdentityConnectionInterceptor.cs
    /// Use access token based authentication for azure sql databases
    /// </summary>
    public class ManagedIdentityConnectionInterceptor : DbConnectionInterceptor
    {
        private const string Resource = "https://database.windows.net/";
        private readonly AzureServiceTokenProvider _tokenProvider;

        public ManagedIdentityConnectionInterceptor()
        {
            _tokenProvider = new AzureServiceTokenProvider();
        }

        public override async Task<InterceptionResult> ConnectionOpeningAsync(DbConnection connection,
            ConnectionEventData eventData,
            InterceptionResult result,
            CancellationToken cancellationToken = default)
        {
            var sqlConnection = (SqlConnection)connection;
            sqlConnection.AccessToken = await _tokenProvider.GetAccessTokenAsync(Resource, cancellationToken: cancellationToken);
            return result;
        }
    }
}
