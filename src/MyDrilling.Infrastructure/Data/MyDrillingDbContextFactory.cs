﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace MyDrilling.Infrastructure.Data
{
    /// <summary>
    /// Used for ef-core cli tool
    /// </summary>
    public class MyDrillingDbContextFactory : IDesignTimeDbContextFactory<MyDrillingDb>
    {
        public MyDrillingDb CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<MyDrillingDb>();
            optionsBuilder.UseSqlServer(nameof(MyDrillingDb));
            return new MyDrillingDb(optionsBuilder.Options);
        }
    }
}
