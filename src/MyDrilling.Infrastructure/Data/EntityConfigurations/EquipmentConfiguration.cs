﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Equipment;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations
{
    class EquipmentConfiguration : IEntityTypeConfiguration<Equipment>
    {
        public void Configure(EntityTypeBuilder<Equipment> builder)
        {
            builder.ToTable("Equipments");
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => x.ReferenceId).IsUnique();
            builder.Property(p => p.Name).HasMaxLength(50).IsRequired();
            builder.Property(p => p.ProductLongReferenceId).HasMaxLength(50);
            builder.Property(p => p.ProductShortReferenceId).HasMaxLength(50);
            builder.Property(p => p.ProductName).HasMaxLength(200);
            builder.Property(p => p.ProductGroup).HasMaxLength(200);
            builder.Property(p => p.RigOwnerReferenceId).HasMaxLength(50);
            builder.Property(p => p.ProductCode).HasMaxLength(5);
            builder.Property(p => p.SerialNumber).HasMaxLength(50);
            builder.Property(p => p.TagNumber).HasMaxLength(50);
            builder.Property(p => p.Material).HasMaxLength(50);
            builder.HasMany(x => x.Children).WithOne(x => x.Parent).HasForeignKey(x => x.ParentId);
            builder.Metadata.FindNavigation(nameof(Equipment.Children)).SetPropertyAccessMode(PropertyAccessMode.Field);
            builder.HasOne(x => x.Rig).WithMany().HasForeignKey(x => x.RigId);
            builder.HasOne(x => x.RigOwner).WithMany().HasForeignKey(x => x.RigOwnerId);
            builder.Property(p => p.Created).HasDefaultValueSql("getutcdate()");
        }
    }
}
