﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations
{
    class SapConnectorStatusConfiguration : IEntityTypeConfiguration<SapConnectorStatus>
    {
        public void Configure(EntityTypeBuilder<SapConnectorStatus> builder)
        {
            builder.ToTable("SapConnectorStatuses");
            builder.HasKey(x => x.Id);
        }
    }
}
