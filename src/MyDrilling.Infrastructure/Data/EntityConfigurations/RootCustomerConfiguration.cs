﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations
{
    class RootCustomerConfiguration : IEntityTypeConfiguration<RootCustomer>
    {
        public void Configure(EntityTypeBuilder<RootCustomer> builder)
        {
            builder.ToTable("RootCustomers");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasMaxLength(RootCustomer.MaxNameLength).IsRequired();
            builder.HasIndex(x => x.Name).IsUnique();
            builder.Property(x => x.Subdomain).HasMaxLength(255);
            builder.HasMany(x => x.Customers).WithOne(x => x.RootCustomer);
            builder.Metadata.FindNavigation(nameof(RootCustomer.Customers)).SetPropertyAccessMode(PropertyAccessMode.Field);
            builder.HasOne(x => x.DefaultCustomer).WithMany().HasForeignKey(x => x.DefaultCustomerId);
            builder.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById);
            builder.HasOne(x => x.ModifiedBy).WithMany().HasForeignKey(x => x.ModifiedById);
        }
    }
}
