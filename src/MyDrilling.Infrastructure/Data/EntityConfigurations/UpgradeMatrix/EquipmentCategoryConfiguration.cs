﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.UpgradeMatrix;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.UpgradeMatrix
{
    public class EquipmentCategoryConfiguration : IEntityTypeConfiguration<EquipmentCategory>
    {
        public void Configure(EntityTypeBuilder<EquipmentCategory> builder)
        {
            builder.ToTable("UpgradeMatrix_EquipmentCategory");
            builder.HasKey(p=> p.Id);
            builder.Property(p => p.ShortName).HasMaxLength(20).IsRequired();
            builder.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById);
            builder.HasOne(x => x.ModifiedBy).WithMany().HasForeignKey(x => x.ModifiedById);
            builder.Property(p => p.Created).HasDefaultValueSql("getutcdate()");
            builder.Property(p => p.Modified).HasDefaultValueSql("getutcdate()");
        }
    }
}
