﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.UpgradeMatrix;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.UpgradeMatrix
{
    public class QuotationStateConfiguration : IEntityTypeConfiguration<QuotationState>
    {
        public void Configure(EntityTypeBuilder<QuotationState> builder)
        {
            builder.ToTable("UpgradeMatrix_QuotationState");
            builder.HasKey(p => p.Id);
            builder.HasOne(x => x.QuotationItem).WithMany().HasForeignKey(x => x.QuotationItemId);
            builder.HasOne(x => x.RigSettings).WithMany().HasForeignKey(x => x.RigSettingsId);
            builder.Property(p => p.Status).IsRequired();
            builder.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById);
            builder.HasOne(x => x.ModifiedBy).WithMany().HasForeignKey(x => x.ModifiedById);
            builder.Property(p => p.Created).HasDefaultValueSql("getutcdate()");
            builder.Property(p => p.Modified).HasDefaultValueSql("getutcdate()");
        }
    }
}
