﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.UpgradeMatrix;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.UpgradeMatrix
{
    public class CcnItemConfiguration : IEntityTypeConfiguration<CcnItem>
    {
        public void Configure(EntityTypeBuilder<CcnItem> builder)
        {
            builder.ToTable("UpgradeMatrix_CcnItem");
            builder.HasKey(p => p.Id);
            builder.HasOne(x => x.RootCustomer).WithMany().HasForeignKey(x => x.RootCustomerId);
            builder.HasOne(x => x.EquipmentCategory).WithMany().HasForeignKey(x => x.EquipmentCategoryId);
            builder.Property(p => p.Description).HasMaxLength(5000).IsRequired();
            builder.Property(p => p.Comments).HasMaxLength(5000);
            builder.Property(p => p.PackageUpgrade).IsRequired();
            builder.Property(p => p.RigType).IsRequired();
            builder.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById);
            builder.HasOne(x => x.ModifiedBy).WithMany().HasForeignKey(x => x.ModifiedById);
            builder.Property(p => p.Created).HasDefaultValueSql("getutcdate()");
            builder.Property(p => p.Modified).HasDefaultValueSql("getutcdate()");
        }
    }
}
