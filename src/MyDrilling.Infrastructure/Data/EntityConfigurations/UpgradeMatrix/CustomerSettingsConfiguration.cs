﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.UpgradeMatrix;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.UpgradeMatrix
{
    public class CustomerSettingsConfiguration : IEntityTypeConfiguration<CustomerSettings>
    {
        public void Configure(EntityTypeBuilder<CustomerSettings> builder)
        {
            builder.ToTable("UpgradeMatrix_CustomerSettings");
            builder.HasKey(p=> p.Id);
            builder.HasOne(x => x.RootCustomer).WithMany().HasForeignKey(x => x.RootCustomerId);
            builder.Property(p => p.Key).HasMaxLength(100).IsRequired();
            builder.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById);
            builder.HasOne(x => x.ModifiedBy).WithMany().HasForeignKey(x => x.ModifiedById);
            builder.Property(p => p.Created).HasDefaultValueSql("getutcdate()");
            builder.Property(p => p.Modified).HasDefaultValueSql("getutcdate()");
            builder.HasIndex(p => new { p.RootCustomerId , p.Key}).IsUnique();
        }
    }
}
