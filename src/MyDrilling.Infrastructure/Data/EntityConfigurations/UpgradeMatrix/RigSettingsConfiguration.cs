﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.UpgradeMatrix;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.UpgradeMatrix
{
    public class RigSettingsConfiguration : IEntityTypeConfiguration<RigSettings>
    {
        public void Configure(EntityTypeBuilder<RigSettings> builder)
        {
            builder.ToTable("UpgradeMatrix_RigSettings");
            builder.HasKey(p => p.Id);
            builder.HasOne(x => x.Rig).WithMany().HasForeignKey(x => x.RigId);
            builder.Property(p => p.ShortName).HasMaxLength(20).IsRequired();
            builder.Property(p => p.RigType).IsRequired();
            builder.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById);
            builder.HasOne(x => x.ModifiedBy).WithMany().HasForeignKey(x => x.ModifiedById);
            builder.Property(p => p.Created).HasDefaultValueSql("getutcdate()");
            builder.Property(p => p.Modified).HasDefaultValueSql("getutcdate()");
            builder.HasIndex(p => p.ShortName).IsUnique();

        }
    }
}
