﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.UpgradeMatrix;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.UpgradeMatrix
{
    public class CcnStateConfiguration : IEntityTypeConfiguration<CcnState>
    {
        public void Configure(EntityTypeBuilder<CcnState> builder)
        {
            builder.ToTable("UpgradeMatrix_CcnState");
            builder.HasKey(p => p.Id);
            builder.HasOne(x => x.CcnItem).WithMany().HasForeignKey(x => x.CcnItemId);
            builder.HasOne(x => x.RigSettings).WithMany().HasForeignKey(x => x.RigSettingsId);
            builder.Property(p => p.Status).IsRequired();
            builder.Property(p => p.CcnName).HasMaxLength(20);
            builder.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById);
            builder.HasOne(x => x.ModifiedBy).WithMany().HasForeignKey(x => x.ModifiedById);
            builder.Property(p => p.Created).HasDefaultValueSql("getutcdate()");
            builder.Property(p => p.Modified).HasDefaultValueSql("getutcdate()");
        }
    }
}
