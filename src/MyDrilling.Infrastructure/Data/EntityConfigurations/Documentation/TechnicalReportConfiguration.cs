﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Documentation;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Documentation
{
    public class TechnicalReportConfiguration : IEntityTypeConfiguration<TechnicalReport>
    {
        public void Configure(EntityTypeBuilder<TechnicalReport> builder)
        {
            builder.ToTable("Documentation_TechnicalReports");
            builder.HasKey(p => p.Id);
            builder.Property(p => p.ReferenceId).HasMaxLength(255).IsRequired();
            builder.Property(p => p.FileName).HasMaxLength(255);
            builder.Property(p => p.Url).HasMaxLength(255);
            builder.Property(p => p.Author).HasMaxLength(30);
            builder.Property(p => p.RigReferenceId).IsRequired();
            builder.HasOne(x => x.Rig).WithMany().HasForeignKey(x => x.RigId);
            builder.Property(p => p.DocumentDate).HasDefaultValueSql("getutcdate()");
            builder.Property(p => p.Created).HasDefaultValueSql("getutcdate()");
            builder.HasIndex(x => x.DocumentDate);
        }
    }
}
