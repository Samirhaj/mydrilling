﻿using System;
using System.Security.Cryptography.X509Certificates;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Documentation;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Documentation
{
    public class UserManualConfiguration : IEntityTypeConfiguration<UserManual>
    {
        public void Configure(EntityTypeBuilder<UserManual> builder)
        {
            builder.ToTable("Documentation_UserManuals");
            builder.HasKey(p => p.Id);
            builder.Property(p => p.ReferenceId).HasMaxLength(60).IsRequired();
            builder.Property(p => p.RelatedReferenceId).HasMaxLength(60);
            builder.Property(p => p.Name).HasMaxLength(255);
            builder.Property(p => p.Discipline).HasMaxLength(50);
            builder.Property(p => p.DisciplineCode).HasMaxLength(5);
            builder.Property(p => p.FileName).HasMaxLength(255);
            builder.Property(p => p.Url).HasMaxLength(255);
            builder.Property(p => p.ProductCode).HasMaxLength(5);
            builder.Property(p => p.ProductName).HasMaxLength(50);
            builder.Property(p => p.RigReferenceId).IsRequired();
            builder.HasOne(x => x.Rig).WithMany().HasForeignKey(x => x.RigId);
            builder.Property(p => p.DocumentDate).HasDefaultValueSql("getutcdate()");
            builder.Property(p => p.Created).HasDefaultValueSql("getutcdate()");
            builder.HasIndex(x => x.DocumentDate);
            builder.HasIndex(x => new { x.RigId, x.DisciplineCode, x.IsMaster }).IncludeProperties(new string[] { "Discipline" });
        }
    }
}