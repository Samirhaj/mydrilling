﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations
{
    class UserSettingConfiguration : IEntityTypeConfiguration<UserSetting>
    {
        public void Configure(EntityTypeBuilder<UserSetting> builder)
        {
            builder.ToTable("UserSettings");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Key).HasMaxLength(500).IsRequired();
            builder.Property(x => x.Value).HasMaxLength(500).IsRequired();
            builder.HasIndex(x => new { x.UserId, x.Key }).IsUnique();
            builder.HasOne(x => x.User).WithMany(x => x.Settings).HasForeignKey(x => x.UserId);
        }
    }
}
