﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations
{
    class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Upn).HasMaxLength(User.MaxUpnLength).IsRequired();
            builder.HasIndex(x => x.Upn).IsUnique();
            builder.Property(x => x.ObjectId).HasMaxLength(255);
            builder.HasIndex(x => x.ObjectId).IsUnique();
            builder.Property(x => x.TenantId).HasMaxLength(255);
            builder.Property(x => x.FirstName).HasMaxLength(255);
            builder.Property(x => x.LastName).HasMaxLength(255);
            builder.Property(x => x.DisplayName).HasMaxLength(255);
            builder.Property(x => x.Position).HasMaxLength(255);
            builder.Property(x => x.PhoneNumber).HasMaxLength(255);
            builder.Property(x => x.MobileNumber).HasMaxLength(255);
            builder.Property(x => x.FaxNumber).HasMaxLength(255);
            builder.Property(x => x.Country).HasMaxLength(255);
            builder.Property(x => x.CountryCode).HasMaxLength(2);
            builder.Property(x => x.Image).HasMaxLength(255);
            builder.Property(x => x.ReferenceId).HasMaxLength(50);
            builder.HasOne(x => x.RootCustomer).WithMany().HasForeignKey(x => x.RootCustomerId);
            builder.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById);
            builder.HasOne(x => x.ModifiedBy).WithMany().HasForeignKey(x => x.ModifiedById);
            builder.HasMany(x => x.Settings).WithOne(x => x.User);
            builder.Metadata.FindNavigation(nameof(User.Settings)).SetPropertyAccessMode(PropertyAccessMode.Field);
            builder.HasMany(x => x.Profiles).WithOne(x => x.User);
            builder.Metadata.FindNavigation(nameof(User.Profiles)).SetPropertyAccessMode(PropertyAccessMode.Field);
            builder.HasMany(x => x.Roles).WithOne(x => x.User);
            builder.Metadata.FindNavigation(nameof(User.Roles)).SetPropertyAccessMode(PropertyAccessMode.Field);
            builder.HasMany(x => x.SubscriptionSettings).WithOne(x => x.User);
            builder.Metadata.FindNavigation(nameof(User.SubscriptionSettings)).SetPropertyAccessMode(PropertyAccessMode.Field);
            builder.HasMany(x => x.SubscriptionRigFilters).WithOne(x => x.User);
            builder.Metadata.FindNavigation(nameof(User.SubscriptionRigFilters)).SetPropertyAccessMode(PropertyAccessMode.Field);
            builder.HasMany(x => x.SubscriptionEnquiryTypeFilters).WithOne(x => x.User);
            builder.Metadata.FindNavigation(nameof(User.SubscriptionEnquiryTypeFilters)).SetPropertyAccessMode(PropertyAccessMode.Field);
            builder.HasMany(x => x.Settings).WithOne(x => x.User);
            builder.Metadata.FindNavigation(nameof(User.Settings)).SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}
