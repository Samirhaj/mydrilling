﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations
{
    class FunctionalLocationLogConfiguration : IEntityTypeConfiguration<FunctionalLocationLog>
    {
        public void Configure(EntityTypeBuilder<FunctionalLocationLog> builder)
        {
            builder.ToTable("FunctionalLocationLogs");
            builder.HasKey(x => x.Id);
            builder.Property(p => p.ProductCode).HasMaxLength(5);
            builder.HasOne(x => x.Rig).WithMany().HasForeignKey(x => x.RigId);
            builder.HasOne(x => x.Customer).WithMany().HasForeignKey(x => x.CustomerId);
            builder.HasOne(x => x.Equipment).WithMany().HasForeignKey(x => x.EquipmentId);
        }
    }
}
