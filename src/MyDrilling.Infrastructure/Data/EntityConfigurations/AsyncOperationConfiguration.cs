﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.AsyncOperation;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations
{
    class AsyncOperationConfiguration : IEntityTypeConfiguration<AsyncOperation>
    {
        public void Configure(EntityTypeBuilder<AsyncOperation> builder)
        {
            builder.ToTable("AsyncOperations");
            builder.HasKey(x => x.Id);
            builder.Property(p => p.Response).HasMaxLength(500);
            builder.HasOne(x => x.User).WithMany().HasForeignKey(x => x.UserId);
        }
    }
}
