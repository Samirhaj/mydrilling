﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations
{
    class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.ToTable("Customers");
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => x.ReferenceId).IsUnique();
            builder.Property(p => p.ReferenceId).HasMaxLength(50);
            builder.Property(p => p.Name).HasMaxLength(100).IsRequired();
            builder.OwnsOne(p => p.Address);
            builder.HasOne(p => p.RootCustomer).WithMany(x => x.Customers).HasForeignKey(x => x.RootCustomerId);
            builder.Property(p => p.Created).HasDefaultValueSql("getutcdate()");
        }
    }
}
