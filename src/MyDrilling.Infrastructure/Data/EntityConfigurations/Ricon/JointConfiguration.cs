﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Ricon;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Ricon
{
    public class JointConfiguration : IEntityTypeConfiguration<Joint>
    {
        public void Configure(EntityTypeBuilder<Joint> builder)
        {
            builder.ToTable("Ricon_Joints");
            builder.HasKey(p => p.Id);
            builder.HasOne(x => x.RootEquipment).WithMany().HasForeignKey(x => x.RootEquipmentId);
            builder.Property(p => p.ReferenceId).IsRequired();
            builder.Property(p => p.TagNumber).HasMaxLength(30);
            builder.Property(p => p.Fatigue).HasMaxLength(40);
            builder.HasIndex(p => new { p.RootEquipmentId, p.ReferenceId }).IsUnique();
            builder.Property(p => p.Created).HasDefaultValueSql("getutcdate()");
        }
    }
}
