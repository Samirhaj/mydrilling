﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Ricon;


namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Ricon
{
    public class RootEquipmentConfiguration : IEntityTypeConfiguration<RootEquipment>
    {
        public void Configure(EntityTypeBuilder<RootEquipment> builder)
        {
            builder.ToTable("Ricon_RootEquipments");
            builder.HasKey(p => p.Id);
            builder.Property(p => p.RigId).IsRequired();
            builder.HasOne(x => x.Rig).WithMany().HasForeignKey(x => x.RigId);
            builder.Property(p => p.ReferenceId).IsRequired();
            builder.HasIndex(p => new { p.RigId, p.ReferenceId }).IsUnique();
        }
    }
}
