﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Ricon;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Ricon
{
    public class DocumentConfiguration : IEntityTypeConfiguration<Document>
    {
        public void Configure(EntityTypeBuilder<Document> builder)
        {
            builder.ToTable("Ricon_Documents");
            builder.HasKey(p => p.Id);
            builder.HasOne(x => x.RootEquipment).WithMany().HasForeignKey(x => x.RootEquipmentId);
            builder.HasOne(x => x.Joint).WithMany().HasForeignKey(x => x.JointId);
            builder.Property(p => p.ReferenceId).IsRequired();
            builder.Property(p => p.Description).HasMaxLength(40);
            builder.Property(p => p.Version).HasMaxLength(2).HasDefaultValue("00");
            builder.Property(p => p.Url).HasMaxLength(50);
            builder.Property(p => p.PublishedDate).HasDefaultValueSql("getutcdate()");
            builder.Property(p => p.Created).HasDefaultValueSql("getutcdate()");
        }
    }
}
