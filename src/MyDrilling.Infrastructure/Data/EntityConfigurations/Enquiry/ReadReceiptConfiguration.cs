﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Enquiry;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Enquiry
{
    sealed class ReadReceiptConfiguration : IEntityTypeConfiguration<ReadReceipt>
    {
        public void Configure(EntityTypeBuilder<ReadReceipt> builder)
        {
            builder.ToTable("Enquiry_ReadReceipts");
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => new { x.EnquiryId, x.UserId }).IsUnique();
            builder.HasOne(x => x.Enquiry).WithMany().HasForeignKey(x => x.EnquiryId);
            builder.HasOne(x => x.User).WithMany().HasForeignKey(x => x.UserId);
        }
    }
}
