﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Enquiry;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Enquiry
{
    sealed class AttachmentConfiguration : IEntityTypeConfiguration<Attachment>
    {
        public void Configure(EntityTypeBuilder<Attachment> builder)
        {
            builder.ToTable("Enquiry_Attachments");
            builder.HasKey(x => x.Id);
            builder.Property(p => p.ReferenceId).HasMaxLength(50);
            builder.HasIndex(x => x.ReferenceId).IsUnique();
            builder.Property(p => p.BlobPath).HasMaxLength(500);
            builder.Property(p => p.FileName).HasMaxLength(255);
            builder.Property(p => p.FileExtension).HasMaxLength(10);
            builder.HasOne(x => x.Comment).WithMany(x => x.Attachments).HasForeignKey(x => x.CommentId);
            builder.HasOne(x => x.Enquiry).WithMany(x => x.Attachments).HasForeignKey(x => x.EnquiryId);
            builder.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById);
        }
    }
}
