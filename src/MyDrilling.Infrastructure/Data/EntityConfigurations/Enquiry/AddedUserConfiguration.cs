﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Enquiry;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Enquiry
{
    sealed class AddedUserConfiguration : IEntityTypeConfiguration<AddedUser>
    {
        public void Configure(EntityTypeBuilder<AddedUser> builder)
        {
            builder.ToTable("Enquiry_AddedUsers");
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => new { x.EnquiryId, x.UserId }).IsUnique();
            builder.HasOne(x => x.Enquiry).WithMany(x => x.AddedUsers).HasForeignKey(x => x.EnquiryId);
            builder.HasOne(x => x.User).WithMany().HasForeignKey(x => x.UserId);
            builder.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById);
        }
    }
}
