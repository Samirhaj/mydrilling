﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Enquiry;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Enquiry
{
    class CommentConfiguration : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.ToTable("Enquiry_Comments");
            builder.HasKey(x => x.Id);
            builder.Property(p => p.ReferenceId).HasMaxLength(50);
            builder.HasIndex(x => x.ReferenceId).IsUnique();
            builder.Property(p => p.Title).HasMaxLength(50);
            builder.Property(p => p.Text).HasMaxLength(8000);
            builder.HasOne(x => x.Enquiry).WithMany(x => x.Comments).HasForeignKey(x => x.EnquiryId);
            builder.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById); 
            builder.HasMany(x => x.Attachments).WithOne(x => x.Comment);
            builder.Metadata.FindNavigation(nameof(Comment.Attachments)).SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}
