﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Enquiry;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Enquiry
{
    sealed class StateLogConfiguration : IEntityTypeConfiguration<StateLog>
    {
        public void Configure(EntityTypeBuilder<StateLog> builder)
        {
            builder.ToTable("Enquiry_StateLogs");
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.Enquiry).WithMany(x => x.StateLogs).HasForeignKey(x => x.EnquiryId);
            builder.HasOne(x => x.ChangedBy).WithMany().HasForeignKey(x => x.ChangedById);
        }
    }
}
