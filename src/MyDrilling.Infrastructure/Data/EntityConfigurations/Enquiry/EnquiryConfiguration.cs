﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Enquiry
{
    class EnquiryConfiguration : IEntityTypeConfiguration<Core.Entities.Enquiry.Enquiry>
    {
        public void Configure(EntityTypeBuilder<Core.Entities.Enquiry.Enquiry> builder)
        {
            builder.ToTable("Enquiry_Enquiries");
            builder.HasKey(x => x.Id);
            builder.Property(p => p.ReferenceId).HasMaxLength(50);
            builder.HasIndex(x => x.ReferenceId).IsUnique();
            builder.Property(p => p.Title).HasMaxLength(Core.Entities.Enquiry.Enquiry.TitleMaxLength);
            builder.Property(p => p.OldGlobalId).HasMaxLength(255);
            builder.HasIndex(x => x.OldGlobalId);
            builder.Property(x => x.Version).IsConcurrencyToken();
            builder.Property(p => p.Description).HasMaxLength(Core.Entities.Enquiry.Enquiry.DescriptionMaxLength);
            builder.HasOne(x => x.Rig).WithMany().HasForeignKey(x => x.RigId);
            builder.HasOne(x => x.Equipment).WithMany().HasForeignKey(x => x.EquipmentId);
            builder.HasOne(x => x.Customer).WithMany().HasForeignKey(x => x.CustomerId);
            builder.HasOne(x => x.RigOwner).WithMany().HasForeignKey(x => x.RigOwnerId);
            builder.HasOne(x => x.ApprovedBy).WithMany().HasForeignKey(x => x.ApprovedById);
            builder.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById);
            builder.HasOne(x => x.ModifiedBy).WithMany().HasForeignKey(x => x.ModifiedById);
            builder.HasOne(x => x.AssignedBy).WithMany().HasForeignKey(x => x.AssignedById);
            builder.HasOne(x => x.AssignedTo).WithMany().HasForeignKey(x => x.AssignedToId);
            builder.HasOne(x => x.RejectedBy).WithMany().HasForeignKey(x => x.RejectedById);
            builder.HasOne(x => x.AssignedToCustomer).WithMany().HasForeignKey(x => x.AssignedToCustomerId);
            builder.HasOne(x => x.RootCustomer).WithMany().HasForeignKey(x => x.RootCustomerId);
            builder.HasOne(x => x.Supplier).WithMany().HasForeignKey(x => x.SupplierId);
            builder.HasMany(x => x.AddedUsers).WithOne(x => x.Enquiry);
            builder.Metadata.FindNavigation(nameof(Core.Entities.Enquiry.Enquiry.AddedUsers)).SetPropertyAccessMode(PropertyAccessMode.Field);
            builder.HasMany(x => x.Attachments).WithOne(x => x.Enquiry);
            builder.Metadata.FindNavigation(nameof(Core.Entities.Enquiry.Enquiry.Attachments)).SetPropertyAccessMode(PropertyAccessMode.Field);
            builder.HasMany(x => x.Comments).WithOne(x => x.Enquiry);
            builder.Metadata.FindNavigation(nameof(Core.Entities.Enquiry.Enquiry.Comments)).SetPropertyAccessMode(PropertyAccessMode.Field);
            builder.HasMany(x => x.StateLogs).WithOne(x => x.Enquiry);
            builder.Metadata.FindNavigation(nameof(Core.Entities.Enquiry.Enquiry.StateLogs)).SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}
