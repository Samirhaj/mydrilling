﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Enquiry;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Enquiry
{
    sealed class MessageTemplateConfiguration : IEntityTypeConfiguration<MessageTemplate>
    {
        public void Configure(EntityTypeBuilder<MessageTemplate> builder)
        {
            builder.ToTable("Enquiry_MessageTemplates");
            builder.HasKey(x => x.Id);
            builder.Property(p => p.Subject);
            builder.Property(p => p.Body);
        }
    }
}
