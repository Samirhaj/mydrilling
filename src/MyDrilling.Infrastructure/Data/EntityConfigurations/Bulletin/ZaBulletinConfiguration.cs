﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Bulletin;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Bulletin
{
    sealed class ZaBulletinConfiguration : IEntityTypeConfiguration<ZaBulletin>
    {
        public void Configure(EntityTypeBuilder<ZaBulletin> builder)
        {
            builder.ToTable("Bulletin_ZaBulletins");
            builder.HasKey(x => x.Id);
            builder.Property(b => b.ReferenceId).HasMaxLength(50).IsRequired();
            builder.HasIndex(b => b.ReferenceId).IsUnique();
            builder.Property(b => b.DocumentReferenceId).HasMaxLength(50);
            builder.Property(b => b.FileType).HasMaxLength(10);
            builder.HasOne(b => b.Type).WithMany().HasForeignKey(b => b.TypeId);
            builder.Property(p => p.Created).HasDefaultValueSql("getutcdate()");
        }
    }
}