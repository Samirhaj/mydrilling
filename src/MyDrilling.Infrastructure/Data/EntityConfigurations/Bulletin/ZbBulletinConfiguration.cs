﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Bulletin;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Bulletin
{
    sealed class ZbBulletinConfiguration : IEntityTypeConfiguration<ZbBulletin>
    {
        public void Configure(EntityTypeBuilder<ZbBulletin> builder)
        {
            builder.ToTable("Bulletin_ZbBulletins");
            builder.HasKey(x => x.Id);
            builder.Property(b => b.ReferenceId).HasMaxLength(50).IsRequired();
            builder.HasIndex(b => new {b.ReferenceId, b.Group}).IsUnique();
            builder.Property(b => b.Group).HasConversion<short>();
            builder.HasOne(b => b.ZaBulletin).WithMany().HasForeignKey(b => b.ZaBulletinId);
            builder.HasOne(b => b.Customer).WithMany().HasForeignKey(b => b.CustomerId);
            builder.HasOne(b => b.Rig).WithMany().HasForeignKey(b => b.RigId);
            builder.HasOne(b => b.Equipment).WithMany().HasForeignKey(b => b.EquipmentId);
            builder.HasOne(b => b.ConfirmedByUser).WithMany().HasForeignKey(b => b.ConfirmedByUserId);
            builder.Property(b => b.Status).HasConversion<short>();
            builder.Property(b => b.State).HasConversion<short>();
            builder.Property(p => p.Created).HasDefaultValueSql("getutcdate()");
        }
    }
}