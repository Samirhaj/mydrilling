﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Bulletin
{
    sealed class BulletinTypeConfiguration : IEntityTypeConfiguration<BulletinType>
    {
        public void Configure(EntityTypeBuilder<BulletinType> builder)
        {
            builder.ToTable("Bulletin_BulletinTypes");
            builder.HasKey(x => x.Id);
            builder.Property(t => t.Name).HasMaxLength(50).IsRequired();
            builder.Property(t => t.ConsolidatedName).HasMaxLength(50).IsRequired();
        }
    }
}