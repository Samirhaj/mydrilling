﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Permission;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Permission
{
    sealed class ProfileRoleConfiguration : IEntityTypeConfiguration<ProfileRole>
    {
        public void Configure(EntityTypeBuilder<ProfileRole> builder)
        {
            builder.ToTable("Permission_ProfileRoles");
            builder.HasKey(x => x.Id);
            builder.HasIndex(x => new {x.ProfileId, x.Role}).IsUnique();
            builder.HasOne(x => x.Profile).WithMany(x => x.ProfileRoles).HasForeignKey(x => x.ProfileId);
        }
    }
}
