﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Permission;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Permission
{
    sealed class UserProfileConfiguration : IEntityTypeConfiguration<UserProfile>
    {
        public void Configure(EntityTypeBuilder<UserProfile> builder)
        {
            builder.ToTable("Permission_UserProfiles");
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.Profile).WithMany().HasForeignKey(x => x.ProfileId);
            builder.HasOne(x => x.User).WithMany(x => x.Profiles).HasForeignKey(x => x.UserId);
            builder.HasOne(x => x.Rig).WithMany().HasForeignKey(x => x.RigId);
            builder.HasIndex(x => new { x.UserId, x.RigId }).IsUnique();
        }
    }
}
