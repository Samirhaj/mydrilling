﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Permission;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Permission
{
    sealed class UserRoleConfiguration : IEntityTypeConfiguration<UserRole>
    {
        public void Configure(EntityTypeBuilder<UserRole> builder)
        {
            builder.ToTable("Permission_UserRoles");
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.User).WithMany(x => x.Roles).HasForeignKey(x => x.UserId);
            builder.HasOne(x => x.Rig).WithMany().HasForeignKey(x => x.RigId);
            builder.HasOne(x => x.RootCustomer).WithMany().HasForeignKey(x => x.RootCustomerId);
        }
    }
}
