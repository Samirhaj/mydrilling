﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Permission;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Permission
{
    sealed class ProfileConfiguration : IEntityTypeConfiguration<Profile>
    {
        public void Configure(EntityTypeBuilder<Profile> builder)
        {
            builder.ToTable("Permission_Profiles");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Name).HasMaxLength(Profile.MaxNameLength).IsRequired();
            builder.HasMany(x => x.ProfileRoles).WithOne(x => x.Profile).HasForeignKey(x => x.ProfileId);
            builder.Metadata.FindNavigation(nameof(Profile.ProfileRoles)).SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}
