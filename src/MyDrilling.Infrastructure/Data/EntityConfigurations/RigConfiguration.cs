﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Rig;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations
{
    class RigConfiguration : IEntityTypeConfiguration<Rig>
    {
        public void Configure(EntityTypeBuilder<Rig> builder)
        {
            builder.ToTable("Rigs");
            builder.HasKey(x => x.Id);
            builder.Property(p => p.Name).HasMaxLength(50).IsRequired();
            builder.HasIndex(x => x.ReferenceId).IsUnique();
            builder.Property(p => p.OwnerReferenceId).HasMaxLength(50);
            builder.Property(p => p.Sector).HasMaxLength(50);
            builder.Property(p => p.Region).HasMaxLength(50);
            builder.Property(x => x.TypeCode).HasMaxLength(50);
            builder.OwnsOne(p => p.Coordinates);
            builder.HasOne(x => x.Owner).WithMany().HasForeignKey(x => x.OwnerId);
            builder.HasOne(x => x.RootOwner).WithMany().HasForeignKey(x => x.RootOwnerId);
            builder.HasOne(x => x.RootOperator).WithMany().HasForeignKey(x => x.RootOperatorId);
            builder.HasOne(x => x.ResponsibleUser).WithMany().HasForeignKey(x => x.ResponsibleUserId);
            builder.Property(p => p.Created).HasDefaultValueSql("getutcdate()");
        }
    }
}
