﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Sparepart
{
    public class SparepartConfiguration : IEntityTypeConfiguration<Core.Entities.Spareparts.Sparepart>
    {
        public void Configure(EntityTypeBuilder<Core.Entities.Spareparts.Sparepart> builder)
        {
            builder.ToTable("Sparepart_Spareparts");
            builder.HasKey(p => p.Id);
            builder.Property(p => p.MaterialNo).HasMaxLength(20);
            builder.Property(p => p.ReferenceId).HasMaxLength(20);
            builder.Property(p => p.Description).HasMaxLength(255);
            builder.Property(p => p.Created).HasDefaultValueSql("getutcdate()");
        }
    }
}
