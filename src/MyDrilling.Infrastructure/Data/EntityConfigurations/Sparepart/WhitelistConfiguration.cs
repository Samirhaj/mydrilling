﻿using MyDrilling.Core.Entities.Spareparts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Sparepart
{
    public class WhitelistConfiguration : IEntityTypeConfiguration<Whitelist>
    {
        public void Configure(EntityTypeBuilder<Whitelist> builder)
        {
            builder.ToTable("Sparepart_Whitelist");
            builder.HasKey(p => p.Id);
            builder.Property(p => p.GlobalId).HasMaxLength(25);
           
        }
    }
}
