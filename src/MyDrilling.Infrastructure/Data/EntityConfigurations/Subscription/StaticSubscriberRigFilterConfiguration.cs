﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Subscription;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Subscription
{
    sealed class StaticSubscriberRigFilterConfiguration : IEntityTypeConfiguration<StaticSubscriberRigFilter>
    {
        public void Configure(EntityTypeBuilder<StaticSubscriberRigFilter> builder)
        {
            builder.ToTable("Subscription_StaticSubscriberRigFilters");
            builder.HasKey(p => p.Id);
            builder.HasIndex(x => new { x.StaticSubscriberId, x.RigId }).IsUnique();
            builder.HasOne(x => x.Rig).WithMany().HasForeignKey(x => x.RigId);
            builder.HasOne(x => x.StaticSubscriber).WithMany(x => x.SubscriptionRigFilters).HasForeignKey(x => x.StaticSubscriberId);
        }
    }
}
