﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Subscription;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Subscription
{
    sealed class StaticSubscriberSettingConfiguration : IEntityTypeConfiguration<StaticSubscriberSetting>
    {
        public void Configure(EntityTypeBuilder<StaticSubscriberSetting> builder)
        {
            builder.ToTable("Subscription_StaticSubscriberSettings");
            builder.HasKey(p => p.Id);
            builder.HasIndex(x => new { x.StaticSubscriberId, x.SubscriptionType }).IsUnique();
            builder.HasOne(x => x.StaticSubscriber).WithMany(x => x.SubscriptionSettings).HasForeignKey(x => x.StaticSubscriberId);
        }
    }
}
