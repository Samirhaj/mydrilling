﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Subscription;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Subscription
{
    sealed class UserSettingConfiguration : IEntityTypeConfiguration<UserSetting>
    {
        public void Configure(EntityTypeBuilder<UserSetting> builder)
        {
            builder.ToTable("Subscription_UserSettings");
            builder.HasKey(p => p.Id);
            builder.HasIndex(x => new { x.UserId, x.SubscriptionType }).IsUnique();
            builder.HasOne(x => x.User).WithMany(x => x.SubscriptionSettings).HasForeignKey(x => x.UserId);
        }
    }
}
