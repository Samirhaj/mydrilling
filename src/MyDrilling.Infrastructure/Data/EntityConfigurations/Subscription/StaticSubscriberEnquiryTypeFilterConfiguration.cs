﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Subscription;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Subscription
{
    sealed class StaticSubscriberEnquiryTypeFilterConfiguration : IEntityTypeConfiguration<StaticSubscriberEnquiryTypeFilter>
    {
        public void Configure(EntityTypeBuilder<StaticSubscriberEnquiryTypeFilter> builder)
        {
            builder.ToTable("Subscription_StaticSubscriberEnquiryTypeFilters");
            builder.HasKey(p => p.Id);
            builder.HasIndex(x => new { x.StaticSubscriberId, x.Type }).IsUnique();
            builder.HasOne(x => x.StaticSubscriber).WithMany(x => x.SubscriptionEnquiryTypeFilters).HasForeignKey(x => x.StaticSubscriberId);
        }
    }
}
