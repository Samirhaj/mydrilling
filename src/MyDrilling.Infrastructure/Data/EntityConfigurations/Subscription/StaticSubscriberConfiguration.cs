﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Subscription;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Subscription
{
    sealed class StaticSubscriberConfiguration : IEntityTypeConfiguration<StaticSubscriber>
    {
        public void Configure(EntityTypeBuilder<StaticSubscriber> builder)
        {
            builder.ToTable("Subscription_StaticSubscribers");
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Email).HasMaxLength(255);
            builder.Property(p => p.FirstName).HasMaxLength(255);
            builder.Property(p => p.LastName).HasMaxLength(255);
            builder.HasMany(x => x.SubscriptionSettings).WithOne(x => x.StaticSubscriber);
            builder.Metadata.FindNavigation(nameof(StaticSubscriber.SubscriptionSettings)).SetPropertyAccessMode(PropertyAccessMode.Field);
            builder.HasMany(x => x.SubscriptionRigFilters).WithOne(x => x.StaticSubscriber);
            builder.Metadata.FindNavigation(nameof(StaticSubscriber.SubscriptionRigFilters)).SetPropertyAccessMode(PropertyAccessMode.Field);
            builder.HasMany(x => x.SubscriptionEnquiryTypeFilters).WithOne(x => x.StaticSubscriber);
            builder.Metadata.FindNavigation(nameof(StaticSubscriber.SubscriptionEnquiryTypeFilters)).SetPropertyAccessMode(PropertyAccessMode.Field);
        }
    }
}
