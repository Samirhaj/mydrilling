﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Subscription;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Subscription
{
    sealed class EnquiryTypeFilterConfiguration : IEntityTypeConfiguration<EnquiryTypeFilter>
    {
        public void Configure(EntityTypeBuilder<EnquiryTypeFilter> builder)
        {
            builder.ToTable("Subscription_EnquiryTypeFilters");
            builder.HasKey(p => p.Id);
            builder.HasIndex(x => new { x.UserId, x.Type }).IsUnique();
            builder.HasOne(x => x.User).WithMany(x => x.SubscriptionEnquiryTypeFilters).HasForeignKey(x => x.UserId);
        }
    }
}
