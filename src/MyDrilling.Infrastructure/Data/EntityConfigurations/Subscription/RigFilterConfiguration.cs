﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyDrilling.Core.Entities.Subscription;

namespace MyDrilling.Infrastructure.Data.EntityConfigurations.Subscription
{
    sealed class RigFilterConfiguration : IEntityTypeConfiguration<RigFilter>
    {
        public void Configure(EntityTypeBuilder<RigFilter> builder)
        {
            builder.ToTable("Subscription_RigFilters");
            builder.HasKey(p => p.Id);
            builder.HasIndex(x => new { x.UserId, x.RigId }).IsUnique();
            builder.HasOne(x => x.Rig).WithMany().HasForeignKey(x => x.RigId);
            builder.HasOne(x => x.User).WithMany(x => x.SubscriptionRigFilters).HasForeignKey(x => x.UserId);
        }
    }
}
