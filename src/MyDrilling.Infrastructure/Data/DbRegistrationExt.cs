﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MyDrilling.Infrastructure.Data
{
    public static class DbRegistrationExt
    {
        public static void AddMyDrillingDb(this IServiceCollection services, IConfiguration config)
        {
            var connectionString = config.GetConnectionString("MyDrillingDb");

            if (connectionString.IsManagedIdentityTokenRequired())
            {
                services.AddDbContext<MyDrillingDb>(x => x.UseSqlServer(connectionString)
                    .AddInterceptors(new ManagedIdentityConnectionInterceptor())
#if DEBUG
                    .EnableSensitiveDataLogging()
#endif
                );
            }
            else
            {
                services.AddDbContext<MyDrillingDb>(x => x.UseSqlServer(connectionString)
#if DEBUG
                    .EnableSensitiveDataLogging()
#endif
                );
            }
        }
    }
}
