﻿using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.AsyncOperation;
using MyDrilling.Core.Entities.Bulletin;
using MyDrilling.Core.Entities.Documentation;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Core.Entities.Equipment;
using MyDrilling.Core.Entities.Permission;
using MyDrilling.Core.Entities.Ricon;
using MyDrilling.Core.Entities.Rig;
using MyDrilling.Core.Entities.Spareparts;
using MyDrilling.Core.Entities.Subscription;
using MyDrilling.Core.Entities.UpgradeMatrix;
using MessageTemplate = MyDrilling.Core.Entities.Enquiry.MessageTemplate;

namespace MyDrilling.Infrastructure.Data
{
    public class MyDrillingDb : DbContext
    {
        public MyDrillingDb(DbContextOptions<MyDrillingDb> options) : base(options)
        { }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Equipment> Equipments { get; set; }
        public DbSet<FunctionalLocationLog> FunctionalLocationLogs { get; set; }
        public DbSet<Rig> Rigs { get; set; }
        public DbSet<RootCustomer> RootCustomers { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<AsyncOperation> AsyncOperations { get; set; }
        public DbSet<SapConnectorStatus> SapConnectorStatuses { get; set; }

        // ReSharper disable InconsistentNaming

        public DbSet<CustomerSettings> UpgradeMatrix_CustomerSettings { get; set; }
        public DbSet<RigSettings> UpgradeMatrix_RigSettings { get; set; }
        public DbSet<EquipmentCategory> UpgradeMatrix_EquipmentCategories { get; set; }
        public DbSet<CcnItem> UpgradeMatrix_CcnItems { get; set; }
        public DbSet<CcnState> UpgradeMatrix_CcnStates { get; set; }
        public DbSet<QuotationItem> UpgradeMatrix_QuotationItems { get; set; }
        public DbSet<QuotationState> UpgradeMatrix_QuotationStates { get; set; }
        public DbSet<Core.Entities.UpgradeMatrix.MessageTemplate> UpgradeMatrix_MessageTemplates { get; set; }

        public DbSet<Document> Ricon_Documents { get; set; }
        public DbSet<Joint> Ricon_Joints { get; set; }
        public DbSet<RootEquipment> Ricon_RootEquipments { get; set; }

        public DbSet<ZaBulletin> Bulletin_ZaBulletins { get; set; }
        public DbSet<ZbBulletin> Bulletin_ZbBulletins { get; set; }
        public DbSet<BulletinType> Bulletin_BulletinTypes { get; set; }

        public DbSet<Attachment> Enquiry_Attachments { get; set; }
        public DbSet<StateLog> Enquiry_StateLogs { get; set; }
        public DbSet<Enquiry> Enquiry_Enquiries { get; set; }
        public DbSet<Comment> Enquiry_Comments { get; set; }
        public DbSet<ReadReceipt> Enquiry_ReadReceipts { get; set; }
        public DbSet<AddedUser> Enquiry_AddedUsers { get; set; }
        public DbSet<MessageTemplate> Enquiry_MessageTemplates { get; set; }

        public DbSet<Sparepart> Sparepart_Spareparts { get; set; }
        public DbSet<Whitelist> Sparepart_WhiteList { get; set; }

        public DbSet<Profile> Permission_Profiles { get; set; }
        public DbSet<ProfileRole> Permission_ProfileRoles { get; set; }
        public DbSet<UserProfile> Permission_UserProfiles { get; set; }
        public DbSet<UserRole> Permission_UserRoles { get; set; }

        public DbSet<TechnicalReport> Documentation_TechnicalReports { get; set; }
        public DbSet<UserManual> Documentation_UserManuals { get; set; }
        
        public DbSet<EnquiryTypeFilter> Subscription_EnquiryTypeFilters { get; set; }
        public DbSet<RigFilter> Subscription_RigFilters { get; set; }
        public DbSet<StaticSubscriber> Subscription_StaticSubscribers { get; set; }
        public DbSet<StaticSubscriberSetting> Subscription_StaticSubscriberSettings { get; set; }
        public DbSet<StaticSubscriberRigFilter> Subscription_StaticSubscriberRigFilters { get; set; }
        public DbSet<StaticSubscriberEnquiryTypeFilter> Subscription_StaticSubscriberEnquiryTypeFilters { get; set; }
        public DbSet<Core.Entities.Subscription.UserSetting> Subscription_UserSettings { get; set; }
        public DbSet<Core.Entities.UserSetting> UserSettings { get; set; }

        // ReSharper restore InconsistentNaming

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            //disable "ON DELETE CASCADE"
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }
    }
}
