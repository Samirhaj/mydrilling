﻿using System;
using System.Data;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace MyDrilling.Infrastructure.Data
{
    /// <summary>
    /// Based on https://github.com/madelson/DistributedLock
    /// </summary>
    public static class SqlDistributedLockExt
    {
        public static async Task<IAsyncDisposable> AcquireDistributedLockAsync(this MyDrillingDb db, string lockKey)
        {
            var conn = await db.Database.GetDbConnection().ConnectionString.CreateRawSqlConnection();
            SqlDistributedLock @lock = null;

            try
            {
                using (var cmd = conn.CreateCommand())
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandText = "sp_getapplock";
                    cmd.Parameters.Add(new SqlParameter("Resource", lockKey));
                    cmd.Parameters.Add(new SqlParameter("LockMode", "Exclusive"));
                    cmd.Parameters.Add(new SqlParameter("LockOwner", "Session"));
                    cmd.Parameters.Add(new SqlParameter("LockTimeout", 50));

                    var returnCode = cmd.CreateParameter();
                    returnCode.Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add(returnCode);

                    await conn.OpenAsync();
                    await cmd.ExecuteNonQueryAsync();

                    ParseExitCode((int)returnCode.Value, lockKey);
                }

                //lock acquired
                @lock = new SqlDistributedLock(conn, lockKey);
                return @lock;
            }
            finally
            {
                if (@lock == null)
                {
                    //dispose connection in case of fail
                    await conn.DisposeAsync();
                }
            }
        }

        private sealed class SqlDistributedLock : IAsyncDisposable
        {
            private readonly string _lockKey;
            private readonly SqlConnection _conn;

            public SqlDistributedLock(SqlConnection conn, string lockKey)
            {
                _lockKey = lockKey;
                _conn = conn;
            }

            public async ValueTask DisposeAsync()
            {
                try
                {
                    using (var cmd = _conn.CreateCommand())
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandText = "sp_releaseapplock";
                        cmd.Parameters.Add(new SqlParameter("Resource", _lockKey));
                        cmd.Parameters.Add(new SqlParameter("LockOwner", "Session"));

                        var returnCode = cmd.CreateParameter();
                        returnCode.Direction = ParameterDirection.ReturnValue;
                        cmd.Parameters.Add(returnCode);
                        await cmd.ExecuteNonQueryAsync();

                        ParseExitCode((int)returnCode.Value, _lockKey);
                    }
                }
                finally
                {
                    //dispose connection when lock is released
                    await _conn.DisposeAsync();
                }
            }
        }

        /// <summary>
        /// https://docs.microsoft.com/en-us/sql/relational-databases/system-stored-procedures/sp-getapplock-transact-sql?redirectedfrom=MSDN&view=sql-server-ver15
        /// </summary>
        private static void ParseExitCode(int exitCode, string lockKey)
        {
            switch (exitCode)
            {
                case 0:
                case 1:
                    return;
                case -1:
                    throw new SqlDistributedLockException(exitCode, $"failed to acquire distributed lock, reason: timeout, lockKey: {lockKey}");
                case -2:
                    throw new SqlDistributedLockException(exitCode, $"failed to acquire distributed lock, reason: canceled, lockKey: {lockKey}");
                case -3:
                    throw new SqlDistributedLockException(exitCode, $"failed to acquire distributed lock, reason: deadlock, lockKey: {lockKey}");
                case -999:
                    throw new SqlDistributedLockException(exitCode, $"failed to acquire distributed lock, reason: parameter validation or other error, lockKey: {lockKey}");

                default:
                    if (exitCode <= 0)
                    {
                        throw new SqlDistributedLockException(exitCode, $"failed to acquire distributed lock, reason: unknown, lockKey: {lockKey}");
                    }

                    return;
            }
        }
    }

    public sealed class SqlDistributedLockException : Exception
    {
        public SqlDistributedLockException(int exitCode, string reason) : base($"{reason}, exitCode: {exitCode}")
        { }
    }
}
