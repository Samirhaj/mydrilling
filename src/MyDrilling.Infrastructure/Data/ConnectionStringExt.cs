﻿using System;
using System.Threading.Tasks;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.Data.SqlClient;

namespace MyDrilling.Infrastructure.Data
{
    public static class ConnectionStringExt
    {
        private const string Resource = "https://database.windows.net/";
        private static readonly AzureServiceTokenProvider TokenProvider = new AzureServiceTokenProvider();
        private const string CloudDatabasePattern = ".database.windows.net";
        private const string PasswordPattern = ";Password=";

        public static bool IsManagedIdentityTokenRequired(this string connectionString)
        {
            return connectionString.Contains(CloudDatabasePattern, StringComparison.InvariantCultureIgnoreCase) 
                   && !connectionString.Contains(PasswordPattern, StringComparison.InvariantCultureIgnoreCase);
        }

        public static async Task<SqlConnection> CreateRawSqlConnection(this string connectionString)
        {
            var conn = new SqlConnection(connectionString);
            if (conn.ConnectionString.IsManagedIdentityTokenRequired())
            {
                conn.AccessToken = await TokenProvider.GetAccessTokenAsync(Resource);
            }

            return conn;
        }
    }
}
