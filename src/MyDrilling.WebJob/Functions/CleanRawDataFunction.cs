﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.MessageContracts;

namespace MyDrilling.WebJob.Functions
{
    public class CleanRawDataFunction
    {
#if DEBUG
        private const bool RunOnStartup = true;
#else
        private const bool RunOnStartup = false;
#endif
        private readonly string _rawDataDbConnectionString;

        public CleanRawDataFunction(IConfiguration config)
        {
            _rawDataDbConnectionString = config.GetConnectionString("RawDataDb");
        }

        [FunctionName("MyDrilling_CleanRawDataFunction")]
        public async Task Process([TimerTrigger("0 0 8,15 * * *", RunOnStartup = RunOnStartup)]TimerInfo timerInfo, ILogger logger)
        {
            if (string.IsNullOrEmpty(_rawDataDbConnectionString))
            {
                logger.LogWarning("RawDataDb connection string is empty");
                return;
            }

            using (var sqlConnection = await _rawDataDbConnectionString.CreateRawSqlConnection())
            {
                var startTime = DateTime.UtcNow;
                try
                {
                    await sqlConnection.ExecuteAsync("EXEC dbo.CleanCharact", null, null, 600);
                    LogSpentTime(logger, startTime, "dbo.CleanCharact");
                }
                catch (Exception e)
                {
                    logger.LogError("Store procedure {sp} failed on execution, reason: {reason}", "dbo.CleanCharact", e.Message);
                }
                try
                {
                    startTime = DateTime.UtcNow;
                    await sqlConnection.ExecuteAsync("EXEC dbo.CleanCustomers", null, null, 600);
                    LogSpentTime(logger, startTime, "dbo.CleanCustomers");
                }
                catch (Exception e)
                {
                    logger.LogError("Store procedure {sp} failed on execution, reason: {reason}", "dbo.CleanCustomers", e.Message);
                }
                try
                {
                    startTime = DateTime.UtcNow;
                    await sqlConnection.ExecuteAsync("EXEC dbo.CleanEquipments", null, null, 600);
                    LogSpentTime(logger, startTime, "dbo.CleanEquipments");
                }
                catch (Exception e)
                {
                    logger.LogError("Store procedure {sp} failed on execution, reason: {reason}", "dbo.CleanEquipments", e.Message);
                }
                try
                {
                    startTime = DateTime.UtcNow;
                    await sqlConnection.ExecuteAsync("EXEC dbo.CleanFuncloc", null, null, 600);
                    LogSpentTime(logger, startTime, "dbo.CleanFuncloc");
                }
                catch (Exception e)
                {
                    logger.LogError("Store procedure {sp} failed on execution, reason: {reason}", "dbo.CleanFuncloc", e.Message);
                }
                try
                {
                    startTime = DateTime.UtcNow;
                    await sqlConnection.ExecuteAsync("EXEC dbo.CleanPartner", null, null, 600);
                    LogSpentTime(logger, startTime, "dbo.CleanPartner");
                }
                catch (Exception e)
                {
                    logger.LogError("Store procedure {sp} failed on execution, reason: {reason}", "dbo.CleanPartner", e.Message);
                }
                try
                {
                    startTime = DateTime.UtcNow;
                    await sqlConnection.ExecuteAsync("EXEC dbo.CleanRiconDocuments", null, null, 600);
                    LogSpentTime(logger, startTime, "dbo.CleanRiconDocuments");
                }
                catch (Exception e)
                {
                    logger.LogError("Store procedure {sp} failed on execution, reason: {reason}", "dbo.CleanRiconDocuments", e.Message);
                }
                try
                {
                    startTime = DateTime.UtcNow;
                    await sqlConnection.ExecuteAsync("EXEC dbo.CleanRigs", null, null, 600);
                    LogSpentTime(logger, startTime, "dbo.CleanRigs");
                }
                catch (Exception e)
                {
                    logger.LogError("Store procedure {sp} failed on execution, reason: {reason}", "dbo.CleanRigs", e.Message);
                }
                try
                {
                    startTime = DateTime.UtcNow;
                    await sqlConnection.ExecuteAsync("EXEC dbo.CleanSpareParts", null, null, 600);
                    LogSpentTime(logger, startTime, "dbo.CleanSpareParts");
                }
                catch (Exception e)
                {
                    logger.LogError("Store procedure {sp} failed on execution, reason: {reason}", "dbo.CleanSpareParts", e.Message);
                }
                try
                {
                    startTime = DateTime.UtcNow;
                    await sqlConnection.ExecuteAsync("EXEC dbo.CleanTechnicalReports", null, null, 600);
                    LogSpentTime(logger, startTime, "dbo.CleanTechnicalReports");
                }
                catch (Exception e)
                {
                    logger.LogError("Store procedure {sp} failed on execution, reason: {reason}", "dbo.CleanTechnicalReports", e.Message);
                }
                try
                {
                    startTime = DateTime.UtcNow;
                    await sqlConnection.ExecuteAsync("EXEC dbo.CleanUserManuals", null, null, 600);
                    LogSpentTime(logger, startTime, "dbo.CleanUserManuals");
                }
                catch (Exception e)
                {
                    logger.LogError("Store procedure {sp} failed on execution, reason: {reason}", "dbo.CleanUserManuals", e.Message);
                }
            }
        }

        private static void LogSpentTime(ILogger logger, DateTime startTime, string procName)
        {
            var timeSpentInSec = (DateTime.UtcNow - startTime).TotalSeconds;
            logger.LogInformation("Store procedure {sp} successfully finished, time spent(sec.): {timeSpent}", procName, timeSpentInSec);
        }
    }
}
