﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Infrastructure.Data;

namespace MyDrilling.WebJob.Functions
{
    public class RigConsistencyCheckFunction
    {
#if DEBUG
        private const bool RunOnStartup = true;
#else
        private const bool RunOnStartup = false;
#endif
        private readonly MyDrillingDb _db;

        public RigConsistencyCheckFunction(MyDrillingDb db)
        {
            _db = db;
        }

        [FunctionName("MyDrilling_RigConsistencyCheckFunction")]
        public async Task Process([TimerTrigger("0 0 */2 * * *", RunOnStartup = RunOnStartup)] TimerInfo timerInfo,
            ILogger logger)
        {
            await CheckForRigReferences(logger);
            await CheckForDeletedRigs(logger);
            await _db.SaveChangesAsync();
        }

        private async Task CheckForRigReferences(ILogger logger)
        {
            var rigsToCheck = await _db.Rigs
                .Where(x => !string.IsNullOrEmpty(x.OwnerReferenceId) && (x.Owner == null || x.Owner.ReferenceId != x.OwnerReferenceId))
                .ToArrayAsync();

            if (rigsToCheck.Length == 0)
            {
                return;
            }

            var customerMapping = await _db.Customers.Where(x => !string.IsNullOrEmpty(x.ReferenceId)).Select(x => new {x.Id, x.ReferenceId})
                .ToDictionaryAsync(x => x.ReferenceId, x => x.Id);

            var processedRigsCount = 0;
            foreach (var rig in rigsToCheck)
            {
                if (customerMapping.TryGetValue(rig.OwnerReferenceId, out var customerId))
                {
                    rig.OwnerId = customerId;
                    processedRigsCount++;

                    logger.LogInformation("set customer for rig {referenceId}", rig.ReferenceId);
                }
            }

            if (processedRigsCount > 0)
            {
                logger.LogInformation("set customer for {processedRigsCount} rigs", processedRigsCount);
            }
        }

        private async Task CheckForDeletedRigs(ILogger logger)
        {
            var rigsLatestUpdate = await _db.Rigs.MaxAsync(x => x.LastSeen);
            if (rigsLatestUpdate == DateTime.MinValue) return;

            //delete not updated ones
            var threshold = rigsLatestUpdate.AddDays(-2);
            var allOutdatedRigs = await _db.Rigs.Where(x => x.LastSeen <= threshold && !x.IsDeleted).ToArrayAsync();
            foreach (var oRig in allOutdatedRigs)
            {
                oRig.IsDeleted = true;
                oRig.Modified = DateTime.UtcNow;
                logger.LogInformation("Rig {referenceId} has been deleted.", oRig.ReferenceId);
            }

            //reactivate deleted ones
            var allReactivatedRigs = await _db.Rigs.Where(x => x.LastSeen == rigsLatestUpdate && x.IsDeleted).ToArrayAsync();
            foreach (var rRig in allReactivatedRigs)
            {
                rRig.IsDeleted = false;
                rRig.Modified = DateTime.UtcNow;
                logger.LogInformation("Rig {referenceId} has been reactivated.", rRig.ReferenceId);
            }
        }
    }
}
