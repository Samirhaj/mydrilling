﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Azure.Search.Documents.Indexes;
using Azure.Search.Documents.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Bulletin;
using MyDrilling.Core.Entities.Documentation;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.Services.Search;

namespace MyDrilling.WebJob.Functions
{
    public class SearchIndexDeltaFunction
    {
#if DEBUG
        private const bool RunOnStartup = true;
#else
        private const bool RunOnStartup = false;
#endif
        private readonly string _searchServiceName;
        private readonly string _searchApiKey;
        private readonly MyDrillingDb _db;

        public SearchIndexDeltaFunction(MyDrillingDb db, IConfiguration config)
        {
            _db = db;
            _searchServiceName = config.GetValue<string>("SearchServiceName");
            _searchApiKey = config.GetValue<string>("SearchApiKey");
        }

        [FunctionName("MyDrilling_SearchIndexDeltaFunction")]
        public async Task Process([TimerTrigger("0 0 */2 * * *", RunOnStartup = RunOnStartup)]TimerInfo timerInfo, ILogger logger)
        {
            try
            {
                var searchService = new SearchService(_searchServiceName, _searchApiKey);
                SearchIndexClient idxClient = searchService.GetSearchIndexClient();
                var searchIndex = idxClient.GetIndexAsync(searchService.IndexName);
                if (searchIndex == null)
                {
                    return;
                }

                var startTime = DateTime.UtcNow;
                logger.LogInformation("Starting delta index...time: {time}", startTime);
                // Load enquiries documents
                var enquiries = await _db.Enquiry_Enquiries
                    .Where(x => x.RigId > 0 && x.Type != TypeInfo.Type.Internal_Internal && !x.IsDeleted 
                                && (x.Created.AddHours(12) >= startTime || x.Modified.AddHours(12) >= startTime))
                    .Select(x => IndexDocumentsAction.MergeOrUpload(x.ToSearchItem())).ToArrayAsync();
                await searchService.IndexDocuments(enquiries);
                logger.LogInformation("Finished delta indexing enquiries - num of indexed {numOfItems}", enquiries.Length);

                // Load bulletins documents
                var zbBulletins = await _db.Bulletin_ZbBulletins
                    .Where(x => x.Created.AddHours(12) >= startTime || x.Modified.HasValue && x.Modified.Value.AddHours(12) >= startTime)
                    .Include(b => b.Rig)
                    .ToArrayAsync();
                var groupedBulletins = zbBulletins
                    .GroupBy(b => new { b.ZaBulletinId, RigId = b.RigId ?? default, RigName = b.Rig?.Name })
                    .Select(b => new { b.Key.ZaBulletinId, RigId = b.Key.RigId, RigName = b.Key.RigName, AllBulletins = b.ToArray() }).ToArray();
                var zaBulletins = await _db.Bulletin_ZaBulletins.Include(b => b.Type).ToArrayAsync();
                var bulletinEntries = groupedBulletins
                    .Join(zaBulletins, zb => zb.ZaBulletinId, za => za.Id, (zb, za) => IndexDocumentsAction.Upload(
                        new SearchItem
                        {
                            Id = nameof(ZaBulletin) + SearchItemExt.IdSeparator + za.Id + SearchItemExt.IdSeparator + zb.RigId,
                            EntityId = za.Id,
                            EntityType = nameof(ZaBulletin),
                            RigId = zb.RigId.ToString(),
                            Name = za.Title,
                            Description = za.Description,
                            ReferenceId = za.ReferenceId
                        })).ToArray();
                await searchService.IndexDocuments(bulletinEntries);
                logger.LogInformation("Finished delta indexing bulletins - num of indexed {numOfItems}", bulletinEntries.Length);

                // Load technical reports documents
                var technicalReports = await _db.Documentation_TechnicalReports
                    .Include(x => x.Rig)
                    .Where(x => x.RigId.HasValue && x.RigId.Value > 0 && x.RigId.HasValue && x.Rig.ImportDocumentation 
                                && (x.Created.AddHours(12) >= startTime || x.Modified.HasValue && x.Modified.Value.AddHours(12) >= startTime))
                    .Select(x => IndexDocumentsAction.MergeOrUpload(x.ToSearchItem())).ToArrayAsync();
                await searchService.IndexDocuments(technicalReports);
                logger.LogInformation("Finished delta indexing technical reports - num of indexed {numOfItems}", technicalReports.Length);

                // Load user manuals documents
                var userManuals = await _db.Documentation_UserManuals
                    .Include(x => x.Rig)
                    .Where(x => x.RigId.HasValue && x.RigId.Value > 0 && x.RigId.HasValue && x.Rig.ImportDocumentation
                                && (x.Created.AddHours(12) >= startTime || x.Modified.HasValue && x.Modified.Value.AddHours(12) >= startTime))
                    .Select(x => IndexDocumentsAction.MergeOrUpload(x.ToSearchItem())).ToArrayAsync();
                await searchService.IndexDocuments(userManuals);
                logger.LogInformation("Finished delta indexing user manuals - num of indexed {numOfItems}", enquiries.Length);

                logger.LogInformation("Delta indexing successfully finished...time spent(sec): {time}", (DateTime.UtcNow - startTime).TotalSeconds);
            }
            catch (Exception e)
            {
                logger.LogError(e, "Delta indexing failed, reason: {reason}", e.Message);
            }
        }
    }
}
