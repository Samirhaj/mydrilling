﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Infrastructure.Data;

namespace MyDrilling.WebJob.Functions
{
    public class DocumentationConsistencyCheckFunction
    {
#if DEBUG
        private const bool RunOnStartup = true;
#else
        private const bool RunOnStartup = false;
#endif
        private readonly MyDrillingDb _db;
        public DocumentationConsistencyCheckFunction(MyDrillingDb db)
        {
            _db = db;
        }

        [FunctionName("MyDrilling_DocumentationConsistencyCheckFunction")]
        public async Task Process([TimerTrigger("0 0 */2 * * *", RunOnStartup = false)] TimerInfo timerInfo,
            ILogger logger)
        {
            if (await CheckDocumentationForReferences())
            {
                await _db.SaveChangesAsync();
            }
        }

        private async Task<bool> CheckDocumentationForReferences()
        {
            var userManualsToCheck = await _db.Documentation_UserManuals
                .Where(x => x.RigReferenceId > 0 && (!x.RigId.HasValue || x.Rig.ReferenceId != x.RigReferenceId))
                .ToArrayAsync();

            var technicalReportsToCheck = await _db.Documentation_TechnicalReports
                .Where(x => x.RigReferenceId > 0 && !x.RigId.HasValue || x.Rig.ReferenceId != x.RigReferenceId)
                .ToArrayAsync();

            if (userManualsToCheck.Length == 0 && technicalReportsToCheck.Length == 0)
            {
                return false;
            }

            var rigsMapping = await _db.Rigs.Select(x => new {x.Id, x.ReferenceId})
                .ToDictionaryAsync(x => x.ReferenceId, x => x.Id);

            foreach (var userManual in userManualsToCheck)
            {
                if (rigsMapping.TryGetValue(userManual.RigReferenceId, out var rigId))
                {
                    userManual.RigId = rigId;
                }
            }

            foreach (var technicalReport in technicalReportsToCheck)
            {
                if (rigsMapping.TryGetValue(technicalReport.RigReferenceId, out var rigId))
                {
                    technicalReport.RigId = rigId;
                }
            }

            return true;
        }

    }
}
