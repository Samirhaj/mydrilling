﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Infrastructure.Data;

namespace MyDrilling.WebJob.Functions
{
    public class RiconConsistencyCheckFunction
    {
#if DEBUG
        private const bool RunOnStartup = true;
#else
        private const bool RunOnStartup = false;
#endif
        private readonly MyDrillingDb _db;
        public RiconConsistencyCheckFunction(MyDrillingDb db)
        {
            _db = db;
        }

        [FunctionName("MyDrilling_RiconConsistencyCheckFunction")]
        public async Task Process([TimerTrigger("0 0 */2 * * *", RunOnStartup = RunOnStartup)] TimerInfo timerInfo,
            ILogger logger)
        {
            await CheckForDocumentReferences();
            await CheckForDeletedJoints(logger);
            await CheckForDeletedDocuments(logger);
            await _db.SaveChangesAsync();
        }

        private async Task CheckForDeletedJoints(ILogger logger)
        {
            var jointsLatestUpdate = await _db.Ricon_Joints.MaxAsync(x => x.LastSeen);
            if (jointsLatestUpdate == DateTime.MinValue) return;

            //delete not updated ones
            var threshold = jointsLatestUpdate.AddDays(-2);
            var allOutdatedJoints = await _db.Ricon_Joints.Where(x => x.LastSeen <= threshold).ToArrayAsync();
            foreach (var oJoint in allOutdatedJoints)
            {
                oJoint.IsDeleted = true;
                oJoint.Modified = DateTime.UtcNow;
                logger.LogInformation("Ricon Joint {referenceId} has been deleted.", oJoint.ReferenceId);

            }

            //reactivate deleted ones
            var allReactivatedJoints = await _db.Ricon_Joints.Where(x => x.LastSeen == jointsLatestUpdate && x.IsDeleted).ToArrayAsync();
            foreach (var rJoint in allReactivatedJoints)
            {
                rJoint.IsDeleted = false;
                rJoint.Modified = DateTime.UtcNow;
                logger.LogInformation("Ricon Joint {referenceId} has been reactivated.", rJoint.ReferenceId);

            }
        }

        private async Task CheckForDeletedDocuments(ILogger logger)
        {
            var jointsLatestUpdate = await _db.Ricon_Documents.MaxAsync(x => x.LastSeen);
            if (jointsLatestUpdate == DateTime.MinValue) return;

            //delete not updated ones
            var threshold = jointsLatestUpdate.AddDays(-2);
            var allOutdatedDocuments = await _db.Ricon_Documents.Where(x => x.LastSeen <= threshold && !x.IsDeleted).ToArrayAsync();
            foreach (var oDocument in allOutdatedDocuments)
            {
                oDocument.IsDeleted = true;
                oDocument.Modified = DateTime.UtcNow;
                logger.LogInformation("Ricon Document {referenceId} has been deleted.", oDocument.ReferenceId);

            }

            //reactivate deleted ones
            var allReactivatedDocuments = await _db.Ricon_Documents.Where(x => x.LastSeen == jointsLatestUpdate && x.IsDeleted).ToArrayAsync();
            foreach (var rDocument in allReactivatedDocuments)
            {
                rDocument.IsDeleted = false;
                rDocument.Modified = DateTime.UtcNow;
                logger.LogInformation("Ricon Document {referenceId} has been reactivated.", rDocument.ReferenceId);

            }
        }

        private async Task CheckForDocumentReferences()
        {
            var riconDocumentsToCheck = await _db.Ricon_Documents
                .Where(x => !x.RootEquipmentId.HasValue 
                            && x.JointReferenceId.HasValue 
                            && x.JointReferenceId > 0 
                            && (!x.JointId.HasValue || x.Joint.ReferenceId != x.JointReferenceId))
                .ToArrayAsync();

            if (riconDocumentsToCheck.Length == 0)
            {
                return;
            }

            var jointsMapping = await _db.Ricon_Joints.Select(x => new {x.Id, x.ReferenceId})
                .ToDictionaryAsync(x => x.ReferenceId, x => x.Id);

            foreach (var riconDoc in riconDocumentsToCheck)
            {
                if (riconDoc.JointReferenceId.HasValue &&
                    jointsMapping.TryGetValue((long) riconDoc.JointReferenceId, out var jointId))
                {
                    riconDoc.JointId = jointId;
                }
            }

            await _db.SaveChangesAsync();
        }
    }
}
