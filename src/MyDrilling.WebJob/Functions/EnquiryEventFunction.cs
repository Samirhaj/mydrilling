﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using DotLiquid;
using Microsoft.Azure.WebJobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Core.Entities.Permission;
using MyDrilling.Core.Entities.Subscription;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.MessageContracts;
using MyDrilling.Infrastructure.Storage;

namespace MyDrilling.WebJob.Functions
{
    /// <summary>
    /// Based on [MD_DWH_MyDrilling].[dbo].[Notify_Me_Get_Subscription_Data]
    /// </summary>
    public class EnquiryEventFunction
    {
        private readonly MyDrillingDb _db;
        private readonly IConfiguration _config;

        public EnquiryEventFunction(MyDrillingDb db,
            IConfiguration config)
        {
            _db = db;
            _config = config;
        }

        [FunctionName("MyDrilling_EnquiryEventFunction")]
        public async Task ProcessQueueMessage([QueueTrigger(StorageConfig.EnquiryEventQueue, Connection = "AzureWebJobsStorage")] string message,
            [Queue(StorageConfig.EmailQueue, Connection = "AzureWebJobsStorage")] IAsyncCollector<string> emailOutput,
            ILogger logger)
        {
            var msg = JsonSerializer.Deserialize<EnquiryEventMessage>(message);

            logger.LogInformation("new event: {enquiryEvent}, {enquiryId}", (msg.EnquiryEventType).ToString(), msg.EnquiryId);

            var emails = await GenerateEmails(msg);
            logger.LogInformation("generated " + emails.Length + " emails for: {enquiryEvent}, {enquiryId}", (msg.EnquiryEventType).ToString(), msg.EnquiryId);
            if (emails.Length == 0) return;

            var emailTasks = new Task[emails.Length];
            for (var i = 0; i < emails.Length; i++)
            {
                await Task.Delay(TimeSpan.FromSeconds(3));
                emailTasks[i] = emailOutput.AddAsync(JsonSerializer.Serialize(emails[i]));
            }

            await Task.WhenAll(emailTasks);
        }

        private async Task<EmailMessage[]> GenerateEmails(EnquiryEventMessage msg)
        {
            var enquiry = await _db.Enquiry_Enquiries
                .Include(x => x.Rig)
                .Include(x => x.Equipment)
                .Include(x => x.AssignedTo)
                .Include(x => x.CreatedBy)
                .Include(x => x.Comments)
                .Include(x => x.Attachments)
                .Include(x => x.StateLogs)
                .SingleAsync(x => x.Id == msg.EnquiryId);

            var contentDetails = string.Empty;
            var users = Array.Empty<Recipient>();
            var staticSubscribers = Array.Empty<Recipient>();

            switch (msg.EnquiryEventType)
            {
                case EnquiryEventType.EnquiryDraftDeleted:

                    users = await _db.Users
                        .Where(x => x.Id == enquiry.CreatedById)
                        .WithRigRole(RoleInfo.Role.EnquiryHandlingPreparationViewer, enquiry.RigId)
                        .WithCustomerRole(RoleInfo.Role.CustomerAccess, enquiry.RootCustomerId)
                        .WithSubscription(SubscriptionType.DraftEnquiryCreatedByMeDeleted)
                        .Where(u => !string.IsNullOrEmpty(u.Email))
                        .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName=  x.LastName})
                        .ToArrayAsync();

                    staticSubscribers = await _db.Subscription_StaticSubscribers
                        .WithSubscription(SubscriptionType.DraftEnquiryCreatedByMeDeleted)
                        .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName })
                        .ToArrayAsync();

                    break;

                case EnquiryEventType.EnquiryAssigned:

                    users = await _db.Users
                        .Where(x => x.Id == enquiry.AssignedToId)
                        .WithRigRole(RoleInfo.Role.EnquiryHandlingApprovedViewer, enquiry.RigId)
                        .WithCustomerRole(RoleInfo.Role.CustomerAccess, enquiry.RootCustomerId)
                        .WithSubscription(SubscriptionType.EnquiryAssignedToMe)
                        .Where(u => !string.IsNullOrEmpty(u.Email))
                        .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName })
                        .ToArrayAsync();

                    staticSubscribers = await _db.Subscription_StaticSubscribers
                        .Where(x => x.Email == enquiry.AssignedTo.Email)
                        .WithSubscription(SubscriptionType.EnquiryAssignedToMe)
                        .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName })
                        .ToArrayAsync();

                    break;

                case EnquiryEventType.EnquiryDraftAssigned:

                    users = await _db.Users
                        .Where(x => x.Id == enquiry.AssignedToId)
                        .WithRigRole(RoleInfo.Role.EnquiryHandlingPreparationViewer, enquiry.RigId)
                        .WithCustomerRole(RoleInfo.Role.CustomerAccess, enquiry.RootCustomerId)
                        .WithSubscription(SubscriptionType.EnquiryAssignedToMe)
                        .Where(u => !string.IsNullOrEmpty(u.Email))
                        .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName })
                        .ToArrayAsync();

                    break;

                case EnquiryEventType.EnquiryRigMovedCreated:

                    users = await _db.Users
                        .WithRigRole(RoleInfo.Role.EnquiryHandlingApprovedViewer, enquiry.RigId)
                        .WithCustomerRole(RoleInfo.Role.CustomerAccess, enquiry.RootCustomerId)
                        .WithSubscription(SubscriptionType.EnquiryRigMovedCreated)
                        .WithRigSubscription(enquiry.RigId)
                        .Where(u => !string.IsNullOrEmpty(u.Email))
                        .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName })
                        .ToArrayAsync();

                    staticSubscribers = await _db.Subscription_StaticSubscribers
                        .WithSubscription(SubscriptionType.EnquiryRigMovedCreated)
                        .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName })
                        .ToArrayAsync();

                    break;

                case EnquiryEventType.EnquiryRigDowntimeCreated:

                    users = await _db.Users
                        .WithRigRole(RoleInfo.Role.EnquiryHandlingApprovedViewer, enquiry.RigId)
                        .WithCustomerRole(RoleInfo.Role.CustomerAccess, enquiry.RootCustomerId)
                        .WithSubscription(SubscriptionType.EnquiryRigOnDowntimeCreated)
                        .WithRigSubscription(enquiry.RigId)
                        .Where(u => !string.IsNullOrEmpty(u.Email))
                        .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName })
                        .ToArrayAsync();

                    staticSubscribers = await _db.Subscription_StaticSubscribers
                        .WithSubscription(SubscriptionType.EnquiryRigOnDowntimeCreated)
                        .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName })
                        .ToArrayAsync();

                    break;

                case EnquiryEventType.EnquiryNewCommentAdded:

                    var commentId = msg.Details != null 
                                    && msg.Details.TryGetValue(EnquiryEventMessage.DetailKeys.CommentId, out var commentIdString)
                                    && long.TryParse(commentIdString, out var commentIdParsed)
                            ? commentIdParsed
                            : enquiry.Comments.OrderBy(x => x.Created).Last().Id;
                    contentDetails = enquiry.Comments.First(x => x.Id == commentId).Text;

                    var involvedUserIds = enquiry.GetInvolvedUserIds();
                    var involvedUserEmails = await _db.Users.Where(x => involvedUserIds.Contains(x.Id))
                        .Select(x => x.Email)
                        .ToArrayAsync();

                    var usersQuery = _db.Users
                        .WithRigRole(RoleInfo.Role.EnquiryHandlingApprovedViewer, enquiry.RigId)
                        .WithCustomerRole(RoleInfo.Role.CustomerAccess, enquiry.RootCustomerId)
                        .WithSubscription(SubscriptionType.EnquiryCommentAdded)
                        .Where(u => !string.IsNullOrEmpty(u.Email));

                    var involvedUsers = await usersQuery
                        .WithSubscription(SubscriptionType.EnquiryIAmInvolvedInCommentAdded)
                        .Where(x => involvedUserIds.Contains(x.Id))
                        .WithRigSubscription(enquiry.RigId)
                        .WithEnquiryTypeSubscription(enquiry.Type)
                        .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName })
                        .ToListAsync();

                    var usersSubscribedForRigOnDowntimeComments = enquiry.Type == TypeInfo.Type.Support_RigOnDowntime
                        ? await usersQuery
                            .WithSubscription(SubscriptionType.EnquiryRigOnDowntimeCommentAdded)
                            .WithRigSubscription(enquiry.RigId)
                            .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName })
                            .ToArrayAsync()
                        : Array.Empty<Recipient>();

                    var usersSubscribedByRigAndEnquiryType = await usersQuery
                        .WithRigSubscription(enquiry.RigId)
                        .WithEnquiryTypeSubscription(enquiry.Type)
                        .WithoutSubscription(SubscriptionType.EnquiryIAmInvolvedInCommentAdded)
                        .WithoutSubscription(SubscriptionType.EnquiryRigOnDowntimeCommentAdded)
                        .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName })
                        .ToListAsync();

                    involvedUsers.AddRange(usersSubscribedForRigOnDowntimeComments);
                    involvedUsers.AddRange(usersSubscribedByRigAndEnquiryType);
                    users = involvedUsers.GroupBy(x => x.Email).Select(x => x.First()).ToArray();

                    var staticSubscribersQuery = _db.Subscription_StaticSubscribers
                        .WithSubscription(SubscriptionType.EnquiryCommentAdded);

                    var staticSubscribersByEnquiryType = await staticSubscribersQuery
                        .WithEnquiryTypeSubscription(enquiry.Type)
                        .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName })
                        .ToListAsync();

                    var involvedStaticSubscribers = await staticSubscribersQuery
                        .WithSubscription(SubscriptionType.EnquiryIAmInvolvedInCommentAdded)
                        .Where(x => involvedUserEmails.Contains(x.Email))
                        .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName })
                        .ToListAsync();

                    staticSubscribersByEnquiryType.AddRange(involvedStaticSubscribers);
                    staticSubscribers = staticSubscribersByEnquiryType.GroupBy(x => x.Email).Select(x => x.First()).ToArray();

                    break;

                case EnquiryEventType.EnquiryAssignedToRig:

                    users = await _db.Users
                        .WithRigRole(RoleInfo.Role.EnquiryHandlingApprover, enquiry.RigId)
                        .WithCustomerRole(RoleInfo.Role.CustomerAccess, enquiry.RootCustomerId)
                        .WithSubscription(SubscriptionType.EnquiryAssignedToMyOrganization)
                        .WithRigSubscription(enquiry.RigId)
                        .WithEnquiryTypeSubscription(enquiry.Type)
                        .Where(u => !string.IsNullOrEmpty(u.Email))
                        .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName })
                        .ToArrayAsync();

                    staticSubscribers = await _db.Subscription_StaticSubscribers
                        .WithSubscription(SubscriptionType.EnquiryAssignedToMyOrganization)
                        .WithEnquiryTypeSubscription(enquiry.Type)
                        .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName })
                        .ToArrayAsync();

                    break;

                case EnquiryEventType.EnquiryDraftAssignedToRig:

                    users = await _db.Users
                        .WithRigRole(RoleInfo.Role.EnquiryHandlingApprover, enquiry.RigId)
                        .WithCustomerRole(RoleInfo.Role.CustomerAccess, enquiry.RootCustomerId)
                        .Where(x => x.RootCustomerId!= null && x.RootCustomerId == enquiry.CreatedBy.RootCustomerId)
                        .WithSubscription(SubscriptionType.EnquiryAssignedToMyOrganization)
                        .WithRigSubscription(enquiry.RigId)
                        .WithEnquiryTypeSubscription(enquiry.Type)
                        .Where(u => !string.IsNullOrEmpty(u.Email))
                        .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName })
                        .ToArrayAsync();

                    break;

                case EnquiryEventType.EnquiryAssignedToProvider:

                    users = await _db.Users
                        .WithRigRole(RoleInfo.Role.EnquiryHandlingProvider, enquiry.RigId)
                        .WithCustomerRole(RoleInfo.Role.CustomerAccess, enquiry.RootCustomerId)
                        .WithSubscription(SubscriptionType.EnquiryAssignedToMyOrganization)
                        .WithRigSubscription(enquiry.RigId)
                        .WithEnquiryTypeSubscription(enquiry.Type)
                        .Where(u => !string.IsNullOrEmpty(u.Email))
                        .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName })
                        .ToArrayAsync();

                    staticSubscribers = await _db.Subscription_StaticSubscribers
                        .WithSubscription(SubscriptionType.EnquiryAssignedToMyOrganization)
                        .WithEnquiryTypeSubscription(enquiry.Type)
                        .Select(x => new Recipient { Email = x.Email, FirstName = x.FirstName, LastName = x.LastName })
                        .ToArrayAsync();

                    break;
            }

            if (users.Length == 0 && staticSubscribers.Length == 0)
            {
                return Array.Empty<EmailMessage>();
            }
            
            var template = await _db.Enquiry_MessageTemplates.SingleAsync(x => x.EnquiryEventType == msg.EnquiryEventType);
            var subjectTemplate = Template.Parse(template.Subject);
            var bodyTemplate = Template.Parse(template.Body);
            var emails = new List<EmailMessage>(users.Length + staticSubscribers.Length);

            EmailMessage CreateEmail(Recipient recipient)
            {
                var templateValues = Hash.FromAnonymousObject(
                    new
                    {
                        SubscriberFirstName = recipient.FirstName,
                        SubscriberLastName = recipient.LastName,
                        SubscriberEmail = recipient.Email,
                        ContentDescription = enquiry.Description,
                        ContentDetails = contentDetails,
                        ContentEquipmentID = enquiry.Equipment?.ReferenceId.ToString(),
                        ContentEquipmentName = enquiry.Equipment?.Name,
                        ContentReferenceID = enquiry.ReferenceId,
                        ContentRigName = enquiry.Rig.Name,
                        ContentSubject = "", //not used
                        ContentTitle = enquiry.Title,
                        ContentCreatedByFirstName = enquiry.CreatedBy.FirstName,
                        ContentCreatedByLastName = enquiry.CreatedBy.LastName,
                        ContentCreatedByUPN = enquiry.CreatedBy.Upn,
                        ContentGlobalID = enquiry.Id.ToString(),
                        MDPortalUrl = _config.GetValue<string>("MyDrillingPortalUrl"),
                        ContentCreatedDateTime = enquiry.Created.ToString("o"),
                        EventCreatedDateTime = "", //not used
                        EventCreatedByUPN = "", //not used
                        subscriptionEvent = "" //not used
                    });

                return new EmailMessage
                {
                    Subject = subjectTemplate.Render(templateValues),
                    Body = bodyTemplate.Render(templateValues),
                    To = recipient.Email
                };
            }

            emails.AddRange(users.Select(CreateEmail));
            emails.AddRange(staticSubscribers.Select(CreateEmail));

            return emails.ToArray();
        }

        private sealed class Recipient
        {
            public string Email { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
        }
    }
}
