﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Azure.Search.Documents.Indexes;
using Azure.Search.Documents.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Bulletin;
using MyDrilling.Core.Entities.Documentation;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.Services.Search;

namespace MyDrilling.WebJob.Functions
{
    public class SearchIndexFullFunction
    {
#if DEBUG
        private const bool RunOnStartup = true;
#else
        private const bool RunOnStartup = false;
#endif

        private readonly string _searchServiceName;
        private readonly string _searchApiKey;
        private readonly MyDrillingDb _db;

        public SearchIndexFullFunction(IConfiguration config, MyDrillingDb db)
        {
            _db = db;
            _searchServiceName = config.GetValue<string>("SearchServiceName");
            _searchApiKey = config.GetValue<string>("SearchApiKey");
        }

        [FunctionName("MyDrilling_SearchIndexFullFunction")]
        public async Task Process([TimerTrigger("0 0 21 * * Sat", RunOnStartup = RunOnStartup)]TimerInfo timerInfo, ILogger logger)
        {
            try
            {
                // Create a SearchIndexClient to send create/delete index commands
                var searchService = new SearchService(_searchServiceName, _searchApiKey);
                SearchIndexClient idxClient = searchService.GetSearchIndexClient();
                var searchIndexNames = idxClient.GetIndexes();
                if (searchIndexNames.Any(x => x.Name == searchService.IndexName))
                {
                    await idxClient.DeleteIndexAsync(searchService.IndexName);
                }
                //create index
                await CreateSearchIndex(logger, searchService);
                // Load enquiries documents
                var startTime = DateTime.UtcNow;
                logger.LogInformation("Starting full index...time: {time}", startTime);
                var enquiries = await _db.Enquiry_Enquiries
                    .Where(x => x.RigId > 0 && x.Type != TypeInfo.Type.Internal_Internal && !x.IsDeleted)
                    .Select(x => IndexDocumentsAction.Upload(x.ToSearchItem())).ToArrayAsync();
                await searchService.IndexDocuments(enquiries);
                logger.LogInformation("Finished full indexing enquiries - num of indexed {numOfItems}", enquiries.Length);

                // Load bulletins documents
                var zbBulletins = await _db.Bulletin_ZbBulletins
                    .Include(b => b.Rig)
                    .ToArrayAsync();
                var groupedBulletins = zbBulletins
                    .GroupBy(b => new { b.ZaBulletinId, RigId = b.RigId ?? default, RigName = b.Rig?.Name })
                    .Select(b => new { b.Key.ZaBulletinId, RigId = b.Key.RigId, RigName = b.Key.RigName, AllBulletins = b.ToArray() }).ToArray();
                var zaBulletins = await _db.Bulletin_ZaBulletins.Include(b => b.Type).ToArrayAsync();
                var bulletinEntries = groupedBulletins
                    .Join(zaBulletins, zb => zb.ZaBulletinId, za => za.Id, (zb, za) => IndexDocumentsAction.Upload(
                        new SearchItem
                        {
                            Id = nameof(ZaBulletin) + SearchItemExt.IdSeparator + za.Id + SearchItemExt.IdSeparator + zb.RigId,
                            EntityId = za.Id,
                            EntityType = nameof(ZaBulletin),
                            RigId = zb.RigId.ToString(),
                            Name = za.Title,
                            Description = za.Description,
                            ReferenceId = za.ReferenceId
                        })).ToArray();
                await searchService.IndexDocuments(bulletinEntries);
                logger.LogInformation("Finished full indexing bulletins - num of indexed {numOfItems}", bulletinEntries.Length);

                // Load technical reports documents
                var technicalReports = await _db.Documentation_TechnicalReports
                    .Include(x => x.Rig)
                    .Where(x => x.RigId.HasValue && x.RigId.Value > 0 && x.RigId.HasValue && x.Rig.ImportDocumentation)
                    .Select(x => IndexDocumentsAction.Upload(x.ToSearchItem())).ToArrayAsync();
                await searchService.IndexDocuments(technicalReports);
                logger.LogInformation("Finished full indexing technical reports - num of indexed {numOfItems}", technicalReports.Length);

                // Load user manuals documents
                var userManuals = await _db.Documentation_UserManuals
                    .Include(x => x.Rig)
                    .Where(x => x.RigId.HasValue && x.RigId.Value > 0 && x.RigId.HasValue && x.Rig.ImportDocumentation)
                    .Select(x => IndexDocumentsAction.Upload(x.ToSearchItem())).ToArrayAsync();
                await searchService.IndexDocuments(userManuals);
                logger.LogInformation("Finished full indexing user manuals - num of indexed {numOfItems}", userManuals.Length);
                logger.LogInformation("Full indexing successfully finished...time spent(sec): {time}", (DateTime.UtcNow - startTime).TotalSeconds);
            }
            catch (Exception e)
            {
                logger.LogError(e, "Full indexing failed, reason: {reason}", e.Message);
            }
        }

        private async Task CreateSearchIndex(ILogger logger, SearchService searchService)
        {
            await searchService.CreateSearchIndex();
            logger.LogInformation("Search index created.");
        }
    }
}
