﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Equipment;
using MyDrilling.Infrastructure.Data;

namespace MyDrilling.WebJob.Functions
{
    public class EquipmentConsistencyCheckFunction
    {
#if DEBUG
        private const bool RunOnStartup = true;
#else
        private const bool RunOnStartup = false;
#endif
        private readonly MyDrillingDb _db;

        public EquipmentConsistencyCheckFunction(MyDrillingDb db)
        {
            _db = db;
        }

        [FunctionName("MyDrilling_EquipmentConsistencyCheckFunction")]
        public async Task Process([TimerTrigger("0 0 */2 * * *", RunOnStartup = RunOnStartup)] TimerInfo timerInfo,
            ILogger logger)
        {
            await CheckEquipmentReferences(logger);
            await _db.SaveChangesAsync();

            await CheckForDeletedEquipments(logger);
            await _db.SaveChangesAsync();
        }

        private async Task CheckEquipmentReferences(ILogger logger)
        {
            var equipmentsToCheck = await _db.Equipments
                .Where(x => x.ParentReferenceId > 0 && (x.Parent == null || x.Parent.ReferenceId != x.ParentReferenceId)
                            || !string.IsNullOrEmpty(x.ProductShortReferenceId) && x.ProductCode != x.ProductShortReferenceId
                            || x.RigReferenceId > 0 && (x.Rig == null || x.Rig.ReferenceId != x.RigReferenceId)
                            || !string.IsNullOrEmpty(x.RigOwnerReferenceId) && (x.RigOwner == null || x.RigOwner.ReferenceId != x.RigOwnerReferenceId))
                .ToArrayAsync();

            if (equipmentsToCheck.Length == 0)
            {
                return;
            }

            var equipmentMapping = await _db.Equipments.Select(x => new {x.ReferenceId, x.Id})
                .ToDictionaryAsync(x => x.ReferenceId, x => x.Id);

            var rigMapping = await _db.Rigs.Select(x => new {x.Id, x.ReferenceId})
                .ToDictionaryAsync(x => x.ReferenceId, x => x.Id);

            var customerMapping = await _db.Customers.Where(x => !string.IsNullOrEmpty(x.ReferenceId)).Select(x => new {x.Id, x.ReferenceId})
                .ToDictionaryAsync(x => x.ReferenceId, x => x.Id);

            var now = DateTime.UtcNow;
            var parentModifiedCount = 0;
            var productCodeModifiedCount = 0;
            var rigModifiedCount = 0;
            var rigOwnerModifiedCount = 0;
            var functionalLocationsAddedCount = 0;
            foreach (var equipment in equipmentsToCheck)
            {
                bool modified = false;

                if (equipment.ParentReferenceId > 0
                    && equipmentMapping.TryGetValue(equipment.ParentReferenceId.Value, out var parentId)
                    && parentId != equipment.ParentId)
                {
                    equipment.ParentId = parentId;
                    equipment.Modified = now;

                    logger.LogInformation("set parent for equipment {referenceId}", equipment.ReferenceId);
                    parentModifiedCount++;

                    modified = true;
                }

                if (!string.IsNullOrEmpty(equipment.ProductShortReferenceId)
                    && equipment.ProductShortReferenceId != equipment.ProductCode)
                {
                    equipment.ProductCode = equipment.ProductShortReferenceId;
                    equipment.Modified = now;

                    logger.LogInformation("set product code for equipment {referenceId}", equipment.ReferenceId);
                    productCodeModifiedCount++;

                    modified = true;
                }

                if (equipment.RigReferenceId > 0
                    && rigMapping.TryGetValue(equipment.RigReferenceId.Value, out var rigId)
                    && rigId != equipment.RigId)
                {
                    equipment.RigId = rigId;
                    equipment.Modified = now;

                    logger.LogInformation("set rig for equipment {referenceId}", equipment.ReferenceId);
                    rigModifiedCount++;

                    modified = true;
                }

                if (!string.IsNullOrEmpty(equipment.RigOwnerReferenceId)
                    && customerMapping.TryGetValue(equipment.RigOwnerReferenceId, out var customerId)
                    && customerId != equipment.RigOwnerId)
                {
                    equipment.RigOwnerId = customerId;
                    equipment.Modified = now;

                    logger.LogInformation("set rig owner for equipment {referenceId}", equipment.ReferenceId);
                    rigOwnerModifiedCount++;

                    modified = true;
                }

                if (modified && await CheckFunctionalLocation(equipment, logger))
                {
                    functionalLocationsAddedCount++;
                }
            }

            if (parentModifiedCount > 0)
            {
                logger.LogInformation("set parent for {parentModifiedCount} equipments", parentModifiedCount);
            }

            if (productCodeModifiedCount > 0)
            {
                logger.LogInformation("set product code for {productCodeModifiedCount} equipments", productCodeModifiedCount);
            }

            if (rigModifiedCount > 0)
            {
                logger.LogInformation("set rig for {rigModifiedCount} equipments", rigModifiedCount);
            }

            if (rigOwnerModifiedCount > 0)
            {
                logger.LogInformation("set rig owner for {rigOwnerModifiedCount} equipments", rigOwnerModifiedCount);
            }

            if (functionalLocationsAddedCount > 0)
            {
                logger.LogInformation("updated functional location for {functionalLocationsAddedCount} equipments", functionalLocationsAddedCount);
            }
        }

        private async Task<bool> CheckFunctionalLocation(Equipment equipment, ILogger logger)
        {
            if (!equipment.IsLocated())
            {
                return false;
            }

            var lastLocation = await _db.FunctionalLocationLogs.Where(x => x.EquipmentId == equipment.Id)
                .OrderByDescending(x => x.EffectiveDate)
                .FirstOrDefaultAsync();

            if (lastLocation != null && equipment.IsTheSame(lastLocation))
            {
                return false;
            }

            if (lastLocation != null && equipment.IsOutdated(lastLocation))
            {
                return false;
            }

            var newLocation = equipment.CreateFunctionalLocation();
            if (newLocation == FunctionalLocationLog.Invalid)
            {
                return false;
            }

            _db.FunctionalLocationLogs.Add(newLocation);
            logger.LogInformation("added new functional location for {referenceId} equipment", equipment.ReferenceId);
            return true;
        }

        private async Task CheckForDeletedEquipments(ILogger logger)
        {
            var equipmentLatestUpdate = await _db.Equipments.MaxAsync(x => x.LastSeen);
            if (equipmentLatestUpdate == DateTime.MinValue) return;

            //delete not updated ones
            var threshold = equipmentLatestUpdate.AddDays(-2);
            var allOutdatedEquipment = _db.Equipments.Where(x => x.LastSeen <= threshold && !x.IsDeleted);
            foreach (var oEquipment in allOutdatedEquipment)
            {
                oEquipment.IsDeleted = true;
                oEquipment.Modified = DateTime.UtcNow;
                logger.LogInformation("Equipment {referenceId} has been deleted.", oEquipment.ReferenceId);
            }

            //reactivate deleted ones
            var allReactivatedEquipment = _db.Equipments.Where(x => x.LastSeen == equipmentLatestUpdate && x.IsDeleted);
            foreach (var rEquipment in allReactivatedEquipment)
            {
                rEquipment.IsDeleted = false;
                rEquipment.Modified = DateTime.UtcNow;
                logger.LogInformation("Equipment {referenceId} has been reactivated.", rEquipment.ReferenceId);
            }
        }
    }
}
