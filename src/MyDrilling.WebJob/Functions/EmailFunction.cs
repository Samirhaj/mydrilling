﻿using System;
using System.IO;
using System.Net.Mail;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MyDrilling.Infrastructure.MessageContracts;
using MyDrilling.Infrastructure.Storage;

namespace MyDrilling.WebJob.Functions
{
    public class EmailFunction
    {
        private readonly SmtpClient _smtpClient;
        private readonly IConfiguration _config;

        public EmailFunction(SmtpClient smtpClient,
            IConfiguration config)
        {
            _smtpClient = smtpClient;
            _config = config;
        }

        [FunctionName("MyDrilling_EmailFunction")]
        public async Task ProcessQueueMessage([QueueTrigger(StorageConfig.EmailQueue, Connection = "AzureWebJobsStorage")] string message, ILogger logger)
        {
            var msg = JsonSerializer.Deserialize<EmailMessage>(message);
            var prefix = _config.GetValue<string>("Email:SubjectPrefix");
            msg.Subject = $"{prefix}{msg.Subject}";
            try
            {
#if DEBUG
                var tmpFolder = @"C:\temp\md-emails";
                Directory.CreateDirectory(tmpFolder);
                var body = msg.Body.Replace("<title>myDrilling&trade; Notification</title>", $"<title>{msg.Subject}</title>");
                await File.WriteAllTextAsync($@"{tmpFolder}\{msg.To}_{Guid.NewGuid()}.html", body);
#else
                await _smtpClient.SendMailAsync(new MailMessage(_config.GetValue<string>("Email:Smtp:Sender"), msg.To, msg.Subject, msg.Body)
                {
                    IsBodyHtml = true
                });
#endif
                logger.LogInformation("email is sent: '{emailTo}', '{emailSubject}'", msg.To, msg.Subject);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "email failed on sending: '{emailTo}', '{emailSubject}'", msg.To, msg.Subject);
            }
        }
    }
}
