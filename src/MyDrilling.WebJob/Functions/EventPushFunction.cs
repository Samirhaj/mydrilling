﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.MessageContracts;
using MyDrilling.Infrastructure.Storage;

namespace MyDrilling.WebJob.Functions
{
    public class EventPushFunction
    {
#if DEBUG
        private const bool RunOnStartup = true;
#else
        private const bool RunOnStartup = false;
#endif
        private readonly string _rawDataDbConnectionString;

        public EventPushFunction(IConfiguration config)
        {
            _rawDataDbConnectionString = config.GetConnectionString("RawDataDb");
        }

        [FunctionName("MyDrilling_EventPushFunction")]
        public async Task Process([TimerTrigger("0 */5 * * * *", RunOnStartup = RunOnStartup)]TimerInfo timerInfo,
            [Queue(StorageConfig.EnquiryNotificationFromSapQueue, Connection = "AzureWebJobsStorage")] IAsyncCollector<string> enquiryFromSapOutput,
            [Queue(StorageConfig.BulletinNotificationFromSapQueue, Connection = "AzureWebJobsStorage")] IAsyncCollector<string> bulletinFromSapOutput,
            ILogger logger)
        {
            if (string.IsNullOrEmpty(_rawDataDbConnectionString))
            {
                logger.LogWarning("RawDataDb connection string is empty");
                return;
            }

            EventFromSap[] events;
            using (var sqlConnection = await _rawDataDbConnectionString.CreateRawSqlConnection())
            {
                events = (await sqlConnection.QueryAsync<EventFromSap>("SELECT Timestamp, DocType, ReferenceId, CustomerId FROM ZCS_EXTRACT_EVENT")).ToArray();
            }

            if (events.Length == 0)
            {
                return;
            }

            var filteredEvents = events.Where(x => !string.IsNullOrEmpty(x.CustomerId)).ToArray();

            var tasks = new Task[filteredEvents.Length];
            for (var i = 0; i < filteredEvents.Length; i++)
            {
                var @event = events[i];
                if (@event.DocType == 1)
                {
                    tasks[i] = enquiryFromSapOutput.AddAsync(JsonSerializer.Serialize(new EnquiryNotificationFromSapMessage
                    {
                        ReferenceId = @event.ReferenceId, 
                        CustomerId = @event.CustomerId
                    }));
                    continue;
                }

                if (@event.DocType == 6)
                {
                    tasks[i] = bulletinFromSapOutput.AddAsync(JsonSerializer.Serialize(new BulletinNotificationFromSapMessage
                    {
                        ReferenceId = @event.ReferenceId,
                        CustomerId = @event.CustomerId
                    }));
                }
            }

            await Task.WhenAll(tasks);

            using (var sqlConnection = await _rawDataDbConnectionString.CreateRawSqlConnection())
            {
                await sqlConnection.ExecuteAsync("DELETE FROM ZCS_EXTRACT_EVENT WHERE ReferenceId IN @referenceIds AND Timestamp IN @timestamps",
                    new
                    {
                        referenceIds = events.Select(x => x.ReferenceId).Distinct(),
                        timestamps = events.Select(x => x.Timestamp).Distinct()
                    });
            }

            logger.LogInformation("pushed {tasksLength} events", tasks.Length);
        }

        // ReSharper disable once ClassNeverInstantiated.Local
        [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local")]
        private sealed class EventFromSap
        {
            public DateTime? Timestamp { get; set; }
            public int DocType { get; set; }
            public string ReferenceId { get; set; }
            public string CustomerId { get; set; }
        }
    }
}
