﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using MyDrilling.Infrastructure.Data;

namespace MyDrilling.WebJob.Functions
{
    class SparepartsConsistencyCheck
    {
#if DEBUG
        private const bool RunOnStartup = true;
#else
        private const bool RunOnStartup = true;
#endif
        private readonly MyDrillingDb _db;
        public SparepartsConsistencyCheck(MyDrillingDb db)
        {
            _db = db;
        }

        [FunctionName("MyDrilling_SparepartsConsistencyCheckFunction")]
        public async Task Process([TimerTrigger("0 0 */2 * * *", RunOnStartup = RunOnStartup)] TimerInfo timerInfo,
            ILogger logger)
        {
            CheckForDeletedSpareparts(logger);
            await _db.SaveChangesAsync();
        }

        private void CheckForDeletedSpareparts(ILogger logger)
        {
            var sparepartsLatestUpdate = _db.Sparepart_Spareparts.Max(x => x.LastSeen);
            if (sparepartsLatestUpdate == DateTime.MinValue) return;

            //delete not updated ones
            var threshold = sparepartsLatestUpdate.AddDays(-2);
            var allOutdatedSpareparts = _db.Sparepart_Spareparts.Where(x => x.LastSeen <= threshold && !x.IsDeleted);
            foreach (var oSparepart in allOutdatedSpareparts)
            {
                oSparepart.IsDeleted = true;
                oSparepart.Modified = DateTime.UtcNow;
                logger.LogInformation("Sparepart {referenceId} has been deleted.", oSparepart.ReferenceId);
            }

            //reactivate deleted ones
            var allReactivatedSpareparts = _db.Sparepart_Spareparts.Where(x => x.LastSeen == sparepartsLatestUpdate && x.IsDeleted);
            foreach (var rSparepart in allReactivatedSpareparts)
            {
                rSparepart.IsDeleted = false;
                rSparepart.Modified = DateTime.UtcNow;
                logger.LogInformation("Sparepart {referenceId} has been reactivated.", rSparepart.ReferenceId);

            }
        }
    }
}
