﻿using System;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using DotLiquid;
using Microsoft.Azure.WebJobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MyDrilling.Core.Entities.UpgradeMatrix;
using MyDrilling.Core.Entities.Permission;
using MyDrilling.Core.Entities.Subscription;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.MessageContracts;
using MyDrilling.Infrastructure.Storage;

namespace MyDrilling.WebJob.Functions
{
    public class UpgradeMatrixEventFunction
    {
        private readonly MyDrillingDb _db;
        private readonly IConfiguration _config;

        public UpgradeMatrixEventFunction(MyDrillingDb db,
            IConfiguration config)
        {
            _db = db;
            _config = config;
        }

        [FunctionName("MyDrilling_UpgradeMatrixEventFunction")]
        public async Task ProcessQueueMessage([QueueTrigger(StorageConfig.UpgradeMatrixEventQueue, Connection = "AzureWebJobsStorage")] string message,
            [Queue(StorageConfig.EmailQueue, Connection = "AzureWebJobsStorage")] IAsyncCollector<string> emailOutput,
            ILogger logger)
        {
            var msg = JsonSerializer.Deserialize<UpgradeMatrixEventMessage>(message);

            logger.LogInformation("new event: {upgrade_matrix_item_id}, {item_type}", msg.Id, msg.ItemType.ToString());

            var emails = await GenerateEmails(msg);
            logger.LogInformation("generated " + emails.Length + " emails for: {upgrade_matrix_item_id}, {item_type}", msg.Id, msg.ItemType);
            if (emails.Length == 0) return;

            var emailTasks = new Task[emails.Length];
            for (var i = 0; i < emails.Length; i++)
            {
                await Task.Delay(TimeSpan.FromSeconds(3));
                emailTasks[i] = emailOutput.AddAsync(JsonSerializer.Serialize(emails[i]));
            }

            await Task.WhenAll(emailTasks);
        }

        private async Task<EmailMessage[]> GenerateEmails(UpgradeMatrixEventMessage msg)
        {
            long customerId = 0;
            var availableForRigs = new long[] { };
            var itemEquipment = string.Empty;
            var itemDesc = string.Empty;
            if (msg.ItemType == UpgradeMatrixItemType.Ccn)
            {
                var item = await _db.UpgradeMatrix_CcnItems.Include(x => x.EquipmentCategory).FirstOrDefaultAsync(x => x.Id == msg.Id);
                if (item != null)
                {
                    customerId = item.RootCustomerId;
                    itemEquipment = item.EquipmentCategory.ShortName;
                    itemDesc = item.Description;
                    availableForRigs = _db.UpgradeMatrix_CcnStates
                        .Where(x => x.CcnItemId == msg.Id && x.RigSettings != null)
                        .Select(x => x.RigSettings.RigId).ToArray();
                }
            }
            else
            {
                var item = await _db.UpgradeMatrix_QuotationItems.Include(x => x.EquipmentCategory).FirstOrDefaultAsync(x => x.Id == msg.Id);
                if (item != null)
                {
                    customerId = item.RootCustomerId;
                    itemEquipment = item.EquipmentCategory.ShortName;
                    itemDesc = item.Description;
                    availableForRigs = _db.UpgradeMatrix_QuotationStates
                                .Where(x => x.QuotationItemId == msg.Id && x.RigSettings != null)
                                .Select(x => x.RigSettings.RigId).ToArray();
                }
            }

            if (customerId == 0 || availableForRigs.Length == 0 || string.IsNullOrEmpty(itemEquipment))
                return Array.Empty<EmailMessage>();
           
            var users = await _db.Users.Where(u =>
                    u.Roles.Any(r => r.Role == RoleInfo.Role.CustomerAccess && r.RootCustomerId == customerId)
                    && u.SubscriptionSettings.Any(ss => ss.SubscriptionType == SubscriptionType.NewUpgradeMatrixItemAvailable)
                    && (u.Profiles.Any(p => p.Profile.ProfileRoles.Any(r => r.Role == RoleInfo.Role.UpgradeMatrixAdmin)
                                            ||   p.Profile.ProfileRoles.Any(r => r.Role == RoleInfo.Role.UpgradeMatrixEditor) && availableForRigs.Contains(p.RigId)
                                            ||   p.Profile.ProfileRoles.Any(r => r.Role == RoleInfo.Role.UpgradeMatrixViewer) && availableForRigs.Contains(p.RigId)
                                        ) 
                        || u.Roles.Any(r => r.Role == RoleInfo.Role.UpgradeMatrixAdmin
                            || r.Role == RoleInfo.Role.UpgradeMatrixEditor && r.RigId.HasValue && availableForRigs.Contains((long)r.RigId)
                            || r.Role == RoleInfo.Role.UpgradeMatrixViewer && r.RigId.HasValue && availableForRigs.Contains((long)r.RigId)
                            ))
                   )
                .ToArrayAsync();

            var template = await _db.UpgradeMatrix_MessageTemplates.FirstOrDefaultAsync(x => x.UpgradeMatrixEventType == UpgradeMatrixEventType.NewItemAvailable);
            if (template == null) return Array.Empty<EmailMessage>();
            
            var emails = new EmailMessage[users.Length];
            var itemTypeText = msg.ItemType == UpgradeMatrixItemType.Ccn ? "CCN" : "Quotation";
            for (var i = 0; i < users.Length; i++)
            {
                var user = users[i];
                //template parameters:
                //{{ItemEquipment}}
                //{{ItemDescription}}
                //{{MDPortalUrl}}
                //{{SubscriberFirstName}}
                //{{SubscriberLastName}}

                var templateBody = template.Body
                    .Replace("{{ItemEquipment}}", itemEquipment)
                    .Replace("{{ItemDescription}}", itemDesc)
                    .Replace("{{MDPortalUrl}}", _config.GetValue<string>("MyDrillingPortalUrl"))
                    .Replace("{{SubscriberFirstName}}", user.FirstName)
                    .Replace("{{SubscriberLastName}}", user.LastName);
                
                emails[i] = new EmailMessage
                {
                    Subject = $"{template.Subject} - ({itemTypeText})",
                    Body = templateBody,
                    To = !string.IsNullOrEmpty(users[i].Email) ? users[i].Email : users[i].Upn
                };
            }

            return emails;
        }
    }
}