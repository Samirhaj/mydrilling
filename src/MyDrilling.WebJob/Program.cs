using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using DotLiquid;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MyDrilling.Infrastructure.Data;

namespace MyDrilling.WebJob
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = new HostBuilder();
            builder
                .ConfigureWebJobs(b =>
                {
                    b.AddAzureStorageCoreServices();
                    b.AddAzureStorage();
                    b.AddTimers();
                })
                .ConfigureAppConfiguration((context, config) =>
                {
                    //todo: configure context.HostingEnvironment.EnvironmentName instead of ASPNETCORE_ENVIRONMENT
                    var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
                    var configBuilder = config.SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile($"appsettings.{environment}.json", optional: false, reloadOnChange: true);

                    if (string.Equals(environment, "Development"))
                    {
                        configBuilder.AddUserSecrets<Program>();
                    }

                    configBuilder.AddEnvironmentVariables().Build();
                })
                .ConfigureServices((context, services) =>
                {
                    services.AddMyDrillingDb(context.Configuration);
                    services.AddScoped(serviceProvider =>
                    {
                        var config = serviceProvider.GetRequiredService<IConfiguration>();
                        return new SmtpClient
                        {
                            EnableSsl = true,
                            UseDefaultCredentials = false,
                            Host = config.GetValue<string>("Email:Smtp:Host"),
                            Port = config.GetValue<int>("Email:Smtp:Port"),
                            Credentials = new NetworkCredential(
                                config.GetValue<string>("Email:Smtp:Username"),
                                config.GetValue<string>("Email:Smtp:Password")
                            )
                        };
                    });

                    Template.RegisterFilter(typeof(DotLiquidFilters));
                    Liquid.UseRubyDateFormat = true;
                })
                .ConfigureLogging((context, loggingBuilder) =>
                {
                    loggingBuilder.AddConsole();

                    string instrumentationKey = context.Configuration["APPINSIGHTS_INSTRUMENTATIONKEY"];
                    if (!string.IsNullOrEmpty(instrumentationKey))
                    {
                        loggingBuilder.AddApplicationInsightsWebJobs(o => o.InstrumentationKey = instrumentationKey);
                    }
                });

            Console.WriteLine($"Version: {Assembly.GetExecutingAssembly().GetName().Version}");

            var host = builder.Build();
            using (host)
            {
                host.Run();
            }
        }
    }
}
