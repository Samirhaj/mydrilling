﻿using DotLiquid;

namespace MyDrilling.WebJob
{
    public static class DotLiquidFilters
    {
        public static string Trim_Start(string input, string trimCharacter)
        {
            return string.IsNullOrEmpty(input) ? input : input.TrimStart(trimCharacter[0]);
        }

        public static string Trim_End(string input, string trimCharacter)
        {
            return string.IsNullOrEmpty(input) ? input : input.TrimEnd(trimCharacter[0]);
        }

        // ReSharper disable once UnusedMember.Global
        public static string Replace_Linebreaks(string input)
        {
            return string.IsNullOrEmpty(input) ? input : input.Replace("\n", "<br />");
        }
    }
}
