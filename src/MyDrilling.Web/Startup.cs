using System;
using System.Linq;
using System.Net;
using System.Net.Mime;
using System.Reflection;
using System.Threading.Tasks;
using FluentValidation.AspNetCore;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.AzureAD.UI;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using MyDrilling.Core.Entities;
using MyDrilling.Infrastructure;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.Services.AsyncOperation;
using MyDrilling.Infrastructure.Services.Permission;
using MyDrilling.Infrastructure.Services.UpgradeMatrix;
using MyDrilling.Infrastructure.Storage.Blob;
using MyDrilling.Infrastructure.Storage.Queue;
using MyDrilling.Web.Filters;
using MyDrilling.Web.HealthChecks;
using MyDrilling.Web.Lookups;
using MyDrilling.Web.Models.Enquiries;
using MyDrilling.Web.UserDetails;
using Newtonsoft.Json;

namespace MyDrilling.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(AzureADDefaults.AuthenticationScheme)
                .AddAzureAD(options => Configuration.Bind("AzureAd", options));

            services.Configure<OpenIdConnectOptions>(AzureADDefaults.OpenIdScheme, options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    // Instead of using the default validation (validating against a single issuer value, as we do in
                    // line of business apps), we inject our own multitenant validation logic
                    //todo do we need multitenancy here? or all id tokens will be issued by akastor tenant
                    ValidateIssuer = false,

                    // If the app is meant to be accessed by entire organizations, add your issuer validation logic here.
                    //IssuerValidator = (issuer, securityToken, validationParameters) => {
                    //    if (myIssuerValidationLogic(issuer)) return issuer;
                    //}
                };

                options.Events = new OpenIdConnectEvents
                {
                    OnTicketReceived = context =>
                    {
                        // If your authentication logic is based on users then add your logic here
                        return Task.CompletedTask;
                    },
                    OnRemoteSignOut = async context =>
                    {
                        await context.HttpContext.SignOutAsync(AzureADDefaults.CookieScheme);
                    },
                    OnAuthenticationFailed = context =>
                    {
                        context.Response.Redirect("/Error");
                        context.HandleResponse(); // Suppress the exception
                        return Task.CompletedTask;
                    },
                    OnRemoteFailure = context => {
                        var logger = context.HttpContext.RequestServices.GetRequiredService<ILogger<Startup>>();
                        logger.LogError("remote failure on login, message: {message}", context.Failure.Message);
                        if (context.Failure.Message.Contains("access_denied") || context.Failure.Message.Contains("AADSTS50105"))
                        {
                            context.Response.Redirect("/Home/AppNoAccess");
                        }
                        context.HandleResponse();
                        return Task.CompletedTask;
                    },
                    OnRedirectToIdentityProvider = context => {
                        if (!context.HttpContext.User.Identity.IsAuthenticated)
                        {
                            var request = context.Request;
                            var isAjaxRequest = string.Equals(request.Query["X-Requested-With"], "XMLHttpRequest", StringComparison.Ordinal) ||
                                                string.Equals(request.Headers["X-Requested-With"], "XMLHttpRequest", StringComparison.Ordinal);
                            if (isAjaxRequest)
                            {
                                context.Response.StatusCode = (int)HttpStatusCode.RequestTimeout;
                                context.Response.Redirect("/Home/SessionExpired");
                                context.HandleResponse();
                            }
                        }

                        return Task.CompletedTask;
                    },
                    OnTokenValidated = async context =>
                    {
                        var db = context.HttpContext.RequestServices.GetRequiredService<MyDrillingDb>();
                        var logger = context.HttpContext.RequestServices.GetRequiredService<ILogger<Startup>>();

                        var email = context.Principal.GetEmail();
                        var objectId = context.Principal.GetObjectId();
                        var tenantId = context.Principal.GetTenantId();
                        if (string.IsNullOrEmpty(email))
                        {
                            var claims = string.Join("; ", context.Principal.Claims.Select(x => $"'{x.Type}' = '{x.Value}'"));
                            logger.LogError("failed on getting user email, claims : {claims}", claims);
                        }

                        var user = await db.Users.FindByObjectId(objectId).SingleOrDefaultAsync();
                        if (user == null)
                        {
                            user = await db.Users.FindByUpn(email).SingleOrDefaultAsync();
                            if (user == null)
                            {
                                //user not found neither by objectId nor upn
                                logger.LogError("user email={upn}, objectId={objectId}, tenantId={tenantId} not found", email, objectId, tenantId);
                                context.Response.Redirect("/Home/AppNoAccess");
                                context.HandleResponse();
                                return;
                            }

                            user.ObjectId = objectId;
                            user.TenantId = tenantId;
                            user.Modified = DateTime.UtcNow;
                        }

                        user.LastLogIn = DateTime.UtcNow;
                        await db.SaveChangesAsync();
                    }
                };
            });

            services.AddScoped<ICombinedUserRoleService, CombinedUserRoleService>();
            services.AddScoped<IUpgradeMatrixService, UpgradeMatrixService>();
            services.AddMemoryCache();
            services.AddSingleton<IBlobSasGenerator, BlobSasGenerator>();
            services.AddScoped<IReferenceIdLookup, ReferenceIdLookup>();

            services.AddMyDrillingBlob(Configuration);
            services.AddMyDrillingQueues(Configuration);
            services.AddMyDrillingDb(Configuration);

            services.AddHealthChecks()
                .AddDbContextCheck<MyDrillingDb>(name: "mydrillingdb")
                .AddCheck<StorageQueueHealthCheck>("queues")
                .AddCheck<StorageBlobHealthCheck>("blobs")
                .AddCheck<SapConnectorDbHealthCheck>("sapconnector_mydrillingdb")
                .AddCheck<SapConnectorSapHealthCheck>("sapconnector_sap")
                .AddCheck<SapConnectorQueueHealthCheck>("sapconnector_queues")
                .AddCheck<SapConnectorBlobHealthCheck>("sapconnector_blobs");

            services.AddScoped<ModelValidationFilter>();
            services.AddScoped<ConcurrentUpdateFilter>();
            var mvcBuilder = services.AddControllersWithViews(options =>
            {
                var policy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .Build();
                options.Filters.Add(new AuthorizeFilter(policy));
            })
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<CreateEnquiryViewModelValidator>());

#if DEBUG
            mvcBuilder.AddRazorRuntimeCompilation();
#endif

            services.AddApplicationInsightsTelemetry();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<ITelemetryInitializer, TelemetryEnrichment>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //todo handle exceptions properly
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseHealthChecks("/health", new HealthCheckOptions { Predicate = check => !check.Name.StartsWith("sapconnector") });
            app.UseHealthChecks("/health/info", new HealthCheckOptions
            {
                ResponseWriter = WriteHealthCheck,
                Predicate = check => !check.Name.StartsWith("sapconnector")
            });

            app.UseHealthChecks("/health/sapconnector", new HealthCheckOptions { Predicate = check => check.Name.StartsWith("sapconnector") });
            app.UseHealthChecks("/health/sapconnector/info", new HealthCheckOptions
            {
                ResponseWriter = WriteHealthCheck,
                Predicate = check => check.Name.StartsWith("sapconnector")
            });

            app.UseAuthentication();
            app.UseUserDetails();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapAreaControllerRoute(
                    name: "Admin",
                    areaName: "Admin",
                    pattern: "Admin/{controller=Home}/{action=Index}/{id?}");
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            Console.WriteLine($"Version: {Assembly.GetExecutingAssembly().GetName().Version}");
        }

        private static Task WriteHealthCheck(HttpContext context, HealthReport report)
        {
            context.Response.ContentType = MediaTypeNames.Application.Json;
            var result = JsonConvert.SerializeObject(
                new
                {
                    checks = report.Entries.Select(e =>
                        new {
                            description = e.Key,
                            status = e.Value.Status.ToString(),
                            responseTime = e.Value.Duration.TotalMilliseconds
                        }),
                    totalResponseTime = report.TotalDuration.TotalMilliseconds
                }, Formatting.Indented);
            return context.Response.WriteAsync(result);
        }
    }
}
