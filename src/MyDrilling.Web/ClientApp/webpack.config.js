﻿const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const paths = {
    outputDir: '../wwwroot/dist/main',
    outputBase: '/dist/main/'
};

module.exports = {
    // webpack optimization mode
    mode: (process.env.NODE_ENV ? process.env.NODE_ENV : 'development'),
    // generate source map
    devtool: ('production' === process.env.NODE_ENV ? '' : 'inline-source-map'),
    entry: {
        index: './src/index.tsx'
    },
    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".ts", ".tsx", ".js", ".json"],
        modules: ['node_modules'],
        alias: {
            '../../theme.config$': require('path').join(__dirname, '/src/semantic-ui/theme.config')
        }
    },
    module: {
        rules: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
            {
                test: /\.tsx?$/,
                exclude: /node_modules/,
                loader: "awesome-typescript-loader"
            },
            {
                test: /[.]less$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    {
                        loader: 'less-loader',
                        options: {
                            siteFolder: path.join(__dirname, '/src/semantic-ui/site')
                        }
                    }
                ]
            },
            {
                test: /\.(png|gif|jpg|cur)$/i,
                loader: 'file-loader?name=images/[name].[ext]'
            },
            {
                test: /\.woff2(\?v=[0-9]\.[0-9]\.[0-9])?$/i,
                loader: 'url-loader?name=fonts/[name].[ext]',
                options: { mimetype: 'application/font-woff2' }
            },
            {
                test: /\.woff(\?v=[0-9]\.[0-9]\.[0-9])?$/i,
                loader: 'url-loader?name=fonts/[name].[ext]',
                options: { mimetype: 'application/font-woff' }
            },
            {
                test: /\.(ttf|eot|svg|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/i,
                loader: 'file-loader?name=fonts/[name].[ext]'
            }

        ]
    },
    optimization: {
        minimize: 'production' === process.env.NODE_ENV,
        splitChunks: {
            chunks: 'async',
            minSize: 30000,
            maxSize: 0,
            minChunks: 1,
            maxAsyncRequests: 6,
            maxInitialRequests: 4,
            automaticNameDelimiter: '~',
            automaticNameMaxLength: 30,
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    priority: -10
                },
                default: {
                    minChunks: 2,
                    priority: -20,
                    reuseExistingChunk: true
                }
            }
        }
    },
    output: {
        filename: '[name].js',
        chunkFilename: 'production' === process.env.NODE_ENV ? '[name].[chunkhash].js' : '[name].js',
        path: path.join(__dirname, paths.outputDir),
        publicPath: paths.outputBase
    },
    plugins: [
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: 'css/[name].css',
            chunkFilename: 'css/[id].css'
        })
    ]
};