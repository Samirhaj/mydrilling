﻿export default {
    undefined : 1,
    catD : 2,
    ramRig : 3,
    drillShips : 4,
    eeClass : 5,
    jackUp : 6,
    fixed : 7
}