﻿type QuotationState = {
    id: number,
    itemId: number,
    rigSettingsId: number,
    rigShortName: string,
    status: number,   
    enquiryCreationEnabled: boolean,
    elementIndex?: number;
}