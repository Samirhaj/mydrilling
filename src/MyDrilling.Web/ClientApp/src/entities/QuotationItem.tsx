﻿type QuotationItem = {
    id: number,
    customerId: number,
    equipmentCategoryId: number,
    equipmentCategoryName: string,
    description: string,
    heading: string,
    benefits: string,    
    rigStates: QuotationState[],
    quotationReference: string;
}