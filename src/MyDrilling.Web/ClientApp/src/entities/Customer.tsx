﻿type Customer = {
    id: number,
    customerId: number,
    name: string,
    status: string,
    rigs: Rig[];
}