﻿export default {
    undefined: 1,
    na: 2,
    ready: 3,
    approved : 4,
    completed: 5
}