﻿type SimpleOptionItem = {
    key: string,
    text: string,
    value: number;
}