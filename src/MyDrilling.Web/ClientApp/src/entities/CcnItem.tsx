﻿type CcnItem = {
    id: number,
    customerId: number,
    equipmentCategoryId: number,
    equipmentCategoryName: string,
    description: string,
    comments: string,
    packageUpgrade: number,
    rigType: number,        
    rigStates: CcnState[];
}