﻿type Rig = {
    id: number,
    rigId: number,
    name: string,
    referenceId: string,
    shortName: string,
    rigType: number,
    showInCcn: boolean,
    showInQuotation: boolean,
    elementIndex: number;
}