﻿type User = {
    id: number,
    isUpgradeMatrixAdmin: boolean,
    hasUpgradeMatrixAccess: boolean,
    viewerOnRigs: number[],
    editorOnRigs: number[];
}