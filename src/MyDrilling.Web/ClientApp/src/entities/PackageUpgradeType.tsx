﻿export default {
    undefined: 1,
    performanceUpgrade: 2,
    individual: 3,
    dw: 4,
    tmUpgrade: 5,
    mudPumps: 6,
    bulletin: 7
}