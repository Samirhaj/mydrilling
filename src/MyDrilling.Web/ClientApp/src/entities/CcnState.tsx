﻿type CcnState = {
   id : number,
   itemId: number,
   rigSettingsId: number,
   rigShortName: string,
   status: number,
   ccnName: string,
   documentPath: string,
   document?: File,
   enquiryCreationEnabled: boolean,
   elementIndex?: number,
   approvedBy:string;
}