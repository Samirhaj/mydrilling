﻿import * as React from "react";
import { Route, Switch, BrowserRouter as Router } from "react-router-dom";
import { Loader } from 'semantic-ui-react';
import 'semantic-ui-less/semantic.less';
import styled from 'styled-components';
import { handleResponse, handleError } from "./Utils";
import axios from 'axios';

import UpgradeMatrixPage from '../pages/UpgradeMatrix';
import CustomerPage from '../pages/Customer';
import EquipmentPage from '../pages/Equipment';

const UpgradeMatrixWrapper = styled.div`
    padding: 20px 0;  
    .disabled, .disabled:hover{
        color: unset !important;
    }
  .loader {
        background: none !important;
        border-radius: unset !important;
        box-shadow: none !important;
    }
    i.green.icon{
        border: none !important;
        background-color: transparent !important;
    }   
    .dropdown{
        font-size: unset;
        margin-left:3px;
    }
    .ui.dropdown .menu > .item {
        font-size:1em !important;
    }
    .file, .commentFile {
        background: unset !important;
        padding: unset !important;
    } 
`;


interface IMainLayoutState {
    currentUser?: User;   
}

class MainLayout extends React.Component<any, IMainLayoutState> {
    private paths = {
        getCurrentUser: '/upgradematrix/getcurrentuser'
    };

    constructor(props: Readonly<any>) {
        super(props);
        this.state = {
            currentUser: undefined    
        };
    }  
    
    componentDidMount() {       
        this.getCurrentUser();
    }

    getCurrentUser = () => {
        axios.get(this.paths.getCurrentUser)
        .then(handleResponse)
        .then(resp => {
            var user = resp.user as User;
            if (user) {
                this.setState({ currentUser: user });
            }
        }).catch(err => {
            handleError(err, this);
        });
    }

    render() {
        const { currentUser } = this.state;
        return (
            <UpgradeMatrixWrapper>
                {!currentUser
                    ? <Loader active inline />
                    : (currentUser.hasUpgradeMatrixAccess
                        ? <Router>
                            <Switch>
                                <Route exact path="/upgradematrix" component={() => <UpgradeMatrixPage currentUser={currentUser} />} />
                                <Route exact path="/upgradematrix/customer" component={() => currentUser.isUpgradeMatrixAdmin ? <CustomerPage /> : <p>Only admin has access to Customer page!!</p>} />
                                <Route exact path="/upgradematrix/equipment" component={() => currentUser.isUpgradeMatrixAdmin ? <EquipmentPage /> : <p>Only admin has access to Equipment page!!</p>} />
                            </Switch>
                        </Router>
                        : <p>No access to upgrade matrix!!</p>
                        )
                }
            </UpgradeMatrixWrapper>
        );
    }
}

export default MainLayout;