﻿import styled from "styled-components";
import { Table, Form, Dropdown, Button } from 'semantic-ui-react';

export const SearchInputWrapper = styled.div`
    float:right;
`;

export const FormArea = styled.div`
  width:45%;
`;

export const ButtonFieldInline = styled(Form.Button)`     
    width: 7% !important; 
    display: inline !important;
`;

export const ButtonInline = styled(Button)`    
    display: inline !important;
`;

export const ButtonCell = styled(Table.HeaderCell)`    
    width: 100px !important;
`;


export const FilterDropdownWrapper = styled(Dropdown)`
   width: 90px;   
`;

export const EnquiryButton = styled(Button)`
    width: 35px !important;
    padding: 6px 0px !important;
    margin: 0px !important;
    font-size: 0.71em !important;
    i.icon, i.icons{
        font-size:1.5em !important;
        margin:0px !important;
    }
`;