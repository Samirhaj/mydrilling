﻿import * as React from 'react';
import { map, remove, findIndex } from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import RigType from '../entities/RigType';
import RigState from '../entities/RigState';
import PackageUpgradeType from "../entities/PackageUpgradeType";
import axios from 'axios';

export const getuid = () => {
    return uuidv4();
};

export const checkIfSessionExpired = (message: string) => {
    if (message && message.indexOf('Session expired.') == 0)
        window.location.reload();
}

export const onMessageModalClose = (instance : any, message: string) => {
    checkIfSessionExpired(message);
    instance.setState({ messageModalOpen: false, message: '', isError: false });
}

export const backToList = (instance: any) => {
    const { onBack } = instance.props;
    if (onBack)
        onBack();
}

export const handleResponse = (response: any) => {
    if (!response.data) return Error('Response empty!');
    if (response.data.ok)
        return response.data;
    throw Error(response.data.message);
}

export const handleError = (error: any, instance: any, onError?: any) => {
    var errMessage = error.response && error.response.data ? error.response.data.message : error.message;
    if (onError) {
        onError(errMessage);
    } else {
        instance.setState({ messageModalOpen: true, isError: true, message: errMessage });
    }
}

export const getGridData = (instance: any, getEntitiesPath: string, onSuccess?: any, onError?: any) => {
    axios.get(getEntitiesPath)
        .then(handleResponse)
        .then(resp => {
            var items = resp.items ? resp.items : [];
            if (onSuccess) {
                onSuccess(items, instance);
            } else {
                instance.setState({ data: items });
            }
        }).catch(err => {
            handleError(err, instance, onError);
        });
};

export const fetchAllEntitiesForDropdown = (isFetchingPropName: string, optionsPropName: string, fetchAllPath: string, instance: any, identifier?: string, label?: string) => {
    var isFetchingObj = {} as any;
    isFetchingObj[isFetchingPropName] = true;
    instance.setState(isFetchingObj);
    getGridData(instance, fetchAllPath, (items: any) => { onFetchAllEntitiesForDropdown(items, isFetchingPropName, optionsPropName, instance, identifier, label); });
}

export const onFetchAllEntitiesForDropdown = (items: any, isFetchingPropName: string, optionsPropName: string, instance: any, identifier?: string, label?: string) => {
    var optionsObj = {} as any;
    optionsObj[isFetchingPropName] = false;
    optionsObj[optionsPropName] = createSimpleOptionItems(items, identifier, label);
    instance.setState(optionsObj);
}

export const createSimpleOptionItems = (items: any, identifier?: string, label?: string) => {
    var valueIdentifier : string = !identifier ? 'id' : identifier;
    var valueLabel : string = !label ? 'name' : label;
    return map(items, (item: any) => ({
        "key": getuid(),
        "text": item[valueLabel],
        "value": item[valueIdentifier]
    }));
}

export const handleInputChange = (e : any, name : string, value: any, instance: any) => {
    var currentEntity = instance.state.currentEntity;
    currentEntity[name] = value;
    instance.setState({ currentEntity: currentEntity });
};

export const saveEntity = (e: any, instance: any) => {
    e.preventDefault();
    var currentEntity = instance.state.currentEntity;
    var message = '';
    //if (instance.state.mandatory && instance.state.mandatory.length > 0) {
    //    forEach(instance.state.mandatory, (field) => {
    //        if (!currentEntity[field.key] || currentEntity[field.key] === '') {
    //            message += (message == '' ? 'Please fill in the following: ' : '') + (message == '' ? '' : ', ') + field.value;
    //        }
    //    });
    //}
    //if (message) {
    //    instance.setState({ messageModalOpen: true, isError: true, isRegularDialog: true, message: message });
    //} else {
        var newEntity = JSON.stringify(currentEntity);
        handleSubmit(instance, instance.paths.postSaveEntity, newEntity);
    //}
}

export const handleSubmit = (instance: any, addEntityPath: string, newEntityJson: any) => {
    instance.setState({ loading: true });
    axios(addEntityPath, {
        method: 'post',
        data: newEntityJson,
        headers: { 'Content-Type': 'application/json' }
        })
    .then(handleResponse)
    .then(resp => {
        instance.setState({ messageModalOpen: true, isError: false, message: resp.message, loading: false });
    }).catch(err => {
        handleError(err, instance);
    });
}

export const onCancelEditing = (e: any, instance: any) => {
    e.preventDefault();
    backToList(instance);
}

export const getEmptyCcnItem = (customerId: number) : CcnItem => {
    return {
        id: 0,
        customerId: customerId,
        description: '',
        comments: '',
        equipmentCategoryId: 0,
        equipmentCategoryName: '',
        packageUpgrade: PackageUpgradeType.undefined,
        rigType: RigType.undefined,
        rigStates: []
    } as CcnItem;
}

export const getEmptyQuotationItem = (customerId: number) : QuotationItem => {
    return {
        id: 0,
        customerId: customerId,
        description: '',
        heading: '',
        equipmentCategoryId: 0,
        equipmentCategoryName: '',
        benefits: '',
        rigStates: [],
        quotationReference: ''
    } as QuotationItem;
}

export const getPackageUpgradeText = (packageUpgrade: number) : string => {
    switch (packageUpgrade) {
        case PackageUpgradeType.performanceUpgrade:
            return 'Performance upgrade';
        case PackageUpgradeType.individual:
            return 'Individual';
        case PackageUpgradeType.dw:
            return 'DW';
        case PackageUpgradeType.tmUpgrade:
            return 'TM upgrade';
        case PackageUpgradeType.mudPumps:
            return 'Mud pumps';
        case PackageUpgradeType.bulletin:
            return 'Bulletin';
       default:
           return '-';
    }
}

export const getRigStateText = (rigState: number): string => {
    switch (rigState) {
        case RigState.na:
            return 'Not applicable';
        case RigState.ready:
            return 'Ready MHWirth';
        case RigState.approved:
            return 'Approved customer';
        case RigState.completed:
            return 'Completed';
        default:
            return 'Undefined';
    }
}

export const getRigTypeText = (rigType: number): string => {
    switch (rigType) {
        case RigType.catD:
            return 'Cat-D';
        case RigType.ramRig:
            return 'Ram Rig';
        case RigType.drillShips:
            return 'Drill Ships';
        case RigType.eeClass:
            return 'EE-Class';
        case RigType.jackUp:
            return 'JackUp';
        case RigType.fixed:
            return 'Fixed';
        default:
            return 'Not applicable';
    }
}

export const rigTypes = [
    { key: getuid(), text: getRigTypeText(RigType.undefined), value: RigType.undefined },
    { key: getuid(), text: getRigTypeText(RigType.catD), value: RigType.catD },
    { key: getuid(), text: getRigTypeText(RigType.ramRig), value: RigType.ramRig },
    { key: getuid(), text: getRigTypeText(RigType.drillShips), value: RigType.drillShips },
    { key: getuid(), text: getRigTypeText(RigType.eeClass), value: RigType.eeClass },
    { key: getuid(), text: getRigTypeText(RigType.jackUp), value: RigType.jackUp },
    { key: getuid(), text: getRigTypeText(RigType.fixed), value: RigType.fixed }
];

export const rigStates = [
    { key: getuid(), text: getRigStateText(RigState.undefined), value: RigState.undefined },
    { key: getuid(), text: getRigStateText(RigState.na), value: RigState.na },
    { key: getuid(), text: getRigStateText(RigState.ready), value: RigState.ready },
    { key: getuid(), text: getRigStateText(RigState.approved), value: RigState.approved },
    { key: getuid(), text: getRigStateText(RigState.completed), value: RigState.completed }
];

export const quotationRigStates = [
    { key: getuid(), text: getRigStateText(RigState.undefined), value: RigState.undefined },
    { key: getuid(), text: getRigStateText(RigState.na), value: RigState.na },
    { key: getuid(), text: getRigStateText(RigState.completed), value: RigState.completed }
];

export const getFileNameFromPath = (filePath?: string) => {
    if (!filePath) return '';
    var filePathParts = filePath.split('/');
    return filePathParts[filePathParts.length - 1];
}

export const confirmDelete = (e:any, id:number, instance:any) => {
    e.preventDefault();
    const { accessType } = instance.props;
    //if (accessType && accessType != AccessType.full) return;
    var confirmMessage = 'Are you sure?';
    instance.setState({ openConfirmDialog: true, itemToDelete: id, confirmDialogMessage: confirmMessage });
}


export const onConfirmDelete = (instance : any, deletePath: string, onDeleteSuccess?: any) => {
    const { itemToDelete } = instance.state;
    axios(deletePath + '/' + itemToDelete, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' }
        })
        .then(handleResponse)
        .then(resp => {
            
            if (onDeleteSuccess) {
                onDeleteSuccess(instance, resp.message);
            }
            else {
                const { data, searchResults } = instance.state;
                remove(data, (entity: any) => {
                    return entity.id === itemToDelete;
                });
                if (searchResults) {
                    remove(searchResults, (entity: any) => {
                        return entity.id === itemToDelete;
                    });
                }
                instance.setState({ openConfirmDialog: false, itemToDelete: null, messageModalOpen: true, isError: false, message: resp.message, data: data, searchResults: searchResults });
            }
        }).catch(err => {
            var errMessage = err.response && err.response.data ? err.response.data.message : err.message;
            instance.setState({ openConfirmDialog: false, itemToDelete: null, messageModalOpen: true, isError: true, message: errMessage });
        });
}

export const onCancelDelete = (instance : any) => {
    instance.setState({ openConfirmDialog: false, confirmDialogMessage: '', itemToDelete: null });
}

//Ccn/Quotations forms
export const handleStateChange = (e: any, instance: any, index: number, rigState?: any) => {
    if(rigState)
        rigState.elementIndex = index;
    instance.setState(({ activeStateItem: rigState }) as any);
}

export const handleSaveState = (e: any, instance: any) => {
    e.preventDefault();
    const { activeStateItem } = instance.state;
    var currentEntity = instance.state.currentEntity;
    if (!currentEntity || !currentEntity.rigStates || !activeStateItem) return;
    if (activeStateItem.elementIndex < currentEntity.rigStates.length) {
        currentEntity.rigStates[activeStateItem.elementIndex] = activeStateItem;
        instance.setState(({ currentEntity: currentEntity, activeStateItem: undefined }) as any);
    } else {
        instance.setState(({ activeStateItem: undefined }) as any);
    }
}

export const handleRigStateFieldChange = (rigStateFieldName: string, rigStateFieldValue: any, instance: any) => {
    var activeStateItem = instance.state.activeStateItem as any;
    if (!activeStateItem) return;
    activeStateItem[rigStateFieldName] = rigStateFieldValue;
    if (rigStateFieldName === 'document') {
        activeStateItem['documentPath'] = rigStateFieldValue ? (rigStateFieldValue as File).name : undefined;
    }
    instance.setState(({ activeStateItem: activeStateItem }) as any);
}

export const handleEnquiryClick = (title: string, rigId: string) => {
    var enquiryLink = document.getElementById('react-enquiry-link');
    if (enquiryLink) {
        enquiryLink.setAttribute("href", "/enquiries/create?type=8&rig=" + rigId + "&title=" + encodeURIComponent(title));
        enquiryLink.click();
    }
}