﻿import * as React from 'react';
import { Button, Form, Dropdown, Table, Input } from 'semantic-ui-react';
import { map, filter, indexOf, find, forEach, cloneDeep, remove } from 'lodash';
import styled from 'styled-components';
import { FormArea, ButtonFieldInline } from "../common/CommonStyles";
import {
    fetchAllEntitiesForDropdown,
    getuid,
    rigTypes,
    getGridData,
    createSimpleOptionItems,
    getRigTypeText,
    saveEntity,
    onCancelEditing,
    onMessageModalClose,
    backToList
} from "../common/Utils";
import MessageModal from '../components/MessageModal';


interface ICustomerFormProps {
    onBack?: any,
    editEntity?: Customer;
}

interface ICustomerFormState {
    currentEntity?: Customer,
    existingCustomers?: Customer[],
    rigToAdd?: Rig,
    rigToEdit?: Rig,
    isFetchingCustomer?: boolean,
    searchQueryCustomer?: string,
    optionsCustomer: any,
    isFetchingRig?: boolean,
    searchQueryRig?: string,
    customerRigs?: Rig[];
    optionsRig: any,
    messageModalOpen: boolean,
    isError?: boolean,
    message: string;
}

class CustomerForm extends React.Component<ICustomerFormProps, ICustomerFormState> {
    private paths = {
        postSaveEntity: '/upgradematrix/savecustomersettings',
        fetchRootCustomers: '/upgradematrix/getcustomers',
        fetchCustomerRigs: '/upgradematrix/getcustomerrigs',
        fetchExistingCustomers: '/upgradematrix/getcustomersettings',
    };

    constructor(props: Readonly<ICustomerFormProps>) {
        super(props);
        if (props.editEntity) {
            forEach(props.editEntity.rigs, (rig: Rig, ix: number) => {
                rig.elementIndex = ix;
            });
        }
        this.state = {
            isFetchingCustomer: false,
            searchQueryCustomer: undefined,
            optionsCustomer: undefined,
            isFetchingRig: false,
            searchQueryRig: undefined,
            optionsRig: undefined,
            customerRigs: undefined,
            currentEntity: props.editEntity ? props.editEntity : this.getEmptyEntity(),
            messageModalOpen: false,
            message: ''
        };
    }

    componentDidMount() {
        this.setState(({
            optionsCustomer: fetchAllEntitiesForDropdown('isFetchingCustomer', 'optionsCustomer', this.paths.fetchRootCustomers, this) as any
        }) as any);  
        this.getRigs();
        //TODO - check if necessary
        //getGridData(this, this.paths.fetchExistingCustomers, this.onGetExistingCustomers);
    }

    componentDidUpdate(prevProps: Readonly<ICustomerFormProps>, prevState: Readonly<ICustomerFormState>) {
        this.getRigs();  
    }

    onGetExistingCustomers = (items: Customer[], instance: any) => {
        instance.setState(({ existingCustomers: items } as any));
    }

    getRigs() {
        const { currentEntity } = this.state;
        if (currentEntity && currentEntity.customerId > 0 && !this.state.customerRigs) {
            getGridData(this, this.paths.fetchCustomerRigs + '/' + currentEntity.customerId, this.onGetCustomerRigs);
        }
    }

    onGetCustomerRigs = (items: Rig[], instance: any) => {
        instance.setState(({
            isFetchingRig: false,
            customerRigs: items,
            optionsRig: createSimpleOptionItems(map(items, (r: Rig) => { return { id: r.rigId, name: r.name }; })),
            rigToAdd: this.getEmptyRig()
        } as any));
    }

    getEmptyEntity = () => {
        return {
            id: 0,
            customerId: 0,
            name: ''  
        } as Customer;
    }

    getEmptyRig = () => {
        return { id: 0, rigId: 0, name: '', referenceId: '', shortName: '', rigType: 0 } as Rig;
    }

    handleCustomerChange = (e: any, data: any) => {
        var currentEntity = this.state.currentEntity;
        if (currentEntity) {
            currentEntity.customerId = data.value;
            currentEntity.rigs = [];
            this.setState(({
                currentEntity: currentEntity,
                rigToAdd: this.getEmptyRig(),
                isFetchingRig: true,
                searchQueryRig: undefined,
                optionsRig: undefined,
                customerRigs: undefined
            }) as any);
            this.getRigs();
        }        
    };

    handleRigChange = (e: any, data: any) => {
        const { customerRigs } = this.state;
        var rigToAdd = this.state.rigToAdd;
        if (!rigToAdd) return;
        var selectedRig = find(customerRigs, (rig) => { return rig.rigId === data.value });
        if (selectedRig) {
            rigToAdd.rigId = data.value;
            rigToAdd.name = selectedRig.name;
            rigToAdd.referenceId = selectedRig.referenceId;
            rigToAdd.rigType = 0;
            this.setState(({
                rigToAdd: rigToAdd
            }) as any);
        }
    };

    handleRigNameOrTypeChange = (e: any, data: any, dataFieldName: string, isAdd : boolean) => {
        var rig = (isAdd ? this.state.rigToAdd : this.state.rigToEdit) as any;
        if (rig) {
            rig[dataFieldName] = data.value;
            if (isAdd) {
                this.setState(({ rigToAdd: rig }) as any);
            } else {
                this.setState(({ rigToEdit: rig }) as any);
            }
        }
    };
   
    handleAddRigClick = (e: any) => {
        e.preventDefault();
        const { rigToAdd } = this.state;
        if (!rigToAdd || !rigToAdd.rigId || !rigToAdd.shortName) return;
        var currentEntity = this.state.currentEntity;
        if (!currentEntity) return;
        rigToAdd.elementIndex = currentEntity.rigs.length;
        currentEntity.rigs.push(rigToAdd);
        this.setState(({
            currentEntity: currentEntity,
            rigToAdd: this.getEmptyRig()
        }) as any);
    }
    onEditRig = (e: any, rig: Rig) => {
        e.preventDefault();
        this.setState(({
            rigToAdd: undefined,
            rigToEdit: rig
        }) as any);
    }
    onDeleteRig = (e: any, ix: number) => {
        e.preventDefault();
        var currentEntity = this.state.currentEntity;
        if (!currentEntity) return;
        //delete selected rig
        remove(currentEntity.rigs, (entity: any) => {
            return entity.elementIndex === ix;
        });
        //rearrange indexes
        forEach(currentEntity.rigs, (rig: Rig, ix: number) => {
            rig.elementIndex = ix;
        });
        this.setState(({ currentEntity: currentEntity }) as any);
    }
    onSaveRig = (e: any, ix: number) => {
        e.preventDefault();
        var currentEntity = this.state.currentEntity as any;
        if (!currentEntity) return;
        currentEntity.rigs[ix] = this.state.rigToEdit;
        this.setState(({ currentEntity: currentEntity, rigToEdit: undefined }) as any);
    }
    onCancelRig = (e: any, ix: number) => {
        e.preventDefault();
        this.setState(({ rigToEdit: undefined }) as any);
    }
    render() {
        const { isFetchingCustomer, optionsCustomer, currentEntity, rigToAdd, rigToEdit, isFetchingRig, optionsRig, message, messageModalOpen, isError } = this.state;
        var optionsForRig = optionsRig;
        if (optionsForRig && optionsForRig.length > 0 && currentEntity && currentEntity.rigs && currentEntity.rigs.length > 0) {
            var rigsInUse = map(currentEntity.rigs, (rig) => { return rig.rigId; });
            optionsForRig = filter(optionsForRig, (rigOption) => { return indexOf(rigsInUse, rigOption.value) === -1});
        }

        return (
            <React.Fragment>
                <h3>Customer</h3>
                <Form onSubmit={(e) => { saveEntity(e, this) }}>
                    <Form.Field>
                        <label>Customer</label>
                        <Dropdown
                            search
                            selection
                            name='id'
                            value={currentEntity && currentEntity.customerId > 0 ? currentEntity.customerId: ''}
                            placeholder={'Select Customer...'}
                            options={optionsCustomer && optionsCustomer.length > 0 ? optionsCustomer : []}
                            disabled={isFetchingCustomer || (currentEntity && currentEntity.id > 0) }
                            loading={isFetchingCustomer}
                            onChange={this.handleCustomerChange}
                        />
                        <Table>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>Rig</Table.HeaderCell>
                                    <Table.HeaderCell>Short name</Table.HeaderCell>
                                    <Table.HeaderCell>Type</Table.HeaderCell>
                                    <Table.HeaderCell></Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                <Table.Row>
                                    <Table.Cell>
                                        <Dropdown
                                            search
                                            selection
                                            name='id'
                                            value={rigToAdd && rigToAdd.rigId > 0 ? rigToAdd.rigId : ''}
                                            placeholder={optionsForRig && optionsForRig.length === 0 ? 'No rigs.' : 'Select Rig...'}
                                            options={optionsForRig && optionsForRig.length > 0 ? optionsForRig : []}
                                            disabled={!currentEntity || !currentEntity.customerId || isFetchingRig || (optionsForRig && optionsForRig.length === 0)}
                                            loading={isFetchingRig}
                                            onChange={this.handleRigChange}
                                        />
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Input
                                            name='shortName'
                                            value={rigToAdd ? rigToAdd.shortName : ''}
                                            onChange={(e, data) => { this.handleRigNameOrTypeChange(e, data, 'shortName', true)}}
                                            disabled={!currentEntity || !currentEntity.customerId || (optionsForRig && optionsForRig.length === 0)}
                                        />
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Dropdown
                                            selection
                                            value={rigToAdd ? rigToAdd.rigType : ''}
                                            name='rigType'
                                            options={rigTypes && rigTypes.length > 0 ? rigTypes : []}
                                            onChange={(e, data) => { this.handleRigNameOrTypeChange(e, data, 'rigType', true) }}
                                            disabled={!currentEntity || !currentEntity.customerId || (optionsForRig && optionsForRig.length === 0)}
                                        />
                                    </Table.Cell>
                                    <Table.Cell>
                                        <Button
                                            icon='plus'
                                            content='Add'
                                            color='blue'
                                            size='mini'
                                            disabled={!rigToAdd}
                                            onClick={this.handleAddRigClick}/>
                                    </Table.Cell>
                                </Table.Row>
                                {!currentEntity || !currentEntity.rigs || currentEntity.rigs.length === 0
                                    ? <Table.Row>
                                        <Table.Cell colSpan='4'>No data.</Table.Cell>
                                    </Table.Row>
                                    : map(currentEntity.rigs, (res: Rig, ix: number) => (
                                        <Table.Row key={'customer-rig-' + ix}>
                                        <Table.Cell>
                                            {res.name + ' - ' + res.referenceId}
                                        </Table.Cell>
                                        <Table.Cell>
                                                {(!rigToEdit || rigToEdit.elementIndex !== ix) && <span>{res.shortName}</span>}
                                                {rigToEdit && rigToEdit.elementIndex === ix && <Input
                                                                name='shortName'
                                                                value={rigToEdit ? rigToEdit.shortName : ''}
                                                                onChange={(e, data) => { this.handleRigNameOrTypeChange(e, data, 'shortName', false) }}
                                                              />}
                                        </Table.Cell>
                                        <Table.Cell>
                                                {(!rigToEdit || rigToEdit.elementIndex !== ix) && <span>{getRigTypeText(res.rigType)}</span>}
                                                {rigToEdit && rigToEdit.elementIndex === ix && <Dropdown
                                                    selection
                                                    value={rigToEdit ? rigToEdit.rigType : ''}
                                                    name='rigType'
                                                    options={rigTypes && rigTypes.length > 0 ? rigTypes : []}
                                                    onChange={(e, data) => { this.handleRigNameOrTypeChange(e, data, 'rigType', false) }}
                                                />}
                                        </Table.Cell>
                                        <Table.Cell>
                                                {(!rigToEdit || rigToEdit.elementIndex !== ix) && <Button icon='pencil' title='Edit' size='mini' onClick={(e) => { this.onEditRig(e, cloneDeep(res)) }} />}
                                                {(!rigToEdit || rigToEdit.elementIndex !== ix) && <Button icon='x' title='Delete' size='mini' onClick={(e) => { this.onDeleteRig(e, res.elementIndex)}} />}
                                                {rigToEdit && rigToEdit.elementIndex === ix && <Button icon='check' title='Save' size='mini' onClick={(e) => { this.onSaveRig(e, res.elementIndex) }} />}
                                                {rigToEdit && rigToEdit.elementIndex === ix && <Button icon='x' title='Cancel' size='mini' onClick={(e) => { this.onCancelRig(e, res.elementIndex) }} />}
                                        </Table.Cell>
                                    </Table.Row>
                                ))}
                            </Table.Body>
                        </Table>
                    </Form.Field>
                    {currentEntity && currentEntity.id > 0 && <ButtonFieldInline onClick={(e: any) => { onCancelEditing(e, this) }}>Cancel</ButtonFieldInline>}
                    <ButtonFieldInline positive>Save</ButtonFieldInline>
                </Form>
                <MessageModal
                    message={message}
                    open={messageModalOpen}
                    onClose={() => { onMessageModalClose(this, message) }}
                    onBack={() => { backToList(this) }}
                    isError={isError}
                />
            </React.Fragment>
        );
    }
}
export default CustomerForm;