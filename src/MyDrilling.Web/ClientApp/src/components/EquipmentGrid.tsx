﻿import * as React from 'react';
import { Button, Table, Confirm, Loader } from 'semantic-ui-react';
import { map } from 'lodash';
import { getGridData, getuid, onCancelDelete, onConfirmDelete, confirmDelete, onMessageModalClose } from "../common/Utils";
import MessageModal from '../components/MessageModal';

interface IEquipmentGridProps {
    accessType?: number,
    onEdit: any, 
    onDelete: any;
}

interface IEquipmentGridState {
    openConfirmDialog: boolean,
    data?: EquipmentCategory[],
    itemToDelete?: number,
    messageModalOpen: boolean,
    isError?: boolean,
    message: string;
}

class EquipmentGrid extends React.Component<IEquipmentGridProps, IEquipmentGridState> {
    private paths = {
        fetchAllEquipmentCategories: '/upgradematrix/getequipmentcategories',
        deleteEntity: '/upgradematrix/deleteequipmentcategory'
    };

    constructor(props: Readonly<IEquipmentGridProps>) {
        super(props);
        this.state = {
            data: undefined,
            openConfirmDialog: false,
            messageModalOpen: false,
            message: ''
        };
    }

    public componentDidMount() {
        getGridData(this, this.paths.fetchAllEquipmentCategories);
    }

    handleEditClick = (itemForEditing: EquipmentCategory) => {
        const { onEdit } = this.props;
        if (onEdit) {
            onEdit(itemForEditing);
        }
    }

    handleDeleteClick = (itemId: number) => {
        const { onDelete } = this.props;
        if (onDelete) {
            onDelete(itemId);
        }
    }

    render() {
        const {
            data, openConfirmDialog, message, messageModalOpen, isError } = this.state;
        return (
            <React.Fragment>
                <Table celled striped>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Short name</Table.HeaderCell>
                            <Table.HeaderCell></Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {!data
                            ? <Table.Row>
                                <Table.Cell colSpan='5'><Loader active inline='centered' /></Table.Cell>
                            </Table.Row>
                            : data.length === 0
                                ? <Table.Row>
                                    <Table.Cell colSpan='5' textAlign='center'>No data.</Table.Cell>
                                </Table.Row>
                                : map(data, (res: EquipmentCategory) => (
                                    <Table.Row key={getuid()}>
                                        <Table.Cell>
                                            {res.shortName}
                                        </Table.Cell>
                                        <Table.Cell textAlign='center'>
                                            <Button icon='pencil' title='Edit' color='blue' size='mini' onClick={() => { this.handleEditClick(res) }} />
                                            <Button icon='x' title='Delete' color='orange' size='mini' onClick={(e) => { confirmDelete(e, res.id, this) }} />
                                        </Table.Cell>
                                    </Table.Row>
                                ))
                        }
                       
                    </Table.Body>
                </Table>
                <Confirm
                    open={openConfirmDialog}
                    header='Delete'
                    content='Are you sure?'
                    onCancel={() => { onCancelDelete(this) }}
                    onConfirm={() => { onConfirmDelete(this, this.paths.deleteEntity) }} />
                <MessageModal
                    message={message}
                    open={messageModalOpen}
                    onClose={() => { onMessageModalClose(this, message) }}
                    isError={isError}
                />
            </React.Fragment>
        );
    }
}
export default EquipmentGrid;