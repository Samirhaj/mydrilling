﻿import * as React from 'react';
import { Button, Form, Input, Dropdown, Table } from 'semantic-ui-react';
import { map, find, cloneDeep } from 'lodash';
import { FormArea, ButtonFieldInline, ButtonInline } from "../common/CommonStyles";
import {
    handleInputChange,
    saveEntity,
    onCancelEditing,
    getEmptyQuotationItem,
    fetchAllEntitiesForDropdown,
    getuid,
    quotationRigStates,
    handleStateChange,
    handleSaveState,
    getRigStateText,
    handleRigStateFieldChange,
    onMessageModalClose,
    backToList
} from "../common/Utils";
import MessageModal from '../components/MessageModal';

interface IQuotationFormProps {
    onBack?:any,
    accessType?: number,
    editEntity?: any;    
}

interface IQuotationFormState {
    currentEntity?: QuotationItem,
    activeStateItem?: QuotationState,
    isFetchingEquipment: boolean,
    optionsEquipment?: any,
    messageModalOpen: boolean,
    isError?: boolean,
    message: string;
}

class QuotationForm extends React.Component<IQuotationFormProps, IQuotationFormState> {
    private paths = {
        postSaveEntity: '/upgradematrix/savequotationitem',
        fetchAllEquipmentCategories: '/upgradematrix/getequipmentcategories'
    };

    constructor(props: Readonly<IQuotationFormProps>) {
        super(props);
        this.state = {
            currentEntity: this.props.editEntity,
            isFetchingEquipment: false,
            messageModalOpen: false,
            message: ''
        };
    }

    componentDidMount() {
        fetchAllEntitiesForDropdown('isFetchingEquipment', 'optionsEquipment', this.paths.fetchAllEquipmentCategories, this, 'id', 'shortName');
    }

    render() {
        const { currentEntity, activeStateItem, isFetchingEquipment, optionsEquipment, message, messageModalOpen, isError } = this.state;
        return (
            <React.Fragment>
                <h3>Quotation Item</h3>
                {!currentEntity
                    ? <div>No data.</div>
                    : <Form onSubmit={(e) => { saveEntity(e, this) }}>
                    <Form.Field>
                        <label>Equipment</label>
                        <Dropdown
                            selection
                            search
                            name='equipmentCategoryId'
                            value={currentEntity.equipmentCategoryId ? currentEntity.equipmentCategoryId : ''}
                            options={optionsEquipment ? optionsEquipment : []}
                            loading={isFetchingEquipment}
                            disabled={isFetchingEquipment}
                            onChange={(e, data) => { handleInputChange(e, 'equipmentCategoryId', data.value, this) }}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Description</label>
                        <Input
                            name='description'
                            value={currentEntity.description ? currentEntity.description : ''}
                            onChange={(e, data) => { handleInputChange(e, 'description', data.value, this) }}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Quotation</label>
                        <Input
                              name='quotationReference'
                              value={currentEntity.quotationReference ? currentEntity.quotationReference : ''}
                              onChange={(e, data) => { handleInputChange(e, 'quotationReference', data.value, this) }}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Heading</label>
                        <Input
                            name='heading'
                            value={currentEntity.heading ? currentEntity.heading : ''}
                            onChange={(e, data) => { handleInputChange(e, 'heading', data.value, this) }}
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>Benefits</label>
                        <Input
                            name='benefits'
                            value={currentEntity.benefits ? currentEntity.benefits : ''}
                            onChange={(e, data) => { handleInputChange(e, 'benefits', data.value, this) }}
                        />
                    </Form.Field>
                    <Form.Field>
                        <Table>
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>Rig</Table.HeaderCell>
                                    <Table.HeaderCell>Status</Table.HeaderCell>
                                    <Table.HeaderCell></Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                            <Table.Body>
                                {map(currentEntity.rigStates, (rigState: QuotationState, ix:number) => (
                                    <Table.Row key={'rigState-' + ix}>
                                        <Table.Cell>{rigState.rigShortName}</Table.Cell>
                                        <Table.Cell>
                                            {(!activeStateItem || activeStateItem.elementIndex !== ix) && <span>{getRigStateText(rigState.status)}</span>}
                                            {activeStateItem && activeStateItem.elementIndex === ix
                                                &&
                                                <Dropdown
                                                    selection
                                                    name='status'
                                                    value={activeStateItem.status ? activeStateItem.status : ''}
                                                    options={quotationRigStates}
                                                    onChange={(e, data) => { handleRigStateFieldChange('status', data.value, this) }}
                                                />}
                                        </Table.Cell>
                                        <Table.Cell>
                                            {(!activeStateItem || activeStateItem.elementIndex !== ix) && <ButtonInline icon='pencil' title='Edit' onClick={(e: any) => { handleStateChange(e, this, ix, cloneDeep(rigState)); }} />}
                                            {activeStateItem && activeStateItem.elementIndex === ix && <ButtonInline icon='check' title='Save' onClick={(e: any ) => {handleSaveState(e, this)}} />}
                                            {activeStateItem && activeStateItem.elementIndex === ix && <ButtonInline icon='x' title='Cancel' onClick={(e: any) => { handleStateChange(e, this, 0, undefined); }} />}
                                        </Table.Cell>
                                    </Table.Row>
                                ))}
                            </Table.Body>
                        </Table>
                    </Form.Field>
                    {currentEntity.id > 0 && <ButtonFieldInline onClick={(e: any) => { onCancelEditing(e, this) }}>Cancel</ButtonFieldInline>}
                    <ButtonFieldInline positive>Save</ButtonFieldInline>
                </Form>}
                <MessageModal
                    message={message}
                    open={messageModalOpen}
                    onClose={() => { onMessageModalClose(this, message) }}
                    onBack={() => { backToList(this) }}
                    isError={isError}
                />
            </React.Fragment>            
        );
    }
}
export default QuotationForm;