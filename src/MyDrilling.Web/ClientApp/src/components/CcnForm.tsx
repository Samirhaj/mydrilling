﻿import * as React from 'react';
import { Button, Form, Input, Dropdown, Table } from 'semantic-ui-react';
import { map, find, findIndex, forEach, cloneDeep, filter } from 'lodash';
import axios from 'axios';
import { FormArea, ButtonFieldInline, ButtonInline } from "../common/CommonStyles";
import {
    handleInputChange,
    saveEntity,
    onCancelEditing,
    getuid,
    getPackageUpgradeText,
    rigTypes,
    fetchAllEntitiesForDropdown,
    getRigStateText,
    rigStates,
    handleResponse,
    handleError,
    getFileNameFromPath,
    handleStateChange,
    handleSaveState,
    handleRigStateFieldChange,
    onMessageModalClose,
    backToList
} from "../common/Utils";
import PackageUpgradeType from "../entities/PackageUpgradeType";
import RigType from "../entities/RigType";
import RigState from "../entities/RigState";
import FileUploadButton from "./FileUploadButton";
import MessageModal from '../components/MessageModal';

interface ICcnFormProps {
    onBack?:any,
    accessType?: number,
    editEntity?: CcnItem;    
}

interface ICcnFormState {
    currentEntity?: CcnItem,
    activeStateItem?: CcnState,
    isFetchingEquipment: boolean,   
    optionsEquipment?: any,
    messageModalOpen: boolean,
    isError?: boolean,
    message: string;
}

const packageUpgradeTypes = [
    { key: getuid(), text: getPackageUpgradeText(PackageUpgradeType.undefined), value: PackageUpgradeType.undefined },
    { key: getuid(), text: getPackageUpgradeText(PackageUpgradeType.performanceUpgrade), value: PackageUpgradeType.performanceUpgrade },
    { key: getuid(), text: getPackageUpgradeText(PackageUpgradeType.individual), value: PackageUpgradeType.individual },
    { key: getuid(), text: getPackageUpgradeText(PackageUpgradeType.dw), value: PackageUpgradeType.dw },
    { key: getuid(), text: getPackageUpgradeText(PackageUpgradeType.tmUpgrade), value: PackageUpgradeType.tmUpgrade },
    { key: getuid(), text: getPackageUpgradeText(PackageUpgradeType.mudPumps), value: PackageUpgradeType.mudPumps },
    { key: getuid(), text: getPackageUpgradeText(PackageUpgradeType.bulletin), value: PackageUpgradeType.bulletin } 
];

class CcnForm extends React.Component<ICcnFormProps, ICcnFormState> {
    private paths = {
        postSaveEntity: '/upgradematrix/saveccnitem',
        fetchAllEquipmentCategories: '/upgradematrix/getequipmentcategories',
        uploadCcnDocument: '/upgradematrix/uploadccndocument'
    };

    constructor(props: Readonly<ICcnFormProps>) {
        super(props);
        this.state = {
            currentEntity: this.props.editEntity,
            activeStateItem: undefined,
            isFetchingEquipment: false,
            messageModalOpen: false,
            message: ''
        };
    }

    componentDidMount() {
        fetchAllEntitiesForDropdown('isFetchingEquipment', 'optionsEquipment', this.paths.fetchAllEquipmentCategories, this, 'id', 'shortName');
    }

    onSelectCcnFile = (selectedFile: any, stateId: number) => {
        var activeStateItem  = this.state.activeStateItem;
        if (!activeStateItem || !stateId || activeStateItem.id !== stateId) return;
        activeStateItem.document = selectedFile;
        
        this.setState(({ activeStateItem: activeStateItem }) as any);
    }

    saveEntity = (e: any) => {
        e.preventDefault();
        const { currentEntity } = this.state;
        if (!currentEntity) {
            return false;
        }
        axios(this.paths.postSaveEntity, {
            method: 'POST',
            data: JSON.stringify(currentEntity),
            headers: { 'Content-Type': 'application/json' }
        })
        .then(handleResponse)
        .then(resp => {
            var savedItem = resp.item as CcnItem;
            var docErrorMessages = '';
            var statesWithDocs = filter(currentEntity.rigStates, (rigState: CcnState) => { return rigState.document != undefined; });
            if (statesWithDocs.length > 0) {
                forEach(statesWithDocs, (rigState: CcnState, ix: number) => {
                    let formData = new FormData();
                    formData.append('ccnDocument', rigState.document as File);
                    var rigStateId = 0;
                    if (rigState.id > 0) {
                        rigStateId = rigState.id;
                    } else {
                        const dbEntity = find(savedItem.rigStates, (rs) => { return rigState.rigSettingsId === rs.rigSettingsId; });
                        if (dbEntity)
                            rigStateId = dbEntity.id;
                    }
                    if (rigStateId > 0) {
                        axios(this.paths.uploadCcnDocument + '/' + rigStateId,
                            {
                                method: 'POST',
                                data: formData
                            })
                        .then(handleResponse)
                        .then(docResp => {
                            this.showMessageOnSavingFinished(ix, statesWithDocs, resp.message, docErrorMessages);
                        }).catch(docErr => {
                            docErrorMessages += docErr.response.data.message;
                            this.showMessageOnSavingFinished(ix, statesWithDocs, resp.message, docErrorMessages);
                        });
                    } else {
                        this.showMessageOnSavingFinished(ix, statesWithDocs, resp.message, docErrorMessages);
                    }
                });
            } else {
                this.setState(({ messageModalOpen: true, isError: false, message: resp.message }) as any);
            }
        }).catch(err => {
            handleError(err, this);
        });
    }

    showMessageOnSavingFinished = (itemIndex: number, statesWithDocs: CcnState[], respMessage: string, docErrorMessages: string) => {
        if (itemIndex === statesWithDocs.length - 1) {
            var msgToShow = respMessage + (docErrorMessages ? 'With the following errors on uploading documents: ' + docErrorMessages : '');
            this.setState(({ messageModalOpen: true, isError: false, message: msgToShow }) as any);
        }
    }

    render() {
        const { currentEntity, isFetchingEquipment, optionsEquipment, activeStateItem, message, messageModalOpen, isError } = this.state;
        return (
            <React.Fragment>
                <h3>CCN Item</h3>
                {!currentEntity
                    ? <div>No data.</div>
                    : <Form onSubmit={this.saveEntity}>
                          <Form.Field>
                              <label>Equipment</label>
                              <Dropdown
                                  selection
                                  search
                                  name='equipmentCategoryId'
                                  value={currentEntity.equipmentCategoryId ? currentEntity.equipmentCategoryId : ''}
                                  options={optionsEquipment ? optionsEquipment : []}
                                  loading={isFetchingEquipment}
                                  disabled={isFetchingEquipment}
                                  onChange={(e, data) => { handleInputChange(e, 'equipmentCategoryId', data.value, this) }}
                              />
                          </Form.Field>
                          <Form.Field>
                              <label>Description</label>
                              <Input
                                  name='description'
                                  value={currentEntity.description ? currentEntity.description : ''}
                                  onChange={(e, data) => { handleInputChange(e, 'description', data.value, this) }}
                              />
                          </Form.Field>
                          <Form.Field>
                              <label>Comments</label>
                              <Input
                                  name='comments'
                                  value={currentEntity.comments ? currentEntity.comments : ''}
                                  onChange={(e, data) => { handleInputChange(e, 'comments', data.value, this) }}
                              />
                          </Form.Field>
                          <Form.Field>
                              <label>Package upgrade</label>
                              <Dropdown
                                  selection
                                  name='packageUpgrade'
                                  value={currentEntity.packageUpgrade ? currentEntity.packageUpgrade : ''}
                                  options={packageUpgradeTypes}
                                  onChange={(e, data) => { handleInputChange(e, 'packageUpgrade', data.value, this) }}
                              />
                          </Form.Field>
                          <Form.Field>
                              <label>Rig type</label>
                              <Dropdown
                                  selection
                                  name='rigType'
                                  value={currentEntity.rigType ? currentEntity.rigType : ''}
                                  options={rigTypes}
                                  onChange={(e, data) => { handleInputChange(e, 'rigType', data.value, this) }}
                              />
                          </Form.Field>
                          <Form.Field>
                              <Table>
                                  <Table.Header>
                                      <Table.Row>
                                          <Table.HeaderCell>Rig</Table.HeaderCell>
                                          <Table.HeaderCell>Status</Table.HeaderCell>
                                          <Table.HeaderCell>CCN</Table.HeaderCell>
                                          <Table.HeaderCell>Document</Table.HeaderCell>
                                          <Table.HeaderCell>Approved by</Table.HeaderCell>
                                          <Table.HeaderCell></Table.HeaderCell>
                                      </Table.Row>
                                  </Table.Header>
                                  <Table.Body>
                                      {map(currentEntity.rigStates, (rigState: CcnState, ix: number) => (
                                          <Table.Row key={'rigState-' + ix}>
                                              <Table.Cell>{rigState.rigShortName}</Table.Cell>
                                              <Table.Cell>
                                                  {(!activeStateItem || activeStateItem.elementIndex !== ix) && <span>{getRigStateText(rigState.status)}</span>}
                                                  {activeStateItem && activeStateItem.elementIndex === ix
                                                      && <Dropdown
                                                            selection
                                                            name='status'
                                                            value={activeStateItem.status ? activeStateItem.status : ''}
                                                            options={rigStates}
                                                            onChange={(e, data) => { handleRigStateFieldChange('status', data.value, this) }}
                                                            />}
                                              </Table.Cell>
                                              <Table.Cell>
                                                  {(!activeStateItem || activeStateItem.elementIndex !== ix) && <span>{rigState.ccnName}</span>}
                                                  {activeStateItem && activeStateItem.elementIndex === ix
                                                      && <Input
                                                            type='text'
                                                            name='ccnName'
                                                            value={activeStateItem.ccnName ? activeStateItem.ccnName : ''}
                                                            onChange={(e, data) => { handleRigStateFieldChange('ccnName', data.value, this) }}
                                                            />}
                                              </Table.Cell>
                                              <Table.Cell>
                                                  {(!activeStateItem || activeStateItem.elementIndex !== ix) && <span>{getFileNameFromPath(rigState.documentPath)}</span>}
                                                  {activeStateItem && activeStateItem.elementIndex === ix && <FileUploadButton
                                                      onSelectFile={(selectedFile: File) => { handleRigStateFieldChange('document', selectedFile, this) }}
                                                      buttonLabel='Select CCN'
                                                      fileName={activeStateItem ? getFileNameFromPath(activeStateItem.documentPath) : getFileNameFromPath(rigState.documentPath)}
                                                      selectedFile={activeStateItem.document}
                                                      elementId={ix.toString()}
                                                  />}
                                              </Table.Cell>
                                              <Table.Cell>{rigState.approvedBy}</Table.Cell>
                                              <Table.Cell>
                                                  {(!activeStateItem || activeStateItem.elementIndex !== ix)  && <ButtonInline icon='pencil' title='Edit' onClick={(e: any) => { handleStateChange(e, this, ix, cloneDeep(rigState));}} />}
                                                  {activeStateItem && activeStateItem.elementIndex === ix && <ButtonInline icon='check' title='Save' onClick={(e: any) => { handleSaveState(e, this) }}  />}
                                                  {activeStateItem && activeStateItem.elementIndex === ix  && <ButtonInline icon='x' title='Cancel' onClick={(e:any) => { handleStateChange(e, this, 0, undefined); }} />}
                                              </Table.Cell>
                                          </Table.Row>
                                    ))}
                                  </Table.Body>
                              </Table>
                          </Form.Field>
                          {currentEntity.id > 0 && <ButtonFieldInline onClick={(e: any) => { onCancelEditing(e, this) }}>Cancel</ButtonFieldInline>}
                          <ButtonFieldInline positive>Save</ButtonFieldInline>
                      </Form>
                }
                <MessageModal
                    message={message}
                    open={messageModalOpen}
                    onClose={() => { onMessageModalClose(this, message) }}
                    onBack={() => { backToList(this) }}
                    isError={isError}
                />
            </React.Fragment>
        );
    }
}
export default CcnForm;