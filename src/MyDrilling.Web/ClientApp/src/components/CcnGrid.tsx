﻿import * as React from 'react';
import { Table, Icon, Loader, Dropdown, Button, Confirm } from 'semantic-ui-react';
import styled from 'styled-components';
import { map, find, filter, findIndex, uniq, orderBy } from 'lodash';
import axios from 'axios';
import {
    getGridData,
    getuid,
    createSimpleOptionItems,
    getEmptyCcnItem,
    getPackageUpgradeText,
    getRigStateText,
    onCancelDelete,
    onConfirmDelete,
    confirmDelete,
    handleResponse,
    handleError,
    onMessageModalClose,
    handleEnquiryClick
} from "../common/Utils";
import RigState from "../entities/RigState";
import RigType from "../entities/RigType";
import PackageUpgradeType from "../entities/PackageUpgradeType";
import ItemStatusType from '../entities/ItemStatusType';
import { FilterDropdownWrapper, ButtonInline, ButtonCell, EnquiryButton } from "../common/CommonStyles";
import MessageModal from '../components/MessageModal';

interface ICcnGridProps {
    accessType?: number,
    customerId: number,
    itemStatus: number,
    rigType?: number,
    rigId?: number, 
    activeRigs?: Rig[],
    isEditMode: boolean,
    onEdit: any,
    onDelete: any,
    currentUser?: User;
}

interface ICcnGridState {
    data?: CcnItem[],
    itemsToShow?: CcnItem[],
    selectedEquipment?: string, 
    equipmentOptions?: any,
    showPackageUpgrade: boolean,
    customerId: number,
    itemStatus: number,
    rigType?: number,
    rigId?: number,
    activeRigs?: Rig[],
    isEditMode: boolean,
    openConfirmDialog: boolean,
    openApproveConfirmDialog: boolean,
    itemToDelete?: number,
    messageModalOpen: boolean,
    isError?: boolean,
    message: string,
    rigStateItemClickedId?: number,
    approveStateId?: number,
    approveCcnItem?: CcnItem;
}

const RigStatetWrapper = styled.span`
   display:block;    
`;

const CcnInlineWrapper = styled.span`
    display: inline !important;
`;

const CcnButton = styled(Button)`
    border: none !important;
    box-shadow: 0 0 !important;
    display: inline !important;
    margin: 1px !important;
    padding: 1px !important; 
`;

class CcnGrid extends React.Component<ICcnGridProps, ICcnGridState> {
    private paths = {
        fetchAllCcnItems: '/upgradematrix/getccnitems',
        getCcnDownloadLink: '/upgradematrix/getdownloadlink',
        deleteEntity: '/upgradematrix/deleteccnitem',
        approveCcnForRig: '/upgradematrix/approveccnforrig'
    };

    constructor(props: Readonly<ICcnGridProps>) {
        super(props);
        this.state = {
            data: undefined,
            itemsToShow: undefined,
            selectedEquipment: undefined,
            equipmentOptions: undefined,
            showPackageUpgrade: true,
            customerId: this.props.customerId,
            itemStatus: this.props.itemStatus,
            rigType: this.props.rigType,
            rigId: this.props.rigId,
            activeRigs: this.props.activeRigs,
            isEditMode: this.props.isEditMode,
            openConfirmDialog: false,
            openApproveConfirmDialog: false,
            messageModalOpen: false,
            message: ''
        };
    }   

    static getDerivedStateFromProps(props: Readonly<ICcnGridProps>, state: ICcnGridState) {
        if (props.customerId !== state.customerId
            || props.itemStatus !== state.itemStatus
            || props.rigType !== state.rigType
            || props.rigId !== state.rigId
            || props.isEditMode !== state.isEditMode) {
            return {
                data: (props.customerId !== state.customerId || props.itemStatus !== state.itemStatus) ? undefined : state.data,
                itemsToShow: (props.customerId !== state.customerId || props.itemStatus !== state.itemStatus || props.rigType !== state.rigType) ? undefined : state.itemsToShow,
                selectedEquipment: (props.customerId !== state.customerId || props.itemStatus !== state.itemStatus) ? undefined : state.selectedEquipment,
                customerId: props.customerId,
                itemStatus: props.itemStatus,
                rigType: props.rigType,
                rigId: props.rigId,
                activeRigs: props.activeRigs,
                isEditMode: props.isEditMode
            };
        }
        return null;
    }

    public componentDidMount = () => { 
        this.getCcnItems();
    }

    componentDidUpdate(prevProps: Readonly<ICcnGridProps>, prevState: Readonly<ICcnGridState>) {
        if (this.state.data == undefined) {
            this.getCcnItems();
        } else if (this.state.itemsToShow == undefined) {
            this.onGetCcnItems(this.state.data, this);
        }
    }

    getCcnItems() {
        var getDataPath = this.paths.fetchAllCcnItems + '?customerId=' + this.state.customerId + '&open=' + (this.state.itemStatus == ItemStatusType.open).toString().toLowerCase();
        getGridData(this, getDataPath, this.onGetCcnItems);
    }

    onGetCcnItems = (items: CcnItem[], instance: any) => {
        const { rigType } = this.state;
        var showPackageUpgrade = true;
        var resItems = items;
        var equipmentOptions = undefined;
        if (resItems && resItems.length > 0) {
            if (rigType != undefined)
                resItems = filter(resItems, (i: CcnItem) => { return i.rigType == rigType || i.rigType == RigType.undefined; });
            var equipmentList = uniq(map(resItems, (item: CcnItem) => { return item.equipmentCategoryName }));
            equipmentOptions = createSimpleOptionItems(map(equipmentList, (eq) => { return { id: eq, name: eq }; }));
            equipmentOptions = orderBy(equipmentOptions, 'text', 'asc');
            equipmentOptions.unshift({ key: getuid(), text: '- All -', value: '' });
            showPackageUpgrade = findIndex(resItems, (i: CcnItem) => { return i.packageUpgrade !== PackageUpgradeType.undefined; }) > -1;
        }
        instance.setState(({ data: items, itemsToShow: resItems, showPackageUpgrade: showPackageUpgrade, equipmentOptions: equipmentOptions} as any));
    }

    getStatusFieldForRig(ccnItem: CcnItem, rig: Rig, rigStateItemClickedId?: number ) {
        var rigStates = ccnItem.rigStates;
        var rigState = find(rigStates, (rs: CcnState) => { return rs.rigSettingsId === rig.id });
        var rigStatus = rigState ? rigState.status : RigState.undefined as number;
        if (rigStatus === RigState.undefined && rigState && rigState.enquiryCreationEnabled) {
            return <React.Fragment>
                <EnquiryButton compact color='grey' size='mini' icon='question circle' content='Enquiry' onClick={() => { handleEnquiryClick("Upgrade software for " + ccnItem.description, rig.referenceId) }} />
            </React.Fragment>;
        }
        var docName = rigState ? rigState.ccnName : '';
        var rigStateId = rigState ? rigState.id : 0;
        if (rigStateItemClickedId && rigStateItemClickedId === rigStateId) {
            return <React.Fragment>
                       <Loader active inline='centered' />
                   </React.Fragment>;
        }
        var canApprove = this.hasApproveAccess(rig.rigId);
        var documentContent = <RigStatetWrapper>{
            rigState && docName
                ? (rigState.documentPath
                    ? <CcnButton basic size='tiny' content={docName} onClick={() => { this.handleDownloadClick(rigStateId, docName) }} />
                    : <CcnInlineWrapper>{docName}</CcnInlineWrapper>)
                : ''
        }</RigStatetWrapper>;

        var rigStatusContent = undefined;
        var approveLinkContent = undefined;
        switch (rigStatus) {
            case RigState.na:
                rigStatusContent = <span>{getRigStateText(RigState.na)}</span>;
                break;
            case RigState.ready:
                rigStatusContent = <Icon name='circle' color='red' title={getRigStateText(RigState.ready)} />;
                if(canApprove)
                    approveLinkContent = <CcnButton basic size='tiny' content='Approve' onClick={(e: any) => { this.confirmApprove(e, rigStateId, ccnItem) }}/>;
                break;
            case RigState.approved:
                rigStatusContent = <Icon name='circle' color='yellow' title={getRigStateText(RigState.approved) + (rigState && rigState.approvedBy ? ' (by ' + rigState.approvedBy + ')' : '')} />;
                break;
            case RigState.completed:
                rigStatusContent = <Icon name='circle' color='green' title={getRigStateText(RigState.completed)} />;
                break;
        }
        return <React.Fragment>
                    {documentContent}
                    {rigStatusContent && <RigStatetWrapper>{rigStatusContent}</RigStatetWrapper>}
                    {approveLinkContent && <RigStatetWrapper>{approveLinkContent}</RigStatetWrapper>}
                </React.Fragment>;
    }

    onEquipmentChanged = (e: any, data: any) => {
        var selectedEquipment = data.value ? data.value : undefined;
        this.setState(({ selectedEquipment: selectedEquipment }) as any);
    }

    handleEditClick = (itemForEditing: CcnItem) => {
        const { onEdit } = this.props;
        if (onEdit) {
            onEdit(itemForEditing);
        }
    }

    handleDeleteClick = (itemId: number) => {
        const { onDelete } = this.props;
        if (onDelete) {
            onDelete(itemId);
        }
    }

    handleDownloadClick = (itemId: number, ccnName: string) => {
        this.setState(({ rigStateItemClickedId: itemId }) as any);
        axios.get(this.paths.getCcnDownloadLink + '/' + itemId)
            .then(handleResponse)
            .then(resp => {
                var downloadLink = resp.downloadLink ? resp.downloadLink : '';
                if (downloadLink) {
                    location.href = downloadLink;
                }
            }).catch(err => {
                handleError(err, this);
            }).then(resp => {
                this.setState(({ rigStateItemClickedId: undefined }) as any);
            });
    }

    confirmApprove = (e: any, stateId: number, ccnItem: CcnItem) => {
        e.preventDefault();
        this.setState(({ openApproveConfirmDialog: true, approveStateId: stateId, approveCcnItem: ccnItem }) as any);
    }

    onCancelApprove = () => {
        this.setState(({ openApproveConfirmDialog: false, approveStateId: undefined, approveCcnItemId: undefined }) as any);
    }

    handleApproveClick = (stateId: number, ccnItemId: number) => {
        this.setState(({ rigStateItemClickedId: stateId, openApproveConfirmDialog: false, approveStateId: undefined, approveCcnItem: undefined }) as any);
        axios(this.paths.approveCcnForRig + '/' + stateId , {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' }
        })
        .then(handleResponse)
        .then(resp => {
            //update row status icon
            var itemsToShow = this.state.itemsToShow;
            var selectedCcnItem = find(itemsToShow, (item: CcnItem) => { return item.id === ccnItemId; });
            if (selectedCcnItem) {
                var selectedCcnState = find(selectedCcnItem.rigStates, (state: CcnState) => { return state.id === stateId; }); 
                if (selectedCcnState) {
                    selectedCcnState.status = RigState.approved;
                    this.setState(({ itemsToShow: itemsToShow }) as any);
                }
            }
        }).catch(err => {
            handleError(err, this);
        }).then(resp => {
            this.setState(({ rigStateItemClickedId: undefined }) as any);
        });
    }

    hasApproveAccess = (rigId : number) => {
        const { currentUser } = this.props;
        if (!currentUser) return false;
        if (currentUser.isUpgradeMatrixAdmin) return true;
        return find(currentUser.editorOnRigs, (editorRigId: number) => { return editorRigId === rigId; }) != null;
    }

    render() {
        const { customerId, itemsToShow, activeRigs, rigId, showPackageUpgrade, equipmentOptions, selectedEquipment,
            isEditMode, openConfirmDialog, message, messageModalOpen, isError, rigStateItemClickedId,
            openApproveConfirmDialog, approveStateId, approveCcnItem
        } = this.state;
        var resItems = itemsToShow;
        if (selectedEquipment) {
            resItems = filter(resItems, (i: CcnItem) => { return i.equipmentCategoryName === selectedEquipment });
        }
        var rigsInGrid = activeRigs;
        if (rigsInGrid && rigsInGrid.length > 0 && rigId != undefined)
            rigsInGrid = filter(rigsInGrid, (r: Rig) => { return r.id == rigId; });
        var approveDialogText = '';
        if (openApproveConfirmDialog && approveCcnItem) {
            approveDialogText = 'Are you sure you want to approve item ' +
                (approveCcnItem as CcnItem).equipmentCategoryName +
                ' - ' +
                (approveCcnItem as CcnItem).description.substring(0, 30) +
                '... ?';
        }
        return (
            <React.Fragment>
            <Table celled striped>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>
                            <FilterDropdownWrapper
                            text='Equipment'
                            icon='filter'
                            value={selectedEquipment ? selectedEquipment : ''}
                            options={equipmentOptions && equipmentOptions.length > 0 ? equipmentOptions : []}
                            onChange={this.onEquipmentChanged}
                            />
                        </Table.HeaderCell>
                        <Table.HeaderCell>Description</Table.HeaderCell>
                        <Table.HeaderCell>Comments</Table.HeaderCell>
                        {!resItems || resItems.length === 0
                            ? <Table.HeaderCell>Rig</Table.HeaderCell>
                            : map(rigsInGrid, (rig: Rig) => (                                        
                                <Table.HeaderCell key={getuid()}>{rig.shortName}</Table.HeaderCell>                                
                            ))}
                        {showPackageUpgrade && <Table.HeaderCell width={1}>PackageUpgrade</Table.HeaderCell>}
                        {isEditMode && <ButtonCell>
                                           <Button color='blue' icon='plus' content='Add' size='mini' onClick={() => {this.handleEditClick(getEmptyCcnItem(customerId))}} />
                                       </ButtonCell>}
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                {!resItems
                    ? <Table.Row>
                          <Table.Cell colSpan={isEditMode ? '6' : '5'} textAlign='center'><Loader active inline='centered' /></Table.Cell>
                      </Table.Row>
                    : resItems.length === 0
                    ? <Table.Row>
                          <Table.Cell colSpan={isEditMode ? '6' : '5'} textAlign='center'>No data.</Table.Cell>
                      </Table.Row>
                    : map(resItems, (res: CcnItem) => (
                        <Table.Row key={getuid()}>
                            <Table.Cell>{res.equipmentCategoryName}</Table.Cell>
                            <Table.Cell>{res.description}</Table.Cell>
                            <Table.Cell>{res.comments}</Table.Cell>
                            {map(rigsInGrid, (rig: Rig) => (
                                <Table.Cell key={getuid()} textAlign='center'>{this.getStatusFieldForRig(res, rig, rigStateItemClickedId)}</Table.Cell>
                        ))}
                            {showPackageUpgrade && <Table.Cell>{getPackageUpgradeText(res.packageUpgrade)}</Table.Cell>}
                            {isEditMode && <Table.Cell>
                        <ButtonInline icon='pencil' color='blue' size='mini' onClick={() => { this.handleEditClick(res) }} />
                        <ButtonInline icon='x' color='orange' size='mini' onClick={(e: any) => { confirmDelete(e, res.id, this) }} />
                        </Table.Cell>}
                        </Table.Row>
                    ))}
                </Table.Body>
            </Table>
            <Confirm
                open={openConfirmDialog}
                header='Delete'
                content='Are you sure?'
                onCancel={() => { onCancelDelete(this) }}
                onConfirm={() => { onConfirmDelete(this, this.paths.deleteEntity) }} />
            <Confirm
                open={openApproveConfirmDialog}
                header='Approve'
                content={approveDialogText}
                onCancel={this.onCancelApprove}
                onConfirm={() => { this.handleApproveClick(approveStateId as number, (approveCcnItem as CcnItem).id); }} />
            <MessageModal
                message={message}
                open={messageModalOpen}
                onClose={() => { onMessageModalClose(this, message) }}
                isError={isError}
            />
        </React.Fragment>            
        );
    }
}
export default CcnGrid;