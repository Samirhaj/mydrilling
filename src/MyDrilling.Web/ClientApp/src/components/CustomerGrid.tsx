﻿import * as React from 'react';
import { Button, Table, Confirm, Loader } from 'semantic-ui-react';
import { map, forEach } from 'lodash';
import styled from 'styled-components';
import { getGridData, getuid, getRigTypeText, onCancelDelete, onConfirmDelete, confirmDelete, onMessageModalClose } from "../common/Utils";
import MessageModal from '../components/MessageModal';

const MultiCell = styled(Table.Cell)`
   span { display:block !important; }
`;

interface ICustomerGridProps {
    accessType?: number,
    onEdit: any,
    onDelete: any;
}

interface ICustomerGridState {
    openConfirmDialog: boolean,
    data?: Customer[],
    itemToDelete?: number,
    messageModalOpen: boolean,
    isError?: boolean,
    message: string;
}

const archivedText = 'Archived';

class CustomerGrid extends React.Component<ICustomerGridProps, ICustomerGridState> {
    private paths = {
        fetchAllCustomers: '/upgradematrix/getcustomersettings',
        archiveEntity: '/upgradematrix/archivecustomersettings'
    };

    constructor(props: Readonly<ICustomerGridProps>) {
        super(props);
        this.state = {  
            openConfirmDialog: false,
            data: undefined,
            messageModalOpen: false,
            message: ''
        };
    }

    handleEditClick = (itemForEditing: Customer) => {
        const { onEdit } = this.props;
        if (onEdit) {
            onEdit(itemForEditing);
        }
    }

    handleDeleteClick = (itemId: number) => {
        const { onDelete } = this.props;
        if (onDelete) {
            onDelete(itemId);
        }
    }

    public componentDidMount() {
        getGridData(this, this.paths.fetchAllCustomers);
    }

    onArchiveSuccess = (instance: any, message: string) => {
        const { data, itemToDelete } = instance.state;
        forEach(data, (customer: Customer) => {
            if (customer.id === itemToDelete) {
                customer.status = archivedText;
            }
        });
        instance.setState({ openConfirmDialog: false, itemToDelete: null, messageModalOpen: true, isError: false, message: message, data: data });
    }

    render() {
        const { data, openConfirmDialog, message, messageModalOpen, isError } = this.state;
        return (
            <React.Fragment>
                <Table celled striped>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell>Customer</Table.HeaderCell>
                            <Table.HeaderCell>Rigs</Table.HeaderCell>
                            <Table.HeaderCell>Short name</Table.HeaderCell>
                            <Table.HeaderCell>Type</Table.HeaderCell>
                            <Table.HeaderCell>Status</Table.HeaderCell>
                            <Table.HeaderCell></Table.HeaderCell>
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {!data
                            ? <Table.Row>
                                <Table.Cell colSpan='5'><Loader active inline='centered' /></Table.Cell>
                            </Table.Row>
                            : data.length === 0
                                ? <Table.Row>
                                    <Table.Cell colSpan='5' textAlign='center'>No data.</Table.Cell>
                                </Table.Row>
                                : map(data, (res: Customer) => (
                                    <Table.Row key={getuid()}>
                                        <Table.Cell>
                                            {res.name}
                                        </Table.Cell>
                                        <MultiCell>
                                            {map(res.rigs, (rig: Rig) => (
                                                <span key={getuid()}>{rig.name + ' - ' + rig.referenceId}</span>
                                            ))}
                                        </MultiCell>
                                        <MultiCell>
                                            {map(res.rigs, (rig: Rig) => (
                                                <span key={getuid()}>{rig.shortName}</span>
                                            ))}
                                        </MultiCell>
                                        <MultiCell>
                                            {map(res.rigs, (rig: Rig) => (
                                                <span key={getuid()}>{getRigTypeText(rig.rigType)}</span>
                                            ))}
                                        </MultiCell>
                                        <Table.Cell>
                                            {res.status}
                                        </Table.Cell>
                                        <Table.Cell textAlign='center'>
                                            <Button icon='pencil' title='Edit' color='blue' size='mini' onClick={() => { this.handleEditClick(res) }} />
                                            {res.status !== archivedText && <Button icon='x' title='Archive' color='orange' size='mini' onClick={(e) => { confirmDelete(e, res.id, this) }} />}
                                        </Table.Cell>
                                    </Table.Row>
                                ))
                        }
                      
                    </Table.Body>
                </Table>
                <Confirm
                    open={openConfirmDialog}
                    header='Archive'
                    content='Are you sure?'
                    onCancel={() => { onCancelDelete(this) }}
                    onConfirm={() => { onConfirmDelete(this, this.paths.archiveEntity, this.onArchiveSuccess) }} />
                <MessageModal
                    message={message}
                    open={messageModalOpen}
                    onClose={() => { onMessageModalClose(this, message) }}
                    isError={isError}
                />
            </React.Fragment>
            
        );
    }
}
export default CustomerGrid;