﻿import * as React from 'react';
import { Button, Modal } from 'semantic-ui-react';
import styled from 'styled-components';

interface IAddEntityMessageModalProps {    
    onClose: any,
    open: boolean,
    message: string,
    onBack?: any,
    isError?: boolean;
}

const MessageWrapper = styled.div`
    margin: 5px;
    overflow-wrap: break-word;
    word-wrap: break-word;
    word-break: break-word;
`;

class MessageModal extends React.Component<IAddEntityMessageModalProps, any> {  
    closeModal = () => {
        const { onClose } = this.props;
        if (onClose)
            onClose();        
    };

    render() {
        const { onClose, message, isError, open, onBack } = this.props;

        return (
            <Modal size='tiny' open={open} closeIcon onClose={onClose}>
                <Modal.Header>{isError ? 'Error' : 'Info'}</Modal.Header>
                <Modal.Content>
                    <MessageWrapper>{message}</MessageWrapper>
                </Modal.Content>
                <Modal.Actions>
                    {onBack && <Button color='blue' icon='arrow left' labelPosition='left' content='Back to list' onClick={onBack} />}
                    {!isError && <Button positive content='OK' onClick={this.closeModal} /> } 
                    {isError && <Button negative content='OK' onClick={this.closeModal} /> } 
                </Modal.Actions>
            </Modal>
        );
    }
}
export default MessageModal;