﻿import * as React from 'react';
import { Button, Form, Input } from 'semantic-ui-react';
import { map } from 'lodash';
import { FormArea, ButtonFieldInline } from "../common/CommonStyles";
import { handleInputChange, saveEntity, onCancelEditing, onMessageModalClose, backToList } from "../common/Utils";
import MessageModal from '../components/MessageModal';

interface IEquipmentFormProps {
    onBack?: any,
    accessType?: number,
    editEntity?: EquipmentCategory;
}

interface IEquipmentFormState {
    currentEntity: EquipmentCategory,
    messageModalOpen: boolean,
    isError?: boolean,
    message: string;
}

class EquipmentForm extends React.Component<IEquipmentFormProps, IEquipmentFormState> {
    private paths = {
        postSaveEntity: '/upgradematrix/saveequipmentcategory'
    };

    constructor(props: Readonly<IEquipmentFormProps>) {
        super(props);
        this.state = {            
            currentEntity: this.props.editEntity ? this.props.editEntity : this.getEmptyEquipmentCategory(),
            messageModalOpen: false,
            message: ''
        };
    }

    getEmptyEquipmentCategory = (): EquipmentCategory => {
        return {
            id: 0,
            shortName: ""
        } as EquipmentCategory;
    }

    render() {
        const { currentEntity, message, messageModalOpen, isError } = this.state;
        return (
            <React.Fragment>
                <h3>Equipment</h3>
                <Form onSubmit={(e) => { saveEntity(e, this) }}>
                    <Form.Field>
                        <label>Short name</label>
                        <Input
                            name='shortName'
                            value={currentEntity ? currentEntity.shortName : ''}
                            onChange={(e, data) => { handleInputChange(e, 'shortName', data.value, this) }}
                        />
                    </Form.Field>
                    {currentEntity.id > 0 && <ButtonFieldInline onClick={(e: any) => { onCancelEditing(e, this) }}>Cancel</ButtonFieldInline>}
                    <ButtonFieldInline positive>Save</ButtonFieldInline>
                </Form>
                <MessageModal
                    message={message}
                    open={messageModalOpen}
                    onClose={() => { onMessageModalClose(this, message) }}
                    onBack={() => { backToList(this) }}
                    isError={isError}
                />
            </React.Fragment>
           
        );
    }
}
export default EquipmentForm;