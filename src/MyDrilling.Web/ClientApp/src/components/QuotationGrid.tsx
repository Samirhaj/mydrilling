﻿import * as React from 'react';
import { Button, Table, Icon, Loader, Confirm } from 'semantic-ui-react';
import { map, filter, find, findIndex, uniq, orderBy } from 'lodash';
import {
    getGridData,
    getuid,
    createSimpleOptionItems,
    getEmptyQuotationItem,
    onCancelDelete,
    onConfirmDelete,
    confirmDelete,
    onMessageModalClose,
    handleEnquiryClick
} from "../common/Utils";
import RigState from "../entities/RigState";
import PackageUpgradeType from "../entities/PackageUpgradeType";
import ItemStatusType from '../entities/ItemStatusType';
import { FilterDropdownWrapper, ButtonInline, ButtonCell, EnquiryButton } from "../common/CommonStyles";
import MessageModal from '../components/MessageModal';


interface IQuotationGridProps {
    accessType?: number,
    customerId: number,
    itemStatus: number,
    rigId?: number,
    activeRigs?: Rig[],
    isEditMode: boolean,
    onEdit: any,
    onDelete: any,
    showQuotationReference:boolean;
}

interface IQuotationGridState {
    data?: QuotationItem[],
    itemsToShow?: CcnItem[],
    selectedEquipment?: string,
    equipmentOptions?: any,
    customerId: number,
    itemStatus: number,
    rigId?: number,
    activeRigs?: Rig[],
    isEditMode: boolean,
    openConfirmDialog: boolean,
    itemToDelete?: number,
    messageModalOpen: boolean,
    isError?: boolean,
    message: string;
}

class CcnGrid extends React.Component<IQuotationGridProps, IQuotationGridState> {
    private paths = {
        fetchAllQuotationItems: '/upgradematrix/getquotationitems',
        deleteEntity: '/upgradematrix/deletequotationitem'
    };

    constructor(props: Readonly<IQuotationGridProps>) {
        super(props);
        this.state = {   
            data: undefined,
            itemsToShow: undefined,
            selectedEquipment: undefined,
            equipmentOptions: undefined,
            customerId: this.props.customerId,
            itemStatus: this.props.itemStatus,
            rigId: this.props.rigId,
            activeRigs: this.props.activeRigs,
            isEditMode: this.props.isEditMode,
            openConfirmDialog: false,
            messageModalOpen: false,
            message: ''
        };
    }

    static getDerivedStateFromProps(props: Readonly<IQuotationGridProps>, state: IQuotationGridState) {
        if (props.customerId !== state.customerId
            || props.itemStatus !== state.itemStatus
            || props.rigId !== state.rigId
            || props.isEditMode !== state.isEditMode) {
            return {
                data: (props.customerId !== state.customerId || props.itemStatus !== state.itemStatus) ? undefined : state.data,
                itemsToShow: (props.customerId !== state.customerId || props.itemStatus !== state.itemStatus) ? undefined : state.itemsToShow,
                selectedEquipment: (props.customerId !== state.customerId || props.itemStatus !== state.itemStatus) ? undefined : state.selectedEquipment,
                customerId: props.customerId,
                itemStatus: props.itemStatus,
                rigId: props.rigId,
                activeRigs: props.activeRigs,
                isEditMode: props.isEditMode
            };
        }

        return null;
    }

    public componentDidMount() {
        this.getQuotationItems();
    }

    componentDidUpdate(prevProps: Readonly<IQuotationGridProps>, prevState: Readonly<IQuotationGridState>) {        
        if (this.state.data == undefined) {
            this.getQuotationItems();
        }
    }

    getQuotationItems() {
        var getDataPath = this.paths.fetchAllQuotationItems + '?customerId=' + this.state.customerId + '&open=' + (this.state.itemStatus == ItemStatusType.open).toString().toLowerCase();
        getGridData(this, getDataPath, this.onGetQuotationItems);
    }

    onGetQuotationItems = (items: QuotationItem[], instance: any) => {
        var resItems = items;
        var equipmentOptions = undefined;
        if (resItems && resItems.length > 0) {
            var equipmentList = uniq(map(resItems, (item: QuotationItem) => { return item.equipmentCategoryName }));
            equipmentOptions = createSimpleOptionItems(map(equipmentList, (eq) => { return { id: eq, name: eq }; }));
            equipmentOptions = orderBy(equipmentOptions, 'text', 'asc');
            equipmentOptions.unshift({ key: getuid(), text: '- All -', value: '' });
        }
        instance.setState(({ data: items, itemsToShow: resItems, equipmentOptions: equipmentOptions } as any));
    }

    getStatusFieldForRig(quotationItem: QuotationItem, rig: Rig) {
        var rigState = find(quotationItem.rigStates, (rs: QuotationState) => { return rs.rigSettingsId == rig.id });
        var rigStatus = rigState ? rigState.status : RigState.undefined as number;
        if (rigStatus === RigState.undefined && rigState && rigState.enquiryCreationEnabled) {
            return <React.Fragment>
                       <EnquiryButton compact color='grey' size='mini' icon='question circle' content='Enquiry' onClick={() => { handleEnquiryClick(quotationItem.heading, rig.referenceId) }} />
                   </React.Fragment>;
        }
        var rigStatusContent = <span></span>;
        switch (rigStatus) {
        case RigState.na:
            rigStatusContent = <Icon name='circle' color='red' title='Not Applicable' />;
            break;
        case RigState.completed:
            rigStatusContent = <Icon name='circle' color='green' title='PO Received' />;
            break;
        case RigState.undefined:
            rigStatusContent = <span></span>;
            break;
        }
        return <React.Fragment>{rigStatusContent}</React.Fragment>;
    }

    onEquipmentChanged = (e: any, data: any) => {
        var selectedEquipment = data.value ? data.value : undefined;
        this.setState(({ selectedEquipment: selectedEquipment }) as any);
    }

    handleEditClick = (itemForEditing: QuotationItem) => {
        const { onEdit } = this.props;
        if (onEdit) {
            onEdit(itemForEditing);
        }
    }

    handleDeleteClick = (itemId: number) => {
        const { onDelete } = this.props;
        if (onDelete) {
            onDelete(itemId);
        }
    }

    render() {
        const { customerId, itemsToShow, activeRigs, rigId, equipmentOptions, selectedEquipment, isEditMode, openConfirmDialog, message, messageModalOpen, isError } = this.state;
        const { showQuotationReference } = this.props;
        var resItems = itemsToShow;
        if (selectedEquipment) {
            resItems = filter(resItems, (i: CcnItem) => { return i.equipmentCategoryName === selectedEquipment });
        }
        var rigsInGrid = activeRigs;
        if (rigsInGrid && rigsInGrid.length > 0 && rigId != undefined)
            rigsInGrid = filter(rigsInGrid, (r: Rig) => { return r.id == rigId; });
        return (
            <React.Fragment>
                <Table celled striped>
                    <Table.Header>
                        <Table.Row>
                            <Table.HeaderCell textAlign='center'>
                                <FilterDropdownWrapper
                                  text='Equipment'
                                  icon='filter'
                                  options={equipmentOptions && equipmentOptions.length > 0 ? equipmentOptions : []}
                                  onChange={this.onEquipmentChanged}
                                />
                            </Table.HeaderCell>
                            {showQuotationReference && <Table.HeaderCell textAlign='center'>Quotation</Table.HeaderCell>}
                            <Table.HeaderCell textAlign='center'>Heading</Table.HeaderCell>
                            <Table.HeaderCell textAlign='center'>Description</Table.HeaderCell>
                            <Table.HeaderCell textAlign='center'>ROI/Benefits</Table.HeaderCell>
                            {!resItems || resItems.length === 0
                                ? <Table.HeaderCell>Rig</Table.HeaderCell>
                                : map(rigsInGrid, (rig: Rig) => (
                                    <Table.HeaderCell key={getuid()}>{rig.shortName}</Table.HeaderCell>
                                ))}
                            {isEditMode && <ButtonCell><Button color='blue' icon='plus' content='Add' size='mini' onClick={() => { this.handleEditClick(getEmptyQuotationItem(customerId)) }} /></ButtonCell>}
                        </Table.Row>
                    </Table.Header>
                    <Table.Body>
                        {!resItems ? 
                            <Table.Row>
                                <Table.Cell colSpan={isEditMode ? '6' : '5'} textAlign='center'><Loader active inline='centered' /></Table.Cell>
                            </Table.Row>
                            : resItems.length === 0
                                ? <Table.Row>
                                    <Table.Cell colSpan={isEditMode ? '6' : '5'} textAlign='center'>No data.</Table.Cell>
                                </Table.Row>
                                : map(resItems, (res: QuotationItem) => (
                                    <Table.Row key={getuid()}>
                                        <Table.Cell>{res.equipmentCategoryName}</Table.Cell>
                                        <Table.Cell>{res.quotationReference}</Table.Cell>
                                        {showQuotationReference && <Table.Cell>{res.heading}</Table.Cell>}
                                        <Table.Cell>{res.description}</Table.Cell>
                                        <Table.Cell>{res.benefits}</Table.Cell>
                                        {map(rigsInGrid, (rig: Rig) => (
                                            <Table.Cell key={getuid()} textAlign='center'>{this.getStatusFieldForRig(res, rig)}</Table.Cell>
                                        ))}
                                        {isEditMode && <Table.Cell>
                                            <ButtonInline icon='pencil' title='Edit' color='blue' size='mini' onClick={() => { this.handleEditClick(res) }} />
                                            <ButtonInline icon='x' title='Delete' color='orange' size='mini' onClick={(e: any) => { confirmDelete(e, res.id, this) }} />
                                        </Table.Cell>}
                                    </Table.Row>
                                ))
                            }
                        {}
                    </Table.Body>
                </Table>            
                <Confirm
                    open={openConfirmDialog}
                    header='Delete'
                    content='Are you sure?'
                    onCancel={() => { onCancelDelete(this) }}
                    onConfirm={() => { onConfirmDelete(this, this.paths.deleteEntity) }} />
                <MessageModal
                    message={message}
                    open={messageModalOpen}
                    onClose={() => { onMessageModalClose(this, message) }}
                    isError={isError}
                />
            </React.Fragment>
        );
    }
}
export default CcnGrid;