﻿import * as React from 'react';
import { Label, Button, Icon } from 'semantic-ui-react';
import styled from "styled-components";
import { getFileNameFromPath } from "../common/Utils";


interface IFileUploadButtonProps {
    buttonLabel: string,
    elementId: string,
    onSelectFile: any,
    fileName?: string,
    selectedFile?: any;
}

interface IFileUploadButtonState {   
    selectedFile: any,     
    fileName: string;
}

const ButtonArea = styled.div`
    color:#000;
    background:#fff;    
    margin: 5px 0;    
    padding:5px;
    display:inline-block;   
`;

const DeleteButton = styled(Button)`
    margin-top: 3px !important;   
    margin-left: 3px !important;   
`;

const noFileLabel = 'No file selected.';

class FileUploadButton extends React.Component<IFileUploadButtonProps, IFileUploadButtonState> {

    constructor(props: Readonly<IFileUploadButtonProps>) {
        super(props);

        this.state = {            
            selectedFile: this.props.selectedFile,            
            fileName: this.props.fileName ? getFileNameFromPath(this.props.fileName) : noFileLabel
        };
    }

    static getDerivedStateFromProps(props: Readonly<IFileUploadButtonProps>, state: IFileUploadButtonState) {
        if (props.fileName !== state.fileName) {
            return {
                selectedFile: props.selectedFile,
                fileName: props.fileName ? getFileNameFromPath(props.fileName) : noFileLabel
            };
        }
        return null;
    }

    getUploadElementId = () => {
        const { elementId } = this.props;
        return 'upload-file-' + elementId;
    }

    onSelectFileClick = (e: any) => {
        var uploadInput = document.getElementById(this.getUploadElementId());
        if (uploadInput)
            uploadInput.click();
    }

    fileChangedHandler = (e : any) => {
        e.preventDefault();
        var selectedFile = e.target.files[0];
        const { onSelectFile, elementId } = this.props;
        if(onSelectFile)
            onSelectFile(selectedFile, elementId);
    }

    onRemoveFileClick = (e: any) => {
        e.preventDefault();
        const { onSelectFile, elementId } = this.props;
        if(onSelectFile)
            onSelectFile(undefined, elementId);
    }

    render() {
        const { fileName, selectedFile} = this.state;        
        var uploadElementId: string = this.getUploadElementId();
        return (
            <ButtonArea>             
                <Button as='div' labelPosition='right' onClick={this.onSelectFileClick}>
                    <Button icon='upload' />
                    <Label htmlFor={uploadElementId} as='a' basic pointing='left'>{this.props.buttonLabel}</Label>
                    <input hidden id={uploadElementId} type="file" accept="application/pdf,.doc,.docx" onChange={this.fileChangedHandler} />
                </Button>
                <div>
                    {fileName}
                    {(selectedFile || (fileName && fileName !== noFileLabel)) && <DeleteButton basic size='mini' icon='delete' onClick={this.onRemoveFileClick} />}
                </div>                
            </ButtonArea>
        );
    }
}
export default FileUploadButton;
