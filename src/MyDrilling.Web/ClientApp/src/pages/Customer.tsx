﻿import * as React from 'react';
import { Button, Table } from 'semantic-ui-react';
import CustomerGrid from '../components/CustomerGrid';
import CustomerForm from '../components/CustomerForm';

interface ICustomerPageState {
    showGrid: boolean,
    editEntity?: Customer;
}

class CustomerPage extends React.Component<any, ICustomerPageState> {
    constructor(props : any) {
        super(props);
        this.state = {
            showGrid: true,
            editEntity: undefined
        }
    }
    toggleHidden() {
        this.setState(({
            showGrid: !this.state.showGrid,
            editEntity: undefined
        }) as any);
    }

    onAddClick = () => {
        this.setState(({ showGrid: false, editEntity: undefined }) as any);
    }

    onEditClick = (editEntity: any) => {
        this.setState(({ showGrid: false, editEntity: editEntity }) as any);
    }

    onDeleteClick = (deleteEntityId: number) => {
        //this.setState(({ editEntity: editEntity }) as any);
        console.log('delete customer - ' + deleteEntityId);
    }

    onBack = () => {
        this.toggleHidden();
    };

    render() {
        const { showGrid, editEntity } = this.state;
        return (
            <React.Fragment>
                {showGrid && <Button icon='plus' color='blue' content='Add' onClick={this.onAddClick} />}
                {!showGrid && <Button icon='arrow left' content='Back' onClick={() => this.toggleHidden()} />}
                {showGrid && <CustomerGrid onEdit={this.onEditClick} onDelete={this.onDeleteClick} />}
                {!showGrid && <CustomerForm editEntity={editEntity} onBack={this.onBack} />}
            </React.Fragment>
        );
    }
}
export default CustomerPage;