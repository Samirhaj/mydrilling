﻿import * as React from 'react';
import { Button } from 'semantic-ui-react';
import EquipmentGrid from '../components/EquipmentGrid';
import EquipmentForm from '../components/EquipmentForm';


interface IEquipmentPageState {
    showGrid: boolean,
    editEntity?: EquipmentCategory;
}

class EquipmentPage extends React.Component<any, IEquipmentPageState> {
    constructor(props: any) {
        super(props);
        this.state = {
            showGrid: true,
            editEntity: undefined
        }
    }
    toggleHidden() {
        this.setState({
            showGrid: !this.state.showGrid,
            editEntity: undefined
        });
    }

    onBack = () => {
        this.toggleHidden();
    };

    onAddClick = () => {
        this.setState(({ showGrid: false, editEntity: undefined }) as any);
    }

    onEditClick = (editEntity: any) => {
        this.setState(({ showGrid: false, editEntity: editEntity }) as any);
    }

    onDeleteClick = (deleteEntityId: number) => {
        //this.setState(({ editEntity: editEntity }) as any);
        console.log('delete equipment - ' + deleteEntityId);
    }

    

    render() {
        const { showGrid, editEntity } = this.state;
        return (
            <React.Fragment>
                {showGrid && <Button icon='plus' color='blue' content='Add' onClick={this.onAddClick} />}
                {!showGrid && <Button icon='arrow left' content='Back' onClick={() => this.toggleHidden()} />}
                {showGrid && <EquipmentGrid onEdit={this.onEditClick} onDelete={this.onDeleteClick} />}
                {!showGrid && <EquipmentForm editEntity={editEntity} onBack={this.onBack} />}
            </React.Fragment>
        );
    }
}
export default EquipmentPage;