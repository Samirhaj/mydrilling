﻿import * as React from 'react';
import styled from 'styled-components';
import { Button, Dropdown, Table, Menu, Icon } from 'semantic-ui-react';
import { map, filter, find, head, indexOf, forEach, cloneDeep } from 'lodash';
import CcnGrid from '../components/CcnGrid';
import QuotationGrid from '../components/QuotationGrid';
import CcnForm from '../components/CcnForm';
import QuotationForm from '../components/QuotationForm';
import ItemStatusType from '../entities/ItemStatusType';
import RigType from '../entities/RigType';
import RigState from '../entities/RigState';
import { rigTypes, getuid, getGridData, createSimpleOptionItems } from "../common/Utils";

const UpgradeMatrixWrapper = styled.div`
   
`;

const DropDownWrapper = styled.div`
    float: left;
    margin-right: 15px;
    .ui.dropdown { 
        min-width:95px !important;        
    }
`;
const DropDownShort = styled(Dropdown)`    
    min-width:70px !important;      
   
`;
const StatusIconsWrapper = styled.div`
    vertical-align: text-top;
    float: right;
`;

interface IUpgradeMatrixProps {
    currentUser?: User;
}

interface IUpgradeMatrixState {
    isEditMode: boolean, 
    editEntity?: any,
    isFetchingCustomer?: boolean,
    searchQueryCustomer?: string,
    optionsCustomer: any,
    customerData?: Customer[],
    optionsRig?: any,
    activeRigs?: Rig[],
    optionsRigType?: any,
    selectedCustomerId?: number,
    selectedRigType?: number,
    selectedItemStatus?: number,
    selectedRig?: number,
    activeMenuItem?: string;
}

export const itemStatusTypes = [
    { key: getuid(), text: 'Open', value: ItemStatusType.open},
    { key: getuid(), text: 'Implemented', value: ItemStatusType.implemented }
];

class UpgradeMatrixPage extends React.Component<IUpgradeMatrixProps, IUpgradeMatrixState> {
    private paths = {
        fetchAllCustomers: '/upgradematrix/getcustomersettings?onlyActive=true'
    };

    constructor(props: Readonly<IUpgradeMatrixProps>) {
        super(props);
        this.state = {
            isEditMode: false,
            editEntity: undefined,
            isFetchingCustomer: false,
            searchQueryCustomer: undefined,
            optionsCustomer: undefined,
            customerData: undefined,
            selectedCustomerId: undefined,
            selectedRigType: undefined,
            selectedItemStatus: ItemStatusType.open,
            activeMenuItem: 'ccn'
        };
    }
    componentDidMount() {
        this.setState(({ isFetchingCustomer: true }) as any);
        getGridData(this, this.paths.fetchAllCustomers, this.onGetCustomerData);
    }

    onGetCustomerData(items: Customer[], instance: any) {
        instance.setDropdowns(true, instance, items);
    }

    setDropdowns(setCustomerDropdown: boolean, instance: any, items?: Customer[], selectedCustomerId?: number, selectedRigType?: number, activeMenuItem?: string) {
        var currentCustomerId = this.state.selectedCustomerId;
        var currentRigType = this.state.selectedRigType;
        var optionsRig,
            optionsRigType = undefined;
        var selectedTab = activeMenuItem ? activeMenuItem : this.state.activeMenuItem;
        var activeRigs = undefined;
        var resetRigId = selectedCustomerId !== currentCustomerId || selectedRigType !== currentRigType;
        if (items && items.length > 0) {
            var selectedCustomer = !selectedCustomerId
                ? head(items) as Customer 
                : find(items, (item) => { return item.customerId == selectedCustomerId });
            if (selectedCustomer) {
                if (!selectedCustomerId)
                    selectedCustomerId = selectedCustomer.customerId;
                activeRigs = selectedCustomer.rigs;
                if (selectedTab == 'ccn') {
                    var activeRigTypes = filter(map(activeRigs, (rig: Rig) => { return rig.rigType }), (rt) => { return rt !== RigType.undefined });
                    optionsRigType = filter(rigTypes, (rigType) => { return indexOf(activeRigTypes, rigType.value) > -1 });
                    if (optionsRigType && optionsRigType.length > 0) {
                        if (!selectedRigType)
                            selectedRigType = (head(optionsRigType) as any).value;
                        activeRigs = filter(activeRigs, (rig: Rig) => { return rig.rigType === selectedRigType });
                    }
                }
                optionsRig = createSimpleOptionItems(activeRigs, 'id', 'shortName');
            }
        }
        var stateObj = {
            isFetchingCustomer: false,
            selectedCustomerId: selectedCustomerId,
            optionsRig: optionsRig,
            activeRigs: activeRigs,
            selectedRigType: selectedRigType,
            optionsRigType: optionsRigType,
            customerData: instance.state.customerData,
            optionsCustomer: instance.state.optionsCustomer,
            selectedRig: resetRigId ? undefined : instance.state.selectedRig,
            activeMenuItem: selectedTab
        };
        if (setCustomerDropdown) {
            var optionsCustomer = createSimpleOptionItems(items, 'customerId');
            stateObj.customerData = items;
            stateObj.optionsCustomer = optionsCustomer;
        }
        instance.setState(stateObj as any);
    }

    handleCustomerChange = (e: any, data: any) => {
        this.setDropdowns(false, this, this.state.customerData, data.value);
    };
    handleRigTypeChange = (e: any, data: any) => {
        const { selectedCustomerId } = this.state;
        this.setDropdowns(false, this, this.state.customerData, selectedCustomerId, data.value);
    };
    handleRigChange = (e: any, data: any) => {
        this.setState(({ selectedRig: data.value ? data.value : undefined }) as any);
    };
    handleItemStatusChange = (e: any, data: any) => {
        this.setState(({ selectedItemStatus: data.value }) as any);
    };
    handleMenuItemClick = (e: any, data: any) => {
        const { selectedCustomerId, selectedRigType } = this.state;
        this.setDropdowns(false, this, this.state.customerData, selectedCustomerId, undefined, data.name);
    }
    handleEditingClick = (e: any, data: any) => {
        const { isEditMode } = this.state;
        this.setState(({ isEditMode: !isEditMode }) as any);
    }
    onEditClick = (editEntity: any) => {
        const { selectedCustomerId, customerData, selectedRigType } = this.state;
        var selectedCustomer = find(customerData, (item : Customer) => { return item.customerId === selectedCustomerId });
        if (selectedCustomer) {
            var activeRigs = selectedCustomer.rigs;
            var rigStates = [] as any;
            if (editEntity.id == 0) {
                //set up all rigs for selected customer, since we dont know which type of item it should be
                //if item stays with type Undefined, then it should be possible to set state for all rigs
                rigStates = map(activeRigs, (rig: Rig) => {
                    return {
                        id: 0,
                        rigSettingsId: rig.id,
                        rigShortName: rig.shortName,
                        status: RigState.undefined
                    }
                });
            } else {
                if (selectedRigType)
                    activeRigs = filter(activeRigs, (rig: Rig) => { return rig.rigType === selectedRigType });
                forEach(activeRigs, (rig: Rig) => {
                    //check if exists in editEntity.rigStates
                    var existingRigState = find(editEntity.rigStates, (state) => { return state.rigSettingsId === rig.id; });
                    if (existingRigState) {
                        rigStates.push(cloneDeep(existingRigState));
                    } else {
                        rigStates.push({
                            id: 0,
                            rigSettingsId: rig.id,
                            rigShortName: rig.shortName,
                            status: RigState.undefined
                        });
                    }
                    
                });
            }
            editEntity.rigStates = rigStates;
        }
        this.setState(({ editEntity: editEntity }) as any);
    }
    onDeleteClick = (deleteEntityId: number) => {
        //TODO
        //this.setState(({ editEntity: editEntity }) as any);
        console.log((this.state.activeMenuItem ? this.state.activeMenuItem : '') +  deleteEntityId);
    }
    handleBackClick = (e: any, data: any) => {
        this.setState(({ editEntity: undefined }) as any);
    }
 
    render() {
        const { currentUser } = this.props;
        const { isEditMode, editEntity, isFetchingCustomer, optionsCustomer, selectedCustomerId, selectedRigType, selectedItemStatus, activeMenuItem, activeRigs, optionsRig, optionsRigType, selectedRig } = this.state;
        const isAdmin = (currentUser && currentUser.isUpgradeMatrixAdmin) as boolean;
        return (
            <UpgradeMatrixWrapper>
                {editEntity === undefined && <React.Fragment>
                <Table basic='very'>
                    <Table.Body>
                        <Table.Row><Table.Cell><h3>UpgradeMatrix</h3></Table.Cell></Table.Row>
                        <Table.Row>
                            <Table.Cell>
                                <DropDownWrapper>
                                    <span>Customer:</span>
                                    <Dropdown
                                        search
                                        compact
                                        selection
                                        name='selectedCustomerId'
                                        value={selectedCustomerId && selectedCustomerId > 0 ? selectedCustomerId : ''}
                                        placeholder={'Select Customer...'}
                                        options={optionsCustomer && optionsCustomer.length > 0 ? optionsCustomer : []}
                                        disabled={isFetchingCustomer}
                                        loading={isFetchingCustomer}
                                        onChange={this.handleCustomerChange}
                                    />
                                </DropDownWrapper>
                                <DropDownWrapper>
                                    <span>Status:</span>
                                    <Dropdown
                                        compact
                                        selection
                                        value={selectedItemStatus ? selectedItemStatus : ''}
                                        name='name'
                                        options={itemStatusTypes && itemStatusTypes.length > 0 ? itemStatusTypes : []}
                                        onChange={this.handleItemStatusChange}
                                    />
                                </DropDownWrapper>
                                {activeMenuItem == 'ccn' && <DropDownWrapper>
                                    <span>Rig type:</span>
                                    <Dropdown
                                        compact
                                        selection
                                        value={selectedRigType ? selectedRigType : ''}
                                        name='name'
                                        options={optionsRigType && optionsRigType.length > 0 ? optionsRigType : []}
                                        disabled={!optionsRigType || optionsRigType.length === 0 }
                                        onChange={this.handleRigTypeChange}
                                    />
                                </DropDownWrapper>}
                                <DropDownWrapper>
                                    <span>Rig:</span>
                                    <DropDownShort
                                        compact
                                        clearable
                                        selection
                                        value={selectedRig ? selectedRig : ''}
                                        placeholder='Select Rig...'
                                        name='name'
                                        options={optionsRig && optionsRig.length > 0 ? optionsRig : []}
                                        onChange={this.handleRigChange}
                                    />
                                </DropDownWrapper>
                            </Table.Cell>
                            <Table.Cell>
                                {isAdmin && <Button color='blue' content={isEditMode ? 'Disable editing' : 'Enable editing'} icon='pencil' size='tiny' onClick={this.handleEditingClick} />}
                            </Table.Cell>
                            <Table.Cell>
                                <StatusIconsWrapper>
                                    <div>
                                        <Icon name='circle' color='green' title={activeMenuItem == 'ccn' ? 'Completed' : 'PO Received'} />Completed
                                    </div>
                                    {activeMenuItem == 'ccn' && <div>
                                        <Icon name='circle' color='yellow' title='Ready For Installation' />Approved customer
                                    </div>}
                                    <div>
                                        <Icon name='circle' color='red' title='Not Ready' />{activeMenuItem == 'ccn' ? 'Ready MHWirth' : 'Not Applicable'}
                                </div>
                                </StatusIconsWrapper>
                            </Table.Cell>
                        </Table.Row>
                    </Table.Body>
                </Table>
                <Menu>
                    <Menu.Item
                        name='ccn'
                        active={activeMenuItem === 'ccn'}
                        onClick={this.handleMenuItemClick}>
                        CCN
                    </Menu.Item>
                    <Menu.Item
                        name='quotations'
                        active={activeMenuItem === 'quotations'}
                        onClick={this.handleMenuItemClick}>
                        Quotations
                    </Menu.Item>
                </Menu>
                {activeMenuItem === 'ccn' && selectedCustomerId && selectedCustomerId > 0
                    && <CcnGrid
                        currentUser={currentUser}
                        customerId={selectedCustomerId}
                        itemStatus={selectedItemStatus as number}
                        rigType={selectedRigType}
                        rigId={selectedRig}
                        activeRigs={activeRigs}
                        isEditMode={isEditMode}
                        onEdit={this.onEditClick}
                        onDelete={this.onDeleteClick} />}
                {activeMenuItem === 'quotations' && selectedCustomerId && selectedCustomerId > 0
                    && <QuotationGrid
                        customerId={selectedCustomerId}
                        itemStatus={selectedItemStatus as number}
                        rigId={selectedRig}
                        activeRigs={activeRigs}
                        isEditMode={isEditMode}
                        onEdit={this.onEditClick}
                        onDelete={this.onDeleteClick}
                        showQuotationReference={isAdmin}
                        />}
                </React.Fragment>}
                {editEntity !== undefined && <React.Fragment>
                    <Button content='Back' icon='arrow left' onClick={this.handleBackClick}/>
                    {activeMenuItem === 'ccn' && <CcnForm editEntity={editEntity} onBack={this.handleBackClick} />}
                    {activeMenuItem === 'quotations' && <QuotationForm editEntity={editEntity} onBack={this.handleBackClick} />}
                </React.Fragment>}
            </UpgradeMatrixWrapper>
        );
    }
}
export default UpgradeMatrixPage;