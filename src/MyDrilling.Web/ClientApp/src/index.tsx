﻿import * as React from 'react';
import { render } from 'react-dom';
import MainLayout from './common/MainLayout';

render(<MainLayout />, document.querySelector('#react-root'));
