﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Ricon;
using MyDrilling.Core.Entities.Rig;
using PdfSharpCore;
using PdfSharpCore.Drawing;
using PdfSharpCore.Pdf;

namespace MyDrilling.Web.Helpers
{
    public static class RiconPdfCreator
    {
        public static MemoryStream ExportCbmRiserToPdf(List<Joint> items, Rig rig, string webRootPath)
        {
            MemoryStream stream = new MemoryStream();

            if (rig == null) return stream;

            // Create new PDF document
            PdfDocument document = new PdfDocument();
            document.Info.Title = "RiCon CoC";
            document.Info.Author = "MHwirth - myDrilling";
            string dateStamp = DateTime.Now.ToString("MMMM dd, yyyy hh:mm", CultureInfo.InvariantCulture);
            document.Info.Subject = "Server time: " + dateStamp;

            // A4, page height is 29.7 cm
            LayoutHelper helper = new LayoutHelper(document, XUnit.FromCentimeter(3.3), XUnit.FromCentimeter(29.7 - (6 - 2.33)), webRootPath);
            XUnit left = XUnit.FromCentimeter(2.3);
            const int headerFontSize = 16;
            const int normalFontSize = 11;
            XFont fontHeader = new XFont("Arial", headerFontSize, XFontStyle.Bold);
            XFont fontNormal = new XFont("Arial", normalFontSize, XFontStyle.Regular);
            XPen innerLine = new XPen(XColors.Black, 0.5);
            XImage riconImage = XImage.FromFile(Path.Combine(webRootPath, "graphics/ricon/pdf-mhwirth-ricon-logo.png"));

            XUnit topStart = helper.GetLinePosition(riconImage.PointHeight + 5, riconImage.PointHeight);

            helper.Gfx.DrawImage(riconImage, XUnit.FromCentimeter(2.1), topStart.Point, riconImage.PointWidth, riconImage.PointHeight);

            topStart = helper.GetLinePosition(normalFontSize + 5, normalFontSize);

            helper.Gfx.DrawLine(innerLine, XUnit.FromCentimeter(2).Point, topStart.Point, XUnit.FromCentimeter(21 - 2).Point, topStart.Point);

            topStart = helper.GetLinePosition(normalFontSize, normalFontSize);

            string rigName = rig.NameFormatted;
            
            helper.Gfx.DrawString("Rig: " + rigName + " (" + rig.ReferenceId + ")", fontNormal, XBrushes.Black, left.Point, topStart);

            var rightColumnLeft = left.Point + riconImage.PointWidth;
            helper.Gfx.DrawString(dateStamp, fontNormal, XBrushes.Black, rightColumnLeft + XUnit.FromCentimeter(0.3).Point, topStart);

            topStart = helper.GetLinePosition(normalFontSize * 2, normalFontSize);
            helper.Gfx.DrawLine(innerLine, XUnit.FromCentimeter(2).Point, topStart.Point, XUnit.FromCentimeter(21 - 2).Point, topStart.Point);
            helper.Gfx.DrawLine(innerLine, rightColumnLeft, XUnit.FromCentimeter(3).Point, rightColumnLeft, topStart.Point);

            XUnit center = XUnit.FromCentimeter(10.5);
            topStart = helper.GetLinePosition(headerFontSize + normalFontSize, headerFontSize * 2 + normalFontSize);
            helper.Gfx.DrawString("Statement of Compliance", fontHeader, XBrushes.Black, center.Point, topStart, XStringFormats.Center);
            topStart = helper.GetLinePosition(headerFontSize + normalFontSize, headerFontSize * 2 + normalFontSize);
            helper.Gfx.DrawString("(Certificate of compliance)", fontHeader, XBrushes.Black, center.Point, topStart, XStringFormats.Center);

            AddItemsSection(helper, left, items.Where(e => !string.IsNullOrEmpty(e.Coc) && e.Coc.ToLower() == "coc").ToList(), true);

            AddItemsSection(helper, left, items.Where(e => !string.IsNullOrEmpty(e.Coc) && e.Coc.ToLower() != "coc").ToList(), false);

            document.Save(stream);
            return stream;
        }

        private static void AddItemsSection(LayoutHelper helper, XUnit left, List<Joint> items, bool isValidCoc)
        {
            //Section - Equipment with valid CoC
            const int sectionHeaderFontSize = 12;
            const int sectionNormalFontSize = 10;
            XFont fontSectionHeader = new XFont("Arial", sectionHeaderFontSize, XFontStyle.Bold);
            XFont fontSectionNormal = new XFont("Arial", sectionNormalFontSize, XFontStyle.Regular);

            bool isNewPageForSection = helper.IsNewPage(sectionHeaderFontSize + 5, sectionHeaderFontSize);
            XUnit topStart = helper.GetLinePosition(sectionHeaderFontSize + 5, sectionHeaderFontSize);
            if (!isNewPageForSection)
            {
                //if section starts with the new page separating line is not necessary
                helper.Gfx.DrawLine(new XPen(XColors.Black, 0.5), XUnit.FromCentimeter(2).Point, topStart.Point, XUnit.FromCentimeter(21 - 2).Point, topStart.Point);
                topStart = helper.GetLinePosition(sectionHeaderFontSize + 5, sectionHeaderFontSize);
            }

            string sectionHeader = isValidCoc
                ? "Equipment with valid CoC according to RiCon programme:"
                : "Equipment without valid CoC according to RiCon programme:";
            helper.Gfx.DrawString(sectionHeader, fontSectionHeader, XBrushes.Black, left.Point, topStart);


            int cnt = 0;
            foreach (Joint item in items.OrderBy(i => i.ReferenceId))
            {
                var itemContent = item.ReferenceId + (!string.IsNullOrEmpty(item.TagNumber) ? ", " + item.TagNumber : "");
                bool isNewPage = helper.IsNewPage(sectionNormalFontSize + 5, sectionNormalFontSize + 2);
                var currentColumn = cnt++ % 3;
                if (isNewPage)
                {
                    if (currentColumn == 0)
                    {
                        topStart = helper.GetLinePosition(sectionHeaderFontSize * 2, sectionHeaderFontSize * 2, sectionHeader);
                    }
                }
                if (currentColumn == 0)
                    topStart = helper.GetLinePosition(isNewPage ? sectionHeaderFontSize + 5 : sectionNormalFontSize + 5, isNewPage ? sectionHeaderFontSize * 2 + 5 : sectionNormalFontSize + 2);

                XUnit leftForItem = left + XUnit.FromCentimeter(6 * currentColumn).Point;
                helper.Gfx.DrawString(itemContent, fontSectionNormal, XBrushes.Black, leftForItem, topStart);
            }
        }
    }

    public class LayoutHelper
    {
        private readonly PdfDocument _document;
        private readonly XUnit _topPosition;
        private readonly XUnit _bottomMargin;
        private XUnit _currentPosition;
        private string _webRootPath;

        public LayoutHelper(PdfDocument document, XUnit topPosition, XUnit bottomMargin, string webRootPath)
        {
            _document = document;
            _topPosition = topPosition;
            _bottomMargin = bottomMargin;
            // Set a value outside the page - a new page will be created on the first request.
            _currentPosition = bottomMargin + 10000;
            _webRootPath = webRootPath;
        }

        public bool IsNewPage(XUnit requestedHeight, XUnit requiredHeight)
        {
            XUnit required = requiredHeight == -1f ? requestedHeight : requiredHeight;
            return _currentPosition + required > _bottomMargin;
        }

        public XUnit GetLinePosition(XUnit requestedHeight, XUnit requiredHeight, string startText = "")
        {
            XUnit required = requiredHeight == -1f ? requestedHeight : requiredHeight;
            if (_currentPosition + required > _bottomMargin)
                CreatePage(startText);
            XUnit result = _currentPosition;
            _currentPosition += requestedHeight;
            return result;
        }

        public XGraphics Gfx { get; private set; }
        public PdfPage Page { get; private set; }

        void CreatePage(string startText = "")
        {
            Page = _document.AddPage();

            Page.Size = PageSize.A4;
            Gfx = XGraphics.FromPdfPage(Page);

            XImage image = XImage.FromFile(Path.Combine(_webRootPath, "graphics/ricon/pdf-mhwirth-logo.png"));
            // set header
            double width = image.PixelWidth * 72 / image.HorizontalResolution;
            double height = image.PixelHeight * 72 / image.HorizontalResolution;
            Gfx.DrawImage(image, XUnit.FromCentimeter(2), (XUnit.FromCentimeter(3).Point - image.PointHeight) / 2, width, height);

            XRect A4 = new XRect(XUnit.FromCentimeter(2), XUnit.FromCentimeter(3), XUnit.FromCentimeter(21 - 4).Point, XUnit.FromCentimeter(29.7 - 6).Point);
            Gfx.DrawRectangle(XPens.Black, A4);
            if (!string.IsNullOrEmpty(startText))
            {
                Gfx.DrawString(startText, new XFont("Arial", 12, XFontStyle.Bold), XBrushes.Black, XUnit.FromCentimeter(2.3).Point, XUnit.FromCentimeter(3.6).Point);
            }

            // set footer
            XFont footerFont = new XFont("Arial", 9);
            XUnit footerLeft = XUnit.FromCentimeter(10.5);
            XUnit footerTop = XUnit.FromCentimeter(29.7 - 2.8);
            XStringFormat footerFormat = new XStringFormat();
            footerFormat.Alignment = XStringAlignment.Center;
            Gfx.DrawString("This document and all information and data herein or herewith is the confidential and proprietary property of MHWirth AS ", footerFont, XBrushes.Red, footerLeft, footerTop, footerFormat);
            Gfx.DrawString("and is not to be used, reproduced or disclosed in whole or in part by or to anyone without the written confirmation from ", footerFont, XBrushes.Red, footerLeft, footerTop + footerFont.Size + 1, footerFormat);
            Gfx.DrawString("MHWirth AS.", footerFont, XBrushes.Red, footerLeft, footerTop + footerFont.Size * 2 + 1, footerFormat);

            Gfx.DrawString(@"Internal", new XFont("Arial", 7), XBrushes.Gray, XUnit.FromCentimeter(2), footerTop + footerFont.Size * 4 + 1);
            Gfx.DrawString($"© {DateTime.Now.Year} MHWirth", footerFont, XBrushes.Black, XUnit.FromCentimeter(3), footerTop + footerFont.Size * 4 + 1);

            _currentPosition = _topPosition;
        }
    }
}
