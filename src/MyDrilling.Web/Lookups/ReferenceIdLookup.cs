﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using MyDrilling.Core;
using MyDrilling.Infrastructure.Data;

namespace MyDrilling.Web.Lookups
{
    public interface IReferenceIdLookup
    {
        long GetRigId(long rigReferenceId);
        long GetEquipmentId(long equipmentReferenceId);
    }

    public class ReferenceIdLookup : IReferenceIdLookup
    {
        private readonly IMemoryCache _cache;
        private readonly MyDrillingDb _db;
        private const string RigCacheKey = "RIG_REFERENCE_LOOKUP";
        private const string EquipmentCacheKey = "EQUIPMENT_REFERENCE_LOOKUP";
        private static readonly TimeSpan ExpirationSpan = TimeSpan.FromHours(5);

        public ReferenceIdLookup(MyDrillingDb db,
            IMemoryCache cache)
        {
            _db = db;
            _cache = cache;
        }

        public long GetRigId(long rigReferenceId) => GetRigs().TryGetValue(rigReferenceId, out var rigId) ? rigId : default;

        public long GetEquipmentId(long equipmentReferenceId) => GetEquipments().TryGetValue(equipmentReferenceId, out var equipmentId) ? equipmentId : default;

        private Dictionary<long, long> GetRigs()
        {
            return _cache.GetOrCreate(RigCacheKey, x =>
            {
                x.AbsoluteExpiration = DateTime.UtcNow.Add(ExpirationSpan);
                return _db.Rigs.Select(r => new { r.Id, r.ReferenceId }).ToDictionary(r => r.ReferenceId, r => r.Id);
            });
        }

        private Dictionary<long, long> GetEquipments()
        {
            return _cache.GetOrCreate(EquipmentCacheKey, x =>
            {
                x.AbsoluteExpiration = DateTime.UtcNow.Add(ExpirationSpan);
                return _db.Equipments.Select(e => new { e.Id, e.ReferenceId }).ToDictionary(e => e.ReferenceId, e => e.Id);
            });
        }
    }

    public static class RigLookupExtensions
    {
        public static long GetRigId(this HttpContext httpContext, string rigReferenceId)
        {
            var lookupService = httpContext.RequestServices.GetRequiredService<IReferenceIdLookup>();
            return lookupService.GetRigId(rigReferenceId.ToLongReferenceId());
        }

        public static long GetEquipmentId(this HttpContext httpContext, string equipmentReferenceId)
        {
            var lookupService = httpContext.RequestServices.GetRequiredService<IReferenceIdLookup>();
            return lookupService.GetEquipmentId(equipmentReferenceId.ToLongReferenceId());
        }
    }
}
