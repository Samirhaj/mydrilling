﻿namespace MyDrilling.Web.Models.Menu
{
    public class RigMenuViewModel
    {
        public string RigName { get; set; }
        public long RigReferenceId { get; set; }
    }
}
