﻿namespace MyDrilling.Web.Models.Menu
{
    public class EquipmentMenuViewModel
    {
        public string EquipmentName { set; get; }
        public long EquipmentReferenceId { set; get; }
        public long? RigReferenceId { set; get; }
    }
}
