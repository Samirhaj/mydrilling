﻿using System.Collections.Generic;

namespace MyDrilling.Web.Models.Menu
{
    public class DrillDownMenuViewModel
    {
        public string CurrentAction { get; set; }
        public string CurrentController { get; set; }
        public string SelectedRig { get; set; }
        public bool HasRig { get; set; }
        public string SelectedProduct { get; set; }
        public string SelectedProductName { get; set; }
        public bool HasProduct { get; set; }
        public bool HasEquipment { get; set; }
        public string SelectedEquipment { get; set; }
        public Dictionary<string, List<RigMenuViewModel>> TypedRigs { get; set; }
        public List<EquipmentMenuViewModel> EquipmentForSelectedProduct { get; set; }
    }
}
