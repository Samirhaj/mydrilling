﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyDrilling.Core.Entities.Enquiry;

namespace MyDrilling.Web.Models.Enquiries
{
    public class DetailViewModel
    {
        public long Id { get; set; }
        public string ReferenceId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public TypeInfo.Type Type { get; set; }
        public TypeInfo TypeInfo => TypeInfo.ById[(int)Type];
        public StateInfo.State State { get; set; }
        public StateInfo StateInfo => StateInfo.ByState[State];
        public bool IsDraft => State == StateInfo.State.Draft;
        public DateTime CreatedDateTime { get; set; }
        public string AssigneeFirstName { get; set; }
        public string AssigneeLastName { get; set; }
        public string AssigneeName => $"{AssigneeFirstName} {AssigneeLastName}";
        public string RootCustomerName { get; set; }
        public string RigName { get; set; }
        public long RigId { get; set; }
        public string EquipmentName { get; set; }
        public long? EquipmentId { get; set; }
        public long Version { get; set; }

        public CommentViewModel[] Comments { get; set; } = Array.Empty<CommentViewModel>();
        public FileViewModel[] Files { get; set; } = Array.Empty<FileViewModel>();

        public IDictionary<long, CommentViewModel> CommentsById { get; set; }
        public IDictionary<long, long[]> CommentsToFiles { get; set; }

        public StateLogViewModel[] StateLogs { get; set; }

        public StateLogViewModel[] GetWorkflow()
        {
            var workflow = new List<StateLogViewModel>(3);

            var draftStateLog = StateLogs
                .Where(s => s.State == StateInfo.State.Draft)
                .OrderByDescending(x => x.ChangedDateTime)
                .FirstOrDefault();
            if(draftStateLog != null) workflow.Add(draftStateLog);

            var openStateLog = StateLogs
                .Where(s => s.State == StateInfo.State.AssignedProvider)
                .OrderByDescending(x => x.ChangedDateTime)
                .FirstOrDefault();
            if (openStateLog != null) workflow.Add(openStateLog);

            var closedStateLog = StateLogs
                .Where(s => s.State == StateInfo.State.Closed)
                .OrderByDescending(x => x.ChangedDateTime)
                .FirstOrDefault();
            if (closedStateLog != null) workflow.Add(closedStateLog);

            return workflow.OrderByDescending(x => x.ChangedDateTime).ToArray();
        }

        public List<AddedPersonnelItemViewModel> AddedPersonnel { get; set; }
        public List<InvolvedPersonnelItemViewModel> InvolvedPersonnel { get; set; }

        public bool CanAddComment { get; set; }
        public bool CanDeleteComment { get; set; }
        public bool CanSendForApproval { get; set; }
        public bool CanAssignToCustomer { get; set; }
        public bool CanAssignToProvider { get; set; }
        public bool CanSetAssignee { get; set; }
        public bool CanAddPersonnel { get; set; }
        public bool CanDeletePersonnel { get; set; }
        public bool CanRemoveAssignee { get; set; }
        public bool CanSuggestToClose { get; set; }
        public bool CanApprove { get; set; }
        public bool CanReject { get; set; }
        public bool CanEdit { get; set; }
        public bool CanDelete { get; set; }
        public bool CanDeleteAttachment { get; set; }
        public bool CanReopen { get; set; }

        public string GetMetaCategory()
        {
            string requestMetaCategory = "metaRequest";

            if (Type == TypeInfo.Type.Support_RigOnDowntime)
            {
                if (State == StateInfo.State.Draft || State == StateInfo.State.Rejected)
                {
                    return "metaDowntimeDraft";
                }

                requestMetaCategory = State == StateInfo.State.AssignedCustomer || State == StateInfo.State.AssignedProvider || State == StateInfo.State.SuggestedClosed 
                    ? "metaDowntimeOpen" 
                    : "metaDowntime";
                return requestMetaCategory;
            }

            if (State ==  StateInfo.State.Closed)
            {
                requestMetaCategory = "metaRequestClosed";
            }
            if (State == StateInfo.State.Deleted)
            {
                requestMetaCategory = "metaRequestDeleted";
            }
            else if (State == StateInfo.State.Draft || State == StateInfo.State.Rejected || State == StateInfo.State.ReadyForApproval)
            {
                requestMetaCategory = "metaRequestDraft";
            }

            return requestMetaCategory;
        }

        public string GetCommentId(long commentId) => $"comment-{commentId}";

        public string GetFileId(long fileId) => $"file-{fileId}";

        public string GetDeleteCommentFormId(long commentId) => $"delete-comment-{commentId}";

        public string GetDeleteFileFormId(long fileId) => $"delete-file-{fileId}";

        public string GetDeleteAddedPersonnelFormId(long userId) => $"delete-added-personnel-{userId}";

        public class InvolvedPersonnelItemViewModel
        {
            public long UserId { get; set; }
            public string Upn { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Position { get; set; }
            public string CustomerName { get; set; }
            public bool IsInternalUser { get; set; }
        }

        public class AddedPersonnelItemViewModel
        {
            public long UserId { get; set; }
            public string Upn { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Position { get; set; }
            public string CustomerName { get; set; }
            public bool IsInternalUser { get; set; }
            public bool CanBeDeleted { get; set; }
        }

        public class CommentViewModel
        {
            public long Id { get; set; }
            public string Text { get; set; }
            public long? CreatedByUserId { get; set; }
            public string CreatedByUserFirstName { get; set; }
            public string CreatedByUserLastName { get; set; }
            public string CreatedByUserCustomerName { get; set; }
            public DateTime CreatedDateTime { get; set; }
            public string CreatedByUserName => $"{CreatedByUserFirstName} {CreatedByUserLastName}";
        }

        public class FileViewModel
        {
            public long Id { get; set; }
            public string FileName { get; set;}
            public DateTime Created { get; set; }
            public long? CommentId { get; set; }
            public string FileExtension { get; set; }
            public long? CreatedByUserId { get; set; }
            public string CreatedByUserFirstName { get; set; }
            public string CreatedByUserLastName { get; set; }
            public string CreateByInfo => $"{CreatedByUserFirstName} {CreatedByUserLastName}";
        }

        public class StateLogViewModel
        {
            public long? ChangedByUserId { get; set; }
            public string ChangedBy { get; set; }
            public DateTime ChangedDateTime { get; set; }
            public StateInfo.State State { get; set; }
            public string StateName => StateInfo.ByState[State].Title;

            public string StateCss
            {
                get 
                {
                    switch (State)
                    {
                        case StateInfo.State.AssignedCustomer:
                        case StateInfo.State.AssignedProvider:
                            return "open";
                        case StateInfo.State.Closed:
                        case StateInfo.State.SuggestedClosed:
                            return "closed";
                        case StateInfo.State.Draft:
                        case StateInfo.State.ReadyForApproval:
                            return "draft";
                        case StateInfo.State.Rejected:
                            return "rejected";
                        case StateInfo.State.Deleted:
                            return "deleted";
                        default:
                            return "";
                    }
                }
            }
    }
    }
}
