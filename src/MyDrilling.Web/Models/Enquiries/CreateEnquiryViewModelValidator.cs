﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Infrastructure.Data;

namespace MyDrilling.Web.Models.Enquiries
{
    public class CreateEnquiryViewModelValidator : AbstractValidator<CreateEnquiryViewModel>
    {
        private readonly MyDrillingDb _db;

        public CreateEnquiryViewModelValidator(MyDrillingDb db)
        {
            _db = db;

            RuleFor(x => x.Title)
                .NotNull().NotEmpty()
                .MaximumLength(Enquiry.TitleMaxLength);

            RuleFor(x => x.Description)
                .NotNull().NotEmpty()
                .MaximumLength(Enquiry.DescriptionMaxLength);

            RuleFor(x => x.RootCustomerId)
                .GreaterThan(default(long))
                .WithMessage("Customer is required")
                .DependentRules(() =>
                {
                    RuleFor(x => x.RootCustomerId)
                        .MustAsync(async (x, ct) => await _db.RootCustomers.SingleOrDefaultAsync(r => r.Id == x, ct) != null)
                        .WithMessage("Customer doesn't exists")
                        .DependentRules(() =>
                        {
                            RuleFor(x => x.RootCustomerId)
                                .MustAsync(async (x, ct) =>
                                {
                                    var rootCustomer =  await _db.RootCustomers.SingleAsync(r => r.Id == x, ct);
                                    return rootCustomer.DefaultCustomerId.HasValue;
                                })
                                .WithMessage("Customer doesn't have default SAP customer");
                        });
                });

            RuleFor(x => x.RigId)
                .GreaterThan(default(long))
                .WithMessage("Rig is required")
                .DependentRules(() =>
                {
                    RuleFor(x => x.RigId)
                        .MustAsync(async (x, ct) => await _db.Rigs.SingleOrDefaultAsync(r => r.Id == x, ct) != null)
                        .WithMessage("Rig doesn't exists");
                });

            RuleFor(x => x.RigId)
                .MustAsync(CheckRigToCustomerRelationshipAsync)
                .When(x => x.RigId > 0 && x.RootCustomerId > 0)
                .WithMessage("Rig doesn't belong to customer");

            RuleFor(x => x.EquipmentId)
                .MustAsync(async (x, ct) => await _db.Equipments.SingleOrDefaultAsync(r => r.Id == x, ct) != null)
                .When(x => x.EquipmentId > 0)
                .WithMessage("Equipment doesn't exists");

            RuleFor(x => x.EquipmentId)
                .MustAsync(CheckEquipmentToRigRelationshipAsync)
                .When(x => x.EquipmentId > 0 && x.RigId > 0)
                .WithMessage("Equipment doesn't belong to rig");

            RuleFor(x => x.TypeId)
                .GreaterThan(default(int))
                .WithMessage("Type is required")
                .DependentRules(() =>
                {
                    RuleFor(x => x.TypeId)
                        .Must(x => Enum.IsDefined(typeof(TypeInfo.Type), x))
                        .WithMessage("Type is invalid")
                        .DependentRules(() =>
                        {
                            RuleFor(x => x.EquipmentId)
                                .Must((model, equipmentId) => !TypeInfo.ById[model.TypeId].CanHaveEquipment || equipmentId >= 0)
                                .WithMessage("Equipment is required");
                        });
                });
        }

        private async Task<bool> CheckRigToCustomerRelationshipAsync(CreateEnquiryViewModel model, long rigId,
            CancellationToken stoppingToken)
        {
            var rig = await _db.Rigs.SingleAsync(x => x.Id == rigId, stoppingToken);
            var rootCustomer = await _db.RootCustomers.SingleAsync(x => x.Id == model.RootCustomerId, stoppingToken);

            return rig.RootOperatorId == rootCustomer.Id || rig.RootOwnerId == rootCustomer.Id;
        }

        private async Task<bool> CheckEquipmentToRigRelationshipAsync(CreateEnquiryViewModel model, long? equipmentId,
            CancellationToken stoppingToken)
        {
            var rig = await _db.Rigs.SingleAsync(x => x.Id == model.RigId, stoppingToken);
            var equipment = await _db.Equipments.SingleAsync(x => x.Id == equipmentId, cancellationToken: stoppingToken);

            return equipment.RigId == rig.Id;
        }
    }
}
