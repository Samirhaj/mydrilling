﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Infrastructure.Data;

namespace MyDrilling.Web.Models.Enquiries
{
    public class EditEnquiryViewModelValidator : AbstractValidator<EditEnquiryViewModel>
    {
        private readonly MyDrillingDb _db;

        public EditEnquiryViewModelValidator(MyDrillingDb db)
        {
            _db = db;

            RuleFor(x => x.Title)
                .NotNull().NotEmpty()
                .MaximumLength(Enquiry.TitleMaxLength);

            RuleFor(x => x.Description)
                .NotNull().NotEmpty()
                .MaximumLength(Enquiry.DescriptionMaxLength);

            RuleFor(x => x.EquipmentId)
                .MustAsync(async (x, ct) => await _db.Equipments.SingleOrDefaultAsync(r => r.Id == x, ct) != null)
                .When(x => x.EquipmentId > 0)
                .WithMessage("Equipment doesn't exists");

            RuleFor(x => x.EquipmentId)
                .MustAsync(CheckEquipmentToRigRelationshipAsync)
                .When(x => x.EquipmentId > 0)
                .WithMessage("Equipment doesn't belong to rig");

            RuleFor(x => x.TypeId)
                .MustAsync(CheckTypeConsistencyAsync)
                .When(x => x.TypeId > 0)
                .WithMessage("Selected type is in another category");
        }

        private async Task<bool> CheckEquipmentToRigRelationshipAsync(EditEnquiryViewModel model, long? equipmentId,
            CancellationToken stoppingToken)
        {
            var enquiry = await _db.Enquiry_Enquiries.SingleAsync(x => x.Id == model.Id, stoppingToken);
            var equipment = await _db.Equipments.SingleAsync(x => x.Id == equipmentId, cancellationToken: stoppingToken);

            return equipment.RigId == enquiry.RigId;
        }

        private async Task<bool> CheckTypeConsistencyAsync(EditEnquiryViewModel model, int typeId,
            CancellationToken stoppingToken)
        {
            var enquiry = await _db.Enquiry_Enquiries.SingleAsync(x => x.Id == model.Id, stoppingToken);
            var selectedCategory = TypeInfo.ById[(int) enquiry.Type].Category;
            var availableTypes = TypeInfo.ByCategory[selectedCategory].Select(x => x.Id).ToHashSet();

            return availableTypes.Contains(typeId);
        }
    }
}
