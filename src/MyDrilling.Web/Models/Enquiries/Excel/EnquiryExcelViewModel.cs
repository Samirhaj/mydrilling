﻿using System;
using MyDrilling.Core.Entities.Enquiry;

namespace MyDrilling.Web.Models.Enquiries.Excel
{
    public class EnquiryExcelViewModel
    {
        public string ReferenceId { get; set; }
        public string Title { get; set; }
        public string CustomerName { get; set; }
        public string RigName { get; set; }
        public TypeInfo.Type TypeId { get; set; }
        public TypeInfo TypeInfo => TypeInfo.ById[(int)TypeId];
        public string Category => TypeInfo.Category;
        public string Type => TypeInfo.Title; 
        public string AssignedToCustomerName { get; set; }
        public string EquipmentName { get; set; }
        public LogicalStateInfo.LogicalState LogicalState { get; set; }
        public LogicalStateInfo LogicalStateInfo => LogicalStateInfo.ByLogicalState[LogicalState];
        public StateInfo.State State { get; set; }
        public StateInfo StateInfo => StateInfo.ByState[State];
        public DateTime Date { get; set; }
    }
}
