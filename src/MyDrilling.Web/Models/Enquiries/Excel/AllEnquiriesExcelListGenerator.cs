﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using MyDrilling.Infrastructure.Excel;

namespace MyDrilling.Web.Models.Enquiries.Excel
{
    public class AllEnquiriesExcelListGenerator : ExcelListGeneratorBase
    {
        public Stream CreateExcelDocument(IEnumerable<EnquiryExcelViewModel> entities, Core.Entities.User user)
        {
            var columns = GetColumns();
            var rows = entities.Select(x => MapEntityToRowData(x, user));
            var stream = CreateExcelDocumentStream(columns, rows, "All enquiries");
            return stream;
        }

        private IEnumerable<string> MapEntityToRowData(EnquiryExcelViewModel entity, Core.Entities.User user)
        {
            return new List<string>
            {
                entity.LogicalStateInfo.Title,
                entity.ReferenceId ?? LanguageConstants.NotAvailable,
                entity.Title,
                entity.CustomerName,
                entity.RigName,
                entity.Date.StdDateTimeString(user.GetTimeZoneInfo()),
                entity.Category,
                entity.Type,
                entity.EquipmentName
            };
        }

        private IEnumerable<ExcelColumnInfo> GetColumns()
        {
            return new List<ExcelColumnInfo>
            {
                new ExcelColumnInfo {Title = "State", Width = 20},
                new ExcelColumnInfo {Title = "Ref. number", Width = 20},
                new ExcelColumnInfo {Title = "Title", Width = 50},
                new ExcelColumnInfo {Title = "Customer", Width = 50},
                new ExcelColumnInfo {Title = "Rig", Width = 30},
                new ExcelColumnInfo {Title = "Date", Width = 20},
                new ExcelColumnInfo {Title = "Category", Width = 30},
                new ExcelColumnInfo {Title = "Type", Width = 30},
                new ExcelColumnInfo {Title = "Equipment", Width = 30}
            };
        }
    }
}
