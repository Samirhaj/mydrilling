﻿using System.Collections.Generic;
using System.ComponentModel;
using Microsoft.AspNetCore.Mvc.Rendering;
using MyDrilling.Core.Entities.Enquiry;

namespace MyDrilling.Web.Models.Enquiries
{
    public class CreateEnquiryViewModel
    {
        public SelectListItem[] Customers { get; set; }
        public IReadOnlyDictionary<long, SelectListItem[]> RigsForCustomer { get; set; }

        public SelectListItem[] Rigs { get; set; }
        public IReadOnlyDictionary<long, SelectListItem[]> CustomersForRig { get; set; }

        public string[] Categories { get; set; }
        public IReadOnlyDictionary<string, TypeInfo[]> TypesForCategory { get; set; }
        public IReadOnlyDictionary<int, TypeInfo> Types { get; set; }
        [DisplayName(LanguageConstants.Customer)]
        public long RootCustomerId { get; set; }
        [DisplayName(LanguageConstants.Rig)]
        public long RigId { get; set; }
        [DisplayName(LanguageConstants.Category)]
        public string CategoryId { get; set; }
        [DisplayName(LanguageConstants.Type)]
        public int TypeId { get; set; }
        [DisplayName(LanguageConstants.Equipment)]
        public long? EquipmentId { get; set; }
        [DisplayName(LanguageConstants.Title)]
        public string Title { get; set; }
        [DisplayName(LanguageConstants.Description)]
        public string Description { get; set; }
        [DisplayName(LanguageConstants.Attachments)]
        public string[] Files { get; set; }

        public bool IsDraft { get; set; }

        public const int DefaultInstructionId = 0;
        public static readonly IReadOnlyDictionary<int, string[]> Instructions = new Dictionary<int, string[]>
        {
            //default instruction
            {DefaultInstructionId, new []
                {
                    "Please make sure:",
                    "You provide an adequate description."
                }
            },
            {(int)TypeInfo.Type.Support_Service, new []
                {
                    "Please use relevant guidelines to ensure correct description:",
                    "Caused by",
                    "Type of personnel (hydraulic or software)",
                    "Number of personnel",
                    "Time period for service",
                    "(Important: a request for service that is less than ten days from today is considered to be an ad hoc enquiry. In order to provide you with the best support, please call us directly for these types of services)"
                }
            },
            {(int)TypeInfo.Type.Support_RigOnDowntime, new []
                {
                    "Please use relevant guidelines to ensure correct description:",
                    "Date / time",
                    "Type of operation",
                    "Issue related to software, mechanical, operational, others and description of it",
                    "Detailed description of what happened and how",
                    "Recent alarms (numbers and description)",
                    "Mode of operation (direct, normal, automatic, guided, not applicable)",
                    "Expected actions from MHWirth",
                    "Customer reference number"
                }
            },
            {(int)TypeInfo.Type.Support_Overhaul, new []
                {
                    "Please use relevant guidelines to ensure correct description:",
                    "Please specify scope of work",
                    "Please specify engineering scope",
                    "Rig survey (If yes, please state time period and if a rig survey study report is part of the delivery)",
                    "Dismount",
                    "Installation",
                    "Commissioning",
                    "Special requirements for documentation",
                    "Also relevant to other rigs (please specify which rigs)"
                }
            },
            {(int)TypeInfo.Type.Support_Training, new []
                {
                    "Please use relevant guidelines to ensure correct description:",
                    "Course",
                    "Type of personnel",
                    "Number of participants and needed classes",
                    "Preferred location (Norway-Kristiansand, Brazil-Macae, Singapore, USA-Houston, other)"
                }
            },
            {(int)TypeInfo.Type.Support_RemoteDiagnostics, new []
                {
                    "Please use relevant guidelines to ensure correct description:",
                    "Remote connection available from (date and time). Please ensure that your internet line is connected. If required, have your pin code ready",
                    "Recent alarms (numbers and description)",
                    "Interlock status (SZMS)",
                    "Mode of operation (direct, normal, automatic, guided, not applicable)"
                }
            },
            {(int)TypeInfo.Type.Support_BulletinSupport, new []
                {
                    "Please use relevant guidelines to ensure correct description:",
                    "Limitations",
                    "Additional utilities",
                    "Planned maintenance stops",
                    "Access to equipment",
                    "Changes to equipment"
                }
            },
            {(int)TypeInfo.Type.Sparepart_IdentifyPartNumber, new []
                {
                    "Please enter",
                    "Tag number and drawing number for each part number to be identified."
                }
            },
            {(int)TypeInfo.Type.Sparepart_PriceAndAvailability, new []
                {
                    "Please indicate",
                    "Which part number(s) the request is related to."
                }
            },
        };

    }
}
