﻿using System.ComponentModel;
using MyDrilling.Core.Entities.Enquiry;

namespace MyDrilling.Web.Models.Enquiries
{
    public class EditEnquiryViewModel
    {
        public long Id { get; set; }
        public long Version { get; set; }
        [DisplayName(LanguageConstants.Rig)]
        public string RigName { get; set; }
        public long RigId { get; set; }
        [DisplayName(LanguageConstants.Type)]
        public int TypeId { get; set; }
        [DisplayName(LanguageConstants.Category)]
        public string CategoryName { get;set; }
        public TypeInfo[] Types { get; set; }
        [DisplayName(LanguageConstants.Equipment)]
        public long? EquipmentId { get; set; }
        public string EquipmentName { get; set; }
        public bool CanHaveEquipment { get; set; }
        public bool CanChangeType { get; set; }
        [DisplayName(LanguageConstants.Title)]
        public string Title { get; set; }
        [DisplayName(LanguageConstants.Description)]
        public string Description { get; set; }
    }
}
