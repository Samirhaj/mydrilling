﻿using System;
using MyDrilling.Core.Entities.Enquiry;

namespace MyDrilling.Web.Models.Enquiries
{
    public class ItemsViewModel
    {
        public const string TypeFilter = "TypeFilter";
        public const string CustomerFilter = "CustomerFilter";
        public const string AssignedToFilter = "AssignedToFilter";
        public const string AssigneeFilter = "AssigneeFilter";
        public const string CategoryFilter = "CategoryFilter";
        public const string DraftStateFilter = "StateFilter";
        public const string LogicalStateFilter = "LogicalStateFilter";

        public const string CustomerSort = "CustomerSort";
        public const string RigSort = "RigSort";
        public const string TitleSort = "TitleSort";
        public const string DateSort = "DateSort";
        public const string AssignedToCustomerSort = "AssignedToCustomerSort";
        public const string AssignedToUserSort = "AssignedToUserSort";
        public const string EquipmentSort = "EquipmentSort";
        public const string LastCommentSort = "LastCommentSort";
        public const string LogicalStateSort = "LogicalStateSort";

        public MenuViewModel Menu { get; set; }
        public ItemViewModel[] Items { get; set; }
        public int TotalCount { get; set; }
    }

    public class ItemViewModel
    {
        public string ReferenceId { get; set; }
        public string Title { get; set; }
        public long Id { get; set; }
        public string CustomerName { get; set; }
        public string RigName { get; set; }
        public long RigReferenceId { get; set; }
        public string Category => TypeInfo.Category;
        public TypeInfo.Type TypeId { get; set; }
        public TypeInfo TypeInfo => TypeInfo.ById[(int)TypeId];
        public StateInfo.State State { get; set; }
        public StateInfo StateInfo => StateInfo.ByState[State];
        public LogicalStateInfo.LogicalState LogicalState { get; set; }
        public LogicalStateInfo LogicalStateInfo => LogicalStateInfo.ByLogicalState[LogicalState];
        public string AssignedToCustomerName { get; set; }
        public string AssignedToName { get; set; }
        public string EquipmentName { get; set; }
        public DateTime Date { get; set; }
        public DateTime? LastCommentedDateTime { get; set; }
        public bool IsRead { get; set; }
    }
}
