﻿namespace MyDrilling.Web.Models.Enquiries
{
    public class MenuViewModel
    {
        public bool CanSeeOpenEnquiries { set; get; }
        public bool CanSeeDraftEnquiries { set; get; }
        public bool CanSeeClosedEnquiries { set; get; }
        public bool CanSeeDeletedEnquiries { set; get; }
        public bool CanSeeAllEnquiries { set; get; }
        public string CurrentActionName { get; set; }
    }
}
