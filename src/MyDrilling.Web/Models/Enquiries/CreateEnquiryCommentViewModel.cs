﻿namespace MyDrilling.Web.Models.Enquiries
{
    public class CreateEnquiryCommentViewModel
    {
        public long EnquiryId { get; set; }
        public string Text { get; set; }
        public string[] Files { get; set; }
    }
}
