﻿using FluentValidation;

namespace MyDrilling.Web.Models.Enquiries
{
    public class CreateEnquiryCommentViewModelValidator : AbstractValidator<CreateEnquiryCommentViewModel>
    {
        public CreateEnquiryCommentViewModelValidator()
        {
            RuleFor(x => x.Text).NotNull().NotEmpty();
        }
    }
}
