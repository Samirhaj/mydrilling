﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyDrilling.Web.Models.UpgradeMatrix
{
    public class WebQuotationItem
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public long EquipmentCategoryId { get; set; }
        public string EquipmentCategoryName { get; set; }
        public string Description { get; set; }
        public string Heading { get; set; }
        public string Benefits { get; set; }
        public string QuotationReference { get; set; }
        public bool Open { get; set; }
        public IEnumerable<WebItemState> RigStates { get; set; }
    }
}
