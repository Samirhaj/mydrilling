﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyDrilling.Web.Models.UpgradeMatrix
{
    public class WebCcnItem
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public long EquipmentCategoryId { get; set; }
        public string EquipmentCategoryName  { get; set; }
        public string Description { get; set; }
        public string Comments { get; set; }
        public int PackageUpgrade { get; set; }
        public int RigType { get; set; }
        public bool Open { get; set; }
        public IEnumerable<WebItemState> RigStates { get; set; }
    }
}
