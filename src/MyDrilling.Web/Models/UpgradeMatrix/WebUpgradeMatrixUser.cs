﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyDrilling.Web.Models.UpgradeMatrix
{
    public class WebUpgradeMatrixUser
    {
        public long Id { get; set; }
        public long[] ViewerOnRigs { get; set; }
        public long[] EditorOnRigs { get; set; }
        public long[] EnquiryCreatorOnRigs { get; set; }
        public bool IsUpgradeMatrixAdmin { get; set; }
        public bool HasUpgradeMatrixAccess { get; set; }
    }
}
