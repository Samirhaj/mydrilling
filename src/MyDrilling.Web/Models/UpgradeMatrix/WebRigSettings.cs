﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyDrilling.Web.Models.UpgradeMatrix
{
    public class WebRigSettings
    {
        public long Id { get; set; }
        public long RigId { get; set; }
        public string Name { get; set; }
        public long ReferenceId { get; set; }
        public string ShortName { get; set; }
        public int RigType { get; set; }
    }
}
