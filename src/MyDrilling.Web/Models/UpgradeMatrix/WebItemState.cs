﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyDrilling.Web.Models.UpgradeMatrix
{
    public class WebItemState
    {
        public long Id { get; set; }
        public long ItemId { get; set; }
        public long RigSettingsId { get; set; }
        public string RigShortName { get; set; }
        public int Status { get; set; }
        public string CcnName { get; set; }
        public string DocumentPath { get; set; }
        public bool EnquiryCreationEnabled { get; set; }
        public string ApprovedBy { get; set; }
        
    }
}
