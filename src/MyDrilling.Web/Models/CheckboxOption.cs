﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyDrilling.Web.Models
{
    public class CheckboxOption
    {
        public string OptionLabel { get; set; }
        public string OptionValue { get; set; }
        public bool Selected { get; set; }
    }
}
