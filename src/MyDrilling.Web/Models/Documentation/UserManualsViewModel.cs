﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyDrilling.Web.Models.Documentation
{
    public class UserManualsViewModel
    {
        public const string DisciplineFilter = "DisciplineFilter";

        public const string ReferenceIdSort = "ReferenceIdSort";
        public const string TitleSort = "TitleSort";
        public const string EquipmentTypeSort = "EquipmentTypeSort";
        public const string RigSort = "RigSort";
        public const string DisciplineSort = "DisciplineSort";
        public const string DateSort = "DateSort";

        public MenuViewModel Menu { get; set; }
        public UserManualViewModel[] Items { get; set; }
        public int TotalCount { get; set; }
    }

    public class UserManualViewModel
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public string RigName { get; set; }
        public string EquipmentType { get; set; }
        public string Discipline { get; set; }
        public string ReferenceId { get; set; }
        public string Url { get; set; }
        public string DocumentDate { get; set; }
        public UserManualViewModel[] RelatedDocuments { get; set; }
        public bool IsSharePoint { get; set; }
    }
}
