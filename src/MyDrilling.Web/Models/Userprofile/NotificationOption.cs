﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyDrilling.Web.Models.Userprofile
{
    public class NotificationOption : CheckboxOption
    {
        public NotificationOption()
        {
            Options = new List<NotificationOption>();
        }

        public NotifyMeType NotifyMeType { get; set; }
        public List<NotificationOption> Options { get; set; }
        public string GroupId { get; set; }
        public string GroupName { get; set; }
        public string WrapperStyle { get; set; }
    }

    public enum NotifyMeType
    {
        None,
        CommonSubscription, 
        RigFilters,
        EnquiryTypeFilters
    }
}
