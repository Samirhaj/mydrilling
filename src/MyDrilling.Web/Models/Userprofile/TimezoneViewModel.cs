﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MyDrilling.Web.Models.Userprofile
{
    public class TimeZoneViewModel
    {
        public TimeZoneViewModel()
        {
            TimeZones = GetTimeZoneSelectList("UTC");
        }

        [Display(Name = "Timezone")]
        public string SelectedTimeZone { get; set; }
        public List<SelectListItem> TimeZones { get; set; }

        private List<SelectListItem> GetTimeZoneSelectList(string selectedValue)
        {
            var timeZones = TimeZoneInfo.GetSystemTimeZones();
            return timeZones.Select(timeZone => new SelectListItem(timeZone.DisplayName, timeZone.Id, timeZone.Id == selectedValue)).ToList();
        }
    }
}
