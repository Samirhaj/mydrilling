﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyDrilling.Web.Models.Userprofile
{
    public class MyNotificationsViewModel
    {
        public List<NotificationOption> NotificationOptions { get; set; }
        public List<string> SelectedSubscriptions { get; set; }
    }
}
