﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyDrilling.Web.Models.Userprofile
{
    public class UserProfileViewModel
    {
        public string FullName { set; get; }

        [Display(Name = "Username")]
        public string Upn { get; set; }

        [Display(Name = "Tel.num", Prompt = "Enter phone number")]
        public string PhoneNumber { get; set; }

        [DataType(DataType.EmailAddress), Display(Name = "E-mail", Prompt = "Enter e-mail")]
        public string EmailAddress { get; set; }

        [Display(Name = "First name", Prompt = "Enter firstname")]
        public string FirstName { get; set; }

        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Display(Name = "Company")]
        public string CompanyName { get; set; }

        [Display(Name = "Title", Prompt = "Enter title")]
        public string Title { get; set; }

        [Display(Name = "Timezone")]
        public string SelectedTimeZone { get; set; }
    }
}
