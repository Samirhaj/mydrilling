﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyDrilling.Web.Models.Userprofile
{
    public class MyPreferencesViewModel
    {
        public List<CheckboxOption> PreferencesOptions { get; set; }
        public List<string> SelectedPreferences { get; set; }
    }
}
