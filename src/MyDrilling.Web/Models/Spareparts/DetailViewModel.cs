﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace MyDrilling.Web.Models.Spareparts
{
    public class DetailViewModel
    {
        public long Id { get; set; }
        public string ReferenceId { get; set; }
        public string MaterialNo { get; set; }
        public string Description { get; set; }
        public bool ShowCustomers { get; set; }
        public SelectListItem[] Customers { get; set; }
    }
}