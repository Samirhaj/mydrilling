﻿namespace MyDrilling.Web.Models.Spareparts
{
    public class ItemsViewModel
    {
        public const string PartNumberSort = "PartNumberSort";
        public const string TitleSort = "TitleSort";
        
        public ItemViewModel[] Items { get; set; }
        public int TotalCount { get; set; }
    }

    public class ItemViewModel
    {
        public long Id { get; set; }
        public string MaterialNo { get; set; }
        public string Description { get; set; }
    }
}