﻿namespace MyDrilling.Web.Models.Spareparts
{
    public class PriceViewModel
    {
        public string Price { get; set; }
        public string Currency { get; set; }
    }
}