﻿using System.Collections.Generic;

namespace MyDrilling.Web.Models.Equipment
{
    public class GroupedProductsViewModel
    {
        public string CurrentAction { get; set; }
        public string CurrentController { get; set; }
        public Dictionary<string, List<ProductViewModel>> GroupedProducts { get; set; }
    }
}
