﻿namespace MyDrilling.Web.Models.Equipment
{
    public class ProductViewModel
    {
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
    }
}
