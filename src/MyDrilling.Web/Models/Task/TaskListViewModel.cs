﻿namespace MyDrilling.Web.Models.Task
{
    public class TaskListViewModel
    {
        public const string TaskTypeFilter = "TaskTypeFilter";
        public const string TaskActionFilter = "TaskActionFilter";
        public const string CategoryFilter = "CategoryFilter";

        public const string TitleSort = "TitleSort";
        public const string RigSort = "RigSort";
        public const string DateSort = "DateSort";

        public TaskViewModel[] Tasks { get; set; }
        public int TotalCount { get; set; }
        public bool IsOverviewPage { get; set; }
        public MenuViewModel Menu { get; set; }

    }
}
