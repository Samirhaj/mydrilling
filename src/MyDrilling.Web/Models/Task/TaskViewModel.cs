﻿using System;

namespace MyDrilling.Web.Models.Task
{
    public class TaskViewModel
    {
        public long Id { get; set; }
        public string Icon { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public DateTime? Date { get; set; }
        public string RigName { get; set; }
        public long RigId { get; set; }
        public string ReferenceNumber { get; set; }
        public string TaskCategory { set; get; }
        public string TaskAction { set; get; }
        public string ItemType { set; get; }
        public string IconHoverText
        {
            get
            {
                return Icon.Replace("meta", "")
                    .Replace("Draft", "<br />Draft")
                    .Replace("task", "Task")
                    .Replace("rigT", "Rig t")
                    .Replace("myT", "My t");
            }
        }
    }
}
