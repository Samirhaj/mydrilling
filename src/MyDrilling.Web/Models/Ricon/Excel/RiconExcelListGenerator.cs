﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using MyDrilling.Infrastructure.Excel;

namespace MyDrilling.Web.Models.Ricon.Excel
{
    public class RiconExcelListGenerator : ExcelListGeneratorBase
    {
        public Stream CreateExcelDocument(IEnumerable<JointItemViewModel> entities)
        {
            var columns = GetColumns();
            var rows = entities.Select(MapEntityToRowData);
            var stream = CreateExcelDocumentStream(columns, rows, "Riser Joints");
            return stream;
        }

        private IEnumerable<ExcelColumnInfo> GetColumns()
        {
            return new List<ExcelColumnInfo>
            {
                new ExcelColumnInfo {Title = "Equipment", Width = 30},
                new ExcelColumnInfo {Title = "Joint ID", Width = 30},
                new ExcelColumnInfo {Title = "Fatigue usage [%]", Width = 30},
                new ExcelColumnInfo {Title = "CoC", Width = 50},
                new ExcelColumnInfo {Title = "Status", Width = 30},
                new ExcelColumnInfo {Title = "Last visual inspection", Width = 30},
                new ExcelColumnInfo {Title = "Last wall thickness inspection", Width = 30},
                new ExcelColumnInfo {Title = "Rig", Width = 30}
            };
        }

        private IEnumerable<string> MapEntityToRowData(JointItemViewModel entity)
        {
            return new List<string>
            {
                entity.ReferenceId,
                entity.TagNumber,
                entity.Fatigue,
                entity.Coc,
                entity.Status,
                entity.LastVisualInspection,
                entity.LastWallThicknessInspection,
                entity.RigReferenceId              
            };
        }
    }
}
