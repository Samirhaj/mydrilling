﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyDrilling.Web.Models.Ricon
{
    public class RiserDocumentsViewModel
    {
        public MenuViewModel Menu { get; set; }
        public RiserDocumentViewModel[] Items { get; set; }
        public int TotalCount { get; set; }
        public string HeaderContent { get; set; }
    }

    public class RiserDocumentViewModel
    {
        public long Id { get; set; }
        public string Description { get; set; }
        public string ReferenceId { get; set; }
        public string Version { get; set; }
        public string Url { get; set; }
        public string PublishedDate { get; set; }
    }
}
