﻿namespace MyDrilling.Web.Models.Ricon
{
    public class MenuViewModel
    {
        public string CurrentActionName { get; set; }
        public int NumberOfRigDocuments { get; set; }
    }
}
