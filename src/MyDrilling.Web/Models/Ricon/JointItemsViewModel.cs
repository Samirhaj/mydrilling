﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyDrilling.Web.Models.Ricon
{
    public class JointItemsViewModel
    {
        public const string CocFilter = "CocFilter";
        public const string StatusFilter = "StatusFilter";

        public const string EquipmentSort = "EquipmentSort";
        public const string JointSort = "JointSort";
        public const string FatigueSort = "FatigueSort";
        public const string CocSort = "CocSort";
        public const string StatusSort = "StatusSort";
        public const string LastVisualInspectionSort = "LastVisualInspectionSort";
        public const string LastWallThicknessInspectionSort = "LastWallThicknessInspectionSort";

        public MenuViewModel Menu { get; set; }
        public JointItemViewModel[] Items { get; set; }
        public int TotalCount { get; set; }
    }

    public class JointItemViewModel
    {
        public long Id { get; set; }
        public string ReferenceId { get; set; }
        public string RigReferenceId { get; set; }
        public string TagNumber { get; set; }
        public string Fatigue { get; set; }
        public string Coc { get; set; }
        public string Status { get; set; }
        public string LastVisualInspection { get; set; }
        public string LastWallThicknessInspection { get; set; }
        public int NumberOfDocuments { get; set; }
    }
}
