﻿using System;

namespace MyDrilling.Web.Models
{
    public class PagerViewModel
    {
        public PagerViewModel(int totalCount, string actionName = null, string controllerName = null, object additionalQueryParams = null)
        {
            if(totalCount < 0) throw new ArgumentOutOfRangeException(nameof(totalCount));

            TotalCount = totalCount;
            ActionName = actionName;
            ControllerName = controllerName;
            AdditionalQueryParams = additionalQueryParams;
        }

        public int TotalCount { get; }
        public string ActionName { get; }
        public string ControllerName { get; }

        public object AdditionalQueryParams { get; }
    }
}
