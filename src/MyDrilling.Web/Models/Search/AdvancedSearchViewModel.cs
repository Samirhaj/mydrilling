﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Office2016.Drawing.ChartDrawing;

namespace MyDrilling.Web.Models.Search
{
    public class AdvancedSearchViewModel
    {
        public string SearchTerm { get; set; }
        public bool IncludeEnquiries { get; set; }
        public bool IncludeBulletins { get; set; }
        public bool IncludeDocumentation { get; set; }
        public SearchItemViewModel[] SearchItems { get; set; }
        public long? TotalCount { get; set; }
    }
}
