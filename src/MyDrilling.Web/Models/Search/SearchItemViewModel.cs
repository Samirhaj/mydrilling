﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyDrilling.Web.Models.Search
{
    public class SearchItemViewModel
    {
        public string Id { get; set; }
        public string EntityType { get; set; }
        public string Name { get; set; }
        public string ReferenceId { get; set; }
        public string Url { get; set; }
        public string RigName { get; set; }
        
    }
}
