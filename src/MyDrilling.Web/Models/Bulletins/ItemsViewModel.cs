﻿using System;
using MyDrilling.Core.Entities;

namespace MyDrilling.Web.Models.Bulletins
{
    public class ItemsViewModel
    {
        public const string TypeFilter = "TypeFilter";

        public const string IdSort = "IdSort";
        public const string DescriptionSort = "DescriptionSort";
        public const string TypeSort = "TypeSort";
        public const string IssuedDateSort = "IssuedDateSort";
        public const string ModifiedDateSort = "ModifiedDateSort";
        public const string RigSort = "RigSort";

        public MenuViewModel Menu { get; set; }
        public ItemViewModel[] Items { get; set; }

        public int TotalCount { get; set; }
    }

    public class ItemViewModel
    {
        public long Id { get; set; }
        public long RigId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public BulletinType Type { get; set; }
        public string IssuedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string RigName { get; set; }
        public ImplementedRowViewModel[] ImplementedRows { get; set; }
        public bool IsRead { get; set; }
    }

    public class ImplementedRowViewModel
    {
        public string GlobalConfirmationId { set; get; }
        public bool CurrentUserCanConfirmBulletin { set; get; }
        public bool Implemented { set; get; }
        public bool NotApplicable { set; get; }
        public string Caption { set; get; }
        public string EquipmentName { set; get; }
        public long EquipmentReferenceId { set; get; }
        public string ImplementedBy { set; get; }
        public string Comment { set; get; }
        public DateTime? ImplementedDate { set; get; }
    }
}
