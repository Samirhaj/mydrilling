﻿using System;
using System.Collections.Generic;

namespace MyDrilling.Web.Models.Bulletins
{
    public class DetailViewModel
    {
        public long Id { get; set; }
        public string Description { set; get; }
        public string Title { set; get; }
        public string Type { set; get; }
        public DateTime? IssuedDate { set; get; }
        public string DocumentId { set; get; }
        public IEnumerable<ImplementedRowViewModel> ImplementedRows { set; get; }
        public long RigId { set; get; }
        public string RigName { set; get; }
        public string RelevantFor { get; set; }
    }

    public class DocumentDetailViewModel
    {
        public string FileName { get; set; }
        public DateTime? Date { get; set; }
    }
}