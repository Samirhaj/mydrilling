﻿namespace MyDrilling.Web.Models.User
{
    public class FindUsersDialogViewModel
    {
        public FindUsersDialogViewModel(string findUsersUrl, string title, string eventName, bool singleUserMode)
        {
            FindUsersUrl = findUsersUrl;
            Title = title;
            EventName = eventName;
            SingleUserMode = singleUserMode;
        }

        public string FindUsersUrl { get; }
        public string Title { get; }
        public string EventName { get; }
        public bool SingleUserMode { get; }
    }
}
