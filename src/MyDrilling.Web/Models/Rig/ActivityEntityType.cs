﻿namespace MyDrilling.Web.Models.Rig
{
    public enum ActivityEntityType
    {
        Enquiry,
        Bulletin,
        Document
    }
}
