﻿namespace MyDrilling.Web.Models.Rig
{
    public class ActivitiesViewModel
    {
        public ActivityViewModel[] Activities { get; set; }
        public int ActivityPageNumber { get; set; }
    }
}
