﻿namespace MyDrilling.Web.Models.Rig
{
    public class RigsViewModel
    {
        public RigViewModel[] Rigs { get; set; }
        public int RigsTotalCount { get; set; }
    }
}
