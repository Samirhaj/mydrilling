﻿namespace MyDrilling.Web.Models.Rig
{
    public class RigOverviewModel
    {
        public RigViewModel RigData { get; set; }
        public int NumberOfOpenEnquiries { get; set; }
        public string RigDowntimeDate { get; set; }
        public long RigDowntimeEnquiryId { get; set; }
    }
}
