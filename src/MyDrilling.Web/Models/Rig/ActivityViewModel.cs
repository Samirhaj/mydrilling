﻿using System;

namespace MyDrilling.Web.Models.Rig
{
    public class ActivityViewModel
    {
        public long EntityId { get; set; }
        public string EntityReferenceId { get; set; }
        public ActivityType ActivityType { get; set; }
        public ActivityEntityType ActivityEntityType { get; set; }
        public string EntityState { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string CreatorName { get; set; }
        public long RigId { get; set; }
        public string RigName { get; set; }
        public DateTime Date { get; set; }
    }

    public static class ActivityViewModelExt
    {
        public static string DraftText = "Draft";
        public static string NewText = "New";
        public static string ActivityTitle(this ActivityViewModel item)
        {
            switch (item.ActivityType)
            {
                case ActivityType.Created:
                    switch (item.ActivityEntityType)
                    {
                        case ActivityEntityType.Enquiry:
                            return item.EntityState == DraftText ? "New draft enquiry" : "New enquiry";
                        case ActivityEntityType.Bulletin:
                            return "New bulletin";
                        case ActivityEntityType.Document:
                            return "New document";
                    }
                    break;
                case ActivityType.Comment:
                        return "New comment";
                case ActivityType.Closed:
                    return "Closed enquiry";
            }

            return string.Empty;
        }
    }

}
