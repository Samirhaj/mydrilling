﻿namespace MyDrilling.Web.Models.Rig
{
    public class RigViewModel
    {
        public long Id { get; set; }
        public long ReferenceId { get; set; }
        public string Name { get; set; }
        public string Owner { get; set; }
        public string Client { get; set; }
        public string Type { get; set; }
        public string Sector { get; set; }
        public bool RigOnDowntime { get; set; }
        
    }
}
