﻿using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Permission;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Web.Areas.Admin.Models.User;
using MyDrilling.Web.Areas.Admin.Models.User.Excel;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("/admin/users")]
    public class UsersController : Controller
    {
        private readonly MyDrillingDb _db;
        public UsersController(MyDrillingDb db)
        {
            _db = db;
        }

        [HttpGet("")]
        [HttpGet("index")]
        public async Task<IActionResult> Index(long rootCustomerId, bool showAll)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsUserAdmin() && !roles.IsSystemAdmin())
                return RedirectToAction("NoAccess", "Home");
            var availableCustomerIds = roles.IsSystemAdmin() ? null : roles.GetUserAdminAccessToCustomers();
           
            var availableCustomers = _db.RootCustomers.Where(x => availableCustomerIds == null || availableCustomerIds.Contains(x.Id))
                .Select(x => new SelectListItem {Text = x.Name, Value = x.Id.ToString()}).OrderBy(x => x.Text).ToList();
            if(availableCustomerIds == null || availableCustomerIds.Length > 1)
                availableCustomers = availableCustomers.Prepend(new SelectListItem("All", "-1")).ToList();
            availableCustomers = availableCustomers.Prepend(new SelectListItem("Select customer", "0")).ToList();

            var pager = HttpContext.GetPagerParameters();
            var order = HttpContext.GetOrderParameters();
            var model = new UsersViewModel
            {
                RootCustomerId = rootCustomerId <= 0 && availableCustomerIds != null && availableCustomerIds.Length == 1 ? availableCustomerIds[0] : rootCustomerId,
                ShowAll = showAll,
                AvailableRootCustomers = availableCustomers
            };
            if (rootCustomerId == 0)
                return View(model);
            var users = _db.Users
                .Where(x => (rootCustomerId == -1 || x.RootCustomerId == rootCustomerId) && (showAll || x.IsActive));
            model.TotalCount = await users.CountAsync();
            var usersForPage = users
                .ApplyOrder(order)
                .ApplyPager(pager)
                .Select(x => new UserViewModel
                {
                    Id = x.Id,
                    Upn = x.Upn,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    IsActive = x.IsActive ? "Yes" : "No",
                    LastLogin = x.LastLogIn.HasValue ? x.LastLogIn.Value.StdDateTimeString(HttpContext.GetCurrentUser().GetTimeZoneInfo()) : ""
                }).ToArray();
            model.Users = usersForPage;
            return View(model);
        }

        [HttpGet("detail/{id}")]
        public async Task<IActionResult> Detail(long id)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsUserAdmin() && !roles.IsSystemAdmin())
                return RedirectToAction("NoAccess", "Home");
            if(id <= 0)
                return RedirectToAction("ContentNotFound", "Home");
            var user = await _db.Users.FirstOrDefaultAsync(x => x.Id == id);
            if(user == null)
                return RedirectToAction("ContentNotFound", "Home");
            var userRootCustomerId = user.RootCustomerId;
            var availableCustomerIds = roles.IsSystemAdmin() ? null : roles.GetUserAdminAccessToCustomers();
            if(userRootCustomerId.HasValue && (availableCustomerIds != null && !availableCustomerIds.Contains((long)userRootCustomerId)))
                return RedirectToAction("NoAccess", "Home");
           
            var model = new UserViewModel
            {
                Id = user.Id,
                Upn = user.Upn,
                FirstName = user.FirstName,
                LastName = user.LastName,
                IsActive = user.IsActive ? "Yes" : "No",
                LastLogin = user.LastLogIn.HasValue ? user.LastLogIn.Value.StdDateTimeString(HttpContext.GetCurrentUser().GetTimeZoneInfo()) : ""
            };
            return View(model);
        }

        [HttpGet("useraccesstorigs")]
        public IActionResult UserAccessToRigs(long userId)
        {
            return ViewComponent("UserAccessToRigs", new { userId });
        }

        [HttpGet("useraccesstocustomers")]
        public IActionResult UserAccessToCustomers(long userId)
        {
            return ViewComponent("UserAccessToCustomers", new { userId });
        }

        [HttpGet("getadduserdialog")]
        public IActionResult GetAddUserDialog()
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsUserAdmin() && !roles.IsSystemAdmin())
                return RedirectToAction("NoAccess", "Home");
            var availableCustomerIds = roles.IsSystemAdmin() ? null : roles.GetUserAdminAccessToCustomers();
            var availableCustomers = _db.RootCustomers.Where(x => availableCustomerIds == null || availableCustomerIds.Contains(x.Id))
                .Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).OrderBy(x => x.Text).ToList();
            availableCustomers = availableCustomers.Prepend(new SelectListItem("Select customer", "0")).ToList();
            var model = new AddUserViewModel { AvailableRootCustomers = availableCustomers };
            return View("AddUser", model);
        }

        [HttpPost("AddUser")]
        public async Task<IActionResult> AddUser(AddUserViewModel addUser)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsUserAdmin() && !roles.IsSystemAdmin())
                return StatusCode(401, "No access");
            if (string.IsNullOrEmpty(addUser.Upn) || addUser.RootCustomerId <= 0)
                return StatusCode(500, new { message = "Data is missing."});
            var availableCustomerIds = roles.IsSystemAdmin() ? null : roles.GetUserAdminAccessToCustomers();
            if(availableCustomerIds != null && !availableCustomerIds.Contains(addUser.RootCustomerId))
                return StatusCode(401, "No access");
            var emailAttr = new EmailAddressAttribute();
            if(!emailAttr.IsValid(addUser.Upn))
                return StatusCode(500, new { message = "Upn should be in email format."});
            var user = await _db.Users.FirstOrDefaultAsync(x => x.Upn == addUser.Upn);
            if (user != null)
                return StatusCode(500,new { message = "User with selected Upn already exists." });
            var newUser = new User(addUser.Upn) {RootCustomerId = addUser.RootCustomerId, IsActive = true};
            _db.Users.Add(newUser);
            await _db.SaveChangesAsync();

            return StatusCode(200,new { id = newUser.Id, message = "User has been successfully created." });
        }

        [HttpGet("getaccesstocustomerdialog")]
        public IActionResult GetAccessToCustomerDialog(long userId)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsUserAdmin() && !roles.IsSystemAdmin())
                return RedirectToAction("NoAccess", "Home");
            var availableCustomerIds = roles.IsSystemAdmin() ? null : roles.GetUserAdminAccessToCustomers();
            var withAccessToCustomerIds = _db.Permission_UserRoles
                .Where(x => x.Role == RoleInfo.Role.CustomerAccess && x.UserId == userId).Select(x => (long)x.RootCustomerId).ToArray();
            var availableCustomers = _db.RootCustomers.Where(x => (availableCustomerIds == null || availableCustomerIds.Contains(x.Id)) && !withAccessToCustomerIds.Contains(x.Id))
                .Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).OrderBy(x => x.Text).ToList();
            availableCustomers = availableCustomers.Prepend(new SelectListItem("Select customer", "0")).ToList();
            var model = new AddAccessToCustomerViewModel
            {
                UserId = userId, 
                AvailableRootCustomers = availableCustomers
            };
            return View("AddAccessToCustomer", model);
        }

        [HttpPost("AddAccessToCustomer")]
        public async Task<IActionResult> AddAccessToCustomer(AddAccessToCustomerViewModel addAccessToCustomer)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsUserAdmin() && !roles.IsSystemAdmin())
                return StatusCode(401, "No access");
            if (addAccessToCustomer.UserId <= 0 || addAccessToCustomer.RootCustomerId <= 0)
                return StatusCode(500, "Data is missing.");
            var availableCustomerIds = roles.IsSystemAdmin() ? null : roles.GetUserAdminAccessToCustomers();
            if (availableCustomerIds != null && !availableCustomerIds.Contains(addAccessToCustomer.RootCustomerId))
                return StatusCode(401, "No access");
            var userRole = await _db.Permission_UserRoles.FirstOrDefaultAsync(x =>
                x.Role == RoleInfo.Role.CustomerAccess 
                && x.UserId == addAccessToCustomer.UserId &&
                x.RootCustomerId == addAccessToCustomer.RootCustomerId);
            if(userRole != null)
                return StatusCode(500, "User already has access to selected customer.");
            var user = await _db.Users.FirstOrDefaultAsync(x => x.Id == addAccessToCustomer.UserId);
            if(user == null)
                return StatusCode(500, "User doesn't exist'.");
            var rootCustomer = await _db.RootCustomers.FirstOrDefaultAsync(x => x.Id == addAccessToCustomer.RootCustomerId);
            if(rootCustomer == null)
                return StatusCode(500, "Customer doesn't exist'.");

            var newUserRole = UserRole.AssignRootCustomerRole(user, RoleInfo.ByRole[RoleInfo.Role.CustomerAccess], rootCustomer);
            _db.Permission_UserRoles.Add(newUserRole);
            await _db.SaveChangesAsync();
            return StatusCode(200, new {id = addAccessToCustomer.UserId, message = "Access to customer has been added."});
        }

        [HttpGet("getaccesstorigdialog")]
        public async Task<IActionResult> GetAccessToRigDialog(long userId = 0, long profileId = 0)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsUserAdmin() && !roles.IsSystemAdmin())
                return RedirectToAction("NoAccess", "Home");
            var availableRigIds = roles.IsSystemAdmin() ? null : roles.GetViewAccessToRigs();
            var withAccessToRigIds = _db.Permission_UserProfiles
                .Where(x => x.UserId == userId).Select(x => (long)x.RigId).ToArray();
            var dbAvailableRigs = await _db.Rigs.Where(x => x.RootOwnerId.HasValue && (availableRigIds == null || availableRigIds.Contains(x.Id)) && !withAccessToRigIds.Contains(x.Id)).ToArrayAsync();
            var availableRigs = dbAvailableRigs.Select(x => new SelectListItem { Text = x.NameFormatted, Value = x.Id.ToString() }).OrderBy(x => x.Text).ToList();
            availableRigs = availableRigs.Prepend(new SelectListItem("Select rig", "0")).ToList();
            var availableAccessProfiles = _db.Permission_Profiles.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).OrderBy(x => x.Text).ToList();
            var model = new AddAccessToRigViewModel
            {
                UserId = userId,
                ProfileId = profileId,
                AvailableRigs = availableRigs,
                AvailableAccessProfiles = availableAccessProfiles

            };
            return View("AddAccessToRig", model);
        }

        [HttpPost("addaccesstorig")]
        public async Task<IActionResult> AddAccessToRig(AddAccessToRigViewModel addAccessToRigs)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsUserAdmin() && !roles.IsSystemAdmin())
                return StatusCode(401, "No access");
            if (addAccessToRigs.UserId <= 0 || addAccessToRigs.RigId <= 0 || addAccessToRigs.ProfileId <= 0)
                return StatusCode(500, "Data is missing.");
            var availableRigIds = roles.IsSystemAdmin() ? null : roles.GetViewAccessToRigs();
            if (availableRigIds != null && !availableRigIds.Contains(addAccessToRigs.RigId))
                return StatusCode(401, "No access");
            var userProfile = await _db.Permission_UserProfiles.FirstOrDefaultAsync(x =>
                x.UserId == addAccessToRigs.UserId &&
                x.RigId == addAccessToRigs.RigId);
            if (userProfile != null)
                return StatusCode(500, "User already has access to selected rig.");
            var user = await _db.Users.FirstOrDefaultAsync(x => x.Id == addAccessToRigs.UserId);
            if (user == null)
                return StatusCode(500, "User doesn't exist'.");
            var rig = await _db.Rigs.FirstOrDefaultAsync(x => x.Id == addAccessToRigs.RigId);
            if (rig == null)
                return StatusCode(500, "Rig doesn't exist'.");
            var profile = await _db.Permission_Profiles.FirstOrDefaultAsync(x => x.Id == addAccessToRigs.ProfileId);
            if(profile == null)
                return StatusCode(500, "Profile doesn't exist'.");

            var newUserProfile = new UserProfile(profile, user, rig);
            _db.Permission_UserProfiles.Add(newUserProfile);
            await _db.SaveChangesAsync();
            return StatusCode(200, new { id = addAccessToRigs.UserId, message = "Access to rig has been added." });
        }

        [HttpPost("deleteaccesstocustomer")]
        public async Task<IActionResult> DeleteAccessToCustomer(long id, long userId)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            var userRole = await _db.Permission_UserRoles.FirstOrDefaultAsync(x => x.Id == id);
            if (userRole == null)
                return RedirectToAction("ContentNotFound", "Home");
            var availableCustomerIds = roles.IsSystemAdmin() ? null : roles.GetUserAdminAccessToCustomers();
            if(userRole.RootCustomerId.HasValue && availableCustomerIds != null && !availableCustomerIds.Contains((long)(userRole.RootCustomerId)))
                return RedirectToAction("NoAccess", "Home");
            _db.Permission_UserRoles.Remove(userRole);
            await _db.SaveChangesAsync();
            return RedirectToAction("Detail", "Users", new { id = userId });
        }

        [HttpPost("deleteaccesstorig")]
        public async Task<IActionResult> DeleteAccessToRig(long id, long userId)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            var userProfile = await _db.Permission_UserProfiles.FirstOrDefaultAsync(x => x.Id == id);
            if (userProfile == null)
                return RedirectToAction("ContentNotFound", "Home");
            var availableRigIds = roles.IsSystemAdmin() ? null : roles.GetViewAccessToRigs();
            if (availableRigIds != null && !availableRigIds.Contains(userProfile.RigId))
                return RedirectToAction("NoAccess", "Home");
            _db.Permission_UserProfiles.Remove(userProfile);
            await _db.SaveChangesAsync();
            return RedirectToAction("Detail", "Users", new { id = userId });
        }

        [HttpPost("changeuserstate")]
        public async Task<IActionResult> ChangeUserState(long userId)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            var user = await _db.Users.FirstOrDefaultAsync(x => x.Id == userId);
            if (user == null)
                return RedirectToAction("ContentNotFound", "Home");
            var availableCustomerIds = roles.IsSystemAdmin() ? null : roles.GetUserAdminAccessToCustomers();
            if (user.RootCustomerId.HasValue && availableCustomerIds != null && !availableCustomerIds.Contains((long)(user.RootCustomerId)))
                return RedirectToAction("NoAccess", "Home");
            user.IsActive = !user.IsActive;
            await _db.SaveChangesAsync();
            return RedirectToAction("Detail", "Users", new { id = userId });
        }

        [HttpGet("search")]
        public async Task<IActionResult> Search(string term, int limit = 10)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsUserAdmin() && !roles.IsSystemAdmin())
                return Json("Access denied.");
            term = term.ToLower();

            var userResults = await _db.Users
                .ApplyPermissions(roles)
                .ApplySearchFilter(term)
                .OrderBy(x => x.Upn)
                .Take(limit)
                .Select(x => new
                {
                    id = x.Id,
                    upn = x.Upn,
                    firstName = x.FirstName,
                    lastName = x.LastName
                })
                .ToArrayAsync();
            return Json(userResults);
        }

        [HttpGet("exporttoexcel")]
        public async Task<IActionResult> ExportToExcel(long rootCustomerId, bool showAll)
        {
            var drillDowns = HttpContext.GetDrillDownParameters();
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsUserAdmin() && !roles.IsSystemAdmin())
                return Json("Access denied.");
            var order = HttpContext.GetOrderParameters();
            var users = _db.Users
                .Where(x => (rootCustomerId == -1 || x.RootCustomerId == rootCustomerId) && (showAll || x.IsActive))
                .ApplyOrder(order)
                .Select(x => new UserViewModel
                {
                    Id = x.Id,
                    Upn = x.Upn,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    IsActive = x.IsActive ? "Yes" : "No",
                    LastLogin = x.LastLogIn.HasValue ? x.LastLogIn.Value.StdDateTimeString(HttpContext.GetCurrentUser().GetTimeZoneInfo()) : ""
                }).ToArray();
            
            return File(
                new UserExcelListGenerator().CreateExcelDocument(users),
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                $"Users - {System.DateTime.UtcNow.ToString(HtmlExt.StdDateFormat, CultureInfo.InvariantCulture)}.xlsx");
        }

    }

    public static class UserFilterExt
    {
        public static IQueryable<User> ApplySearchFilter(this IQueryable<User> query, string term) =>
            string.IsNullOrEmpty(term) ? query : query.Where(x => x.Upn.Contains(term) || x.Email.Contains(term) || x.FirstName.Contains(term) || x.LastName.Contains(term));

        public static IQueryable<User> ApplyPermissions(this IQueryable<User> query, CombinedUserRoles roles)
        {
            var availableCustomerIds = roles.GetUserAdminAccessToCustomers();
            return roles.IsSystemAdmin() 
                ? query
                : query.Where(x => availableCustomerIds.Contains((long)x.RootCustomerId));
        }

        public static IQueryable<User> ApplyPager(this IQueryable<User> query, PagerParameters pager)
        {
            return query
                .Skip(pager.PageSize * (pager.CurrentPageNumber - 1))
                .Take(pager.PageSize);
        }

        public static IOrderedQueryable<User> ApplyOrder(this IQueryable<User> query, SortParameters sort)
        {
            switch (sort.SortField)
            {
                case UsersViewModel.UpnSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.Upn)
                        : query.OrderByDescending(b => b.Upn);
                case UsersViewModel.FirstNameSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.FirstName)
                        : query.OrderByDescending(b => b.FirstName);
                case UsersViewModel.LastNameSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.LastName)
                        : query.OrderByDescending(b => b.LastName);
                case UsersViewModel.IsActiveSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.IsActive)
                        : query.OrderByDescending(b => b.IsActive);
                case UsersViewModel.LastLoginSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.LastLogIn)
                        : query.OrderByDescending(b => b.LastLogIn);
                default:
                    return query.OrderBy(b => b.Upn);
            }
        }

        public static IQueryable<UserAccessToRig> ApplyPager(this IQueryable<UserAccessToRig> query, PagerParameters pager)
        {
            return query
                .Skip(pager.PageSize * (pager.CurrentPageNumber - 1))
                .Take(pager.PageSize);
        }

        public static IOrderedQueryable<UserAccessToRig> ApplyOrder(this IQueryable<UserAccessToRig> query, SortParameters sort)
        {
            switch (sort.SortField)
            {
                case UserAccessToRigViewModel.RigNameSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.RigName)
                        : query.OrderByDescending(b => b.RigName);
                case UserAccessToRigViewModel.ProfileNameSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.ProfileName)
                        : query.OrderByDescending(b => b.ProfileName);
                default:
                    return query.OrderBy(b => b.RigName);
            }
        }

        public static IQueryable<UserAccessToCustomer> ApplyPager(this IQueryable<UserAccessToCustomer> query, PagerParameters pager)
        {
            return query
                .Skip(pager.PageSize * (pager.CurrentPageNumber - 1))
                .Take(pager.PageSize);
        }

        public static IOrderedQueryable<UserAccessToCustomer> ApplyOrder(this IQueryable<UserAccessToCustomer> query, SortParameters sort)
        {
            switch (sort.SortField)
            {
                case UserAccessToCustomerViewModel.NameSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.CustomerName)
                        : query.OrderByDescending(b => b.CustomerName);
                default:
                    return query.OrderBy(b => b.CustomerName);
            }
        }
    }
}