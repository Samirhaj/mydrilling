﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Azure.Storage.Sas;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Rig;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.Storage;
using MyDrilling.Infrastructure.Storage.Blob;
using MyDrilling.Web.Areas.Admin.Models.Customer;
using MyDrilling.Web.Areas.Admin.Models.Facility;
using MyDrilling.Web.Controllers;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("/admin/facilities")]
    public class FacilitiesController : Controller
    {
        private readonly MyDrillingDb _db;
        private readonly IBlobSasGenerator _blobSasGenerator;
        public FacilitiesController(MyDrillingDb db, IBlobSasGenerator blobSasGenerator)
        {
            _db = db;
            _blobSasGenerator = blobSasGenerator;
        }

        [HttpGet("")]
        [HttpGet("index")]
        public async Task<IActionResult> Index(string startsWith)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin())
                return RedirectToAction("NoAccess", "Home");
            var pager = HttpContext.GetPagerParameters();
            var order = HttpContext.GetOrderParameters();
            
            var rigs = _db.Rigs.ApplyOrder(order).Select(x => new FacilityViewModel
            {
                Id = x.Id,
                Name = x.Name,
                OwnerName = x.Owner != null ? x.Owner.Name : "",
                RigType = x.TypeCode.GetRigTypeInfo(),
                Sector = x.Sector
            });
            var rigsLetters = _db.Rigs.Select(x => x.Name[0].ToString().ToUpper()).ToArray().Distinct().OrderBy(x => x).ToArray();
            if (!string.IsNullOrEmpty(startsWith))
            {
                rigs = rigs.Where(x => x.Name.StartsWith(startsWith)).Select(x => x);
            }
            var totalCount = rigs.Count();
            var pagedRigs = await rigs.ApplyPager(pager).ToArrayAsync();
            var model = new FacilitiesViewModel
            {
                TotalCount = totalCount,
                Items = pagedRigs,
                StartLetters = rigsLetters,
                StartsWith = startsWith
            };
            return View(model);
        }

        [HttpGet("detail/{id}")]
        public async Task<IActionResult> Detail(long id)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin())
                return RedirectToAction("NoAccess", "Home");
            var rig = await _db.Rigs
                .Include(x => x.Owner)
                .Include(x => x.ResponsibleUser)
                .Include(x => x.RootOwner)
                .Include(x => x.RootOperator)
                .FirstOrDefaultAsync(x => x.Id == id);
            if(rig == null)
                return RedirectToAction("ContentNotFound", "Home");
            var imageUrl = "";

            if(rig.ResponsibleUser != null)
                imageUrl = await GetUserImageBlobUri(rig.ResponsibleUser.Image);
            var model = new FacilityDetailViewModel
            {
                Id = rig.Id,
                Name = rig.NameFormatted,
                ReferenceId = rig.ReferenceId,
                Sector = rig.Sector,
                OwnerId = rig.OwnerId,
                OwnerName = rig.Owner?.Name,
                OwnerReferenceId = rig.Owner?.ReferenceId,
                RigType = rig.TypeCode.GetRigTypeInfo(),
                RootOperatorId = rig.RootOperatorId,
                RootOperatorName = rig.RootOperator?.Name,
                RootOwnerId = rig.RootOwnerId,
                RootOwnerName = rig.RootOwner?.Name,
                Sam = rig.ResponsibleUser != null ? new ServiceAccountManager
                {
                    Id = rig.ResponsibleUser.Id,
                    DisplayName = rig.ResponsibleUser.DisplayName,
                    EmailAddress = !string.IsNullOrEmpty(rig.ResponsibleUser.Email) ? rig.ResponsibleUser.Email : rig.ResponsibleUser.Upn,
                    MobileNumber = rig.ResponsibleUser.MobileNumber,
                    PhoneNumber = rig.ResponsibleUser.PhoneNumber,
                    ImageUrl = imageUrl

                } : null
            };
            return View(model);
        }

        [HttpGet("getaddsamdialog")]
        public IActionResult GetAddSamDialog(long facilityId)
        {
            if (facilityId <= 0)
                return StatusCode(500, "Missing data");
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin())
                return RedirectToAction("NoAccess", "Home");
           
            var model = new AddSamViewModel { FacilityId = facilityId};
            return View("AddSamForFacility", model);
        }


        [HttpPost("addsamforfacility")]
        public async Task<IActionResult> AddSamForFacility(AddSamViewModel addSam)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin())
                return StatusCode(401, "No access");
            if (addSam.UserId <= 0 || addSam.FacilityId <= 0)
                return StatusCode(500, new { message = "Data is missing." });
            var rig = await _db.Rigs.FirstOrDefaultAsync(x => x.Id == addSam.FacilityId);
            if(rig == null)
                return StatusCode(500, new { message = "Facility doesn't exist." });
            var user = await _db.Users.FirstOrDefaultAsync(x => x.Id == addSam.UserId);
            if (user == null)
                return StatusCode(500, new { message = "Selected user doesn't exist." });
            rig.ResponsibleUserId = addSam.UserId;
            await _db.SaveChangesAsync();

            return StatusCode(200, new { message = "SAM has been successfully added." });
        }

        [HttpGet("searchsam")]
        public async Task<IActionResult> SearchSam(string term, int limit = 10)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsUserAdmin() && !roles.IsSystemAdmin())
                return Json("Access denied.");
            term = term.ToLower();

            var userResults = await _db.Users
                .Where(x => x.IsInternal)
                .ApplyPermissions(roles)
                .ApplySearchFilter(term)
                .OrderBy(x => x.Upn)
                .Take(limit)
                .Select(x => new
                {
                    id = x.Id,
                    upn = x.Upn,
                    firstName = x.FirstName,
                    lastName = x.LastName
                })
                .ToArrayAsync();
            return Json(userResults);
        }

        [HttpPost("removesamforfacility/{id}")]
        public async Task<IActionResult> RemoveSamForFacility(long id)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin())
                return RedirectToAction("NoAccess", "Home");
            var dbRig = await _db.Rigs.FirstOrDefaultAsync(x => x.Id == id);
            if (dbRig == null)
                return RedirectToAction("ContentNotFound", "Home");
            dbRig.ResponsibleUserId = null;
            dbRig.Modified = DateTime.UtcNow;
            await _db.SaveChangesAsync();

            return RedirectToAction("Detail", "Facilities", new {id});
        }

        [HttpGet("getaddcustomerdialog")]
        public IActionResult GetAddCustomerDialog(long facilityId, bool isOwner)
        {
            if (facilityId <= 0)
                return StatusCode(500, "Missing data");
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin())
                return RedirectToAction("NoAccess", "Home");
            var availableCustomers = _db.RootCustomers.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString() }).OrderBy(x => x.Text).ToList();
            availableCustomers = availableCustomers.Prepend(new SelectListItem("Select customer", "0")).ToList();
            var model = new AddCustomerViewModel
            {
                FacilityId = facilityId, 
                IsOwner = isOwner,
                AvailableRootCustomers = availableCustomers
            };
            return View("AddCustomerForFacility", model);
        }


        [HttpPost("addcustomerforfacility")]
        public async Task<IActionResult> AddCustomerForFacility(AddCustomerViewModel addCustomer)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin())
                return StatusCode(401, "No access");
            if (addCustomer.FacilityId <= 0 
                || (addCustomer.IsOwner && (!addCustomer.RootOwnerId.HasValue || addCustomer.RootOwnerId <= 0)) 
                || (!addCustomer.IsOwner && (!addCustomer.RootOperatorId.HasValue || addCustomer.RootOperatorId <= 0)))
                return StatusCode(500, new { message = "Data is missing." });
            var dbRig = await _db.Rigs.FirstOrDefaultAsync(x => x.Id == addCustomer.FacilityId);
            if (dbRig == null)
                return StatusCode(500, new { message = "Facility doesn't exist." });
            var customer = await _db.RootCustomers.FirstOrDefaultAsync(x =>
                addCustomer.IsOwner && x.Id == addCustomer.RootOwnerId ||
                !addCustomer.IsOwner && x.Id == addCustomer.RootOperatorId);
            if(customer == null)
                return StatusCode(500, new { message = "Selected customer doesn't exist." });
            if (addCustomer.IsOwner)
            {
                dbRig.RootOwnerId = customer.Id;
            }
            else
            {
                dbRig.RootOperatorId = customer.Id;
            }
            dbRig.Modified = DateTime.UtcNow;
            await _db.SaveChangesAsync();

            return StatusCode(200, new { message = "Customer has been successfully added." });
        }

        [HttpPost("removecustomerforfacility")]
        public async Task<IActionResult> RemoveCustomerForFacility(long facilityId, bool isOwner)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin())
                return RedirectToAction("NoAccess", "Home");
            if (facilityId <= 0)
                return RedirectToAction("ContentNotFound", "Home");
            var dbRig = await _db.Rigs.FirstOrDefaultAsync(x => x.Id == facilityId);
            if (dbRig == null)
                return RedirectToAction("ContentNotFound", "Home");
            if (isOwner)
            {
                dbRig.RootOwnerId = null;
            }
            else
            {
                dbRig.RootOperatorId = null;
            }
            dbRig.Modified = DateTime.UtcNow;
            await _db.SaveChangesAsync();

            return RedirectToAction("Detail", "Facilities", new { id = facilityId });
        }

        [HttpGet("search")]
        public async Task<IActionResult> Search(string term, int limit = 10)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin())
                return Json("Access denied.");
            term = term.ToLower();

            var facilityResults = await _db.Rigs
                .ApplySearchFilter(term)
                .OrderBy(x => x.Name)
                .Take(limit)
                .Select(x => new
                {
                    id = x.Id,
                    name = x.NameFormatted,
                    referenceId = x.ReferenceId
                })
                .ToArrayAsync();
            return Json(facilityResults);
        }

        private async Task<string> GetUserImageBlobUri(string image)
        {
            if (string.IsNullOrEmpty(image)) return "";
            var blobName = image;
            var blobUri = await _blobSasGenerator.GetSasBlobUri(blobName, StorageConfig.BlobUserImagesContainerName, BlobAccountSasPermissions.Read);
            return blobUri.OriginalString;
        }
    }

    public static class FacilityFilterExt
    {
        public static IQueryable<Rig> ApplySearchFilter(this IQueryable<Rig> query, string term) =>
            string.IsNullOrEmpty(term) ? query : query.Where(x => x.Name.Contains(term) || x.ReferenceId.ToString().Contains(term));
        
        public static IQueryable<FacilityViewModel> ApplyPager(this IQueryable<FacilityViewModel> query, PagerParameters pager)
        {
            return query
                .Skip(pager.PageSize * (pager.CurrentPageNumber - 1))
                .Take(pager.PageSize);
        }

        public static IOrderedQueryable<Rig> ApplyOrder(this IQueryable<Rig> query, SortParameters sort)
        {
            switch (sort.SortField)
            {
                case FacilitiesViewModel.NameSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.Name)
                        : query.OrderByDescending(b => b.Name);
                case FacilitiesViewModel.OwnerSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.Owner.Name)
                        : query.OrderByDescending(b => b.Owner.Name);
                case FacilitiesViewModel.RigTypeSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.TypeCode)
                        : query.OrderByDescending(b => b.TypeCode);
                case FacilitiesViewModel.SectorSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.Sector)
                        : query.OrderByDescending(b => b.Sector);
                default:
                    return query.OrderBy(b => b.Name);
            }
        }
    }
}
