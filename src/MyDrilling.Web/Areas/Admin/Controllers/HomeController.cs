﻿using Microsoft.AspNetCore.Mvc;

namespace MyDrilling.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult ContentNotFound()
        {
            return View();
        }

        public IActionResult NoAccess()
        {
            return View();
        }
    }
}