﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core.Entities;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Web.Areas.Admin.Models.Customer;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("/admin/customers")]
    public class CustomersController : Controller
    {
        private readonly MyDrillingDb _db;
        public CustomersController(MyDrillingDb db)
        {
            _db = db;
        }

        [HttpGet("")]
        [HttpGet("index")]
        public async Task<IActionResult> Index()
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin() && !roles.IsCustomerAdmin())
                return RedirectToAction("NoAccess", "Home");
            var pager = HttpContext.GetPagerParameters();
            var order = HttpContext.GetOrderParameters();
            var adminForCustomers = roles.IsSystemAdmin() ? null : roles.GetCustomerAdminAccess();
            var availableCustomers = _db.RootCustomers
                .Where(x => (adminForCustomers == null || adminForCustomers.Contains(x.Id)) && !x.IsDeleted);
            var totalCount = await availableCustomers.CountAsync();
            var customers = await availableCustomers.ApplyOrder(order).ApplyPager(pager).ToArrayAsync();
            var model = new RootCustomersViewModel
            {
                TotalCount = totalCount,
                Items = customers.Select(x => new RootCustomerViewModel
                {
                    Id = x.Id, Name = x.Name
                }).ToArray()
            };
            return View(model);
        }

        [HttpGet("detail/{id}")]
        public async Task<IActionResult> Detail(long id)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin() && !roles.IsCustomerAdmin())
                return RedirectToAction("NoAccess", "Home");
            var adminForCustomers = roles.IsSystemAdmin() ? null : roles.GetCustomerAdminAccess();
            if(adminForCustomers != null && !adminForCustomers.Contains(id))
                return RedirectToAction("NoAccess", "Home");
            var rootCustomer = await _db.RootCustomers.Include(x => x.Customers).FirstOrDefaultAsync(x => x.Id == id);
            if (rootCustomer == null || rootCustomer.IsDeleted)
                return RedirectToAction("ContentNotFound", "Home");
            var relatedCustomers = rootCustomer.Customers.Select(x => new CustomerViewModel
            {
                Id = x.Id,
                Name = x.Name,
                ReferenceId = x.ReferenceId,
                IsDefault = x.Id == rootCustomer.DefaultCustomerId
            }).ToArray();
            var model = new RootCustomerDetailViewModel
            {
                Id = rootCustomer.Id,
                Name = rootCustomer.Name,
                DefaultCustomerId = rootCustomer.DefaultCustomerId,
                DefaultCustomerName = rootCustomer.DefaultCustomer != null ? rootCustomer.DefaultCustomer.Name : "",
                RelatedCustomers = relatedCustomers
            };
            return View(model);
        }

        [HttpPost("delete/{id}")]
        public async Task<IActionResult> DeleteCustomer(long id)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            var user = HttpContext.GetCurrentUser();
            if (!roles.IsSystemAdmin())
                return RedirectToAction("NoAccess", "Home");
            var adminForCustomers = roles.IsSystemAdmin() ? null : roles.GetCustomerAdminAccess();
            if (adminForCustomers != null && !adminForCustomers.Contains(id))
                return RedirectToAction("NoAccess", "Home");
            var dbRootCustomer = await _db.RootCustomers.FirstOrDefaultAsync(x => x.Id == id); 
            if(dbRootCustomer == null)
                return RedirectToAction("ContentNotFound", "Home");
            dbRootCustomer.IsDeleted = true;
            dbRootCustomer.Modified = DateTime.UtcNow;
            dbRootCustomer.ModifiedById = user.Id;
            await _db.SaveChangesAsync();

            return RedirectToAction("Index", "Customers");
        }

        [HttpPost("savecustomername")]
        public async Task<IActionResult> SaveCustomerName(RootCustomerDetailViewModel rootCustomer)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            var user = HttpContext.GetCurrentUser();
            if (!roles.IsSystemAdmin() && !roles.IsCustomerAdmin())
                return RedirectToAction("NoAccess", "Home");
            var adminForCustomers = roles.IsSystemAdmin() ? null : roles.GetCustomerAdminAccess();
            if (adminForCustomers != null && !adminForCustomers.Contains(rootCustomer.Id))
                return RedirectToAction("NoAccess", "Home");
            var dbRootCustomer = await _db.RootCustomers.FirstOrDefaultAsync(x => x.Id == rootCustomer.Id);
            if(dbRootCustomer == null)
                return RedirectToAction("ContentNotFound", "Home");
            dbRootCustomer.Name = rootCustomer.Name;
            dbRootCustomer.Modified = DateTime.UtcNow;
            dbRootCustomer.ModifiedById = user.Id;
            await _db.SaveChangesAsync();
            return RedirectToAction("Detail", "Customers", new {id = rootCustomer.Id});
        }

        [HttpPost("setdefaultcustomer")]
        public async Task<IActionResult> SetDefaultCustomer(long rootCustomerId, long defaultCustomerId)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            var user = HttpContext.GetCurrentUser();
            if (!roles.IsSystemAdmin() && !roles.IsCustomerAdmin())
                return RedirectToAction("NoAccess", "Home");
            var adminForCustomers = roles.IsSystemAdmin() ? null : roles.GetCustomerAdminAccess();
            if (adminForCustomers != null && !adminForCustomers.Contains(rootCustomerId))
                return RedirectToAction("NoAccess", "Home");

            var dbRootCustomer = await _db.RootCustomers.FirstOrDefaultAsync(x => x.Id == rootCustomerId);
            var dbCustomer = await _db.Customers.FirstOrDefaultAsync(x => x.Id == defaultCustomerId);
            if (dbRootCustomer == null || dbCustomer == null)
                return RedirectToAction("ContentNotFound", "Home");
            dbRootCustomer.SetDefaultCustomer(dbCustomer);
            dbRootCustomer.Modified = DateTime.UtcNow;
            dbRootCustomer.ModifiedById = user.Id;
            await _db.SaveChangesAsync();
            return RedirectToAction("Detail", "Customers", new { id = rootCustomerId });
        }

        [HttpPost("deleterelatedcustomer")]
        public async Task<IActionResult> DeleteRelatedCustomer(long rootCustomerId, long customerId)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin() && !roles.IsCustomerAdmin())
                return RedirectToAction("NoAccess", "Home");
            var adminForCustomers = roles.IsSystemAdmin() ? null : roles.GetCustomerAdminAccess();
            if (adminForCustomers != null && !adminForCustomers.Contains(rootCustomerId))
                return RedirectToAction("NoAccess", "Home");
            
            var dbRootCustomer = await _db.RootCustomers.FirstOrDefaultAsync(x => x.Id == rootCustomerId);
            var dbCustomer = await _db.Customers.FirstOrDefaultAsync(x => x.Id == customerId);
            if (dbRootCustomer == null || dbCustomer == null)
                return RedirectToAction("ContentNotFound", "Home");
            dbCustomer.RootCustomerId = null;
            dbCustomer.Modified = DateTime.UtcNow;
            if (dbRootCustomer.DefaultCustomerId.HasValue && dbRootCustomer.DefaultCustomerId.Value == dbCustomer.Id)
                return StatusCode(500, "It is not possible to remove default customer.");
            await _db.SaveChangesAsync();
            return RedirectToAction("Detail", "Customers", new { id = rootCustomerId });
        }

        [HttpGet("getaddrelatedcustomerdialog")]
        public IActionResult GetAddRelatedCustomerDialog(long rootCustomerId)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin() && !roles.IsCustomerAdmin())
                return RedirectToAction("NoAccess", "Home");
            var adminForCustomers = roles.IsSystemAdmin() ? null : roles.GetCustomerAdminAccess();
            if (adminForCustomers != null && !adminForCustomers.Contains(rootCustomerId))
                return RedirectToAction("NoAccess", "Home");
            var model = new AddRelatedCustomerViewModel { RootCustomerId = rootCustomerId };
            return View("AddRelatedCustomer", model);
        }

        [HttpPost("addrelatedcustomer")]
        public async Task<IActionResult> AddRelatedCustomer(AddRelatedCustomerViewModel relatedCustomer)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin() && !roles.IsCustomerAdmin())
                return StatusCode(401, new { message = "No access." });
            var adminForCustomers = roles.IsSystemAdmin() ? null : roles.GetCustomerAdminAccess();
            if (adminForCustomers != null && !adminForCustomers.Contains(relatedCustomer.RootCustomerId))
                return StatusCode(401, new { message = "No access." });
            var dbRootCustomer = await _db.RootCustomers.FirstOrDefaultAsync(x => x.Id == relatedCustomer.RootCustomerId);
            var dbCustomer = await _db.Customers.FirstOrDefaultAsync(x => x.ReferenceId == relatedCustomer.CustomerReference.Trim());
            if (dbRootCustomer == null || dbCustomer == null)
                return StatusCode(404, new {message = "Content not found."});
            if (relatedCustomer.IsDefault)
            {
                dbRootCustomer.SetDefaultCustomer(dbCustomer);
            }
            else
            {
                dbRootCustomer.AddCustomer(dbCustomer);
            }
            await _db.SaveChangesAsync();
            return StatusCode(200, new { id = relatedCustomer.RootCustomerId, message = "Sold to party has been added." });
        }

        [HttpGet("getaddrootcustomerdialog")]
        public IActionResult GetAddRootCustomerDialog()
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin())
                return RedirectToAction("NoAccess", "Home");
            
            var model = new AddRootCustomerViewModel();
            return View("AddRootCustomer", model);
        }

        [HttpPost("addrootcustomer")]
        public async Task<IActionResult> AddRootCustomer(AddRootCustomerViewModel rootCustomer)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin())
                return StatusCode(401, new { message = "No access." });
            if (rootCustomer == null || string.IsNullOrEmpty(rootCustomer.Name) || string.IsNullOrEmpty(rootCustomer.CustomerReference))
                return StatusCode(500, new { message = "Missing data." });
            var dbRootCustomer = await _db.RootCustomers.FirstOrDefaultAsync(x => x.Name == rootCustomer.Name && !x.IsDeleted);
            if(dbRootCustomer != null)
                return StatusCode(500, new { message = "Customer with the same name already exists." });
            var dbCustomer = await _db.Customers.FirstOrDefaultAsync(x => x.ReferenceId == rootCustomer.CustomerReference.Trim());
            if (dbCustomer == null)
                return StatusCode(404, new { message = $"Sold to party '{rootCustomer.CustomerReference.Trim()}' doesn't exist in DB."});
            if(_db.RootCustomers.Any(x => x.DefaultCustomerId == dbCustomer.Id))
                return StatusCode(500, new { message = $"'{rootCustomer.CustomerReference.Trim()}' is already set as Sold to party in DB." });
            var newRootCustomer = new RootCustomer(rootCustomer.Name);
            newRootCustomer.SetDefaultCustomer(dbCustomer);
            await _db.SaveChangesAsync();
            return StatusCode(200, new { id = newRootCustomer.Id, message = "Customer has been added." });
        }
    }

    public static class CustomerFilterExt
    {
        public static IQueryable<RootCustomer> ApplyPager(this IQueryable<RootCustomer> query, PagerParameters pager)
        {
            return query
                .Skip(pager.PageSize * (pager.CurrentPageNumber - 1))
                .Take(pager.PageSize);
        }

        public static IOrderedQueryable<RootCustomer> ApplyOrder(this IQueryable<RootCustomer> query, SortParameters sort)
        {
            switch (sort.SortField)
            {
                case RootCustomersViewModel.NameSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.Name)
                        : query.OrderByDescending(b => b.Name);
                default:
                    return query.OrderBy(b => b.Name);
            }
        }
    }
}