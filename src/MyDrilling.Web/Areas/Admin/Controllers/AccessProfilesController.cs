﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core;
using MyDrilling.Core.Entities.Permission;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Web.Areas.Admin.Models.AccessProfile;
using MyDrilling.Web.Models;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;
using static MyDrilling.Core.Entities.Permission.RoleInfo;

namespace MyDrilling.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Route("/admin/accessprofiles")]
    public class AccessProfilesController : Controller
    {
        private readonly MyDrillingDb _db;
        public AccessProfilesController(MyDrillingDb db)
        {
            _db = db;
        }

        [HttpGet("")]
        [HttpGet("index")]
        public async Task<IActionResult> Index()
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin() && !roles.IsUserAdmin())
                return RedirectToAction("NoAccess", "Home");
            var model = new AccessProfilesViewModel();
            var pager = HttpContext.GetPagerParameters();
            var order = HttpContext.GetOrderParameters();
            var accessProfiles = _db.Permission_Profiles.Include(x => x.ProfileRoles);
            model.TotalCount = accessProfiles.Count();
            var accessProfilesForPage = accessProfiles.ApplyOrder(order).ApplyPager(pager).ToArray().Select(x => new AccessProfileItemViewModel
            {
                Id = x.Id,
                Name = x.Name,
                ProfileRoles = x.ProfileRoles.Select(y => new AccessProfileRoleViewModel
                {
                    Id = y.Id,
                    Role = y.Role,
                    RoleDescription = $"{ByRole[y.Role].Area} - {ByRole[y.Role].Description}"
                }).OrderBy(y => y.RoleDescription).ToArray()
            }).ToArray();

            model.Items = accessProfilesForPage;
            return View("Index", model);
        }

        [HttpGet("detail/{id}")]
        public async Task<IActionResult> Detail(long id)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin() && !roles.IsUserAdmin())
                return RedirectToAction("NoAccess", "Home");
            if (id <= 0)
                return RedirectToAction("ContentNotFound", "Home");
            var accessProfile = await _db.Permission_Profiles.FirstOrDefaultAsync(x => x.Id == id);
            if (accessProfile == null)
                return RedirectToAction("ContentNotFound", "Home");
            var numOfUsers = await _db.Permission_UserProfiles.Where(y => y.ProfileId == accessProfile.Id).Select(x => x.UserId).Distinct().CountAsync();
            var model = new AccessProfileViewModel
            {
                Id = accessProfile.Id,
                Name = accessProfile.Name,
                NumberOfUsers = numOfUsers,
                ProfileRoleGroups = GetAvailableClaimGroups()
            };

            var accessProfileRoles = await _db.Permission_ProfileRoles.Where(x => x.ProfileId == id).ToArrayAsync();
            if (accessProfileRoles.Any())
            {
                //map selected claims to 
                var allRoles = model.ProfileRoleGroups.SelectMany(x => x.ProfileRoles).ToArray();
                foreach (var profileRole in accessProfile.ProfileRoles)
                {
                    var mappedRole = allRoles.FirstOrDefault(x => x.OptionValue == ((int)profileRole.Role).ToString());
                    if (mappedRole != null)
                        mappedRole.Selected = true;
                }
            }
            return View(model);
        }

        [HttpGet("usersforaccessprofile")]
        public IActionResult UsersForAccessProfile(long id)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin())
                return StatusCode(401, "No access");
            var users = _db.Permission_UserProfiles.Where(x => x.ProfileId == id).Select(x => new AccessProfileUserViewModel()
            {
                UserId = x.UserId,
                User = x.User.Upn,
                RigId = x.RigId,
                RigName = x.Rig.Name.ToCamelCaseWithRomanNumerals()
            }).ToArray().GroupBy(x => x.User).Select(x => new {User = x.Key, UserId = x.First().UserId });
            //}).ToArray().GroupBy(x => x.User).Select(x => new {User = x.Key, UserId = x.First().UserId, Rigs = x.Select(y => y.RigName).ToArray()});
            return StatusCode(200, new {users});
        }


        [HttpGet("newaccessprofile")]
        public IActionResult NewAccessProfile()
        {
            var model = new AccessProfileViewModel
            {
                Name = "New access profile",
                ProfileRoleGroups = GetAvailableClaimGroups()
            };
            return View("Detail", model);
        }
        
        [HttpPost("saveaccessprofile")]
        public async Task<IActionResult> SaveAccessProfile(AccessProfileViewModel accessProfile)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin())
                return RedirectToAction("NoAccess", "Home");
            if (accessProfile == null || accessProfile.Id <= 0)
                return RedirectToAction("ContentNotFound", "Home");
            var dbAccessProfile = await _db.Permission_Profiles.FirstOrDefaultAsync(x => x.Id == accessProfile.Id);
            if (dbAccessProfile == null)
                return RedirectToAction("ContentNotFound", "Home");

            if (accessProfile.Name != dbAccessProfile.Name)
                dbAccessProfile.Name = accessProfile.Name;
            var allSelectedRoles = accessProfile.ProfileRoleGroups.SelectMany(x => x.ProfileRoles).Where(x => x.Selected).ToArray();
            var accessProfileRoles = await _db.Permission_ProfileRoles.Where(x => x.ProfileId == accessProfile.Id).ToArrayAsync();
       
            //get deleted roles
            var deletedRoles = accessProfileRoles
                .Where(s => allSelectedRoles.All(x => (int)s.Role != int.Parse(x.OptionValue)))
                .Select(x => x);
            //delete them
            foreach (var deleted in deletedRoles)
            {
                var dbItem = await _db.Permission_ProfileRoles.FirstOrDefaultAsync(x => x.Id == deleted.Id);
                _db.Permission_ProfileRoles.Remove(dbItem);
            }
            //get new roles 
            var newProfileRoles = allSelectedRoles
                .Where(x => accessProfileRoles.All(s => (int)s.Role != int.Parse(x.OptionValue)))
                .Select(x => x);
            //add them
            foreach (var newRole in newProfileRoles)
            {
                _db.Permission_ProfileRoles.Add(new ProfileRole (dbAccessProfile, (RoleInfo.Role)int.Parse(newRole.OptionValue)));
            }
            await _db.SaveChangesAsync();

            return RedirectToAction("Detail", "AccessProfiles", new {id = accessProfile.Id});
        }

        [HttpPost("deleteaccessprofile")]
        public async Task<IActionResult> DeleteAccessProfile(long id)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.IsSystemAdmin())
                return RedirectToAction("NoAccess", "Home");
            if (id <= 0)
                return RedirectToAction("ContentNotFound", "Home");
            var dbAccessProfile = await _db.Permission_Profiles.FirstOrDefaultAsync(x => x.Id == id);
            if(dbAccessProfile == null)
                return RedirectToAction("ContentNotFound", "Home");
            //delete UserProfiles
            var userProfiles = _db.Permission_UserProfiles.Where(x => x.ProfileId == id);
            foreach (var userProf in userProfiles)
            {
                _db.Permission_UserProfiles.Remove(userProf);
            }
            //delete ProfileRoles
            var profileRoles = _db.Permission_ProfileRoles.Where(x => x.ProfileId == id);
            foreach (var profRole in profileRoles)
            {
                _db.Permission_ProfileRoles.Remove(profRole);
            }
            //delete Profile
            _db.Permission_Profiles.Remove(dbAccessProfile);
            await _db.SaveChangesAsync();
            return RedirectToAction("Index", "AccessProfiles");
        }

        private List<ProfileRoleGroup> GetAvailableClaimGroups()
        {
            var list = new List<ProfileRoleGroup>
            {
                new ProfileRoleGroup 
                {
                    GroupName = "Rig Information",
                    ProfileRoles = new List<CheckboxOption>
                    {
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.MyDrillingViewer).ToString(),
                            OptionLabel = RoleDescriptions.MyDrillingViewer
                        }
                    }
                },
                new ProfileRoleGroup
                {
                    GroupName = "Bulletin and alerts",
                    ProfileRoles = new List<CheckboxOption>
                    {
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.BulletinViewer).ToString(),
                            OptionLabel = RoleDescriptions.BulletinViewer
                        }
                    }
                },
                new ProfileRoleGroup
                {
                    GroupName = "Documentation",
                    ProfileRoles = new List<CheckboxOption>
                    {
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.DocumentationViewer).ToString(),
                            OptionLabel = RoleDescriptions.DocumentationViewer
                        }
                    }
                },
                new ProfileRoleGroup
                {
                    GroupName = "Sparepart",
                    ProfileRoles = new List<CheckboxOption>
                    {
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.SparePartViewer).ToString(),
                            OptionLabel = RoleDescriptions.SparePartViewer
                        }
                    }
                },
                new ProfileRoleGroup
                {
                    GroupName = "Enquiry handling",
                    ProfileRoles = new List<CheckboxOption>
                    {
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.EnquiryHandlingApprover).ToString(),
                            OptionLabel = RoleDescriptions.EnquiryHandlingApprover
                        },
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.EnquiryHandlingInitiator).ToString(),
                            OptionLabel = RoleDescriptions.EnquiryHandlingInitiator
                        },
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.EnquiryHandlingPreparationViewer).ToString(),
                            OptionLabel = RoleDescriptions.EnquiryHandlingPreparationViewer
                        },
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.EnquiryHandlingApprovedViewer).ToString(),
                            OptionLabel = RoleDescriptions.EnquiryHandlingApprovedViewer
                        },
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.EnquiryHandlingProvider).ToString(),
                            OptionLabel = RoleDescriptions.EnquiryHandlingProvider
                        },
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.EnquiryHandlingCollaborator).ToString(),
                            OptionLabel = RoleDescriptions.EnquiryHandlingCollaborator
                        },
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.EnquiryHandlingCustomerAssigner).ToString(),
                            OptionLabel = RoleDescriptions.EnquiryHandlingCustomerAssigner
                        },
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.EnquiryHandlingProviderAssigner).ToString(),
                            OptionLabel = RoleDescriptions.EnquiryHandlingProviderAssigner
                        },
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.EnquiryHandlingNotifier).ToString(),
                            OptionLabel = RoleDescriptions.EnquiryHandlingNotifier
                        }
                    }
                },
                new ProfileRoleGroup
                {
                    GroupName = "RiCon™",
                    ProfileRoles = new List<CheckboxOption>
                    {
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.CbmViewer).ToString(),
                            OptionLabel = RoleDescriptions.CbmViewer
                        }
                    }
                },
                new ProfileRoleGroup
                {
                    GroupName = "Upgrade Matrix",
                    ProfileRoles = new List<CheckboxOption>
                    {
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.UpgradeMatrixViewer).ToString(),
                            OptionLabel = RoleDescriptions.UpgradeMatrixViewer
                        },
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.UpgradeMatrixEditor).ToString(),
                            OptionLabel = RoleDescriptions.UpgradeMatrixEditor
                        },
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.UpgradeMatrixAdmin).ToString(),
                            OptionLabel = RoleDescriptions.UpgradeMatrixAdmin
                        }
                    }
                },
                new ProfileRoleGroup
                {
                    GroupName = "DrillCon",
                    ProfileRoles = new List<CheckboxOption>
                    {
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.DrillConViewer).ToString(),
                            OptionLabel = RoleDescriptions.DrillConViewer
                        }
                    }
                },
                new ProfileRoleGroup
                {
                    GroupName = "DRILLPerform™",
                    ProfileRoles = new List<CheckboxOption>
                    {
                        new CheckboxOption
                        {
                            OptionValue = ((int)Role.SmartModuleViewer).ToString(),
                            OptionLabel = RoleDescriptions.SmartModuleViewer
                        }
                    }
                }
            };
 
            return list;
        }
    }

    public static class AccessProfileFilterExt
    {
        public static IQueryable<Profile> ApplyPager(this IQueryable<Profile> query, PagerParameters pager)
        {
            return query
                .Skip(pager.PageSize * (pager.CurrentPageNumber - 1))
                .Take(pager.PageSize);
        }

        public static IOrderedQueryable<Profile> ApplyOrder(this IQueryable<Profile> query, SortParameters sort)
        {
            switch (sort.SortField)
            {
                case AccessProfilesViewModel.NameSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.Name)
                        : query.OrderByDescending(b => b.Name);
                default:
                    return query.OrderBy(b => b.Name);
            }
        }
    }
}