﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Core.Entities.Permission;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Web.Areas.Admin.Controllers;
using MyDrilling.Web.Areas.Admin.Models.User;
using MyDrilling.Web.QueryParameters;

namespace MyDrilling.Web.Areas.Admin.ViewComponents
{
    [ViewComponent(Name = "UserAccessToCustomers")]
    public class UserAccessToCustomersViewComponent : ViewComponent
    {
        private readonly ILogger<UserAccessToCustomersViewComponent> _logger;
        private readonly MyDrillingDb _db;


        public UserAccessToCustomersViewComponent(ILogger<UserAccessToCustomersViewComponent> logger, MyDrillingDb db)
        {
            _logger = logger;
            _db = db;
        }

        public async Task<IViewComponentResult> InvokeAsync(long userId)
        {
            var pager = HttpContext.GetPagerParameters();
            var order = HttpContext.GetOrderParameters();
            var accessToCustomers = _db.Permission_UserRoles
                .Where(x => x.Role == RoleInfo.Role.CustomerAccess && x.UserId == userId)
                .Select(x => new UserAccessToCustomer
                {
                    UserRoleId = x.Id,
                    CustomerId = (long) x.RootCustomerId,
                    CustomerName = x.RootCustomer.Name

                });
            var totalCount = await accessToCustomers.CountAsync();    
            var accessToCustomerPaged = await accessToCustomers
                .ApplyOrder(order)
                .ApplyPager(pager)
                .ToArrayAsync();
            var model = new UserAccessToCustomerViewModel
            {
                TotalCount = totalCount,
                UserAccessToCustomers = accessToCustomerPaged,
                UserId = userId
            };
            return View("UserAccessToCustomers", model);
        }
    }
}
