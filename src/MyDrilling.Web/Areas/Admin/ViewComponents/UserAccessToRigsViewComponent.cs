﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Web.Areas.Admin.Controllers;
using MyDrilling.Web.Areas.Admin.Models.User;
using MyDrilling.Web.QueryParameters;

namespace MyDrilling.Web.Areas.Admin.ViewComponents
{
    public class UserAccessToRigsViewComponent : ViewComponent
    {
        private readonly ILogger<UserAccessToRigsViewComponent> _logger;
        private readonly MyDrillingDb _db;


        public UserAccessToRigsViewComponent(ILogger<UserAccessToRigsViewComponent> logger, MyDrillingDb db)
        {
            _logger = logger;
            _db = db;
        }

        public async Task<IViewComponentResult> InvokeAsync(long userId)
        {
            var pager = HttpContext.GetPagerParameters();
            var order = HttpContext.GetOrderParameters();
            var accessToRigs =  _db.Permission_UserProfiles
                .Where(x => x.UserId == userId)
                .Select(x => new UserAccessToRig
                {
                    UserProfileId = x.Id,
                    RigId = x.RigId,
                    RigName = x.Rig.Name,
                    ProfileId = x.ProfileId,
                    ProfileName = x.Profile.Name
                });
            var totalCount = await accessToRigs.CountAsync();
            var accessToRigsPaged = await accessToRigs
                .ApplyOrder(order)
                .ApplyPager(pager)
                .ToArrayAsync();
            var model = new UserAccessToRigViewModel
            {
                TotalCount = totalCount,
                UserAccessToRigs = accessToRigsPaged,
                UserId = userId
            };
            return View("UserAccessToRigs", model);
        }
    }
}
