﻿
namespace MyDrilling.Web.Areas.Admin.Models.Customer
{
    public class RootCustomersViewModel
    {
        public const string NameSort = "NameSort";
        public int TotalCount { get; set; }
        public RootCustomerViewModel[] Items { get; set; }
    }
}
