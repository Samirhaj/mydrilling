﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MyDrilling.Web.Areas.Admin.Models.Customer
{
    public class AddRootCustomerViewModel
    {
        public long RootCustomerId { get; set; }
        public string Name { get; set; }
        public string CustomerReference { get; set; }
    }
}
