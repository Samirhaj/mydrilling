﻿
namespace MyDrilling.Web.Areas.Admin.Models.Customer
{
    public class RootCustomerViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
