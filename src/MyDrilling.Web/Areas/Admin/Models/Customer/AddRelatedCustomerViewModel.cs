﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MyDrilling.Web.Areas.Admin.Models.Customer
{
    public class AddRelatedCustomerViewModel
    {
        public string CustomerReference { get; set; }
        public long RootCustomerId { get; set; }
        public bool IsDefault { get; set; }
    }
}
