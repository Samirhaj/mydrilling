﻿
namespace MyDrilling.Web.Areas.Admin.Models.Customer
{
    public class RootCustomerDetailViewModel : RootCustomerViewModel
    {
        public long? DefaultCustomerId { get; set; }
        public string DefaultCustomerName { get; set; }
        public CustomerViewModel[] RelatedCustomers { get; set; }
    }

    public class CustomerViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string ReferenceId { get; set; }
        public bool IsDefault { get; set; }
    }
}
