﻿
namespace MyDrilling.Web.Areas.Admin.Models.User
{
    public class UserAccessToCustomerViewModel
    {
        public const string NameSort = "NameSort";
        public long UserId { get; set; }
        public int TotalCount { get; set; }
        public UserAccessToCustomer[] UserAccessToCustomers { get; set; }
    }
}
