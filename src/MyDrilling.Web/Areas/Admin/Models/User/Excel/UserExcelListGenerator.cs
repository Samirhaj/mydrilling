﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using MyDrilling.Infrastructure.Excel;

namespace MyDrilling.Web.Areas.Admin.Models.User.Excel
{
    public class UserExcelListGenerator : ExcelListGeneratorBase
    {
        public Stream CreateExcelDocument(IEnumerable<UserViewModel> entities)
        {
            var columns = GetColumns();
            var rows = entities.Select(MapEntityToRowData);
            var stream = CreateExcelDocumentStream(columns, rows, "Users");
            return stream;
        }

        private IEnumerable<ExcelColumnInfo> GetColumns()
        {
            return new List<ExcelColumnInfo>
            {
                new ExcelColumnInfo {Title = "Upn", Width = 30},
                new ExcelColumnInfo {Title = "First Name", Width = 30},
                new ExcelColumnInfo {Title = "Last Name", Width = 30},
                new ExcelColumnInfo {Title = "Is Active", Width = 50},
                new ExcelColumnInfo {Title = "Last Login", Width = 30}
            };
        }

        private IEnumerable<string> MapEntityToRowData(UserViewModel entity)
        {
            return new List<string>
            {
                entity.Upn,
                entity.FirstName,
                entity.LastName,
                entity.IsActive,
                entity.LastLogin
            };
        }
    }
}
