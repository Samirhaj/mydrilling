﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MyDrilling.Web.Areas.Admin.Models.User
{
    public class AddAccessToCustomerViewModel
    {
        public long UserId { get; set; }
        public long RootCustomerId { get; set; }
        public List<SelectListItem> AvailableRootCustomers { get; set; }
    }
}
