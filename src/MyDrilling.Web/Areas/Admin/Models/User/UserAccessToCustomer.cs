﻿
namespace MyDrilling.Web.Areas.Admin.Models.User
{
    public class UserAccessToCustomer
    {
        public long UserRoleId { get; set; }
        public long CustomerId { get; set; }
        public string CustomerName { get; set; }
    }
}
