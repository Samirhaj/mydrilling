﻿using System.ComponentModel.DataAnnotations;

namespace MyDrilling.Web.Areas.Admin.Models.User
{
    public class UserViewModel
    {
        public long Id { get; set; }
        public string Upn { get; set; }
        
        [Display(Name = "First name")]
        public string FirstName { get; set; }
        
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Display(Name = "Is active")]
        public string IsActive { get; set; }

        [Display(Name = "Last login")]
        public string LastLogin { get; set; }
    }
}
