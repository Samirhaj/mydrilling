﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MyDrilling.Web.Areas.Admin.Models.User
{
    public class UsersViewModel
    {
        public const string UpnSort = "UpnSort";
        public const string FirstNameSort = "FirstNameSort";
        public const string LastNameSort = "LastNameSort";
        public const string IsActiveSort = "IsActiveSort";
        public const string LastLoginSort = "LastLoginSort";

        public long RootCustomerId { get; set; }
        public List<SelectListItem> AvailableRootCustomers { get; set; }
        public bool ShowAll { get; set; }
        public int TotalCount { get; set; }
        public UserViewModel[] Users { get; set; }
    }
}
