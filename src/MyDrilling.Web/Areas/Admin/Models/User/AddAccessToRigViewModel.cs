﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MyDrilling.Web.Areas.Admin.Models.User
{
    public class AddAccessToRigViewModel
    {
        public long UserId { get; set; }
        public long RigId { get; set; }
        public List<SelectListItem> AvailableRigs { get; set; }
        public long ProfileId { get; set; }
        public List<SelectListItem> AvailableAccessProfiles { get; set; }
    }
}
