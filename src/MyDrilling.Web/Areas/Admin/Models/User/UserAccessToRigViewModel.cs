﻿
namespace MyDrilling.Web.Areas.Admin.Models.User
{
    public class UserAccessToRigViewModel
    {
        public const string RigNameSort = "RigNameSort";
        public const string ProfileNameSort = "ProfileNameSort";

        public long UserId { get; set; }
        public int TotalCount { get; set; }
        public UserAccessToRig[] UserAccessToRigs { get; set; }
    }
}
