﻿
namespace MyDrilling.Web.Areas.Admin.Models.User
{
    public class UserAccessToRig
    {
        public long UserProfileId { get; set; }
        public long RigId { get; set; }
        public string RigName { get; set; }
        public string CustomerName { get; set; }
        public long ProfileId { get; set; }
        public string ProfileName { get; set; }
    }
}
