﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace MyDrilling.Web.Areas.Admin.Models.Facility
{
    public class FacilityDetailViewModel : FacilityViewModel
    {
        public long ReferenceId { get; set; }
        public long? OwnerId { get; set; }
        [Display(Name = "Owner referenceId")]
        public string OwnerReferenceId { get; set; }

        public long? RootOwnerId { get; set; }
        public string RootOwnerName { get; set; }
        public long? RootOperatorId { get; set; }
        public string RootOperatorName { get; set; }


        public ServiceAccountManager Sam { get; set; }
    }

    public class ServiceAccountManager
    {
        public long Id { get; set; }
        [Display(Name = "Name")]
        public string DisplayName { get; set; }
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Mobile number")]
        public string MobileNumber { get; set; }
        [Display(Name = "Email address")]
        public string EmailAddress { get; set; }
        public string ImageUrl { get; set; }
    }
}
