﻿
namespace MyDrilling.Web.Areas.Admin.Models.Facility
{
    public class FacilitiesViewModel
    {
        public const string NameSort = "NameSort";
        public const string OwnerSort = "OwnerSort";
        public const string RigTypeSort = "RigTypeSort";
        public const string SectorSort = "SectorSort";
        public int TotalCount { get; set; }
        public FacilityViewModel[] Items { get; set; }
        public string[] StartLetters { get; set; }
        public string StartsWith { get; set; }
    }
}
