﻿
using System.ComponentModel.DataAnnotations;

namespace MyDrilling.Web.Areas.Admin.Models.Facility
{
    public class FacilityViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        [Display(Name = "Owner name")]
        public string OwnerName { get; set; }
        public string RigType { get; set; }
        public string Sector { get; set; }
        
    }
}
