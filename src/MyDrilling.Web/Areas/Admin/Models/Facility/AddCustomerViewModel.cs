﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace MyDrilling.Web.Areas.Admin.Models.Facility
{
    public class AddCustomerViewModel
    {
        public long FacilityId { get; set; }
        public long? RootOwnerId { get; set; }
        public long? RootOperatorId { get; set; }
        public bool IsOwner { get; set; }
        public List<SelectListItem> AvailableRootCustomers { get; set; }
    }
}
