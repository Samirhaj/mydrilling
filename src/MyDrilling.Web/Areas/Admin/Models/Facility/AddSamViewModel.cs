﻿
namespace MyDrilling.Web.Areas.Admin.Models.Facility
{
    public class AddSamViewModel
    {
        public long FacilityId { get; set; }
        public long UserId { get; set; }
    }
}
