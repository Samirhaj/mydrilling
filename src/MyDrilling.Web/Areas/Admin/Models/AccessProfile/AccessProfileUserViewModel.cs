﻿
namespace MyDrilling.Web.Areas.Admin.Models.AccessProfile
{
    public class AccessProfileUserViewModel
    {
        public long ProfileId { get; set; }
        public string ProfileName { get; set; }
        public long UserId { get; set; }
        public string User { get; set; }
        public long RigId { get; set; }
        public string RigName { get; set; }

    }
}
