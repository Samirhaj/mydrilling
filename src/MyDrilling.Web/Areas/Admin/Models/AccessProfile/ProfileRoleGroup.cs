﻿using System.Collections.Generic;
using MyDrilling.Web.Models;

namespace MyDrilling.Web.Areas.Admin.Models.AccessProfile
{
    public class ProfileRoleGroup 
    {
        public string GroupName { get; set; }
        public List<CheckboxOption> ProfileRoles { get; set; }
    }
}
