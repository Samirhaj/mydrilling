﻿using MyDrilling.Core.Entities.Permission;

namespace MyDrilling.Web.Areas.Admin.Models.AccessProfile
{
    public class AccessProfilesViewModel
    {
        public const string NameSort = "NameSort";
        public AccessProfileItemViewModel[] Items { get; set; }
        public int TotalCount { get; set; }
    }


    public class AccessProfileItemViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public AccessProfileRoleViewModel[] ProfileRoles { get; set; }
    }

    public class AccessProfileRoleViewModel
    {
        public long Id { get; set; }
        public RoleInfo.Role Role { get; set; }
        public string RoleDescription { get;set;}
    }

}
