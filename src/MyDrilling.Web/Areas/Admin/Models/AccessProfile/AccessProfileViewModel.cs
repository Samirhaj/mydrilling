﻿using System.Collections.Generic;

namespace MyDrilling.Web.Areas.Admin.Models.AccessProfile
{
    public class AccessProfileViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int NumberOfUsers { get; set; }
        public List<ProfileRoleGroup> ProfileRoleGroups { get; set; }
    }
}
