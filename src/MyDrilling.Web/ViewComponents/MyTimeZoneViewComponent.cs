﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MyDrilling.Web.Models.Userprofile;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.ViewComponents
{
    public class MyTimeZoneViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            var user = HttpContext.GetCurrentUser();
            var model = new TimeZoneViewModel();
            model.SelectedTimeZone = user.GetTimeZoneInfo().Id;
            return View("MyTimeZone", model);
        }
    }
}
