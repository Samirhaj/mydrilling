﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Web.Controllers;
using MyDrilling.Web.Models.Task;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.ViewComponents
{
    [ViewComponent(Name = "TaskList")]
    public class TaskListComponent : ViewComponent
    {
        private readonly ILogger<TaskListComponent> _logger;
        private readonly MyDrillingDb _db;
        private readonly string[] taskTypeValues = { TaskInfoExt.MyTaskLabel, TaskInfoExt.RigTaskLabel };
        private readonly string[] taskActionValues = { TaskInfoExt.ApproveActionLabel, TaskInfoExt.WaitActionLabel };

        public TaskListComponent(ILogger<TaskListComponent> logger, MyDrillingDb db)
        {
            _logger = logger;
            _db = db;
        }

        public async Task<IViewComponentResult> InvokeAsync(bool isOverviewPage, bool onlyNotified)
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var drillDown = HttpContext.GetDrillDownParameters();
            var pager = HttpContext.GetPagerParameters();
            var order = HttpContext.GetOrderParameters();
            var filter = HttpContext.GetFilterParameters();
            
            var availableEnquiries = await _db.Enquiry_Enquiries
                .Include(x => x.CreatedBy)
                .Include(x => x.Rig)
                .ApplyPermissions(roles)
                .ApplyDrillDown(drillDown)
                .ExcludeInternal()
                .ExcludeSoftDeleted()
                .WithLogicalState(null)
                .ApplyTaskFilter(filter, user)
                .Where(x => !onlyNotified || x.AddedUsers.Any(y => y.UserId == user.Id))
                .ToArrayAsync();
            if (!onlyNotified)
            {
                var enqApproverOnRigs = roles.GetEnquiryHandlingApproverRigs();
                var enqProviderOnRigs = roles.GetEnquiryHandlingProviderRigs();
                var accessToCustomers = roles.GetAccessToRootCustomers();

                if (accessToCustomers.Length > 0 && (enqApproverOnRigs.Length > 0 || enqProviderOnRigs.Length > 0))
                {
                    availableEnquiries = availableEnquiries
                        .Where(x =>
                            (!x.AssignedToId.HasValue || x.AssignedToId.Value == user.Id)
                            && (
                                ((x.State == StateInfo.State.ReadyForApproval || x.State == StateInfo.State.AssignedCustomer) && enqApproverOnRigs.Contains(x.RigId))
                                ||
                                (x.State == StateInfo.State.AssignedProvider && enqProviderOnRigs.Contains(x.RigId))
                            )
                        ).ToArray();
                }
            }

            var myTasksTotalCount = availableEnquiries.Count();
            var myTasks = availableEnquiries
                .ApplyTaskOrder(order)
                .ApplyTaskPager(pager)
                .Select(x => x.State == StateInfo.State.Draft
                    ? x.MapDraftToTaskItem(user.Id)
                    : x.MapEnquiryToTaskItem(user.Id)).ToArray();

            var taskListModel = new TaskListViewModel
            {
                Tasks = isOverviewPage ? myTasks.Take(5).ToArray() : myTasks,
                TotalCount = myTasksTotalCount,
                IsOverviewPage = isOverviewPage
            };
            AddFilterData();
            return View(taskListModel);
        }

        private void AddFilterData()
        {
            ViewData[TaskListViewModel.TaskTypeFilter] = taskTypeValues
                .Select(x => new SelectListItem(x, x))
                .OrderBy(x => x.Text)
                .ToArray();

            ViewData[TaskListViewModel.TaskActionFilter] = taskActionValues
                .Select(x => new SelectListItem(x, x))
                .OrderBy(x => x.Text)
                .ToArray();

            ViewData[TaskListViewModel.CategoryFilter] = TypeInfo.Types
                .Select(x => x.Category).Distinct()
                .Select(x => new SelectListItem(x, x))
                .OrderBy(x => x.Text)
                .ToArray();
        }
    }
}
