﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Core.Entities.Bulletin;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Web.Controllers;
using MyDrilling.Web.Models.Rig;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.ViewComponents
{
    [ViewComponent(Name = "LastActivities")]
    public class LastActivitiesComponent : ViewComponent
    {
        private readonly ILogger<LastActivitiesComponent> _logger;
        private readonly MyDrillingDb _db;

        public LastActivitiesComponent(ILogger<LastActivitiesComponent> logger, MyDrillingDb db)
        {
            _logger = logger;
            _db = db;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var drillDown = HttpContext.GetDrillDownParameters();
            var roles = HttpContext.GetCurrentUserRoles();
            var pager = HttpContext.GetPagerParameters();

            var availableEnquiries = await _db.Enquiry_Enquiries
                .Include(x => x.CreatedBy)
                .Include(x => x.Rig)
                .ApplyPermissions(roles)
                .ApplyDrillDown(drillDown)
                .ExcludeInternal()
                .ExcludeSoftDeleted()
                .ToArrayAsync();
            ActivityViewModel[] lastEnquiryActivities = availableEnquiries.OrderByDescending(x => x.LastActivity).Select(x => new ActivityViewModel
            {
                EntityId = x.Id,
                EntityReferenceId = x.ReferenceId,
                ActivityEntityType = ActivityEntityType.Enquiry,
                ActivityType = x.State == StateInfo.State.Closed ? ActivityType.Closed : (x.Created == x.LastActivity ? ActivityType.Created : ActivityType.Comment),
                CreatorName = x.CreatedBy != null ? $"{x.CreatedBy.FirstName} {x.CreatedBy.LastName}" : "",
                RigId = x.RigId,
                RigName = x.Rig.Name,
                Date = x.Modified,
                EntityState = x.State == StateInfo.State.Draft ? ActivityViewModelExt.DraftText : ActivityViewModelExt.NewText,
                Title = x.Title,
                Url = "/enquiries/detail/" + x.Id
            }).ToArray();
            var zbBulletins = _db.Bulletin_ZbBulletins
                .ApplyPermissions(HttpContext.GetCurrentUserRoles())
                .ApplyDrillDown(drillDown)
                .GroupBy(b => new { b.ZaBulletinId, RigId = b.RigId.Value, RigName = b.Rig.Name })
                .Select(b => new { b.Key.ZaBulletinId, b.Key.RigId, b.Key.RigName });

            var zaBulletins = _db.Bulletin_ZaBulletins;
            var lastBulletins = zbBulletins
                .Join(zaBulletins, zb => zb.ZaBulletinId, za => za.Id, (zb, za) => new
                {
                    ZaId = za.Id,
                    zb.RigId,
                    za.TypeId,
                    za.Type,
                    za.IssueDate,
                    za.Description,
                    za.Title,
                    zb.RigName
                });
            ActivityViewModel[] lastBulletinActivities = lastBulletins
                .OrderByDescending(x => x.IssueDate).Select(x => new ActivityViewModel
                {
                    EntityId = x.ZaId,
                    //EntityReferenceId = x.ReferenceId,
                    ActivityEntityType = ActivityEntityType.Bulletin,
                    ActivityType = ActivityType.Created,
                    CreatorName = LanguageConstants.Provider,
                    RigName = x.RigName,
                    Date = x.IssueDate,
                    Title = x.Title,
                    Url = $"/bulletins/detail?id={x.ZaId}&rigId={x.RigId}"
                }).ToArray();
            ActivityViewModel[] lastActivities =
                lastBulletinActivities
                    .Concat(lastEnquiryActivities)
                    .OrderByDescending(x => x.Date)
                    .ApplyPager(pager)
                    .ToArray();
            var lastActivitiesModel = new ActivitiesViewModel
            {
                Activities = lastActivities,
                ActivityPageNumber = pager.CurrentPageNumber
            };
            return View(lastActivitiesModel);
        }



    }
}
