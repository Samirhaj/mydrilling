﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Core;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Core.Entities.Rig;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Web.Controllers;
using MyDrilling.Web.Models.Rig;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.ViewComponents
{
    [ViewComponent(Name = "RigsOverview")]
    public class RigsOverviewComponent : ViewComponent
    {
        private readonly ILogger<RigsOverviewComponent> _logger;
        private readonly MyDrillingDb _db;

        public RigsOverviewComponent(ILogger<RigsOverviewComponent> logger, MyDrillingDb db)
        {
            _logger = logger;
            _db = db;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var roles = HttpContext.GetCurrentUserRoles();
            var pager = HttpContext.GetPagerParameters();

            var rigs = _db.Rigs
                .Include(x => x.RootOwner)
                .ApplyPermission(roles)
                .OrderBy(x => x.Name);
            StateInfo[] openStates =
            {
                StateInfo.ByState[StateInfo.State.AssignedProvider],
                StateInfo.ByState[StateInfo.State.AssignedCustomer],
                StateInfo.ByState[StateInfo.State.SuggestedClosed]
            };
            var rigsOnDowntimeEnquiries = await _db.Enquiry_Enquiries
                .Include(x => x.CreatedBy)
                .ApplyPermissions(roles)
                .ExcludeInternal()
                .ExcludeSoftDeleted()
                .WithLogicalState(null)
                .Where(x => x.Type == TypeInfo.Type.Support_RigOnDowntime
                            && (x.State == StateInfo.State.AssignedCustomer
                              || x.State == StateInfo.State.AssignedProvider
                              || x.State == StateInfo.State.SuggestedClosed))
                .Select(x => x.RigId).ToArrayAsync();
            var totalCount = rigs.Count();
            var rigsToShow = rigs.ApplyPager(pager).ToArray();
            var rigsOverview = new RigsViewModel
            {
                RigsTotalCount = totalCount,
                Rigs = rigsToShow.Select(x => new RigViewModel
                {
                    Id = x.Id,
                    ReferenceId = x.ReferenceId,
                    Name = x.NameFormatted,
                    Owner = x.RootOwner?.Name.ToCamelCase(),
                    Type = x.TypeCode.GetRigTypeInfo(),
                    Sector = x.Sector,
                    RigOnDowntime = rigsOnDowntimeEnquiries.Contains(x.Id)
                }).ToArray()
            };
            return View(rigsOverview);
        }
    }
}
