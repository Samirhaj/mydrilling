﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Core.Entities.Equipment;
using MyDrilling.Core.Entities.Rig;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Web.Controllers;
using MyDrilling.Web.Models.Menu;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.ViewComponents
{
    [ViewComponent(Name = "DrillDownMenu")]
    public class DrillDownMenuComponent : ViewComponent
    {
        private readonly ILogger<DrillDownMenuComponent> _logger;
        private readonly MyDrillingDb _db;

        public DrillDownMenuComponent(ILogger<DrillDownMenuComponent> logger, MyDrillingDb db)
        {
            _logger = logger;
            _db = db;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var roles = HttpContext.GetCurrentUserRoles();
            var drillDown = HttpContext.GetDrillDownParameters();
            var rigs = await _db.Rigs
                .ApplyPermission(roles)
                .ToArrayAsync();
            var selectedRig = drillDown.HasRig ? rigs.FirstOrDefault(x => x.Id == drillDown.RigId) : null;
            string selectedRigName = string.Empty;
            if (selectedRig != null)
            {
                selectedRigName = selectedRig.NameFormatted;
            }

            var equipments = await _db.Equipments
                .ApplyPermission(roles)
                .ApplyRigFilter(drillDown.RigId)
                .ExcludeProductless()
                .ToArrayAsync();
            var selectedProductName = string.Empty;
            var equipmentForProduct = new List<EquipmentMenuViewModel>();
            if (drillDown.HasProductCode)
            {
                var equipmentWithSelectedProduct = equipments.Where(x => x.ProductCode == drillDown.ProductCode).ToArray();
                if (equipmentWithSelectedProduct.Length > 0)
                {
                    selectedProductName = equipmentWithSelectedProduct.First().ProductNameFormatted;
                }

                equipmentForProduct = equipmentWithSelectedProduct.Select(x => new EquipmentMenuViewModel
                {
                    EquipmentName = x.ToFilterName(),
                    EquipmentReferenceId = x.ReferenceId,
                    RigReferenceId = x.RigReferenceId
                }).ToList();
            }

            var selectedEquipmentName = string.Empty;
            if (drillDown.HasEquipment)
            {
                var selEquipment = equipments.FirstOrDefault(x => x.Id == drillDown.EquipmentId);
                if (selEquipment != null)
                    selectedEquipmentName = selEquipment.ToFilterName();
            }

            var drillDownMenu = new DrillDownMenuViewModel
            {
                CurrentAction = ViewContext.RouteData.Values["Action"].ToString(),
                CurrentController = ViewContext.RouteData.Values["Controller"].ToString(),
                HasRig = drillDown.HasRig,
                HasProduct = drillDown.HasProductCode,
                HasEquipment = drillDown.HasEquipment,
                SelectedProduct = drillDown.ProductCode,
                SelectedProductName = selectedProductName,
                SelectedRig = selectedRigName,
                SelectedEquipment = selectedEquipmentName,
                TypedRigs = rigs.GroupBy(x => x.TypeCode.GetRigTypeInfo())
                    .ToDictionary(x => x.Key, x => x.Select(y => new RigMenuViewModel
                    {
                        RigName = y.NameFormatted,
                        RigReferenceId = y.ReferenceId
                    }).OrderBy(y => y.RigName)
                    .ToList()),

                EquipmentForSelectedProduct = equipmentForProduct
            };
            return View(drillDownMenu);
        }
    }
}
