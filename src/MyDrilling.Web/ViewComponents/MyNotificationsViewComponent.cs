﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Core.Entities.Rig;
using MyDrilling.Core.Entities.Subscription;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Web.Models.Userprofile;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.ViewComponents
{
    public class MyNotificationsViewComponent : ViewComponent
    {
        private readonly MyDrillingDb _db;

        public MyNotificationsViewComponent(MyDrillingDb db)
        {
            _db = db;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user = HttpContext.GetCurrentUser();
            
            var model = new MyNotificationsViewModel { NotificationOptions = AvailableNotificationOptions(user) };
            if (!model.NotificationOptions.Any())
                return View("MyNotifications", model);

            var dbUser = await _db.Users
                .Include(x => x.SubscriptionSettings)
                .Include(x => x.SubscriptionRigFilters)
                .Include(x => x.SubscriptionEnquiryTypeFilters)
                .FirstOrDefaultAsync(x => x.Id == user.Id);

            ApplyUserChoices(dbUser, model);

            return View("MyNotifications", model);
        }

        private List<NotificationOption> AvailableNotificationOptions(User user)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            var list = new List<NotificationOption>();
            bool putBorderSeparator = false;
            var enqProviderViewerRigs = roles.GetEnquiryHandlingProviderAssignerRigs();
            var enqApproverViewerRigs = roles.GetEnquiryHandlingApprovedViewerRigs();
            var hasApprovedViewerRigs = roles.HasEnquiryHandlingApprovedViewerRigs();
            if (hasApprovedViewerRigs)
            {
                putBorderSeparator = true;
                list.Add(new NotificationOption
                {
                    OptionLabel = "Rig reported on downtime",
                    OptionValue = ((int)SubscriptionType.EnquiryRigOnDowntimeCreated).ToString(),
                    NotifyMeType = NotifyMeType.CommonSubscription,
                    Selected = false
                });

                list.Add(new NotificationOption
                {
                    OptionLabel = "Rig move reported",
                    OptionValue = ((int)SubscriptionType.EnquiryRigMovedCreated).ToString(),
                    NotifyMeType = NotifyMeType.CommonSubscription,
                    Selected = false
                });
                list.Add(new NotificationOption
                {
                    OptionLabel = "New comment added",
                    OptionValue = ((int)SubscriptionType.EnquiryCommentAdded).ToString(),
                    NotifyMeType = NotifyMeType.CommonSubscription,
                    Selected = false,
                    Options = new List<NotificationOption>
                    {
                        new NotificationOption
                        {
                            OptionLabel = "Only from enquiries I'm involved in",
                            OptionValue = ((int)SubscriptionType.EnquiryIAmInvolvedInCommentAdded).ToString(),
                            NotifyMeType = NotifyMeType.CommonSubscription,
                            Selected = false
                        },
                        new NotificationOption
                        {
                            OptionLabel = "Only for rigs on downtime",
                            OptionValue = ((int)SubscriptionType.EnquiryRigOnDowntimeCommentAdded).ToString(),
                            NotifyMeType = NotifyMeType.CommonSubscription,
                            Selected = false
                        }
                    }
                });
            }

            if (enqProviderViewerRigs.Length > 0)
            {
                putBorderSeparator = true;
                list.Add(new NotificationOption
                {
                    OptionLabel = "Task assigned to my organization",
                    OptionValue = ((int)SubscriptionType.EnquiryAssignedToMyOrganization).ToString(),
                    NotifyMeType = NotifyMeType.CommonSubscription,
                    Selected = false
                });
            }

            if (enqProviderViewerRigs.Length > 0 || hasApprovedViewerRigs)
            {
                NotificationOption rigsOption = new NotificationOption
                {
                    OptionLabel = "Notify me for the following rigs:",
                    NotifyMeType = NotifyMeType.None,
                    GroupId = "NotifyForRig",
                    GroupName = "Rigs"
                };
                list.Add(rigsOption);

                var rigs = _db.Rigs
                    .ApplyPermission(roles)
                    .Where(x => enqApproverViewerRigs.Contains(x.Id) || enqProviderViewerRigs.Contains(x.Id))
                    .ToArray();
                foreach (var rig in rigs.OrderBy(x => x.Name))
                {
                    rigsOption.Options.Add(new NotificationOption
                    {
                        OptionLabel = rig.NameFormatted,
                        OptionValue = rig.Id.ToString(),
                        NotifyMeType = NotifyMeType.RigFilters,
                        Selected = false
                    });
                }

                NotificationOption typesOption = new NotificationOption
                {
                    OptionLabel = "Notify me for the following types:",
                    NotifyMeType = NotifyMeType.None,
                    GroupId = "NotifyForType",
                    GroupName = "Types"
                };
                list.Add(typesOption);

                typesOption.Options.Add(new NotificationOption
                {
                    OptionLabel = "Remote diagnostics",
                    OptionValue = ((int)TypeInfo.Type.Support_RemoteDiagnostics).ToString(),
                    NotifyMeType = NotifyMeType.EnquiryTypeFilters,
                    Selected = false
                   
                });
                typesOption.Options.Add(new NotificationOption
                {
                    OptionLabel = "Service",
                    OptionValue = ((int)TypeInfo.Type.Support_Service).ToString(),
                    NotifyMeType = NotifyMeType.EnquiryTypeFilters,
                    Selected = false
                });
                typesOption.Options.Add(new NotificationOption
                {
                    OptionLabel = "Spareparts",
                    OptionValue = ((int)TypeInfo.Type.Sparepart_General).ToString(),
                    NotifyMeType = NotifyMeType.EnquiryTypeFilters,
                    Selected = false
                   
                });
                typesOption.Options.Add(new NotificationOption
                {
                    OptionLabel = "Rig Optimization",
                    OptionValue = ((int)TypeInfo.Type.Support_RigOptimization).ToString(),
                    NotifyMeType = NotifyMeType.EnquiryTypeFilters,
                    Selected = false
                });
            }

            if (hasApprovedViewerRigs)
            {
                list.Add(new NotificationOption
                {
                    OptionLabel = "Task assigned to me",
                    OptionValue = ((int)SubscriptionType.EnquiryAssignedToMe).ToString(),
                    Selected = false,
                    NotifyMeType = NotifyMeType.CommonSubscription,
                    WrapperStyle = putBorderSeparator ? "border-top:1px dotted #000;padding-top:4px;" : ""
                });
                putBorderSeparator = false;
            }
            if (roles.GetEnquiryHandlingInitiatorRigs().Length > 0)
            {
                list.Add(new NotificationOption
                {
                    OptionLabel = "Drafts created by me deleted",
                    OptionValue = ((int)SubscriptionType.DraftEnquiryCreatedByMeDeleted).ToString(),
                    Selected = false,
                    NotifyMeType = NotifyMeType.CommonSubscription,
                    WrapperStyle = putBorderSeparator ? "border-top:1px dotted #000;padding-top:4px;" : ""
                });
                putBorderSeparator = false;
            }
            if (roles.HasUpgradeMatrixAccess())
            {
                list.Add(new NotificationOption
                {
                    OptionLabel = "New upgrade available",
                    OptionValue = ((int)SubscriptionType.NewUpgradeMatrixItemAvailable).ToString(),
                    Selected = false,
                    NotifyMeType = NotifyMeType.CommonSubscription,
                    WrapperStyle = putBorderSeparator ? "border-top:1px dotted #000;padding-top:4px;" : ""
                });
            }
            
            return list;
        }

        private void ApplyUserChoices(User user, MyNotificationsViewModel model)
        {

            foreach (var subscriptionSetting in user.SubscriptionSettings)
            {
                var selectedSubscription = model.NotificationOptions.FirstOrDefault(x => x.NotifyMeType == NotifyMeType.CommonSubscription && x.OptionValue == ((int)subscriptionSetting.SubscriptionType).ToString()) ??
                                           model.NotificationOptions.SelectMany(x => x.Options)
                                               .FirstOrDefault(x => x.NotifyMeType == NotifyMeType.CommonSubscription && x.OptionValue == ((int) subscriptionSetting.SubscriptionType).ToString());
                if (selectedSubscription != null)
                    selectedSubscription.Selected = true;
            }

            foreach (var subscriptionRigFilter in user.SubscriptionRigFilters)
            {
                var selectedRigFilter = model.NotificationOptions.SelectMany(x => x.Options)
                                               .FirstOrDefault(x => x.NotifyMeType == NotifyMeType.RigFilters && x.OptionValue == subscriptionRigFilter.RigId.ToString());
                if (selectedRigFilter != null)
                    selectedRigFilter.Selected = true;
            }

            foreach (var subscriptionRigFilter in user.SubscriptionEnquiryTypeFilters)
            {
                var selectedRigFilter = model.NotificationOptions.SelectMany(x => x.Options)
                    .FirstOrDefault(x => x.NotifyMeType == NotifyMeType.EnquiryTypeFilters && x.OptionValue == ((int)subscriptionRigFilter.Type).ToString());
                if (selectedRigFilter != null)
                    selectedRigFilter.Selected = true;
            }
        }
    }
}
