﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Web.QueryParameters;

namespace MyDrilling.Web.ViewComponents
{
    [ViewComponent(Name = "Contacts")]
    public class ContactsViewComponent : ViewComponent
    {
        private readonly MyDrillingDb _db;

        public ContactsViewComponent(MyDrillingDb db)
        {
            _db = db;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var drillDown = HttpContext.GetDrillDownParameters();
            if (!drillDown.HasRig)
            {
                return View("DefaultContact");
            }

            var rig = await _db.Rigs
                .Include(x => x.ResponsibleUser)
                .SingleOrDefaultAsync(x => x.Id == drillDown.RigId);

            if (rig?.ResponsibleUser == null)
            {
                return View("DefaultContact");
            }

            return View("Contact", rig.ResponsibleUser);
        }
    }
}
