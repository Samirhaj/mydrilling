﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Core;
using MyDrilling.Core.Entities.Equipment;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Web.Controllers;
using MyDrilling.Web.Models.Equipment;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.ViewComponents
{
    [ViewComponent(Name = "GroupedProducts")]
    public class GroupedProductsComponent : ViewComponent
    {
        private readonly ILogger<GroupedProductsComponent> _logger;
        private readonly MyDrillingDb _db;

        public GroupedProductsComponent(ILogger<GroupedProductsComponent> logger, MyDrillingDb db)
        {
            _logger = logger;
            _db = db;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var drillDown = HttpContext.GetDrillDownParameters();
            var roles = HttpContext.GetCurrentUserRoles();

            var equipments = await _db.Equipments
                .ApplyPermission(roles)
                .ApplyRigFilter(drillDown.RigId)
                .ExcludeProductless()
                .ToArrayAsync();
            var productsViewModel = new GroupedProductsViewModel
            {
                CurrentAction = ViewContext.RouteData.Values["Action"].ToString(),
                CurrentController = ViewContext.RouteData.Values["Controller"].ToString(),
                GroupedProducts = equipments.GroupBy(x => x.ProductGroup)
                    .OrderBy(x => x.Key)
                    .ToDictionary(
                        x => x.Key.ToCamelCase(),
                        x => x.GroupBy(y => y.ProductCode)
                            .OrderBy(y => y.Key)
                            .Select(y => new ProductViewModel
                            {
                                ProductCode = y.Key,
                                ProductName = y.First().ProductNameFormatted

                            })
                            .OrderBy(y => y.ProductName).ToList())
            };
            return View(productsViewModel);
        }
    }
}
