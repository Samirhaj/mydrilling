﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using MyDrilling.Core.Entities;
using MyDrilling.Web.Models;
using MyDrilling.Web.Models.Userprofile;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.ViewComponents
{
    public class MyPreferencesViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            var user = HttpContext.GetCurrentUser();
            var model = new MyPreferencesViewModel { PreferencesOptions = AvailablePreferencesOptions() };
            if (!model.PreferencesOptions.Any())
                return View("MyPreferences", model);
            var showUnreadOption = model.PreferencesOptions.FirstOrDefault(x => x.OptionValue == UserSetting.Keys.ShowUnreadEnquiries);
            if (showUnreadOption != null)
                showUnreadOption.Selected = user.GetShowUnreadEnquiries();
            var allowOthersOption = model.PreferencesOptions.FirstOrDefault(x => x.OptionValue == UserSetting.Keys.DoNotAllowOthersToAddMeToEnquiries);
            if (allowOthersOption != null)
                allowOthersOption.Selected = user.GetDoNotAllowOthersToAddMeToEnquiries();

            return View("MyPreferences", model);
        }

        private List<CheckboxOption> AvailablePreferencesOptions()
        {
            var list = new List<CheckboxOption>();
            var roles = HttpContext.GetCurrentUserRoles();

            if (roles.HasEnquiryHandlingPreparationViewerRigs() || roles.HasEnquiryHandlingApprovedViewerRigs())
            {
                list.Add(new CheckboxOption
                {
                    OptionLabel = "Show unread enquiries",
                    OptionValue = UserSetting.Keys.ShowUnreadEnquiries,
                    Selected = false
                });
                list.Add(new CheckboxOption
                {
                    OptionLabel = "Don't allow others to add me to enquiries",
                    OptionValue = UserSetting.Keys.DoNotAllowOthersToAddMeToEnquiries,
                    Selected = false
                });
            }
            return list;
        }
    }
}
