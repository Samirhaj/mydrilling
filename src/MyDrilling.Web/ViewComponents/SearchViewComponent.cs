﻿
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace MyDrilling.Web.ViewComponents
{
    public class SearchViewComponent : ViewComponent
    {
        private readonly ILogger<SearchViewComponent> _logger;

        public SearchViewComponent(ILogger<SearchViewComponent> logger)
        {
            _logger = logger;
        }

        public IViewComponentResult Invoke()
        {
            return View("Search");
        }
    }
}
