﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Web.Controllers;
using MyDrilling.Web.Models.Task;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.ViewComponents
{
    [ViewComponent(Name = "MyDrafts")]
    public class MyDraftsComponent : ViewComponent
    {
        private readonly ILogger<MyDraftsComponent> _logger;
        private readonly MyDrillingDb _db;

        public MyDraftsComponent(ILogger<MyDraftsComponent> logger, MyDrillingDb db)
        {
            _logger = logger;
            _db = db;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var draftViewerRigs = roles.GetEnquiryHandlingPreparationViewerRigs();
            if (!draftViewerRigs.Any())
                return View(new TaskListViewModel());

            var drillDown = HttpContext.GetDrillDownParameters();
            var availableDraftEnquiries = await _db.Enquiry_Enquiries
                .Include(x => x.Rig)
                .ApplyPermissions(roles)
                .ApplyDrillDown(drillDown)
                .ExcludeInternal()
                .ExcludeSoftDeleted()
                .WithLogicalState(LogicalStateInfo.LogicalState.Draft)
                .Where(x => x.CreatedById == user.Id)
                .OrderByDescending(x => x.Created)
                .ToArrayAsync();

            int myDraftsTotalCount = availableDraftEnquiries.Length;
            TaskViewModel[] myDrafts = availableDraftEnquiries.Take(5)
                .Select(x =>  x.MapDraftToTaskItem(user.Id))
                .ToArray();
            
            var taskListModel = new TaskListViewModel
            {
                Tasks = myDrafts,
                TotalCount = myDraftsTotalCount
            };
            return View(taskListModel);
        }
    }
}
