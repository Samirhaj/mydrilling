﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace MyDrilling.Web.Filters
{
    public class ModelValidationFilter : ActionFilterAttribute
    { 
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.ModelState.IsValid) return;

            var errors = context.ModelState.Where(x => x.Value.ValidationState == ModelValidationState.Invalid)
                .Select(x => new { x.Key, x.Value.Errors });
            
            context.Result = new BadRequestObjectResult(errors);
        }
    }
}
