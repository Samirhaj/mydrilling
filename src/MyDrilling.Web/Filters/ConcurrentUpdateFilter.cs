﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Web.QueryParameters;

namespace MyDrilling.Web.Filters
{
    public class ConcurrentUpdateFilter : IExceptionFilter
    {
        private readonly IUrlHelperFactory _urlHelperFactory;

        public ConcurrentUpdateFilter(IUrlHelperFactory urlHelperFactory)
        {
            _urlHelperFactory = urlHelperFactory;
        }

        public void OnException(ExceptionContext context)
        {
            if (!(context.Exception is DbUpdateConcurrencyException)) return;

            if (!context.RouteData.Values.TryGetValue("controller", out var controllerName)) return;

            var controllerNameString = controllerName.ToString();
            if (!controllerNameString.Equals("enquiries", StringComparison.InvariantCultureIgnoreCase)) return;

            if (!context.HttpContext.Request.Query.TryGetValue("id", out var id)) return;

            var idString = id.ToString();
            var url = _urlHelperFactory.GetUrlHelper(context);
            var enquiryUrl = url.Action("Detail", "Enquiries", 
                context.HttpContext.GetDrillDownParameters().ToRouteValueDictionary(new { id = idString, conflict = 1 }));
            if (IsAjaxRequest(context))
            {
                context.Result = new ConflictObjectResult(new {Url = enquiryUrl });
                return;
            }

            context.Result = new RedirectResult(enquiryUrl);
        }

        private bool IsAjaxRequest(ExceptionContext context)
        {
            return context.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
        }
    }
}
