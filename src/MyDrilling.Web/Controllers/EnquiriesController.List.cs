﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Web.Models.Enquiries;
using MyDrilling.Web.Models.Enquiries.Excel;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Controllers
{
    public partial class EnquiriesController
    {
        private static readonly SelectListItem[] DraftEnquiryStateFilterItems = new[]
        {
            new SelectListItem("Draft", ((int)StateInfo.State.Draft).ToString()),
            new SelectListItem("Ready for approval", ((int)StateInfo.State.ReadyForApproval).ToString()),
            new SelectListItem("Rejected", ((int)StateInfo.State.Rejected).ToString()),
        };

        [HttpGet("")]
        [HttpGet("index")] 
        public async Task<IActionResult> Index() => 
            View("OpenEnquiries", await PrepareListModel(nameof(Index), LogicalStateInfo.LogicalState.Open));

        [HttpGet("drafts")] 
        public async Task<IActionResult> Drafts() =>
            View("DraftEnquiries", await PrepareListModel(nameof(Drafts), LogicalStateInfo.LogicalState.Draft));

        [HttpGet("archive")]
        public async Task<IActionResult> Archive() =>
            View("ClosedEnquiries", await PrepareListModel(nameof(Archive), LogicalStateInfo.LogicalState.Closed));

        [HttpGet("deleted")]
        public async Task<IActionResult> Deleted() =>
            View("DeletedEnquiries", await PrepareListModel(nameof(Deleted), LogicalStateInfo.LogicalState.Deleted));

        [HttpGet("all")]
        public async Task<IActionResult> All() =>
            View("AllEnquiries", await PrepareListModel(nameof(All)));

        [HttpGet("OpenToExcel")]
        public Task<IActionResult> OpenToExcel() =>
            ImportToExcel(LogicalStateInfo.LogicalState.Open);

        [HttpGet("DraftToExcel")]
        public Task<IActionResult> DraftToExcel() =>
            ImportToExcel(LogicalStateInfo.LogicalState.Draft);

        [HttpGet("ArchiveToExcel")]
        public Task<IActionResult> ArchiveToExcel() =>
            ImportToExcel( LogicalStateInfo.LogicalState.Closed);

        [HttpGet("DeletedToExcel")]
        public Task<IActionResult> DeletedToExcel() =>
            ImportToExcel(LogicalStateInfo.LogicalState.Deleted);

        [HttpGet("AllToExcel")]
        public Task<IActionResult> AllToExcel() =>
            ImportToExcel();

        private async Task<ItemsViewModel> PrepareListModel(string currentActionName, LogicalStateInfo.LogicalState? logicalState = null)
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var menuModel =  new MenuViewModel
            {
                CanSeeOpenEnquiries = roles.HasEnquiryHandlingApprovedViewerRigs(),
                CanSeeDraftEnquiries = roles.HasEnquiryHandlingPreparationViewerRigs(),
                CanSeeClosedEnquiries = roles.HasEnquiryHandlingApprovedViewerRigs(),
                CanSeeDeletedEnquiries = roles.HasEnquiryHandlingPreparationViewerRigs(),
                CanSeeAllEnquiries = roles.HasEnquiryHandlingApprovedViewerRigs() && roles.HasEnquiryHandlingPreparationViewerRigs(),
                CurrentActionName = currentActionName
            };

            var enquiryQuery = _db.Enquiry_Enquiries.ApplyPermissions(HttpContext.GetCurrentUserRoles())
                .ApplyDrillDown(HttpContext.GetDrillDownParameters())
                .ExcludeInternal()
                .ExcludeSoftDeleted()
                .WithLogicalState(logicalState);
            var enquiryQueryFilter = enquiryQuery
                .ApplyFilter(HttpContext.GetFilterParameters());

            var totalCount = await enquiryQueryFilter.CountAsync();

            var enquiries = await enquiryQueryFilter
                .ApplyOrder(HttpContext.GetOrderParameters())
                .ApplyPager(HttpContext.GetPagerParameters())
                .Select(e => new ItemViewModel
                {
                    ReferenceId = e.ReferenceId,
                    Title = e.Title,
                    Id = e.Id,
                    CustomerName = e.RootCustomer.Name,
                    RigName = e.Rig.Name.ToCamelCaseWithRomanNumerals(),
                    RigReferenceId = e.Rig.ReferenceId,
                    TypeId = e.Type,
                    AssignedToCustomerName = e.AssignedToCustomer.Name,
                    AssignedToName = e.AssignedTo.Upn,
                    EquipmentName = e.Equipment.Name.ToCamelCase(),
                    LastCommentedDateTime = e.LastCommented,
                    Date = e.Date,
                    State = e.State,
                    LogicalState = e.LogicalState,
                    IsRead = true
                })
                .ToArrayAsync();

            await AddFilterData(enquiryQuery);
            if (user.GetShowUnreadEnquiries())
            {
                var selectedEnquiryIds = enquiries.Select(x => x.Id).ToArray();
                var readEnquiryIds = (await _db.Enquiry_ReadReceipts
                    .Where(x => x.UserId == user.Id && selectedEnquiryIds.Contains(x.EnquiryId))
                    .Select(x => x.EnquiryId)
                    .ToArrayAsync()).ToHashSet();
                foreach (var enquiry in enquiries)
                {
                    enquiry.IsRead = readEnquiryIds.Contains(enquiry.Id);
                }
            }

            var model = new ItemsViewModel
            {
                Menu = menuModel,
                Items = enquiries,
                TotalCount = totalCount
            };

            return model;
        }

        private async Task<IActionResult> ImportToExcel(LogicalStateInfo.LogicalState? logicalState = null)
        {
            var user = HttpContext.GetCurrentUser();

            var enquiries = await _db.Enquiry_Enquiries.ApplyPermissions(HttpContext.GetCurrentUserRoles())
                .ApplyDrillDown(HttpContext.GetDrillDownParameters())
                .ExcludeInternal()
                .ExcludeSoftDeleted()
                .WithLogicalState(logicalState)
                .ApplyFilter(HttpContext.GetFilterParameters())
                .ApplyOrder(HttpContext.GetOrderParameters())
                .Select(e => new EnquiryExcelViewModel
                {
                    ReferenceId = e.ReferenceId,
                    Title = e.Title,
                    CustomerName = e.RootCustomer.Name,
                    RigName = e.Rig.Name.ToCamelCaseWithRomanNumerals(),
                    TypeId = e.Type,
                    AssignedToCustomerName = e.AssignedToCustomer.Name,
                    EquipmentName = e.Equipment.Name.ToCamelCase(),
                    LogicalState = e.LogicalState,
                    Date = e.Date,
                    State = e.State
                })
                .ToArrayAsync();

            if (!logicalState.HasValue)
            {
                return File(new AllEnquiriesExcelListGenerator().CreateExcelDocument(enquiries, user),
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "AllEnquiries.xlsx");
            }

            switch (logicalState)
            {
                case LogicalStateInfo.LogicalState.Open:
                    return File(new OpenEnquiriesExcelListGenerator().CreateExcelDocument(enquiries, user), 
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "OpenEnquiries.xlsx");
                case LogicalStateInfo.LogicalState.Draft:
                    return File(new DraftEnquiriesExcelListGenerator().CreateExcelDocument(enquiries, user), 
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "DraftEnquiries.xlsx");
                case LogicalStateInfo.LogicalState.Deleted:
                    return File(new DeletedEnquiriesExcelListGenerator().CreateExcelDocument(enquiries, user), 
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "DeletedEnquiries.xlsx");
                case LogicalStateInfo.LogicalState.Closed:
                    return File(new ClosedEnquiriesExcelListGenerator().CreateExcelDocument(enquiries, user), 
                        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ClosedEnquiries.xlsx");
            }

            throw new ArgumentException("Enquiry logical state should be Open, Draft or Closed");
        }

        private async Task AddFilterData(IQueryable<Enquiry> enquiries)
        {
            var availableCustomers = HttpContext.GetCurrentUserRoles().GetAccessToRootCustomers();
            var rootCustomers = await _db.RootCustomers.AsNoTracking().Where(x => availableCustomers.Contains(x.Id)).ToArrayAsync();
            var mhWirth = await _db.RootCustomers.AsNoTracking().FirstAsync(x => x.IsInternal);

            ViewData[ItemsViewModel.TypeFilter] = TypeInfo.Types
                .Select(x => new SelectListItem(x.Title, ((int)x.Value).ToString()))
                .ToArray();

            ViewData[ItemsViewModel.CustomerFilter] = rootCustomers
                .Select(x => new SelectListItem(x.Name, x.Id.ToString()))
                .ToArray(); 
            
            var assignedTo = rootCustomers
                .Select(x => new SelectListItem(x.Name, x.Id.ToString()))
                .ToList();
            assignedTo.Add(new SelectListItem(mhWirth.Name, mhWirth.Id.ToString()));
            ViewData[ItemsViewModel.AssignedToFilter] = assignedTo.ToArray();

            ViewData[ItemsViewModel.AssigneeFilter] = await enquiries
                .Where(x => x.AssignedToId != null && availableCustomers.Contains(x.RootCustomer.Id))
                .Select(x => new SelectListItem(x.AssignedTo.Upn, x.AssignedToId.Value.ToString()))
                .Distinct()
                .ToArrayAsync();

            ViewData[ItemsViewModel.CategoryFilter] = TypeInfo.Types
                .Select(x => x.Category).Distinct()
                .Select(x => new SelectListItem(x, x))
                .ToArray();
            
            ViewData[ItemsViewModel.DraftStateFilter] = DraftEnquiryStateFilterItems;

            ViewData[ItemsViewModel.LogicalStateFilter] = LogicalStateInfo.LogicalStates
                .Select(x => new SelectListItem(x.Title, ((int)x.Value).ToString()))
                .ToArray();
        }
    }

    public static class EnquiryFilterExt
    {
        public static IQueryable<Enquiry> ExcludeSoftDeleted(this IQueryable<Enquiry> query) =>
            query.Where(e => !e.IsDeleted);

        public static IQueryable<Enquiry> ExcludeInternal(this IQueryable<Enquiry> query) =>
            query.Where(e => e.Type != TypeInfo.Type.Internal_Internal);

        public static IQueryable<Enquiry> WithLogicalState(this IQueryable<Enquiry> query, LogicalStateInfo.LogicalState? logicalState) =>
            logicalState.HasValue ? query.Where(e => e.LogicalState == logicalState.Value) : query;

        public static IQueryable<Enquiry> ApplyPager(this IQueryable<Enquiry> query, PagerParameters pager) =>
            query.Skip(pager.PageSize * (pager.CurrentPageNumber - 1))
                .Take(pager.PageSize);

        public static IQueryable<Enquiry> ApplyFilter(this IQueryable<Enquiry> query, FilterParameters filter)
        {
            switch (filter.FilterField)
            {
                case ItemsViewModel.TypeFilter:
                    var type = (TypeInfo.Type)int.Parse(filter.FilterValue);
                    return query.Where(x => x.Type == type);
                case ItemsViewModel.CustomerFilter:
                    var rootCustomerId = long.Parse(filter.FilterValue);
                    return query.Where(x => x.RootCustomerId == rootCustomerId);
                case ItemsViewModel.AssignedToFilter:
                    var assignedToId = long.Parse(filter.FilterValue);
                    return query.Where(x => x.AssignedToCustomerId == assignedToId);
                case ItemsViewModel.AssigneeFilter:
                    var assignedTo = long.Parse(filter.FilterValue);
                    return query.Where(x => x.AssignedToId == assignedTo);
                case ItemsViewModel.CategoryFilter:
                    var types = TypeInfo.ByCategory[filter.FilterValue].Select(x => x.Value).ToArray();
                    return query.Where(x => types.Contains(x.Type));
                case ItemsViewModel.DraftStateFilter:
                    var state = (StateInfo.State)int.Parse(filter.FilterValue);
                    return query.Where(x => x.State == state);
                case ItemsViewModel.LogicalStateFilter:
                    var logicalState = (LogicalStateInfo.LogicalState)int.Parse(filter.FilterValue);
                    return query.Where(x => x.LogicalState == logicalState);
                default:
                    return query;
            }
        }

        public static IQueryable<Enquiry> ApplyOrder(this IQueryable<Enquiry> query, SortParameters sort)
        {
            switch (sort.SortField)
            {
                case ItemsViewModel.CustomerSort:
                    return sort.IsAsc
                        ? query.OrderBy(x => x.RootCustomer.Name)
                        : query.OrderByDescending(x => x.RootCustomer.Name);
                case ItemsViewModel.RigSort:
                    return sort.IsAsc
                        ? query.OrderBy(x => x.Rig.Name)
                        : query.OrderByDescending(x => x.Rig.Name);
                case ItemsViewModel.TitleSort:
                    return sort.IsAsc
                        ? query.OrderBy(x => x.Title)
                        : query.OrderByDescending(x => x.Title);
                case ItemsViewModel.DateSort:
                    return sort.IsAsc
                        ? query.OrderBy(x => x.Date)
                        : query.OrderByDescending(x => x.Date);
                case ItemsViewModel.AssignedToCustomerSort:
                    return sort.IsAsc
                        ? query.OrderBy(x => x.AssignedToCustomer.Name)
                        : query.OrderByDescending(x => x.AssignedToCustomer.Name);
                case ItemsViewModel.AssignedToUserSort:
                    return sort.IsAsc
                        ? query.OrderBy(x => x.AssignedTo.Upn)
                        : query.OrderByDescending(x => x.AssignedTo.Upn);
                case ItemsViewModel.EquipmentSort:
                    return sort.IsAsc
                        ? query.OrderBy(x => x.Equipment.Name)
                        : query.OrderByDescending(x => x.Equipment.Name);
                case ItemsViewModel.LastCommentSort:
                    return sort.IsAsc
                        ? query.OrderBy(x => x.LastCommented)
                        : query.OrderByDescending(x => x.LastCommented);
                case ItemsViewModel.LogicalStateSort:
                    return sort.IsAsc
                        ? query.OrderBy(x => x.LogicalState)
                        : query.OrderByDescending(x => x.LogicalState);
                default:
                    return query.OrderByDescending(x => x.Date);
            }
        }

        public static IQueryable<Enquiry> ApplyDrillDown(this IQueryable<Enquiry> query, DrillDownParameters drillDown)
        {
            if (drillDown.HasRig)
            {
                query = query.Where(x => x.RigId == drillDown.RigId);
            }
            if (drillDown.HasEquipment)
            {
                query = query.Where(x => x.EquipmentId == drillDown.EquipmentId);
            }
            if (drillDown.HasProductCode)
            {
                query = query.Where(x => x.Equipment.ProductCode == drillDown.ProductCode);
            }
            return query;
        }
    }
}