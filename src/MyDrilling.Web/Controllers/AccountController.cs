﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.AzureAD.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MyDrilling.Web.Controllers
{
    public class AccountController : Controller
    {
        [HttpGet("signout")]
        public IActionResult SignOut()
        {
            var callbackUrl = Url.Action("SignedOut");
            return SignOut(
                new AuthenticationProperties
                {
                    RedirectUri = callbackUrl,
                }, AzureADDefaults.AuthenticationScheme,
                AzureADDefaults.CookieScheme,
                AzureADDefaults.OpenIdScheme);
        }

        [AllowAnonymous]
        [HttpGet("signedout")]
        public IActionResult SignedOut()
        {
            return Redirect("https://www.mydrilling.com");
        }
    }
}
