﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core;
using MyDrilling.Core.Entities.Bulletin;
using MyDrilling.Core.Entities.Rig;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.Services.AsyncOperation;
using MyDrilling.Infrastructure.Storage.Queue;
using MyDrilling.Web.Models.Bulletins;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Controllers
{
    [Route("bulletins")]
    public class BulletinsController : Controller
    {
        private readonly MyDrillingDb _db;
        private readonly IQueueSender _queueSender;

        private readonly IReadOnlyList<string> _hseAndAlertsBulletins = new List<string>
        {
            BulletinTypes.HseAlert, BulletinTypes.HseAlertGeneral, BulletinTypes.HseBulletin, BulletinTypes.HseBulletinGeneral
        };

        private readonly IReadOnlyList<string> _productBulletins = new List<string> { BulletinTypes.ProductBulletin, BulletinTypes.ProductBulletinGeneral};

        public BulletinsController(MyDrillingDb db,
            IQueueSender queueSender)
        {
            _db = db;
            _queueSender = queueSender;
        }


        [HttpGet("")]
        [HttpGet("index")]
        public async Task<IActionResult> Index() =>
            View("HseAndAlertsBulletins", await CreateItemsViewModel(nameof(Index), _hseAndAlertsBulletins));

        [HttpGet("hseandalerts")]
        public async Task<IActionResult> HseAndAlerts() =>
            View("HseAndAlertsBulletins", await CreateItemsViewModel(nameof(HseAndAlerts), _hseAndAlertsBulletins));

        [HttpGet("product")]
        public async Task<IActionResult> Product() =>
            View("ProductBulletins", await CreateItemsViewModel(nameof(Product), _productBulletins));

        [HttpGet("detail")]
        public async Task<IActionResult> Detail(long id, long rigId)
        {
            if (id <= 0 || rigId <= 0)
            {
                return StatusCode(400, "Missing data.");
            }
            var roles = HttpContext.GetCurrentUserRoles();
            var rig = await _db.Rigs.ApplyPermission(roles).FirstOrDefaultAsync(r => r.Id == rigId);
            if (rig == null)
            {
                return RedirectToAction("ContentNotFound", "Home");
            }
            var allowedRigIds = roles.GetBulletinViewerAccessToRigs();
            if (!allowedRigIds.Contains(rigId))
            {
                return RedirectToAction("NoAccess", "Home");
            }

            var zaBulletin = await _db.Bulletin_ZaBulletins
                .Include(za => za.Type)
                .AsNoTracking()
                .FirstOrDefaultAsync(za => za.Id == id);

            if (zaBulletin == null)
            {
                return RedirectToAction("ContentNotFound", "Home");
            }
            //check if this zaBulletin has zbBulletin for this rig
            var zbBulletins = await _db.Bulletin_ZbBulletins
                .ApplyPermissions(roles)
                .Where(x => x.ZaBulletinId == id && x.RigId == rigId).ToArrayAsync();
            if (!zbBulletins.Any())
                return RedirectToAction("ContentNotFound", "Home");

            var equipment = await _db.Bulletin_ZbBulletins
                .ApplyPermissions(roles)
                .Where(zb => zb.Equipment != null)
                .Where(zb => zb.ZaBulletinId == id && zb.RigId.HasValue && zb.RigId.Value == rigId)
                .Select(zb => new { zb.Equipment.Name, zb.Equipment.Id })
                .ToListAsync();

            var rows = equipment.Select(eq => new ImplementedRowViewModel
            {
                Caption = "Relevant for all equipment",
                EquipmentName = eq.Name,
                EquipmentReferenceId = eq.Id
            });

            var model = new DetailViewModel
            {
                Id = zaBulletin.Id,
                RigId = rig.Id,
                Title = zaBulletin.Title,
                Description = zaBulletin.Description,
                Type = zaBulletin.Type.Name,
                DocumentId = zaBulletin.DocumentReferenceId,
                ImplementedRows = rows,
                IssuedDate = zaBulletin.IssueDate,
                RelevantFor = $"Relevant for ({rig.NameFormatted})"
            };

            return View(model);
        }

        [HttpPost("requestdownload")]
        public async Task<IActionResult> RequestDownload(long id, long rigId)
        {
            if (id <= 0 || rigId <= 0)
            {
                return StatusCode(400, "Missing data.");
            }
            var roles = HttpContext.GetCurrentUserRoles();
            var allowedRigIds = roles.GetBulletinViewerAccessToRigs();
            if (!allowedRigIds.Contains(rigId))
            {
                return Unauthorized();
            }
            //check if this zaBulletin has zbBulletin for this rig
            var zbBulletins = await _db.Bulletin_ZbBulletins
                .ApplyPermissions(roles)
                .Where(x => x.ZaBulletinId == id && x.RigId == rigId).ToArrayAsync();
            if (!zbBulletins.Any())
                return NotFound();
            string fileUrl = "";
            string fileName = "";
            var bulletinDoc = await _db.Bulletin_ZaBulletins
                .FirstOrDefaultAsync(x => x.Id == id);
            if (bulletinDoc == null 
                || string.IsNullOrEmpty(bulletinDoc.ReferenceId) 
                || string.IsNullOrEmpty(bulletinDoc.DocumentReferenceId))
                return NotFound();
            
            fileUrl = bulletinDoc.DocumentReferenceId;
            fileName = $"{bulletinDoc.Title}.pdf";

            if (string.IsNullOrEmpty(fileUrl) || string.IsNullOrEmpty(fileName))
                return StatusCode(404, "File not found or you don't have permissions.");

            var requestId = await _db.CreateNewAsyncOperation(HttpContext.GetCurrentUser());
            var docUrlParts = fileUrl.Split("|");
            if (docUrlParts.Length < 4)
            {
                return StatusCode(400, "Missing data.");
            }
            await _queueSender.EnqueueFileRequestAsync(requestId, $"bulletin-documents/{fileName}", docUrlParts[0], docUrlParts[1], docUrlParts[2], docUrlParts[3], true);

            return StatusCode(200, requestId);
        }

        private async Task<ItemsViewModel> CreateItemsViewModel(string actionName, IReadOnlyList<string> bulletinTypes)
        {
            var menuModel = new MenuViewModel
            {
                CurrentActionName = actionName
            };

            var zbBulletins = await _db.Bulletin_ZbBulletins
                .Include(b => b.Rig)
                .ApplyPermissions(HttpContext.GetCurrentUserRoles())
                .WithType(bulletinTypes)
                .ApplyFilter(HttpContext.GetFilterParameters())
                .ApplyDrillDown(HttpContext.GetDrillDownParameters())
                .ToArrayAsync();
            var groupedBulletins = zbBulletins
                .GroupBy(b => new { b.ZaBulletinId, RigId = b.RigId ?? default, RigName = b.Rig?.Name })
                //.Select(b => new { b.Key.ZaBulletinId, b.Key.RigId, b.Key.RigName, Modified = b.First().Modified.HasValue ? b.First().Modified.Value : b.First().Created  });
                .Select(b => new { b.Key.ZaBulletinId, RigId = b.Key.RigId, RigName = b.Key.RigName, AllBulletins = b.ToArray()  }).ToArray();
            
            var totalCount = groupedBulletins.Count();
            
            var zaBulletins = _db.Bulletin_ZaBulletins.Include(b => b.Type);
            var entries = groupedBulletins
                .Join(zaBulletins, zb => zb.ZaBulletinId, za => za.Id,  (zb, za) => new
                {
                    ZaId = za.Id,
                    zb.RigId,
                    za.TypeId,
                    za.Type,
                    za.IssueDate,
                    za.Description,
                    za.Title,
                    zb.RigName,
                    Modified = zb.AllBulletins.Any() && zb.AllBulletins.First().Modified.HasValue ? zb.AllBulletins.First().Modified.Value : zb.AllBulletins.First().Created
                });
            
            var sort = HttpContext.GetOrderParameters();
            switch (sort.SortField)
            {
                case ItemsViewModel.IdSort:
                    entries = sort.IsAsc ? entries.OrderBy(g => g.ZaId) : entries.OrderByDescending(g => g.ZaId);
                    break;
                case ItemsViewModel.DescriptionSort:
                    entries = sort.IsAsc ? entries.OrderBy(g => g.Description) : entries.OrderByDescending(g => g.Description);
                    break;
                case ItemsViewModel.TypeSort:
                    entries = sort.IsAsc ? entries.OrderBy(g => g.TypeId) : entries.OrderByDescending(g => g.TypeId);
                    break;
                case ItemsViewModel.IssuedDateSort:
                    entries = sort.IsAsc ? entries.OrderBy(g => g.IssueDate) : entries.OrderByDescending(g => g.IssueDate);
                    break;
                case ItemsViewModel.ModifiedDateSort:
                    entries = sort.IsAsc ? entries.OrderBy(g => g.Modified) : entries.OrderByDescending(g => g.Modified);
                    break;
                case ItemsViewModel.RigSort:
                    entries = sort.IsAsc ? entries.OrderBy(g => g.RigName) : entries.OrderByDescending(g => g.RigName);
                    break;
                default:
                    entries = entries.OrderByDescending(g => g.IssueDate);
                    break;
            }

            var pager = HttpContext.GetPagerParameters();
            entries = entries
                .Skip(pager.PageSize * (pager.CurrentPageNumber - 1))
                .Take(pager.PageSize);

            var bulletins = entries
                .Select(e => new ItemViewModel
                {
                    Id = e.ZaId,
                    RigId = e.RigId,
                    Title = e.Title,
                    Description = e.Description,
                    Type = e.Type,
                    RigName = e.RigName.ToCamelCase(true),
                    IssuedDate = e.IssueDate.ToString(HtmlExt.StdDateFormat, CultureInfo.InvariantCulture),
                    ModifiedDate = e.Modified.ToString(HtmlExt.StdDateFormat, CultureInfo.InvariantCulture)
                }).ToArray();
            
            await AddFilterData();

            var model = new ItemsViewModel
            {
                Menu = menuModel,
                Items = bulletins,
                TotalCount = totalCount
            };

            return model;
        }

        private async Task AddFilterData()
        {
            var bulletinTypes = await _db.Bulletin_BulletinTypes
                .Where(bt => _hseAndAlertsBulletins.Contains(bt.Name))
                .Select(bt => new {bt.Id, bt.Name })
                .ToListAsync();

            ViewData[ItemsViewModel.TypeFilter] = bulletinTypes
                .Select(bt => new SelectListItem(bt.Name, bt.Id.ToString()))
                .OrderBy(i => i.Text)
                .ToArray();
        }
    }

    public static class BulletinFilterExt
    {
        public static IQueryable<ZbBulletin> WithType(this IQueryable<ZbBulletin> query, IReadOnlyList<string> bulletinTypes)
        {
            return bulletinTypes != null && bulletinTypes.Count > 0 ? query.Where(b => bulletinTypes.Contains(b.ZaBulletin.Type.Name)) : query;
        }

        public static IQueryable<ZbBulletin> ApplyFilter(this IQueryable<ZbBulletin> query, FilterParameters filter)
        {
            switch (filter.FilterField)
            {
                case ItemsViewModel.TypeFilter:
                    return short.TryParse(filter.FilterValue, out var id) ? query.Where(b => b.ZaBulletin.Type.Id == id) : query;
                default:
                    return query;
            }
        }

        public static IQueryable<ZbBulletin> ApplyDrillDown(this IQueryable<ZbBulletin> query, DrillDownParameters drillDown)
        {
            var newQuery = query;
            if (drillDown.HasRig)
                newQuery = newQuery.Where(x => x.RigId == drillDown.RigId);
            if (drillDown.HasProductCode)
                newQuery = newQuery.Where(x => x.Equipment != null && x.Equipment.ProductCode == drillDown.ProductCode);
            if (drillDown.HasEquipment)
                newQuery = newQuery.Where(x => x.Equipment != null && x.Equipment.Id == drillDown.EquipmentId);
            if (drillDown.HasDuration)
                newQuery = newQuery.Where(x => x.ZaBulletin.IssueDate >= DateTime.UtcNow.AddDays(-drillDown.DurationInDays));
            return newQuery;
        }
    }
}