﻿using System.Collections.Generic;
using System.Linq;
using Azure.Storage.Blobs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.Storage.Blob;
using MyDrilling.Infrastructure.Storage.Queue;
using MyDrilling.Web.Filters;

namespace MyDrilling.Web.Controllers
{
    [Route("enquiries")]
    [ServiceFilter(typeof(ConcurrentUpdateFilter))]
    public partial class EnquiriesController : Controller
    {
        private readonly MyDrillingDb _db;
        private readonly IQueueSender _queueSender;
        private readonly BlobServiceClient _blobServiceClient;
        private readonly IBlobSasGenerator _blobSasGenerator;
        private readonly HashSet<string> _fileExtensionsWhiteList;

        public EnquiriesController(MyDrillingDb db,
            IQueueSender queueSender,
            BlobServiceClient blobServiceClient,
            IBlobSasGenerator blobSasGenerator,
            IConfiguration config)
        {
            _db = db;
            _queueSender = queueSender;
            _blobServiceClient = blobServiceClient;
            _blobSasGenerator = blobSasGenerator;
            _fileExtensionsWhiteList = config.GetValue<string>("EnquiryFileExtensionsWhiteList").Split(';').ToHashSet();
        }
    }
}
