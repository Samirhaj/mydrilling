﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Azure.Storage.Sas;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Infrastructure.Storage;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;
using MyDrilling.Infrastructure.Services.AsyncOperation;

namespace MyDrilling.Web.Controllers
{
    public partial class EnquiriesController
    {
        [HttpPost("addFile")]
        public async Task<IActionResult> AddFile(IFormFile file)
        {
            if (file == null || file.Length == 0 || string.IsNullOrEmpty(file.FileName) || string.IsNullOrEmpty(Path.GetFileName(file.FileName)))
            {
                return BadRequest("File not selected");
            }

            if (!_fileExtensionsWhiteList.Contains(Path.GetExtension(file.FileName)?.ToLower().TrimStart('.')))
            {
                return BadRequest("File type is not allowed");
            }

            var user = HttpContext.GetCurrentUser();

            var blobPath = $"{Guid.NewGuid()}\\{Path.GetFileName(file.FileName)}";
            var blobClient = _blobServiceClient.GetBlobContainerClient(StorageConfig.EnquiriesContainerName)
                .GetBlobClient(blobPath);

            await using (var readStream = file.OpenReadStream())
            {
                await blobClient.UploadAsync(readStream, metadata: new Dictionary<string, string> { { StorageConstants.BlobMetadataKeys.AuthorId, user.Id.ToString() } });
            }

            return Json(new 
            {
                BlobPath = blobPath,
                DownloadUrl = await _blobSasGenerator.GetSasBlobUri(blobPath, StorageConfig.EnquiriesContainerName, BlobAccountSasPermissions.Read)
            });
        }

        [HttpPost("deleteFile")]
        public async Task<IActionResult> DeleteFile(long id, long fileId)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries
                .Include(x => x.Attachments)
                .Include(x => x.Comments)
                .FirstOrDefaultAsync(x => x.Id == id);

            var fileToDelete = enquiry.Attachments.FirstOrDefault(x => x.Id == fileId);
            if (fileToDelete == null)
            {
                return RedirectToAction("Detail", new { id });
            }

            if (!enquiry.CanDeleteAttachment(roles))
            {
                return Unauthorized();
            }

            _db.Enquiry_Attachments.Remove(fileToDelete);
            await _db.SaveChangesAsync();

            var blobClient = _blobServiceClient.GetBlobContainerClient(StorageConfig.EnquiriesContainerName)
                .GetBlobClient(fileToDelete.BlobPath);
            await blobClient.DeleteIfExistsAsync();

            return RedirectToAction("Detail", HttpContext.GetDrillDownParameters().ToRouteValueDictionary(new { enquiry.Id }));
        }

        [HttpPost("getFile")]
        public async Task<IActionResult> GetFile(long id, long fileId)
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries.AsNoTracking()
                .Include(x => x.Attachments)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (enquiry.Attachments.All(f => f.Id != fileId))
            {
                return BadRequest();
            }

            var file = enquiry.Attachments.First(f => f.Id == fileId);

            if (!enquiry.CanRead(roles))
            {
                return Unauthorized();
            }

            //file isn't in SAP, read from temp blob location
            if (string.IsNullOrEmpty(file.ReferenceId) && file.CreatedById.HasValue)
            {
                return Ok(await _blobSasGenerator.GetSasBlobUri(file.BlobPath, StorageConfig.EnquiriesContainerName, BlobAccountSasPermissions.Read));
            }

            if (string.IsNullOrEmpty(file.ReferenceId))
            {
                return BadRequest();
            }

            var finalBlobPath = StorageConstants.Enquiry.GetFinalBlobLocation(enquiry.Id, file.BlobPath);
            var blobClient = _blobServiceClient.GetBlobContainerClient(StorageConfig.TemporaryDocumentsContainerName)
                .GetBlobClient(finalBlobPath);

            //file in SAP and file is already downloaded into final blob location
            if (await blobClient.ExistsAsync())
            {
                return Ok(await _blobSasGenerator.GetSasBlobUri(finalBlobPath, StorageConfig.TemporaryDocumentsContainerName, BlobAccountSasPermissions.Read));
            }

            //file in SAP but it's not downloaded, trigger downloading
            var operationId = await _db.CreateNewAsyncOperation(user);
            var fileParts = file.ReferenceId.Split('|');
            await _queueSender.EnqueueFileRequestAsync(operationId, finalBlobPath, fileParts[0], fileParts[1], fileParts[2], fileParts[3], false);
            return Ok(operationId);
        }
    }
}
