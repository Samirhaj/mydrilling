﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Core.Entities.Subscription;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Web.Models.Userprofile;
using MyDrilling.Web.UserDetails;
using UserSetting = MyDrilling.Core.Entities.UserSetting;

namespace MyDrilling.Web.Controllers
{
    public class UserprofileController : Controller
    {
        private readonly ILogger<UserprofileController> _logger;
        private readonly MyDrillingDb _db;

        public UserprofileController(ILogger<UserprofileController> logger, MyDrillingDb db)
        {
            _logger = logger;
            _db = db;
        }
        public IActionResult Index()
        {
            var user = HttpContext.GetCurrentUser();
            var userProfile = new UserProfileViewModel
            {
                FullName = user.DisplayName,
                FirstName = user.FirstName,
                LastName = user.LastName,
                CompanyName = user.RootCustomer.Name,
                EmailAddress = user.Email,
                PhoneNumber = user.PhoneNumber,
                Upn = user.Upn

            };
            return View(userProfile);
        }

        public ActionResult MyPreferences()
        {
            return ViewComponent("MyPreferences");
        }

        public ActionResult MyNotifications()
        {
            return ViewComponent("MyNotifications");
        }

        public ActionResult MyTimeZone()
        {
            return ViewComponent("MyTimeZone");
        }

        [HttpPost]
        public async Task<ActionResult> SubmitMyTimeZone(TimeZoneViewModel model)
        {
            var user = HttpContext.GetCurrentUser();
            var currentTimeZone = user.GetTimeZoneInfo();
            if (currentTimeZone.Id != model.SelectedTimeZone)
            {
                var timeZoneRow = await _db.UserSettings.FirstOrDefaultAsync(x => x.UserId == user.Id && x.Key == UserSetting.Keys.SelectedTimezone);
                if (timeZoneRow != null)
                {
                    timeZoneRow.Value = model.SelectedTimeZone;
                }
                else
                {
                    _db.UserSettings.Add(new UserSetting
                    {
                        UserId = user.Id,
                        Key = UserSetting.Keys.SelectedTimezone,
                        Value = model.SelectedTimeZone
                    });
                }
                await _db.SaveChangesAsync();
            }
            return MyTimeZone();
        }


        [HttpPost]
        public async Task<ActionResult> SubmitMyPreferences(MyPreferencesViewModel model)
        {
            if (model.SelectedPreferences == null)
                model.SelectedPreferences = new List<string>();
            var user = HttpContext.GetCurrentUser();
            var currentShowUnreadEnquiries = user.GetShowUnreadEnquiries();
            var showUnreadOption = model.PreferencesOptions.FirstOrDefault(x => x.OptionValue == UserSetting.Keys.ShowUnreadEnquiries);
            if (showUnreadOption != null && currentShowUnreadEnquiries != showUnreadOption.Selected)
            {
                //update user preference for ShowUnreadEnquiries
                var showUnreadEnquiriesRow = await _db.UserSettings.FirstOrDefaultAsync(x => x.UserId == user.Id && x.Key == UserSetting.Keys.ShowUnreadEnquiries);
                if (showUnreadEnquiriesRow != null)
                {
                    showUnreadEnquiriesRow.Value = showUnreadOption.Selected.ToString();
                    
                }
                else
                {
                    _db.UserSettings.Add(new UserSetting
                    {
                        UserId = user.Id,
                        Key = UserSetting.Keys.ShowUnreadEnquiries,
                        Value = showUnreadOption.Selected.ToString()
                    });
                }
                await _db.SaveChangesAsync();
            }

            var currentDontAllowOthersToAddMeToEnquiries = user.GetDoNotAllowOthersToAddMeToEnquiries();
            var dontAllowOthersOption = model.PreferencesOptions.FirstOrDefault(x => x.OptionValue == UserSetting.Keys.DoNotAllowOthersToAddMeToEnquiries);
            if (dontAllowOthersOption != null && currentDontAllowOthersToAddMeToEnquiries != dontAllowOthersOption.Selected)
            {
                //update user preference for DoNotAllowOthersToAddMeToEnquiries
                var dontAllowOthersRow = await _db.UserSettings.FirstOrDefaultAsync(x => x.UserId == user.Id && x.Key == UserSetting.Keys.DoNotAllowOthersToAddMeToEnquiries);
                if (dontAllowOthersRow != null)
                {
                    dontAllowOthersRow.Value = dontAllowOthersOption.Selected.ToString();
                }
                else
                {
                    _db.UserSettings.Add(new UserSetting
                    {
                        UserId = user.Id,
                        Key = UserSetting.Keys.DoNotAllowOthersToAddMeToEnquiries,
                        Value = dontAllowOthersOption.Selected.ToString()
                    });
                }
                await _db.SaveChangesAsync();
            }

            return MyPreferences();
        }

        [HttpPost]
        public async Task<ActionResult> SubmitMyNotifications(MyNotificationsViewModel model)
        {
            var checkedCommonSubscriptions = GetCheckedSubscriptions(model, NotifyMeType.CommonSubscription);
            var checkedRigFilterSubscriptions = GetCheckedSubscriptions(model, NotifyMeType.RigFilters);
            var checkedEnquiryTypeFilterSubscriptions = GetCheckedSubscriptions(model, NotifyMeType.EnquiryTypeFilters);
            //get current subscriptions for user
            var user = HttpContext.GetCurrentUser();
            var dbUser = await _db.Users
                .Include(x => x.SubscriptionSettings)
                .Include(x => x.SubscriptionRigFilters)
                .Include(x => x.SubscriptionEnquiryTypeFilters)
                .FirstOrDefaultAsync(x => x.Id == user.Id);
            var deletedCommonSubscriptions = dbUser.SubscriptionSettings.Where(s =>
                    checkedCommonSubscriptions.All(x => (int)s.SubscriptionType != int.Parse(x.OptionValue)))
                .Select(x => x);
            foreach (var deleted in deletedCommonSubscriptions)
            {
                var itemToDelete = dbUser.SubscriptionSettings.FirstOrDefault(x => x.SubscriptionType == deleted.SubscriptionType);
                var dbItem = await _db.Subscription_UserSettings.FirstOrDefaultAsync(x => x.Id == itemToDelete.Id);
                _db.Subscription_UserSettings.Remove(dbItem);
            }
            var newCommonSubscriptions = checkedCommonSubscriptions
                .Where(x => dbUser.SubscriptionSettings.All(s => (int)s.SubscriptionType != int.Parse(x.OptionValue)))
                .Select(x => x);
            foreach (var newCommon in newCommonSubscriptions)
            {
                _db.Subscription_UserSettings.Add(new Core.Entities.Subscription.UserSetting
                {
                    UserId = dbUser.Id,
                    SubscriptionType = (SubscriptionType)int.Parse(newCommon.OptionValue),
                    Created = DateTime.UtcNow
                });
            }

            var deletedRigFilterSubscriptions = dbUser.SubscriptionRigFilters.Where(s =>
                    checkedCommonSubscriptions.All(x => s.RigId != int.Parse(x.OptionValue)))
                .Select(x => x);
            foreach (var deleted in deletedRigFilterSubscriptions)
            {
                var itemToDelete = dbUser.SubscriptionRigFilters.FirstOrDefault(x => x.RigId == deleted.RigId);
                var dbItem = await _db.Subscription_RigFilters.FirstOrDefaultAsync(x => x.Id == itemToDelete.Id);
                _db.Subscription_RigFilters.Remove(dbItem);
            }
            var newRigFilterSubscriptions = checkedRigFilterSubscriptions
                .Where(x => dbUser.SubscriptionRigFilters.All(s => s.RigId != int.Parse(x.OptionValue)))
                .Select(x => x);
            foreach (var newRigFilter in newRigFilterSubscriptions)
            {
                _db.Subscription_RigFilters.Add(new RigFilter
                {
                    UserId = dbUser.Id,
                    RigId = int.Parse(newRigFilter.OptionValue),
                    Created = DateTime.UtcNow
                });
            }

            var deletedEnquiryTypeFilterSubscriptions = dbUser.SubscriptionEnquiryTypeFilters.Where(s =>
                    checkedEnquiryTypeFilterSubscriptions.All(x => (int)s.Type != int.Parse(x.OptionValue)))
                .Select(x => x);
            foreach (var deleted in deletedEnquiryTypeFilterSubscriptions)
            {
                var itemToDelete = dbUser.SubscriptionEnquiryTypeFilters.FirstOrDefault(x => x.Type == deleted.Type);
                var dbItem = await _db.Subscription_EnquiryTypeFilters.FirstOrDefaultAsync(x => x.Id == itemToDelete.Id);
                _db.Subscription_EnquiryTypeFilters.Remove(dbItem);
            }
            var newEnquiryTypeFilterSubscriptions = checkedEnquiryTypeFilterSubscriptions
                .Where(x => dbUser.SubscriptionEnquiryTypeFilters.All(s => (int)s.Type != int.Parse(x.OptionValue)))
                .Select(x => x);
            foreach (var newEnquiryTypeFilter in newEnquiryTypeFilterSubscriptions)
            {
                _db.Subscription_EnquiryTypeFilters.Add(new EnquiryTypeFilter
                {
                    UserId = dbUser.Id,
                    Type = (TypeInfo.Type)int.Parse(newEnquiryTypeFilter.OptionValue),
                    Created = DateTime.UtcNow
                });
            }

            await _db.SaveChangesAsync();

            return MyNotifications();
        }

        private List<NotificationOption> GetCheckedSubscriptions(MyNotificationsViewModel model, NotifyMeType notifyMeType)
        {
            var checkedOptions = new List<NotificationOption>();
            var allNotificationOptions = model.NotificationOptions;
            if (model.NotificationOptions.Count == 0)
                return checkedOptions;

            foreach (var notifOption in allNotificationOptions)
            {
                if(notifOption.NotifyMeType == notifyMeType && notifOption.Selected)
                    checkedOptions.Add(notifOption);
                if (notifOption.Options.Count > 0)
                {
                    checkedOptions.AddRange(notifOption.Options.Where(x => x.NotifyMeType == notifyMeType && x.Selected).Select(x => x));
                }
            }
            
            return checkedOptions;
        }
    }
}