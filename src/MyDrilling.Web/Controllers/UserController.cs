﻿using Microsoft.AspNetCore.Mvc;
using MyDrilling.Web.Models.User;

namespace MyDrilling.Web.Controllers
{
    public class UserController : Controller
    {
        [HttpGet("FindUsersDialog")]
        public IActionResult FindUsersDialog(string findUsersUrl, string title, string eventName, bool singleUserMode)
        {
            return View(new FindUsersDialogViewModel(findUsersUrl, title, eventName, singleUserMode));
        }
    }
}
