﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Web.Models;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        
        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            var drillDown = HttpContext.GetDrillDownParameters();
            var routeValues = drillDown.ToRouteValueDictionary();
            if (drillDown.HasEquipment)
                return RedirectToAction("Detail", "Equipment", routeValues);
            if (drillDown.HasProductCode)
                return RedirectToAction("Type", "Equipment", routeValues);
            if (drillDown.HasRig)
                return RedirectToAction("Index", "Rigs", new { rig = drillDown.Rig });

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult NoAccess()
        {
            return View();
        }

        public IActionResult ContentNotFound()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult AppNoAccess()
        {
            return View();
        }

        [AllowAnonymous]
        public IActionResult IsSessionAlive()
        {
            return !User.Identity.IsAuthenticated 
                ? StatusCode(408, new { ok = false, message = "Session expired. Page will be reloaded." }) 
                : StatusCode(200, new { ok = true, message = "Session is alive." });
        }

        [AllowAnonymous]
        public IActionResult SessionExpired()
        {
            return Json(new { ok = false, message = "Session expired. Page will be reloaded." });
        }

        [AllowAnonymous]
        public ActionResult TermsAndConditions()
        {
            return View("TermsAndConditions");
        }

        [AllowAnonymous]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
