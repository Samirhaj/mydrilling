﻿using System.Threading.Tasks;
using Azure.Storage.Sas;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core.Entities.AsyncOperation;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.Storage;
using MyDrilling.Infrastructure.Storage.Blob;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Controllers
{
    [Route("asyncoperations")]
    public class AsyncOperationController : Controller
    {
        private readonly MyDrillingDb _db;
        private readonly IBlobSasGenerator _blobSasGenerator;

        public AsyncOperationController(MyDrillingDb db,
            IBlobSasGenerator blobSasGenerator)
        {
            _db = db;
            _blobSasGenerator = blobSasGenerator;
        }

        [HttpPost("download/{requestId}")]
        public async Task<IActionResult> Download(long requestId)
        {
            if (requestId <= 0)
            {
                return BadRequest($"Invalid {nameof(requestId)}");
            }

            var request = await _db.AsyncOperations.FirstOrDefaultAsync(r => r.Id == requestId);
            if(request == null)
            {
                return NotFound();
            }

            if (request.UserId != HttpContext.GetCurrentUser().Id)
            {
                return Unauthorized("You don't have permissions");
            }

            if (request.Status == AsyncOperationStatus.Requested)
            {
                return Ok();
            }

            if (request.Status == AsyncOperationStatus.Succeeded)
            {
                var blobUri = await _blobSasGenerator.GetSasBlobUri(request.Response, StorageConfig.TemporaryDocumentsContainerName, BlobAccountSasPermissions.Read);
                return StatusCode(200, blobUri);
            }

            return StatusCode(500, request.Response);
        }
    }
}
