﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Web.Models.Enquiries;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Controllers
{
    public partial class EnquiriesController
    {
        /// <summary>
        /// This action is needed to support old global identifiers.
        /// </summary>
        /// <param name="gid">Enquiry global object identifier</param>
        [HttpGet("detail")]
        public async Task<IActionResult> Detail([FromQuery] string gid)
        {
            var enquiryDetailsQuery = long.TryParse(gid, out var id)
                ? _db.Enquiry_Enquiries.Where(x => x.Id == id)
                : _db.Enquiry_Enquiries.Where(x => x.OldGlobalId == gid);

            var enquiryDetails = await enquiryDetailsQuery
                .Select(x => new { x.Id, rig = x.Rig.ReferenceId })
                .FirstOrDefaultAsync();
            if (enquiryDetails == default)
            {
                return NotFound();
            }

            return RedirectToAction(nameof(Detail), enquiryDetails);
        }

        [HttpGet("detail/{id}")]
        public async Task<IActionResult> Detail(long id)
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries
                    //these includes are required to get involved users
                .Include(x => x.Comments)
                .Include(x => x.Attachments)
                .Include(x => x.StateLogs)
                .AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            if (!enquiry.CanRead(roles))
            {
                return RedirectToAction("NoAccess", "Home");
            }

            if (enquiry.Type == TypeInfo.Type.Internal_Internal)
            {
                return RedirectToAction("Index", HttpContext.GetDrillDownParameters().ToRouteValueDictionary());
            }

            //add read ack
            if (!await _db.Enquiry_ReadReceipts.AnyAsync(x => x.EnquiryId == id && x.UserId == user.Id))
            {
                try
                {
                    _db.Enquiry_ReadReceipts.Add(new ReadReceipt
                    {
                        UserId = user.Id,
                        EnquiryId = id,
                        Created = DateTime.UtcNow
                    });
                    await _db.SaveChangesAsync();
                }
                catch (DbUpdateException)
                {
                    //ignore unique key violation
                }
            }

            var model = await _db.Enquiry_Enquiries.AsNoTracking()
                .Select(enq => new DetailViewModel
                {
                    Id = enq.Id,
                    ReferenceId = enq.ReferenceId,
                    Title = enq.Title,
                    Description = enq.Description,
                    Type = enq.Type,
                    State = enq.State,
                    CreatedDateTime = enq.Created,
                    AssigneeFirstName = enq.AssignedTo.FirstName,
                    AssigneeLastName = enq.AssignedTo.LastName,
                    RootCustomerName = enq.RootCustomer.Name,
                    RigId = enq.Rig.ReferenceId,
                    RigName = enq.Rig.Name.ToCamelCaseWithRomanNumerals(),
                    EquipmentId = enq.EquipmentId,
                    EquipmentName = enq.Equipment.Name.ToCamelCase(),
                    Version = enq.Version,
                    Comments = enq.Comments.Where(c => !c.IsInitial).Select(c => new DetailViewModel.CommentViewModel
                    {
                        Id = c.Id,
                        Text = c.Text,
                        CreatedByUserId = c.CreatedById,
                        CreatedByUserFirstName = c.CreatedBy.FirstName,
                        CreatedByUserLastName = c.CreatedBy.LastName,
                        CreatedByUserCustomerName = c.CreatedBy.RootCustomer.Name,
                        CreatedDateTime = c.Created
                    }).ToArray(),
                    Files = enq.Attachments.Select(f => new DetailViewModel.FileViewModel
                    {
                        Id = f.Id,
                        FileName = f.FileName,
                        Created = f.Created,
                        FileExtension = f.FileExtension,
                        CreatedByUserId = f.CreatedById,
                        CreatedByUserFirstName = f.CreatedBy.FirstName,
                        CreatedByUserLastName = f.CreatedBy.LastName,
                        CommentId = f.CommentId
                    }).ToArray(),
                    StateLogs = enq.StateLogs.Select(s => new DetailViewModel.StateLogViewModel
                    {
                        State = s.State,
                        ChangedByUserId = s.ChangedById,
                        ChangedBy = s.ChangedBy.Upn,
                        ChangedDateTime = s.Changed
                    }).ToArray(),
                    AddedPersonnel = enq.AddedUsers.Select(au => new DetailViewModel.AddedPersonnelItemViewModel
                    {
                        UserId = au.UserId,
                        Upn = au.User.Upn,
                        FirstName = au.User.FirstName,
                        LastName = au.User.LastName,
                        Position = au.User.Position,
                        CustomerName = au.User.RootCustomer.Name
                    }).ToList()
                }).FirstOrDefaultAsync(x => x.Id == id);

            var involvedUserIds = enquiry.GetInvolvedUserIds();
            model.InvolvedPersonnel = await _db.Users.Where(x => involvedUserIds.Contains(x.Id))
                .Select(x => new DetailViewModel.InvolvedPersonnelItemViewModel
                {
                    UserId = x.Id,
                    Upn = x.Upn,
                    FirstName = x.FirstName,
                    LastName = x.LastName,
                    Position = x.Position,
                    CustomerName = x.RootCustomer.Name
                }).ToListAsync();
            model.AddedPersonnel.RemoveAll(x => involvedUserIds.Contains(x.UserId));
            foreach (var addedPersonnelItem in model.AddedPersonnel)
            {
                addedPersonnelItem.CanBeDeleted = addedPersonnelItem.UserId == user.Id;
            }

            model.CommentsById = model.Comments.ToDictionary(x => x.Id, x => x);
            model.CommentsToFiles = model.Files.Where(f => f.CommentId.HasValue).GroupBy(f => f.CommentId.Value)
                .ToDictionary(fg => fg.Key, fg => fg.Select(f => f.Id).ToArray());
            model.CanAddComment = enquiry.CanAddComment(roles);
            model.CanDeleteComment = enquiry.CanDeleteComment(roles);
            model.CanSendForApproval = enquiry.CanSendForApproval(roles);
            model.CanAssignToCustomer = enquiry.CanAssignToCustomer(roles);
            model.CanAssignToProvider = enquiry.CanAssignToProvider(roles);
            model.CanSetAssignee = enquiry.CanSetAssignee(roles);
            model.CanAddPersonnel = enquiry.CanAddPersonnel(roles);
            model.CanDeletePersonnel = enquiry.CanDeletePersonnel(roles);
            model.CanRemoveAssignee = enquiry.CanRemoveAssignee(roles);
            model.CanSuggestToClose = enquiry.CanSuggestToClose(roles);
            model.CanApprove = enquiry.CanApprove(roles);
            model.CanReject = enquiry.CanReject(roles);
            model.CanEdit = enquiry.CanEdit(roles);
            model.CanDelete = enquiry.CanDelete(roles);
            model.CanDeleteAttachment = enquiry.CanDeleteAttachment(roles);
            model.CanReopen = enquiry.CanReopen(roles);

            return View(model);
        }
    }
}
