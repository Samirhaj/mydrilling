﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Controllers
{
    public class DrillPerformController : Controller
    {
        private readonly IConfiguration _config;

        public DrillPerformController(IConfiguration config)
        {
            _config = config;
        }

        public IActionResult Index()
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.HasDrillPerformAccess())
            {
                return RedirectToAction("NoAccess", "Home");
            }

            var drillPerformUrl = _config.GetValue<string>("ExternalUrls:DrillPerformUrl");
            return View("Index", drillPerformUrl);
        }
    }
}