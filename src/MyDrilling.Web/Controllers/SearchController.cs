﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Azure.Search.Documents;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Bulletin;
using MyDrilling.Core.Entities.Documentation;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Core.Entities.Rig;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.Services.Search;
using MyDrilling.Web.Models.Search;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Controllers
{
    public class SearchController : Controller
    {
        private readonly ILogger<SearchController> _logger;
        private readonly SearchService _searchService;
        private readonly MyDrillingDb _db;

        public SearchController(ILogger<SearchController> logger, IConfiguration config, MyDrillingDb db)
        {
            _logger = logger;
            var searchServiceName = config.GetValue<string>("SearchServiceName");
            var searchApiKey = config.GetValue<string>("SearchApiKey");
            _searchService = new SearchService(searchServiceName, searchApiKey);
            _db = db;
        }
        public IActionResult Index(string term)
        {
            var advancedSearchModel = new AdvancedSearchViewModel
            {
                SearchTerm = term,
                IncludeBulletins = true,
                IncludeEnquiries = true,
                IncludeDocumentation = true
            };
            return View("Index", advancedSearchModel);
        }


        public async Task<IActionResult> AdvancedSearch(string term, bool inclBulletin, bool inclEnquiry, bool inclDocumentation)
        {
            var advSearchModel = new AdvancedSearchViewModel
            {
                SearchTerm = term,
                IncludeBulletins = inclBulletin,
                IncludeEnquiries = inclEnquiry,
                IncludeDocumentation = inclDocumentation
            };
            if (string.IsNullOrEmpty(term) || !inclEnquiry && !inclBulletin && !inclDocumentation)
            {
                advSearchModel.SearchItems = null;
                advSearchModel.TotalCount = null;
                return View("Index", advSearchModel);
            }
            var drilldown = HttpContext.GetDrillDownParameters();
            var pager = HttpContext.GetPagerParameters();
            var roles = HttpContext.GetCurrentUserRoles();
            var searchClient = _searchService.GetSearchClient();
           
            var filterList = new List<string>();
            if (inclEnquiry)
            {
                var enquiryHandlingApprovedViewerRigs = roles.GetEnquiryHandlingApprovedViewerRigs();
                var enquiryHandlingPreparationViewerRigs = roles.GetEnquiryHandlingPreparationViewerRigs();
                var enquiryViewerRigs = enquiryHandlingApprovedViewerRigs.Concat(enquiryHandlingPreparationViewerRigs).Distinct().ToArray();
                if (drilldown.HasRig)
                {
                    if (enquiryViewerRigs.Contains(drilldown.RigId))
                    {
                        filterList.Add($"EntityType eq '{nameof(Enquiry)}' and RigId eq '{drilldown.RigId}'");
                    }
                }
                else if (enquiryViewerRigs.Length > 0)
                    filterList.Add($"EntityType eq '{nameof(Enquiry)}' and search.in(RigId, '{string.Join(", ", enquiryViewerRigs)}')");
            }

            if (inclBulletin)
            {
                var bulletinViewerRigs = roles.GetBulletinViewerAccessToRigs();
                if (drilldown.HasRig)
                {
                    if (bulletinViewerRigs.Contains(drilldown.RigId))
                    {
                        filterList.Add($"EntityType eq '{nameof(ZaBulletin)}' and RigId eq '{drilldown.RigId}'");
                    }
                }
                else if (bulletinViewerRigs.Length > 0)
                    filterList.Add($"EntityType eq '{nameof(ZaBulletin)}' and search.in(RigId, '{string.Join(", ", bulletinViewerRigs)}')");
            }

            if (inclDocumentation)
            {
                var documentationViewerRigs = roles.GetDocumentationViewerAccessToRigs();
                if (drilldown.HasRig)
                {
                    if (documentationViewerRigs.Contains(drilldown.RigId))
                    {
                        filterList.Add($"search.in(EntityType, '{nameof(TechnicalReport)}, {nameof(UserManual)}') and RigId eq '{drilldown.RigId}'");
                    }
                }
                else if (documentationViewerRigs.Length > 0)
                    filterList.Add($"search.in(EntityType, '{nameof(TechnicalReport)}, {nameof(UserManual)}') and search.in(RigId, '{string.Join(", ", documentationViewerRigs)}')");
            }

            var filterContent = string.Join(" or ", filterList);
            var options = new SearchOptions
            {
                Select = { "EntityId", "EntityType", "ReferenceId", "Name", "RigId" },
                Filter = filterContent,
                IncludeTotalCount = true,
                Size = pager.PageSize,
                Skip = (pager.CurrentPageNumber - 1) * pager.PageSize
            };
            var searchResponse = await searchClient.SearchAsync<SearchItem>(term, options).ConfigureAwait(false);
            advSearchModel.TotalCount = searchResponse.Value.TotalCount;
            var results = searchResponse.Value.GetResults().ToArray();
            if (results.Length > 0)
            {
                var rigs = await _db.Rigs.ApplyPermission(roles).ToArrayAsync();
                advSearchModel.SearchItems = results.Select(res => ToSearchItemViewModel(res.Document, rigs)).ToArray();
            }
            else
            {
                advSearchModel.TotalCount = 0;
                advSearchModel.SearchItems = null;
            }
            return View("Index", advSearchModel);

        }

        [HttpPost]
        public IActionResult SubmitSearch(AdvancedSearchViewModel advSearchModel)
        {
            var pager = HttpContext.GetPagerParameters();
            var searchParams = new SearchParameters(advSearchModel.SearchTerm, advSearchModel.IncludeEnquiries, advSearchModel.IncludeBulletins, advSearchModel.IncludeDocumentation);
            return RedirectToAction("AdvancedSearch", HttpContext.GetDrillDownParameters().ToRouteValueDictionary(searchParams, pager, new { page = 1 }));
        }

        [HttpGet("globalsearch")]
        public async Task<IActionResult> GlobalSearch(string term, int limit = 10)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            term = term.ToLower();
            var searchClient = _searchService.GetSearchClient();

            var filterList = new List<string>();
            var documentationViewerRigs = roles.GetDocumentationViewerAccessToRigs();
            if(documentationViewerRigs.Length > 0)
                filterList.Add($"search.in(EntityType, '{nameof(TechnicalReport)}, {nameof(UserManual)}') and search.in(RigId, '{string.Join(", ", documentationViewerRigs)}')");
            var bulletinViewerRigs = roles.GetBulletinViewerAccessToRigs();
            if(bulletinViewerRigs.Length > 0)
                filterList.Add($"EntityType eq '{nameof(ZaBulletin)}' and search.in(RigId, '{string.Join(", ", bulletinViewerRigs)}')");
            var enquiryHandlingApprovedViewerRigs = roles.GetEnquiryHandlingApprovedViewerRigs();
            var enquiryHandlingPreparationViewerRigs = roles.GetEnquiryHandlingPreparationViewerRigs();
            var enquiryViewerRigs = enquiryHandlingApprovedViewerRigs.Concat(enquiryHandlingPreparationViewerRigs).Distinct().ToArray();
            if (enquiryViewerRigs.Length > 0)
            {
                filterList.Add($"EntityType eq '{nameof(Enquiry)}' and search.in(RigId, '{string.Join(", ", enquiryViewerRigs)}')");
            }
            var filterContent = string.Join(" or ", filterList);
            var options = new SuggestOptions
            {
                Size = limit, 
                Select = {"EntityId", "EntityType", "ReferenceId", "Name", "RigId" },
                Filter = filterContent
            };
            var searchResult = await searchClient.SuggestAsync<SearchItem>(term, _searchService.SuggesterName, options).ConfigureAwait(false);
            var rigs = await _db.Rigs.ApplyPermission(roles).ToArrayAsync();
            var results = searchResult.Value.Results.Select(res => ToSearchItemViewModel(res.Document, rigs)).ToArray();
            return Json(results);
        }
        private string GetItemUrl(long itemId, string itemType, string rigId)
        {
            if (string.IsNullOrEmpty(itemType) || itemId <= 0) return "";
            var url = "";
            switch (itemType)
            {
                case nameof(Enquiry):
                    url = $"/enquiries/detail/{itemId}";
                    break;
                case nameof(ZaBulletin):
                    url = $"/bulletins/detail?id={itemId}&rigId={rigId}";
                    break;
                case nameof(TechnicalReport):
                    url = $"/documentation/requestdownload?id={itemId}&docType={DocumentType.TechnicalReports}";
                    break;
                case nameof(UserManual):
                    url = $"/documentation/usermanualdetail/{itemId}";
                    break;
            }
            return url;
        }

        private SearchItemViewModel ToSearchItemViewModel(SearchItem item, Rig[] rigs)
        {
            var rig = rigs.FirstOrDefault(x => x.Id == long.Parse(item.RigId));
            return new SearchItemViewModel
            {
                Name = item.Name,
                ReferenceId = item.ReferenceId,
                EntityType = item.EntityType,
                Url = GetItemUrl(item.EntityId, item.EntityType, item.RigId),
                RigName = rig != null ? rig.NameFormatted : item.RigId
            };
        }
    }
}