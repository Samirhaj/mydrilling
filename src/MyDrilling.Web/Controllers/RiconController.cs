﻿using System;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core;
using MyDrilling.Core.Entities.Ricon;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.Services.AsyncOperation;
using MyDrilling.Infrastructure.Storage.Queue;
using MyDrilling.Web.Helpers;
using MyDrilling.Web.Models.Ricon;
using MyDrilling.Web.Models.Ricon.Excel;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;
using Document = MyDrilling.Core.Entities.Ricon.Document;

namespace MyDrilling.Web.Controllers
{
    [Route("Ricon")]
    [Route("Cbm")]
    public class RiconController : Controller
    {
        private readonly MyDrillingDb _db;
        private IWebHostEnvironment _env;
        private readonly string[] cocValues = {"-", "CoC"};
        private readonly string[] statusValues = {"Concern", "Not OK", "OK"};
        private readonly IQueueSender _queueSender;
        
        public RiconController(MyDrillingDb db, 
            IWebHostEnvironment env, 
            IQueueSender queueSender)
        {
            _db = db;
            _env = env;
            _queueSender = queueSender;
        }

        [HttpGet("")]
        [HttpGet("index")]
        public async Task<IActionResult> Index() {

            var drillDowns = HttpContext.GetDrillDownParameters();

            var jointsQuery = _db.Ricon_Joints
                .ApplyPermissions(HttpContext.GetCurrentUserRoles())
                .ApplyDrillDown(drillDowns)
                .ApplyFilter(HttpContext.GetFilterParameters())
                .Where(x => !x.IsDeleted);

            var totalCount = await jointsQuery.CountAsync();

            var joints = jointsQuery
                .ApplyOrder(HttpContext.GetOrderParameters())
                .ApplyPager(HttpContext.GetPagerParameters());

            var documents = _db.Ricon_Documents.Where(x => joints.Any(j => j.Id == x.JointId) && !x.IsDeleted);
            var modelJoints = await joints.Select(ToJointViewModel(documents)).ToArrayAsync();

            var model = new JointItemsViewModel
            {
                Menu = GetMenuViewModel(nameof(Index), drillDowns),
                Items = modelJoints,
                TotalCount = totalCount
            };

            AddFilterData();

            return View("Index", model);
        }

        private static Expression<Func<Joint, JointItemViewModel>> ToJointViewModel(IQueryable<Document> documents)
        {
            return j => new JointItemViewModel
            {
                Id = j.Id,
                ReferenceId = j.ReferenceId.ToString(),
                RigReferenceId = j.RootEquipment.Rig.ReferenceId.ToString(),
                TagNumber = j.TagNumber,
                Fatigue = FormatFatigueValue(j.Fatigue),
                Coc = j.Coc,
                Status = j.Status,
                LastWallThicknessInspection = j.LastWallThicknessInspection.HasValue ? j.LastWallThicknessInspection.Value.ToString(HtmlExt.StdDateFormat, CultureInfo.InvariantCulture) : "",
                LastVisualInspection = j.LastVisualInspection.HasValue ? j.LastVisualInspection.Value.ToString(HtmlExt.StdDateFormat, CultureInfo.InvariantCulture) : "",
                NumberOfDocuments = documents == null ? 0 : documents.Count(d => d.JointId == j.Id)
            };
        }

        [HttpGet("documents")]
        public async Task<IActionResult> Documents([FromQuery] string eqId)
        {
            var drillDowns = HttpContext.GetDrillDownParameters();
            var roles = HttpContext.GetCurrentUserRoles();
            
            if (!string.IsNullOrEmpty(eqId) && long.TryParse(eqId, out long eqReference))
            {
                var equipmentDetails = await _db.Ricon_Joints
                    .ApplyPermissions(roles)
                    .FirstOrDefaultAsync(x => x.ReferenceId == eqReference && !x.IsDeleted);
                if (equipmentDetails == default)
                {
                    return RedirectToAction("ContentNotFound", "Home");
                }
                //return RedirectToAction(nameof(Documents), new RouteValueDictionary(new { rig = drillDowns.HasRig ? drillDowns.Rig : null, equipment = equipmentDetails.ReferenceId }));
                return View("Documents", await PrepareDocumentsViewModel(nameof(Documents), equipmentDetails));

            }
            return View("Documents", await PrepareDocumentsViewModel(nameof(Documents), null));
        }

        private void AddFilterData()
        {
            ViewData[JointItemsViewModel.CocFilter] = cocValues
                .Select(x => new SelectListItem(x, x))
                .OrderBy(x => x.Text)
                .ToArray();

            ViewData[JointItemsViewModel.StatusFilter] = statusValues
                .Select(x => new SelectListItem(x,x))
                .OrderBy(x => x.Text)
                .ToArray();
        }

        private async Task<RiserDocumentsViewModel> PrepareDocumentsViewModel(string currentActionName, Joint selectedJoint)
        {
            string headerContent = "Documents";
            var drillDown = HttpContext.GetDrillDownParameters();
            if (!drillDown.HasRig && selectedJoint == null)
            {
                return new RiserDocumentsViewModel
                {
                    HeaderContent = headerContent,
                    Menu = GetMenuViewModel(currentActionName, drillDown),
                    Items = new RiserDocumentViewModel[0],
                    TotalCount = 0
                };
            }
            var documentsQuery = _db.Ricon_Documents
                .ApplyPermissions(HttpContext.GetCurrentUserRoles())
                .ApplyDrillDown(drillDown, selectedJoint?.Id ?? null)
                .Where(x => !x.IsDeleted);

            var totalCount = await documentsQuery.CountAsync();

            var documents = await documentsQuery
                .Select(d => new RiserDocumentViewModel
                {
                    Id = d.Id,
                    ReferenceId = d.ReferenceId.ToString(),
                    Description = d.Description,
                    Version = d.Version,
                    PublishedDate = d.PublishedDate.ToString(HtmlExt.StdDateFormat, CultureInfo.InvariantCulture),
                    Url = d.Url
                })
                .ToArrayAsync();
            
            if (selectedJoint != null)
            {
                headerContent = $"Riser CBM documents for equipment {selectedJoint.ReferenceId}";
            } 
            else if (drillDown.HasRig)
            {
                var rig = _db.Rigs.FirstOrDefault(r => r.Id == drillDown.RigId);
                if(rig != null)
                    headerContent = $"Generic documents for {rig.NameFormatted}";
            }
            var model = new RiserDocumentsViewModel
            {
                HeaderContent = headerContent,
                Menu = GetMenuViewModel(currentActionName, drillDown),
                Items = documents,
                TotalCount = totalCount
            };

            return model;
        }

        [HttpGet("getcocpdf")]
        public async Task<IActionResult> ExportCocStatuses()
        {

            var drillDowns = HttpContext.GetDrillDownParameters();
            var roles = HttpContext.GetCurrentUserRoles();
            if (drillDowns.HasRig)
            {
                var rig = await _db.Rigs.FirstOrDefaultAsync(r => r.Id == drillDowns.RigId);
                if (rig == null)
                    return NoContent();
                var rigJoints = await _db.Ricon_Joints
                    .ApplyPermissions(roles)
                    .ApplyDrillDown(drillDowns)
                    .Where(x => x.RootEquipment.RigId == drillDowns.RigId && !x.IsDeleted).ToListAsync();
                return File(RiconPdfCreator.ExportCbmRiserToPdf(rigJoints, rig, _env.WebRootPath), "application/pdf", $"RiCon CoC - {rig.ReferenceId}.pdf");
            }
            return NoContent();
        }

        [HttpGet("exporttoexcel")]
        public async Task<IActionResult> ExportToExcel()
        {
            var drillDowns = HttpContext.GetDrillDownParameters();
            var roles = HttpContext.GetCurrentUserRoles();
            var rigJoints = await _db.Ricon_Joints
                .Include(x => x.RootEquipment).ThenInclude(x => x.Rig)
                .ApplyPermissions(roles)
                .ApplyDrillDown(drillDowns)
                .ApplyFilter(HttpContext.GetFilterParameters())
                .ApplyOrder(HttpContext.GetOrderParameters())
                .Where(x => !x.IsDeleted)
                .Select(ToJointViewModel(null))
                .ToArrayAsync();
            var rigInfo = drillDowns.HasRig ? $" - Rig {drillDowns.Rig.ToLongReferenceId()} " : "";
            return File(
                new RiconExcelListGenerator().CreateExcelDocument(rigJoints),
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                $"RiserJoints{rigInfo}- {DateTime.UtcNow.ToString(HtmlExt.StdDateFormat, CultureInfo.InvariantCulture)}.xlsx");
        }

        [HttpPost("requestdownload")]
        public async Task<IActionResult> RequestDownload(long id)
        {
            if (id <= 0)
            {
                return StatusCode(400, "Missing data.");
            }
            var roles = HttpContext.GetCurrentUserRoles();
            string fileUrl = "";
            string fileName = "";

            var riconDoc = await _db.Ricon_Documents.ApplyPermissions(roles).FirstOrDefaultAsync(x => x.Id == id && !x.IsDeleted);
            if (riconDoc == null || riconDoc.ReferenceId == 0 || string.IsNullOrEmpty(riconDoc.Url))
                return NotFound();
            fileUrl = riconDoc.Url;
            fileName = $"{riconDoc.ReferenceId}.pdf";

            if (string.IsNullOrEmpty(fileUrl) || string.IsNullOrEmpty(fileName))
                return StatusCode(404, "File not found or you don't have permissions.");
           
            var requestId = await _db.CreateNewAsyncOperation(HttpContext.GetCurrentUser());
            var docUrlParts = fileUrl.Split("|");
            if (docUrlParts.Length < 5)
            {
                return StatusCode(400, "Missing data.");
            }
            await _queueSender.EnqueueFileRequestAsync(requestId, $"ricon-documents/{fileName}", docUrlParts[2], docUrlParts[1], docUrlParts[3], docUrlParts[4], true);

            return StatusCode(200, requestId);
        }

        private MenuViewModel GetMenuViewModel(string currentActionName, DrillDownParameters drillDowns)
        {
            int numberOfRigDocuments = 0;
            if (drillDowns.HasRig)
            {
                numberOfRigDocuments = _db.Ricon_Documents
                    .Count(x => x.RootEquipment != null 
                                && x.RootEquipment.Rig != null 
                                && x.RootEquipment.RigId == drillDowns.RigId
                                && !x.IsDeleted);
            }

            var menuModel = new MenuViewModel
            {
                CurrentActionName = currentActionName,
                NumberOfRigDocuments = numberOfRigDocuments
            };
            return menuModel;
        }

        private static string FormatFatigueValue(string fatigueV)
        {
            if (decimal.TryParse(fatigueV, NumberStyles.AllowDecimalPoint, CultureInfo.CreateSpecificCulture("nb-NO"), out var fatigueValue))
            {
                fatigueValue = decimal.Round(fatigueValue, 1);
                return fatigueValue > 0 ? fatigueValue.ToString(CultureInfo.InvariantCulture) : "";
            }
            return "";
        }
    }

    public static class RiconFilterExt
    {
        public static IQueryable<Joint> ApplyDrillDown(this IQueryable<Joint> query, DrillDownParameters drillDown)
        {
            return drillDown.HasRig ? query.Where(j => j.RootEquipment.RigId == drillDown.RigId) : query;
        }
        public static IQueryable<Document> ApplyDrillDown(this IQueryable<Document> query, DrillDownParameters drillDown, long? eqId)
        {
            var newQuery = query;
            if (eqId.HasValue)
                newQuery = newQuery.Where(d => d.JointId ==  eqId.Value);
            else if (drillDown.HasRig)
                newQuery = newQuery.Where(d => d.RootEquipment.RigId == drillDown.RigId);
            return newQuery;
        }

        public static IQueryable<Joint> ApplyFilter(this IQueryable<Joint> query, FilterParameters filter)
        {
            if (filter.FilterValue == HtmlExt.AllFilterValue) return query;
            switch (filter.FilterField)
            {
                case JointItemsViewModel.CocFilter:
                    return query.Where(x => x.Coc == filter.FilterValue);
                case JointItemsViewModel.StatusFilter:
                    return query.Where(x => x.Status == filter.FilterValue);
                default:
                    return query;
            }
        }

        public static IOrderedQueryable<Joint> ApplyOrder(this IQueryable<Joint> query, SortParameters sort)
        {
            switch (sort.SortField)
            {
                case JointItemsViewModel.EquipmentSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.ReferenceId)
                        : query.OrderByDescending(b => b.ReferenceId);
                case JointItemsViewModel.JointSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.TagNumber)
                        : query.OrderByDescending(b => b.TagNumber);
                case JointItemsViewModel.FatigueSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.Fatigue)
                        : query.OrderByDescending(b => b.Fatigue);
                case JointItemsViewModel.CocSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.Coc)
                        : query.OrderByDescending(b => b.Coc);
                case JointItemsViewModel.StatusSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.Status)
                        : query.OrderByDescending(b => b.Status);
                case JointItemsViewModel.LastVisualInspectionSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.LastVisualInspection)
                        : query.OrderByDescending(b => b.LastVisualInspection);
                case JointItemsViewModel.LastWallThicknessInspectionSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.LastWallThicknessInspection)
                        : query.OrderByDescending(b => b.LastWallThicknessInspection);
                default:
                    return query.OrderByDescending(b => b.Id);
            }
        }

        public static IQueryable<Joint> ApplyPager(this IQueryable<Joint> query, PagerParameters pager)
        {
            return query
                .Skip(pager.PageSize * (pager.CurrentPageNumber - 1))
                .Take(pager.PageSize);
        }
    }
}
