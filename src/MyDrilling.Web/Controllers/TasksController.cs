﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Web.Models.Task;
using MyDrilling.Web.QueryParameters;
using MenuViewModel = MyDrilling.Web.Models.Task.MenuViewModel;
using MyDrilling.Core.Entities;

namespace MyDrilling.Web.Controllers
{
    [Route("Tasks")]
    public class TasksController : Controller
    {
        [HttpGet("")]
        [HttpGet("index")]
        public IActionResult Index()
        {
            return View(new MenuViewModel{ CurrentActionName = nameof(Index)});
        }
        
        [HttpGet("notifiedtasks")]
        public IActionResult NotifiedTasks()
        {
            return View(new MenuViewModel { CurrentActionName = nameof(NotifiedTasks) });
        }
    }

    public static class TaskFilterExt
    {
        public static IEnumerable<Enquiry> ApplyTaskPager(this IEnumerable<Enquiry> query, PagerParameters pager) =>
            query.Skip(pager.PageSize * (pager.CurrentPageNumber - 1))
                .Take(pager.PageSize);

        public static IQueryable<Enquiry> ApplyTaskFilter(this IQueryable<Enquiry> query, FilterParameters filter, User currentUser)
        {
            switch (filter.FilterField)
            {
                case TaskListViewModel.TaskTypeFilter:
                    var type = filter.FilterValue;
                    return query.Where(x => type != TaskInfoExt.MyTaskLabel || x.AssignedToId != null && x.AssignedToId == currentUser.Id);
                case TaskListViewModel.TaskActionFilter:
                    var action = filter.FilterValue;
                    return query.Where(x => action != TaskInfoExt.ApproveActionLabel || x.State == StateInfo.State.Draft);
                case TaskListViewModel.CategoryFilter:
                    var types = TypeInfo.ByCategory[filter.FilterValue].Select(x => x.Value).ToArray();
                    return query.Where(x => types.Contains(x.Type));
                default:
                    return query;
            }
        }

        public static IEnumerable<Enquiry> ApplyTaskOrder(this IEnumerable<Enquiry> query, SortParameters sort)
        {
            switch (sort.SortField)
            {
                case TaskListViewModel.RigSort:
                    return sort.IsAsc
                        ? query.OrderBy(x => x.Rig.Name)
                        : query.OrderByDescending(x => x.Rig.Name);
                case TaskListViewModel.TitleSort:
                    return sort.IsAsc
                        ? query.OrderBy(x => x.Title)
                        : query.OrderByDescending(x => x.Title);
                case TaskListViewModel.DateSort:
                    return sort.IsAsc
                        ? query.OrderBy(x => x.Date)
                        : query.OrderByDescending(x => x.Date);
                default:
                    return query.OrderByDescending(x => x.Date);
            }
        }
    }

    public static class TaskInfoExt
    {
        public static string MyTaskLabel = "myTask";
        public static string RigTaskLabel = "rigTask";
        public static string ApproveActionLabel = "Approve and submit";
        public static string WaitActionLabel = "Awaiting Response";

        public static TaskViewModel MapDraftToTaskItem(this Enquiry draft, long userId)
        {
            return new TaskViewModel
            {
                Id = draft.Id,
                Icon = GetTaskTypeName(draft, userId),
                Title = !string.IsNullOrEmpty(draft.Title) ? draft.Title : "No title",
                Date = draft.Created,
                RigId = draft.RigId,
                RigName = draft.Rig.NameFormatted,
                ReferenceNumber = "Draft",
                TaskAction = ApproveActionLabel,
                Url = "/enquiries/detail/" + draft.Id,
                TaskCategory = "Service",
                ItemType = "Enquiry"
            };
        }

        public static TaskViewModel MapEnquiryToTaskItem(this Enquiry enquiry, long userId)
        {
            return new TaskViewModel
            {
                Id = enquiry.Id,
                Icon = GetTaskTypeName(enquiry, userId),
                Title = !string.IsNullOrEmpty(enquiry.Title) ? enquiry.Title : "No title",
                Date = enquiry.Created,
                RigId = enquiry.RigId,
                RigName = enquiry.Rig.NameFormatted,
                ReferenceNumber = enquiry.ReferenceId,
                TaskAction = WaitActionLabel,
                Url = "/enquiries/detail/" + enquiry.Id,
                TaskCategory = "Service",
                ItemType = "Enquiry"
            };
        }

        private static string GetTaskTypeName(Enquiry enquiry, long userId)
        {
            return enquiry.AssignedToId != null && Equals(enquiry.AssignedToId, userId) ? MyTaskLabel : RigTaskLabel;
        }
    }
}