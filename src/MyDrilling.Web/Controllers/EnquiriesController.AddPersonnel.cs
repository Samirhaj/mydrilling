﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Core.Entities.Permission;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Controllers
{
    public partial class EnquiriesController
    {
        [HttpGet("findPersonnelToAdd")]
        public async Task<IActionResult> FindPersonnelToAdd(long id, string startsWith, int limit)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries.AsNoTracking().Where(x => x.Id == id).FirstAsync();
            if (!enquiry.CanAddPersonnel(roles))
            {
                return Unauthorized();
            }

            var personnelSearch = _db.Users.WithCustomerRole(RoleInfo.Role.CustomerAccess, enquiry.RootCustomerId)
                .WithRigRole(RoleInfo.Role.EnquiryHandlingApprovedViewer, enquiry.RigId);

            personnelSearch = string.IsNullOrEmpty(startsWith)
                ? personnelSearch
                : personnelSearch.Where(t => t.FirstName.StartsWith(startsWith) ||
                                             t.LastName.StartsWith(startsWith) ||
                                             t.Upn.StartsWith(startsWith) ||
                                             t.Position.StartsWith(startsWith));

            personnelSearch = personnelSearch.Take(limit == 0 ? 10 : limit);

            var personnelSearchResult = await personnelSearch.Select(x => new SelectListItem(x.Id.ToString(), $"{x.FirstName} {x.LastName}")).ToArrayAsync();
            return Json(personnelSearchResult.Where(x => !string.IsNullOrWhiteSpace(x.Value)));
        }

        [HttpPost("addPersonnel")]
        public async Task<IActionResult> AddPersonnel(long id, long[] userIds)
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries.FirstAsync(x => x.Id == id);
            if (!enquiry.CanAddPersonnel(roles))
            {
                return Unauthorized();
            }

            var personnelUsers = await _db.Users.Where(x => userIds.Contains(x.Id))
                .WithCustomerRole(RoleInfo.Role.CustomerAccess, enquiry.RootCustomerId)
                .WithRigRole(RoleInfo.Role.EnquiryHandlingApprovedViewer, enquiry.RigId)
                .Where(u => !u.Settings.Any(s => s.Key == UserSetting.Keys.DoNotAllowOthersToAddMeToEnquiries && s.Value == true.ToString()))
                .ToArrayAsync();

            foreach (var personnelUser in personnelUsers)
            {
                try
                {
                    enquiry.AddPersonnel(personnelUser, user);
                    await _db.SaveChangesAsync();
                }
                catch (DbUpdateException)
                {
                    //ignore unique key violation
                }
            }

            return Json(new {});
        }

        [HttpPost("deletePersonnel")]
        public async Task<IActionResult> DeletePersonnel(long id, long userId)
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries
                .Include(x => x.AddedUsers)
                .FirstAsync(x => x.Id == id);
            if (!enquiry.CanDeletePersonnel(roles) && user.Id != userId)
            {
                return Unauthorized();
            }

            var userToDelete = enquiry.AddedUsers.FirstOrDefault(x => x.UserId == userId);
            if (userToDelete != null)
            {
                _db.Enquiry_AddedUsers.Remove(userToDelete);
                await _db.SaveChangesAsync();
            }

            return RedirectToAction("Detail", HttpContext.GetDrillDownParameters().ToRouteValueDictionary(new { id }));
        }
    }
}
