﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.SharePoint.Client;
using MyDrilling.Core;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Documentation;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.Services.AsyncOperation;
using MyDrilling.Infrastructure.Storage.Blob;
using MyDrilling.Infrastructure.Storage.Queue;
using MyDrilling.Web.Lookups;
using MyDrilling.Web.Models.Documentation;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Controllers
{
    [Route("Documentation")]
    public class DocumentationController : Controller
    {
        private readonly MyDrillingDb _db;
        private readonly IQueueSender _queueSender;
        private readonly IBlobSasGenerator _blobSasGenerator;

        private readonly Regex _sharePointOnlineUrlRegex;
        private readonly string _sharePointOnlineUsername;
        private readonly string _sharePointOnlinePassword;

        public DocumentationController(MyDrillingDb db, 
            IQueueSender queueSender,
            IBlobSasGenerator blobSasGenerator,
            IConfiguration config)
        {
            _db = db;
            _queueSender = queueSender;
            _blobSasGenerator = blobSasGenerator;
            
            _sharePointOnlineUrlRegex = new Regex(config.GetValue<string>("SharePointOnline:UrlRegex"), RegexOptions.Compiled | RegexOptions.IgnoreCase);
            _sharePointOnlineUsername = config.GetValue<string>("SharePointOnline:Username");
            _sharePointOnlinePassword = config.GetValue<string>("SharePointOnline:Password");
        }

        [HttpGet("")]
        [HttpGet("index")]
        public async Task<IActionResult> Index() =>
            View("Index", await PrepareUserManualsViewModel(nameof(Index)));


        [HttpGet("technicalreports")]
        public async Task<IActionResult> TechnicalReports() =>
            View("TechnicalReports", await PrepareTechnicalReportsViewModel(nameof(TechnicalReports)));

        [HttpGet("usermanualdetail/{id}")]
        public async Task<IActionResult> UserManualDetail(long id)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            var userMan = await _db.Documentation_UserManuals
                .ApplyPermissions(roles)
                .OnlyAllowedRigs()
                .FirstOrDefaultAsync(u => u.Id == id);
            if (userMan == null || !userMan.RigId.HasValue)
                return RedirectToAction("ContentNotFound", "Home");
            var userManualViewModel = new UserManualViewModel
            {
                Id = userMan.Id,
                ReferenceId = userMan.ReferenceId,
                Description = userMan.Name,
                EquipmentType = !string.IsNullOrEmpty(userMan.ProductCode) ? $"{userMan.ProductNameFormatted} ({userMan.ProductCode})" : "",
                Discipline = userMan.Discipline,
                DocumentDate = userMan.DocumentDate.ToString(HtmlExt.StdDateFormat, CultureInfo.InvariantCulture),
                IsSharePoint = !string.IsNullOrEmpty(userMan.Url) && _sharePointOnlineUrlRegex.Match(userMan.Url).Success
            };
            
            var userManualsSameRef = _db.Documentation_UserManuals
                .ApplyPermissions(roles)
                .OnlyAllowedRigs()
                .Where(r => r.ReferenceId == userMan.ReferenceId && r.RigId == userMan.RigId);
            var relatedUserManuals = await _db.Documentation_UserManuals
                .ApplyPermissions(roles)
                .OnlyAllowedRigs()
                .Where(r => userManualsSameRef.Select(x => x.RelatedReferenceId).Contains(r.ReferenceId))
                .Select(r => new UserManualViewModel
                {
                    Id = r.Id,
                    ReferenceId = r.ReferenceId,
                    Description = r.Name,
                    EquipmentType = !string.IsNullOrEmpty(r.ProductCode) ? $"{r.ProductNameFormatted} ({r.ProductCode})" : "",
                    Discipline = r.Discipline,
                    DocumentDate = r.DocumentDate.ToString(HtmlExt.StdDateFormat, CultureInfo.InvariantCulture),
                    IsSharePoint = !string.IsNullOrEmpty(r.Url) && _sharePointOnlineUrlRegex.Match(r.Url).Success
                }).ToArrayAsync();
            userManualViewModel.RelatedDocuments = relatedUserManuals;
            return View("UserManualDetail", userManualViewModel);
        }

        [HttpPost("requestdownload")]
        public async Task<IActionResult> RequestDownload(long id, DocumentType docType)
        {
            if (id <= 0 || docType == DocumentType.None) 
            {
                return StatusCode(400, "Missing data.");
            }
            var roles = HttpContext.GetCurrentUserRoles();
            string fileUrl = "";
            string fileName = "";
            if (docType == DocumentType.UserManual)
            {
                var userMan = await _db.Documentation_UserManuals
                    .ApplyPermissions(roles)
                    .OnlyAllowedRigs()
                    .FirstOrDefaultAsync(x => x.Id == id);
                if(userMan == null || string.IsNullOrEmpty(userMan.Url))
                    return NotFound();
                fileUrl = userMan.Url;
                fileName = !string.IsNullOrEmpty(userMan.FileName) ? userMan.FileName : $"{userMan.ReferenceId}.pdf" ;
            }
            else if (docType == DocumentType.TechnicalReports)
            {
                var techReport = await _db.Documentation_TechnicalReports
                    .ApplyPermissions(roles)
                    .OnlyAllowedRigs()
                    .FirstOrDefaultAsync(x => x.Id == id);
                if (techReport == null || string.IsNullOrEmpty(techReport.Url))
                    return NotFound();
                fileUrl = techReport.Url;
                fileName = !string.IsNullOrEmpty(techReport.FileName) ? techReport.FileName : $"{techReport.ReferenceId}.pdf";
            }
            
            if(string.IsNullOrEmpty(fileUrl) || string.IsNullOrEmpty(fileName))
                return StatusCode(404, "File not found or you don't have permissions.");
            
            var requestId = await _db.CreateNewAsyncOperation(HttpContext.GetCurrentUser());
            var docUrlParts = fileUrl.Split("|");
            if (docUrlParts.Length < 5)
            {
                return StatusCode(400, "Missing data.");
            }

            //TODO it's more efficient to check right here whether blob exists or not
            //if it exists you can generate SAS link right now without queue at all

            var blobFolder = docType == DocumentType.UserManual ? "user-manuals" : "technical-reports";
            await _queueSender.EnqueueFileRequestAsync(requestId, $"{blobFolder}/{fileName}", docUrlParts[2], docUrlParts[1], docUrlParts[3], docUrlParts[4], true);

            return StatusCode(200, requestId);
        }

        [HttpGet("downloadsp/{id}")]
        public async Task<IActionResult> DownloadSharePointDocument(long id)
        {
            if (id <= 0)
            {
                return StatusCode(400, "Missing data.");
            }
            var roles = HttpContext.GetCurrentUserRoles();
            var userMan = await _db.Documentation_UserManuals
                .ApplyPermissions(roles)
                .OnlyAllowedRigs()
                .FirstOrDefaultAsync(x => x.Id == id);
            if (userMan == null || string.IsNullOrEmpty(userMan.Name) || string.IsNullOrEmpty(userMan.Url))
                return NotFound();

            var docUrl = userMan.Url;
           
            if (string.IsNullOrEmpty(docUrl))
                return StatusCode(404, "File not found or you don't have permissions.");
            var spoMatch = _sharePointOnlineUrlRegex.Match(docUrl);

            if (spoMatch.Success)
            {
                var siteUrl = docUrl.Substring(0, spoMatch.Index + spoMatch.Length - 1);
                var context = new ClientContext(siteUrl)
                {
                    Credentials = new SharePointOnlineCredentials(_sharePointOnlineUsername, _sharePointOnlinePassword)
                };
                var file = context.Web.GetFileByServerRelativeUrl(new Uri(docUrl).PathAndQuery);
                context.Load(file);
                ClientResult<Stream> streamResult = file.OpenBinaryStream();
                await context.ExecuteQueryAsync();
                new FileExtensionContentTypeProvider().TryGetContentType(file.Name, out string contentType);

                return File(streamResult.Value, contentType, file.Name);
            }
            return StatusCode(500, "Something went wrong.");
        }

        private async Task<UserManualsViewModel> PrepareUserManualsViewModel(string currentActionName)
        {
            var drillDowns = HttpContext.GetDrillDownParameters();
            var roles = HttpContext.GetCurrentUserRoles();

            var userManualQuery = _db.Documentation_UserManuals
                .OnlyAllowedRigs()
                .ApplyPermissions(roles)
                .ApplyDrillDown(drillDowns)
                .Where(u => u.RigId.HasValue && !u.IsMaster);

            AddFilterData(userManualQuery);

            userManualQuery = userManualQuery
                .ApplyFilter(HttpContext.GetFilterParameters());

            var userManuals = userManualQuery
                .ApplyOrder(HttpContext.GetOrderParameters())
                .ApplyPager(HttpContext.GetPagerParameters());
            var totalCount = await userManualQuery.CountAsync();

            var modelUserManuals = await
                userManuals
                .Select(u => new UserManualViewModel
                {
                    Id = u.Id,
                    Description = u.Name,
                    ReferenceId = u.ReferenceId,
                    EquipmentType = !string.IsNullOrEmpty(u.ProductCode) ? $"{u.ProductNameFormatted} ({u.ProductCode})" : "",
                    RigName = u.Rig.NameFormatted,
                    Discipline = u.Discipline,
                    DocumentDate = u.DocumentDate.ToString(HtmlExt.StdDateFormat, CultureInfo.InvariantCulture),
                    IsSharePoint = !string.IsNullOrEmpty(u.Url) && _sharePointOnlineUrlRegex.Match(u.Url).Success
                }).ToArrayAsync();

            var model = new UserManualsViewModel
            {
                Menu = new MenuViewModel { CurrentActionName = currentActionName },
                Items = modelUserManuals,
                TotalCount = totalCount
            };

            return model;
        }

        private void AddFilterData(IQueryable<UserManual> userManualQuery)
        {
            var availableDisciplineCodes = userManualQuery
                .Select(x => new { x.Discipline, x.DisciplineCode })
                .Where(u => !string.IsNullOrEmpty(u.DisciplineCode))
                .Distinct()
                .ToList()
                .GroupBy(x => x.DisciplineCode)
                .ToDictionary(g => g.Key, g => g.First().Discipline);
            ViewData[UserManualsViewModel.DisciplineFilter] = availableDisciplineCodes
                .Select(x => new SelectListItem($"{x.Key} ({x.Value})", x.Key))
                .OrderBy(x => x.Text)
                .ToArray();
        }

        private async Task<TechnicalReportsViewModel> PrepareTechnicalReportsViewModel(string currentActionName)
        {
            var drillDowns = HttpContext.GetDrillDownParameters();
            var roles = HttpContext.GetCurrentUserRoles();

            var technicalReportQuery = _db.Documentation_TechnicalReports
                .ApplyPermissions(roles)
                .OnlyAllowedRigs()
                .ApplyDrillDown(drillDowns);

            var totalCount = await technicalReportQuery.CountAsync();

            var technicalReports = technicalReportQuery
               .ApplyOrder(HttpContext.GetOrderParameters())
               .ApplyPager(HttpContext.GetPagerParameters());

            var modelTechnicalReports = await technicalReports.Select(u => new TechnicalReportViewModel
            {
                Id = u.Id,
                Description = u.Name,
                ReferenceId = u.ReferenceId,
                EquipmentType = !string.IsNullOrEmpty(u.ProductCode) ? $"{u.ProductNameFormatted} ({u.ProductCode})" : "",
                RigName = u.Rig.NameFormatted,
                DocumentDate = u.DocumentDate.ToString(HtmlExt.StdDateFormat, CultureInfo.InvariantCulture),
                Url = u.Url
            }).ToArrayAsync();

            var model = new TechnicalReportsViewModel
            {
                Menu = new MenuViewModel { CurrentActionName = currentActionName },
                Items = modelTechnicalReports,
                TotalCount = totalCount
            };

            return model;
        }
    }

    public static class DocumentationFilterExt
    {
        public static IQueryable<UserManual> ApplyDrillDown(this IQueryable<UserManual> query, DrillDownParameters drillDown)
        {
            var newQuery = query;
            if (drillDown.HasRig)
                newQuery = newQuery.Where(u => u.RigId == drillDown.RigId);
            if (drillDown.HasProductCode)
                newQuery = newQuery.Where(u => u.ProductCode == drillDown.ProductCode);
            if (drillDown.HasDuration)
                newQuery = newQuery.Where(x => x.DocumentDate >= DateTime.UtcNow.AddDays(-drillDown.DurationInDays));
            return newQuery;
        }
        public static IOrderedQueryable<UserManual> ApplyOrder(this IQueryable<UserManual> query, SortParameters sort)
        {
            switch (sort.SortField)
            {
                case UserManualsViewModel.ReferenceIdSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.ReferenceId)
                        : query.OrderByDescending(b => b.ReferenceId);
                case UserManualsViewModel.TitleSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.Name)
                        : query.OrderByDescending(b => b.Name);
                case UserManualsViewModel.EquipmentTypeSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.ProductName)
                        : query.OrderByDescending(b => b.ProductName);
                case UserManualsViewModel.RigSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.Rig.Name)
                        : query.OrderByDescending(b => b.Rig.Name);
                case UserManualsViewModel.DisciplineSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.Discipline)
                        : query.OrderByDescending(b => b.Discipline);
                case UserManualsViewModel.DateSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.DocumentDate)
                        : query.OrderByDescending(b => b.DocumentDate);
                default:
                    return query.OrderByDescending(b => b.DocumentDate);
            }
        }
        public static IQueryable<UserManual> ApplyPager(this IQueryable<UserManual> query, PagerParameters pager)
        {
            return query
                .Skip(pager.PageSize * (pager.CurrentPageNumber - 1))
                .Take(pager.PageSize);
        }

        public static IQueryable<TechnicalReport> ApplyDrillDown(this IQueryable<TechnicalReport> query, DrillDownParameters drillDown)
        {
            var newQuery = query;
            if (drillDown.HasRig)
                newQuery = newQuery.Where(u => u.RigId == drillDown.RigId);
            if (drillDown.HasProductCode)
                newQuery = newQuery.Where(u => u.ProductCode == drillDown.ProductCode);
            if (drillDown.HasDuration)
                newQuery = newQuery.Where(x => x.DocumentDate >= DateTime.UtcNow.AddDays(-drillDown.DurationInDays));
            return newQuery;
        }
        public static IOrderedQueryable<TechnicalReport> ApplyOrder(this IQueryable<TechnicalReport> query, SortParameters sort)
        {
            switch (sort.SortField)
            {
                case TechnicalReportsViewModel.ReferenceIdSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.ReferenceId)
                        : query.OrderByDescending(b => b.ReferenceId);
                case TechnicalReportsViewModel.TitleSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.Name)
                        : query.OrderByDescending(b => b.Name);
                case TechnicalReportsViewModel.EquipmentTypeSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.ProductName)
                        : query.OrderByDescending(b => b.ProductName);
                case TechnicalReportsViewModel.RigSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.Rig.Name)
                        : query.OrderByDescending(b => b.Rig.Name);
                case TechnicalReportsViewModel.DateSort:
                    return sort.IsAsc
                        ? query.OrderBy(b => b.DocumentDate)
                        : query.OrderByDescending(b => b.DocumentDate);
                default:
                    return query.OrderByDescending(b => b.DocumentDate);
            }
        }

        public static IQueryable<TechnicalReport> ApplyPager(this IQueryable<TechnicalReport> query, PagerParameters pager)
        {
            return query
                .Skip(pager.PageSize * (pager.CurrentPageNumber - 1))
                .Take(pager.PageSize);
        }

        public static IQueryable<UserManual> ApplyFilter(this IQueryable<UserManual> query, FilterParameters filter)
        {
            switch (filter.FilterField)
            {
                case UserManualsViewModel.DisciplineFilter:
                    var disciplineCode = filter.FilterValue;
                    return query.Where(x => x.DisciplineCode == disciplineCode);
                default:
                    return query;
            }
        }

        public static IQueryable<UserManual> OnlyAllowedRigs(this IQueryable<UserManual> query)
        {
            return query.Where(x => x.RigId.HasValue && x.Rig.ImportDocumentation);
        }

        public static IQueryable<TechnicalReport> OnlyAllowedRigs(this IQueryable<TechnicalReport> query)
        {
            return query.Where(x => x.RigId.HasValue && x.Rig.ImportDocumentation);
        }
    }
}