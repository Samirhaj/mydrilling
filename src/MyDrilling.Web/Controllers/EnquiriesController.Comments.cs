﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Infrastructure.MessageContracts;
using MyDrilling.Infrastructure.Storage;
using MyDrilling.Web.Filters;
using MyDrilling.Web.Models.Enquiries;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Controllers
{
    public partial class EnquiriesController
    {
        [HttpPost("addComment")]
        [ServiceFilter(typeof(ModelValidationFilter))]
        public async Task<IActionResult> AddComment(CreateEnquiryCommentViewModel model)
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries.FirstOrDefaultAsync(x => x.Id == model.EnquiryId);
            if (!enquiry.CanAddComment(roles))
            {
                return Unauthorized();
            }

            //just retry in case of concurrent updates
            //enquiry.AddComment updates LastCommented only
            Comment comment;
            while (true)
            {
                var attachments = new List<Attachment>();
                comment = enquiry.AddComment(model.Text, user);

                if (model.Files != null && model.Files.Length > 0)
                {
                    foreach (var file in model.Files)
                    {
                        var blobClient = _blobServiceClient.GetBlobContainerClient(StorageConfig.EnquiriesContainerName)
                            .GetBlobClient(file);
                        var properties = await blobClient.GetPropertiesAsync();
                        if (properties.Value.Metadata.TryGetValue(StorageConstants.BlobMetadataKeys.AuthorId, out var authorId)
                            && authorId == user.Id.ToString())
                        {
                            attachments.Add(enquiry.AddAttachment(file, user, comment));
                        }
                    }
                }

                try
                {
                    await _db.SaveChangesAsync();
                    break;
                }
                catch (DbUpdateConcurrencyException)
                {
                    _db.Entry(comment).State = EntityState.Detached;
                    foreach (var attachment in attachments)
                    {
                        _db.Entry(attachment).State = EntityState.Detached;
                    }
                    _db.Entry(enquiry).Reload();
                }
            }

            if (StateInfo.PublishedStates.Contains(enquiry.State))
            {
                var details = new Dictionary<string, string> {{ EnquiryEventMessage.DetailKeys.CommentId, comment.Id.ToString()} };
                await Task.WhenAll(_queueSender.EnqueueSyncEnquiryToSapAsync(enquiry.Id),
                    _queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryNewCommentAdded, details));
            }

            return Json(new { Url = Url.Action("Detail", "Enquiries", HttpContext.GetDrillDownParameters().ToRouteValueDictionary(new { enquiry.Id })) });
        }

        [HttpPost("deleteComment")]
        public async Task<IActionResult> DeleteComment(long id, long commentId)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries
                .Include(x => x.Attachments)
                .Include(x => x.Comments)
                .FirstOrDefaultAsync(x => x.Id == id);

            var commentToDelete = enquiry.Comments.FirstOrDefault(x => x.Id == commentId);
            if (commentToDelete == null)
            {
                return RedirectToAction("Detail", new { id });
            }

            if (!enquiry.CanDeleteComment(roles))
            {
                return Unauthorized();
            }

            enquiry.DeleteComment(commentToDelete);
            _db.Enquiry_Comments.Remove(commentToDelete);
            await _db.SaveChangesAsync();

            return RedirectToAction("Detail", HttpContext.GetDrillDownParameters().ToRouteValueDictionary(new { enquiry.Id }));
        }
    }
}
