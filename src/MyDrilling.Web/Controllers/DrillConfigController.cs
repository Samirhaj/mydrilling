﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Controllers
{
    public class DrillConfigController : Controller
    {
        private readonly IConfiguration _config;

        public DrillConfigController(IConfiguration config)
        {
            _config = config;
        }
        public IActionResult Index()
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.HasDrillConfigAccess())
            {
                return RedirectToAction("NoAccess", "Home");
            }

            var drillConfigUrl = _config.GetValue<string>("ExternalUrls:DrillConfigUrl");
            return View("Index", drillConfigUrl);
        }
    }
}