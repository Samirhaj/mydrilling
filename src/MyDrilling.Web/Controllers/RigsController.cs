﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Core;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Core.Entities.Rig;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Web.Models.Rig;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Controllers
{
    [Route("Rigs")]
    [Route("Rig")]
    public class RigsController : Controller
    {

        private readonly ILogger<RigsController> _logger;
        private readonly MyDrillingDb _db;

        public RigsController(ILogger<RigsController> logger, MyDrillingDb db)
        {
            _logger = logger;
            _db = db;
        }

        [HttpGet("")]
        [HttpGet("index")]
        public async Task<IActionResult> Index()
        {
            var drillDown = HttpContext.GetDrillDownParameters();
            var routeValues = drillDown.ToRouteValueDictionary();
            var tz = HttpContext.GetCurrentUser().GetTimeZoneInfo();
            if (drillDown.HasEquipment)
                return RedirectToAction("Detail", "Equipment", routeValues);
            if (drillDown.HasProductCode)
                return RedirectToAction("Type", "Equipment", routeValues);
            if (!drillDown.HasRig)
                return RedirectToAction("Index", "Home", routeValues);
            var roles = HttpContext.GetCurrentUserRoles();
            var rig = _db.Rigs
                .Include(x => x.Owner)
                .Include(x => x.RootOwner)
                .ApplyPermission(roles)
                .FirstOrDefault(x => x.Id == drillDown.RigId);
            if (rig == null)
            {
                return RedirectToAction("NoAccess", "Home");
            }

            var availableEnquiries = await _db.Enquiry_Enquiries
                .Include(x => x.CreatedBy)
                .ApplyPermissions(roles)
                .ApplyDrillDown(drillDown)
                .ExcludeInternal()
                .ExcludeSoftDeleted()
                .Where(x => 
                    x.State == StateInfo.State.AssignedCustomer
                    || x.State == StateInfo.State.AssignedProvider
                    || x.State == StateInfo.State.SuggestedClosed)
                .ToArrayAsync(); 
            
            var rigOnDowntimeEnquiry = availableEnquiries
                .Where(x => x.Type == TypeInfo.Type.Support_RigOnDowntime)
                .OrderByDescending(x => x.Created).FirstOrDefault();
            var rigOverviewModel = new RigOverviewModel
            {
                RigData = new RigViewModel
                {
                    Name = rig.NameFormatted,
                    Owner = rig.RootOwner?.Name.ToCamelCase(),
                    Client = rig.Owner?.Name.ToCamelCase(),
                    Sector = rig.Sector,
                    Type = rig.TypeCode.GetRigTypeInfo(),
                    RigOnDowntime = rigOnDowntimeEnquiry != null
                },
                RigDowntimeDate = rigOnDowntimeEnquiry?.Created.ToLocal(tz).ToFullString(),
                RigDowntimeEnquiryId = rigOnDowntimeEnquiry?.Id ?? default,
                NumberOfOpenEnquiries = availableEnquiries.Length
            };
            return View("Index", rigOverviewModel);
        }

        [HttpGet("rigsoverview")]
        public IActionResult RigsOverview()
        {
            return ViewComponent("RigsOverview");
        }

        [HttpGet("lastactivities")]
        public IActionResult LastActivities()
        {
            return ViewComponent("LastActivities");
        }
    }

    public static class RigFilterExt
    {
        public static IQueryable<Rig> ApplyPager(this IQueryable<Rig> query, PagerParameters pager)
        {
            return query
                .Skip(pager.PageSize * (pager.CurrentPageNumber - 1))
                .Take(pager.PageSize);
        }

        public static IEnumerable<ActivityViewModel> ApplyPager(this IEnumerable<ActivityViewModel> query, PagerParameters pager)
        {
            return query
                .Skip(pager.PageSize * (pager.CurrentPageNumber - 1))
                .Take(pager.PageSize);
        }
    }

    public static class RigInfoExt
    {
        public static string GetRigTypeInfo(this string rigType)
        {
            return string.IsNullOrEmpty(rigType)
                ? RigTypeInfo.ByCode["NA"].Name
                : RigTypeInfo.ByCode.TryGetValue(rigType, out var typeInfo)
                    ? typeInfo.Name
                    : rigType;
        }
    }
}