﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Core.Entities.Permission;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Controllers
{
    public partial class EnquiriesController
    {
        [HttpGet("findAssignees")]
        public async Task<IActionResult> FindAssignees(long id, string startsWith, int limit)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries.AsNoTracking().Where(x => x.Id == id).FirstAsync();
            if (!enquiry.CanSetAssignee(roles))
            {
                return Unauthorized();
            }

            var assigneeSearch = _db.Users.WithCustomerRole(RoleInfo.Role.CustomerAccess, enquiry.RootCustomerId)
                .WithRigRole(RoleInfo.Role.EnquiryHandlingCollaborator, enquiry.RigId);

            assigneeSearch = enquiry.State == StateInfo.State.AssignedProvider
                ? assigneeSearch.Where(x => x.IsInternal)
                : assigneeSearch.Where(x => !x.IsInternal);

            assigneeSearch = string.IsNullOrEmpty(startsWith)
                ? assigneeSearch
                : assigneeSearch.Where(t => t.FirstName.StartsWith(startsWith) ||
                    t.LastName.StartsWith(startsWith) ||
                    t.Upn.StartsWith(startsWith) ||
                    t.Position.StartsWith(startsWith));

            assigneeSearch = assigneeSearch.Take(limit == 0 ? 10 : limit);

            var assigneeSearchResult = await assigneeSearch.Select(x => new SelectListItem(x.Id.ToString(), $"{x.FirstName} {x.LastName}")).ToArrayAsync();
            return Json(assigneeSearchResult.Where(x => !string.IsNullOrWhiteSpace(x.Value)));
        }

        [HttpPost("setAssignee")]
        public async Task<IActionResult> SetAssignee(long id, long assigneeId, long version)
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries.FirstOrDefaultAsync(x => x.Id == id);
            if (enquiry.Version != version)
            {
                throw new DbUpdateConcurrencyException();
            }

            if (!enquiry.CanSetAssignee(roles))
            {
                return Unauthorized();
            }

            var assigneeSearch = _db.Users.Where(x => x.Id == assigneeId)
                .WithCustomerRole(RoleInfo.Role.CustomerAccess, enquiry.RootCustomerId)
                .WithRigRole(RoleInfo.Role.EnquiryHandlingCollaborator, enquiry.RigId);

            assigneeSearch = enquiry.State == StateInfo.State.AssignedProvider
                ? assigneeSearch.Where(x => x.IsInternal)
                : assigneeSearch.Where(x => !x.IsInternal);

            var assignee = await assigneeSearch.SingleOrDefaultAsync();
            if (assignee != null)
            {
                enquiry.SetAssignee(user, assignee);

                await _db.SaveChangesAsync();
                await _queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryAssigned);
            }

            return Json(new {});
        }
    }
}
