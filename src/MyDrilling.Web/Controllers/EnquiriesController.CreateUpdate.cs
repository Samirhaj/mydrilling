﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Infrastructure.Storage;
using MyDrilling.Web.Filters;
using MyDrilling.Web.Models.Enquiries;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Controllers
{
    public partial class EnquiriesController
    {
        [HttpGet("create")]
        public async Task<IActionResult> Create(int type, string title)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            var drillDown = HttpContext.GetDrillDownParameters();
            if (!roles.CanCreate(rigId: drillDown.RigId))
            {
                return Unauthorized();
            }

            var customerIds = roles.GetAccessToRootCustomers();
            var rootCustomers = await _db.RootCustomers.AsNoTracking()
                .Where(x => customerIds.Contains(x.Id)).ToArrayAsync();
            var uiRootCustomers = rootCustomers.Select(x => new SelectListItem(x.Name, x.Id.ToString())).ToArray();

            var initiatorRigIds = roles.GetEnquiryHandlingInitiatorRigs().ToList();
            var approverRigIds = roles.GetEnquiryHandlingApproverRigs();
            initiatorRigIds.AddRange(approverRigIds);
            var rigs = await _db.Rigs.AsNoTracking()
                .Where(x => initiatorRigIds.Contains(x.Id)).ToArrayAsync();
            var uiRigs = rigs.Select(x => new SelectListItem(x.Name.ToCamelCaseWithRomanNumerals(), x.Id.ToString())).ToArray();
            
            var rigsForCustomer = rootCustomers
                .ToDictionary(rc => rc.Id, rc => rigs.Where(r => r.RootOperatorId == rc.Id || r.RootOwnerId == rc.Id)
                    .Select(x => new SelectListItem(x.Name.ToCamelCaseWithRomanNumerals(), x.Id.ToString())).ToArray());

            var customersForRig = rigs
                .ToDictionary(r => r.Id, r => rootCustomers.Where(rc => r.RootOperatorId == rc.Id || r.RootOwnerId == rc.Id)
                    .Select(x => new SelectListItem(x.Name, x.Id.ToString())).ToArray());

            var types = TypeInfo.Types;
            if (!roles.HasBulletinViewerAccessToRigs())
            {
                types = TypeInfo.Types.Where(x => x.Value != TypeInfo.Type.Support_BulletinSupport).ToArray();
            }

            var model = new CreateEnquiryViewModel
            {
                Title = title,
                RigsForCustomer = rigsForCustomer,
                CustomersForRig = customersForRig,
                Customers = uiRootCustomers,
                Rigs = uiRigs,
                Categories = types.Select(x => x.Category).Distinct().ToArray(),
                TypesForCategory = types.GroupBy(x => x.Category).ToDictionary(x => x.Key, x => x.ToArray()),
                Types = types.ToDictionary(x => x.Id, x => x),
                TypeId = type,
                CategoryId = TypeInfo.ById.TryGetValue(type, out var typeInfo) ? typeInfo.Category : default,
                RigId = rigs.Any(x => x.Id == drillDown.RigId) ? drillDown.RigId : default
            };
            return View(model);
        }

        [HttpPost("create")]
        [ServiceFilter(typeof(ModelValidationFilter))]
        public async Task<IActionResult> Create(CreateEnquiryViewModel model)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (model.IsDraft && !roles.CanCreateDraft(model.RootCustomerId, model.RigId)
                ||
                !model.IsDraft && !roles.CanCreateOpen(model.RootCustomerId, model.RigId))
            {
                return Unauthorized();
            }

            if ((TypeInfo.Type) model.TypeId == TypeInfo.Type.Support_BulletinSupport
                && !roles.HasBulletinViewerAccessToRigs())
            {
                return Unauthorized();
            }

            var user = HttpContext.GetCurrentUser();
            var rig = await _db.Rigs
                .Include(x => x.Owner)
                .SingleAsync(x => x.Id == model.RigId);
            var rootCustomer = await _db.RootCustomers
                .Include(x => x.DefaultCustomer)
                .SingleAsync(x => x.Id == model.RootCustomerId);
            var mhWirth = await _db.RootCustomers.SingleAsync(x => x.IsInternal);
            var equipment = model.EquipmentId > 0 && TypeInfo.ById[model.TypeId].CanHaveEquipment
                ? await _db.Equipments.SingleAsync(x => x.Id == model.EquipmentId)
                : default;

            var enquiry = new Enquiry(model.Title, model.Description, (TypeInfo.Type)model.TypeId, rig,
                rootCustomer, rootCustomer.DefaultCustomer, rig.Owner, mhWirth, equipment, user);
            enquiry.AddInitialComment();

            if (model.Files != null && model.Files.Length > 0)
            {
                foreach (var file in model.Files)
                {
                    var blobClient = _blobServiceClient.GetBlobContainerClient(StorageConfig.EnquiriesContainerName)
                        .GetBlobClient(file);
                    var properties = await blobClient.GetPropertiesAsync();
                    if (properties.Value.Metadata.TryGetValue(StorageConstants.BlobMetadataKeys.AuthorId, out var authorId)
                        && authorId == user.Id.ToString())
                    {
                        enquiry.AddAttachment(file, user);
                    }
                }
            }

            if (model.IsDraft)
            {
                enquiry.AsDraft(user);
            }
            else
            {
                enquiry.Approve(user);
            }
            
            _db.Enquiry_Enquiries.Add(enquiry);
            await _db.SaveChangesAsync();

            if (!model.IsDraft)
            {
                var events = new List<Task>
                {
                    _queueSender.EnqueueSyncEnquiryToSapAsync(enquiry.Id),
                    _queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryAssignedToProvider)
                };

                if (enquiry.Type == TypeInfo.Type.Other_RigMove)
                {
                    events.Add(_queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryRigMovedCreated));
                }

                if (enquiry.Type == TypeInfo.Type.Support_RigOnDowntime)
                {
                    events.Add(_queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryRigDowntimeCreated));
                }
                await Task.WhenAll(events);
            }

            return Json(new { Url = Url.Action("Detail", "Enquiries", new { enquiry.Id, rig = rig.ReferenceId}) });
        }

        [HttpGet("edit")]
        public async Task<IActionResult> Edit(long id)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries
                .AsNoTracking().FirstOrDefaultAsync(x => x.Id == id);
            if (!enquiry.CanEdit(roles))
            {
                return Unauthorized();
            }

            var model = await _db.Enquiry_Enquiries.Where(x => x.Id == id)
                .Select(x => new EditEnquiryViewModel
                {
                    Id = x.Id,
                    RigName = x.Rig.Name.ToCamelCaseWithRomanNumerals(),
                    RigId = x.Rig.Id,
                    TypeId = (int)x.Type,
                    EquipmentId = x.EquipmentId,
                    EquipmentName = x.Equipment.Name.ToCamelCase(),
                    Title = x.Title,
                    Description = x.Description,
                    Version = x.Version
                }).FirstAsync();

            if (!model.EquipmentId.HasValue)
            {
                model.EquipmentName = LanguageConstants.NotAvailableShort;
                model.EquipmentId = default(long);
            }

            model.CategoryName = TypeInfo.ById[model.TypeId].Category;
            model.Types = TypeInfo.ByCategory[model.CategoryName];
            model.CanChangeType = TypeInfo.ByCategory[model.CategoryName].Length > 1;
            model.CanHaveEquipment = TypeInfo.ById[model.TypeId].CanHaveEquipment;

            return View(model);
        }

        [HttpPost("edit")]
        [ServiceFilter(typeof(ModelValidationFilter))]
        public async Task<IActionResult> Edit(EditEnquiryViewModel model)
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries.FirstOrDefaultAsync(x => x.Id == model.Id);
            if (enquiry.Version != model.Version)
            {
                throw new DbUpdateConcurrencyException();
            }

            if (!enquiry.CanEdit(roles))
            {
                return Unauthorized();
            }

            if (model.TypeId > 0)
            {
                enquiry.SetType((TypeInfo.Type) model.TypeId, user);
            }

            if (model.EquipmentId.HasValue)
            {
                enquiry.SetEquipment(await _db.Equipments.FirstOrDefaultAsync(x => x.Id == model.EquipmentId), user);
            }

            enquiry.SetTitle(model.Title, user);
            enquiry.SetDescription(model.Description, user);

            await _db.SaveChangesAsync();

            return Json(new { Url = Url.Action("Detail", "Enquiries", HttpContext.GetDrillDownParameters().ToRouteValueDictionary(new { enquiry.Id })) });
        }
    }
}
