﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Infrastructure.MessageContracts;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Controllers
{
    public partial class EnquiriesController
    {
        [HttpPost("assignToCustomer")]
        public async Task<IActionResult> AssignToCustomer(long id, long version)
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries.FirstOrDefaultAsync(x => x.Id == id);
            if (enquiry.Version != version)
            {
                throw new DbUpdateConcurrencyException();
            }

            if (!enquiry.CanAssignToCustomer(roles))
            {
                return Unauthorized();
            }

            enquiry.AssignToCustomer(user);

            await _db.SaveChangesAsync();
            await _queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryAssignedToRig);

            return RedirectToAction("Detail", HttpContext.GetDrillDownParameters().ToRouteValueDictionary(new { id }));
        }

        [HttpPost("assignToProvider")]
        public async Task<IActionResult> AssignToProvider(long id, long version)
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries.FirstOrDefaultAsync(x => x.Id == id);
            if (enquiry.Version != version)
            {
                throw new DbUpdateConcurrencyException();
            }

            if (!enquiry.CanAssignToProvider(roles))
            {
                return Unauthorized();
            }

            enquiry.AssignToProvider(user);

            await _db.SaveChangesAsync();
            await _queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryAssignedToProvider);

            return RedirectToAction("Detail", HttpContext.GetDrillDownParameters().ToRouteValueDictionary(new { id }));
        }

        [HttpPost("removeInternalUser")]
        public async Task<IActionResult> RemoveInternalUser(long id, long version)
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries.FirstOrDefaultAsync(x => x.Id == id);
            if (enquiry.Version != version)
            {
                throw new DbUpdateConcurrencyException();
            }

            if (!enquiry.CanRemoveAssignee(roles))
            {
                return Unauthorized();
            }

            enquiry.RemoveAssignee(user);

            await _db.SaveChangesAsync();

            return RedirectToAction("Detail", HttpContext.GetDrillDownParameters().ToRouteValueDictionary(new { id }));
        }

        [HttpPost("suggestToClose")]
        public async Task<IActionResult> SuggestToClose(long id, long version)
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries.FirstOrDefaultAsync(x => x.Id == id);
            if (enquiry.Version != version)
            {
                throw new DbUpdateConcurrencyException();
            }

            if (!enquiry.CanSuggestToClose(roles))
            {
                return Unauthorized();
            }

            var comment = enquiry.SuggestToClose(user);

            await _db.SaveChangesAsync();

            var details = new Dictionary<string, string> { { EnquiryEventMessage.DetailKeys.CommentId, comment.Id.ToString() } };
            await Task.WhenAll(_queueSender.EnqueueSyncEnquiryToSapAsync(enquiry.Id),
                _queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryNewCommentAdded, details));

            return RedirectToAction("Detail", HttpContext.GetDrillDownParameters().ToRouteValueDictionary(new { id }));
        }

        [HttpPost("approveAndSubmit")]
        public async Task<IActionResult> ApproveAndSubmit(long id, long version)
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries.FirstOrDefaultAsync(x => x.Id == id);
            if (enquiry.Version != version)
            {
                throw new DbUpdateConcurrencyException();
            }

            if (!enquiry.CanApprove(roles))
            {
                return Unauthorized();
            }

            enquiry.Approve(user);

            await _db.SaveChangesAsync();

            var events = new List<Task>
            {
                _queueSender.EnqueueSyncEnquiryToSapAsync(enquiry.Id),
                _queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryAssignedToProvider)
            };

            if (enquiry.Type == TypeInfo.Type.Other_RigMove)
            {
                events.Add(_queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryRigMovedCreated));
            }

            if (enquiry.Type == TypeInfo.Type.Support_RigOnDowntime)
            {
                events.Add(_queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryRigDowntimeCreated));
            }

            await Task.WhenAll(events);

            return RedirectToAction("Detail", HttpContext.GetDrillDownParameters().ToRouteValueDictionary(new { id }));
        }

        [HttpPost("rejectEnquiry")]
        public async Task<IActionResult> RejectEnquiry(long id, long version)
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries.FirstOrDefaultAsync(x => x.Id == id);
            if (enquiry.Version != version)
            {
                throw new DbUpdateConcurrencyException();
            }

            if (!enquiry.CanReject(roles))
            {
                return Unauthorized();
            }

            enquiry.Reject(user);

            await _db.SaveChangesAsync();

            return RedirectToAction("Detail", HttpContext.GetDrillDownParameters().ToRouteValueDictionary(new { id }));
        }

        [HttpPost("reopenEnquiry")]
        public async Task<IActionResult> ReopenEnquiry(long id, long version)
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries.FirstOrDefaultAsync(x => x.Id == id);
            if (enquiry.Version != version)
            {
                throw new DbUpdateConcurrencyException();
            }

            if (!enquiry.CanReopen(roles))
            {
                return Unauthorized();
            }

            enquiry.Reopen(user);

            await _db.SaveChangesAsync();

            return RedirectToAction("Detail", HttpContext.GetDrillDownParameters().ToRouteValueDictionary(new { id }));
        }

        [HttpPost("removeEnquiry")]
        public async Task<IActionResult> RemoveEnquiry(long id, long version)
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries.FirstOrDefaultAsync(x => x.Id == id);
            if (enquiry.Version != version)
            {
                throw new DbUpdateConcurrencyException();
            }

            if (!enquiry.CanDelete(roles))
            {
                return Unauthorized();
            }

            enquiry.Delete(user);

            await _db.SaveChangesAsync();
            await _queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryDraftDeleted);

            return RedirectToAction("Detail", HttpContext.GetDrillDownParameters().ToRouteValueDictionary(new { id }));
        }
    }
}
