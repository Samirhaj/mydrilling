﻿using Microsoft.AspNetCore.Mvc;

namespace MyDrilling.Web.Controllers
{
    public class MenuController : Controller
    {
        public IActionResult GetDrillDownMenu()
        {
            return ViewComponent("DrillDownMenu");
        }
    }
}