﻿using System.Globalization;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core.Entities.AsyncOperation;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.Services.AsyncOperation;
using MyDrilling.Infrastructure.Services.AsyncOperation.Entities;
using MyDrilling.Infrastructure.Storage.Queue;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;
using MyDrilling.Web.Models.Spareparts;

namespace MyDrilling.Web.Controllers
{
    [Route("spareparts")]
    public class SparepartsController : Controller
    {
        private readonly MyDrillingDb _db;
        private readonly IQueueSender _queueSender;

        public SparepartsController(MyDrillingDb db, IQueueSender queueSender)
        {
            _db = db;
            _queueSender = queueSender;
        }

        [HttpGet("")]
        [HttpGet("index")]
        public async Task<IActionResult> Index() =>
            View("Spareparts", await CreateItemsViewModel());

        [HttpGet("detail/{id}")]
        public async Task<IActionResult> Detail(long id)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.HasSparepartAccess())
            {
                return RedirectToAction("NoAccess", "Home");
            }
            var sparepart = await _db.Sparepart_Spareparts
                .Join(_db.Sparepart_WhiteList, sp => sp.ReferenceId, wl => wl.GlobalId, (sp, wl) => sp)
                .AsNoTracking()
                .FirstOrDefaultAsync(s => s.Id == id);

            if (sparepart == null)
            {
                return RedirectToAction("ContentNotFound", "Home");
            }
            
            var allowedCustomerIds = roles.GetAccessToRootCustomers();
            var customers = await _db.RootCustomers
                .Where(c => allowedCustomerIds.Contains(c.Id) && c.DefaultCustomerId.HasValue )
                .Select(c => new SelectListItem
                    {
                        Value = c.DefaultCustomer.ReferenceId,
                        Text = c.Name
                    })
                .ToArrayAsync();

            var model = new DetailViewModel
            {
                Id = sparepart.Id,
                ReferenceId = sparepart.ReferenceId,
                MaterialNo = sparepart.MaterialNo,
                Description = sparepart.Description,
                ShowCustomers = customers != null && customers.Length > 0,
                Customers = customers
            };

            return View(model);
        }

        [HttpPost("requestprice")]
        public async Task<IActionResult> RequestPrice(string referenceId, string customerId)
        {
            if (string.IsNullOrWhiteSpace(referenceId) || string.IsNullOrWhiteSpace(customerId))
            {
                return NotFound();
            }

            var requestId = await _db.CreateNewAsyncOperation(HttpContext.GetCurrentUser());

            await _queueSender.EnqueueRequestSparepartPriceAsync(requestId, referenceId, customerId);

            return Accepted(requestId);
        }

        [HttpGet("getprice")]
        public async Task<IActionResult> GetPrice(long requestId)
        {
            var model = new PriceViewModel();
            var view = PartialView("_Price", model);
            
            if (requestId <= 0)
            {
                return view;
            }

            var request = await _db.AsyncOperations.FirstOrDefaultAsync(r => r.Id == requestId);
            if (request == null)
            {
                return view;
            }

            if (request.Status == AsyncOperationStatus.Requested && string.IsNullOrWhiteSpace(request.Response))
            {
                return Accepted();
            }

            var price = JsonSerializer.Deserialize<SparepartPrice>(request.Response);

            model.Price = FormatPrice(price.Value);
            model.Currency = price.Currency;

            return PartialView("_Price", model);
        }

        private async Task<ItemsViewModel> CreateItemsViewModel()
        {
            var spareparts = _db.Sparepart_Spareparts
                .Join(_db.Sparepart_WhiteList, sp => sp.ReferenceId, wl => wl.GlobalId, (sp, wl) => sp)
                .AsQueryable();
            var totalCount = await spareparts.CountAsync();
            
            var sort = HttpContext.GetOrderParameters();
            switch (sort.SortField)
            {
                case ItemsViewModel.PartNumberSort:
                    spareparts = sort.IsAsc ? spareparts.OrderBy(s => s.ReferenceId) : spareparts.OrderByDescending(s => s.ReferenceId);
                    break;
                case ItemsViewModel.TitleSort:
                    spareparts = sort.IsAsc ? spareparts.OrderBy(s => s.Description) : spareparts.OrderByDescending(s => s.Description);
                    break;
                default:
                    spareparts = spareparts.OrderBy(g => g.ReferenceId);
                    break;
            }

            var pager = HttpContext.GetPagerParameters();
            spareparts = spareparts
                .Skip(pager.PageSize * (pager.CurrentPageNumber - 1))
                .Take(pager.PageSize);

            var bulletins = await spareparts
                .Select(e => new ItemViewModel
                {
                    Id = e.Id,
                    MaterialNo = e.MaterialNo,
                    Description = e.Description ?? string.Empty,
                }).ToArrayAsync();

            var model = new ItemsViewModel
            {
                Items = bulletins,
                TotalCount = totalCount
            };

            return model;
        }

        private string FormatPrice(decimal price)
        {
            var cultureInfo = new CultureInfo(CultureInfo.InvariantCulture.LCID)
            {
                NumberFormat = {NumberGroupSeparator = " ", NumberDecimalSeparator = "."}
            };

            return decimal.Round(price, 2).ToString("n", cultureInfo);
        }
    }
}