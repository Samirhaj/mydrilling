﻿using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Core.Entities.Permission;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Controllers
{
    public partial class EnquiriesController
    {
        [HttpGet("sendDraftForApproval")]
        public async Task<IActionResult> SendDraftForApprovalDialog(long id, long version)
        {
            var user = HttpContext.GetCurrentUser();
            var rigId = await _db.Enquiry_Enquiries.Where(x => x.Id == id).Select(x => x.RigId).FirstAsync();
            var enquiry = await _db.Enquiry_Enquiries.SingleAsync(x => x.Id == id);
            if (enquiry.Version != version)
            {
                throw new DBConcurrencyException();
            }

            if (!enquiry.CanSendForApproval(HttpContext.GetCurrentUserRoles()))
            {
                return Unauthorized();
            }

            var approversSearch = await _db.Users.WithRigRole(RoleInfo.Role.EnquiryHandlingApprover, rigId)
                .Where(x => x.RootCustomerId == user.RootCustomerId && x.IsActive)
                .AsNoTracking().ToArrayAsync();
            var approversSearchResult = approversSearch.Select(x => new SelectListItem($"{x.FirstName} {x.LastName}", x.Id.ToString()))
                .Where(x => !string.IsNullOrWhiteSpace(x.Text)).ToList();
            approversSearchResult.Insert(0, new SelectListItem(LanguageConstants.AllApprovers, default(long).ToString()));
            
            ViewBag.Approvers = approversSearchResult;
            return View();
        }

        [HttpPost("sendDraftForApproval")]
        public async Task<IActionResult> SendDraftForApproval(long id, long approverId, long version)
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var enquiry = await _db.Enquiry_Enquiries.FirstOrDefaultAsync(x => x.Id == id);
            if (enquiry.Version != version)
            {
                throw new DbUpdateConcurrencyException();
            }

            if (!enquiry.CanSendForApproval(roles))
            {
                return Unauthorized();
            }

            if (approverId > 0)
            {
                var approver = await _db.Users.Where(x => x.Id == approverId)
                    .WithRigRole(RoleInfo.Role.EnquiryHandlingApprover, enquiry.RigId)
                    .Where(x => x.RootCustomerId == user.RootCustomerId && x.IsActive).SingleAsync();
                if (approver != null)
                {
                    enquiry.AssignToInternalUser(user, approver);
                }
            }
            enquiry.SendForApproval(user);

            await _db.SaveChangesAsync();

            if (approverId > 0)
            {
                await _queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryDraftAssigned);
            }
            else
            {
                await _queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryDraftAssignedToRig);
            }

            return Json(new {});
        }
    }
}
