﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Azure.Storage.Sas;
using Microsoft.AspNetCore.Http;
using MyDrilling.Core;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Core.Entities.UpgradeMatrix;
using MyDrilling.Infrastructure.Services.UpgradeMatrix;
using MyDrilling.Infrastructure.Storage;
using MyDrilling.Infrastructure.Storage.Blob;
using MyDrilling.Infrastructure.Storage.Queue;
using MyDrilling.Web.Models.UpgradeMatrix;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Controllers
{
    [Authorize]
    public class UpgradeMatrixController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IUpgradeMatrixService _upgradeMatrixService;
        private readonly IBlobSasGenerator _blobSasGenerator;
        private readonly IQueueSender _queueSender;

        public UpgradeMatrixController(ILogger<HomeController> logger, 
            IUpgradeMatrixService upgradeMatrixService,
            IBlobSasGenerator blobSasGenerator,
            IQueueSender queueSender)
        {
            _logger = logger;
            _upgradeMatrixService = upgradeMatrixService;
            _blobSasGenerator = blobSasGenerator;
            _queueSender = queueSender;
        }

        public IActionResult Index()
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.HasUpgradeMatrixAccess())
                return RedirectToAction("NoAccess");
            var isInternetExplorer = IsInternetExplorer();
            if (isInternetExplorer)
                return View("SwitchBrowser");
            return View();

        }

        private bool IsInternetExplorer()
        {
            var userAgent = HttpContext.Request.Headers["User-Agent"].ToString();
            var isInternetExplorer =
                (userAgent.Contains("MSIE") || userAgent.Contains("Trident")) && !userAgent.Contains("Edge");
            return isInternetExplorer;
        }

        public IActionResult Customer()
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.HasUpgradeMatrixAccess())
                return RedirectToAction("NoAccess");
            var isInternetExplorer = IsInternetExplorer();
            if (isInternetExplorer)
                return View("SwitchBrowser");
            return View("Index");
        }

        public IActionResult Equipment()
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (!roles.HasUpgradeMatrixAccess())
                return RedirectToAction("NoAccess");
            var isInternetExplorer = IsInternetExplorer();
            if (isInternetExplorer)
                return View("SwitchBrowser");
            return View("Index");
        }

        public IActionResult NoAccess()
        {
            var roles = HttpContext.GetCurrentUserRoles();
            if (roles.HasUpgradeMatrixAccess())
                return RedirectToAction("Index");
            return View();
        }

        [HttpGet]
        [Route("[controller]/getcurrentuser")]
        public IActionResult GetCurrentUser()
        {
            var user = HttpContext.GetCurrentUser();
            var roles = HttpContext.GetCurrentUserRoles();
            var webUser = new WebUpgradeMatrixUser
            {
                Id = user.Id,
                ViewerOnRigs = roles.GetUpgradeMatrixViewAccessToRigs(),
                EditorOnRigs = roles.GetUpgradeMatrixEditorAccessToRigs(),
                EnquiryCreatorOnRigs = roles.GetEnquiryHandlingInitiatorRigs(),//TODO - check whether it is enough
                IsUpgradeMatrixAdmin = roles.IsUpgradeMatrixAdmin(),
                HasUpgradeMatrixAccess = roles.HasUpgradeMatrixAccess()
            };
            return StatusCode(200, new { ok = true, user = webUser  });
        }


        [HttpGet]
        [Route("[controller]/getcustomers")]
        public async Task<IActionResult> GetCustomersAsync(bool filterUsed)
        {
            var customerList = await _upgradeMatrixService.GetCustomersAsync(HttpContext.GetCurrentUserRoles());
            return StatusCode(200, new {ok = true, items = customerList });
        }

        [HttpGet]
        [Route("[controller]/getcustomerrigs/{customerId}")]
        public async Task<IActionResult> GetCustomerRigsAsync(long customerId)
        {
            var customerList = await _upgradeMatrixService.GetRigsForCustomerAsync(customerId, HttpContext.GetCurrentUserRoles());
            return StatusCode(200, new {ok = true, items = customerList.Select(r => new { RigId = r.Id, Name = r.Name, ReferenceId = r.ReferenceId }) });
        }

        [HttpGet]
        [Route("[controller]/getcustomersettings")]
        public async Task<IActionResult> GetCustomerSettingsAsync(bool onlyActive)
        {
            var customerList = new List<WebCustomerSettings>();
            foreach (var cSettings in await _upgradeMatrixService.GetCustomerSettingsWithStatusAsync(HttpContext.GetCurrentUserRoles()))
            {
                if (onlyActive && cSettings.Value != "Active") continue;
                var rigSettings = await _upgradeMatrixService.GetRigSettingsForCustomerAsync(cSettings.RootCustomerId, HttpContext.GetCurrentUserRoles());
                if(onlyActive && rigSettings.Count == 0) continue;
                var webCustomerSettings = new WebCustomerSettings
                {
                    Id = cSettings.Id,
                    CustomerId = cSettings.RootCustomerId,
                    Name = cSettings.RootCustomer.Name,
                    Status = cSettings.Value,
                    Rigs = rigSettings.Select(rs => new WebRigSettings
                    {
                        Id = rs.Id,
                        RigId = rs.RigId,
                        Name = rs.Rig.NameFormatted,
                        ReferenceId = rs.Rig.ReferenceId,
                        ShortName = rs.ShortName,
                        RigType = (int)rs.RigType
                    }).ToList()
                };
                customerList.Add(webCustomerSettings);
            }

            return StatusCode(200,new { ok = true, items = customerList });
        }

        [HttpPost]
        [Route("[controller]/savecustomersettings")]
        public async Task<IActionResult> SaveCustomerSettingsAsync([FromBody] WebCustomerSettings entity)
        {
            if (entity == null)
            {
                var message = "Incorrect input data.";
                return StatusCode(400, new { ok = false, message });
            }
            //save ccnItem
            var dbEntity = await _upgradeMatrixService.SaveCustomerSettingsAsync(new CustomerSettings
            {
                Id = entity.Id,
                RootCustomerId = entity.CustomerId,
                Key = "Status",
                Value = "Active"
            }, HttpContext.GetCurrentUserRoles(), HttpContext.GetCurrentUser());
            if (dbEntity == null)
                return StatusCode(500, new { ok = false, message = "Something went wrong or you don't have access to perform this action." });
            var rigSettings = await _upgradeMatrixService.GetRigSettingsForCustomerAsync(entity.CustomerId, HttpContext.GetCurrentUserRoles());
            foreach (var rig in rigSettings)
            {
                if (entity.Rigs.All(r => r.Id != rig.Id))
                {
                    await _upgradeMatrixService.DeleteRigSettings(rig.Id, HttpContext.GetCurrentUserRoles());
                }
            }
            foreach (var rig in entity.Rigs)
            {
                var dbRigEntity = await _upgradeMatrixService.SaveRigSettings(new RigSettings
                {
                    Id = rig.Id,
                    RigId = rig.RigId,
                    ShortName = rig.ShortName,
                    RigType = (UpgradeMatrixRigType)rig.RigType
                }, HttpContext.GetCurrentUserRoles(), HttpContext.GetCurrentUser());
                if (dbRigEntity == null)
                    return StatusCode(500, new { ok = false, message = "Something went wrong or you don't have access to perform this action." });
            }
            return StatusCode(200,new { ok = true, message = $"Customer has been successfully saved." });
        }

        [HttpPost]
        [Route("[controller]/archivecustomersettings/{id}")]
        public async Task<IActionResult> ArchiveCustomerSettingsAsync(long id)
        {
            var dbCustomer = await _upgradeMatrixService.GetCustomerSettingsAsync(id, HttpContext.GetCurrentUserRoles());
            if (dbCustomer == null)
            {
                var message = "Incorrect input data.";
                return StatusCode(400, new { ok = false, message });
            }

            //save ccnItem
            var dbEntity = await _upgradeMatrixService.SaveCustomerSettingsAsync(new CustomerSettings
            {
                Id = id,
                RootCustomerId = dbCustomer.RootCustomerId,
                Key = "Status",
                Value = "Archived"
            }, HttpContext.GetCurrentUserRoles(), HttpContext.GetCurrentUser());
            if(dbEntity != null)
                return StatusCode(200,new { ok = true, message = $"Customer has been successfully archived." });
            return StatusCode(500, new { ok = false, message = "Something went wrong or you don't have access to perform this action." });
        }

        [HttpGet]
        [Route("[controller]/getequipmentcategories")]
        public async Task<IActionResult> GetEquipmentCategoriesAsync()
        {
            var equipmentCategories = await _upgradeMatrixService.GetEquipmentCategoriesAsync(HttpContext.GetCurrentUserRoles());
            return StatusCode(200,new {ok = true, items = equipmentCategories});
        }

        [HttpPost]
        [Route("[controller]/saveequipmentcategory")]
        public async Task<IActionResult> SaveEquipmentCategoryAsync([FromBody] EquipmentCategory entity)
        {
            if (entity == null || string.IsNullOrEmpty(entity.ShortName))
                return StatusCode(400,new { ok = false, message = "Missing data." });
            var dbEqCategory = await _upgradeMatrixService.GetEquipmentCategoryByNameAsync(entity.ShortName, HttpContext.GetCurrentUserRoles());
            if (dbEqCategory != null)
                return StatusCode(400,new { ok = false, message = "Equipment with defined name already exists." });
            var dbEntity = await _upgradeMatrixService.SaveEquipmentCategoryAsync(entity, HttpContext.GetCurrentUserRoles(), HttpContext.GetCurrentUser());
            if(dbEntity != null)
                return StatusCode(200,new { ok = true, message = $"Equipment has been successfully saved." });
            return StatusCode(500, new { ok = false, message = "Something went wrong or you don't have access to perform this action." });
        }

        [HttpPost]
        [Route("[controller]/deleteequipmentcategory/{id}")]
        public async Task<IActionResult> DeleteEquipmentCategory(long id)
        {
            if (id <= 0)
            {
                var message = "Incorrect input data.";
                return StatusCode(400, new { ok = false, message });
            }

            var entityDeleted = await _upgradeMatrixService.DeleteEquipmentCategory(id, HttpContext.GetCurrentUserRoles());
            if(entityDeleted)
                return StatusCode(200,new { ok = true, message = $"Equipment has been successfully deleted." });
            return StatusCode(500,new { ok = false, message = $"Something went wrong." });
        }

        [HttpGet]
        [Route("[controller]/getccnitems")]
        public async Task<IActionResult> GetCcnItemsAsync(long customerId, bool open)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            var ccnItems = new List<WebCcnItem>();
            var dbCcnItems = await _upgradeMatrixService.GetCcnItemsAsync(customerId, roles);
            var dbCcnStates = _upgradeMatrixService.GetCcnStates(dbCcnItems.Select(ci => ci.Id), roles);

            foreach (var ccnItem in dbCcnItems)
            {
                var rigStates = dbCcnStates.ContainsKey(ccnItem.Id) ? dbCcnStates[ccnItem.Id] : null;
                var itemOpen = rigStates != null && !rigStates.All(s =>
                                   s.Status == UpgradeMatrixItemStatus.Completed ||
                                   s.Status == UpgradeMatrixItemStatus.Na);
                if (open != itemOpen) continue;
                ccnItems.Add(GetWebCcnItem(ccnItem, itemOpen, rigStates, roles));
            }

            System.Diagnostics.Debug.WriteLine("GetCcnItems end - " + DateTime.Now);

            return StatusCode(200,new {ok = true, items = ccnItems});
        }

        private static WebCcnItem GetWebCcnItem(CcnItem ccnItem, bool itemOpen, List<CcnState> rigStates, CombinedUserRoles roles)
        {
            return new WebCcnItem
            {
                Id = ccnItem.Id,
                CustomerId = ccnItem.RootCustomerId,
                Comments = ccnItem.Comments,
                Description = ccnItem.Description,
                EquipmentCategoryId = ccnItem.EquipmentCategoryId,
                EquipmentCategoryName = ccnItem.EquipmentCategory.ShortName,
                PackageUpgrade = (int) ccnItem.PackageUpgrade,
                RigType = (int) ccnItem.RigType,
                Open = itemOpen,
                RigStates = rigStates == null
                    ? new List<WebItemState>()
                    : rigStates.Select(cs => new WebItemState
                    {
                        Id = cs.Id,
                        ItemId = cs.CcnItemId,
                        RigSettingsId = cs.RigSettingsId,
                        RigShortName = cs.RigSettings.ShortName,
                        CcnName = cs.CcnName,
                        DocumentPath = cs.DocumentPath,
                        EnquiryCreationEnabled = cs.Status == UpgradeMatrixItemStatus.Undefined && roles.CanCreate(rigId:cs.RigSettings.RigId), 
                        Status = (int) cs.Status,
                        ApprovedBy = roles.IsUpgradeMatrixAdmin() && cs.ApprovedBy != null ? $"{cs.ApprovedBy.Upn}" : ""
                    })
            };
        }

        [HttpGet]
        [Route("[controller]/getquotationitems")]
        public async Task<IActionResult> GetQuotationItemsAsync(long customerId, bool open)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            var quotationItems = new List<WebQuotationItem>();
            var dbQuotationItems = await _upgradeMatrixService.GetQuotationItemsAsync(customerId, roles);
            var dbQuotationStates = _upgradeMatrixService.GetQuotationStates(dbQuotationItems.Select(ci => ci.Id), roles);

            foreach (var quotationItem in dbQuotationItems)
            {
                var rigStates = dbQuotationStates.ContainsKey(quotationItem.Id)
                    ? dbQuotationStates[quotationItem.Id]
                    : null;
                var itemOpen = rigStates != null && !rigStates.All(s =>
                                   s.Status == UpgradeMatrixItemStatus.Completed ||
                                   s.Status == UpgradeMatrixItemStatus.Na);
                if (open != itemOpen) continue;
                quotationItems.Add(new WebQuotationItem
                {
                    Id = quotationItem.Id,
                    Description = quotationItem.Description,
                    EquipmentCategoryId = quotationItem.EquipmentCategoryId,
                    EquipmentCategoryName = quotationItem.EquipmentCategory.ShortName,
                    Heading = quotationItem.Heading,
                    Benefits = quotationItem.Benefits,
                    QuotationReference = quotationItem.QuotationReference,
                    Open = itemOpen,
                    RigStates = rigStates == null
                        ? new List<WebItemState>()
                        : rigStates.Select(cs => new WebItemState
                        {
                            Id = cs.Id,
                            ItemId = cs.QuotationItemId,
                            RigSettingsId = cs.RigSettingsId,
                            RigShortName = cs.RigSettings.ShortName,
                            EnquiryCreationEnabled = cs.Status == UpgradeMatrixItemStatus.Undefined && roles.CanCreate(rigId:cs.RigSettings.RigId),
                            Status = (int) cs.Status
                        })
                });
            }

            return StatusCode(200,new {ok = true, items = quotationItems});
        }

        [Route("[controller]/getdownloadlink/{id}")]
        [HttpGet]
        public async Task<IActionResult> DownloadCcnDocumentAsync(int id)
        {
            if (id <= 0)
            {
                var message = "Incorrect input data.";
                return StatusCode(400, new {ok = false, message});
            }

            try
            {
                var ccnState = await _upgradeMatrixService.GetCcnStateAsync(id, HttpContext.GetCurrentUserRoles());
                if (ccnState == null || string.IsNullOrEmpty(ccnState.DocumentPath))
                    return StatusCode(400, new {ok = false, message = "Data is missing."});

                var blobName = ccnState.DocumentPath;
                var blobUri = await _blobSasGenerator.GetSasBlobUri(blobName, StorageConfig.BlobCcnContainerName, BlobAccountSasPermissions.Read);

                return StatusCode(200, new {ok = true, downloadLink = blobUri});
            }
            catch (Exception ex)
            {
                var message = $"Error while downloading ccn document: {ex.Message}";
                return StatusCode(500, new {ok = false, message});
            }
        }

        [HttpPost]
        [Route("[controller]/saveccnitem")]
        public async Task<IActionResult> SaveCcnItemAsync([FromBody]WebCcnItem entity)
        {
            if (entity == null)
            {
                var message = "Incorrect input data.";
                return StatusCode(400, new { ok = false, message });
            }
            //save ccnItem
            var dbEntity = await _upgradeMatrixService.SaveCcnItemAsync(new CcnItem
            {
                Id = entity.Id,
                RootCustomerId = entity.CustomerId,
                EquipmentCategoryId = entity.EquipmentCategoryId,
                Description = entity.Description,
                Comments = entity.Comments,
                PackageUpgrade = (UpgradeMatrixPackageUpgrade)entity.PackageUpgrade,
                RigType = (UpgradeMatrixRigType)entity.RigType
            }, HttpContext.GetCurrentUserRoles(), HttpContext.GetCurrentUser());
            if (dbEntity == null)
                return StatusCode(500, new { ok = false, message = "Something went wrong or you don't have access to perform this action." });
            var newItem = false;
            if (entity.Id == 0)
            {
                entity.Id = dbEntity.Id;
                newItem = true;
            }
            //save ccnStates
            foreach (var rigState in entity.RigStates)
            {
                var downloadPath = rigState.DocumentPath;
                
                if (rigState.Id > 0)
                {
                    var ccnState = await _upgradeMatrixService.GetCcnStateAsync(rigState.Id, HttpContext.GetCurrentUserRoles());
                    if (ccnState != null && ccnState.DocumentPath != null && ccnState.DocumentPath != rigState.DocumentPath)
                    {
                        //first delete file if rigState.documentPath != original.documentPath
                        var blobName = ccnState.DocumentPath;
                        var blobUri = await _blobSasGenerator.GetSasBlobUri(blobName, StorageConfig.BlobCcnContainerName, BlobAccountSasPermissions.Read | BlobAccountSasPermissions.Delete);

                        BlobClient blobClient = new BlobClient(blobUri, null);
                        if (blobClient.Exists())
                        {
                            //delete it
                            await blobClient.DeleteAsync();
                            downloadPath = null;
                        }
                    }
                }
                var dbState = await _upgradeMatrixService.SaveCcnStateAsync(new CcnState
                {
                    Id = rigState.Id,
                    CcnItemId = dbEntity.Id,
                    RigSettingsId = rigState.RigSettingsId,
                    Status = (UpgradeMatrixItemStatus)rigState.Status,
                    CcnName = rigState.CcnName,
                    DocumentPath = downloadPath
                }, HttpContext.GetCurrentUserRoles(), HttpContext.GetCurrentUser());
                if(dbState == null)
                    return StatusCode(500, new { ok = false, message = "Something went wrong or you don't have access to perform this action." });
                if (rigState.Id == 0)
                    rigState.Id = dbState.Id;
            }
            if (newItem && entity.RigStates.Any())
            {
                await _queueSender.EnqueueUpgradeMatrixEventAsync(entity.Id, UpgradeMatrixItemType.Ccn);
            }

            return StatusCode(200, new {ok = true, message = $"CCN item has been successfully saved.", item = entity});
        }

        [Route("[controller]/uploadccndocument/{id}")]
        [HttpPost]
        public async Task<IActionResult> UploadCcnDocument(int id, [FromForm(Name = "ccnDocument")] IFormFile ccnDocument)
        {
            if (id <= 0 || ccnDocument == null)
            { 
                return StatusCode(400, new { ok = false, message = "Missing data." });
            }

            try
            {
                var ccnState = await _upgradeMatrixService.GetCcnStateAsync(id, HttpContext.GetCurrentUserRoles());
                if (ccnState == null)
                    return StatusCode(400, new { ok = false, message = "Incorrect input data." });
                var blobName = $"{ccnState.RigSettings.Rig.ReferenceId}\\{ccnDocument.FileName}";

                var blobUri = await _blobSasGenerator.GetSasBlobUri(blobName, StorageConfig.BlobCcnContainerName,
                    BlobAccountSasPermissions.Write | BlobAccountSasPermissions.Read | BlobAccountSasPermissions.Delete);
                BlobClient blobClient = new BlobClient(blobUri, null);
                if (blobClient.Exists())
                {
                    //delete it
                    await blobClient.DeleteAsync();
                }
                await using Stream uploadFileStream = ccnDocument.OpenReadStream();
                var response = await blobClient.UploadAsync(uploadFileStream);
                if (response.GetRawResponse().Status == (int)HttpStatusCode.Created)
                {
                    ccnState.DocumentPath = blobName.Replace("\\", "/");
                    var dbEntity = await _upgradeMatrixService.SaveCcnStateAsync(ccnState, HttpContext.GetCurrentUserRoles(), HttpContext.GetCurrentUser());
                    if (dbEntity == null)
                        return StatusCode(500, new { ok = false, message = "Something went wrong or you don't have access to perform this action." });
                    return StatusCode(200, new { ok = true, message = "Ccn document has been successfully uploaded." });
                }
                return StatusCode(500, new { ok = false, message = "Something went wrong." });
            }
            catch (Exception ex)
            {
                var message = $"Error while uploading image : {ex.Message}";
                return StatusCode(500, new { ok = false, message });
            }
        }


        [HttpPost]
        [Route("[controller]/savequotationitem")]
        public async Task<IActionResult> SaveQuotationItemAsync([FromBody]WebQuotationItem entity)
        {
            if (entity == null)
            {
                var message = "Incorrect input data.";
                return StatusCode(400, new { ok = false, message });
            }

            var roles = HttpContext.GetCurrentUserRoles();
            var user = HttpContext.GetCurrentUser();
            //save quotationItem
            var dbEntity = await _upgradeMatrixService.SaveQuotationItemAsync(new QuotationItem
            {
                Id = entity.Id,
                RootCustomerId = entity.CustomerId,
                EquipmentCategoryId = entity.EquipmentCategoryId,
                Description = entity.Description,
                Heading = entity.Heading,
                Benefits = entity.Benefits,
                QuotationReference = entity.QuotationReference
               
            }, roles, user);
            if (dbEntity == null)
                return StatusCode(500, new { ok = false, message = "Something went wrong or you don't have access to perform this action." });
            var newItem = false;
            if (entity.Id == 0)
            {
                entity.Id = dbEntity.Id;
                newItem = true;
            }
            //save quotationStates
            foreach (var rigState in entity.RigStates)
            {
                var dbState = await _upgradeMatrixService.SaveQuotationStateAsync(new QuotationState
                {
                    Id = rigState.Id,
                    QuotationItemId = dbEntity.Id,
                    RigSettingsId = rigState.RigSettingsId,
                    Status = (UpgradeMatrixItemStatus)rigState.Status
                }, roles, user);
                if (dbState == null)
                    return StatusCode(500, new { ok = false, message = "Something went wrong or you don't have access to perform this action." });
            }

            if (newItem && entity.RigStates.Any())
            {
                await _queueSender.EnqueueUpgradeMatrixEventAsync(entity.Id, UpgradeMatrixItemType.Quotation);
            }
            return StatusCode(200, new { ok = true, message = $"Quotation item has been successfully saved.", item = entity });
        }

        [HttpPost]
        [Route("[controller]/deleteccnitem/{id}")]
        public async Task<IActionResult> DeleteCcnItem(long id)
        {
            if (id <= 0)
            {
                var message = "Incorrect input data.";
                return StatusCode(400, new { ok = false, message });
            }

            var entityDeleted = await _upgradeMatrixService.DeleteCcnItemAsync(id, HttpContext.GetCurrentUserRoles());
            if(entityDeleted)
                return StatusCode(200,new { ok = true, message = $"CCN item has been successfully deleted." });
            return StatusCode(500,new { ok = false, message = $"Something went wrong." });
        }

        [HttpPost]
        [Route("[controller]/deletequotationitem/{id}")]
        public async Task<IActionResult> DeleteQuotationItem(long id)
        {
            if (id <= 0)
            {
                var message = "Incorrect input data.";
                return StatusCode(400, new { ok = false, message });
            }

            var entityDeleted = await _upgradeMatrixService.DeleteQuotationItemAsync(id, HttpContext.GetCurrentUserRoles());
            if(entityDeleted)
                return StatusCode(200,new { ok = true, message = $"Quotation item has been successfully delete." });
            return StatusCode(500,new { ok = false, message = $"Something went wrong." });
        }

        [HttpPost]
        [Route("[controller]/approveccnforrig/{id}")]
        public async Task<IActionResult> ApproveCcnForRig(long id)
        {
            if (id <= 0)
            {
                var message = "Incorrect input data.";
                return StatusCode(400, new { ok = false, message });
            }

            var entityApproved = await _upgradeMatrixService.ApproveCcnStateAsync(id, HttpContext.GetCurrentUserRoles(), HttpContext.GetCurrentUser());
            if (entityApproved == null)
            {
                return StatusCode(200,new { ok = true, message = $"Something went wrong. The status hasn't been approved." });
            }
            
            return StatusCode(200,new { ok = true, message = $"The status has been successfully approved." });
        }
    }
}