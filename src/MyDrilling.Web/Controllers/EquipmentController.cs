﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core;
using MyDrilling.Core.Entities.Equipment;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Web.QueryParameters;
using MyDrilling.Web.UserDetails;

namespace MyDrilling.Web.Controllers
{
    [Route("equipment")]
    public class EquipmentController : Controller
    {
        private readonly MyDrillingDb _db;

        public EquipmentController(MyDrillingDb db)
        {
            _db = db;
        }

        [HttpGet("")]
        [HttpGet("index")]
        public IActionResult Index()
        {
            var drillDown = HttpContext.GetDrillDownParameters();
            var routeValues = drillDown.ToRouteValueDictionary();
            if (drillDown.HasEquipment) return RedirectToAction("Detail", "Equipment", routeValues);
            if (drillDown.HasProductCode) return RedirectToAction("Type", "Equipment", routeValues);
            if (drillDown.HasRig) return RedirectToAction("Index", "Rigs", routeValues);
            return RedirectToAction("Index", "Home", routeValues);
        }

        [HttpGet("type")]
        public IActionResult Type()
        {
            var drillDown = HttpContext.GetDrillDownParameters();
            var routeValues = drillDown.ToRouteValueDictionary();
            if (drillDown.HasEquipment) return RedirectToAction("Detail", "Equipment", routeValues);
            if (!drillDown.HasProductCode)
            {
                return RedirectToAction("Index", "Home", routeValues);
            }
            return View("Type", drillDown.ProductCode);
        }
        
        [HttpGet("detail")]
        public IActionResult Detail(long id = 0)
        {
            var drillDown = HttpContext.GetDrillDownParameters();
            if (!drillDown.HasEquipment)
            {
                return RedirectToAction("Index", "Home", drillDown.ToRouteValueDictionary());
            }
            return View("Detail", drillDown.Equipment);
        }

        [HttpGet("search")]
        public async Task<IActionResult> Search(string term, long rigId, bool rootOnly, int limit = 10)
        {
            term = term.ToLower();

            var drillDown = HttpContext.GetDrillDownParameters();
            rigId = rigId == 0 ? drillDown.RigId : rigId;

            var equipmentSearchResult = new List<SelectListItem>{ new SelectListItem(default(int).ToString(), LanguageConstants.NotAvailableShort)};

            var equipmentSearch = await _db.Equipments
                .ApplyRootFilter(rootOnly)
                .ApplyPermission(HttpContext.GetCurrentUserRoles())
                .ApplyRigFilter(rigId)
                .ApplyProductFilter(drillDown.ProductCode)
                .ApplySearchFilter(term)
                .ExcludeUninstalled()
                .Take(limit)
                .Select(x => new SelectListItem(x.Id.ToString(), x.Name.ToCamelCase()))
                .ToArrayAsync();

            equipmentSearchResult.AddRange(equipmentSearch);

            return Json(equipmentSearchResult);
        }

        [HttpGet("filtersearch")]
        public async Task<IActionResult> FilterSearch(string term, long rigId, int limit = 10)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            term = term.ToLower();

            var drillDown = HttpContext.GetDrillDownParameters();
            rigId = rigId == 0 ? drillDown.RigId : rigId;

            var equipmentSearch = await _db.Equipments
                .ApplyPermission(roles)
                .ApplyRigFilter(rigId)
                .ExcludeUninstalled()
                .ApplyProductFilter(drillDown.ProductCode)
                .ApplySearchFilter(term)
                .OrderBy(x => x.Name)
                .Take(limit)
                .Select(x => new
                {
                    referenceId = x.ReferenceId, 
                    name = x.ToFilterName(), 
                    rigId = x.RigReferenceId,
                    rigName = rigId > 0 ? "" : x.Rig.NameFormatted,
                    product = x.ProductCode
                })
                .ToArrayAsync();
            return Json(equipmentSearch);
        }

        [HttpGet("filtersearchtype")]
        public async Task<IActionResult> FilterSearchEquipmentType(string term, long rigId, int limit = 10)
        {
            var roles = HttpContext.GetCurrentUserRoles();
            term = term.ToLower();

            var drillDown = HttpContext.GetDrillDownParameters();
            rigId = rigId == 0 ? drillDown.RigId : rigId;

            var equipmentSearch = await _db.Equipments
                .ApplyPermission(roles)
                .ApplyRigFilter(rigId)
                .ExcludeProductless()
                .ExcludeUninstalled()
                .ApplySearchTypeFilter(term)
                .ToArrayAsync();
            var eqSearch = equipmentSearch
                .GroupBy(x => x.ProductCode)
                .OrderBy(x => x.Key)
                .Take(limit)
                .Select(x => new
                {
                    productCode = x.Key,
                    productName = x.First().ProductNameFormatted
                }).ToArray();
                
            return Json(eqSearch);
        }
    }

    public static class EquipmentFilterExt
    {
        public static IQueryable<Equipment> ApplyRootFilter(this IQueryable<Equipment> query, bool rootOnly) =>
            rootOnly ? query.Where(x => x.ParentId == null) : query;

        public static IQueryable<Equipment> ApplySearchFilter(this IQueryable<Equipment> query, string term) =>
            string.IsNullOrEmpty(term) ? query : query.Where(x => x.Name.Contains(term) || x.Material.Contains(term));

        public static IQueryable<Equipment> ApplySearchTypeFilter(this IQueryable<Equipment> query, string term) =>
            string.IsNullOrEmpty(term) ? query : query.Where(x => x.ProductName.Contains(term) || x.ProductCode.Contains(term));

        public static IQueryable<Equipment> ApplyProductFilter(this IQueryable<Equipment> query, string productCode) =>
            string.IsNullOrEmpty(productCode) ? query : query.Where(x => x.ProductCode == productCode);

        public static IQueryable<Equipment> ApplyRigFilter(this IQueryable<Equipment> query, long rigId) =>
            rigId == 0 ? query : query.Where(x => x.RigId == rigId);

        public static IQueryable<Equipment> ExcludeUninstalled(this IQueryable<Equipment> query)
        {
            return query.Where(x => !x.IsDeleted);
        }

        public static IQueryable<Equipment> ExcludeProductless(this IQueryable<Equipment> query)
        {
            return query.Where(x => x.ProductCode != null);
        }
    
        public static string ToFilterName(this Equipment equipment)
        {
            return (!string.IsNullOrEmpty(equipment.SerialNumber) ? equipment.SerialNumber.TrimStart('0').TrimStart() + " " : "")
                   + equipment.NameFormatted
                   + (!string.IsNullOrEmpty(equipment.Material) ? " (" + equipment.Material + ")" : "");
        }
    }
}