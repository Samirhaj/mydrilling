﻿using MyDrilling.Core.Entities.Bulletin;
using MyDrilling.Core.Entities.Documentation;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Web.Models.Rig;
using MyDrilling.Web.Models.Search;

namespace MyDrilling.Web
{
    public static class Css
    {
        public static string AltClass(int index) => index % 2 == 0 ? " class=alt" : string.Empty;

        public static string FirstColumnClass(int index, int columns) => index % columns > 0 ? string.Empty : " class=first";

        public static string ActivityCss(ActivityViewModel item )
        {
            switch (item.ActivityEntityType)
            {
                case ActivityEntityType.Enquiry:
                    if (item.ActivityType == ActivityType.Closed) return "metaRequestClosed";
                    return item.EntityState == "Draft" ? "metaRequestDraft" : "metaRequest";
                case ActivityEntityType.Bulletin:
                    return "metaBulletin";
                case ActivityEntityType.Document:
                    return "metaDocument";
                default:
                    return "";
            }
        }

        public static string SearchResultItemCss(this string itemType)
        {
            switch (itemType)
            {
                case nameof(Enquiry):
                    return "metaRequest";
                case nameof(ZaBulletin):
                    return "metaBulletin";
                case nameof(TechnicalReport):
                    return "metaDocument download-link";
                case nameof(UserManual):
                    return "metaDocument";
                default:
                    return "";
            }
        }
    }
}
