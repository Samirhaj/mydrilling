﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using MyDrilling.Infrastructure.Storage;
using MyDrilling.Infrastructure.Storage.Queue;

namespace MyDrilling.Web.HealthChecks
{
    public class StorageQueueHealthCheck : IHealthCheck
    {
        private readonly IQueueClientFactory _queueClientFactory;

        public StorageQueueHealthCheck(IQueueClientFactory queueClientFactory)
        {
            _queueClientFactory = queueClientFactory;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context,
            CancellationToken cancellationToken = new CancellationToken())
        {
            var healthCheckTasks = new[]
            {
                _queueClientFactory.GetClient(StorageConfig.SyncEnquiryToSapQueue).ExistsAsync(cancellationToken),
                _queueClientFactory.GetClient(StorageConfig.FileRequestQueue).ExistsAsync(cancellationToken),
                _queueClientFactory.GetClient(StorageConfig.EnquiryEventQueue).ExistsAsync(cancellationToken),
                _queueClientFactory.GetClient(StorageConfig.UpgradeMatrixEventQueue).ExistsAsync(cancellationToken),
                _queueClientFactory.GetClient(StorageConfig.RequestSparepartPriceQueue).ExistsAsync(cancellationToken)
            };

            await Task.WhenAll(healthCheckTasks);

            return healthCheckTasks.All(x => x.Result.Value)
                ? HealthCheckResult.Healthy()
                : HealthCheckResult.Unhealthy();
        }
    }
}
