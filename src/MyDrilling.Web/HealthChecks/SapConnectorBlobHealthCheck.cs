﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using MyDrilling.Core.Entities;
using MyDrilling.Infrastructure.Data;
using Dapper;

namespace MyDrilling.Web.HealthChecks
{
    public class SapConnectorBlobHealthCheck : IHealthCheck
    {
        private readonly string _myDrillingConnectionString;
        private readonly TimeSpan _threshold;

        public SapConnectorBlobHealthCheck(IConfiguration config)
        {
            _myDrillingConnectionString = config.GetConnectionString("MyDrillingDb");
            _threshold = config.GetValue<TimeSpan>("SapConnectorHealthRefreshThreshold");
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            SapConnectorStatus state;
            await using(var conn = await _myDrillingConnectionString.CreateRawSqlConnection())
            {
                state = (await conn.QueryAsync<SapConnectorStatus>("SELECT TOP 1 * FROM SapConnectorStatuses")).SingleOrDefault();
            }

            return state != null && state.IsBlobsOk(_threshold)
                ? HealthCheckResult.Healthy()
                : HealthCheckResult.Unhealthy();
        }
    }
}
