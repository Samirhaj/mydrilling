﻿using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using MyDrilling.Infrastructure.Storage;

namespace MyDrilling.Web.HealthChecks
{
    public class StorageBlobHealthCheck : IHealthCheck
    {
        private readonly BlobServiceClient _blobServiceClient;

        public StorageBlobHealthCheck(BlobServiceClient blobServiceClient)
        {
            _blobServiceClient = blobServiceClient;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, 
            CancellationToken cancellationToken = new CancellationToken())
        {
            var healthCheckTasks = new[]
            {
                _blobServiceClient.GetBlobContainerClient(StorageConfig.BlobCcnContainerName).ExistsAsync(cancellationToken),
                _blobServiceClient.GetBlobContainerClient(StorageConfig.BlobUserImagesContainerName).ExistsAsync(cancellationToken),
                _blobServiceClient.GetBlobContainerClient(StorageConfig.EnquiriesContainerName).ExistsAsync(cancellationToken),
                _blobServiceClient.GetBlobContainerClient(StorageConfig.TemporaryDocumentsContainerName).ExistsAsync(cancellationToken)
            };

            await Task.WhenAll(healthCheckTasks);

            return healthCheckTasks.All(x => x.Result.Value)
                ? HealthCheckResult.Healthy()
                : HealthCheckResult.Unhealthy();
        }
    }
}
