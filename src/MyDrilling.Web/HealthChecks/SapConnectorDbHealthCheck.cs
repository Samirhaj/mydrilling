﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using MyDrilling.Core.Entities;
using MyDrilling.Infrastructure.Data;

namespace MyDrilling.Web.HealthChecks
{
    public class SapConnectorDbHealthCheck : IHealthCheck
    {
        private readonly string _myDrillingConnectionString;
        private readonly TimeSpan _threshold;

        public SapConnectorDbHealthCheck(MyDrillingDb db,
            IConfiguration config)
        {
            _myDrillingConnectionString = config.GetConnectionString("MyDrillingDb");
            _threshold = config.GetValue<TimeSpan>("SapConnectorHealthRefreshThreshold");
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            SapConnectorStatus state;
            await using (var conn = await _myDrillingConnectionString.CreateRawSqlConnection())
            {
                state = (await conn.QueryAsync<SapConnectorStatus>("SELECT TOP 1 * FROM SapConnectorStatuses")).SingleOrDefault();
            }

            return state != null && state.IsDbOk(_threshold)
                ? HealthCheckResult.Healthy()
                : HealthCheckResult.Unhealthy();
        }
    }
}
