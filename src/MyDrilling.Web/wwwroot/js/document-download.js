﻿$(function () {
    $('.document-link').on('click',
        function (e) {
            documentLinkClicked(e, $(this));
        });
    $('.document-link-sp').on('click',
        function (e) {
            documentSpLinkClicked(e, $(this));
        });
});

function documentLinkClicked(e, $this, url, predefProgressSpan, insertAfter) {
    e.preventDefault();
    var docLink = url ? url : $this.attr('href');
    var progressSpan = predefProgressSpan ? predefProgressSpan : $('<span/>').addClass('progressbar').height(16);
    if (insertAfter)
        progressSpan.insertAfter($this);
    else
        progressSpan.insertBefore($this);
    $.ajax({
        url: docLink,
        type: 'POST'
    }).done(function (response) {
        if (response && isNaN(response)) {
            location.href = response;
            if (progressSpan)
                progressSpan.remove();
        } else {
            getDocument(response, progressSpan);
        }
    }).fail(function (response) {
        alert(response.responseText);
        if (progressSpan)
            progressSpan.remove();
    });
}

function documentSpLinkClicked(e, $this, url) {
    e.preventDefault();
    var docLink = url ? url : $this.attr('href');
    var progressSpan = $('<span/>').addClass('progressbar').height(16);
    progressSpan.insertBefore($this);
    setTimeout(function () {
        location.href = docLink;
        if (progressSpan)
            progressSpan.remove();
    }, 1500);
}

function getDocument(requestId, progressSpan) {
    $.ajax({
        url: '/asyncoperations/download/' + requestId,
        type: 'POST'
    }).done(function (response) {
        if (!response) {
            setTimeout(function () { getDocument(requestId, progressSpan); }, 3000);
        } else {
            location.href = response;
            if (progressSpan)
                progressSpan.remove();
        }
    }).fail(function (response) {
        alert(response.responseText);
        if (progressSpan)
            progressSpan.remove();
    });
}