﻿(function ($) {
    $.widget("akersolutions.finduserdialog", {
        options: {
            onUserSelected: null,
            onCancel: null,
            action: null,
            singleMode: true
        },

        _create: function () {
            var me = this;

            $("#btnFudSelectUser").click(function () {
                if (me.options.singleMode) {
                    if (me._getUpn()) {
                        var user = {
                            upn: me._getUpn(),
                            userId: me._getUserId(),
                            displayname: me._getDisplayName()
                        };
                        if (typeof me.options.onUserSelected == "function") {
                            me.options.onUserSelected(user);
                        }
                    }
                } else {
                    var users = [];
                    $('#selected-users input.user').each(function(i, obj) {
                        var $obj = $(obj);
                        users.push({
                            upn: $obj.attr("data-upn"),
                            userId: $obj.attr("data-id"),
                            displayname: $obj.attr("data-displayname")
                        });
                    });

                    if (users.length > 0) {
                        if (typeof me.options.onUserSelected == "function")
                            me.options.onUserSelected(users);
                    }
                }

                $.fancybox.close();
            });

            $("#btnFudCancel").click(function () {
                //onCancel event handler
                if (typeof me.options.onCancel == "function")
                    me.options.onCancel();
                
                $.fancybox.close();
            });

            $("#SearchValue").autocomplete({
                source: function (request, response) {
                    me._setUserInfo(null);
                    applySafeSearchTerm(request, $("#SearchValue"));
                    $.ajax({
                        url: me.options.action,
                        dataType: "json",
                        data: {
                            searchValue: request.term
                        },
                        success: function (data) {
                            response(data);
                        },
                        error: handleAjaxErrors
                    });
                },
                select: function (event, ui) {
                    if (ui.item) {
                        if (me.options.singleMode) {
                            me._setUserInfo(ui.item);
                        } else {
                            if ($('#selected-users input[data-id=' + ui.item.userId + ']').length === 0) {
                                var $userBtn = $('<input>').attr({
                                    "type": "button",
                                    "value": ui.item.value + '   X',
                                    "data-upn": ui.item.id,
                                    "data-id": ui.item.userId,
                                    "data-displayname": ui.item.value,
                                    "class": "user",
                                    "title": "Click to remove"
                                });

                                $userBtn.click(function() {
                                    $(this).remove();
                                });

                                $('#selected-users').append($userBtn);
                            }
                        }
                    }
                    me._updateDialogState();

                    if (!me.options.singleMode) {
                        this.value = "";
                        //prevent autocomplete to put selected item into search input
                        return false;
                    }
                }
            });

            me._updateDialogState();
        },
        
        _getUpn: function() {
            return $("#UserUpn").val();
        },

        _getUserId: function() {
            return $("#MyDrillingUserId").val();
        },
        
        _setUserInfo: function (userInfo) {
            var me = this;
            if (userInfo) {
                $("#UserUpn").val(userInfo.id);
                $("#MyDrillingUserId").val(userInfo.userId);
            } else {
                $("#UserUpn").val("");
                $("#MyDrillingUserId").val("");
            }
            me._updateDialogState();
        },
        
        _getDisplayName: function () {
            return $("#SearchValue").val();
        },
        
        _updateDialogState: function () {
            var me = this;
            var enabled = false;

            if (me.options.singleMode) {
                enabled = me._getUpn();
            } else {
                enabled = $('#selected-users input.user').length > 0;
            }

            if (enabled) {
                $("#btnFudSelectUser").removeClass("disabled");
                $("#btnFudSelectUser").removeAttr('disabled');
            } else {
                $("#btnFudSelectUser").addClass("disabled");
                $("#btnFudSelectUser").attr('disabled', 'disabled');
            }
        }
    });
}(jQuery));