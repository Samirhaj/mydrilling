﻿$(document).ready(function () {
    //MakeAutoCompleteField(".ac_input_users", '/admin/users/JsonUsers', "/admin/users/detail/?UserGlobalId=", default_alert_text);
    $("#tabs").tabs();

    //openConfirmDialog($("#createuserstatus"), "User successfully created");

    $('#RigListItems').change(function () {
        var rigvalue = this.value; // get the selected value
        var rigname = $('#RigListItems option:selected').text();
        var addRigLink = $("#addRigLink");
        addRigLink.attr('href', "/admin/users/addrig?rigid=" + rigvalue + "&rigname=" + rigname);
        addRigLink.attr('data-ajax-success', "removeRigListOption('" + rigvalue + "')");
        if (rigvalue != "0") {
            addRigLink.show();
        }
        else {
            addRigLink.hide();
        }
    });

    $(".collapser").click(function () {
        $(this).parent().parent("li").children("ol").toggle();

        if ($(this).hasClass("open")) {
            $(this).removeClass("open").addClass("closed");
        }
        else {
            $(this).removeClass("closed").addClass("open");
        }
    });

    $(".collapsPanel").delegate(".collapsListHeader", 'click', function (e) {
        $(this).parent().children("div.hide").toggle();
        if ($(this).hasClass("bgBlue")) {
            $(this).removeClass("bgBlue");
            $(this).parent().removeClass("open").addClass("closed");
        }
        else {
            $(this).addClass("bgBlue");
            $(this).parent().removeClass("closed").addClass("open");
        }
    });

    refreshAdmin();
});

function refreshAdmin()
{
    $(".stdTable tbody tr:even").addClass("alt");
    //Refresh();
}

function openAdminDialog(dialogTitle) {
    jQuery.validator.unobtrusive.parse('#adminDialog');
    var dialogElement = $("#adminDialog");
    dialogElement.dialog({
        title: dialogTitle,
        autoOpen: false,
        modal: true,
        closeOnEscape: true,
        width: 605,
        resizable: false
    });

    dialogElement.dialog("open");
    refreshAdmin();
}

function closeAdminDialog() {

    $("#adminDialog").dialog('close');
}

function makeSelectUserAutocomplete(id) {
    MakeAutoCompleteFieldForUserInput(".acAccessRoleAddUser", "/admin/accessprofiles/jsonusers/?UserGlobalId=" + id, default_alert_text);
}

function MakeAutoCompleteFieldForUserInput(selector, jasonDataUri, linkHrefBase, default_alert_text) {
    $(selector).autocomplete({
        source: function (reqeust, response) {
            GetJSONForAutoComplete(reqeust, response, jasonDataUri, default_alert_text, selector, this.element);
        },
        select: function (event, ui) {
            $("#SelectedUser").val(ui.item.id);
        },
        delay: 1000
    });
}

function RefreshCustomerAccess(userId, uri) {
    var element = $('#userCustomers');
    Refresh(element);
    $.ajax({
        type: "Get",
        url: uri,
        success: function (html) {
            element.html(html);
            Refresh(element);
        },
        error: handleAjaxErrors,
        complete: function () {

        }
    });
}
function RefreshAdminCustomerAccess(userId, uri) {
    var element = $('#userAdminCustomers');
    Refresh(element);
    $.ajax({
        type: "Get",
        url: uri,
        success: function (html) {
            element.html(html);
            Refresh(element);
        },
        error: handleAjaxErrors,
        complete: function () {

        }
    });
}

function ResetUserDropDownAndFacilityDropDown() {
    $("#SelectedUserDropdown").val("").trigger("change");
}

function complete(result) {
    var isError = $('span.field-validation-error', result.responseText).length > 0;
    if (isError) {
        $('#adminDialog').html(result.responseText);
        Refresh('#adminDialog');
    } else {
        $('#adminDialog .field-validation-error').hide();
        $('#adminDialog .input-validation-error').removeClass('input-validation-error');
        closeAdminDialog();
        $('#userAreas').html(result.responseText);
        Refresh('body');
    }
}
function openCreateUserStatusDialog() {
    $("#mainWithRightBar").dialog("open");
}

function removeRigListOption(rigId) {
    $("#RigListItems option[value='" + rigId + "']").remove();
    if ($("#RigListItems option").size() == 1) {
        $("#RigListItems").hide();
        $("#addRigLink").hide();
    }
}