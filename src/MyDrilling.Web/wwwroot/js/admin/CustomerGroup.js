﻿/// <reference path="../../../Scripts/jQuery/jquery-1.7.js" />
/// <reference path="../../../Scripts/jQuery/jquery-1.7.1-vsdoc.js" />

var CustomerGroupParams = new function () {
    this.polling = false;
    this.refreshTimeOut = 20000;
    this.SoldToPartyListUri = "";
    this.CustomerErrorsUri = "";
    this.SoldToPartyListChange = "0";
    this.ConfirmDeleteSpidMessage = "Delete soldToParty?";
    this.ConfirmDeleteCustomerMessage = "Delete customer?";
    this.ConfirmSetDefaultCustomerUnit = "Set as default?";
    this.PendingCustomerEdit = false;
    this.PendingEditUri = "";
    this.EditPendingPolling = false;
    this.PagedCustomerPolling = false;
    this.CustomerStatusPollingUri = "";
    this.CustomerStatusPollingGid = "";
    this.TicketAckUrl = "";

};

function SubmitCustomerSoldToPartyForm(formid) {
    var $form = $('#' + formid);
        $.ajax({
            type: "POST",
            url: $form.attr( 'action' ),
            data: $form.serialize(),
            error: handleAjaxErrors,
            success: function (response) {
                RefreshSoldToParty();
            }
        });
}

function SubmitDeleteCustomerForm(formid, refreshUri) {
    var $form = $('#' + formid);
    $.ajax({
        type: "POST",
        url: $form.attr('action'),
        data: $form.serialize(),
        error: handleAjaxErrors
        ,
        success: function (response) {
            RefreshPagedCustomers(refreshUri);
        }
    });
}

function InitSubmitbuttonDisabeling() {
    $('input[type=submit]').click(function (evt) {
        evt.preventDefault();
        var self = $(this);
        var frm = self.closest('form');
        frm.validate();
        if (frm.valid()) {
            frm.submit();
            self.attr('disabled', 'disabled');
            self.attr('value', 'Please wait....');
        }
    });
}

function StartEditPendingPolling() {
    if (CustomerGroupParams.polling == true) {
        window.setTimeout(function () { StartEditPendingPolling(); }, CustomerGroupParams.refreshTimeOut);
    } else {
        CustomerGroupParams.polling = true;
        $.ajax({
            type: "POST",
            url: CustomerGroupParams.CustomerStatusPollingUri,
            data: { "gid": CustomerGroupParams.CustomerStatusPollingGid },
            dataType: "json",
            success: function (customerStatus) {
                UpdateSoldToPartyListIfChanges(customerStatus['customerUnitsChange']);
                SetSubmitButtonVisibility(customerStatus['pendingEdit']);
                UpdateCustomerErrorsIfChanges(customerStatus['failedUpdates']);
            },
            error: handleAjaxErrors,
            complete: function () {
                CustomerGroupParams.polling = false;
                window.setTimeout(function () { StartEditPendingPolling(); }, CustomerGroupParams.refreshTimeOut);
            }
        });
    }
}
function UpdateCustomerErrorsIfChanges(anyErrors) {
    if (anyErrors)
        RefreshCustomerErrors();
}
function UpdateSoldToPartyListIfChanges(customerUnitChangeTimeStamp)
{
    if (customerUnitChangeTimeStamp != CustomerGroupParams.SoldToPartyListChange) {
        CustomerGroupParams.SoldToPartyListChange = customerUnitChangeTimeStamp;
        RefreshSoldToParty();
    }
}
function SetSubmitButtonVisibility(pendingEdit) {
    if (pendingEdit) {
        ToggleHidingForPendingEdit();
    } else {
        ToggleHidingForNoPendingEdit();
    }
}


function RefreshCustomerErrors() {
    var element = $('#customerUpdateErrors');
    RefreshElement(CustomerGroupParams.CustomerErrorsUri, element);
}

function RefreshSoldToParty() {
    var element = $('#SoldToPartyIds');
    RefreshElement(CustomerGroupParams.SoldToPartyListUri, element);
}
function RefreshPagedCustomers(uri) {
    var element = $('#PagedCustomerGroups');
    if (CustomerGroupParams.PagedCustomerPolling == false) {
        CustomerGroupParams.PagedCustomerPolling = true;
        RefreshElement(uri, element, function () { CustomerGroupParams.PagedCustomerPolling = false; });
    }
   
}
function RefreshElement(uri, element, completeFunction) {
    StartLoading();
    $.ajax({
        type: "Get",
        url: uri,
        success: function (html) {
            element.html(html);
            Refresh(element);
        },
        error: handleAjaxErrors,
        complete: function () {
            StopLoading();
            if (completeFunction != null) {
                completeFunction();
            }
                
        }
    });
}
function AknowledgeCustomerUnitTicket(ticketid) {
    $.post(CustomerGroupParams.TicketAckUrl, { "tid": ticketid },
        function (data) {
            if (data == true) {
                RefreshSoldToParty();
            } 

        }, "json");
}
function AknowledgeCustomerDeleteTicket(ticketid,refreshUri) {
    $.post(CustomerGroupParams.TicketAckUrl, { "tid": ticketid },
        function (data) {
            if (data == true) {
                RefreshPagedCustomers(refreshUri);
            }

        }, "json");
}
function AcknowledgeCustomerUpdateTicket(ticketId) {
    $.post(CustomerGroupParams.TicketAckUrl, { "tid": ticketId },
      function (data) {
          if (data == true) {
              RefreshCustomerErrors();
          }

      }, "json");
}
function confirmDeleteSpid(customerUnitId) {
    var formId = "deleteForm" + customerUnitId;

    akerDialog.confirm(CustomerGroupParams.ConfirmDeleteSpidMessage + '<br/>ID: ' + customerUnitId, function() {
        SubmitCustomerSoldToPartyForm(formId);
    });

    return false;
}

function confirmDeleteCustomer(customerRow, refreshUri,customerName) {
    var formId = "deleteCustomerForm" + customerRow;
    
    akerDialog.confirm(CustomerGroupParams.ConfirmDeleteCustomerMessage + '<br/>Name: ' + customerName, function () {
        SubmitDeleteCustomerForm(formId, refreshUri);
    });

    return false;
}

function confirmSetDefaultCustomerUnit(customerUnitId) {
    var formId = 'setDefaultForm' + customerUnitId;
    if (confirm(CustomerGroupParams.ConfirmSetDefaultCustomerUnit + '\n ID: ' + customerUnitId)) {
        SubmitCustomerSoldToPartyForm(formId);
    }
}
function ToggleHidingForPendingEdit() {
    $('#customerSubmitButton').hide();
    $('#buttonLoadingIcon').show();
}
function ToggleHidingForNoPendingEdit() {
    $('#customerSubmitButton').show();
    $('#buttonLoadingIcon').hide();
}
function InitCustomerEditPending() {

    if (CustomerGroupParams.PendingCustomerEdit == 'True') {
        ToggleHidingForPendingEdit();
    } else {
        ToggleHidingForNoPendingEdit();
    }
}