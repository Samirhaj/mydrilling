﻿/// <reference path="../../../Scripts/jQuery/jquery-1.7.js" />
/// <reference path="../../../Scripts/jQuery/jquery-1.7.1-vsdoc.js" />

var AccessProfile = function ($) {

    var my = {};

    function setUpSumChecks() {
        $("#roleList input.moduleChk").each(function (chk) {            
            var initialChecked = false;
            $(this).parents("li").first().find("input:visible:not(.moduleChk)").each(function () {
                if ($(this).is(":checked")) initialChecked = true;
            });
            $(this).attr("checked", initialChecked).attr("disabled", "disabled").siblings().first().toggleClass('inactive', !initialChecked);
        });
    }

    my.init = function () {
        // Set up pyjamas on outer li's
        $("#roleList").children("li:even").addClass("alt");
        setUpSumChecks();
        // Setup click-events for "section" checkboxes
        // clicking these should select/deselect all children
        $("#roleList input.moduleChk").on("click", function () {
            var checked = $(this).is(':checked');
            $(this).parents("li").first().find("input[type='checkbox'][disabled!='disabled']").attr("checked", checked);
        });

        $("#roleList input[type = 'checkbox'][disabled != 'disabled']").on("click", setUpSumChecks);
    };

    return my;
} (jQuery);