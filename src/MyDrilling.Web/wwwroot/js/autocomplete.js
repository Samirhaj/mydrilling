﻿function applySafeSearchTerm(request, elements) {
    var safeSearchTerm = getSafeSearchTerm(request.term);
    if (safeSearchTerm !== request.term &&
        elements != null &&
        elements.hasOwnProperty("length") &&
        !isNaN(elements.length) &&
        elements.length > 0 &&
        elements[0].value !== "undefined") {
        elements[0].value = safeSearchTerm;
    }
    request.term = safeSearchTerm;
}

var invalidCharsRegexPattern = /[<>={}]/gi;

function getSafeSearchTerm(value) {
    return value.replace(invalidCharsRegexPattern, " ");
}

//Search autocomplete
function bindSearchAutocomplete(inputId, url) {
    var $searchAutocomplete = $('#' + inputId);
    var noResultsLabel = 'No results.';
    $searchAutocomplete.autocomplete({
        minLength: 3,
        source: function (request, response) {
            applySafeSearchTerm(request, this.element);
            $.ajax({
                url: url,
                dataType: "json",
                data: {
                    term: request.term
                },
                success: function (data) {
                    if (!data.length) {
                        response([{ name: noResultsLabel, value: '' }]);
                    } else {
                        response($.map(data,
                            function (item) {
                                return {
                                    name: item.name,
                                    value: item.name,
                                    type: item.entityType,
                                    referenceId: item.referenceId,
                                    url: item.url
                                }
                            }));
                    }
                }
            });
        },
        select: function (e, ui) {
            if (ui.item.name == noResultsLabel) {
                e.preventDefault();
            }
            else {
                $searchAutocomplete.val(ui.item.value);
                if (ui.item.type == "TechnicalReport") {
                    documentLinkClicked(e, $(this), ui.item.url);
                } else {
                    window.location.href = ui.item.url;
                }
            }
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        var itemWrapper = $('<div />');
        var itemLink = $('<a />').attr('class', 'tinyMeta ' + getSearchItemClass(item.type) + ' noprint');
        itemLink.append("<span>" + item.name + "</span>");
        if (item.referenceId)
            itemLink.append("<span>" + item.referenceId + "</span>");
        itemWrapper.append(itemLink);
        return $("<li class='search-autocomplete-item'></li>").data("item.autocomplete", item)
            .attr('title', item.type + " - " + item.name)
            .append(itemWrapper)
            .appendTo(ul);
    };
}
function getSearchItemClass(itemType) {
    var iconClassName = '';
    switch (itemType) {
        case "Enquiry":
            iconClassName = 'metaRequest';
            break;
        case "ZaBulletin":
            iconClassName = 'metaBulletin';
            break;
        case "TechnicalReport":
        case "UserManual":
            iconClassName = 'metaDocument';
            break;
    }
    return iconClassName;
}