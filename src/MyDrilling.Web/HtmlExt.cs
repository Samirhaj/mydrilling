﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using MyDrilling.Web.QueryParameters;

namespace MyDrilling.Web
{
    public static class HtmlExt
    {
        private const string ControllerRouteName = "controller";
        private const string ActionRouteName = "action";
        public static string AllFilterValue = "[all]";
        public static string StdDateFormat = "d MMM yyyy";
        public static string StdDateTimeFormat = StdDateFormat + " HH:mm";

        public static IHtmlContent FilterColumn(this IHtmlHelper html, string filterParameter,
            string actionName = null, string controllerName = null, bool addAllItem = false)
        {
            if (!html.ViewData.TryGetValue(filterParameter, out var rawFilterValues))
            {
                return new HtmlString("");
            }

            var filterValues = (SelectListItem[])rawFilterValues;
            var filter = html.ViewContext.HttpContext.GetFilterParameters();

            actionName ??= html.ViewContext.RouteData.Values[ActionRouteName].ToString();
            controllerName ??= html.ViewContext.RouteData.Values[ControllerRouteName].ToString();

            var dl = new TagBuilder("dl") { TagRenderMode = TagRenderMode.Normal };
            if (filter.FilterField == filterParameter)
            {
                dl.AddCssClass("currentFilter");
            }

            var dt = new TagBuilder("dt") { TagRenderMode = TagRenderMode.Normal };
            dt.InnerHtml.AppendHtml("Filter");
            var dd = new TagBuilder("dd") { TagRenderMode = TagRenderMode.Normal };
            var ul = new TagBuilder("ul") {TagRenderMode = TagRenderMode.Normal};
            ul.AddCssClass("filterMenu ui-menu ui-widget ui-widget-content ui-corner-all");

            if (addAllItem)
            {
                var firstLi = new TagBuilder("li") { TagRenderMode = TagRenderMode.Normal };
                firstLi.AddCssClass("ui-menu-item");
                firstLi.InnerHtml.AppendHtml(html.FilterLink(filterParameter, AllFilterValue, "- All -", actionName, controllerName));
                ul.InnerHtml.AppendHtml(firstLi);
            }

            foreach (var filterItem in filterValues.OrderBy(x => x.Text))
            {
                var filterLi = new TagBuilder("li") {TagRenderMode = TagRenderMode.Normal};
                filterLi.AddCssClass("ui-menu-item");
                filterLi.InnerHtml.AppendHtml(html.FilterLink(filterParameter, filterItem.Value, filterItem.Text,
                    actionName, controllerName));
                ul.InnerHtml.AppendHtml(filterLi);
            }

            dl.InnerHtml.AppendHtml(dt);
            dd.InnerHtml.AppendHtml(ul);
            dl.InnerHtml.AppendHtml(dd);

            return dl;
        }

        public static IHtmlContent FilterLink(this IHtmlHelper html, string filterField, string filterValue, string filterText,
            string actionName, string controllerName)
        {
            var drillDown = html.ViewContext.HttpContext.GetDrillDownParameters();
            var order = html.ViewContext.HttpContext.GetOrderParameters();
            var pager = html.ViewContext.HttpContext.GetPagerParameters();

            //string filtVal = filterValue;
            //if (filterField == string.Empty)
            //    filtVal = "[all]";

            if (filterValue == null)
            {
                return new HtmlString("");
            }

            return html.ActionLink(filterText, actionName, controllerName,
                drillDown.ToRouteValueDictionary(order, pager, new FilterParameters(filterField, filterValue)), 
                new Dictionary<string, object> { { "class", "ui-corner-all" } });
        }

        public static IHtmlContent SortLink(this IHtmlHelper html, string linkText, string sortField,
            string actionName = null, string controllerName = null, object additionalParams = null)
        {
            var drillDown = html.ViewContext.HttpContext.GetDrillDownParameters();
            var filter = html.ViewContext.HttpContext.GetFilterParameters();
            var order = html.ViewContext.HttpContext.GetOrderParameters();
            var pager = html.ViewContext.HttpContext.GetPagerParameters();
            var current = sortField == order.SortField;

            var cssClass = "sort-column";
            if (current)
            {
                cssClass = order.IsAsc ? "sort-column-current-desc" : "sort-column-current";
            }

            cssClass += " sort-link";
            //if (linkText == "~")
            //    cssClass += " emptyHeader";

            actionName ??= html.ViewContext.RouteData.Values[ActionRouteName].ToString();
            controllerName ??= html.ViewContext.RouteData.Values[ControllerRouteName].ToString();

            var sortParams = SortParameters.Create(sortField, current ? order.OppositeSortDirection : SortParameters.Asc);
            return html.ActionLink(linkText, actionName, controllerName,
                drillDown.ToRouteValueDictionary(
                    filter, 
                    sortParams, 
                    additionalParams, 
                    pager.PageSize != PagerParameters.PageSizes[0] ? new { page = 1, itemsPrPage = pager.PageSize } : null),
                new Dictionary<string, object> { { "class", cssClass } });
        }
        public static IHtmlContent LiWithLabel(this object s, string label)
        {
            return s?.ToString().LiWithLabel(label);
        }

        public static IHtmlContent LiWithLabel(this string s, string label)
        {
            if (!string.IsNullOrWhiteSpace(s))
                return new HtmlString("<li><label>" + label + ":</label>" + s + "</li>");
            return null;
        }

        public static IHtmlContent LiWithLabel(this string s, string label, string id)
        {
            if (!string.IsNullOrWhiteSpace(s))
                return new HtmlString("<li><label>" + label + ":</label><span id=\"" + id + "\">" + s + "</span></li>");
            return null;
        }

        public static IHtmlContent LiWithLabel(this DateTime date, TimeZoneInfo tz, string label, bool includeTime = false)
        {
            HtmlString dateString = includeTime 
                ? new HtmlString("<span class=\"fullDate datetime\" title=\"" + date.ToString(StdDateTimeFormat) + " UTC\">" + date.ToLocal(tz).ToString(StdDateTimeFormat) + "</span>") 
                : new HtmlString("<span class=\"fullDate datetime\" title=\"" + date.ToString(StdDateFormat) + " UTC\">" + date.ToLocal(tz).ToString(StdDateFormat) + "</span>"); ;
            return new HtmlString("<li><label>" + label + ":</label>" + dateString + "</li>");
        }

        public static IHtmlContent LiWithLabel(this DateTime? s, TimeZoneInfo tz, string label, bool includeTime = false)
        {
            return s?.LiWithLabel(tz, label, includeTime);
        }

        public static IHtmlContent ConvertLineBreaks(this IHtmlHelper html, string text)
        {
            return string.IsNullOrEmpty(text) 
                ? new HtmlString("") 
                : new HtmlString(string.Join("<br/>", text.Split("\n").Select(html.Encode)));
        }

        public static IHtmlContent MessageBox(this string message, string type)
        {
            var builder = new TagBuilder("cite");
            builder.MergeAttribute("class", "Message Message" + type);
            builder.InnerHtml.AppendHtml("<em></em>" + message);
            return builder;
        }

        public static IHtmlContent TimeOfAction(this IHtmlHelper helper, DateTime? date)
        {
            return date.HasValue 
                ? new HtmlString($"<span style='display:inline-block; white-space:nowrap;'>{date.Value:d MMM yyyy}</span> <span style='display:inline-block; white-space:nowrap;'>{date.Value:HH:mm}</span>") 
                : new HtmlString("");
        }

        public static HtmlString StdShortDateNoTooltip(this DateTime? date, TimeZoneInfo tz)
        {
            return date.HasValue ? date.Value.StdShortDateNoTooltip(tz) : new HtmlString(string.Empty);
        }

        public static HtmlString StdShortDateNoTooltip(this DateTime date, TimeZoneInfo tz)
        {
            return new HtmlString("<span class=\"shortDate datetime\">" + date.ToLocal(tz).ToString(StdDateFormat) + "</span>");
        }
    }
}
