﻿using Microsoft.AspNetCore.Routing;

namespace MyDrilling.Web.QueryParameters
{
    public interface IParameters
    {
        RouteValueDictionary ToRouteValueDictionary();
    }

    public static class ParametersExt
    {
        public static RouteValueDictionary ToRouteValueDictionary(this IParameters @params, params object[] extraParamsArray)
        {
            var rvd = @params.ToRouteValueDictionary();

            if (extraParamsArray != null && extraParamsArray.Length > 0)
            {
                foreach (var extraParams in extraParamsArray)
                {
                    var extraRvd = extraParams is IParameters parameters
                        ? parameters.ToRouteValueDictionary()
                        : new RouteValueDictionary(extraParams);

                    foreach (var extraParam in extraRvd)
                    {
                        //set or replace
                        rvd[extraParam.Key.ToLower()] = extraParam.Value;
                    }
                }
            }

            return rvd;
        }
    }
}
