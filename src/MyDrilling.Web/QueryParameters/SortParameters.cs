﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace MyDrilling.Web.QueryParameters
{
    public class SortParameters : IParameters
    {
        public const string OrderDirectionParameter = "sortdirection";
        public const string OrderFieldParameter = "sortfield";
        public const string Asc = "asc";
        public const string Desc = "desc";

        public SortParameters(HttpContext httpContext)
        {
            SortDirection = httpContext.Request.Query[OrderDirectionParameter];
            SortField = httpContext.Request.Query[OrderFieldParameter];

            if (string.IsNullOrEmpty(SortDirection) || string.IsNullOrEmpty(SortField)
                                                    || SortDirection != Asc && SortDirection != Desc)
            {
                SortDirection = null;
                SortField = null;
            }
        }

        private SortParameters(string sortField, string sortDirection)
        {
            if (string.IsNullOrEmpty(sortField)) throw  new NullReferenceException(nameof(sortField));
            if (string.IsNullOrEmpty(sortDirection)) throw new NullReferenceException(nameof(sortDirection));
            if (sortDirection != Asc && sortDirection != Desc) throw new ArgumentOutOfRangeException(nameof(sortDirection));

            SortDirection = sortDirection;
            SortField = sortField;
        }

        public static SortParameters Create(string sortField, string direction) => new SortParameters(sortField, direction);

        public static SortParameters CreateAsc(string sortField) => new SortParameters(sortField, Asc);

        public static SortParameters CreateDesc(string sortField) => new SortParameters(sortField, Desc);

        public string SortDirection { get; }
        public string SortField { get; }
        public bool IsSet => !string.IsNullOrEmpty(SortDirection) && !string.IsNullOrEmpty(SortField);
        public bool IsAsc => SortDirection == Asc;
        public string OppositeSortDirection => IsAsc ? Desc : Asc;

        public RouteValueDictionary ToRouteValueDictionary()
        {
            return new RouteValueDictionary
            {
                [OrderDirectionParameter] = SortDirection,
                [OrderFieldParameter] = SortField
            };
        }
    }

    public static class OrderParametersExtensions
    {
        public static SortParameters GetOrderParameters(this HttpContext httpContext) => new SortParameters(httpContext);
    }
}
