﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace MyDrilling.Web.QueryParameters
{
    public class FilterParameters : IParameters
    {
        public const string FilterFieldParameter = "filterfield";
        public const string FilterValueParameter = "filtervalue";

        public FilterParameters(HttpContext httpContext)
        {
            FilterField = httpContext.Request.Query[FilterFieldParameter];
            FilterValue = httpContext.Request.Query[FilterValueParameter];

            if (string.IsNullOrEmpty(FilterField) || string.IsNullOrEmpty(FilterValue))
            {
                FilterField = null;
                FilterValue = null;
            }
        }

        public FilterParameters(string filterField, string filterValue)
        {
            if (string.IsNullOrEmpty(filterField)) throw new NullReferenceException(nameof(filterField));
            if (string.IsNullOrEmpty(filterValue)) throw new NullReferenceException(nameof(filterValue));

            FilterField = filterField;
            FilterValue = filterValue;
        }

        public string FilterField { get; }
        public string FilterValue { get; }
        
        public RouteValueDictionary ToRouteValueDictionary()
        {
            return new RouteValueDictionary
            {
                [FilterFieldParameter] = FilterField,
                [FilterValueParameter] = FilterValue
            };
        }
    }

    public static class FilterParametersExtensions
    {
        public static FilterParameters GetFilterParameters(this HttpContext httpContext) => new FilterParameters(httpContext);
    }
}
