﻿using System;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using MyDrilling.Web.Lookups;

namespace MyDrilling.Web.QueryParameters
{
    public class DrillDownParameters : IParameters
    {
        public const string RigParameter = "rig";
        public const string ProductParameter = "equipmenttype";
        public const string EquipmentParameter = "equipment";
        public const string DurationParameter = "duration";
        public const string FromDateParameter = "fromdate";
        public const string ToDateParameter = "todate";

        public DrillDownParameters(HttpContext httpContext)
        {
            Rig = httpContext.Request.Query[RigParameter];
            RigId = !string.IsNullOrEmpty(Rig) ? httpContext.GetRigId(Rig) : default;
            ProductCode = httpContext.Request.Query[ProductParameter];
            Equipment = httpContext.Request.Query[EquipmentParameter];
            EquipmentId = !string.IsNullOrEmpty(Equipment) ? httpContext.GetEquipmentId(Equipment) : default;
            Duration = httpContext.Request.Query[DurationParameter];
            FromDate = httpContext.Request.Query[FromDateParameter];
            ToDate = httpContext.Request.Query[ToDateParameter];
        }

        public string Rig { get; }
        public bool HasRig => RigId > 0;
        public long RigId { get; }

        public string ProductCode { get; }
        public bool HasProductCode => !string.IsNullOrEmpty(ProductCode);

        public string Equipment { get; }
        public bool HasEquipment => EquipmentId > 0;
        public long EquipmentId { get; }

        public string Duration { get; }
        public bool HasDuration => !string.IsNullOrEmpty(Duration) && DurationInDays > 0;
        public int DurationInDays => GetDurationInDays();
        public string FromDate { get; }
        public bool HasFromDate => !string.IsNullOrEmpty(FromDate);
        public string ToDate { get; }
        public bool HasToDate => !string.IsNullOrEmpty(ToDate);

        public RouteValueDictionary ToRouteValueDictionary()
        {
            return new RouteValueDictionary
            {
                [RigParameter] = Rig,
                [ProductParameter] = ProductCode,
                [EquipmentParameter] = Equipment,
                [DurationParameter] = Duration,
                [FromDateParameter] = FromDate,
                [ToDateParameter] = ToDate
            };
        }

        private int GetDurationInDays()
        {
            if (string.IsNullOrEmpty(Duration)) return 0;
            var durationNumber = Duration
                .Replace("P", "", StringComparison.InvariantCultureIgnoreCase)
                .Replace("D", "", StringComparison.InvariantCultureIgnoreCase);
            return int.TryParse(durationNumber, out int durationInDays) ? durationInDays : 0;
        }
    }

    public static class DrillDownParametersExtensions
    {
        public static DrillDownParameters GetDrillDownParameters(this HttpContext httpContext) => new DrillDownParameters(httpContext);
    }
}
