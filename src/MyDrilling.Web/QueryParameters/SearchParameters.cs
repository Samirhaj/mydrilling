﻿using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Http;

namespace MyDrilling.Web.QueryParameters
{
    public class SearchParameters : IParameters
    {
        public const string SearchTermParameter = "term";
        public const string IncludeEnquiriesParameter = "inclEnquiry";
        public const string IncludeBulletinsParameter = "inclBulletin";
        public const string IncludeDocumentationParameter = "inclDocumentation";
        

        public SearchParameters(HttpContext httpContext)
        {
            SearchTerm = httpContext.Request.Query[SearchTermParameter];
            IncludeEnquiries = bool.TryParse(httpContext.Request.Query[IncludeEnquiriesParameter], out var inclEnq) && inclEnq;
            IncludeBulletins = bool.TryParse(httpContext.Request.Query[IncludeBulletinsParameter], out var inclBul) && inclBul;
            IncludeDocumentation = bool.TryParse(httpContext.Request.Query[IncludeDocumentationParameter], out var inclDoc) && inclDoc;
        }

        public SearchParameters(string term, bool includeEnquiries, bool includeBulletins, bool includeDocumentation )
        {
            SearchTerm = term;
            IncludeEnquiries = includeEnquiries;
            IncludeBulletins = includeBulletins;
            IncludeDocumentation = includeDocumentation;
        }

        public string SearchTerm { get; }
        public bool IncludeEnquiries { get; }
        public bool IncludeBulletins { get; }
        public bool IncludeDocumentation { get; }

        public RouteValueDictionary ToRouteValueDictionary()
        {
            return new RouteValueDictionary
            {
                [SearchTermParameter] = SearchTerm,
                [IncludeEnquiriesParameter] = IncludeEnquiries.ToString().ToLower(),
                [IncludeBulletinsParameter] = IncludeBulletins.ToString().ToLower(),
                [IncludeDocumentationParameter] = IncludeDocumentation.ToString().ToLower()
            };
        }
    }
    public static class SearchParametersExtensions
    {
        public static SearchParameters GetSearchParameters(this HttpContext httpContext) => new SearchParameters(httpContext);
    }
}
