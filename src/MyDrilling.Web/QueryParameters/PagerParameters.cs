﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;

namespace MyDrilling.Web.QueryParameters
{
    public class PagerParameters : IParameters
    {
        public const string PageSizeParameter = "itemsPrPage";
        public const string CurrentPageNumberParameter = "page";

        public static readonly int[] PageSizes = { 10, 20, 50, 100 };
        public const int StartsFrom = 1;

        public PagerParameters(HttpContext httpContext)
        {
            PageSize = PageSizes[0];
            CurrentPageNumber = StartsFrom;

            var pageSizeRaw = httpContext.Request.Query[PageSizeParameter];
            if (!string.IsNullOrEmpty(pageSizeRaw) && int.TryParse(pageSizeRaw, out var parsedPageSize) &&
                parsedPageSize > 0)
            {
                PageSize = parsedPageSize;
            }

            var currentPageNumberRaw = httpContext.Request.Query[CurrentPageNumberParameter];
            if (!string.IsNullOrEmpty(currentPageNumberRaw) && int.TryParse(currentPageNumberRaw, out var parsedCurrentPageNumberRaw) &&
                parsedCurrentPageNumberRaw > 0)
            {
                CurrentPageNumber = parsedCurrentPageNumberRaw;
            }
        }

        public PagerParameters(int pageNumber)
        {
            if(pageNumber <= 0) throw  new ArgumentOutOfRangeException(nameof(pageNumber));

            PageSize = PageSizes[0];
            CurrentPageNumber = pageNumber;
        }

        public int PageSize { get; }
        public int CurrentPageNumber { get; }

        //public int[] GetAvailablePageSizes(int totalCount) => PageSizes.Where(x => totalCount > x).ToArray();
        public int[] GetAvailablePageSizes(int totalCount)
        {
            var availablePageSizes = new List<int>();
            foreach (var pageSize in PageSizes)
            {
                availablePageSizes.Add(pageSize);
                if (pageSize >= totalCount)
                {
                    break;
                }
            }
            return availablePageSizes.ToArray();
        }

        public RouteValueDictionary ToRouteValueDictionary()
        {
            return new RouteValueDictionary
            {
                [PageSizeParameter] = PageSize,
                [CurrentPageNumberParameter] = CurrentPageNumber
            };
        }
    }

    public static class PagerParametersExtensions
    {
        public static PagerParameters GetPagerParameters(this HttpContext httpContext) => new PagerParameters(httpContext);
    }
}
