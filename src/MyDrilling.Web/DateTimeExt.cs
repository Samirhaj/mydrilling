﻿using System;
using Microsoft.AspNetCore.Html;

namespace MyDrilling.Web
{
    public static class DateTimeExt
    {
        public const string StdDateFormat = "d MMM yyyy";
        public const string StdDateTimeFormat = StdDateFormat + " HH:mm";

        public static string ToUniversalSortable(this DateTime? dt) => dt.HasValue ? dt.Value.ToUniversalSortable() : string.Empty;

        public static string ToUniversalSortable(this DateTime dt) => dt.ToString("u");

        public static string ToFullString(this DateTime? dt) => dt.HasValue ? dt.Value.ToFullString() : string.Empty;

        public static string ToFullString(this DateTime dt) => dt.ToString("d MMM yyyy, HH:mm");

        public static DateTime? ToLocal(this DateTime? dt, TimeZoneInfo tzi) => dt?.ToLocal(tzi);

        public static DateTime ToLocal(this DateTime dt, TimeZoneInfo tzi)
        {
            var utcDate = DateTime.SpecifyKind(dt, DateTimeKind.Utc);
            return tzi == null
                ? utcDate
                : TimeZoneInfo.ConvertTimeFromUtc(utcDate, tzi);
        }

        public static HtmlString CleanDateTime(this DateTime date, TimeZoneInfo tzi)
        {
            return new HtmlString(date.ToLocal(tzi).ToString(StdDateTimeFormat));
        }

        public static HtmlString CleanDateTime(this DateTime? date, TimeZoneInfo tzi)
        {
           return date.HasValue
           ? new HtmlString(date.Value.ToLocal(tzi).ToString(StdDateTimeFormat))
           : new HtmlString("");
        }

        public static HtmlString StdShortDate(this DateTime? date, TimeZoneInfo tzi)
        {
            return date.HasValue ? date.Value.StdShortDate(tzi) : new HtmlString(string.Empty);
        }

        public static HtmlString StdShortDate(this DateTime dt, TimeZoneInfo tzi)
        {
            if (tzi == null)
            {
                return new HtmlString(string.Empty);
            }

            var localDate = dt.ToLocal(tzi);
            return new HtmlString("<span class=\"shortDate datetime\" title=\"" + dt.ToString(StdDateFormat) + " UTC\">" + localDate.ToString(StdDateFormat) + "</span>");
        }

        public static HtmlString StdDateTime(this DateTime? date, TimeZoneInfo tzi)
        {
            return date.HasValue ? date.Value.StdDateTime(tzi) : new HtmlString(string.Empty);
        }

        public static HtmlString StdDateTime(this DateTime dt, TimeZoneInfo tzi)
        {
            if (tzi == null)
            {
                return new HtmlString(string.Empty);
            }

            var localDate = dt.ToLocal(tzi);
            return new HtmlString("<span class=\"fullDate datetime\" title=\"" + dt.ToString(StdDateTimeFormat) + " UTC\">" + localDate.ToString(StdDateTimeFormat) + "</span>");
        }

        public static string StdDateTimeString(this DateTime? date, TimeZoneInfo tzi)
        {
            return date.HasValue ? date.Value.StdDateTimeString(tzi) : string.Empty;
        }

        public static string StdDateTimeString(this DateTime date, TimeZoneInfo tzi)
        {
            var localDate = tzi != null ? TimeZoneInfo.ConvertTimeFromUtc(date, tzi) : date;
            return localDate.ToString(StdDateFormat);
        }
    }
}
