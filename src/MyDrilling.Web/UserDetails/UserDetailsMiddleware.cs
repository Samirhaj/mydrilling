﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core;
using MyDrilling.Core.Entities;
using MyDrilling.Infrastructure;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.Services.Permission;

namespace MyDrilling.Web.UserDetails
{
    public class UserDetailsMiddleware
    {
        private readonly RequestDelegate _next;

        public UserDetailsMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context,
            MyDrillingDb db,
            ICombinedUserRoleService combinedUserRoles)
        {
            if (!context.User.Identity.IsAuthenticated)
            {
                await _next(context);
                return;
            }

            var usersQuery = db.Users
                .Include(x => x.RootCustomer)
                .Include(x => x.Settings);
            
            var user = await usersQuery.FindByObjectId(context.User.GetObjectId()).SingleOrDefaultAsync();
            if (user == null)
            {
                user = await usersQuery.FindByUpn(context.User.GetEmail()).SingleOrDefaultAsync();
                if (user == null)
                {
                    context.Items[HttpContextExtensions.CurrentUser] = User.Empty;
                    context.Items[HttpContextExtensions.CurrentUserRoles] = CombinedUserRoles.Empty;

                    await _next(context);
                    return;
                }
            }

            context.Items[HttpContextExtensions.CurrentUser] = user;
            context.Items[HttpContextExtensions.CurrentUserRoles] = await combinedUserRoles.GetRolesAsync(user.Id);

            await _next(context);
        }
    }

    public static class UserDetailsMiddlewareExtensions
    {
        public static IApplicationBuilder UseUserDetails(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<UserDetailsMiddleware>();
        }
    }
}
