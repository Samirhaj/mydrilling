﻿using Microsoft.AspNetCore.Http;
using MyDrilling.Core;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Exceptions;

namespace MyDrilling.Web.UserDetails
{
    public static class HttpContextExtensions
    {
        public const string CurrentUser = "MyDrilling:CurrentUser";
        public const string CurrentUserRoles = "MyDrilling:UserRoles";

        public static User GetCurrentUser(this HttpContext context) =>
            context.GetRequiredData<User>(CurrentUser);

        public static CombinedUserRoles GetCurrentUserRoles(this HttpContext context) => 
            context.GetRequiredData<CombinedUserRoles>(CurrentUserRoles);

        private static T GetRequiredData<T>(this HttpContext context, string key)
            where T : class
        {
            if (!context.Items.TryGetValue(key, out var data)
                || !(data is T))
            {
                throw new BusinessException($"Key '{key}' not found in {nameof(HttpContext)}");
            }

            return (T)data;
        }
    }
}
