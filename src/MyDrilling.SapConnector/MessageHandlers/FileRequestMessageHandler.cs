﻿using System;
using System.IO;
using System.Net;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.MessageContracts;
using MyDrilling.SapConnector.Sap.Executors;

namespace MyDrilling.SapConnector.MessageHandlers
{
    public class FileRequestMessageHandler : IMessageHandler
    {
        private readonly ILogger _logger;
        private readonly MyDrillingDb _db;
        private readonly IReadFileExecutor _readFileVariantExecutor;
        private readonly BlobServiceClient _blobServiceClient;

        public FileRequestMessageHandler(ILogger<FileRequestMessageHandler> logger,
            MyDrillingDb db,
            IReadFileExecutor readFileVariantExecutor,
            BlobServiceClient blobServiceClient)
        {
            _logger = logger;
            _db = db;
            _readFileVariantExecutor = readFileVariantExecutor;
            _blobServiceClient = blobServiceClient;
        }
        public async Task HandleMessageAsync(string queueMessage, CancellationToken stoppingToken)
        {
            var message = JsonSerializer.Deserialize<FileRequestMessage>(queueMessage);
            var operation = await _db.AsyncOperations.FirstAsync(r => r.Id == message.RequestId, stoppingToken);
            var blobName = message.BlobRelativePath;

            _logger.LogInformation("received new file request: async operation {asyncOperationId}, {blobRelativePath}", operation.Id, message.BlobRelativePath);

            BlobContainerClient containerClient = _blobServiceClient.GetBlobContainerClient(Infrastructure.Storage.StorageConfig.TemporaryDocumentsContainerName);
            var blobClient = containerClient.GetBlobClient(blobName);
            if (blobClient.Exists())
            {
                operation.Succeed(blobName);
            }
            else
            {
                var fileWithBinary = message.IsLarge 
                    ? await _readFileVariantExecutor.GetFileVariant(message.BlobRelativePath, message.DocumentName, message.DocumentType, message.AltDocumentId, message.DocumentVersion)
                    : await _readFileVariantExecutor.GetFile(message.BlobRelativePath, message.DocumentName, message.DocumentType, message.AltDocumentId, message.DocumentVersion);
                if (fileWithBinary.Content.Length == 0)
                {
                    operation.Fail(fileWithBinary.Name);
                    _logger.LogInformation("no content found in SAP for async operation {asyncOperationId}", operation.Id);
                }
                else
                {
                    _logger.LogInformation("content found in SAP for async operation {asyncOperationId}: " + fileWithBinary.Content.Length + " bytes", operation.Id);

                    //upload file into temp-doc container
                    //get link, and put link into response
                    blobClient = containerClient.GetBlobClient(blobName);
                    if (blobClient.Exists())
                    {
                        //delete it
                        await blobClient.DeleteAsync(cancellationToken: stoppingToken);
                    }

                    var azureResponse = await blobClient.UploadAsync(new MemoryStream(fileWithBinary.Content), stoppingToken);
                    var azureResponseData = azureResponse.GetRawResponse();
                    if (azureResponseData.Status == (int)HttpStatusCode.Created)
                    {
                        operation.Succeed(blobName);
                    }
                    else
                    {
                        operation.Fail($"Something went wrong on creating file link - {azureResponseData.ReasonPhrase}");
                    }
                }
            }
            await _db.SaveChangesAsync(stoppingToken);
        }
    }
}
