﻿using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.MessageContracts;
using MyDrilling.SapConnector.Sap.Executors;

namespace MyDrilling.SapConnector.MessageHandlers
{
    public class RequestSparepartPriceMessageHandler : IMessageHandler
    {
        private readonly MyDrillingDb _db;
        private readonly IRequestSparepartPriceExecutor _requestPriceExecutor;
        private readonly ILogger _logger;

        public RequestSparepartPriceMessageHandler(MyDrillingDb db, 
            IRequestSparepartPriceExecutor requestPriceExecutor,
            ILogger<RequestSparepartPriceMessageHandler> logger)
        {
            _db = db;
            _requestPriceExecutor = requestPriceExecutor;
            _logger = logger;
        }

        public async Task HandleMessageAsync(string queueMessage, CancellationToken stoppingToken)
        {
            var message = JsonSerializer.Deserialize<RequestSparepartPriceMessage>(queueMessage);
            _logger.LogInformation("received new price request {referenceId} {customerId}", message.ReferenceId, message.CustomerId);

            var price = await _requestPriceExecutor.RequestSparepartPrice(message.ReferenceId, message.CustomerId);
            _logger.LogInformation("found price for {referenceId} {customerId}: " + $"{price.Value} {price.Currency}", message.ReferenceId, message.CustomerId);

            var operation = await _db.AsyncOperations.FirstAsync(r => r.Id == message.RequestId, stoppingToken);
            var response = JsonSerializer.Serialize(price);

            //missing referenceId, quantity or customerId result in 0.0 price
            if (price.Value != 0)
            {
                operation.Succeed(response);
            }
            else
            {
                operation.Fail(response);
            }

            await _db.SaveChangesAsync(stoppingToken);
        }
    }
}