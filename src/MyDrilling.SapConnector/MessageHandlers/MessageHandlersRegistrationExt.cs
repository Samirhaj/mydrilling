﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MyDrilling.SapConnector.MessageHandlers
{
    public static class MessageHandlersRegistrationExt
    {
        public static void AddMessageHandlers(this IServiceCollection services, IConfiguration config)
        {
            var storageConfig = new StorageConfig();
            var storageConfigSection = config.GetSection("StorageConfig");
            storageConfigSection.Bind(storageConfig);
            services.Configure<StorageConfig>(storageConfigSection);

            AddMessageHandler<SyncEnquiryToSapMessageHandler>(services, storageConfig.SyncEnquiryToSapQueueConfig);
            AddMessageHandler<EnquiryNotificationFromSapMessageHandler>(services, storageConfig.EnquiryNotificationFromSapQueueConfig);
            AddMessageHandler<BulletinNotificationFromSapMessageHandler>(services, storageConfig.BulletinNotificationFromSapConfig);
            AddMessageHandler<RequestSparepartPriceMessageHandler>(services, storageConfig.RequestSparepartPriceQueueConfig);
            AddMessageHandler<FileRequestMessageHandler>(services, storageConfig.FileRequestConfig);
        }

        private static void AddMessageHandler<THandler>(IServiceCollection services, StorageConfig.QueueConfig queueConfig) where THandler: class, IMessageHandler
        {
            services.AddScoped<THandler>();
            services.AddHostedService(provider => new QueueListener<THandler>(provider, queueConfig));
        }
    }
}
