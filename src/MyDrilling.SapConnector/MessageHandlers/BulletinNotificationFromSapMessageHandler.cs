﻿using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Core.Entities.Bulletin;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.MessageContracts;
using MyDrilling.SapConnector.Sap;
using MyDrilling.SapConnector.Sap.Executors;
using DateTime = System.DateTime;

namespace MyDrilling.SapConnector.MessageHandlers
{
    public class BulletinNotificationFromSapMessageHandler : IMessageHandler
    {
        private readonly MyDrillingDb _db;
        private readonly ILogger _logger;
        private readonly IReadNotificationExecutor _readNotificationExecutor;
        private readonly IReadZaBulletinExecutor _readZaBulletinExecutor;
        private readonly IReadZbBulletinExecutor _readZbBulletinExecutor;

        public BulletinNotificationFromSapMessageHandler(MyDrillingDb db,
            ILogger<BulletinNotificationFromSapMessageHandler> logger,
            IReadNotificationExecutor readNotificationExecutor,
            IReadZaBulletinExecutor readZaBulletinExecutor,
            IReadZbBulletinExecutor readZbBulletinExecutor)
        {
            _db = db;
            _logger = logger;
            _readNotificationExecutor = readNotificationExecutor;
            _readZaBulletinExecutor = readZaBulletinExecutor;
            _readZbBulletinExecutor = readZbBulletinExecutor;
        }

        public async Task HandleMessageAsync(string queueMessage, CancellationToken stoppingToken)
        {
            var msg = JsonSerializer.Deserialize<BulletinNotificationFromSapMessage>(queueMessage);
            var referenceId = msg.ReferenceId.ToSapReferenceId();
            var customerId = msg.CustomerId;
            _logger.LogInformation("Bulletin - Received new bulletin notification from SAP: referenceId - {referenceId}, customerId - {customerId}", referenceId, customerId);

            //check first if customer is valid (has RootCustomer)
            if (string.IsNullOrEmpty(customerId))
            {
                _logger.LogWarning("No valid customer for given referenceId: {referenceId}", referenceId);
                return;
            }

            var customer = await _db.Customers.FirstOrDefaultAsync(x => x.ReferenceId == customerId, stoppingToken);
            if (customer?.RootCustomerId == null)
            {
                _logger.LogWarning("No valid Root customer for customer {customerId} for given referenceId: {referenceId}", customerId, referenceId);
                return;
            }
            var notification = await _readNotificationExecutor.ReadNotification(referenceId);
            if (notification?.AlmNotifGetDetailResponse?.NotifheaderExport == null)
            {
                _logger.LogWarning("No SAP response for given referenceId: {referenceId}", referenceId);
                return;
            }
            var notificationType = notification.AlmNotifGetDetailResponse.NotifheaderExport.NotifType;
            switch (notificationType)
            {
                case "ZA":
                    _logger.LogInformation("za bulletin detected {referenceId}", referenceId);

                    var zaResult = await _readZaBulletinExecutor.Read(referenceId);
                    if (!zaResult.IsSuccess)
                    {
                        _logger.LogWarning("unable to process za bulletin {referenceId}:" + string.Join(';', zaResult.Errors.Select(x => x.Message)), referenceId);
                        return;
                    }

                    var zaBulletin = zaResult.Value.ZaBulletin;
                    var dbZaBulletin = await InsertOrUpdateZaBulletin(stoppingToken, zaBulletin);
                    await _db.SaveChangesAsync(stoppingToken);

                    _logger.LogInformation("processed za bulletin {referenceId} {zaBulletinId}", referenceId, dbZaBulletin.Id);

                    break;
                case "ZB":
                    _logger.LogInformation("zb bulletin detected {referenceId}", referenceId);

                    var zbResult = await _readZbBulletinExecutor.Read(referenceId, notification);
                    if (!zbResult.IsSuccess)
                    {
                        _logger.LogWarning("unable to process zb bulletin {referenceId}:" + string.Join(';', zbResult.Errors), referenceId);
                        return;
                    }

                    if (zbResult.Value.ZaBulletin == null)
                    {
                        _logger.LogWarning("za bulletin is missing for zb with given reference {referenceId}", referenceId);
                        return;
                    }

                    zaBulletin = zbResult.Value.ZaBulletin;
                    if (zaBulletin.Id == 0)
                    {
                        dbZaBulletin = await InsertOrUpdateZaBulletin(stoppingToken, zaBulletin);
                        await _db.SaveChangesAsync(stoppingToken);

                        _logger.LogInformation("processed za bulletin {referenceId} {zaBulletinId}", zaBulletin.ReferenceId, dbZaBulletin.Id);
                    }

                    var zbBulletin = zbResult.Value.ZbBulletin;
                    zbBulletin.ZaBulletinId = zaBulletin.Id;
                    var dbZbBulletin = await InsertOrUpdateZbBulletin(stoppingToken, zbBulletin, customer.Id);
                    await _db.SaveChangesAsync(stoppingToken);

                    _logger.LogInformation("processed zb bulletin {referenceId} {zbBulletinId}", referenceId, dbZbBulletin.Id);

                    break;
                default:
                    _logger.LogWarning("Wrong notification type {notificationType} for given referenceId: {referenceId}, skip further processing", notificationType, referenceId);
                    return;
            }
        }

        private async Task<ZaBulletin> InsertOrUpdateZaBulletin(CancellationToken stoppingToken, ZaBulletin zaBulletin)
        {
            var dbZaBulletin = await _db.Bulletin_ZaBulletins.FirstOrDefaultAsync(x =>
                x.ReferenceId == zaBulletin.ReferenceId, stoppingToken);
            if (dbZaBulletin == null)
            {
                _logger.LogInformation("created new za bulletin {referenceId}", zaBulletin.ReferenceId);
                zaBulletin.Created = DateTime.UtcNow;
                _db.Bulletin_ZaBulletins.Add(zaBulletin);
                return zaBulletin;
            }

            _logger.LogInformation("found existing za bulletin {zaBulletinId} for {referenceId}", dbZaBulletin.Id, zaBulletin.ReferenceId);
            dbZaBulletin.Description = zaBulletin.Description;
            dbZaBulletin.DocumentReferenceId = zaBulletin.DocumentReferenceId;
            dbZaBulletin.FileType = zaBulletin.FileType;
            dbZaBulletin.Modified = DateTime.UtcNow;
            return dbZaBulletin;
        }

        private async Task<ZbBulletin> InsertOrUpdateZbBulletin(CancellationToken stoppingToken, ZbBulletin zbBulletin, long customerId)
        {
            var dbZbBulletin = await _db.Bulletin_ZbBulletins.FirstOrDefaultAsync(x =>
                x.ReferenceId == zbBulletin.ReferenceId
                && x.Group == zbBulletin.Group, stoppingToken);
            if (dbZbBulletin == null)
            {
                _logger.LogInformation("created new zb bulletin {referenceId}", zbBulletin.ReferenceId);
                zbBulletin.CustomerId = customerId;
                zbBulletin.Created = DateTime.UtcNow;
                _db.Bulletin_ZbBulletins.Add(zbBulletin);
                return zbBulletin;
            }

            _logger.LogInformation("found existing zb bulletin {zbBulletinId} for {referenceId}", dbZbBulletin.Id, zbBulletin.ReferenceId);
            dbZbBulletin.CustomerId = customerId;
            dbZbBulletin.EquipmentId = zbBulletin.EquipmentId;
            dbZbBulletin.ConfirmedByUserId = zbBulletin.ConfirmedByUserId;
            if (zbBulletin.ConfirmationTimestamp.HasValue
                && zbBulletin.ConfirmationTimestamp != DateTime.MinValue
                && dbZbBulletin.ConfirmationTimestamp != zbBulletin.ConfirmationTimestamp)
            {
                dbZbBulletin.ConfirmationTimestamp = zbBulletin.ConfirmationTimestamp;
            }

            dbZbBulletin.Group = zbBulletin.Group;
            dbZbBulletin.Status = zbBulletin.Status;
            dbZbBulletin.State = zbBulletin.State;
            dbZbBulletin.Modified = DateTime.UtcNow;
            return dbZbBulletin;
        }
    }
}
