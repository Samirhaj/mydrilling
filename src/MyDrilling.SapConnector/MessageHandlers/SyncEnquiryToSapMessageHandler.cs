﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.MessageContracts;
using MyDrilling.Infrastructure.Storage;
using MyDrilling.SapConnector.Sap.Executors;

namespace MyDrilling.SapConnector.MessageHandlers
{
    public class SyncEnquiryToSapMessageHandler : IMessageHandler
    {
        private readonly MyDrillingDb _db;
        private readonly BlobServiceClient _blobServiceClient;
        private readonly ILogger _logger;
        private readonly ICreateEnquiryExecutor _createEnquiryExecutor;
        private readonly IAddCommentToEnquiryExecutor _addCommentToEnquiryExecutor;
        private readonly IAddFileToEnquiryExecutor _addFileToEnquiryExecutor;
        private readonly IReadEnquiryExecutor _readEnquiryExecutor;

        public SyncEnquiryToSapMessageHandler(MyDrillingDb db,
            ICreateEnquiryExecutor createEnquiryExecutor,
            IAddCommentToEnquiryExecutor addCommentToEnquiryExecutor,
            IAddFileToEnquiryExecutor addFileToEnquiryExecutor,
            IReadEnquiryExecutor readEnquiryExecutor,
            BlobServiceClient blobServiceClient,
            ILogger<SyncEnquiryToSapMessageHandler> logger)
        {
            _db = db;
            _createEnquiryExecutor = createEnquiryExecutor;
            _addCommentToEnquiryExecutor = addCommentToEnquiryExecutor;
            _addFileToEnquiryExecutor = addFileToEnquiryExecutor;
            _readEnquiryExecutor = readEnquiryExecutor;
            _blobServiceClient = blobServiceClient;
            _logger = logger;
        }

        public async Task HandleMessageAsync(string queueMessage, CancellationToken stoppingToken)
        {
            var msg = JsonSerializer.Deserialize<SyncEnquiryToSapMessage>(queueMessage);
            var lockKey = $"{nameof(Enquiry)}-{msg.EnquiryId}";

            await using (await _db.AcquireDistributedLockAsync(lockKey))
            {
                var enquiry = await _db.Enquiry_Enquiries
                    .Include(x => x.Attachments)
                    .Include(x => x.Comments)
                    .ThenInclude(x => x.CreatedBy)
                    .Include(x => x.CreatedBy)
                    .Include(x => x.Customer)
                    .Include(x => x.Rig)
                    .Include(x => x.Equipment)
                    .SingleAsync(x => x.Id == msg.EnquiryId, cancellationToken: stoppingToken);

                await SyncEnquiry(enquiry, stoppingToken);
                await SyncComments(enquiry, stoppingToken);
                await SyncFiles(enquiry, stoppingToken);
            }
        }

        private async Task SyncEnquiry(Enquiry enquiry, CancellationToken stoppingToken)
        {
            var referenceId = enquiry.ReferenceId;
            if (!string.IsNullOrEmpty(referenceId))
            {
                _logger.LogDebug("enquiry {enquiryId} already has referenceId: {enquiryReferenceId}", enquiry.Id, referenceId);
                return;
            }

            referenceId = await _createEnquiryExecutor.CreateEnquiry(enquiry.Title,
                enquiry.Type,
                enquiry.Customer.ReferenceId,
                enquiry.Rig.ReferenceId,
                enquiry.Equipment?.ReferenceId);

            //just retry in case of concurrent updates
            while (true)
            {
                try
                {
                    enquiry.SetReferenceId(referenceId);
                    await _db.SaveChangesAsync(stoppingToken);
                    break;
                }
                catch (DbUpdateConcurrencyException)
                {
                    _db.Entry(enquiry).Reload();
                }
            }

            await _db.SaveChangesAsync(stoppingToken);
            _logger.LogInformation("enquiry {enquiryReferenceId} is created in SAP for enquiry {enquiryId}",
                referenceId, enquiry.Id);
        }

        private async Task SyncComments(Enquiry enquiry, CancellationToken stoppingToken)
        {
            if (enquiry.Comments == null || !enquiry.Comments.Any())
            {
                return;
            }

            foreach (var comment in enquiry.Comments.OrderByDescending(x => x.IsInitial).ThenBy(x => x.Created))
            {
                var commentReferenceId = comment.ReferenceId;
                if (!string.IsNullOrEmpty(commentReferenceId))
                {
                    _logger.LogDebug("comment {enquiryCommentId} already has referenceId: {enquiryCommentReferenceId}", comment.Id, commentReferenceId);
                    continue;
                }
                try
                {
                    var commentNumber = await _readEnquiryExecutor.GetLastCommentNumber(comment.Enquiry.ReferenceId) + 1;
                    commentReferenceId = await _addCommentToEnquiryExecutor.AddCommentToEnquiry(enquiry.ReferenceId,
                        comment.Text,
                        commentNumber,
                        comment.CreatedBy?.GetDisplayName());
                    comment.ReferenceId = commentReferenceId;
                    await _db.SaveChangesAsync(stoppingToken);
                    _logger.LogInformation("comment {enquiryCommentReferenceId} {enquiryCommentId} is created in SAP for enquiry {enquiryReferenceId} {enquiryId}",
                        commentReferenceId, comment.Id, enquiry.ReferenceId, enquiry.Id);
                }
                catch (Exception ex)
                {
                    _logger.LogError("comment {enquiryCommentId} failed on creation in SAP for enquiry {enquiryReferenceId} {enquiryId}, reason : {message}",
                        comment.Id, enquiry.ReferenceId, enquiry.Id, ex.Message);
                }
            }
        }

        private async Task SyncFiles(Enquiry enquiry, CancellationToken stoppingToken)
        {
            if (enquiry.Attachments == null || !enquiry.Attachments.Any())
            {
                return;
            }

            foreach (var file in enquiry.Attachments)
            {
                if (!string.IsNullOrEmpty(file.ReferenceId))
                {
                    _logger.LogDebug("file {enquiryFileId} already has referenceId: {enquiryFileReferenceId}", file.Id, file.ReferenceId);
                    continue;
                }
                var blobClient = _blobServiceClient.GetBlobContainerClient(Infrastructure.Storage.StorageConfig.EnquiriesContainerName)
                    .GetBlobClient(file.BlobPath);
                await using var stream = new MemoryStream();
                await blobClient.DownloadToAsync(stream, stoppingToken);
                try
                {
                    file.ReferenceId = await _addFileToEnquiryExecutor.AddFileToEnquiry(enquiry.ReferenceId, file.FileName, stream.ToArray());
                    await _db.SaveChangesAsync(stoppingToken);
                    _logger.LogInformation("file {enquiryFileReferenceId} {enquiryFileId} is uploaded into SAP for enquiry {enquiryReferenceId} {enquiryId}",
                        file.ReferenceId, file.Id, enquiry.ReferenceId, enquiry.Id);
                }
                catch (Exception ex)
                {
                    _logger.LogError("file {enquiryFileId} failed on uploading into SAP for enquiry {enquiryReferenceId} {enquiryId}, reason: {message}",
                        file.Id, enquiry.ReferenceId, enquiry.Id, ex.Message);
                }
                var sourceMetadata = new Dictionary<string, string>
                {
                    {StorageConstants.BlobMetadataKeys.EnquiryId, enquiry.Id.ToString()}
                };
                await blobClient.SetMetadataAsync(sourceMetadata, cancellationToken: stoppingToken);


            }
        }
    }
}