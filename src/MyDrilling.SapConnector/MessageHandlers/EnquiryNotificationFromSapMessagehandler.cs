﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Core.Entities.Equipment;
using MyDrilling.Core.Entities.Rig;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.MessageContracts;
using MyDrilling.Infrastructure.Storage.Queue;
using MyDrilling.SapConnector.Sap;
using MyDrilling.SapConnector.Sap.Executors;
using SapService4;
using SapService5;

namespace MyDrilling.SapConnector.MessageHandlers
{
    /// <summary>
    /// Based on myDrilling.SIB.ESP.N1\myDrilling.SIB.ESP.N1.Orchestrations\V2_2\Tasks\T3\TaskImplementation.odx
    /// </summary>
    public class EnquiryNotificationFromSapMessageHandler : IMessageHandler
    {
        private static readonly Regex SapAutoCommentPattern1 = new Regex(@"^(?<day>\d\d)(.(?<month>\d{1,2})(.(?<year>\d\d\d\d)?)?)[ \t](?<hour>\d\d):(?<min>\d\d)(:(?<sec>\d\d))?", RegexOptions.Compiled);
        private const string SapAutoCommentPattern2 = "--------------------";
        //todo need to find a proper default user and move to configuration
        private const string DefaultUserUpn = "mydrilling@akersolutions.com";

        private readonly MyDrillingDb _db;
        private readonly ILogger _logger;
        private readonly IReadNotificationExecutor _readNotificationExecutor;
        private readonly IReadEnquiryExecutor _readEnquiryExecutor;
        private readonly IQueueSender _queueSender;

        public EnquiryNotificationFromSapMessageHandler(MyDrillingDb db,
            ILogger<EnquiryNotificationFromSapMessageHandler> logger,
            IReadNotificationExecutor readNotificationExecutor,
            IReadEnquiryExecutor readEnquiryExecutor,
            IQueueSender queueSender)
        {
            _db = db;
            _logger = logger;
            _readNotificationExecutor = readNotificationExecutor;
            _readEnquiryExecutor = readEnquiryExecutor;
            _queueSender = queueSender;
        }

        public async Task HandleMessageAsync(string queueMessage, CancellationToken stoppingToken)
        {
            var msg = JsonSerializer.Deserialize<EnquiryNotificationFromSapMessage>(queueMessage);
            var referenceIdNoZeros = msg.ReferenceId.RemoveLeadingZeros();

            _logger.LogInformation("received new enquiry notification from SAP: {enquiryReferenceId}", referenceIdNoZeros);

            var notificationFromSap = await _readNotificationExecutor.ReadNotification(msg.ReferenceId.ToSapReferenceId());
            var enquiryDetailsFromSap = await _readEnquiryExecutor.ReadEnquiry(msg.ReferenceId);

            var lockKey = $"{nameof(Enquiry)}-{msg.ReferenceId.RemoveLeadingZeros()}";
            var enquiryFromDb = await _db.Enquiry_Enquiries
                .Include(x => x.CreatedBy)
                .Include(x => x.Comments)
                .SingleOrDefaultAsync(x => x.ReferenceId == referenceIdNoZeros, cancellationToken: stoppingToken);

            if (enquiryFromDb == null)
            {
                _logger.LogInformation("no enquiry with {enquiryReferenceId} in MyDrilling database", referenceIdNoZeros);
            }
            else
            {
                _logger.LogInformation("found enquiry {enquiryReferenceId} {enquiryId} in MyDrilling database", referenceIdNoZeros, enquiryFromDb.Id);
                lockKey = $"{nameof(Enquiry)}-{enquiryFromDb.Id}";
            }

            await using (await _db.AcquireDistributedLockAsync(lockKey))
            {
                var notificationType = notificationFromSap.AlmNotifGetDetailResponse.NotifheaderExport.NotifType;
                var notificationPartialValue = notificationFromSap.AlmNotifGetDetailResponse.NotifheaderExport.Maintroom;

                if (!notificationType.StartsWith("S") || notificationPartialValue.Equals("partial", StringComparison.InvariantCultureIgnoreCase))
                {
                    _logger.LogInformation("skipped enquiry notification from SAP {enquiryReferenceId} because: type=" + notificationType + ", notificationPartialValue=" + notificationPartialValue, referenceIdNoZeros);
                    return;
                }

                //BizTalk doesn't read attachments for enquiry from SAP

                if (enquiryFromDb == null)
                {
                    await CreateEnquiry(notificationFromSap, enquiryDetailsFromSap, stoppingToken);
                }
                else
                {
                    await UpdateEnquiry(enquiryFromDb, notificationFromSap, enquiryDetailsFromSap, stoppingToken);
                }
            }
        }

        private async Task CreateEnquiry(AlmNotifGetDetailResponse1 notificationFromSap,
            ServiceRequestERPByIDQueryResponse_In_V1Response enquiryDetailsFromSap,
            CancellationToken stoppingToken)
        {
            var referenceIdNoZeros = notificationFromSap.AlmNotifGetDetailResponse.NotifheaderExport.NotifNo
                .RemoveLeadingZeros();
            var rigAndEquipment = await FindRigAndEquipment(notificationFromSap, stoppingToken);
            if (rigAndEquipment.Rig == null)
            {
                _logger.LogInformation("skipped enquiry notification from SAP {enquiryReferenceId} because: equipment or rig " + notificationFromSap.AlmNotifGetDetailResponse.NotifheaderExport.Equipment + " not found", referenceIdNoZeros);
                return;
            }

            var enquiryType = ExtractEnquiryType(enquiryDetailsFromSap);
            var state = ExtractEnquiryState(notificationFromSap.AlmNotifGetDetailResponse.NotifheaderExport.SysStatus);

            var sapComments = Array.Empty<(string Id, string Text, DateTime Created, string CreatedBy)>();
            if (enquiryDetailsFromSap.ServiceRequestERPByIDResponse_sync_V1.ServiceRequest.Activity != null)
            {
                sapComments = enquiryDetailsFromSap.ServiceRequestERPByIDResponse_sync_V1.ServiceRequest.Activity
                    .OrderBy(x => x.ID)
                    .Select(ExtractComment)
                    .Where(x => !string.IsNullOrEmpty(x.Text))
                    .ToArray();
            }

            var currentSoldToParty = notificationFromSap.AlmNotifGetDetailResponse.NotifheaderExport.CustNo;
            var customer = await _db.Customers
                .Include(x => x.RootCustomer)
                .SingleOrDefaultAsync(x => x.ReferenceId == currentSoldToParty, cancellationToken: stoppingToken);
            var mhWirth = await _db.RootCustomers.SingleAsync(x => x.IsInternal, cancellationToken: stoppingToken);
            var author = await FindUser(notificationFromSap.AlmNotifGetDetailResponse.NotifheaderExport.CreatedBy, stoppingToken);

            var description = sapComments.Length > 0
                ? sapComments[0].Text.Length > Enquiry.DescriptionMaxLength
                  ? sapComments[0].Text.Substring(0, Enquiry.DescriptionMaxLength)
                  : sapComments[0].Text
                : string.Empty;

            var title = notificationFromSap.AlmNotifGetDetailResponse.NotifheaderExport.ShortText;
            title = title.Length > Enquiry.TitleMaxLength
                ? title.Substring(0, Enquiry.TitleMaxLength)
                : title;

            var enquiry = new Enquiry(title,
                description,
                enquiryType,
                rigAndEquipment.Rig,
                customer.RootCustomer,
                customer,
                rigAndEquipment.Rig.Owner,
                mhWirth,
                rigAndEquipment.Equipment,
                author);
            enquiry.SetSubmitted(enquiryDetailsFromSap.ServiceRequestERPByIDResponse_sync_V1.ServiceRequest.SystemAdministrativeData.CreationDateTime, author);
            enquiry.SetReferenceId(referenceIdNoZeros);
            if (sapComments.Length > 0)
            {
                var initialComment = enquiry.AddInitialComment();
                initialComment.Submitted = sapComments[0].Created;
                initialComment.ReferenceId = $"{referenceIdNoZeros}{Constants.ReferenceSeparator}{sapComments[0].Id}";
                for (int i = 1; i < sapComments.Length; i++)
                {
                    var sapComment = sapComments[i];
                    var commentAuthor = await FindUser(sapComment.CreatedBy, stoppingToken);
                    var comment = enquiry.AddComment(sapComment.Text, commentAuthor, sapComment.Created);
                    comment.ReferenceId = $"{referenceIdNoZeros}{Constants.ReferenceSeparator}{sapComment.Id}";
                    comment.Submitted = sapComment.Created;
                }
            }

            var created = enquiryDetailsFromSap.ServiceRequestERPByIDResponse_sync_V1.ServiceRequest.SystemAdministrativeData.CreationDateTime;
            switch (state)
            {
                case StateInfo.State.Deleted:
                    enquiry.Delete(author, created);
                    break;
                case StateInfo.State.Closed:
                    enquiry.Close(author, created);
                    break;
                default:
                    enquiry.Approve(author, created);
                    break;
            }

            _db.Enquiry_Enquiries.Add(enquiry);
            await _db.SaveChangesAsync(stoppingToken);

            _logger.LogInformation("added new enquiry {enquiryReferenceId} {enquiryId} in MyDrilling database with: " +
                $"type {enquiry.Type}, {enquiry.State}, {sapComments.Length} comments", referenceIdNoZeros, enquiry.Id);

            var events = new List<Task>();
            //we write enquiries without RootCustomerId into db, it's a legacy logic
            //but these enquiries are not available for users
            if (enquiry.RootCustomerId.HasValue)
            {
                //we write internal enquiries into db, it's a legacy logic
                //but these enquiries are not available for users
                if (enquiry.Type != TypeInfo.Type.Internal_Internal && enquiry.State == StateInfo.State.AssignedProvider)
                {
                    events.Add(_queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryAssignedToProvider));
                }
                if (enquiry.Type == TypeInfo.Type.Other_RigMove)
                {
                    events.Add(_queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryRigMovedCreated));
                }
                if (enquiry.Type == TypeInfo.Type.Support_RigOnDowntime)
                {
                    events.Add(_queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryRigDowntimeCreated));
                }
            }
            await Task.WhenAll(events);
        }

        private async Task UpdateEnquiry(Enquiry enquiry, 
            AlmNotifGetDetailResponse1 notificationFromSap,
            ServiceRequestERPByIDQueryResponse_In_V1Response enquiryDetailsFromSap,
            CancellationToken stoppingToken)
        {
            var referenceIdNoZeros = notificationFromSap.AlmNotifGetDetailResponse.NotifheaderExport.NotifNo.RemoveLeadingZeros();
            var enquiryType = ExtractEnquiryType(enquiryDetailsFromSap);
            var state = ExtractEnquiryState(notificationFromSap.AlmNotifGetDetailResponse.NotifheaderExport.SysStatus);

            var sapComments = Array.Empty<(string Id, string Text, DateTime Created, string CreatedBy)>();
            if (enquiryDetailsFromSap.ServiceRequestERPByIDResponse_sync_V1.ServiceRequest.Activity != null)
            {
                sapComments = enquiryDetailsFromSap.ServiceRequestERPByIDResponse_sync_V1.ServiceRequest.Activity
                    .OrderBy(x => x.ID)
                    .Select(ExtractComment)
                    .Where(x => !string.IsNullOrEmpty(x.Text))
                    .ToArray();
            }

            var newCommentsCount = 0;
            if (sapComments.Length > 0)
            {
                if (string.IsNullOrEmpty(enquiry.Description) && enquiry.CreatedBy != null)
                {
                    enquiry.SetDescription(sapComments[0].Text, enquiry.CreatedBy);
                }

                for (int i = 1; i < sapComments.Length; i++)
                {
                    var sapComment = sapComments[i];
                    var commentReferenceId = $"{referenceIdNoZeros}{Constants.ReferenceSeparator}{sapComment.Id}";
                    if (enquiry.Comments.Any(x => x.ReferenceId.Equals(commentReferenceId)))
                    {
                        continue;
                    }

                    var commentAuthor = await FindUser(sapComment.CreatedBy, stoppingToken);
                    var comment = enquiry.AddComment(sapComment.Text, commentAuthor, sapComment.Created);
                    comment.ReferenceId = commentReferenceId;
                    comment.Submitted = sapComment.Created;
                    newCommentsCount++;
                }
            }

            var author = await FindUser(sapComments.Length > 0 ? sapComments[0].CreatedBy : string.Empty,
                stoppingToken);

            var originalState = enquiry.State;
            var stateChanged = state != enquiry.State;
            if (stateChanged)
            {
                var changed = enquiryDetailsFromSap.ServiceRequestERPByIDResponse_sync_V1.ServiceRequest.SystemAdministrativeData.LastChangeDateTime;
                if (enquiry.State == StateInfo.State.Closed && state == StateInfo.State.AssignedProvider)
                {
                    enquiry.AssignToProvider(author);
                }
                else
                {
                    switch (state)
                    {
                        case StateInfo.State.Deleted:
                            enquiry.Delete(author, changed);
                            break;
                        case StateInfo.State.Closed:
                            enquiry.Close(author, changed);
                            break;
                    }
                }
            }

            var originalType = enquiry.Type;
            var typeChanged = enquiryType != enquiry.Type;
            if (typeChanged)
            {
                enquiry.SetType(enquiryType, author);
            }

            await _db.SaveChangesAsync(stoppingToken);

            if (stateChanged || typeChanged || newCommentsCount > 0)
            {
                _logger.LogInformation("updated enquiry {enquiryReferenceId} {enquiryId} in MyDrilling database: " +
                                       $"type {originalType}->{enquiry.Type}, state {originalState}->{enquiry.State}, {newCommentsCount} new comments",
                    referenceIdNoZeros, enquiry.Id);
            }

            //if (typeChanged)
            //{
            //    if (enquiry.Type == TypeInfo.Type.Other_RigMove)
            //    {
            //        await _queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryRigMovedCreated);
            //    }

            //    if (enquiry.Type == TypeInfo.Type.Support_RigOnDowntime)
            //    {
            //        await _queueSender.EnqueueEnquiryEventAsync(enquiry.Id, EnquiryEventType.EnquiryRigDowntimeCreated);
            //    }
            //}
        }

        private async Task<User> FindUser(string userReferenceId, CancellationToken stoppingToken)
        {
            if (string.IsNullOrEmpty(userReferenceId) || !int.TryParse(userReferenceId, out _))
            {
                return await _db.Users.FirstOrDefaultAsync(x => x.Upn == DefaultUserUpn,
                    cancellationToken: stoppingToken);
            }

            var user = await _db.Users.FirstOrDefaultAsync(x => x.ReferenceId == userReferenceId, cancellationToken: stoppingToken);
            return user ?? await _db.Users.FirstOrDefaultAsync(x => x.Upn == DefaultUserUpn,
                       cancellationToken: stoppingToken);
        }

        private (string Id, string Text, DateTime Created, string CreatedBy) ExtractComment(SrvcReqERPByIDRsp_s_V1Acty activity)
        {
            if (activity.ActivityParentServiceIssueCategoryID != "MYDR"
                || (activity.ActivityServiceIssueCategoryID != "MYDR" && activity.ActivityServiceIssueCategoryID != "DESC")
                || activity.TextCollection == null)
            {
                return (string.Empty, string.Empty, DateTime.MinValue, string.Empty);
            }
                
            var textParts = activity.TextCollection.Text.Where(x => IsCommentPart(x.ContentText.Value))
                .Select(x => HttpUtility.HtmlDecode(x.ContentText.Value)).ToArray();
            var text = string.Join(' ', textParts).Replace("  ", " ");

            return (activity.ID, text, activity.SystemAdministrativeData.CreationDateTime, activity.SystemAdministrativeData.CreationUserAccountID);
        }

        private static bool IsCommentPart(string text)
        {
            return !string.IsNullOrEmpty(text) && !text.Contains(SapAutoCommentPattern2) && !SapAutoCommentPattern1.IsMatch(text);
        }

        private static StateInfo.State ExtractEnquiryState(string referenceStatus)
        {
            var state = StateInfo.State.AssignedProvider;

            if (referenceStatus.Contains("noco", StringComparison.InvariantCultureIgnoreCase))
            {
                state = StateInfo.State.Closed;
            }
            else if (referenceStatus.Contains("dlfl", StringComparison.InvariantCultureIgnoreCase))
            {
                state = StateInfo.State.Deleted;
            }

            return state;
        }

        private async Task<(Equipment Equipment, Rig Rig)> FindRigAndEquipment(AlmNotifGetDetailResponse1 notificationFromSap, CancellationToken stoppingToken)
        {
            var equipmentId = notificationFromSap.AlmNotifGetDetailResponse.NotifheaderExport.Equipment.ToLongReferenceId();

            var equipment = await _db.Equipments
                .Include(x => x.Rig)
                .ThenInclude(x => x.Owner)
                .SingleOrDefaultAsync(x => x.ReferenceId == equipmentId, cancellationToken: stoppingToken);

            if (equipment != null)
            {
                return (equipment, equipment.Rig);
            }

            var rig = await _db.Rigs
                .Include(x => x.Owner)
                .SingleOrDefaultAsync(x => x.ReferenceId == equipmentId, cancellationToken: stoppingToken);

            return (default, rig);
        }

        private static TypeInfo.Type ExtractEnquiryType(ServiceRequestERPByIDQueryResponse_In_V1Response enquiryDetailsFromSap)
        {
            var enquiryType = TypeInfo.Type.Support_General;
            var codingGroup = enquiryDetailsFromSap.ServiceRequestERPByIDResponse_sync_V1.ServiceRequest.IssueParentServiceIssueCategoryID;
            var coding = enquiryDetailsFromSap.ServiceRequestERPByIDResponse_sync_V1.ServiceRequest.IssueServiceIssueCategoryID;

            if (!string.IsNullOrEmpty(coding) && !string.IsNullOrEmpty(coding)
                && Constants.SapCodingsToEnquiryTypes.TryGetValue((codingGroup, coding), out var enquiryTypeMapping))
            {
                enquiryType = enquiryTypeMapping.EnquiryType;
            }

            return enquiryType;
        }
     }
}
