﻿using System.Threading;
using System.Threading.Tasks;

namespace MyDrilling.SapConnector.MessageHandlers
{
    public interface IMessageHandler
    {
        Task HandleMessageAsync(string queueMessage, CancellationToken stoppingToken);
    }
}
