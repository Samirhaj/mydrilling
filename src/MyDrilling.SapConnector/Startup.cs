﻿using System;
using System.Reflection;
using Microsoft.ApplicationInsights.Channel;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.ApplicationInsights.WindowsServer.TelemetryChannel;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.Storage.Blob;
using MyDrilling.Infrastructure.Storage.Queue;
using MyDrilling.SapConnector.MessageHandlers;
using MyDrilling.SapConnector.Sap;
using MyDrilling.SapConnector.Sap.Executors;

namespace MyDrilling.SapConnector
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<ISoapSapClientsFactory, SoapSapClientsFactory>();
            services.AddSingleton<IAddCommentToEnquiryExecutor, AddCommentToEnquiryExecutor>();
            services.AddSingleton<IAddFileToEnquiryExecutor, AddFileToEnquiryExecutor>();
            services.AddSingleton<ICreateEnquiryExecutor, CreateEnquiryExecutor>();
            services.AddSingleton<IReadEnquiryExecutor, ReadEnquiryExecutor>();
            services.AddSingleton<IReadNotificationExecutor, ReadNotificationExecutor>();
            services.AddSingleton<IRequestSparepartPriceExecutor, RequestSparepartPriceExecutor>();
            services.AddSingleton<ISoapSapClientsFactory, SoapSapClientsFactory>();
            services.AddScoped<IReadZaBulletinExecutor, ReadZaBulletinExecutor>();
            services.AddScoped<IReadZbBulletinExecutor, ReadZbBulletinExecutor>();
            services.AddSingleton<IReadFileExecutor, ReadFileExecutor>();
            services.AddSingleton<IReadMaterialExecutor, ReadMaterialExecutor>();
            services.Configure<SapServicesConfig>(Configuration.GetSection("Sap"));

            services.AddHostedService<HealthReporter>();

            services.AddMessageHandlers(Configuration);
            services.AddMyDrillingQueues(Configuration);
            services.AddMyDrillingDb(Configuration);
            services.AddMyDrillingBlob(Configuration);

            services.AddControllers();

            /*
            it's required to avoid this:
            AI: Local storage access has resulted in an error (User: ) (CustomFolder: ). 
            If you want Application Insights SDK to store telemetry locally on disk in case of transient network issues please give the process access to %LOCALAPPDATA% or %TEMP% folder. 
            If application is running in non-windows platform, create StorageFolder yourself, and set ServerTelemetryChannel.StorageFolder to the custom folder name. 
            After you gave access to the folder you need to restart the process. Currently monitoring will continue but if telemetry cannot be sent it will be dropped
             
            It's because in sesam sapconnector works inside linux docker container
            */
            var appInsightsFolder = Environment.GetEnvironmentVariable("ApplicationInsightsFolder");
            if (!string.IsNullOrEmpty(appInsightsFolder))
            {
                services.AddSingleton(typeof(ITelemetryChannel), new ServerTelemetryChannel { StorageFolder = appInsightsFolder });
            }
            services.AddSingleton<ITelemetryInitializer, CloudRoleNameTelemetryInitializer>();
            services.AddApplicationInsightsTelemetry();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            Console.WriteLine($"Version: {Assembly.GetExecutingAssembly().GetName().Version}");
        }
    }
}
