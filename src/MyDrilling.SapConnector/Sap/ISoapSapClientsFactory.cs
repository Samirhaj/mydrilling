﻿using System;
using SapService1;
using SapService10;
using SapService11;
using SapService12;
using SapService13;
using SapService14;
using SapService15;
using SapService2;
using SapService3;
using SapService4;
using SapService5;
using SapService6;
using SapService7;
using SapService8;
using SapService9;

namespace MyDrilling.SapConnector.Sap
{
    public interface ISoapSapClientsFactory
    {
        AsyncDisposeWrapper<ZZMYDRILLING_CREATE_NOTIF_V02> GetCreateEnquiryClient();
        AsyncDisposeWrapper<ZZBAPI_ALM_NOTIF_DATA_ADD_1> GetAddCommentToEnquiryClient();
        AsyncDisposeWrapper<DocumentERPCreateRequestConfirmation_In_V1> GetAddFileToEnquiryClient();
        AsyncDisposeWrapper<ServiceRequestERPByIDQueryResponse_In_V1> GetReadEnquiryClient();
        AsyncDisposeWrapper<ZZBAPI_ALM_NOTIF_GET_DETAIL> GetReadNotificationClient();
        AsyncDisposeWrapper<ServiceRequestERPAttachmentFolderByIDQueryResponse_In> GetReadDocumentsForEnquiryClient();
        AsyncDisposeWrapper<ZZ_MH_ADD_DIR_TO_FOLDER_V1> GetAddFileToFolderClient();
        AsyncDisposeWrapper<DocumentERPByIDQueryResponse_In_V1> GetReadDocumentDetailsClient();
        AsyncDisposeWrapper<DocumentERPFileVariantByIDQueryResponse_In> GetReadFileClient(TimeSpan? timeout = null);
        AsyncDisposeWrapper<DocumentERPFileVariantByIDAndFileVariantIDQueryResponse_In> GetReadFileVariantClient(TimeSpan? timeout = null);
        AsyncDisposeWrapper<ZZ_MH_SALESORDER_SIMULATE_V10> GetRequestSparepartPriceClient();
        AsyncDisposeWrapper<ServiceRequestERPAttachmentFolderByIDQueryResponse_In> GetDocumentsForFolderClient();
        AsyncDisposeWrapper<ZZ_MH_NOTIFICATION_DATA_GET_V1> GetReadNotificationDataClient();
        AsyncDisposeWrapper<ZZ_MH_NOTIFICATION_DOC_FLOW_V11> GetReadNotificationDocumentFlowClient();
        AsyncDisposeWrapper<ZZ_MH_NOTIF_CONTACTPERSON_V11> GetReadBulletinReceivedConfirmerContactDetailsClient();
        AsyncDisposeWrapper<IndividualMaterialByIDQueryResponse_In_V1> GetReadMaterialClient();
        AsyncDisposeWrapper<ServiceRequestERPByIDQueryResponse_In_V1> GetSapNotificationDetailsClient();

    }
}
