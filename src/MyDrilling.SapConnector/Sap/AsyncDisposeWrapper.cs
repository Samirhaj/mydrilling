﻿using System;
using System.Threading.Tasks;

namespace MyDrilling.SapConnector.Sap
{
    public sealed class AsyncDisposeWrapper<T> : IAsyncDisposable
    {
        private readonly Func<T, Task> _dispose;

        public AsyncDisposeWrapper(T target, Func<T, Task> dispose)
        {
            Target = target;
            _dispose = dispose;
        }

        public T Target { get; }

        public ValueTask DisposeAsync()
        {
            return new ValueTask(_dispose.Invoke(Target));
        }
    }
}
