﻿using System;

namespace MyDrilling.SapConnector.Sap
{
    public class SapServicesConfig
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public TimeSpan Timeout { get; set; }
        public bool SkipHttpsCertificateValidation { get; set; }
        public string SapHost { get; set; }
        public string DocumentRepositoryCode { get; set; }

        // ReSharper disable InconsistentNaming
        
        public string Url_ZZMYDRILLING_CREATE_NOTIF_V02 => $"{SapHost}/sap/bc/srt/pm/sap/zzmydrilling_create_notif_v02/101/local/zakerinternal/1/binding_t_http_a_http_zzmydrilling_create_notif_v02_zakerinternal_l";
        public string Url_ZZBAPI_ALM_NOTIF_DATA_ADD_1 => $"{SapHost}/sap/bc/srt/pm/sap/zzbapi_alm_notif_data_add_1/101/local/zakerinternal/1/binding_t_http_a_http_zzbapi_alm_notif_data_add_1_zakerinternal_l";
        public string Url_DocumentERPCreateRequestConfirmation_In_V1 => $"{SapHost}/sap/bc/srt/pm/sap/ecc_documentcrtrc1/101/local/zakerinternal/1/binding_t_http_a_http_ecc_documentcrtrc1_zakerinternal_l";
        public string Url_ServiceRequestERPByIDQueryResponse_In_V1 => $"{SapHost}/sap/bc/srt/pm/sap/ecc_srvcreqidqr1/101/local/zakerinternal/1/binding_t_http_a_http_ecc_srvcreqidqr1_zakerinternal_l";
        public string Url_ZZBAPI_ALM_NOTIF_GET_DETAIL => $"{SapHost}/sap/bc/srt/pm/sap/zzbapi_alm_notif_get_detail/101/local/zakerinternal/1/binding_t_http_a_http_zzbapi_alm_notif_get_detail_zakerinternal_l";
        public string Url_ServiceRequestERPAttachmentFolderByIDQueryResponse_In => $"{SapHost}/sap/bc/srt/pm/sap/ecc_srvcreqattchfldridqr/101/local/zakerinternal/1/binding_t_http_a_http_ecc_srvcreqattchfldridqr_zakerinternal_l";
        public string Url_ZZ_MH_ADD_DIR_TO_FOLDER_V1 => $"{SapHost}/sap/bc/srt/pm/sap/zz_mh_add_dir_to_folder_v1/101/local/zakerinternal/1/binding_t_http_a_http_zz_mh_add_dir_to_folder_v1_zakerinternal_l";
        public string Url_DocumentERPByIDQueryResponse_In_V1 => $"{SapHost}/sap/bc/srt/pm/sap/ecc_documentidqr1/101/local/zakerinternal/1/binding_t_http_a_http_ecc_documentidqr1_zakerinternal_l";
        public string Url_DocumentERPFileVariantByIDQueryResponse_In => $"{SapHost}/sap/bc/srt/pm/sap/ecc_documentfvidqr/101/local/zakerinternal/1/binding_t_http_a_http_ecc_documentfvidqr_zakerinternal_l";
        public string Url_DocumentERPFileVariantByIDAndFileVariantIDQueryResponse_In => $"{SapHost}/sap/bc/srt/pm/sap/ecc_documentfvbyfvidqr/101/local/zakerinternal/1/binding_t_http_a_http_ecc_documentfvbyfvidqr_zakerinternal_l";
        public string Url_ZZ_MH_SALESORDER_SIMULATE_V10 => $"{SapHost}/sap/bc/srt/pm/sap/zz_mh_salesorder_simulate_v10/101/local/zakerinternal/1/binding_t_http_a_http_zz_mh_salesorder_simulate_v10_zakerinternal_l";
        public string Url_ZZ_MH_NOTIFICATION_DATA_GET_V1 => $"{SapHost}/sap/bc/srt/pm/sap/zz_mh_notification_data_get_v1/101/local/zakerinternal/1/binding_t_http_a_http_zz_mh_notification_data_get_v1_zakerinternal_l";
        public string Url_ZZ_MH_NOTIFICATION_DOC_FLOW_V11 => $"{SapHost}/sap/bc/srt/pm/sap/zz_mh_notification_doc_flow_v1/101/local/zakerinternal/1/binding_t_http_a_http_zz_mh_notification_doc_flow_v1_zakerinternal_l";
        public string Url_ZZ_MH_NOTIF_CONTACTPERSON_V11 => $"{SapHost}/sap/bc/srt/pm/sap/zz_mh_notif_contactperson_v1/101/local/zakerinternal/1/binding_t_http_a_http_zz_mh_notif_contactperson_v1_zakerinternal_l";
        public string Url_IndividualMaterialByIDQueryResponse_In_V1 => $"{SapHost}/sap/bc/srt/pm/sap/ecc_indivimatlidqr1/101/local/zakerinternal/1/binding_t_http_a_http_ecc_indivimatlidqr1_zakerinternal_l";

        // ReSharper restore InconsistentNaming
    }
}
