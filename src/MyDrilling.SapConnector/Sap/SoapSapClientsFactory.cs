﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SapService1;
using SapService10;
using SapService11;
using SapService12;
using SapService13;
using SapService14;
using SapService15;
using SapService2;
using SapService3;
using SapService4;
using SapService5;
using SapService6;
using SapService7;
using SapService8;
using SapService9;

namespace MyDrilling.SapConnector.Sap
{
    public sealed class SoapSapClientsFactory : ISoapSapClientsFactory
    {
        private readonly SapServicesConfig _config;

        public SoapSapClientsFactory(IOptions<SapServicesConfig> sapConfig)
        {
            _config = sapConfig.Value;
        }

        public AsyncDisposeWrapper<ZZMYDRILLING_CREATE_NOTIF_V02> GetCreateEnquiryClient()
        {
            var url = _config.Url_ZZMYDRILLING_CREATE_NOTIF_V02;
            var client = new ZZMYDRILLING_CREATE_NOTIF_V02Client(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<ZZMYDRILLING_CREATE_NOTIF_V02>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<ZZBAPI_ALM_NOTIF_DATA_ADD_1> GetAddCommentToEnquiryClient()
        {
            var url = _config.Url_ZZBAPI_ALM_NOTIF_DATA_ADD_1;
            var client = new ZZBAPI_ALM_NOTIF_DATA_ADD_1Client(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<ZZBAPI_ALM_NOTIF_DATA_ADD_1>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<DocumentERPCreateRequestConfirmation_In_V1> GetAddFileToEnquiryClient()
        {
            var url = _config.Url_DocumentERPCreateRequestConfirmation_In_V1;
            var client = new DocumentERPCreateRequestConfirmation_In_V1Client(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<DocumentERPCreateRequestConfirmation_In_V1>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<ServiceRequestERPByIDQueryResponse_In_V1> GetReadEnquiryClient()
        {
            return GetSapNotificationDetailsClient();
        }
        public AsyncDisposeWrapper<ServiceRequestERPByIDQueryResponse_In_V1> GetSapNotificationDetailsClient()
        {
            var url = _config.Url_ServiceRequestERPByIDQueryResponse_In_V1;
            var client = new ServiceRequestERPByIDQueryResponse_In_V1Client(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<ServiceRequestERPByIDQueryResponse_In_V1>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<ZZBAPI_ALM_NOTIF_GET_DETAIL> GetReadNotificationClient()
        {
            var url = _config.Url_ZZBAPI_ALM_NOTIF_GET_DETAIL;
            var client = new ZZBAPI_ALM_NOTIF_GET_DETAILClient(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<ZZBAPI_ALM_NOTIF_GET_DETAIL>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<ServiceRequestERPAttachmentFolderByIDQueryResponse_In> GetReadDocumentsForEnquiryClient()
        {
            return GetDocumentsForFolderClient();
        }
        public AsyncDisposeWrapper<ServiceRequestERPAttachmentFolderByIDQueryResponse_In> GetDocumentsForFolderClient()
        {
            var url = _config.Url_ServiceRequestERPAttachmentFolderByIDQueryResponse_In;
            var client = new ServiceRequestERPAttachmentFolderByIDQueryResponse_InClient(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<ServiceRequestERPAttachmentFolderByIDQueryResponse_In>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<ZZ_MH_ADD_DIR_TO_FOLDER_V1> GetAddFileToFolderClient()
        {
            var url = _config.Url_ZZ_MH_ADD_DIR_TO_FOLDER_V1;
            var client = new ZZ_MH_ADD_DIR_TO_FOLDER_V1Client(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<ZZ_MH_ADD_DIR_TO_FOLDER_V1>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<DocumentERPByIDQueryResponse_In_V1> GetReadDocumentDetailsClient()
        {
            var url = _config.Url_DocumentERPByIDQueryResponse_In_V1;
            var client = new DocumentERPByIDQueryResponse_In_V1Client(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<DocumentERPByIDQueryResponse_In_V1>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<DocumentERPFileVariantByIDQueryResponse_In> GetReadFileClient(TimeSpan? timeout = null)
        {
            var url = _config.Url_DocumentERPFileVariantByIDQueryResponse_In;
            var client = new DocumentERPFileVariantByIDQueryResponse_InClient(CreateBinding(url, timeout), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<DocumentERPFileVariantByIDQueryResponse_In>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<DocumentERPFileVariantByIDAndFileVariantIDQueryResponse_In> GetReadFileVariantClient(TimeSpan? timeout = null)
        {
            var url = _config.Url_DocumentERPFileVariantByIDAndFileVariantIDQueryResponse_In;
            var client = new DocumentERPFileVariantByIDAndFileVariantIDQueryResponse_InClient(CreateBinding(url, timeout), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<DocumentERPFileVariantByIDAndFileVariantIDQueryResponse_In>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<ZZ_MH_SALESORDER_SIMULATE_V10> GetRequestSparepartPriceClient()
        {
            var url = _config.Url_ZZ_MH_SALESORDER_SIMULATE_V10;
            var client = new ZZ_MH_SALESORDER_SIMULATE_V10Client(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<ZZ_MH_SALESORDER_SIMULATE_V10>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<ZZ_MH_NOTIFICATION_DATA_GET_V1> GetReadNotificationDataClient()
        {
            var url = _config.Url_ZZ_MH_NOTIFICATION_DATA_GET_V1;
            var client = new ZZ_MH_NOTIFICATION_DATA_GET_V1Client(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<ZZ_MH_NOTIFICATION_DATA_GET_V1>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<ZZ_MH_NOTIFICATION_DOC_FLOW_V11> GetReadNotificationDocumentFlowClient()
        {
            var url = _config.Url_ZZ_MH_NOTIFICATION_DOC_FLOW_V11;
            var client = new ZZ_MH_NOTIFICATION_DOC_FLOW_V1Client(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<ZZ_MH_NOTIFICATION_DOC_FLOW_V11>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<ZZ_MH_NOTIF_CONTACTPERSON_V11> GetReadBulletinReceivedConfirmerContactDetailsClient()
        {
            var url = _config.Url_ZZ_MH_NOTIF_CONTACTPERSON_V11;
            var client = new ZZ_MH_NOTIF_CONTACTPERSON_V1Client(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<ZZ_MH_NOTIF_CONTACTPERSON_V11>(client, CloseCommunicationObjectAsync);
        }

        public AsyncDisposeWrapper<IndividualMaterialByIDQueryResponse_In_V1> GetReadMaterialClient()
        {
            var url = _config.Url_IndividualMaterialByIDQueryResponse_In_V1;
            var client = new IndividualMaterialByIDQueryResponse_In_V1Client(CreateBinding(url), CreateEndpointAddress(url));
            ConfigureAuthentication(client);
            return new AsyncDisposeWrapper<IndividualMaterialByIDQueryResponse_In_V1>(client, CloseCommunicationObjectAsync);
        }
        
        private static async Task CloseCommunicationObjectAsync<T>(T client)
        {
            if (!(client is ICommunicationObject comObj))
            {
                return;
            }

            try
            {
                // Only want to call Close if it is in the Opened state
                if (comObj.State == CommunicationState.Opened)
                {
                    //copied from generated proxy class, CloseAsync() method
                    await Task.Factory.FromAsync(comObj.BeginClose(null, null), comObj.EndClose);
                }
                // Anything not closed by this point should be aborted
                if (comObj.State != CommunicationState.Closed)
                {
                    comObj.Abort();
                }
            }
            catch (TimeoutException)
            {
                comObj.Abort();
            }
            catch (CommunicationException)
            {
                comObj.Abort();
            }
            catch (Exception)
            {
                comObj.Abort();
                throw;
            }
        }

        private void ConfigureAuthentication<TClient>(ClientBase<TClient> client)
            where TClient: class
        {
            client.ClientCredentials.UserName.UserName = _config.Username;
            client.ClientCredentials.UserName.Password = _config.Password;
            
            //now SAP web services use self-signed TLS certificate
            //so we must skip certificate validation
            if (_config.SkipHttpsCertificateValidation)
            {
                client.ClientCredentials.ServiceCertificate.SslCertificateAuthentication =
                    new X509ServiceCertificateAuthentication
                    {
                        CertificateValidationMode = X509CertificateValidationMode.None,
                        RevocationMode = X509RevocationMode.NoCheck
                    };
            }
        }

        private Binding CreateBinding(string endpointUrl, TimeSpan? timeout = null)
        {
            return endpointUrl.StartsWith("https", StringComparison.InvariantCultureIgnoreCase)
                ? CreateHttpsBindingForEndpoint(timeout)
                : CreateHttpBindingForEndpoint(timeout);
        }

        private Binding CreateHttpsBindingForEndpoint(TimeSpan? timeout)
        {
            var binding = new BasicHttpsBinding();
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            binding.Security.Mode = BasicHttpsSecurityMode.Transport;

            binding.MaxBufferSize = int.MaxValue;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
            binding.AllowCookies = true;

            var timeoutSpan = timeout ?? _config.Timeout;
            binding.ReceiveTimeout = timeoutSpan;
            binding.SendTimeout = timeoutSpan;
            binding.OpenTimeout = timeoutSpan;
            binding.CloseTimeout = timeoutSpan;

            return binding;
        }

        private Binding CreateHttpBindingForEndpoint(TimeSpan? timeout)
        {
            var binding = new BasicHttpBinding();
            binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;
            binding.Security.Mode = BasicHttpSecurityMode.TransportCredentialOnly;

            binding.MaxBufferSize = int.MaxValue;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
            binding.AllowCookies = true;

            var timeoutSpan = timeout ?? _config.Timeout;
            binding.ReceiveTimeout = timeoutSpan;
            binding.SendTimeout = timeoutSpan;
            binding.OpenTimeout = timeoutSpan;
            binding.CloseTimeout = timeoutSpan;

            return binding;
        }

        private static EndpointAddress CreateEndpointAddress(string endpointUrl)
        {
            return new EndpointAddress(endpointUrl);
        }
    }
}
