﻿using System.Collections.Generic;
using System.Linq;
using MyDrilling.Core.Entities.Enquiry;

namespace MyDrilling.SapConnector.Sap
{
    public static class Constants
    {
        public const string SapAdministrationFolderName = "Administration";
        public const string SapDocumentTypeCode = "ADM";
        public const string ReferenceSeparator = "|";

        //values below are hardcoded in requestTemplate in GetPriceAndAvailabilityV2_1.odx
        public const int SparepartsQuantity = 20;
        public const string SparepartsSalesOrg = "1700";
        public const string MhWirthKristiansand = "1700";
        public const string MhWirthSingapore = "1720";
        public const string MhWirthHouston = "2350";

        //from stg1.SIB_DataMapping table
        private static readonly (TypeInfo.Type EnquiryType, string CodingGroup, string Coding)[] EnquiryTypesMappings = 
        {
            (TypeInfo.Type.Support_Service, "SGEN", "SGSU"),
            (TypeInfo.Type.Support_RigOptimization, "SGEN", "SGRO"),
            (TypeInfo.Type.Support_RigOnDowntime, "HSE", "HROD"),
            (TypeInfo.Type.Support_Overhaul, "OVER", "OVER"),
            (TypeInfo.Type.Support_Training, "TRAIN", "TRN"),
            (TypeInfo.Type.Support_RemoteDiagnostics, "REMO", "REMO"),
            (TypeInfo.Type.Support_BulletinSupport, "BULL", "BULL"),
            (TypeInfo.Type.Support_General, "SGEN", "SGEN"),
            (TypeInfo.Type.Sparepart_IdentifyPartNumber, "SPGEN", "SPBA"),
            (TypeInfo.Type.Sparepart_PriceAndAvailability, "SPGEN", "SPPA"),
            (TypeInfo.Type.Sparepart_Order, "SPGEN", "SPOS"),
            (TypeInfo.Type.Sparepart_General, "SPGEN", "SPGE"),
            (TypeInfo.Type.Other_RigMove, "RMOV", "RMOV"),
            (TypeInfo.Type.Other_EquipmentMove, "EMOV", "EMOV"),
            (TypeInfo.Type.Internal_Internal, "INTER", "INT"),
            (TypeInfo.Type.Support_ProductImprovement, "FEEDB", "IMPR")
        };

        public static readonly IReadOnlyDictionary<TypeInfo.Type, (TypeInfo.Type EnquiryType, string CodingGroup, string Coding)> EnquiryTypesToSapCodings =
            EnquiryTypesMappings.ToDictionary(x => x.EnquiryType, x => x);

        public static readonly IReadOnlyDictionary<(string CodingGroup, string Coding), (TypeInfo.Type EnquiryType, string CodingGroup, string Coding)> SapCodingsToEnquiryTypes =
            EnquiryTypesMappings.ToDictionary(x => (x.CodingGroup, x.Coding), x => x);

        //from stg1.SIB_DataMapping table
        private static readonly (string FileExtension, string DocumentFormatCode)[] ContentTypeMappings =
        {
            ("mdb", "ACC"),
            ("adp", "ACC"),
            ("mde", "ACC"),
            ("ade", "ACC"),
            ("dxf", "ACD"),
            ("edrw", "ACD"),
            ("bak", "BCK"),
            ("bmp", "BMP"),
            ("docx", "DCX"),
            ("dgn", "DGN"),
            ("dwg", "DGN"),
            ("doc", "DOC"),
            ("dot", "DOC"),
            ("html", "DOC"),
            ("htm", "DOC"),
            ("gif", "GIF"),
            ("gz",  "GZ"),
            ("gzip", "GZ"),
            ("jpg", "JPG"),
            ("jpeg", "JPG"),
            ("log", "LOG"),
            ("mcd", "MCD"),
            ("xmcd", "MCD"),
            ("mov", "MOV"),
            ("mpg", "MOV"),
            ("wmv", "MOV"),
            ("mpeg", "MOV"),
            ("avi", "MOV"),
            ("mpp", "MPP"),
            ("mp", "MPP"),
            ("msg", "MSG"),
            ("pcx", "PCX"),
            ("pdf", "PDF"),
            ("png", "PNG"),
            ("ppt", "PPT"),
            ("pps", "PPT"),
            ("pptx", "PTX"),
            ("rtf", "RTF"),
            ("wav", "SND"),
            ("mid", "SND"),
            ("aif", "SND"),
            ("snd", "SND"),
            ("sldasm", "SWA"),
            ("slddrw", "SWD"),
            ("sldprt", "SWP"),
            ("prt", "SWP"),
            ("drwdot", "SWT"),
            ("prtdot", "SWT"),
            ("asmdot", "SWT"),
            ("tif", "TIF"),
            ("tiff", "TIF"),
            ("txt", "TXT"),
            ("url", "URL"),
            ("vsd", "VIS"),
            ("vst", "VIS"),
            ("xls", "XLS"),
            ("xlw", "XLS"),
            ("xlt", "XLS"),
            ("csv", "XLS"),
            ("xml", "XML"),
            ("zip", "ZIP"),
            ("mp4", "GEN"),
            ("xlsx", "GEN")
        };

        public static readonly IReadOnlyDictionary<string, string> FileExtensionToDocumentFormatCode =
            ContentTypeMappings.ToDictionary(x => x.FileExtension, x => x.DocumentFormatCode);
    }
}
