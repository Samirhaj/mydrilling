﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SapService3;
using SapService6;
using SapService7;
using SapService8;

namespace MyDrilling.SapConnector.Sap.Executors
{
    public interface IAddFileToEnquiryExecutor
    {
        Task<string> AddFileToEnquiry(string enquiryReferenceId, string fileName, byte[] fileContent);
    }

    public class AddFileToEnquiryExecutor : IAddFileToEnquiryExecutor
    {
        private readonly ISoapSapClientsFactory _sapSoap;
        private readonly SapServicesConfig _sapConfig;

        public AddFileToEnquiryExecutor(ISoapSapClientsFactory sapSoap,
            IOptions<SapServicesConfig> sapConfig)
        {
            _sapSoap = sapSoap;
            _sapConfig = sapConfig.Value;
        }

        public async Task<string> AddFileToEnquiry(string enquiryReferenceId, string fileName, byte[] fileContent)
        {
            //get all files and folders for enquiry
            await using var readDocumentsClient = _sapSoap.GetReadDocumentsForEnquiryClient();
            var documents = await readDocumentsClient.Target.ServiceRequestERPAttachmentFolderByIDQueryResponse_InAsync(new ServiceRequestERPAttachmentFolderByIDQueryResponse_InRequest(new ServiceRequestERPAttachmentFolderByIDQueryMessage_sync
            {
                ServiceRequestAttachmentFolderSelectionByID = new ServiceRequestERPAttachmentFolderByIDQueryMessage_syncServiceRequestAttachmentFolderSelectionByID { ID = new ServiceRequestID { Value = enquiryReferenceId } }
            }));
            
            //find Administration
            DocumentERPByIDQueryResponse_In_V1Response administrationDocumentDetails = null;
            await using var readDocumentDetailsClient = _sapSoap.GetReadDocumentDetailsClient();
            foreach (var document in documents.ServiceRequestERPAttachmentFolderByIDResponse_sync.ServiceRequest.AttachmentFolder)
            {
                var documentDetails = await readDocumentDetailsClient.Target.DocumentERPByIDQueryResponse_In_V1Async(new DocumentERPByIDQueryResponse_In_V1Request(new DocumentByIDQueryMessage_sync
                {
                    DocumentSelectionByID = new DocumentByIDQueryMessage_syncDocumentSelectionByID
                    {
                        DocumentName = document.Name,
                        DocumentTypeCode = new SapService8.DocumentTypeCode { Value = document.TypeCode.Value },
                        AlternativeDocumentID = document.AlternativeDocumentID,
                        DocumentVersionID = document.VersionID
                    }
                }));

                if (documentDetails.DocumentERPByIDResponse_sync_V1.Document.Description.Value == Constants.SapAdministrationFolderName)
                {
                    administrationDocumentDetails = documentDetails;
                    break;
                }
            }

            if (administrationDocumentDetails == null)
            {
                throw new DirectoryNotFoundException($"{Constants.SapAdministrationFolderName} folder not found for enquiry {enquiryReferenceId}.");
            }
            
            //add file
            var documentFormatCode = Constants.FileExtensionToDocumentFormatCode.TryGetValue(Path.GetExtension(fileName).TrimStart('.').ToLower(), out var code)
                ? new SapService3.DocumentFormatCode { Value = code }
                : null;
            await using var createEnquiryClient = _sapSoap.GetAddFileToEnquiryClient();
            var attachedFile = await createEnquiryClient.Target.DocumentERPCreateRequestConfirmation_In_V1Async(new DocumentERPCreateRequestConfirmation_In_V1Request(new DocumentERPCreateRequestMessage_sync_V1
            {
                Document = new DocumentERPCreateRequestMessage_sync_V1Document
                {
                    TypeCode = new SapService3.DocumentTypeCode { Value = Constants.SapDocumentTypeCode },
                    CategoryCode = "2",
                    Description = new SapService3.SHORT_Description { Value = fileName },
                    ComputerAidedDesignDrawing = false,
                    FileVariant = new[]
                    {
                        new DocERPBulkCrteReq_s_V1FileVar
                        {
                            LargeFileHandlingIndicator = false,
                            DocumentFormatCode = documentFormatCode,
                            DocumentPathName = fileName,
                            Description = new SapService3.SHORT_Description{Value = fileName},
                            DocumentRepositoryCode = new SapService3.DocumentRepositoryCode{Value = _sapConfig.DocumentRepositoryCode},
                            BinaryObject = new BinaryObject
                            {
                                mimeCode = "Request",
                                fileName = fileName,
                                Value = fileContent
                            }
                        }
                    },
                    PropertyValuation = new[]
                    {
                        new DocumentERPCreateRequestMessage_sync_V1DocumentPropertyValuation
                        {
                            CollectionTypeCode = "017",
                            CollectionID = "MH_CORRESPONDENCE",
                            Valuation = new []
                            {
                                new SapService3.PropertyValuation
                                {
                                    PropertyReference = new SapService3.PropertyReference{ID = new SapService3.PropertyID{Value = "DOC_CLASSIFICATION"}},
                                    ValueGroup = new []
                                    {
                                        new SapService3.PropertyValuationValueGroup
                                        {
                                            OrdinalNumberValue = 1,
                                            PropertyValue = new SapService3.PropertyValue{NameSpecification = new []
                                            {
                                                new SapService3.PropertyValueNameSpecification
                                                {
                                                    Name = new SapService3.Name{Value = "Public"}
                                                }
                                            }}
                                        }
                                    }
                                }
                            }
                        }
                    },
                    ObjectReference = new[]
                    {
                        new DocERPBulkCrteReq_s_V1ObjRef
                        {
                            ReferenceObjectID = new SapService3.ObjectID{Value = enquiryReferenceId.ToSapReferenceId()},
                            ReferenceObjectTypeCode = new SapService3.DocumentObjectReferenceTypeCode{Value = "SMQMEL"}
                        }
                    },
                    Status = new DocumentERPCreateRequestMessage_sync_V1DocumentStatus
                    {
                        Code = new SapService3.DocumentStatusCode { Value = "96" }
                    }
                }
            }));

            //move to this folder
            var folder = administrationDocumentDetails.DocumentERPByIDResponse_sync_V1.Document;
            var file = attachedFile.DocumentERPCreateConfirmation_sync_V1.Document;
            await using var addFileToFolderClient = _sapSoap.GetAddFileToFolderClient();
            await addFileToFolderClient.Target.ZZ_MH_ADD_DIR_TO_FOLDER_V1Async(new ZZ_MH_ADD_DIR_TO_FOLDER_V1Request(new ZZ_MH_ADD_DIR_TO_FOLDER_V11
            {
                IMP_DIR_DOCUMENTNUMBER = file.Name,
                IMP_DIR_DOCUMENTPART = file.AlternativeDocumentID,
                IMP_DIR_DOCUMENTTYPE = file.TypeCode.Value,
                IMP_DIR_DOCUMENTVERSION = file.VersionID,
                IMP_FOLDER_DOCUMENTNUMBER = folder.Name,
                IMP_FOLDER_DOCUMENTPART = folder.AlternativeDocumentID,
                IMP_FOLDER_DOCUMENTTYPE = folder.TypeCode.Value,
                IMP_FOLDER_DOCUMENTVERSION = folder.VersionID
            }));

            return $"{file.Name}{Constants.ReferenceSeparator}{file.TypeCode.Value}{Constants.ReferenceSeparator}{file.AlternativeDocumentID}{Constants.ReferenceSeparator}{file.VersionID}";
        }
    }
}
