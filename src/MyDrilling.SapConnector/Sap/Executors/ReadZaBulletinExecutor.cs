﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentResults;
using Microsoft.EntityFrameworkCore;
using SapService15;
using SapService6;
using SapService8;
using MyDrilling.Core.Entities.Bulletin;
using MyDrilling.Infrastructure.Data;
using DateTime = System.DateTime;

namespace MyDrilling.SapConnector.Sap.Executors
{
    public interface IReadZaBulletinExecutor
    {
        Task<Result<BulletinMessage>> Read(string referenceId);
    }

    /// <summary>
    /// ReadZaNotficationV1_0.odx
    /// </summary>
    public class ReadZaBulletinExecutor : IReadZaBulletinExecutor
    {
        private readonly ISoapSapClientsFactory _sapFactory;
        private readonly MyDrillingDb _db;

        private readonly Dictionary<string, string> _bulletinTypesMap = new Dictionary<string, string>
        {
            {"HSE_ALERT", BulletinTypes.HseAlert},
            {"HSE_BULL", BulletinTypes.HseBulletin},
            {"PROD_BULL", BulletinTypes.ProductBulletin},
            {"HSE_A_GEN", BulletinTypes.HseAlertGeneral},
            {"HSE_B_GEN", BulletinTypes.HseBulletinGeneral},
            {"PROD_GEN", BulletinTypes.ProductBulletinGeneral}
        };

        public ReadZaBulletinExecutor(ISoapSapClientsFactory sapFactory, MyDrillingDb db)
        {
            _sapFactory = sapFactory;
            _db = db;
        }

        public async Task<Result<BulletinMessage>> Read(string referenceId)
        {
            var documentsResponse = await GetDocumentListAsync(referenceId);
            if (documentsResponse.ServiceRequestERPAttachmentFolderByIDResponse_sync.ServiceRequest?.AttachmentFolder == null)
            {
                var zaLog = documentsResponse.ServiceRequestERPAttachmentFolderByIDResponse_sync.Log;
                return Result.Fail($"Failed on reading document list for ZA bulletin (ref - {referenceId}). reason: {zaLog.Item[0].Note}");
            }
            var documents = documentsResponse.ServiceRequestERPAttachmentFolderByIDResponse_sync.ServiceRequest.AttachmentFolder;
            
            foreach (var doc in documents)
            {
                if (doc.TypeCode.Value.ToUpper() != "BUL")
                    continue;

                var docDetails = await ReadDocumentDetailsAsync(doc);
                if (docDetails.DocumentERPByIDResponse_sync_V1.Document.Status.Code.Value != "71") //Status code 71 - Released
                {
                    continue;
                }

                var issuedDate = docDetails.DocumentERPByIDResponse_sync_V1.Document.PropertyValuation.SelectMany(x => x.Valuation)
                    .FirstOrDefault(x => x.PropertyReference.ID.Value == "ISSUE_DATE2")
                    ?.ValueGroup.FirstOrDefault()
                    ?.PropertyValue.DateSpecification.Date;

                var notificationData = await ReadNotificationDataAsync(referenceId);

                var type = notificationData.ZZ_MH_NOTIFICATION_DATA_GET_V1Response.VIQMEL.ZZBULLETINTYPE;
                if (!_bulletinTypesMap.TryGetValue(type, out var bulType))
                {
                    continue;
                }

                var bulletinType = await _db.Bulletin_BulletinTypes.FirstOrDefaultAsync(bt => string.Equals(bt.Name, bulType));
                if (bulletinType == null)
                {
                    continue;
                }

                var document = docDetails.DocumentERPByIDResponse_sync_V1.Document;

                var bulletin = new ZaBulletin
                {
                    ReferenceId = referenceId.ToMyDrillingId(),
                    Type = bulletinType,
                    IssueDate = issuedDate.HasValue ? issuedDate.Value.ToUniversalTime() : DateTime.MinValue,
                    Title = document.Description.Value,
                    Description = document.LongDescription!= null && string.IsNullOrEmpty(document.LongDescription.Value) 
                        ? document.LongDescription.Value
                            .Replace(document.Description.Value, "")
                            .Replace("#", "")
                        : null,
                    DocumentReferenceId = document.Name,
                    FileType = document.FileVariant[0].DocumentFormatCode.Value
                };

                return Result.Ok(new BulletinMessage(bulletin));
            }

            return Result.Fail("Cannot find za bulletin in SAP.");
        }

        public async Task<ServiceRequestERPAttachmentFolderByIDQueryResponse_InResponse> GetDocumentListAsync(string referenceId)
        {
            await using var client = _sapFactory.GetDocumentsForFolderClient();
            return await client.Target.ServiceRequestERPAttachmentFolderByIDQueryResponse_InAsync(new ServiceRequestERPAttachmentFolderByIDQueryResponse_InRequest(new ServiceRequestERPAttachmentFolderByIDQueryMessage_sync
            {
                ServiceRequestAttachmentFolderSelectionByID = new ServiceRequestERPAttachmentFolderByIDQueryMessage_syncServiceRequestAttachmentFolderSelectionByID
                {
                    ID = new ServiceRequestID { Value = referenceId }
                }
            }));
        }

        public async Task<DocumentERPByIDQueryResponse_In_V1Response> ReadDocumentDetailsAsync(SrvcReqERPAttchFldrByIDRsp_sDoc doc)
        {
            await using var client = _sapFactory.GetReadDocumentDetailsClient();
            return await client.Target.DocumentERPByIDQueryResponse_In_V1Async(new DocumentERPByIDQueryResponse_In_V1Request(new DocumentByIDQueryMessage_sync
            {
                DocumentSelectionByID = new DocumentByIDQueryMessage_syncDocumentSelectionByID
                {
                    DocumentName = doc.Name,
                    DocumentTypeCode = new SapService8.DocumentTypeCode
                    {
                        listID = doc.TypeCode.listID,
                        listVersionID = doc.TypeCode.listVersionID,
                        listAgencyID = doc.TypeCode.listAgencyID,
                        listAgencySchemeID = doc.TypeCode.listAgencySchemeID,
                        listAgencySchemeAgencyID = doc.TypeCode.listAgencySchemeAgencyID,
                        Value = doc.TypeCode.Value,
                    },
                    AlternativeDocumentID = doc.AlternativeDocumentID,
                    DocumentVersionID = doc.VersionID
                }
            }));
        }

        public async Task<ZZ_MH_NOTIFICATION_DATA_GET_V1Response1> ReadNotificationDataAsync(string referenceId)
        {
            await using var client = _sapFactory.GetReadNotificationDataClient();
            return await client.Target.ZZ_MH_NOTIFICATION_DATA_GET_V1Async(new ZZ_MH_NOTIFICATION_DATA_GET_V1Request(new ZZ_MH_NOTIFICATION_DATA_GET_V11 { P_QMNUM = referenceId.ToSapReferenceId() }));
        }
    }

    public sealed class IdentifierDetails
    {
        public string GlobalId { get; set; }
        public string ParentGlobalId { get; set; }
        public string ReferenceId { get; set; }
    }

    public sealed class BulletinMessage
    {
        public BulletinMessage(ZaBulletin zaBulletin, ZbBulletin zbBulletin) : this(zaBulletin)
        {
            ZbBulletin = zbBulletin;
        }

        public BulletinMessage(ZaBulletin zaBulletin)
        {
            ZaBulletin = zaBulletin;
        }

        public ZaBulletin ZaBulletin { get; }
        public ZbBulletin ZbBulletin { get; }
    }



    public sealed class BulletinImplementedOut
    {
        public IdentifierDetails Identifier { get; set; }
        public Confirmation Confirmation { get; set; }
        public string EquipmentId { get; set; }
        public string State { get; set; }
        public string Status { get; set; }
    }

    public sealed class BulletinReceivedOut
    {
        public IdentifierDetails Identifier { get; set; }
        public Confirmation Confirmation { get; set; }
        public string RigId { get; set; }
        public string State { get; set; }
    }

    public sealed class Confirmation
    {
        public SignatureDetails Signature { get; set; }
        public DateTime ConfirmationTimeStamp { get; set; }

        public sealed class SignatureDetails
        {
            public string Value { get; set; }
            public string Type { get; set; }
        }
    }
}
