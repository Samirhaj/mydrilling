﻿using System.Linq;
using System.Threading.Tasks;
using SapService4;

namespace MyDrilling.SapConnector.Sap.Executors
{
    public interface IReadEnquiryExecutor
    {
        Task<ServiceRequestERPByIDQueryResponse_In_V1Response> ReadEnquiry(string referenceId);
        Task<int> GetLastCommentNumber(string enquiryReferenceId);
    }

    public class ReadEnquiryExecutor : IReadEnquiryExecutor
    {
        private readonly ISoapSapClientsFactory _sapSoap;

        public ReadEnquiryExecutor(ISoapSapClientsFactory sapSoap)
        {
            _sapSoap = sapSoap;
        }

        public async Task<ServiceRequestERPByIDQueryResponse_In_V1Response> ReadEnquiry(string referenceId)
        {
            await using var client = _sapSoap.GetReadEnquiryClient();
            return await client.Target.ServiceRequestERPByIDQueryResponse_In_V1Async(new ServiceRequestERPByIDQueryResponse_In_V1Request(new SrvcReqERPByIDQryMsg_s_V1
            {
                ServiceRequestSelectionByID = new SrvcReqERPByIDQry_s_V1SrvcReq { ServiceRequestID = referenceId.ToSapReferenceId() }
            }));
        }

        public async Task<int> GetLastCommentNumber(string enquiryReferenceId)
        {
            var enquiry = await ReadEnquiry(enquiryReferenceId);

            if (enquiry.ServiceRequestERPByIDResponse_sync_V1.ServiceRequest.Activity == null
                || enquiry.ServiceRequestERPByIDResponse_sync_V1.ServiceRequest.Activity.Length == 0)
            {
                return 0;
            }

            var numbers = enquiry.ServiceRequestERPByIDResponse_sync_V1.ServiceRequest.Activity.Select(x => x.ID)
                .Where(x => x != null)
                .Select(x => int.TryParse(x.TrimStart('0'), out var number) ? number : -1)
                .Where(x => x > 0)
                .ToArray();

            return numbers.Length == 0 ? 0 : numbers.Max();
        }
    }
}
