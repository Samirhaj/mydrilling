﻿using System.Threading.Tasks;
using MyDrilling.Core;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Enquiry;
using MyDrilling.Core.Entities.Equipment;
using SapService1;

namespace MyDrilling.SapConnector.Sap.Executors
{
    public interface ICreateEnquiryExecutor
    {
        Task<string> CreateEnquiry(string title, TypeInfo.Type type, string customerSapId, long rigSapId, long? equipmentSapId);
    }

    public class CreateEnquiryExecutor : ICreateEnquiryExecutor
    {
        private readonly ISoapSapClientsFactory _sapSoap;
        private const int SapTitleMaxLength = 40;

        public CreateEnquiryExecutor(ISoapSapClientsFactory sapSoap)
        {
            _sapSoap = sapSoap;
        }

        public async Task<string> CreateEnquiry(string title,
            TypeInfo.Type type,
            string customerSapId,
            long rigSapId,
            long? equipmentSapId)
        {
            var coding = Constants.EnquiryTypesToSapCodings[type];
            var enqTitle = title.Length > SapTitleMaxLength
                ? title.Substring(0, SapTitleMaxLength)
                : title;
            await using var createEnquiryClient = _sapSoap.GetCreateEnquiryClient();
            var res = await createEnquiryClient.Target.ZZMYDRILLING_CREATE_NOTIF_V02Async(new ZZMYDRILLING_CREATE_NOTIF_V02Request(
                new ZZMYDRILLING_CREATE_NOTIF_V021
                {
                    CODE_GROUP = coding.CodingGroup,
                    CODING = coding.Coding,
                    EQUIPMENT = (equipmentSapId ?? rigSapId).ToStringReferenceId(Equipment.SapReferenceLength),
                    NOTIF_TYPE = "S0",
                    PRIORITY = type == TypeInfo.Type.Support_RigOnDowntime ? "1" : "3",
                    SHORT_TEXT = enqTitle,
                    SOLD_TO = customerSapId,
                    RETURN = new[]
                    {
                        new BAPIRET2
                        {
                            MESSAGE = "0 ",
                            MESSAGE_V1 = "0 ",
                            MESSAGE_V2 = "0 ",
                            MESSAGE_V3 = "0 ",
                            MESSAGE_V4 = "0 "
                        }
                    }
                }));

            return res.ZZMYDRILLING_CREATE_NOTIF_V02Response.NOTIFHEADER_EXPORT.NOTIF_NO.RemoveLeadingZeros();
        }
    }
}
