﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SapService11;
using SapService9;
using DocumentTypeCode = SapService11.DocumentTypeCode;

namespace MyDrilling.SapConnector.Sap.Executors
{
    public interface IReadFileExecutor
    {
        Task<(string Name, string FileFormat, byte[] Content)> GetFile(string fileName, string docName, string docType, string altDocId, string docVersion);
        Task<(string Name, string FileFormat, byte[] Content)> GetFileVariant(string fileName, string docName, string docType, string altDocId, string docVersion);
    }
    public class ReadFileExecutor : IReadFileExecutor
    {
        private readonly ISoapSapClientsFactory _sapSoap;

        public ReadFileExecutor(ISoapSapClientsFactory sapSoap)
        {
            _sapSoap = sapSoap;
        }

        public async Task<(string Name, string FileFormat, byte[] Content)> GetFile(string fileName, string docName, string docType, string altDocId, string docVersion)
        {
            if (string.IsNullOrEmpty(fileName) || string.IsNullOrEmpty(docName) || string.IsNullOrEmpty(docType) || string.IsNullOrEmpty(altDocId) || string.IsNullOrEmpty(docVersion))
            {
                return (string.Empty, string.Empty, Array.Empty<byte>());
            }

            try
            {

                var fileWithVariant = await GetFileWithVariants(docName, docType, altDocId, docVersion, false, true);

                if (fileWithVariant == null) return (string.Empty, string.Empty, Array.Empty<byte>());
                if (fileWithVariant.Document == null || fileWithVariant.Document.FileVariant.Length <= 0)
                    return (fileWithVariant.Log.Item[0].Note, string.Empty, Array.Empty<byte>());
                var fileVariant = fileWithVariant.Document.FileVariant.First();
                return (fileName, fileVariant.DocumentFormatCode.Value, fileVariant.BinaryObject.Value);
            }
            catch (Exception e)
            {
                return (e.Message, string.Empty, Array.Empty<byte>());
            }
        }

        public async Task<(string Name, string FileFormat, byte[] Content)> GetFileVariant(string fileName, string docName, string docType, string altDocId, string docVersion)
        {
            if (string.IsNullOrEmpty(fileName) || string.IsNullOrEmpty(docName) || string.IsNullOrEmpty(docType) || string.IsNullOrEmpty(altDocId) || string.IsNullOrEmpty(docVersion))
            {
                return (string.Empty, string.Empty, Array.Empty<byte>());
            }

            try
            {
                string fileVariantId;
                string fileVariantVersionId;

                var fileWithVariants = await GetFileWithVariants(docName, docType, altDocId, docVersion, true, true);
                
                if (fileWithVariants != null)
                {
                    if (fileWithVariants.Document != null && fileWithVariants.Document.FileVariant.Length > 0)
                    {

                        var fileVariants = fileWithVariants.Document.FileVariant;
                        //check first if CONVERTED DOCUMENT exists; if not, check if pdf exists, if not pick the first variant
                        var docVariant = fileVariants.FirstOrDefault(x => x.Description != null && x.Description.Value.ToLowerInvariant() == "converted document")
                                         ?? fileVariants.FirstOrDefault(x => x.DocumentFormatCode != null && x.DocumentFormatCode.Value.ToLowerInvariant() == "pdf") 
                                                ?? fileVariants.First();
                        fileVariantId = docVariant.ID;
                        fileVariantVersionId = docVariant.VersionID;                        
                    }
                    else
                    {
                        return (fileWithVariants.Log.Item[0].Note, string.Empty, Array.Empty<byte>());
                    }
                }
                else
                {
                    return (string.Empty, string.Empty, Array.Empty<byte>());
                }
         

                if (string.IsNullOrEmpty(fileVariantId) || string.IsNullOrEmpty(fileVariantVersionId))
                {
                    return (string.Empty, string.Empty, Array.Empty<byte>());
                }

                var fileVariant = await GetFileVariant(docName, docType, altDocId, docVersion, fileVariantId, fileVariantVersionId, true);
                if (fileVariant.Document?.FileVariant?.BinaryObject == null)
                    return (fileVariant.Log.Item[0].Note, string.Empty, Array.Empty<byte>());
                var file = fileVariant.Document.FileVariant;
                return (fileName, file.DocumentFormatCode.Value, file.BinaryObject.Value);

            }
            catch (Exception e)
            {
                return (e.Message, string.Empty, Array.Empty<byte>());
            }
        }

        private async Task<DocumentERPFileVariantByIDResponseMessage_sync> GetFileWithVariants(string docName, string docType, string altDocId, string docVersion, bool isLarge, bool tryAgain = false)
        {
            DocumentERPFileVariantByIDResponseMessage_sync fileWithVariants = null;
            try
            {
                await using var client = _sapSoap.GetReadFileClient(TimeSpan.FromSeconds(30));
                var res = await client.Target.DocumentERPFileVariantByIDQueryResponse_InAsync(
                    new DocumentERPFileVariantByIDQueryResponse_InRequest(new DocumentERPFileVariantByIDQueryMessage_sync
                    {
                        DocumentFileVariantSelectionByID =
                            new DocumentERPFileVariantByIDQueryMessage_syncDocumentFileVariantSelectionByID
                            {
                                DocumentName = docName,
                                DocumentTypeCode = new SapService9.DocumentTypeCode { Value = docType },
                                AlternativeDocumentID = altDocId,
                                DocumentVersionID = docVersion,
                                LargeFileHandlingIndicator = isLarge
                            }
                    }));
                fileWithVariants = res.DocumentERPFileVariantByIDResponse_sync;

            }
            catch (Exception)
            {
                if (tryAgain)
                    await GetFileWithVariants(docName, docType, altDocId, docVersion, isLarge);
                else
                {
                    throw;
                }
            }
            return fileWithVariants;
        }

        private async Task<DocumentERPFileVariantByIDAndFileVariantIDResponseMessage_sync> GetFileVariant(string docName, string docType, string altDocId, string docVersion, string fileVariantId, string fileVariantVersionId, bool tryAgain = false)
        {
            DocumentERPFileVariantByIDAndFileVariantIDResponseMessage_sync fileVariant = null;
            try
            {
                await using var clientFileVariant = _sapSoap.GetReadFileVariantClient(TimeSpan.FromSeconds(60));
                var resFileVariant = await clientFileVariant.Target.DocumentERPFileVariantByIDAndFileVariantIDQueryResponse_InAsync(
                    new DocumentERPFileVariantByIDAndFileVariantIDQueryResponse_InRequest(
                        new DocumentERPFileVariantByIDAndFileVariantIDQueryMessage_sync
                        {
                            DocumentFileVariantSelectionByIDAndFileVariantID =
                                new
                                    DocumentERPFileVariantByIDAndFileVariantIDQueryMessage_syncDocumentFileVariantSelectionByIDAndFileVariantID()
                                    {
                                        DocumentName = docName,
                                        DocumentTypeCode = new DocumentTypeCode { Value = docType },
                                        AlternativeDocumentID = altDocId,
                                        DocumentVersionID = docVersion,
                                        DocumentFileVariantID = fileVariantId,
                                        DocumentFileVariantVersionID = fileVariantVersionId,
                                        LargeFileHandlingIndicator = false
                                    }
                        }));
                fileVariant = resFileVariant.DocumentERPFileVariantByIDAndFileVariantIDResponse_sync;
            }
            catch (Exception)
            {
                if (tryAgain)
                {
                    await GetFileVariant(docName, docType, altDocId, docVersion, fileVariantId, fileVariantVersionId);
                }
                else
                {
                    throw;
                }
            }
            return fileVariant;
        }
    }
}
