﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using FluentResults;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Bulletin;
using MyDrilling.Core.Entities.Equipment;
using MyDrilling.Core.Entities.Rig;
using MyDrilling.Core.Entities.Subscription;
using MyDrilling.Infrastructure.Data;
using SapService12;
using SapService13;
using SapService14;
using SapService4;
using SapService5;

namespace MyDrilling.SapConnector.Sap.Executors
{
    public interface IReadZbBulletinExecutor
    {
        Task<Result<BulletinMessage>> Read(string referenceId, AlmNotifGetDetailResponse1 notification);
    }

    /// <summary>
    /// ReadZbNotificationV1_0.odx
    /// </summary>
    public class ReadZbBulletinExecutor : IReadZbBulletinExecutor
    {
        private readonly ISoapSapClientsFactory _sapFactory;
        private readonly IReadZaBulletinExecutor _zaExecutor;
        private readonly MyDrillingDb _db;

        public ReadZbBulletinExecutor(ISoapSapClientsFactory sapFactory,
            IReadZaBulletinExecutor zaExecutor,
            MyDrillingDb db)
        {
            _sapFactory = sapFactory;
            _zaExecutor = zaExecutor;
            _db = db;
        }

        public async Task<Result<BulletinMessage>> Read(string referenceId, AlmNotifGetDetailResponse1 notification)
        {
            var notifHeader = notification.AlmNotifGetDetailResponse.NotifheaderExport;
            var zaNotificationId = notifHeader.PurchNoC;
            if (string.IsNullOrEmpty(zaNotificationId) || zaNotificationId.Length < 9 || !int.TryParse(zaNotificationId, out var zaResult))
            {
                var docFlow = await ReadNotificationDocumentFlowAsync(referenceId);
                var linkWithZaData = docFlow.ZZ_MH_NOTIFICATION_DOC_FLOW_V1Response.I_LINKS.FirstOrDefault(x => x.OBJTYPE_A == "BUS2080");
                if (linkWithZaData == null)
                {
                    return Result.Fail($"Unable to find ZA bulletin (ref. - {referenceId}) in DocFlow response.");
                }
                zaNotificationId = linkWithZaData.OBJKEY_A.ToMyDrillingId();
            }

            var zaBulletin = await _db.Bulletin_ZaBulletins.FirstOrDefaultAsync(za => string.Equals(za.ReferenceId, zaNotificationId));
            if (zaBulletin == null)
            {
                var bulletinResult = await _zaExecutor.Read(zaNotificationId.ToSapReferenceId());
                if (bulletinResult == null || bulletinResult.IsFailed)
                {
                    return Result.Fail("Unable to find ZA bulletin.");
                }
                zaBulletin = bulletinResult.Value.ZaBulletin;
            }

            Result<ZbBulletin> zbResult;
            if (notifHeader.Userstatus.Contains("NOTV"))
            {
                zbResult = await ProcessReceivedAsync(referenceId, notification.AlmNotifGetDetailResponse);
            }
            else
            {
                zbResult = await ProcessImplementedAsync(referenceId, notification.AlmNotifGetDetailResponse);
            }
            if (zbResult.IsSuccess)
                return Result.Ok(new BulletinMessage(zaBulletin, zbResult.Value));
            return Result.Fail($"Unable to import ZB bulletin with reference '{referenceId}'. Reason: {zbResult}");
        }

        /// <summary>
        /// FillBulletinReceiveConfirmationV1_0.odx
        /// </summary>
        private async Task<Result<ZbBulletin>> ProcessReceivedAsync(string referenceId, AlmNotifGetDetailResponse notification)
        {
            var contactDetails = await ReadBulletinReceivedConfirmerContactDetailsAsync(referenceId);
            var signature = contactDetails.ZZ_MH_NOTIF_CONTACTPERSON_V1Response.SMTP_ADDR;
            User user = null;
            if (!string.IsNullOrEmpty(signature))
            {
                user = await _db.Users.FirstOrDefaultAsync(u => string.Equals(u.Upn, signature));
            }

            var equipmentId = notification.NotifheaderExport.Equipment.ToMyDrillingId();
            var rig = await ExtractRigInfoFromEquipment(equipmentId);
            if (rig == null)
            {
                return Result.Fail($"Missing equipment or rig data, equipment ref. - {equipmentId}");
            }

            var notificationStatus = notification.NotifheaderExport.SysStatus;
            var zbBulletin = new ZbBulletin
            {
                ReferenceId = referenceId.ToMyDrillingId(),
                RigId = rig?.Id,
                ConfirmedByUser = user,
                ConfirmationTimestamp = GetConfirmationTimeStamp(notification.NotifheaderExport),
                Group = BulletinGroup.Received,
                State = notificationStatus.ToBulletinState()
            };

            return Result.Ok(zbBulletin);
        }

        /// <summary>
        /// FillBulletinImplementationConfirmationV1_0.odx
        /// </summary>
        private async Task<Result<ZbBulletin>> ProcessImplementedAsync(string referenceId, AlmNotifGetDetailResponse notification)
        {
            var userName = notification.NotifheaderExport.ChangedBy;
            var user = await _db.Users.FirstOrDefaultAsync(u => string.Equals(u.Upn, userName));

            var eqRefId = notification.NotifheaderExport.Equipment;
            Equipment equipment = null;
            Rig rig = null;
            if (!string.IsNullOrEmpty(eqRefId))
            {
                equipment = await _db.Equipments.Include(x => x.Rig).FirstOrDefaultAsync(e => e.ReferenceId == eqRefId.ToLongReferenceId());
                if (equipment != null)
                {
                    rig = equipment.Rig;
                }
            }
            if (equipment == null || rig == null)
            {
                return Result.Fail($"Missing equipment or rig data, equipment ref. - {eqRefId}");
            }

            var userStatus = notification.NotifheaderExport.Userstatus;
            var notificationStatus = notification.NotifheaderExport.SysStatus;

            var bulletinImplemented = new ZbBulletin
            {
                ReferenceId = referenceId.ToMyDrillingId(),
                EquipmentId = equipment?.Id,
                RigId = rig?.Id,
                State = notificationStatus.ToBulletinState(),
                Status = userStatus.ToBulletinStatus(),
                ConfirmationTimestamp = GetConfirmationTimeStamp(notification.NotifheaderExport),
                ConfirmedByUser = user,
                Group = BulletinGroup.Implemented
            };

            //for adding comment on bulletin (not in use at all)
            //var details = await ReadSapNotificationDetails(referenceId);
            //var conversations = details.ServiceRequestERPByIDResponse_sync_V1.ServiceRequest.Activity;
            //if (conversations != null)
            //{
            //    foreach (var conversation in conversations)
            //    {
            //        if (conversation.ActivityParentServiceIssueCategoryID == "MYDR"
            //            && conversation.ActivityServiceIssueCategoryID == "BULL")
            //        {
            //            break;
            //        }
            //    }
            //}

            return Result.Ok(bulletinImplemented);
        }


        public async Task<ZZ_MH_NOTIFICATION_DOC_FLOW_V1Response1> ReadNotificationDocumentFlowAsync(string referenceId)
        {
            await using var client = _sapFactory.GetReadNotificationDocumentFlowClient();
            return await client.Target.ZZ_MH_NOTIFICATION_DOC_FLOW_V1Async(new ZZ_MH_NOTIFICATION_DOC_FLOW_V1Request(new ZZ_MH_NOTIFICATION_DOC_FLOW_V1
            {
                P_QMNUM = referenceId.ToSapReferenceId(),
                I_LINKS = Array.Empty<RELGRAPHLK>()
            }));
        }

        public async Task<ZZ_MH_NOTIF_CONTACTPERSON_V1Response1> ReadBulletinReceivedConfirmerContactDetailsAsync(string referenceId)
        {
            await using var client = _sapFactory.GetReadBulletinReceivedConfirmerContactDetailsClient();
            return await client.Target.ZZ_MH_NOTIF_CONTACTPERSON_V1Async(new ZZ_MH_NOTIF_CONTACTPERSON_V1Request(new ZZ_MH_NOTIF_CONTACTPERSON_V1 { P_QMNUM = referenceId.ToSapReferenceId() }));
        }

        public async Task<IndividualMaterialByIDQueryResponse_In_V1Response> ReadMaterialClientAsync(string equipmentId)
        {
            await using var client = _sapFactory.GetReadMaterialClient();
            return await client.Target.IndividualMaterialByIDQueryResponse_In_V1Async(
                new IndividualMaterialByIDQueryResponse_In_V1Request(
                    new IndivMatlERPByIDQryMsg_s_V1
                    {
                        IndividualMaterialSelectionByID = new IndivMatlERPByIDQry_s_V1Sel { IndividualMaterialID = equipmentId }
                    }
                )
            );
        }

        public async Task<ServiceRequestERPByIDQueryResponse_In_V1Response> ReadSapNotificationDetails(string referenceId)
        {
            await using var client = _sapFactory.GetSapNotificationDetailsClient();
            return await client.Target.ServiceRequestERPByIDQueryResponse_In_V1Async(new ServiceRequestERPByIDQueryResponse_In_V1Request(new SrvcReqERPByIDQryMsg_s_V1
            {
                ServiceRequestSelectionByID = new SrvcReqERPByIDQry_s_V1SrvcReq { ServiceRequestID = referenceId.ToSapReferenceId() }
            }));
        }

        private async Task<Rig> ExtractRigInfoFromEquipment(string equipmentId)
        {
            string rigId = null;

            //search in tree until rig is found
            while (!string.IsNullOrEmpty(equipmentId))
            {
                var material = await ReadMaterialClientAsync(equipmentId);
                var individualMaterialTag = material.IndividualMaterialByIDResponse_sync_V1.IndividualMaterial;
                var equipmentLevel = individualMaterialTag.CategoryCode;

                if (equipmentLevel == "R")
                {
                    rigId = individualMaterialTag.ID.ToSapRigId();
                    break;
                }
                equipmentId = individualMaterialTag.HierarchyRelationship?.ParentProductID;
            }

            Rig rig = null;
            if (!string.IsNullOrWhiteSpace(rigId))
            {
                rig = await _db.Rigs.FirstOrDefaultAsync(r => Equals(r.ReferenceId, rigId.ToLongReferenceId()));
            }

            return rig;
        }

        private static DateTime GetConfirmationTimeStamp(Bapi2080Nothdre export)
        {
            if (!DateTime.TryParseExact(export.Compdate, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal, out var compDate)
                || compDate == DateTime.MinValue)
            {
                return DateTime.MinValue;
            }

            if (export.Comptime > DateTime.MinValue)
            {
                var comptimeUtc = export.Comptime.ToUniversalTime();
                return compDate.AddHours(comptimeUtc.Hour)
                    .AddMinutes(comptimeUtc.Minute)
                    .AddSeconds(comptimeUtc.Second)
                    .AddMilliseconds(comptimeUtc.Millisecond);
            }

            return compDate;
        }

    }

    public static class MyDrillingExt
    {
        public static string ToMyDrillingId(this string id)
        {
            return string.IsNullOrEmpty(id) ? id : id.TrimStart('0');
        }

        public static BulletinState ToBulletinState(this string state)
        {
            if (string.IsNullOrWhiteSpace(state))
            {
                return BulletinState.None;
            }
            return state.ToUpper().Equals("NOCO") ? BulletinState.Confirmed : BulletinState.Active;
        }

        public static BulletinStatus ToBulletinStatus(this string status)
        {
            return status.ToUpper().Contains("IMPL")
                ? BulletinStatus.Implemented
                : status.ToUpper().Contains("NA") ? BulletinStatus.Na : BulletinStatus.None;
        }

        public static long? ToLongReferenceId(this string referenceId)
        {
            referenceId = referenceId.Trim().TrimStart('0');
            return long.TryParse(referenceId, out long parsed) ? parsed : (long?)null;
        }
    }
}