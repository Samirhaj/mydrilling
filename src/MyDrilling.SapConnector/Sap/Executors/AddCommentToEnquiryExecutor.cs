﻿using System.Linq;
using System.Threading.Tasks;
using SapService2;

namespace MyDrilling.SapConnector.Sap.Executors
{
    public interface IAddCommentToEnquiryExecutor
    {
        Task<string> AddCommentToEnquiry(string enquiryReferenceId, string commentText, int commentNumber, string createdBy);
    }

    public class AddCommentToEnquiryExecutor : IAddCommentToEnquiryExecutor
    {
        private const int SapCommentDescriptionMaxLength = 40;
        private readonly ISoapSapClientsFactory _sapSoap;

        public AddCommentToEnquiryExecutor(ISoapSapClientsFactory sapSoap)
        {
            _sapSoap = sapSoap;
        }

        public async Task<string> AddCommentToEnquiry(string enquiryReferenceId, string commentText, int commentNumber, string createdBy)
        {
            var sapCommentNumber = commentNumber.ToSapCommentNumber();
            var description = $"Comment added by {createdBy}";
            description = description.Length > SapCommentDescriptionMaxLength
                ? description.Substring(0, SapCommentDescriptionMaxLength)
                : description;

            await using var createEnquiryClient = _sapSoap.GetAddCommentToEnquiryClient();
            await createEnquiryClient.Target.ZzbapiAlmNotifDataAddAsync(new ZzbapiAlmNotifDataAddRequest(new ZzbapiAlmNotifDataAdd
            {
                Number = enquiryReferenceId.ToSapReferenceId(),
                Notfulltxt = commentText.SplitCommentForSap().Select(x => new Bapi2080Notfulltxti
                {
                    Objtype = "QMMA",
                    Objkey = sapCommentNumber,
                    FormatCol = "*",
                    TextLine = x
                }).ToArray(),
                Notifactv = new[]
                {
                    new Bapi2080Notactvi
                    {
                        ActSortNo = sapCommentNumber,
                        Acttext = description,
                        ActCodegrp = "MYDR",
                        ActCode = "DESC"
                    }
                },
                Return = new[]
                {
                    new Bapiret2{Message = "X"}
                }
            }));

            return $"{enquiryReferenceId.RemoveLeadingZeros()}{Constants.ReferenceSeparator}{sapCommentNumber}";
        }
    }
}
