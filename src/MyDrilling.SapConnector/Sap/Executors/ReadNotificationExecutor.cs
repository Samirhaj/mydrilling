﻿using System.Threading.Tasks;
using SapService5;

namespace MyDrilling.SapConnector.Sap.Executors
{
    public interface IReadNotificationExecutor
    {
        Task<AlmNotifGetDetailResponse1> ReadNotification(string referenceId);
    }

    public class ReadNotificationExecutor : IReadNotificationExecutor
    {
        private readonly ISoapSapClientsFactory _sapSoap;

        public ReadNotificationExecutor(ISoapSapClientsFactory sapSoap)
        {
            _sapSoap = sapSoap;
        }

        public async Task<AlmNotifGetDetailResponse1> ReadNotification(string referenceId)
        {
            await using var client = _sapSoap.GetReadNotificationClient();
            return await client.Target.AlmNotifGetDetailAsync(new AlmNotifGetDetailRequest(new AlmNotifGetDetail
            {
                Notifpartnr = new[] { new Bapi2080Notpartnre { PartnRole = "ZN" } },
                Number = referenceId.ToSapReferenceId()
            }));
        }
    }
}
