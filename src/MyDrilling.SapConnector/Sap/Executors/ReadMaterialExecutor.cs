﻿using System.Threading.Tasks;
using SapService14;

namespace MyDrilling.SapConnector.Sap.Executors
{
    public interface IReadMaterialExecutor
    {
        Task<IndividualMaterialByIDQueryResponse_In_V1Response> ReadMaterialClientAsync(string equipmentId);
    }

    public class ReadMaterialExecutor : IReadMaterialExecutor
    {
        private readonly ISoapSapClientsFactory _sapSoap;

        public ReadMaterialExecutor(ISoapSapClientsFactory sapSoap)
        {
            _sapSoap = sapSoap;
        }

        public async Task<IndividualMaterialByIDQueryResponse_In_V1Response> ReadMaterialClientAsync(string equipmentId)
        {
            await using var client = _sapSoap.GetReadMaterialClient();
            return await client.Target.IndividualMaterialByIDQueryResponse_In_V1Async(
                new IndividualMaterialByIDQueryResponse_In_V1Request(
                    new IndivMatlERPByIDQryMsg_s_V1
                    {
                        IndividualMaterialSelectionByID = new IndivMatlERPByIDQry_s_V1Sel { IndividualMaterialID = equipmentId.ToSapRigId() }
                    }
                )
            );
        }
    }
}
