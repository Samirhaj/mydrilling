﻿using System.Threading.Tasks;
using MyDrilling.Infrastructure.Services.AsyncOperation.Entities;
using SapService10;

namespace MyDrilling.SapConnector.Sap.Executors
{
    public interface IRequestSparepartPriceExecutor
    {
        Task<SparepartPrice> RequestSparepartPrice(string referenceId, string customerId);
    }

    public class RequestSparepartPriceExecutor : IRequestSparepartPriceExecutor
    {
        private readonly ISoapSapClientsFactory _sapFactory;

        public RequestSparepartPriceExecutor(ISoapSapClientsFactory sapFactory)
        {
            _sapFactory = sapFactory;
        }

        public async Task<SparepartPrice> RequestSparepartPrice(string referenceId, string customerId)
        {
            await using var requestPriceClient = _sapFactory.GetRequestSparepartPriceClient();
            
            var request = new ZZ_MH_SALESORDER_SIMULATE_V10Request(
                new ZZ_MH_SALESORDER_SIMULATE_V101
                {
                    MATERIAL = referenceId,
                    CUSTOMER = customerId,
                    QUANTITY = Constants.SparepartsQuantity,
                    SALESORG = Constants.SparepartsSalesOrg,
                    SCHEDULE_LINES = new[] 
                    {
                        new ZZBAPIWMDVE
                        {
                            WERKS = Constants.MhWirthKristiansand
                        },
                        new ZZBAPIWMDVE
                        {
                            WERKS = Constants.MhWirthSingapore
                        },
                        new ZZBAPIWMDVE
                        {
                            WERKS = Constants.MhWirthHouston
                        }
                    },
                    RETURN_TAB = new[]
                    {
                        new BAPIRET2()
                    }
                });

            var response = await requestPriceClient.Target.ZZ_MH_SALESORDER_SIMULATE_V10Async(request);
            var price = response.ZZ_MH_SALESORDER_SIMULATE_V10Response.NET_VALUE / Constants.SparepartsQuantity;
            var currency = response.ZZ_MH_SALESORDER_SIMULATE_V10Response.CURRENCY;

            return new SparepartPrice { Value = price, Currency = currency};
        }
    }
}