﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MyDrilling.Core.Entities;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.Storage.Queue;
using MyDrilling.SapConnector.Sap.Executors;

namespace MyDrilling.SapConnector
{
    public class HealthReporter : BackgroundService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly IQueueClientFactory _queueClientFactory;
        private readonly ILogger _logger;
        private readonly BlobServiceClient _blobServiceClient;
        private readonly IReadNotificationExecutor _readNotificationExecutor;
        private readonly TimeSpan _interval;

        public HealthReporter(IServiceProvider serviceProvider,
            IQueueClientFactory queueClientFactory,
            BlobServiceClient blobServiceClient,
            IReadNotificationExecutor readNotificationExecutor,
            ILogger<HealthReporter> logger,
            IConfiguration config)
        {
            _serviceProvider = serviceProvider;
            _queueClientFactory = queueClientFactory;
            _blobServiceClient = blobServiceClient;
            _readNotificationExecutor = readNotificationExecutor;
            _logger = logger;
            _interval = config.GetValue<TimeSpan>("HealthReportInterval");
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return ReportHealthAsync(stoppingToken);
        }

        private async Task ReportHealthAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                using (var scope = _serviceProvider.CreateScope())
                {
                    var db = scope.ServiceProvider.GetRequiredService<MyDrillingDb>();

                    try
                    {
                        var state = await db.SapConnectorStatuses.SingleOrDefaultAsync(cancellationToken: stoppingToken);
                        if (state == null)
                        {
                            state = new SapConnectorStatus();
                            db.SapConnectorStatuses.Add(state);
                        }
                        
                        state.Timestamp = DateTime.UtcNow;
                        state.BlobsState = await GetBlobState(stoppingToken);
                        state.QueuesState = await GetQueueState(stoppingToken);
                        state.SapState = await GetSapState();

                        await db.SaveChangesAsync(stoppingToken);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, "unable to update status");
                    }
                }

                await Task.Delay(_interval, stoppingToken);
            }
        }

        private async Task<bool> GetBlobState(CancellationToken stoppingToken)
        {
            try
            {
                return await _blobServiceClient
                    .GetBlobContainerClient(Infrastructure.Storage.StorageConfig.TemporaryDocumentsContainerName)
                    .ExistsAsync(stoppingToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "blobs not available");
                return false;
            }
        }

        private async Task<bool> GetQueueState(CancellationToken stoppingToken)
        {
            try
            {
                var queueChecks = new[]
                {
                    _queueClientFactory.GetClient(Infrastructure.Storage.StorageConfig.SyncEnquiryToSapQueue).ExistsAsync(stoppingToken),
                    _queueClientFactory.GetClient(Infrastructure.Storage.StorageConfig.RequestSparepartPriceQueue).ExistsAsync(stoppingToken),
                    _queueClientFactory.GetClient(Infrastructure.Storage.StorageConfig.EnquiryNotificationFromSapQueue).ExistsAsync(stoppingToken),
                    _queueClientFactory.GetClient(Infrastructure.Storage.StorageConfig.BulletinNotificationFromSapQueue).ExistsAsync(stoppingToken),
                    _queueClientFactory.GetClient(Infrastructure.Storage.StorageConfig.FileRequestQueue).ExistsAsync(stoppingToken)
                };
                await Task.WhenAll(queueChecks);
                return queueChecks.All(x => x.Result.Value);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "queues not available");
                return false;
            }
        }

        private async Task<bool> GetSapState()
        {
            try
            {
                //just read random notification available in both eaq and eap
                var notification = await _readNotificationExecutor.ReadNotification("000300741503");
                return notification != null;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "sap not available");
                return false;
            }
        }
    }
}
