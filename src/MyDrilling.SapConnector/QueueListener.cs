using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Azure;
using Azure.Storage.Queues;
using Azure.Storage.Queues.Models;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.DataContracts;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MyDrilling.Infrastructure.Storage.Queue;
using MyDrilling.SapConnector.MessageHandlers;
using Newtonsoft.Json.Linq;

namespace MyDrilling.SapConnector
{
    public class QueueListener<TMessageHandler> : BackgroundService where TMessageHandler : IMessageHandler
    {
        private readonly QueueClient _queueClient;
        private readonly QueueClient _poisonQueueClient;
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger _logger;
        private readonly StorageConfig.QueueConfig _queueConfig;
        private readonly TelemetryClient _telemetryClient;

        public QueueListener(IServiceProvider serviceProvider, 
            StorageConfig.QueueConfig queueConfig)
        {
            _queueConfig = queueConfig;
            _serviceProvider = serviceProvider;
            _logger = serviceProvider.GetRequiredService<ILogger<QueueListener<TMessageHandler>>>();
            _telemetryClient = serviceProvider.GetRequiredService<TelemetryClient>();

            var queueClientFactory = serviceProvider.GetRequiredService<IQueueClientFactory>();
            _queueClient = queueClientFactory.GetClient(_queueConfig.QueueName);
            _poisonQueueClient = queueClientFactory.GetClient($"{_queueConfig.QueueName}-poison");
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return HandleMessagesAsync(stoppingToken);
        }

        private async Task HandleMessagesAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                Response<QueueMessage[]> messages;
                try
                {
                    messages = await _queueClient.ReceiveMessagesAsync(_queueConfig.MaxMessagesInBatch, _queueConfig.VisibilityTimeout, stoppingToken);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "failed to read messages from {queueName}: {message}", _queueConfig.QueueName, ex.Message);
                    await Task.Delay(_queueConfig.PollingDelay, stoppingToken);
                    continue;
                }

                if (messages.Value.Length == 0)
                {
                    await Task.Delay(_queueConfig.PollingDelay, stoppingToken);
                    continue;
                }

                await Task.WhenAll(messages.Value.Select(x => HandleMessageAsync(x, stoppingToken)));
            }
        }

        private async Task HandleMessageAsync(QueueMessage msg, CancellationToken stoppingToken)
        {
            using (var operation = _telemetryClient.StartOperation(new RequestTelemetry { Name = typeof(TMessageHandler).Name }))
            { 
                var messageText = msg.MessageText;
                try
                {
                    //most likely string is base64 encoded json
                    var base64DecodedMessageText = Encoding.UTF8.GetString(Convert.FromBase64String(msg.MessageText));
                    JObject.Parse(base64DecodedMessageText);
                    messageText = base64DecodedMessageText;
                }
                catch
                {
                    try
                    {
                        //check whether it's just json
                        JObject.Parse(messageText);
                    }
                    catch
                    {
                        //message is neither base64 encoded json nor just json
                        await _poisonQueueClient.CreateIfNotExistsAsync(cancellationToken: stoppingToken);
                        await _poisonQueueClient.SendMessageAsync(msg.MessageText, stoppingToken);
                        await _queueClient.DeleteMessageAsync(msg.MessageId, msg.PopReceipt, stoppingToken);
                        operation.Telemetry.Success = false;

                        _logger.LogWarning("unrecognized message format, sent message to poison queue, original queue: {queueName}, msg: {msg}", _queueConfig.QueueName, msg.MessageText);
                        return;
                    }
                }

                using var scope = _serviceProvider.CreateScope();
                try
                {
                    await scope.ServiceProvider.GetRequiredService<TMessageHandler>().HandleMessageAsync(messageText, stoppingToken);
                    await _queueClient.DeleteMessageAsync(msg.MessageId, msg.PopReceipt, stoppingToken);
                    operation.Telemetry.Success = true;
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, "failed to handle a message, queue: {queueName}, msg: {msg}, error: {exMessage}", _queueConfig.QueueName, msg.MessageText, ex.Message);
                    operation.Telemetry.Success = false;

                    if (msg.DequeueCount < _queueConfig.MaxDequeueCount)
                    {
                        return;
                    }

                    await _poisonQueueClient.CreateIfNotExistsAsync(cancellationToken:stoppingToken);
                    await _poisonQueueClient.SendMessageAsync(msg.MessageText, stoppingToken);
                    await _queueClient.DeleteMessageAsync(msg.MessageId, msg.PopReceipt, stoppingToken);

                    _logger.LogWarning("dequeue count exceeded, sent message to poison queue, original queue: {queueName}, msg: {msg}", _queueConfig.QueueName, msg.MessageText);
                }
            }
        }
    }
}
