﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SapService10
{
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="urn:sap-com:document:sap:rfc:functions", ConfigurationName="SapService10.ZZ_MH_SALESORDER_SIMULATE_V10")]
    public interface ZZ_MH_SALESORDER_SIMULATE_V10
    {
        
        [System.ServiceModel.OperationContractAttribute(Action="urn:sap-com:document:sap:rfc:functions:ZZ_MH_SALESORDER_SIMULATE_V10:ZZ_MH_SALESO" +
            "RDER_SIMULATE_V10Request", ReplyAction="urn:sap-com:document:sap:rfc:functions:ZZ_MH_SALESORDER_SIMULATE_V10:ZZ_MH_SALESO" +
            "RDER_SIMULATE_V10Response")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Threading.Tasks.Task<SapService10.ZZ_MH_SALESORDER_SIMULATE_V10Response1> ZZ_MH_SALESORDER_SIMULATE_V10Async(SapService10.ZZ_MH_SALESORDER_SIMULATE_V10Request request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="urn:sap-com:document:sap:rfc:functions")]
    public partial class ZZ_MH_SALESORDER_SIMULATE_V101
    {
        
        private string cUSTOMERField;
        
        private string mATERIALField;
        
        private decimal qUANTITYField;

        private string sALESORGField;

        private BAPIRET2[] rETURN_TABField;
        
        private ZZBAPIWMDVE[] sCHEDULE_LINESField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string CUSTOMER
        {
            get
            {
                return this.cUSTOMERField;
            }
            set
            {
                this.cUSTOMERField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string MATERIAL
        {
            get
            {
                return this.mATERIALField;
            }
            set
            {
                this.mATERIALField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public decimal QUANTITY
        {
            get
            {
                return this.qUANTITYField;
            }
            set
            {
                this.qUANTITYField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 3)]
        public string SALESORG
        {
            get
            {
                return this.sALESORGField;
            }
            set
            {
                this.sALESORGField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=4)]
        [System.Xml.Serialization.XmlArrayItemAttribute("item", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=false)]
        public BAPIRET2[] RETURN_TAB
        {
            get
            {
                return this.rETURN_TABField;
            }
            set
            {
                this.rETURN_TABField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=5)]
        [System.Xml.Serialization.XmlArrayItemAttribute("item", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=false)]
        public ZZBAPIWMDVE[] SCHEDULE_LINES
        {
            get
            {
                return this.sCHEDULE_LINESField;
            }
            set
            {
                this.sCHEDULE_LINESField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="urn:sap-com:document:sap:rfc:functions")]
    public partial class BAPIRET2
    {
        
        private string tYPEField;
        
        private string idField;
        
        private string nUMBERField;
        
        private string mESSAGEField;
        
        private string lOG_NOField;
        
        private string lOG_MSG_NOField;
        
        private string mESSAGE_V1Field;
        
        private string mESSAGE_V2Field;
        
        private string mESSAGE_V3Field;
        
        private string mESSAGE_V4Field;
        
        private string pARAMETERField;
        
        private int rOWField;
        
        private string fIELDField;
        
        private string sYSTEMField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string TYPE
        {
            get
            {
                return this.tYPEField;
            }
            set
            {
                this.tYPEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string ID
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string NUMBER
        {
            get
            {
                return this.nUMBERField;
            }
            set
            {
                this.nUMBERField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public string MESSAGE
        {
            get
            {
                return this.mESSAGEField;
            }
            set
            {
                this.mESSAGEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=4)]
        public string LOG_NO
        {
            get
            {
                return this.lOG_NOField;
            }
            set
            {
                this.lOG_NOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=5)]
        public string LOG_MSG_NO
        {
            get
            {
                return this.lOG_MSG_NOField;
            }
            set
            {
                this.lOG_MSG_NOField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=6)]
        public string MESSAGE_V1
        {
            get
            {
                return this.mESSAGE_V1Field;
            }
            set
            {
                this.mESSAGE_V1Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=7)]
        public string MESSAGE_V2
        {
            get
            {
                return this.mESSAGE_V2Field;
            }
            set
            {
                this.mESSAGE_V2Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=8)]
        public string MESSAGE_V3
        {
            get
            {
                return this.mESSAGE_V3Field;
            }
            set
            {
                this.mESSAGE_V3Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=9)]
        public string MESSAGE_V4
        {
            get
            {
                return this.mESSAGE_V4Field;
            }
            set
            {
                this.mESSAGE_V4Field = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=10)]
        public string PARAMETER
        {
            get
            {
                return this.pARAMETERField;
            }
            set
            {
                this.pARAMETERField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=11)]
        public int ROW
        {
            get
            {
                return this.rOWField;
            }
            set
            {
                this.rOWField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=12)]
        public string FIELD
        {
            get
            {
                return this.fIELDField;
            }
            set
            {
                this.fIELDField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=13)]
        public string SYSTEM
        {
            get
            {
                return this.sYSTEMField;
            }
            set
            {
                this.sYSTEMField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="urn:sap-com:document:sap:rfc:functions")]
    public partial class ZZBAPIWMDVE
    {
        
        private string wERKSField;
        
        private string dESCRIPTIONField;
        
        private string bDCNTField;
        
        private string rEQ_DATEField;
        
        private decimal rEQ_QTYField;
        
        private string cOM_DATEField;
        
        private decimal cOM_QTYField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string WERKS
        {
            get
            {
                return this.wERKSField;
            }
            set
            {
                this.wERKSField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string DESCRIPTION
        {
            get
            {
                return this.dESCRIPTIONField;
            }
            set
            {
                this.dESCRIPTIONField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string BDCNT
        {
            get
            {
                return this.bDCNTField;
            }
            set
            {
                this.bDCNTField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        public string REQ_DATE
        {
            get
            {
                return this.rEQ_DATEField;
            }
            set
            {
                this.rEQ_DATEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=4)]
        public decimal REQ_QTY
        {
            get
            {
                return this.rEQ_QTYField;
            }
            set
            {
                this.rEQ_QTYField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=5)]
        public string COM_DATE
        {
            get
            {
                return this.cOM_DATEField;
            }
            set
            {
                this.cOM_DATEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=6)]
        public decimal COM_QTY
        {
            get
            {
                return this.cOM_QTYField;
            }
            set
            {
                this.cOM_QTYField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="urn:sap-com:document:sap:rfc:functions")]
    public partial class ZZ_MH_SALESORDER_SIMULATE_V10Response
    {
        
        private string cURRENCYField;
        
        private decimal nET_VALUEField;
        
        private BAPIRET2[] rETURN_TABField;
        
        private ZZBAPIWMDVE[] sCHEDULE_LINESField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string CURRENCY
        {
            get
            {
                return this.cURRENCYField;
            }
            set
            {
                this.cURRENCYField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public decimal NET_VALUE
        {
            get
            {
                return this.nET_VALUEField;
            }
            set
            {
                this.nET_VALUEField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        [System.Xml.Serialization.XmlArrayItemAttribute("item", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=false)]
        public BAPIRET2[] RETURN_TAB
        {
            get
            {
                return this.rETURN_TABField;
            }
            set
            {
                this.rETURN_TABField = value;
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlArrayAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=3)]
        [System.Xml.Serialization.XmlArrayItemAttribute("item", Form=System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable=false)]
        public ZZBAPIWMDVE[] SCHEDULE_LINES
        {
            get
            {
                return this.sCHEDULE_LINESField;
            }
            set
            {
                this.sCHEDULE_LINESField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ZZ_MH_SALESORDER_SIMULATE_V10Request
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:sap-com:document:sap:rfc:functions", Order=0)]
        public SapService10.ZZ_MH_SALESORDER_SIMULATE_V101 ZZ_MH_SALESORDER_SIMULATE_V10;
        
        public ZZ_MH_SALESORDER_SIMULATE_V10Request()
        {
        }
        
        public ZZ_MH_SALESORDER_SIMULATE_V10Request(SapService10.ZZ_MH_SALESORDER_SIMULATE_V101 ZZ_MH_SALESORDER_SIMULATE_V10)
        {
            this.ZZ_MH_SALESORDER_SIMULATE_V10 = ZZ_MH_SALESORDER_SIMULATE_V10;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class ZZ_MH_SALESORDER_SIMULATE_V10Response1
    {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:sap-com:document:sap:rfc:functions", Order=0)]
        public SapService10.ZZ_MH_SALESORDER_SIMULATE_V10Response ZZ_MH_SALESORDER_SIMULATE_V10Response;
        
        public ZZ_MH_SALESORDER_SIMULATE_V10Response1()
        {
        }
        
        public ZZ_MH_SALESORDER_SIMULATE_V10Response1(SapService10.ZZ_MH_SALESORDER_SIMULATE_V10Response ZZ_MH_SALESORDER_SIMULATE_V10Response)
        {
            this.ZZ_MH_SALESORDER_SIMULATE_V10Response = ZZ_MH_SALESORDER_SIMULATE_V10Response;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    public interface ZZ_MH_SALESORDER_SIMULATE_V10Channel : SapService10.ZZ_MH_SALESORDER_SIMULATE_V10, System.ServiceModel.IClientChannel
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    public partial class ZZ_MH_SALESORDER_SIMULATE_V10Client : System.ServiceModel.ClientBase<SapService10.ZZ_MH_SALESORDER_SIMULATE_V10>, SapService10.ZZ_MH_SALESORDER_SIMULATE_V10
    {
        
        public ZZ_MH_SALESORDER_SIMULATE_V10Client(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress)
        {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<SapService10.ZZ_MH_SALESORDER_SIMULATE_V10Response1> SapService10.ZZ_MH_SALESORDER_SIMULATE_V10.ZZ_MH_SALESORDER_SIMULATE_V10Async(SapService10.ZZ_MH_SALESORDER_SIMULATE_V10Request request)
        {
            return base.Channel.ZZ_MH_SALESORDER_SIMULATE_V10Async(request);
        }
        
        public System.Threading.Tasks.Task<SapService10.ZZ_MH_SALESORDER_SIMULATE_V10Response1> ZZ_MH_SALESORDER_SIMULATE_V10Async(SapService10.ZZ_MH_SALESORDER_SIMULATE_V101 ZZ_MH_SALESORDER_SIMULATE_V10)
        {
            SapService10.ZZ_MH_SALESORDER_SIMULATE_V10Request inValue = new SapService10.ZZ_MH_SALESORDER_SIMULATE_V10Request();
            inValue.ZZ_MH_SALESORDER_SIMULATE_V10 = ZZ_MH_SALESORDER_SIMULATE_V10;
            return ((SapService10.ZZ_MH_SALESORDER_SIMULATE_V10)(this)).ZZ_MH_SALESORDER_SIMULATE_V10Async(inValue);
        }
        
        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }
        
        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }
    }
}
