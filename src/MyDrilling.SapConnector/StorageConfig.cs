﻿using System;

namespace MyDrilling.SapConnector
{
    public class StorageConfig
    {
        public string ConnectionString { get; set; }
        public QueueConfig SyncEnquiryToSapQueueConfig { get; set; }
        public QueueConfig RequestSparepartPriceQueueConfig { get; set; }
        public QueueConfig EnquiryNotificationFromSapQueueConfig { get; set; }
        public QueueConfig BulletinNotificationFromSapConfig { get; set; }
        public QueueConfig FileRequestConfig { get; set; }

        public class QueueConfig
        {
            public string QueueName { get; set; }
            public TimeSpan VisibilityTimeout { get; set; }
            public TimeSpan PollingDelay { get; set; }
            public int MaxMessagesInBatch { get; set; }
            public int MaxDequeueCount { get; set; }
        }
    }
}
