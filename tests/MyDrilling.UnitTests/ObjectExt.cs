﻿namespace MyDrilling.UnitTests
{
    public static class ObjectExt
    {
        public static TObj WithId<TObj>(this TObj obj, long id) => obj.WithProperty(id, "Id");

        private static TObj WithProperty<TObj, TProp>(this TObj obj, TProp propertyValue, string propertyName)
        {
            var property = typeof(TObj).GetProperty(propertyName);
            if (property == null || property.PropertyType != typeof(TProp))
            {
                return obj;
            }

            property.SetValue(obj, propertyValue);
            return obj;
        }
    }
}
