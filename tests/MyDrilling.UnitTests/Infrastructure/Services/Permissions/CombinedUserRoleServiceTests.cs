﻿using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using MyDrilling.Core.Entities;
using MyDrilling.Core.Entities.Permission;
using MyDrilling.Core.Entities.Rig;
using MyDrilling.Infrastructure.Data;
using MyDrilling.Infrastructure.Services.Permission;
using Xunit;

namespace MyDrilling.UnitTests.Infrastructure.Services.Permissions
{
    public class CombinedUserRoleServiceTests
    {
        private const long UserId = 55;
        private const string UserUpn = "bla@bla.io";
        private const long RigId1 = 4;
        private const long RigId2 = 5;
        private const long RootCustomerId1 = 44;

        [Fact]
        public async Task GetRolesAsync_WithUserRolesAndProfiles_ReturnsAllRolesForUser()
        {
            //arrange
            var dbOptions = GetDbOptions();
            using (var db = new MyDrillingDb(dbOptions))
            {
                var user = new User(UserUpn).WithId(UserId);
                db.Users.Add(user);

                var rig1 = new Rig().WithId(RigId1);
                var rig2 = new Rig().WithId(RigId2);
                db.Rigs.Add(rig1);
                db.Rigs.Add(rig2);

                var rootCustomer = new RootCustomer(nameof(RootCustomerId1)).WithId(RootCustomerId1);
                db.RootCustomers.Add(rootCustomer);

                db.Permission_UserRoles.Add(UserRole.AssignGeneralRole(user, RoleInfo.ByRole[RoleInfo.Role.AdminSysadmin]));
                db.Permission_UserRoles.Add(UserRole.AssignRigRole(user, RoleInfo.ByRole[RoleInfo.Role.EnquiryHandlingApprovedViewer], rig1));
                db.Permission_UserRoles.Add(UserRole.AssignRootCustomerRole(user, RoleInfo.ByRole[RoleInfo.Role.CustomerAccess], rootCustomer));

                //var profile1 = new Profile("profile1", rootCustomer);
                //var profile2 = new Profile("profile2", rootCustomer);
                var profile1 = new Profile("profile1");
                var profile2 = new Profile("profile2");
                db.Permission_Profiles.Add(profile1);
                db.Permission_Profiles.Add(profile2);

                db.Permission_ProfileRoles.Add(new ProfileRole(profile1, RoleInfo.Role.EnquiryHandlingApprovedViewer));
                db.Permission_ProfileRoles.Add(new ProfileRole(profile2, RoleInfo.Role.EnquiryHandlingApprovedViewer));

                db.Permission_UserProfiles.Add(new UserProfile(profile1, user, rig1));
                db.Permission_UserProfiles.Add(new UserProfile(profile2, user, rig2));

                db.SaveChanges();
            }

            using (var db = new MyDrillingDb(dbOptions))
            {
                //act
                var rolesService = new CombinedUserRoleService(db);
                var combinedRoles = await rolesService.GetRolesAsync(UserId);

                //assert
                combinedRoles.IsSystemAdmin().Should().BeTrue();

                var enquiryRoles = combinedRoles.GetEnquiryHandlingApprovedViewerRigs();
                enquiryRoles.Length.Should().Be(2);
                enquiryRoles.Contains(RigId1).Should().BeTrue();
                enquiryRoles.Contains(RigId2).Should().BeTrue();

                var customers = combinedRoles.GetAccessToRootCustomers();
                customers.Length.Should().Be(1);
                customers.Contains(RootCustomerId1).Should().BeTrue();
            }
        }

        private DbContextOptions<MyDrillingDb> GetDbOptions()
        {
            return new DbContextOptionsBuilder<MyDrillingDb>()
                .UseInMemoryDatabase(databaseName: nameof(MyDrillingDb))
                .Options;
        }
    }
}
